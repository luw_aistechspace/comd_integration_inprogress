                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module goldseq
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _GOLDSEQUENCE_Obtain
                                     12 	.globl _GOLDSEQUENCE_Init
                                     13 	.globl _delay_ms
                                     14 	.globl _dbglink_writehex32
                                     15 	.globl _dbglink_writehex16
                                     16 	.globl _dbglink_writestr
                                     17 	.globl _dbglink_tx
                                     18 	.globl _flash_read
                                     19 	.globl _flash_write
                                     20 	.globl _flash_pageerase
                                     21 	.globl _flash_lock
                                     22 	.globl _flash_unlock
                                     23 	.globl _PORTC_7
                                     24 	.globl _PORTC_6
                                     25 	.globl _PORTC_5
                                     26 	.globl _PORTC_4
                                     27 	.globl _PORTC_3
                                     28 	.globl _PORTC_2
                                     29 	.globl _PORTC_1
                                     30 	.globl _PORTC_0
                                     31 	.globl _PORTB_7
                                     32 	.globl _PORTB_6
                                     33 	.globl _PORTB_5
                                     34 	.globl _PORTB_4
                                     35 	.globl _PORTB_3
                                     36 	.globl _PORTB_2
                                     37 	.globl _PORTB_1
                                     38 	.globl _PORTB_0
                                     39 	.globl _PORTA_7
                                     40 	.globl _PORTA_6
                                     41 	.globl _PORTA_5
                                     42 	.globl _PORTA_4
                                     43 	.globl _PORTA_3
                                     44 	.globl _PORTA_2
                                     45 	.globl _PORTA_1
                                     46 	.globl _PORTA_0
                                     47 	.globl _PINC_7
                                     48 	.globl _PINC_6
                                     49 	.globl _PINC_5
                                     50 	.globl _PINC_4
                                     51 	.globl _PINC_3
                                     52 	.globl _PINC_2
                                     53 	.globl _PINC_1
                                     54 	.globl _PINC_0
                                     55 	.globl _PINB_7
                                     56 	.globl _PINB_6
                                     57 	.globl _PINB_5
                                     58 	.globl _PINB_4
                                     59 	.globl _PINB_3
                                     60 	.globl _PINB_2
                                     61 	.globl _PINB_1
                                     62 	.globl _PINB_0
                                     63 	.globl _PINA_7
                                     64 	.globl _PINA_6
                                     65 	.globl _PINA_5
                                     66 	.globl _PINA_4
                                     67 	.globl _PINA_3
                                     68 	.globl _PINA_2
                                     69 	.globl _PINA_1
                                     70 	.globl _PINA_0
                                     71 	.globl _CY
                                     72 	.globl _AC
                                     73 	.globl _F0
                                     74 	.globl _RS1
                                     75 	.globl _RS0
                                     76 	.globl _OV
                                     77 	.globl _F1
                                     78 	.globl _P
                                     79 	.globl _IP_7
                                     80 	.globl _IP_6
                                     81 	.globl _IP_5
                                     82 	.globl _IP_4
                                     83 	.globl _IP_3
                                     84 	.globl _IP_2
                                     85 	.globl _IP_1
                                     86 	.globl _IP_0
                                     87 	.globl _EA
                                     88 	.globl _IE_7
                                     89 	.globl _IE_6
                                     90 	.globl _IE_5
                                     91 	.globl _IE_4
                                     92 	.globl _IE_3
                                     93 	.globl _IE_2
                                     94 	.globl _IE_1
                                     95 	.globl _IE_0
                                     96 	.globl _EIP_7
                                     97 	.globl _EIP_6
                                     98 	.globl _EIP_5
                                     99 	.globl _EIP_4
                                    100 	.globl _EIP_3
                                    101 	.globl _EIP_2
                                    102 	.globl _EIP_1
                                    103 	.globl _EIP_0
                                    104 	.globl _EIE_7
                                    105 	.globl _EIE_6
                                    106 	.globl _EIE_5
                                    107 	.globl _EIE_4
                                    108 	.globl _EIE_3
                                    109 	.globl _EIE_2
                                    110 	.globl _EIE_1
                                    111 	.globl _EIE_0
                                    112 	.globl _E2IP_7
                                    113 	.globl _E2IP_6
                                    114 	.globl _E2IP_5
                                    115 	.globl _E2IP_4
                                    116 	.globl _E2IP_3
                                    117 	.globl _E2IP_2
                                    118 	.globl _E2IP_1
                                    119 	.globl _E2IP_0
                                    120 	.globl _E2IE_7
                                    121 	.globl _E2IE_6
                                    122 	.globl _E2IE_5
                                    123 	.globl _E2IE_4
                                    124 	.globl _E2IE_3
                                    125 	.globl _E2IE_2
                                    126 	.globl _E2IE_1
                                    127 	.globl _E2IE_0
                                    128 	.globl _B_7
                                    129 	.globl _B_6
                                    130 	.globl _B_5
                                    131 	.globl _B_4
                                    132 	.globl _B_3
                                    133 	.globl _B_2
                                    134 	.globl _B_1
                                    135 	.globl _B_0
                                    136 	.globl _ACC_7
                                    137 	.globl _ACC_6
                                    138 	.globl _ACC_5
                                    139 	.globl _ACC_4
                                    140 	.globl _ACC_3
                                    141 	.globl _ACC_2
                                    142 	.globl _ACC_1
                                    143 	.globl _ACC_0
                                    144 	.globl _WTSTAT
                                    145 	.globl _WTIRQEN
                                    146 	.globl _WTEVTD
                                    147 	.globl _WTEVTD1
                                    148 	.globl _WTEVTD0
                                    149 	.globl _WTEVTC
                                    150 	.globl _WTEVTC1
                                    151 	.globl _WTEVTC0
                                    152 	.globl _WTEVTB
                                    153 	.globl _WTEVTB1
                                    154 	.globl _WTEVTB0
                                    155 	.globl _WTEVTA
                                    156 	.globl _WTEVTA1
                                    157 	.globl _WTEVTA0
                                    158 	.globl _WTCNTR1
                                    159 	.globl _WTCNTB
                                    160 	.globl _WTCNTB1
                                    161 	.globl _WTCNTB0
                                    162 	.globl _WTCNTA
                                    163 	.globl _WTCNTA1
                                    164 	.globl _WTCNTA0
                                    165 	.globl _WTCFGB
                                    166 	.globl _WTCFGA
                                    167 	.globl _WDTRESET
                                    168 	.globl _WDTCFG
                                    169 	.globl _U1STATUS
                                    170 	.globl _U1SHREG
                                    171 	.globl _U1MODE
                                    172 	.globl _U1CTRL
                                    173 	.globl _U0STATUS
                                    174 	.globl _U0SHREG
                                    175 	.globl _U0MODE
                                    176 	.globl _U0CTRL
                                    177 	.globl _T2STATUS
                                    178 	.globl _T2PERIOD
                                    179 	.globl _T2PERIOD1
                                    180 	.globl _T2PERIOD0
                                    181 	.globl _T2MODE
                                    182 	.globl _T2CNT
                                    183 	.globl _T2CNT1
                                    184 	.globl _T2CNT0
                                    185 	.globl _T2CLKSRC
                                    186 	.globl _T1STATUS
                                    187 	.globl _T1PERIOD
                                    188 	.globl _T1PERIOD1
                                    189 	.globl _T1PERIOD0
                                    190 	.globl _T1MODE
                                    191 	.globl _T1CNT
                                    192 	.globl _T1CNT1
                                    193 	.globl _T1CNT0
                                    194 	.globl _T1CLKSRC
                                    195 	.globl _T0STATUS
                                    196 	.globl _T0PERIOD
                                    197 	.globl _T0PERIOD1
                                    198 	.globl _T0PERIOD0
                                    199 	.globl _T0MODE
                                    200 	.globl _T0CNT
                                    201 	.globl _T0CNT1
                                    202 	.globl _T0CNT0
                                    203 	.globl _T0CLKSRC
                                    204 	.globl _SPSTATUS
                                    205 	.globl _SPSHREG
                                    206 	.globl _SPMODE
                                    207 	.globl _SPCLKSRC
                                    208 	.globl _RADIOSTAT
                                    209 	.globl _RADIOSTAT1
                                    210 	.globl _RADIOSTAT0
                                    211 	.globl _RADIODATA
                                    212 	.globl _RADIODATA3
                                    213 	.globl _RADIODATA2
                                    214 	.globl _RADIODATA1
                                    215 	.globl _RADIODATA0
                                    216 	.globl _RADIOADDR
                                    217 	.globl _RADIOADDR1
                                    218 	.globl _RADIOADDR0
                                    219 	.globl _RADIOACC
                                    220 	.globl _OC1STATUS
                                    221 	.globl _OC1PIN
                                    222 	.globl _OC1MODE
                                    223 	.globl _OC1COMP
                                    224 	.globl _OC1COMP1
                                    225 	.globl _OC1COMP0
                                    226 	.globl _OC0STATUS
                                    227 	.globl _OC0PIN
                                    228 	.globl _OC0MODE
                                    229 	.globl _OC0COMP
                                    230 	.globl _OC0COMP1
                                    231 	.globl _OC0COMP0
                                    232 	.globl _NVSTATUS
                                    233 	.globl _NVKEY
                                    234 	.globl _NVDATA
                                    235 	.globl _NVDATA1
                                    236 	.globl _NVDATA0
                                    237 	.globl _NVADDR
                                    238 	.globl _NVADDR1
                                    239 	.globl _NVADDR0
                                    240 	.globl _IC1STATUS
                                    241 	.globl _IC1MODE
                                    242 	.globl _IC1CAPT
                                    243 	.globl _IC1CAPT1
                                    244 	.globl _IC1CAPT0
                                    245 	.globl _IC0STATUS
                                    246 	.globl _IC0MODE
                                    247 	.globl _IC0CAPT
                                    248 	.globl _IC0CAPT1
                                    249 	.globl _IC0CAPT0
                                    250 	.globl _PORTR
                                    251 	.globl _PORTC
                                    252 	.globl _PORTB
                                    253 	.globl _PORTA
                                    254 	.globl _PINR
                                    255 	.globl _PINC
                                    256 	.globl _PINB
                                    257 	.globl _PINA
                                    258 	.globl _DIRR
                                    259 	.globl _DIRC
                                    260 	.globl _DIRB
                                    261 	.globl _DIRA
                                    262 	.globl _DBGLNKSTAT
                                    263 	.globl _DBGLNKBUF
                                    264 	.globl _CODECONFIG
                                    265 	.globl _CLKSTAT
                                    266 	.globl _CLKCON
                                    267 	.globl _ANALOGCOMP
                                    268 	.globl _ADCCONV
                                    269 	.globl _ADCCLKSRC
                                    270 	.globl _ADCCH3CONFIG
                                    271 	.globl _ADCCH2CONFIG
                                    272 	.globl _ADCCH1CONFIG
                                    273 	.globl _ADCCH0CONFIG
                                    274 	.globl __XPAGE
                                    275 	.globl _XPAGE
                                    276 	.globl _SP
                                    277 	.globl _PSW
                                    278 	.globl _PCON
                                    279 	.globl _IP
                                    280 	.globl _IE
                                    281 	.globl _EIP
                                    282 	.globl _EIE
                                    283 	.globl _E2IP
                                    284 	.globl _E2IE
                                    285 	.globl _DPS
                                    286 	.globl _DPTR1
                                    287 	.globl _DPTR0
                                    288 	.globl _DPL1
                                    289 	.globl _DPL
                                    290 	.globl _DPH1
                                    291 	.globl _DPH
                                    292 	.globl _B
                                    293 	.globl _ACC
                                    294 	.globl _RNGMODE
                                    295 	.globl _RNGCLKSRC1
                                    296 	.globl _RNGCLKSRC0
                                    297 	.globl _RNGBYTE
                                    298 	.globl _AESOUTADDR
                                    299 	.globl _AESOUTADDR1
                                    300 	.globl _AESOUTADDR0
                                    301 	.globl _AESMODE
                                    302 	.globl _AESKEYADDR
                                    303 	.globl _AESKEYADDR1
                                    304 	.globl _AESKEYADDR0
                                    305 	.globl _AESINADDR
                                    306 	.globl _AESINADDR1
                                    307 	.globl _AESINADDR0
                                    308 	.globl _AESCURBLOCK
                                    309 	.globl _AESCONFIG
                                    310 	.globl _XTALREADY
                                    311 	.globl _XTALOSC
                                    312 	.globl _XTALAMPL
                                    313 	.globl _SILICONREV
                                    314 	.globl _SCRATCH3
                                    315 	.globl _SCRATCH2
                                    316 	.globl _SCRATCH1
                                    317 	.globl _SCRATCH0
                                    318 	.globl _RADIOMUX
                                    319 	.globl _RADIOFSTATADDR
                                    320 	.globl _RADIOFSTATADDR1
                                    321 	.globl _RADIOFSTATADDR0
                                    322 	.globl _RADIOFDATAADDR
                                    323 	.globl _RADIOFDATAADDR1
                                    324 	.globl _RADIOFDATAADDR0
                                    325 	.globl _OSCRUN
                                    326 	.globl _OSCREADY
                                    327 	.globl _OSCFORCERUN
                                    328 	.globl _OSCCALIB
                                    329 	.globl _MISCCTRL
                                    330 	.globl _LPXOSCGM
                                    331 	.globl _LPOSCREF
                                    332 	.globl _LPOSCREF1
                                    333 	.globl _LPOSCREF0
                                    334 	.globl _LPOSCPER
                                    335 	.globl _LPOSCPER1
                                    336 	.globl _LPOSCPER0
                                    337 	.globl _LPOSCKFILT
                                    338 	.globl _LPOSCKFILT1
                                    339 	.globl _LPOSCKFILT0
                                    340 	.globl _LPOSCFREQ
                                    341 	.globl _LPOSCFREQ1
                                    342 	.globl _LPOSCFREQ0
                                    343 	.globl _LPOSCCONFIG
                                    344 	.globl _PINSEL
                                    345 	.globl _PINCHGC
                                    346 	.globl _PINCHGB
                                    347 	.globl _PINCHGA
                                    348 	.globl _PALTRADIO
                                    349 	.globl _PALTC
                                    350 	.globl _PALTB
                                    351 	.globl _PALTA
                                    352 	.globl _INTCHGC
                                    353 	.globl _INTCHGB
                                    354 	.globl _INTCHGA
                                    355 	.globl _EXTIRQ
                                    356 	.globl _GPIOENABLE
                                    357 	.globl _ANALOGA
                                    358 	.globl _FRCOSCREF
                                    359 	.globl _FRCOSCREF1
                                    360 	.globl _FRCOSCREF0
                                    361 	.globl _FRCOSCPER
                                    362 	.globl _FRCOSCPER1
                                    363 	.globl _FRCOSCPER0
                                    364 	.globl _FRCOSCKFILT
                                    365 	.globl _FRCOSCKFILT1
                                    366 	.globl _FRCOSCKFILT0
                                    367 	.globl _FRCOSCFREQ
                                    368 	.globl _FRCOSCFREQ1
                                    369 	.globl _FRCOSCFREQ0
                                    370 	.globl _FRCOSCCTRL
                                    371 	.globl _FRCOSCCONFIG
                                    372 	.globl _DMA1CONFIG
                                    373 	.globl _DMA1ADDR
                                    374 	.globl _DMA1ADDR1
                                    375 	.globl _DMA1ADDR0
                                    376 	.globl _DMA0CONFIG
                                    377 	.globl _DMA0ADDR
                                    378 	.globl _DMA0ADDR1
                                    379 	.globl _DMA0ADDR0
                                    380 	.globl _ADCTUNE2
                                    381 	.globl _ADCTUNE1
                                    382 	.globl _ADCTUNE0
                                    383 	.globl _ADCCH3VAL
                                    384 	.globl _ADCCH3VAL1
                                    385 	.globl _ADCCH3VAL0
                                    386 	.globl _ADCCH2VAL
                                    387 	.globl _ADCCH2VAL1
                                    388 	.globl _ADCCH2VAL0
                                    389 	.globl _ADCCH1VAL
                                    390 	.globl _ADCCH1VAL1
                                    391 	.globl _ADCCH1VAL0
                                    392 	.globl _ADCCH0VAL
                                    393 	.globl _ADCCH0VAL1
                                    394 	.globl _ADCCH0VAL0
                                    395 ;--------------------------------------------------------
                                    396 ; special function registers
                                    397 ;--------------------------------------------------------
                                    398 	.area RSEG    (ABS,DATA)
      000000                        399 	.org 0x0000
                           0000E0   400 G$ACC$0$0 == 0x00e0
                           0000E0   401 _ACC	=	0x00e0
                           0000F0   402 G$B$0$0 == 0x00f0
                           0000F0   403 _B	=	0x00f0
                           000083   404 G$DPH$0$0 == 0x0083
                           000083   405 _DPH	=	0x0083
                           000085   406 G$DPH1$0$0 == 0x0085
                           000085   407 _DPH1	=	0x0085
                           000082   408 G$DPL$0$0 == 0x0082
                           000082   409 _DPL	=	0x0082
                           000084   410 G$DPL1$0$0 == 0x0084
                           000084   411 _DPL1	=	0x0084
                           008382   412 G$DPTR0$0$0 == 0x8382
                           008382   413 _DPTR0	=	0x8382
                           008584   414 G$DPTR1$0$0 == 0x8584
                           008584   415 _DPTR1	=	0x8584
                           000086   416 G$DPS$0$0 == 0x0086
                           000086   417 _DPS	=	0x0086
                           0000A0   418 G$E2IE$0$0 == 0x00a0
                           0000A0   419 _E2IE	=	0x00a0
                           0000C0   420 G$E2IP$0$0 == 0x00c0
                           0000C0   421 _E2IP	=	0x00c0
                           000098   422 G$EIE$0$0 == 0x0098
                           000098   423 _EIE	=	0x0098
                           0000B0   424 G$EIP$0$0 == 0x00b0
                           0000B0   425 _EIP	=	0x00b0
                           0000A8   426 G$IE$0$0 == 0x00a8
                           0000A8   427 _IE	=	0x00a8
                           0000B8   428 G$IP$0$0 == 0x00b8
                           0000B8   429 _IP	=	0x00b8
                           000087   430 G$PCON$0$0 == 0x0087
                           000087   431 _PCON	=	0x0087
                           0000D0   432 G$PSW$0$0 == 0x00d0
                           0000D0   433 _PSW	=	0x00d0
                           000081   434 G$SP$0$0 == 0x0081
                           000081   435 _SP	=	0x0081
                           0000D9   436 G$XPAGE$0$0 == 0x00d9
                           0000D9   437 _XPAGE	=	0x00d9
                           0000D9   438 G$_XPAGE$0$0 == 0x00d9
                           0000D9   439 __XPAGE	=	0x00d9
                           0000CA   440 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   441 _ADCCH0CONFIG	=	0x00ca
                           0000CB   442 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   443 _ADCCH1CONFIG	=	0x00cb
                           0000D2   444 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   445 _ADCCH2CONFIG	=	0x00d2
                           0000D3   446 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   447 _ADCCH3CONFIG	=	0x00d3
                           0000D1   448 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   449 _ADCCLKSRC	=	0x00d1
                           0000C9   450 G$ADCCONV$0$0 == 0x00c9
                           0000C9   451 _ADCCONV	=	0x00c9
                           0000E1   452 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   453 _ANALOGCOMP	=	0x00e1
                           0000C6   454 G$CLKCON$0$0 == 0x00c6
                           0000C6   455 _CLKCON	=	0x00c6
                           0000C7   456 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   457 _CLKSTAT	=	0x00c7
                           000097   458 G$CODECONFIG$0$0 == 0x0097
                           000097   459 _CODECONFIG	=	0x0097
                           0000E3   460 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   461 _DBGLNKBUF	=	0x00e3
                           0000E2   462 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   463 _DBGLNKSTAT	=	0x00e2
                           000089   464 G$DIRA$0$0 == 0x0089
                           000089   465 _DIRA	=	0x0089
                           00008A   466 G$DIRB$0$0 == 0x008a
                           00008A   467 _DIRB	=	0x008a
                           00008B   468 G$DIRC$0$0 == 0x008b
                           00008B   469 _DIRC	=	0x008b
                           00008E   470 G$DIRR$0$0 == 0x008e
                           00008E   471 _DIRR	=	0x008e
                           0000C8   472 G$PINA$0$0 == 0x00c8
                           0000C8   473 _PINA	=	0x00c8
                           0000E8   474 G$PINB$0$0 == 0x00e8
                           0000E8   475 _PINB	=	0x00e8
                           0000F8   476 G$PINC$0$0 == 0x00f8
                           0000F8   477 _PINC	=	0x00f8
                           00008D   478 G$PINR$0$0 == 0x008d
                           00008D   479 _PINR	=	0x008d
                           000080   480 G$PORTA$0$0 == 0x0080
                           000080   481 _PORTA	=	0x0080
                           000088   482 G$PORTB$0$0 == 0x0088
                           000088   483 _PORTB	=	0x0088
                           000090   484 G$PORTC$0$0 == 0x0090
                           000090   485 _PORTC	=	0x0090
                           00008C   486 G$PORTR$0$0 == 0x008c
                           00008C   487 _PORTR	=	0x008c
                           0000CE   488 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   489 _IC0CAPT0	=	0x00ce
                           0000CF   490 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   491 _IC0CAPT1	=	0x00cf
                           00CFCE   492 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   493 _IC0CAPT	=	0xcfce
                           0000CC   494 G$IC0MODE$0$0 == 0x00cc
                           0000CC   495 _IC0MODE	=	0x00cc
                           0000CD   496 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   497 _IC0STATUS	=	0x00cd
                           0000D6   498 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   499 _IC1CAPT0	=	0x00d6
                           0000D7   500 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   501 _IC1CAPT1	=	0x00d7
                           00D7D6   502 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   503 _IC1CAPT	=	0xd7d6
                           0000D4   504 G$IC1MODE$0$0 == 0x00d4
                           0000D4   505 _IC1MODE	=	0x00d4
                           0000D5   506 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   507 _IC1STATUS	=	0x00d5
                           000092   508 G$NVADDR0$0$0 == 0x0092
                           000092   509 _NVADDR0	=	0x0092
                           000093   510 G$NVADDR1$0$0 == 0x0093
                           000093   511 _NVADDR1	=	0x0093
                           009392   512 G$NVADDR$0$0 == 0x9392
                           009392   513 _NVADDR	=	0x9392
                           000094   514 G$NVDATA0$0$0 == 0x0094
                           000094   515 _NVDATA0	=	0x0094
                           000095   516 G$NVDATA1$0$0 == 0x0095
                           000095   517 _NVDATA1	=	0x0095
                           009594   518 G$NVDATA$0$0 == 0x9594
                           009594   519 _NVDATA	=	0x9594
                           000096   520 G$NVKEY$0$0 == 0x0096
                           000096   521 _NVKEY	=	0x0096
                           000091   522 G$NVSTATUS$0$0 == 0x0091
                           000091   523 _NVSTATUS	=	0x0091
                           0000BC   524 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   525 _OC0COMP0	=	0x00bc
                           0000BD   526 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   527 _OC0COMP1	=	0x00bd
                           00BDBC   528 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   529 _OC0COMP	=	0xbdbc
                           0000B9   530 G$OC0MODE$0$0 == 0x00b9
                           0000B9   531 _OC0MODE	=	0x00b9
                           0000BA   532 G$OC0PIN$0$0 == 0x00ba
                           0000BA   533 _OC0PIN	=	0x00ba
                           0000BB   534 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   535 _OC0STATUS	=	0x00bb
                           0000C4   536 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   537 _OC1COMP0	=	0x00c4
                           0000C5   538 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   539 _OC1COMP1	=	0x00c5
                           00C5C4   540 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   541 _OC1COMP	=	0xc5c4
                           0000C1   542 G$OC1MODE$0$0 == 0x00c1
                           0000C1   543 _OC1MODE	=	0x00c1
                           0000C2   544 G$OC1PIN$0$0 == 0x00c2
                           0000C2   545 _OC1PIN	=	0x00c2
                           0000C3   546 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   547 _OC1STATUS	=	0x00c3
                           0000B1   548 G$RADIOACC$0$0 == 0x00b1
                           0000B1   549 _RADIOACC	=	0x00b1
                           0000B3   550 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   551 _RADIOADDR0	=	0x00b3
                           0000B2   552 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   553 _RADIOADDR1	=	0x00b2
                           00B2B3   554 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   555 _RADIOADDR	=	0xb2b3
                           0000B7   556 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   557 _RADIODATA0	=	0x00b7
                           0000B6   558 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   559 _RADIODATA1	=	0x00b6
                           0000B5   560 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   561 _RADIODATA2	=	0x00b5
                           0000B4   562 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   563 _RADIODATA3	=	0x00b4
                           B4B5B6B7   564 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   565 _RADIODATA	=	0xb4b5b6b7
                           0000BE   566 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   567 _RADIOSTAT0	=	0x00be
                           0000BF   568 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   569 _RADIOSTAT1	=	0x00bf
                           00BFBE   570 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   571 _RADIOSTAT	=	0xbfbe
                           0000DF   572 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   573 _SPCLKSRC	=	0x00df
                           0000DC   574 G$SPMODE$0$0 == 0x00dc
                           0000DC   575 _SPMODE	=	0x00dc
                           0000DE   576 G$SPSHREG$0$0 == 0x00de
                           0000DE   577 _SPSHREG	=	0x00de
                           0000DD   578 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   579 _SPSTATUS	=	0x00dd
                           00009A   580 G$T0CLKSRC$0$0 == 0x009a
                           00009A   581 _T0CLKSRC	=	0x009a
                           00009C   582 G$T0CNT0$0$0 == 0x009c
                           00009C   583 _T0CNT0	=	0x009c
                           00009D   584 G$T0CNT1$0$0 == 0x009d
                           00009D   585 _T0CNT1	=	0x009d
                           009D9C   586 G$T0CNT$0$0 == 0x9d9c
                           009D9C   587 _T0CNT	=	0x9d9c
                           000099   588 G$T0MODE$0$0 == 0x0099
                           000099   589 _T0MODE	=	0x0099
                           00009E   590 G$T0PERIOD0$0$0 == 0x009e
                           00009E   591 _T0PERIOD0	=	0x009e
                           00009F   592 G$T0PERIOD1$0$0 == 0x009f
                           00009F   593 _T0PERIOD1	=	0x009f
                           009F9E   594 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   595 _T0PERIOD	=	0x9f9e
                           00009B   596 G$T0STATUS$0$0 == 0x009b
                           00009B   597 _T0STATUS	=	0x009b
                           0000A2   598 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   599 _T1CLKSRC	=	0x00a2
                           0000A4   600 G$T1CNT0$0$0 == 0x00a4
                           0000A4   601 _T1CNT0	=	0x00a4
                           0000A5   602 G$T1CNT1$0$0 == 0x00a5
                           0000A5   603 _T1CNT1	=	0x00a5
                           00A5A4   604 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   605 _T1CNT	=	0xa5a4
                           0000A1   606 G$T1MODE$0$0 == 0x00a1
                           0000A1   607 _T1MODE	=	0x00a1
                           0000A6   608 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   609 _T1PERIOD0	=	0x00a6
                           0000A7   610 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   611 _T1PERIOD1	=	0x00a7
                           00A7A6   612 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   613 _T1PERIOD	=	0xa7a6
                           0000A3   614 G$T1STATUS$0$0 == 0x00a3
                           0000A3   615 _T1STATUS	=	0x00a3
                           0000AA   616 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   617 _T2CLKSRC	=	0x00aa
                           0000AC   618 G$T2CNT0$0$0 == 0x00ac
                           0000AC   619 _T2CNT0	=	0x00ac
                           0000AD   620 G$T2CNT1$0$0 == 0x00ad
                           0000AD   621 _T2CNT1	=	0x00ad
                           00ADAC   622 G$T2CNT$0$0 == 0xadac
                           00ADAC   623 _T2CNT	=	0xadac
                           0000A9   624 G$T2MODE$0$0 == 0x00a9
                           0000A9   625 _T2MODE	=	0x00a9
                           0000AE   626 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   627 _T2PERIOD0	=	0x00ae
                           0000AF   628 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   629 _T2PERIOD1	=	0x00af
                           00AFAE   630 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   631 _T2PERIOD	=	0xafae
                           0000AB   632 G$T2STATUS$0$0 == 0x00ab
                           0000AB   633 _T2STATUS	=	0x00ab
                           0000E4   634 G$U0CTRL$0$0 == 0x00e4
                           0000E4   635 _U0CTRL	=	0x00e4
                           0000E7   636 G$U0MODE$0$0 == 0x00e7
                           0000E7   637 _U0MODE	=	0x00e7
                           0000E6   638 G$U0SHREG$0$0 == 0x00e6
                           0000E6   639 _U0SHREG	=	0x00e6
                           0000E5   640 G$U0STATUS$0$0 == 0x00e5
                           0000E5   641 _U0STATUS	=	0x00e5
                           0000EC   642 G$U1CTRL$0$0 == 0x00ec
                           0000EC   643 _U1CTRL	=	0x00ec
                           0000EF   644 G$U1MODE$0$0 == 0x00ef
                           0000EF   645 _U1MODE	=	0x00ef
                           0000EE   646 G$U1SHREG$0$0 == 0x00ee
                           0000EE   647 _U1SHREG	=	0x00ee
                           0000ED   648 G$U1STATUS$0$0 == 0x00ed
                           0000ED   649 _U1STATUS	=	0x00ed
                           0000DA   650 G$WDTCFG$0$0 == 0x00da
                           0000DA   651 _WDTCFG	=	0x00da
                           0000DB   652 G$WDTRESET$0$0 == 0x00db
                           0000DB   653 _WDTRESET	=	0x00db
                           0000F1   654 G$WTCFGA$0$0 == 0x00f1
                           0000F1   655 _WTCFGA	=	0x00f1
                           0000F9   656 G$WTCFGB$0$0 == 0x00f9
                           0000F9   657 _WTCFGB	=	0x00f9
                           0000F2   658 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   659 _WTCNTA0	=	0x00f2
                           0000F3   660 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   661 _WTCNTA1	=	0x00f3
                           00F3F2   662 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   663 _WTCNTA	=	0xf3f2
                           0000FA   664 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   665 _WTCNTB0	=	0x00fa
                           0000FB   666 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   667 _WTCNTB1	=	0x00fb
                           00FBFA   668 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   669 _WTCNTB	=	0xfbfa
                           0000EB   670 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   671 _WTCNTR1	=	0x00eb
                           0000F4   672 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   673 _WTEVTA0	=	0x00f4
                           0000F5   674 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   675 _WTEVTA1	=	0x00f5
                           00F5F4   676 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   677 _WTEVTA	=	0xf5f4
                           0000F6   678 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   679 _WTEVTB0	=	0x00f6
                           0000F7   680 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   681 _WTEVTB1	=	0x00f7
                           00F7F6   682 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   683 _WTEVTB	=	0xf7f6
                           0000FC   684 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   685 _WTEVTC0	=	0x00fc
                           0000FD   686 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   687 _WTEVTC1	=	0x00fd
                           00FDFC   688 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   689 _WTEVTC	=	0xfdfc
                           0000FE   690 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   691 _WTEVTD0	=	0x00fe
                           0000FF   692 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   693 _WTEVTD1	=	0x00ff
                           00FFFE   694 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   695 _WTEVTD	=	0xfffe
                           0000E9   696 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   697 _WTIRQEN	=	0x00e9
                           0000EA   698 G$WTSTAT$0$0 == 0x00ea
                           0000EA   699 _WTSTAT	=	0x00ea
                                    700 ;--------------------------------------------------------
                                    701 ; special function bits
                                    702 ;--------------------------------------------------------
                                    703 	.area RSEG    (ABS,DATA)
      000000                        704 	.org 0x0000
                           0000E0   705 G$ACC_0$0$0 == 0x00e0
                           0000E0   706 _ACC_0	=	0x00e0
                           0000E1   707 G$ACC_1$0$0 == 0x00e1
                           0000E1   708 _ACC_1	=	0x00e1
                           0000E2   709 G$ACC_2$0$0 == 0x00e2
                           0000E2   710 _ACC_2	=	0x00e2
                           0000E3   711 G$ACC_3$0$0 == 0x00e3
                           0000E3   712 _ACC_3	=	0x00e3
                           0000E4   713 G$ACC_4$0$0 == 0x00e4
                           0000E4   714 _ACC_4	=	0x00e4
                           0000E5   715 G$ACC_5$0$0 == 0x00e5
                           0000E5   716 _ACC_5	=	0x00e5
                           0000E6   717 G$ACC_6$0$0 == 0x00e6
                           0000E6   718 _ACC_6	=	0x00e6
                           0000E7   719 G$ACC_7$0$0 == 0x00e7
                           0000E7   720 _ACC_7	=	0x00e7
                           0000F0   721 G$B_0$0$0 == 0x00f0
                           0000F0   722 _B_0	=	0x00f0
                           0000F1   723 G$B_1$0$0 == 0x00f1
                           0000F1   724 _B_1	=	0x00f1
                           0000F2   725 G$B_2$0$0 == 0x00f2
                           0000F2   726 _B_2	=	0x00f2
                           0000F3   727 G$B_3$0$0 == 0x00f3
                           0000F3   728 _B_3	=	0x00f3
                           0000F4   729 G$B_4$0$0 == 0x00f4
                           0000F4   730 _B_4	=	0x00f4
                           0000F5   731 G$B_5$0$0 == 0x00f5
                           0000F5   732 _B_5	=	0x00f5
                           0000F6   733 G$B_6$0$0 == 0x00f6
                           0000F6   734 _B_6	=	0x00f6
                           0000F7   735 G$B_7$0$0 == 0x00f7
                           0000F7   736 _B_7	=	0x00f7
                           0000A0   737 G$E2IE_0$0$0 == 0x00a0
                           0000A0   738 _E2IE_0	=	0x00a0
                           0000A1   739 G$E2IE_1$0$0 == 0x00a1
                           0000A1   740 _E2IE_1	=	0x00a1
                           0000A2   741 G$E2IE_2$0$0 == 0x00a2
                           0000A2   742 _E2IE_2	=	0x00a2
                           0000A3   743 G$E2IE_3$0$0 == 0x00a3
                           0000A3   744 _E2IE_3	=	0x00a3
                           0000A4   745 G$E2IE_4$0$0 == 0x00a4
                           0000A4   746 _E2IE_4	=	0x00a4
                           0000A5   747 G$E2IE_5$0$0 == 0x00a5
                           0000A5   748 _E2IE_5	=	0x00a5
                           0000A6   749 G$E2IE_6$0$0 == 0x00a6
                           0000A6   750 _E2IE_6	=	0x00a6
                           0000A7   751 G$E2IE_7$0$0 == 0x00a7
                           0000A7   752 _E2IE_7	=	0x00a7
                           0000C0   753 G$E2IP_0$0$0 == 0x00c0
                           0000C0   754 _E2IP_0	=	0x00c0
                           0000C1   755 G$E2IP_1$0$0 == 0x00c1
                           0000C1   756 _E2IP_1	=	0x00c1
                           0000C2   757 G$E2IP_2$0$0 == 0x00c2
                           0000C2   758 _E2IP_2	=	0x00c2
                           0000C3   759 G$E2IP_3$0$0 == 0x00c3
                           0000C3   760 _E2IP_3	=	0x00c3
                           0000C4   761 G$E2IP_4$0$0 == 0x00c4
                           0000C4   762 _E2IP_4	=	0x00c4
                           0000C5   763 G$E2IP_5$0$0 == 0x00c5
                           0000C5   764 _E2IP_5	=	0x00c5
                           0000C6   765 G$E2IP_6$0$0 == 0x00c6
                           0000C6   766 _E2IP_6	=	0x00c6
                           0000C7   767 G$E2IP_7$0$0 == 0x00c7
                           0000C7   768 _E2IP_7	=	0x00c7
                           000098   769 G$EIE_0$0$0 == 0x0098
                           000098   770 _EIE_0	=	0x0098
                           000099   771 G$EIE_1$0$0 == 0x0099
                           000099   772 _EIE_1	=	0x0099
                           00009A   773 G$EIE_2$0$0 == 0x009a
                           00009A   774 _EIE_2	=	0x009a
                           00009B   775 G$EIE_3$0$0 == 0x009b
                           00009B   776 _EIE_3	=	0x009b
                           00009C   777 G$EIE_4$0$0 == 0x009c
                           00009C   778 _EIE_4	=	0x009c
                           00009D   779 G$EIE_5$0$0 == 0x009d
                           00009D   780 _EIE_5	=	0x009d
                           00009E   781 G$EIE_6$0$0 == 0x009e
                           00009E   782 _EIE_6	=	0x009e
                           00009F   783 G$EIE_7$0$0 == 0x009f
                           00009F   784 _EIE_7	=	0x009f
                           0000B0   785 G$EIP_0$0$0 == 0x00b0
                           0000B0   786 _EIP_0	=	0x00b0
                           0000B1   787 G$EIP_1$0$0 == 0x00b1
                           0000B1   788 _EIP_1	=	0x00b1
                           0000B2   789 G$EIP_2$0$0 == 0x00b2
                           0000B2   790 _EIP_2	=	0x00b2
                           0000B3   791 G$EIP_3$0$0 == 0x00b3
                           0000B3   792 _EIP_3	=	0x00b3
                           0000B4   793 G$EIP_4$0$0 == 0x00b4
                           0000B4   794 _EIP_4	=	0x00b4
                           0000B5   795 G$EIP_5$0$0 == 0x00b5
                           0000B5   796 _EIP_5	=	0x00b5
                           0000B6   797 G$EIP_6$0$0 == 0x00b6
                           0000B6   798 _EIP_6	=	0x00b6
                           0000B7   799 G$EIP_7$0$0 == 0x00b7
                           0000B7   800 _EIP_7	=	0x00b7
                           0000A8   801 G$IE_0$0$0 == 0x00a8
                           0000A8   802 _IE_0	=	0x00a8
                           0000A9   803 G$IE_1$0$0 == 0x00a9
                           0000A9   804 _IE_1	=	0x00a9
                           0000AA   805 G$IE_2$0$0 == 0x00aa
                           0000AA   806 _IE_2	=	0x00aa
                           0000AB   807 G$IE_3$0$0 == 0x00ab
                           0000AB   808 _IE_3	=	0x00ab
                           0000AC   809 G$IE_4$0$0 == 0x00ac
                           0000AC   810 _IE_4	=	0x00ac
                           0000AD   811 G$IE_5$0$0 == 0x00ad
                           0000AD   812 _IE_5	=	0x00ad
                           0000AE   813 G$IE_6$0$0 == 0x00ae
                           0000AE   814 _IE_6	=	0x00ae
                           0000AF   815 G$IE_7$0$0 == 0x00af
                           0000AF   816 _IE_7	=	0x00af
                           0000AF   817 G$EA$0$0 == 0x00af
                           0000AF   818 _EA	=	0x00af
                           0000B8   819 G$IP_0$0$0 == 0x00b8
                           0000B8   820 _IP_0	=	0x00b8
                           0000B9   821 G$IP_1$0$0 == 0x00b9
                           0000B9   822 _IP_1	=	0x00b9
                           0000BA   823 G$IP_2$0$0 == 0x00ba
                           0000BA   824 _IP_2	=	0x00ba
                           0000BB   825 G$IP_3$0$0 == 0x00bb
                           0000BB   826 _IP_3	=	0x00bb
                           0000BC   827 G$IP_4$0$0 == 0x00bc
                           0000BC   828 _IP_4	=	0x00bc
                           0000BD   829 G$IP_5$0$0 == 0x00bd
                           0000BD   830 _IP_5	=	0x00bd
                           0000BE   831 G$IP_6$0$0 == 0x00be
                           0000BE   832 _IP_6	=	0x00be
                           0000BF   833 G$IP_7$0$0 == 0x00bf
                           0000BF   834 _IP_7	=	0x00bf
                           0000D0   835 G$P$0$0 == 0x00d0
                           0000D0   836 _P	=	0x00d0
                           0000D1   837 G$F1$0$0 == 0x00d1
                           0000D1   838 _F1	=	0x00d1
                           0000D2   839 G$OV$0$0 == 0x00d2
                           0000D2   840 _OV	=	0x00d2
                           0000D3   841 G$RS0$0$0 == 0x00d3
                           0000D3   842 _RS0	=	0x00d3
                           0000D4   843 G$RS1$0$0 == 0x00d4
                           0000D4   844 _RS1	=	0x00d4
                           0000D5   845 G$F0$0$0 == 0x00d5
                           0000D5   846 _F0	=	0x00d5
                           0000D6   847 G$AC$0$0 == 0x00d6
                           0000D6   848 _AC	=	0x00d6
                           0000D7   849 G$CY$0$0 == 0x00d7
                           0000D7   850 _CY	=	0x00d7
                           0000C8   851 G$PINA_0$0$0 == 0x00c8
                           0000C8   852 _PINA_0	=	0x00c8
                           0000C9   853 G$PINA_1$0$0 == 0x00c9
                           0000C9   854 _PINA_1	=	0x00c9
                           0000CA   855 G$PINA_2$0$0 == 0x00ca
                           0000CA   856 _PINA_2	=	0x00ca
                           0000CB   857 G$PINA_3$0$0 == 0x00cb
                           0000CB   858 _PINA_3	=	0x00cb
                           0000CC   859 G$PINA_4$0$0 == 0x00cc
                           0000CC   860 _PINA_4	=	0x00cc
                           0000CD   861 G$PINA_5$0$0 == 0x00cd
                           0000CD   862 _PINA_5	=	0x00cd
                           0000CE   863 G$PINA_6$0$0 == 0x00ce
                           0000CE   864 _PINA_6	=	0x00ce
                           0000CF   865 G$PINA_7$0$0 == 0x00cf
                           0000CF   866 _PINA_7	=	0x00cf
                           0000E8   867 G$PINB_0$0$0 == 0x00e8
                           0000E8   868 _PINB_0	=	0x00e8
                           0000E9   869 G$PINB_1$0$0 == 0x00e9
                           0000E9   870 _PINB_1	=	0x00e9
                           0000EA   871 G$PINB_2$0$0 == 0x00ea
                           0000EA   872 _PINB_2	=	0x00ea
                           0000EB   873 G$PINB_3$0$0 == 0x00eb
                           0000EB   874 _PINB_3	=	0x00eb
                           0000EC   875 G$PINB_4$0$0 == 0x00ec
                           0000EC   876 _PINB_4	=	0x00ec
                           0000ED   877 G$PINB_5$0$0 == 0x00ed
                           0000ED   878 _PINB_5	=	0x00ed
                           0000EE   879 G$PINB_6$0$0 == 0x00ee
                           0000EE   880 _PINB_6	=	0x00ee
                           0000EF   881 G$PINB_7$0$0 == 0x00ef
                           0000EF   882 _PINB_7	=	0x00ef
                           0000F8   883 G$PINC_0$0$0 == 0x00f8
                           0000F8   884 _PINC_0	=	0x00f8
                           0000F9   885 G$PINC_1$0$0 == 0x00f9
                           0000F9   886 _PINC_1	=	0x00f9
                           0000FA   887 G$PINC_2$0$0 == 0x00fa
                           0000FA   888 _PINC_2	=	0x00fa
                           0000FB   889 G$PINC_3$0$0 == 0x00fb
                           0000FB   890 _PINC_3	=	0x00fb
                           0000FC   891 G$PINC_4$0$0 == 0x00fc
                           0000FC   892 _PINC_4	=	0x00fc
                           0000FD   893 G$PINC_5$0$0 == 0x00fd
                           0000FD   894 _PINC_5	=	0x00fd
                           0000FE   895 G$PINC_6$0$0 == 0x00fe
                           0000FE   896 _PINC_6	=	0x00fe
                           0000FF   897 G$PINC_7$0$0 == 0x00ff
                           0000FF   898 _PINC_7	=	0x00ff
                           000080   899 G$PORTA_0$0$0 == 0x0080
                           000080   900 _PORTA_0	=	0x0080
                           000081   901 G$PORTA_1$0$0 == 0x0081
                           000081   902 _PORTA_1	=	0x0081
                           000082   903 G$PORTA_2$0$0 == 0x0082
                           000082   904 _PORTA_2	=	0x0082
                           000083   905 G$PORTA_3$0$0 == 0x0083
                           000083   906 _PORTA_3	=	0x0083
                           000084   907 G$PORTA_4$0$0 == 0x0084
                           000084   908 _PORTA_4	=	0x0084
                           000085   909 G$PORTA_5$0$0 == 0x0085
                           000085   910 _PORTA_5	=	0x0085
                           000086   911 G$PORTA_6$0$0 == 0x0086
                           000086   912 _PORTA_6	=	0x0086
                           000087   913 G$PORTA_7$0$0 == 0x0087
                           000087   914 _PORTA_7	=	0x0087
                           000088   915 G$PORTB_0$0$0 == 0x0088
                           000088   916 _PORTB_0	=	0x0088
                           000089   917 G$PORTB_1$0$0 == 0x0089
                           000089   918 _PORTB_1	=	0x0089
                           00008A   919 G$PORTB_2$0$0 == 0x008a
                           00008A   920 _PORTB_2	=	0x008a
                           00008B   921 G$PORTB_3$0$0 == 0x008b
                           00008B   922 _PORTB_3	=	0x008b
                           00008C   923 G$PORTB_4$0$0 == 0x008c
                           00008C   924 _PORTB_4	=	0x008c
                           00008D   925 G$PORTB_5$0$0 == 0x008d
                           00008D   926 _PORTB_5	=	0x008d
                           00008E   927 G$PORTB_6$0$0 == 0x008e
                           00008E   928 _PORTB_6	=	0x008e
                           00008F   929 G$PORTB_7$0$0 == 0x008f
                           00008F   930 _PORTB_7	=	0x008f
                           000090   931 G$PORTC_0$0$0 == 0x0090
                           000090   932 _PORTC_0	=	0x0090
                           000091   933 G$PORTC_1$0$0 == 0x0091
                           000091   934 _PORTC_1	=	0x0091
                           000092   935 G$PORTC_2$0$0 == 0x0092
                           000092   936 _PORTC_2	=	0x0092
                           000093   937 G$PORTC_3$0$0 == 0x0093
                           000093   938 _PORTC_3	=	0x0093
                           000094   939 G$PORTC_4$0$0 == 0x0094
                           000094   940 _PORTC_4	=	0x0094
                           000095   941 G$PORTC_5$0$0 == 0x0095
                           000095   942 _PORTC_5	=	0x0095
                           000096   943 G$PORTC_6$0$0 == 0x0096
                           000096   944 _PORTC_6	=	0x0096
                           000097   945 G$PORTC_7$0$0 == 0x0097
                           000097   946 _PORTC_7	=	0x0097
                                    947 ;--------------------------------------------------------
                                    948 ; overlayable register banks
                                    949 ;--------------------------------------------------------
                                    950 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        951 	.ds 8
                                    952 ;--------------------------------------------------------
                                    953 ; internal ram data
                                    954 ;--------------------------------------------------------
                                    955 	.area DSEG    (DATA)
                           000000   956 Lgoldseq.GOLDSEQUENCE_Obtain$sloc0$1$0==.
      000022                        957 _GOLDSEQUENCE_Obtain_sloc0_1_0:
      000022                        958 	.ds 4
                                    959 ;--------------------------------------------------------
                                    960 ; overlayable items in internal ram 
                                    961 ;--------------------------------------------------------
                                    962 ;--------------------------------------------------------
                                    963 ; indirectly addressable internal ram data
                                    964 ;--------------------------------------------------------
                                    965 	.area ISEG    (DATA)
                                    966 ;--------------------------------------------------------
                                    967 ; absolute internal ram data
                                    968 ;--------------------------------------------------------
                                    969 	.area IABS    (ABS,DATA)
                                    970 	.area IABS    (ABS,DATA)
                                    971 ;--------------------------------------------------------
                                    972 ; bit data
                                    973 ;--------------------------------------------------------
                                    974 	.area BSEG    (BIT)
                                    975 ;--------------------------------------------------------
                                    976 ; paged external ram data
                                    977 ;--------------------------------------------------------
                                    978 	.area PSEG    (PAG,XDATA)
                                    979 ;--------------------------------------------------------
                                    980 ; external ram data
                                    981 ;--------------------------------------------------------
                                    982 	.area XSEG    (XDATA)
                           007020   983 G$ADCCH0VAL0$0$0 == 0x7020
                           007020   984 _ADCCH0VAL0	=	0x7020
                           007021   985 G$ADCCH0VAL1$0$0 == 0x7021
                           007021   986 _ADCCH0VAL1	=	0x7021
                           007020   987 G$ADCCH0VAL$0$0 == 0x7020
                           007020   988 _ADCCH0VAL	=	0x7020
                           007022   989 G$ADCCH1VAL0$0$0 == 0x7022
                           007022   990 _ADCCH1VAL0	=	0x7022
                           007023   991 G$ADCCH1VAL1$0$0 == 0x7023
                           007023   992 _ADCCH1VAL1	=	0x7023
                           007022   993 G$ADCCH1VAL$0$0 == 0x7022
                           007022   994 _ADCCH1VAL	=	0x7022
                           007024   995 G$ADCCH2VAL0$0$0 == 0x7024
                           007024   996 _ADCCH2VAL0	=	0x7024
                           007025   997 G$ADCCH2VAL1$0$0 == 0x7025
                           007025   998 _ADCCH2VAL1	=	0x7025
                           007024   999 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1000 _ADCCH2VAL	=	0x7024
                           007026  1001 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1002 _ADCCH3VAL0	=	0x7026
                           007027  1003 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1004 _ADCCH3VAL1	=	0x7027
                           007026  1005 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1006 _ADCCH3VAL	=	0x7026
                           007028  1007 G$ADCTUNE0$0$0 == 0x7028
                           007028  1008 _ADCTUNE0	=	0x7028
                           007029  1009 G$ADCTUNE1$0$0 == 0x7029
                           007029  1010 _ADCTUNE1	=	0x7029
                           00702A  1011 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1012 _ADCTUNE2	=	0x702a
                           007010  1013 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1014 _DMA0ADDR0	=	0x7010
                           007011  1015 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1016 _DMA0ADDR1	=	0x7011
                           007010  1017 G$DMA0ADDR$0$0 == 0x7010
                           007010  1018 _DMA0ADDR	=	0x7010
                           007014  1019 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1020 _DMA0CONFIG	=	0x7014
                           007012  1021 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1022 _DMA1ADDR0	=	0x7012
                           007013  1023 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1024 _DMA1ADDR1	=	0x7013
                           007012  1025 G$DMA1ADDR$0$0 == 0x7012
                           007012  1026 _DMA1ADDR	=	0x7012
                           007015  1027 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1028 _DMA1CONFIG	=	0x7015
                           007070  1029 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1030 _FRCOSCCONFIG	=	0x7070
                           007071  1031 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1032 _FRCOSCCTRL	=	0x7071
                           007076  1033 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1034 _FRCOSCFREQ0	=	0x7076
                           007077  1035 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1036 _FRCOSCFREQ1	=	0x7077
                           007076  1037 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1038 _FRCOSCFREQ	=	0x7076
                           007072  1039 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1040 _FRCOSCKFILT0	=	0x7072
                           007073  1041 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1042 _FRCOSCKFILT1	=	0x7073
                           007072  1043 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1044 _FRCOSCKFILT	=	0x7072
                           007078  1045 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1046 _FRCOSCPER0	=	0x7078
                           007079  1047 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1048 _FRCOSCPER1	=	0x7079
                           007078  1049 G$FRCOSCPER$0$0 == 0x7078
                           007078  1050 _FRCOSCPER	=	0x7078
                           007074  1051 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1052 _FRCOSCREF0	=	0x7074
                           007075  1053 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1054 _FRCOSCREF1	=	0x7075
                           007074  1055 G$FRCOSCREF$0$0 == 0x7074
                           007074  1056 _FRCOSCREF	=	0x7074
                           007007  1057 G$ANALOGA$0$0 == 0x7007
                           007007  1058 _ANALOGA	=	0x7007
                           00700C  1059 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1060 _GPIOENABLE	=	0x700c
                           007003  1061 G$EXTIRQ$0$0 == 0x7003
                           007003  1062 _EXTIRQ	=	0x7003
                           007000  1063 G$INTCHGA$0$0 == 0x7000
                           007000  1064 _INTCHGA	=	0x7000
                           007001  1065 G$INTCHGB$0$0 == 0x7001
                           007001  1066 _INTCHGB	=	0x7001
                           007002  1067 G$INTCHGC$0$0 == 0x7002
                           007002  1068 _INTCHGC	=	0x7002
                           007008  1069 G$PALTA$0$0 == 0x7008
                           007008  1070 _PALTA	=	0x7008
                           007009  1071 G$PALTB$0$0 == 0x7009
                           007009  1072 _PALTB	=	0x7009
                           00700A  1073 G$PALTC$0$0 == 0x700a
                           00700A  1074 _PALTC	=	0x700a
                           007046  1075 G$PALTRADIO$0$0 == 0x7046
                           007046  1076 _PALTRADIO	=	0x7046
                           007004  1077 G$PINCHGA$0$0 == 0x7004
                           007004  1078 _PINCHGA	=	0x7004
                           007005  1079 G$PINCHGB$0$0 == 0x7005
                           007005  1080 _PINCHGB	=	0x7005
                           007006  1081 G$PINCHGC$0$0 == 0x7006
                           007006  1082 _PINCHGC	=	0x7006
                           00700B  1083 G$PINSEL$0$0 == 0x700b
                           00700B  1084 _PINSEL	=	0x700b
                           007060  1085 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1086 _LPOSCCONFIG	=	0x7060
                           007066  1087 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1088 _LPOSCFREQ0	=	0x7066
                           007067  1089 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1090 _LPOSCFREQ1	=	0x7067
                           007066  1091 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1092 _LPOSCFREQ	=	0x7066
                           007062  1093 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1094 _LPOSCKFILT0	=	0x7062
                           007063  1095 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1096 _LPOSCKFILT1	=	0x7063
                           007062  1097 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1098 _LPOSCKFILT	=	0x7062
                           007068  1099 G$LPOSCPER0$0$0 == 0x7068
                           007068  1100 _LPOSCPER0	=	0x7068
                           007069  1101 G$LPOSCPER1$0$0 == 0x7069
                           007069  1102 _LPOSCPER1	=	0x7069
                           007068  1103 G$LPOSCPER$0$0 == 0x7068
                           007068  1104 _LPOSCPER	=	0x7068
                           007064  1105 G$LPOSCREF0$0$0 == 0x7064
                           007064  1106 _LPOSCREF0	=	0x7064
                           007065  1107 G$LPOSCREF1$0$0 == 0x7065
                           007065  1108 _LPOSCREF1	=	0x7065
                           007064  1109 G$LPOSCREF$0$0 == 0x7064
                           007064  1110 _LPOSCREF	=	0x7064
                           007054  1111 G$LPXOSCGM$0$0 == 0x7054
                           007054  1112 _LPXOSCGM	=	0x7054
                           007F01  1113 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1114 _MISCCTRL	=	0x7f01
                           007053  1115 G$OSCCALIB$0$0 == 0x7053
                           007053  1116 _OSCCALIB	=	0x7053
                           007050  1117 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1118 _OSCFORCERUN	=	0x7050
                           007052  1119 G$OSCREADY$0$0 == 0x7052
                           007052  1120 _OSCREADY	=	0x7052
                           007051  1121 G$OSCRUN$0$0 == 0x7051
                           007051  1122 _OSCRUN	=	0x7051
                           007040  1123 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1124 _RADIOFDATAADDR0	=	0x7040
                           007041  1125 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1126 _RADIOFDATAADDR1	=	0x7041
                           007040  1127 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1128 _RADIOFDATAADDR	=	0x7040
                           007042  1129 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1130 _RADIOFSTATADDR0	=	0x7042
                           007043  1131 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1132 _RADIOFSTATADDR1	=	0x7043
                           007042  1133 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1134 _RADIOFSTATADDR	=	0x7042
                           007044  1135 G$RADIOMUX$0$0 == 0x7044
                           007044  1136 _RADIOMUX	=	0x7044
                           007084  1137 G$SCRATCH0$0$0 == 0x7084
                           007084  1138 _SCRATCH0	=	0x7084
                           007085  1139 G$SCRATCH1$0$0 == 0x7085
                           007085  1140 _SCRATCH1	=	0x7085
                           007086  1141 G$SCRATCH2$0$0 == 0x7086
                           007086  1142 _SCRATCH2	=	0x7086
                           007087  1143 G$SCRATCH3$0$0 == 0x7087
                           007087  1144 _SCRATCH3	=	0x7087
                           007F00  1145 G$SILICONREV$0$0 == 0x7f00
                           007F00  1146 _SILICONREV	=	0x7f00
                           007F19  1147 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1148 _XTALAMPL	=	0x7f19
                           007F18  1149 G$XTALOSC$0$0 == 0x7f18
                           007F18  1150 _XTALOSC	=	0x7f18
                           007F1A  1151 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1152 _XTALREADY	=	0x7f1a
                           00FC06  1153 Fgoldseq$flash_deviceid$0$0 == 0xfc06
                           00FC06  1154 _flash_deviceid	=	0xfc06
                           007091  1155 G$AESCONFIG$0$0 == 0x7091
                           007091  1156 _AESCONFIG	=	0x7091
                           007098  1157 G$AESCURBLOCK$0$0 == 0x7098
                           007098  1158 _AESCURBLOCK	=	0x7098
                           007094  1159 G$AESINADDR0$0$0 == 0x7094
                           007094  1160 _AESINADDR0	=	0x7094
                           007095  1161 G$AESINADDR1$0$0 == 0x7095
                           007095  1162 _AESINADDR1	=	0x7095
                           007094  1163 G$AESINADDR$0$0 == 0x7094
                           007094  1164 _AESINADDR	=	0x7094
                           007092  1165 G$AESKEYADDR0$0$0 == 0x7092
                           007092  1166 _AESKEYADDR0	=	0x7092
                           007093  1167 G$AESKEYADDR1$0$0 == 0x7093
                           007093  1168 _AESKEYADDR1	=	0x7093
                           007092  1169 G$AESKEYADDR$0$0 == 0x7092
                           007092  1170 _AESKEYADDR	=	0x7092
                           007090  1171 G$AESMODE$0$0 == 0x7090
                           007090  1172 _AESMODE	=	0x7090
                           007096  1173 G$AESOUTADDR0$0$0 == 0x7096
                           007096  1174 _AESOUTADDR0	=	0x7096
                           007097  1175 G$AESOUTADDR1$0$0 == 0x7097
                           007097  1176 _AESOUTADDR1	=	0x7097
                           007096  1177 G$AESOUTADDR$0$0 == 0x7096
                           007096  1178 _AESOUTADDR	=	0x7096
                           007081  1179 G$RNGBYTE$0$0 == 0x7081
                           007081  1180 _RNGBYTE	=	0x7081
                           007082  1181 G$RNGCLKSRC0$0$0 == 0x7082
                           007082  1182 _RNGCLKSRC0	=	0x7082
                           007083  1183 G$RNGCLKSRC1$0$0 == 0x7083
                           007083  1184 _RNGCLKSRC1	=	0x7083
                           007080  1185 G$RNGMODE$0$0 == 0x7080
                           007080  1186 _RNGMODE	=	0x7080
                                   1187 ;--------------------------------------------------------
                                   1188 ; absolute external ram data
                                   1189 ;--------------------------------------------------------
                                   1190 	.area XABS    (ABS,XDATA)
                                   1191 ;--------------------------------------------------------
                                   1192 ; external initialized ram data
                                   1193 ;--------------------------------------------------------
                                   1194 	.area XISEG   (XDATA)
                                   1195 	.area HOME    (CODE)
                                   1196 	.area GSINIT0 (CODE)
                                   1197 	.area GSINIT1 (CODE)
                                   1198 	.area GSINIT2 (CODE)
                                   1199 	.area GSINIT3 (CODE)
                                   1200 	.area GSINIT4 (CODE)
                                   1201 	.area GSINIT5 (CODE)
                                   1202 	.area GSINIT  (CODE)
                                   1203 	.area GSFINAL (CODE)
                                   1204 	.area CSEG    (CODE)
                                   1205 ;--------------------------------------------------------
                                   1206 ; global & static initialisations
                                   1207 ;--------------------------------------------------------
                                   1208 	.area HOME    (CODE)
                                   1209 	.area GSINIT  (CODE)
                                   1210 	.area GSFINAL (CODE)
                                   1211 	.area GSINIT  (CODE)
                                   1212 ;--------------------------------------------------------
                                   1213 ; Home
                                   1214 ;--------------------------------------------------------
                                   1215 	.area HOME    (CODE)
                                   1216 	.area HOME    (CODE)
                                   1217 ;--------------------------------------------------------
                                   1218 ; code
                                   1219 ;--------------------------------------------------------
                                   1220 	.area CSEG    (CODE)
                                   1221 ;------------------------------------------------------------
                                   1222 ;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
                                   1223 ;------------------------------------------------------------
                                   1224 ;i                         Allocated to registers 
                                   1225 ;------------------------------------------------------------
                           000000  1226 	G$GOLDSEQUENCE_Init$0$0 ==.
                           000000  1227 	C$goldseq.c$18$0$0 ==.
                                   1228 ;	..\COMMON\goldseq.c:18: /*__xdata*/ void GOLDSEQUENCE_Init(void)
                                   1229 ;	-----------------------------------------
                                   1230 ;	 function GOLDSEQUENCE_Init
                                   1231 ;	-----------------------------------------
      003F7D                       1232 _GOLDSEQUENCE_Init:
                           000007  1233 	ar7 = 0x07
                           000006  1234 	ar6 = 0x06
                           000005  1235 	ar5 = 0x05
                           000004  1236 	ar4 = 0x04
                           000003  1237 	ar3 = 0x03
                           000002  1238 	ar2 = 0x02
                           000001  1239 	ar1 = 0x01
                           000000  1240 	ar0 = 0x00
                           000000  1241 	C$goldseq.c$23$1$122 ==.
                                   1242 ;	..\COMMON\goldseq.c:23: flash_unlock();
      003F7D 12 67 7A         [24] 1243 	lcall	_flash_unlock
                           000003  1244 	C$goldseq.c$27$1$122 ==.
                                   1245 ;	..\COMMON\goldseq.c:27: i = flash_pageerase(MEM_GOLD_SEQUENCES_START/* + 1*/);
      003F80 90 CC 00         [24] 1246 	mov	dptr,#0xcc00
      003F83 12 71 4A         [24] 1247 	lcall	_flash_pageerase
                           000009  1248 	C$goldseq.c$34$1$122 ==.
                                   1249 ;	..\COMMON\goldseq.c:34: flash_write(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100); // 0xF9A4
      003F86 75 5D A4         [24] 1250 	mov	_flash_write_PARM_2,#0xa4
      003F89 75 5E F9         [24] 1251 	mov	(_flash_write_PARM_2 + 1),#0xf9
      003F8C 90 CC 00         [24] 1252 	mov	dptr,#0xcc00
      003F8F 12 67 62         [24] 1253 	lcall	_flash_write
                           000015  1254 	C$goldseq.c$37$1$122 ==.
                                   1255 ;	..\COMMON\goldseq.c:37: flash_write(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000); // 0x15D8
      003F92 75 5D D8         [24] 1256 	mov	_flash_write_PARM_2,#0xd8
      003F95 75 5E 15         [24] 1257 	mov	(_flash_write_PARM_2 + 1),#0x15
      003F98 90 CC 02         [24] 1258 	mov	dptr,#0xcc02
      003F9B 12 67 62         [24] 1259 	lcall	_flash_write
                           000021  1260 	C$goldseq.c$40$1$122 ==.
                                   1261 ;	..\COMMON\goldseq.c:40: flash_write(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000); // 0xF930
      003F9E 75 5D 30         [24] 1262 	mov	_flash_write_PARM_2,#0x30
      003FA1 75 5E F9         [24] 1263 	mov	(_flash_write_PARM_2 + 1),#0xf9
      003FA4 90 CC 04         [24] 1264 	mov	dptr,#0xcc04
      003FA7 12 67 62         [24] 1265 	lcall	_flash_write
                           00002D  1266 	C$goldseq.c$42$1$122 ==.
                                   1267 ;	..\COMMON\goldseq.c:42: flash_write(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110); // 0x5A8E
      003FAA 75 5D 8E         [24] 1268 	mov	_flash_write_PARM_2,#0x8e
      003FAD 75 5E 5A         [24] 1269 	mov	(_flash_write_PARM_2 + 1),#0x5a
      003FB0 90 CC 06         [24] 1270 	mov	dptr,#0xcc06
      003FB3 12 67 62         [24] 1271 	lcall	_flash_write
                           000039  1272 	C$goldseq.c$45$1$122 ==.
                                   1273 ;	..\COMMON\goldseq.c:45: flash_write(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100); // 0x0094
      003FB6 75 5D 94         [24] 1274 	mov	_flash_write_PARM_2,#0x94
      003FB9 75 5E 00         [24] 1275 	mov	(_flash_write_PARM_2 + 1),#0x00
      003FBC 90 CC 08         [24] 1276 	mov	dptr,#0xcc08
      003FBF 12 67 62         [24] 1277 	lcall	_flash_write
                           000045  1278 	C$goldseq.c$47$1$122 ==.
                                   1279 ;	..\COMMON\goldseq.c:47: flash_write(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110); // 0x4F56
      003FC2 75 5D 56         [24] 1280 	mov	_flash_write_PARM_2,#0x56
      003FC5 75 5E 4F         [24] 1281 	mov	(_flash_write_PARM_2 + 1),#0x4f
      003FC8 90 CC 0A         [24] 1282 	mov	dptr,#0xcc0a
      003FCB 12 67 62         [24] 1283 	lcall	_flash_write
                           000051  1284 	C$goldseq.c$50$1$122 ==.
                                   1285 ;	..\COMMON\goldseq.c:50: flash_write(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100); // 0x853C
      003FCE 75 5D 3C         [24] 1286 	mov	_flash_write_PARM_2,#0x3c
      003FD1 75 5E 85         [24] 1287 	mov	(_flash_write_PARM_2 + 1),#0x85
      003FD4 90 CC 0C         [24] 1288 	mov	dptr,#0xcc0c
      003FD7 12 67 62         [24] 1289 	lcall	_flash_write
                           00005D  1290 	C$goldseq.c$52$1$122 ==.
                                   1291 ;	..\COMMON\goldseq.c:52: flash_write(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111); // 0x389F
      003FDA 75 5D 9F         [24] 1292 	mov	_flash_write_PARM_2,#0x9f
      003FDD 75 5E 38         [24] 1293 	mov	(_flash_write_PARM_2 + 1),#0x38
      003FE0 90 CC 0E         [24] 1294 	mov	dptr,#0xcc0e
      003FE3 12 67 62         [24] 1295 	lcall	_flash_write
                           000069  1296 	C$goldseq.c$55$1$122 ==.
                                   1297 ;	..\COMMON\goldseq.c:55: flash_write(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000); // 0x47E8
      003FE6 75 5D E8         [24] 1298 	mov	_flash_write_PARM_2,#0xe8
      003FE9 75 5E 47         [24] 1299 	mov	(_flash_write_PARM_2 + 1),#0x47
      003FEC 90 CC 10         [24] 1300 	mov	dptr,#0xcc10
      003FEF 12 67 62         [24] 1301 	lcall	_flash_write
                           000075  1302 	C$goldseq.c$57$1$122 ==.
                                   1303 ;	..\COMMON\goldseq.c:57: flash_write(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011); // 0x037B
      003FF2 75 5D 7B         [24] 1304 	mov	_flash_write_PARM_2,#0x7b
      003FF5 75 5E 03         [24] 1305 	mov	(_flash_write_PARM_2 + 1),#0x03
      003FF8 90 CC 12         [24] 1306 	mov	dptr,#0xcc12
      003FFB 12 67 62         [24] 1307 	lcall	_flash_write
                           000081  1308 	C$goldseq.c$60$1$122 ==.
                                   1309 ;	..\COMMON\goldseq.c:60: flash_write(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010); // 0x2682
      003FFE 75 5D 82         [24] 1310 	mov	_flash_write_PARM_2,#0x82
      004001 75 5E 26         [24] 1311 	mov	(_flash_write_PARM_2 + 1),#0x26
      004004 90 CC 14         [24] 1312 	mov	dptr,#0xcc14
      004007 12 67 62         [24] 1313 	lcall	_flash_write
                           00008D  1314 	C$goldseq.c$62$1$122 ==.
                                   1315 ;	..\COMMON\goldseq.c:62: flash_write(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001); // 0x1E89
      00400A 75 5D 89         [24] 1316 	mov	_flash_write_PARM_2,#0x89
      00400D 75 5E 1E         [24] 1317 	mov	(_flash_write_PARM_2 + 1),#0x1e
      004010 90 CC 16         [24] 1318 	mov	dptr,#0xcc16
      004013 12 67 62         [24] 1319 	lcall	_flash_write
                           000099  1320 	C$goldseq.c$65$1$122 ==.
                                   1321 ;	..\COMMON\goldseq.c:65: flash_write(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111); //0x1637
      004016 75 5D 37         [24] 1322 	mov	_flash_write_PARM_2,#0x37
      004019 75 5E 16         [24] 1323 	mov	(_flash_write_PARM_2 + 1),#0x16
      00401C 90 CC 18         [24] 1324 	mov	dptr,#0xcc18
      00401F 12 67 62         [24] 1325 	lcall	_flash_write
                           0000A5  1326 	C$goldseq.c$67$1$122 ==.
                                   1327 ;	..\COMMON\goldseq.c:67: flash_write(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000); // 0x1070
      004022 75 5D 70         [24] 1328 	mov	_flash_write_PARM_2,#0x70
      004025 75 5E 10         [24] 1329 	mov	(_flash_write_PARM_2 + 1),#0x10
      004028 90 CC 1A         [24] 1330 	mov	dptr,#0xcc1a
      00402B 12 67 62         [24] 1331 	lcall	_flash_write
                           0000B1  1332 	C$goldseq.c$70$1$122 ==.
                                   1333 ;	..\COMMON\goldseq.c:70: flash_write(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101); // 0x8E6D
      00402E 75 5D 6D         [24] 1334 	mov	_flash_write_PARM_2,#0x6d
      004031 75 5E 8E         [24] 1335 	mov	(_flash_write_PARM_2 + 1),#0x8e
      004034 90 CC 1C         [24] 1336 	mov	dptr,#0xcc1c
      004037 12 67 62         [24] 1337 	lcall	_flash_write
                           0000BD  1338 	C$goldseq.c$72$1$122 ==.
                                   1339 ;	..\COMMON\goldseq.c:72: flash_write(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100); // 0x570C
      00403A 75 5D 0C         [24] 1340 	mov	_flash_write_PARM_2,#0x0c
      00403D 75 5E 57         [24] 1341 	mov	(_flash_write_PARM_2 + 1),#0x57
      004040 90 CC 1E         [24] 1342 	mov	dptr,#0xcc1e
      004043 12 67 62         [24] 1343 	lcall	_flash_write
                           0000C9  1344 	C$goldseq.c$75$1$122 ==.
                                   1345 ;	..\COMMON\goldseq.c:75: flash_write(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000); // 0xC240
      004046 75 5D 40         [24] 1346 	mov	_flash_write_PARM_2,#0x40
      004049 75 5E C2         [24] 1347 	mov	(_flash_write_PARM_2 + 1),#0xc2
      00404C 90 CC 20         [24] 1348 	mov	dptr,#0xcc20
      00404F 12 67 62         [24] 1349 	lcall	_flash_write
                           0000D5  1350 	C$goldseq.c$77$1$122 ==.
                                   1351 ;	..\COMMON\goldseq.c:77: flash_write(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010); // 0x74B2
      004052 75 5D B2         [24] 1352 	mov	_flash_write_PARM_2,#0xb2
      004055 75 5E 74         [24] 1353 	mov	(_flash_write_PARM_2 + 1),#0x74
      004058 90 CC 22         [24] 1354 	mov	dptr,#0xcc22
      00405B 12 67 62         [24] 1355 	lcall	_flash_write
                           0000E1  1356 	C$goldseq.c$80$1$122 ==.
                                   1357 ;	..\COMMON\goldseq.c:80: flash_write(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110); // 0xE456
      00405E 75 5D 56         [24] 1358 	mov	_flash_write_PARM_2,#0x56
      004061 75 5E E4         [24] 1359 	mov	(_flash_write_PARM_2 + 1),#0xe4
      004064 90 CC 24         [24] 1360 	mov	dptr,#0xcc24
      004067 12 67 62         [24] 1361 	lcall	_flash_write
                           0000ED  1362 	C$goldseq.c$82$1$122 ==.
                                   1363 ;	..\COMMON\goldseq.c:82: flash_write(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101); // 0x256D
      00406A 75 5D 6D         [24] 1364 	mov	_flash_write_PARM_2,#0x6d
      00406D 75 5E 25         [24] 1365 	mov	(_flash_write_PARM_2 + 1),#0x25
      004070 90 CC 26         [24] 1366 	mov	dptr,#0xcc26
      004073 12 67 62         [24] 1367 	lcall	_flash_write
                           0000F9  1368 	C$goldseq.c$85$1$122 ==.
                                   1369 ;	..\COMMON\goldseq.c:85: flash_write(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101); // 0x775D
      004076 75 5D 5D         [24] 1370 	mov	_flash_write_PARM_2,#0x5d
      004079 75 5E 77         [24] 1371 	mov	(_flash_write_PARM_2 + 1),#0x77
      00407C 90 CC 28         [24] 1372 	mov	dptr,#0xcc28
      00407F 12 67 62         [24] 1373 	lcall	_flash_write
                           000105  1374 	C$goldseq.c$87$1$122 ==.
                                   1375 ;	..\COMMON\goldseq.c:87: flash_write(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010); // 0x0D82
      004082 75 5D 82         [24] 1376 	mov	_flash_write_PARM_2,#0x82
      004085 75 5E 0D         [24] 1377 	mov	(_flash_write_PARM_2 + 1),#0x0d
      004088 90 CC 2A         [24] 1378 	mov	dptr,#0xcc2a
      00408B 12 67 62         [24] 1379 	lcall	_flash_write
                           000111  1380 	C$goldseq.c$90$1$122 ==.
                                   1381 ;	..\COMMON\goldseq.c:90: flash_write(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000); // 0xBED8
      00408E 75 5D D8         [24] 1382 	mov	_flash_write_PARM_2,#0xd8
      004091 75 5E BE         [24] 1383 	mov	(_flash_write_PARM_2 + 1),#0xbe
      004094 90 CC 2C         [24] 1384 	mov	dptr,#0xcc2c
      004097 12 67 62         [24] 1385 	lcall	_flash_write
                           00011D  1386 	C$goldseq.c$92$1$122 ==.
                                   1387 ;	..\COMMON\goldseq.c:92: flash_write(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101); // 0x59F5
      00409A 75 5D F5         [24] 1388 	mov	_flash_write_PARM_2,#0xf5
      00409D 75 5E 59         [24] 1389 	mov	(_flash_write_PARM_2 + 1),#0x59
      0040A0 90 CC 2E         [24] 1390 	mov	dptr,#0xcc2e
      0040A3 12 67 62         [24] 1391 	lcall	_flash_write
                           000129  1392 	C$goldseq.c$95$1$122 ==.
                                   1393 ;	..\COMMON\goldseq.c:95: flash_write(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010);
      0040A6 75 5D 1A         [24] 1394 	mov	_flash_write_PARM_2,#0x1a
      0040A9 75 5E 5A         [24] 1395 	mov	(_flash_write_PARM_2 + 1),#0x5a
      0040AC 90 CC 30         [24] 1396 	mov	dptr,#0xcc30
      0040AF 12 67 62         [24] 1397 	lcall	_flash_write
                           000135  1398 	C$goldseq.c$97$1$122 ==.
                                   1399 ;	..\COMMON\goldseq.c:97: flash_write(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110);
      0040B2 75 5D CE         [24] 1400 	mov	_flash_write_PARM_2,#0xce
      0040B5 75 5E 33         [24] 1401 	mov	(_flash_write_PARM_2 + 1),#0x33
      0040B8 90 CC 32         [24] 1402 	mov	dptr,#0xcc32
      0040BB 12 67 62         [24] 1403 	lcall	_flash_write
                           000141  1404 	C$goldseq.c$100$1$122 ==.
                                   1405 ;	..\COMMON\goldseq.c:100: flash_write(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011);
      0040BE 75 5D 7B         [24] 1406 	mov	_flash_write_PARM_2,#0x7b
      0040C1 75 5E A8         [24] 1407 	mov	(_flash_write_PARM_2 + 1),#0xa8
      0040C4 90 CC 34         [24] 1408 	mov	dptr,#0xcc34
      0040C7 12 67 62         [24] 1409 	lcall	_flash_write
                           00014D  1410 	C$goldseq.c$102$1$122 ==.
                                   1411 ;	..\COMMON\goldseq.c:102: flash_write(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011);
      0040CA 75 5D D3         [24] 1412 	mov	_flash_write_PARM_2,#0xd3
      0040CD 75 5E 06         [24] 1413 	mov	(_flash_write_PARM_2 + 1),#0x06
      0040D0 90 CC 36         [24] 1414 	mov	dptr,#0xcc36
      0040D3 12 67 62         [24] 1415 	lcall	_flash_write
                           000159  1416 	C$goldseq.c$105$1$122 ==.
                                   1417 ;	..\COMMON\goldseq.c:105: flash_write(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011); // 0x514B
      0040D6 75 5D 4B         [24] 1418 	mov	_flash_write_PARM_2,#0x4b
      0040D9 75 5E 51         [24] 1419 	mov	(_flash_write_PARM_2 + 1),#0x51
      0040DC 90 CC 38         [24] 1420 	mov	dptr,#0xcc38
      0040DF 12 67 62         [24] 1421 	lcall	_flash_write
                           000165  1422 	C$goldseq.c$107$1$122 ==.
                                   1423 ;	..\COMMON\goldseq.c:107: flash_write(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101); // 0xB8BA
      0040E2 75 5D 5D         [24] 1424 	mov	_flash_write_PARM_2,#0x5d
      0040E5 75 5E 5C         [24] 1425 	mov	(_flash_write_PARM_2 + 1),#0x5c
      0040E8 90 CC 3A         [24] 1426 	mov	dptr,#0xcc3a
      0040EB 12 67 62         [24] 1427 	lcall	_flash_write
                           000171  1428 	C$goldseq.c$110$1$122 ==.
                                   1429 ;	..\COMMON\goldseq.c:110: flash_write(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011);
      0040EE 75 5D D3         [24] 1430 	mov	_flash_write_PARM_2,#0xd3
      0040F1 75 5E 2D         [24] 1431 	mov	(_flash_write_PARM_2 + 1),#0x2d
      0040F4 90 CC 3C         [24] 1432 	mov	dptr,#0xcc3c
      0040F7 12 67 62         [24] 1433 	lcall	_flash_write
                           00017D  1434 	C$goldseq.c$112$1$122 ==.
                                   1435 ;	..\COMMON\goldseq.c:112: flash_write(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010);
      0040FA 75 5D 1A         [24] 1436 	mov	_flash_write_PARM_2,#0x1a
      0040FD 75 5E 71         [24] 1437 	mov	(_flash_write_PARM_2 + 1),#0x71
      004100 90 CC 3E         [24] 1438 	mov	dptr,#0xcc3e
      004103 12 67 62         [24] 1439 	lcall	_flash_write
                           000189  1440 	C$goldseq.c$115$1$122 ==.
                                   1441 ;	..\COMMON\goldseq.c:115: flash_write(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111);
      004106 75 5D 9F         [24] 1442 	mov	_flash_write_PARM_2,#0x9f
      004109 75 5E 93         [24] 1443 	mov	(_flash_write_PARM_2 + 1),#0x93
      00410C 90 CC 40         [24] 1444 	mov	dptr,#0xcc40
      00410F 12 67 62         [24] 1445 	lcall	_flash_write
                           000195  1446 	C$goldseq.c$117$1$122 ==.
                                   1447 ;	..\COMMON\goldseq.c:117: flash_write(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001);
      004112 75 5D B9         [24] 1448 	mov	_flash_write_PARM_2,#0xb9
      004115 75 5E 67         [24] 1449 	mov	(_flash_write_PARM_2 + 1),#0x67
      004118 90 CC 42         [24] 1450 	mov	dptr,#0xcc42
      00411B 12 67 62         [24] 1451 	lcall	_flash_write
                           0001A1  1452 	C$goldseq.c$120$1$122 ==.
                                   1453 ;	..\COMMON\goldseq.c:120: flash_write(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001);
      00411E 75 5D B9         [24] 1454 	mov	_flash_write_PARM_2,#0xb9
      004121 75 5E 4C         [24] 1455 	mov	(_flash_write_PARM_2 + 1),#0x4c
      004124 90 CC 44         [24] 1456 	mov	dptr,#0xcc44
      004127 12 67 62         [24] 1457 	lcall	_flash_write
                           0001AD  1458 	C$goldseq.c$122$1$122 ==.
                                   1459 ;	..\COMMON\goldseq.c:122: flash_write(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000);
      00412A 75 5D E8         [24] 1460 	mov	_flash_write_PARM_2,#0xe8
      00412D 75 5E 6C         [24] 1461 	mov	(_flash_write_PARM_2 + 1),#0x6c
      004130 90 CC 46         [24] 1462 	mov	dptr,#0xcc46
      004133 12 67 62         [24] 1463 	lcall	_flash_write
                           0001B9  1464 	C$goldseq.c$125$1$122 ==.
                                   1465 ;	..\COMMON\goldseq.c:125: flash_write(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010); // 0xA32A
      004136 75 5D 2A         [24] 1466 	mov	_flash_write_PARM_2,#0x2a
      004139 75 5E A3         [24] 1467 	mov	(_flash_write_PARM_2 + 1),#0xa3
      00413C 90 CC 48         [24] 1468 	mov	dptr,#0xcc48
      00413F 12 67 62         [24] 1469 	lcall	_flash_write
                           0001C5  1470 	C$goldseq.c$127$1$122 ==.
                                   1471 ;	..\COMMON\goldseq.c:127: flash_write(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000); // 0x6940
      004142 75 5D 40         [24] 1472 	mov	_flash_write_PARM_2,#0x40
      004145 75 5E 69         [24] 1473 	mov	(_flash_write_PARM_2 + 1),#0x69
      004148 90 CC 4A         [24] 1474 	mov	dptr,#0xcc4a
      00414B 12 67 62         [24] 1475 	lcall	_flash_write
                           0001D1  1476 	C$goldseq.c$130$1$122 ==.
                                   1477 ;	..\COMMON\goldseq.c:130: flash_write(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011);
      00414E 75 5D E3         [24] 1478 	mov	_flash_write_PARM_2,#0xe3
      004151 75 5E D4         [24] 1479 	mov	(_flash_write_PARM_2 + 1),#0xd4
      004154 90 CC 4C         [24] 1480 	mov	dptr,#0xcc4c
      004157 12 67 62         [24] 1481 	lcall	_flash_write
                           0001DD  1482 	C$goldseq.c$132$1$122 ==.
                                   1483 ;	..\COMMON\goldseq.c:132: flash_write(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100);
      00415A 75 5D 94         [24] 1484 	mov	_flash_write_PARM_2,#0x94
      00415D 75 5E 2B         [24] 1485 	mov	(_flash_write_PARM_2 + 1),#0x2b
      004160 90 CC 4E         [24] 1486 	mov	dptr,#0xcc4e
      004163 12 67 62         [24] 1487 	lcall	_flash_write
                           0001E9  1488 	C$goldseq.c$135$1$122 ==.
                                   1489 ;	..\COMMON\goldseq.c:135: flash_write(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111);
      004166 75 5D 07         [24] 1490 	mov	_flash_write_PARM_2,#0x07
      004169 75 5E EF         [24] 1491 	mov	(_flash_write_PARM_2 + 1),#0xef
      00416C 90 CC 50         [24] 1492 	mov	dptr,#0xcc50
      00416F 12 67 62         [24] 1493 	lcall	_flash_write
                           0001F5  1494 	C$goldseq.c$137$1$122 ==.
                                   1495 ;	..\COMMON\goldseq.c:137: flash_write(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110); // 0x4AFE
      004172 75 5D FE         [24] 1496 	mov	_flash_write_PARM_2,#0xfe
      004175 75 5E 4A         [24] 1497 	mov	(_flash_write_PARM_2 + 1),#0x4a
      004178 90 CC 52         [24] 1498 	mov	dptr,#0xcc52
      00417B 12 67 62         [24] 1499 	lcall	_flash_write
                           000201  1500 	C$goldseq.c$140$1$122 ==.
                                   1501 ;	..\COMMON\goldseq.c:140: flash_write(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101); // 0xF2F5
      00417E 75 5D F5         [24] 1502 	mov	_flash_write_PARM_2,#0xf5
      004181 75 5E F2         [24] 1503 	mov	(_flash_write_PARM_2 + 1),#0xf2
      004184 90 CC 54         [24] 1504 	mov	dptr,#0xcc54
      004187 12 67 62         [24] 1505 	lcall	_flash_write
                           00020D  1506 	C$goldseq.c$142$1$122 ==.
                                   1507 ;	..\COMMON\goldseq.c:142: flash_write(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011); // 0x7A4B
      00418A 75 5D 4B         [24] 1508 	mov	_flash_write_PARM_2,#0x4b
      00418D 75 5E 7A         [24] 1509 	mov	(_flash_write_PARM_2 + 1),#0x7a
      004190 90 CC 56         [24] 1510 	mov	dptr,#0xcc56
      004193 12 67 62         [24] 1511 	lcall	_flash_write
                           000219  1512 	C$goldseq.c$144$1$122 ==.
                                   1513 ;	..\COMMON\goldseq.c:144: flash_write(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100); // 0x7C0C
      004196 75 5D 0C         [24] 1514 	mov	_flash_write_PARM_2,#0x0c
      004199 75 5E 7C         [24] 1515 	mov	(_flash_write_PARM_2 + 1),#0x7c
      00419C 90 CC 58         [24] 1516 	mov	dptr,#0xcc58
      00419F 12 67 62         [24] 1517 	lcall	_flash_write
                           000225  1518 	C$goldseq.c$146$1$122 ==.
                                   1519 ;	..\COMMON\goldseq.c:146: flash_write(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001); //0x6211
      0041A2 75 5D 11         [24] 1520 	mov	_flash_write_PARM_2,#0x11
      0041A5 75 5E 62         [24] 1521 	mov	(_flash_write_PARM_2 + 1),#0x62
      0041A8 90 CC 5A         [24] 1522 	mov	dptr,#0xcc5a
      0041AB 12 67 62         [24] 1523 	lcall	_flash_write
                           000231  1524 	C$goldseq.c$149$1$122 ==.
                                   1525 ;	..\COMMON\goldseq.c:149: flash_write(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000); // 0x3B70
      0041AE 75 5D 70         [24] 1526 	mov	_flash_write_PARM_2,#0x70
      0041B1 75 5E 3B         [24] 1527 	mov	(_flash_write_PARM_2 + 1),#0x3b
      0041B4 90 CC 5C         [24] 1528 	mov	dptr,#0xcc5c
      0041B7 12 67 62         [24] 1529 	lcall	_flash_write
                           00023D  1530 	C$goldseq.c$151$1$122 ==.
                                   1531 ;	..\COMMON\goldseq.c:151: flash_write(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100); // 0x2E3C
      0041BA 75 5D 3C         [24] 1532 	mov	_flash_write_PARM_2,#0x3c
      0041BD 75 5E 2E         [24] 1533 	mov	(_flash_write_PARM_2 + 1),#0x2e
      0041C0 90 CC 5E         [24] 1534 	mov	dptr,#0xcc5e
      0041C3 12 67 62         [24] 1535 	lcall	_flash_write
                           000249  1536 	C$goldseq.c$154$1$122 ==.
                                   1537 ;	..\COMMON\goldseq.c:154: flash_write(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110); // 0x98CE
      0041C6 75 5D CE         [24] 1538 	mov	_flash_write_PARM_2,#0xce
      0041C9 75 5E 98         [24] 1539 	mov	(_flash_write_PARM_2 + 1),#0x98
      0041CC 90 CC 60         [24] 1540 	mov	dptr,#0xcc60
      0041CF 12 67 62         [24] 1541 	lcall	_flash_write
                           000255  1542 	C$goldseq.c$156$1$122 ==.
                                   1543 ;	..\COMMON\goldseq.c:156: flash_write(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010);
      0041D2 75 5D 2A         [24] 1544 	mov	_flash_write_PARM_2,#0x2a
      0041D5 75 5E 08         [24] 1545 	mov	(_flash_write_PARM_2 + 1),#0x08
      0041D8 90 CC 62         [24] 1546 	mov	dptr,#0xcc62
      0041DB 12 67 62         [24] 1547 	lcall	_flash_write
                           000261  1548 	C$goldseq.c$159$1$122 ==.
                                   1549 ;	..\COMMON\goldseq.c:159: flash_write(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001); // 0xC911
      0041DE 75 5D 11         [24] 1550 	mov	_flash_write_PARM_2,#0x11
      0041E1 75 5E C9         [24] 1551 	mov	(_flash_write_PARM_2 + 1),#0xc9
      0041E4 90 CC 64         [24] 1552 	mov	dptr,#0xcc64
      0041E7 12 67 62         [24] 1553 	lcall	_flash_write
                           00026D  1554 	C$goldseq.c$161$1$122 ==.
                                   1555 ;	..\COMMON\goldseq.c:161: flash_write(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001); // 0x1B21
      0041EA 75 5D 21         [24] 1556 	mov	_flash_write_PARM_2,#0x21
      0041ED 75 5E 1B         [24] 1557 	mov	(_flash_write_PARM_2 + 1),#0x1b
      0041F0 90 CC 66         [24] 1558 	mov	dptr,#0xcc66
      0041F3 12 67 62         [24] 1559 	lcall	_flash_write
                           000279  1560 	C$goldseq.c$164$1$122 ==.
                                   1561 ;	..\COMMON\goldseq.c:164: flash_write(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110); // 0x61FE
      0041F6 75 5D FE         [24] 1562 	mov	_flash_write_PARM_2,#0xfe
      0041F9 75 5E 61         [24] 1563 	mov	(_flash_write_PARM_2 + 1),#0x61
      0041FC 90 CC 68         [24] 1564 	mov	dptr,#0xcc68
      0041FF 12 67 62         [24] 1565 	lcall	_flash_write
                           000285  1566 	C$goldseq.c$166$1$122 ==.
                                   1567 ;	..\COMMON\goldseq.c:166: flash_write(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100); // 0x52A4
      004202 75 5D A4         [24] 1568 	mov	_flash_write_PARM_2,#0xa4
      004205 75 5E 52         [24] 1569 	mov	(_flash_write_PARM_2 + 1),#0x52
      004208 90 CC 6A         [24] 1570 	mov	dptr,#0xcc6a
      00420B 12 67 62         [24] 1571 	lcall	_flash_write
                           000291  1572 	C$goldseq.c$169$1$122 ==.
                                   1573 ;	..\COMMON\goldseq.c:169: flash_write(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001);
      00420E 75 5D 89         [24] 1574 	mov	_flash_write_PARM_2,#0x89
      004211 75 5E B5         [24] 1575 	mov	(_flash_write_PARM_2 + 1),#0xb5
      004214 90 CC 6C         [24] 1576 	mov	dptr,#0xcc6c
      004217 12 67 62         [24] 1577 	lcall	_flash_write
                           00029D  1578 	C$goldseq.c$171$1$122 ==.
                                   1579 ;	..\COMMON\goldseq.c:171: flash_write(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110);
      00421A 75 5D 66         [24] 1580 	mov	_flash_write_PARM_2,#0x66
      00421D 75 5E 36         [24] 1581 	mov	(_flash_write_PARM_2 + 1),#0x36
      004220 90 CC 6E         [24] 1582 	mov	dptr,#0xcc6e
      004223 12 67 62         [24] 1583 	lcall	_flash_write
                           0002A9  1584 	C$goldseq.c$174$1$122 ==.
                                   1585 ;	..\COMMON\goldseq.c:174: flash_write(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010);
      004226 75 5D B2         [24] 1586 	mov	_flash_write_PARM_2,#0xb2
      004229 75 5E DF         [24] 1587 	mov	(_flash_write_PARM_2 + 1),#0xdf
      00422C 90 CC 70         [24] 1588 	mov	dptr,#0xcc70
      00422F 12 67 62         [24] 1589 	lcall	_flash_write
                           0002B5  1590 	C$goldseq.c$176$1$122 ==.
                                   1591 ;	..\COMMON\goldseq.c:176: flash_write(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111);
      004232 75 5D 07         [24] 1592 	mov	_flash_write_PARM_2,#0x07
      004235 75 5E 44         [24] 1593 	mov	(_flash_write_PARM_2 + 1),#0x44
      004238 90 CC 72         [24] 1594 	mov	dptr,#0xcc72
      00423B 12 67 62         [24] 1595 	lcall	_flash_write
                           0002C1  1596 	C$goldseq.c$179$1$122 ==.
                                   1597 ;	..\COMMON\goldseq.c:179: flash_write(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111);
      00423E 75 5D AF         [24] 1598 	mov	_flash_write_PARM_2,#0xaf
      004241 75 5E 6A         [24] 1599 	mov	(_flash_write_PARM_2 + 1),#0x6a
      004244 90 CC 74         [24] 1600 	mov	dptr,#0xcc74
      004247 12 67 62         [24] 1601 	lcall	_flash_write
                           0002CD  1602 	C$goldseq.c$181$1$122 ==.
                                   1603 ;	..\COMMON\goldseq.c:181: flash_write(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111);
      00424A 75 5D 37         [24] 1604 	mov	_flash_write_PARM_2,#0x37
      00424D 75 5E 3D         [24] 1605 	mov	(_flash_write_PARM_2 + 1),#0x3d
      004250 90 CC 76         [24] 1606 	mov	dptr,#0xcc76
      004253 12 67 62         [24] 1607 	lcall	_flash_write
                           0002D9  1608 	C$goldseq.c$184$1$122 ==.
                                   1609 ;	..\COMMON\goldseq.c:184: flash_write(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001);
      004256 75 5D 21         [24] 1610 	mov	_flash_write_PARM_2,#0x21
      004259 75 5E 30         [24] 1611 	mov	(_flash_write_PARM_2 + 1),#0x30
      00425C 90 CC 78         [24] 1612 	mov	dptr,#0xcc78
      00425F 12 67 62         [24] 1613 	lcall	_flash_write
                           0002E5  1614 	C$goldseq.c$186$1$122 ==.
                                   1615 ;	..\COMMON\goldseq.c:186: flash_write(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111);
      004262 75 5D AF         [24] 1616 	mov	_flash_write_PARM_2,#0xaf
      004265 75 5E 41         [24] 1617 	mov	(_flash_write_PARM_2 + 1),#0x41
      004268 90 CC 7A         [24] 1618 	mov	dptr,#0xcc7a
      00426B 12 67 62         [24] 1619 	lcall	_flash_write
                           0002F1  1620 	C$goldseq.c$189$1$122 ==.
                                   1621 ;	..\COMMON\goldseq.c:189: flash_write(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110);
      00426E 75 5D 66         [24] 1622 	mov	_flash_write_PARM_2,#0x66
      004271 75 5E 1D         [24] 1623 	mov	(_flash_write_PARM_2 + 1),#0x1d
      004274 90 CC 7C         [24] 1624 	mov	dptr,#0xcc7c
      004277 12 67 62         [24] 1625 	lcall	_flash_write
                           0002FD  1626 	C$goldseq.c$191$1$122 ==.
                                   1627 ;	..\COMMON\goldseq.c:191: flash_write(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011);
      00427A 75 5D E3         [24] 1628 	mov	_flash_write_PARM_2,#0xe3
      00427D 75 5E 7F         [24] 1629 	mov	(_flash_write_PARM_2 + 1),#0x7f
      004280 90 CC 7E         [24] 1630 	mov	dptr,#0xcc7e
      004283 12 67 62         [24] 1631 	lcall	_flash_write
                           000309  1632 	C$goldseq.c$193$1$122 ==.
                                   1633 ;	..\COMMON\goldseq.c:193: flash_write(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101);
      004286 75 5D C5         [24] 1634 	mov	_flash_write_PARM_2,#0xc5
      004289 75 5E 0B         [24] 1635 	mov	(_flash_write_PARM_2 + 1),#0x0b
      00428C 90 CC 80         [24] 1636 	mov	dptr,#0xcc80
      00428F 12 67 62         [24] 1637 	lcall	_flash_write
                           000315  1638 	C$goldseq.c$195$1$122 ==.
                                   1639 ;	..\COMMON\goldseq.c:195: flash_write(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101);
      004292 75 5D C5         [24] 1640 	mov	_flash_write_PARM_2,#0xc5
      004295 75 5E 20         [24] 1641 	mov	(_flash_write_PARM_2 + 1),#0x20
      004298 90 CC 82         [24] 1642 	mov	dptr,#0xcc82
      00429B 12 67 62         [24] 1643 	lcall	_flash_write
                           000321  1644 	C$goldseq.c$197$1$122 ==.
                                   1645 ;	..\COMMON\goldseq.c:197: flash_lock();
      00429E 12 70 EE         [24] 1646 	lcall	_flash_lock
                           000324  1647 	C$goldseq.c$200$1$122 ==.
                           000324  1648 	XG$GOLDSEQUENCE_Init$0$0 ==.
      0042A1 22               [24] 1649 	ret
                                   1650 ;------------------------------------------------------------
                                   1651 ;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
                                   1652 ;------------------------------------------------------------
                                   1653 ;sloc0                     Allocated with name '_GOLDSEQUENCE_Obtain_sloc0_1_0'
                                   1654 ;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_124'
                                   1655 ;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_124'
                                   1656 ;------------------------------------------------------------
                           000325  1657 	G$GOLDSEQUENCE_Obtain$0$0 ==.
                           000325  1658 	C$goldseq.c$209$1$122 ==.
                                   1659 ;	..\COMMON\goldseq.c:209: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
                                   1660 ;	-----------------------------------------
                                   1661 ;	 function GOLDSEQUENCE_Obtain
                                   1662 ;	-----------------------------------------
      0042A2                       1663 _GOLDSEQUENCE_Obtain:
                           000325  1664 	C$goldseq.c$214$1$124 ==.
                                   1665 ;	..\COMMON\goldseq.c:214: Rnd = RNGBYTE;
      0042A2 90 70 81         [24] 1666 	mov	dptr,#_RNGBYTE
      0042A5 E0               [24] 1667 	movx	a,@dptr
      0042A6 FF               [12] 1668 	mov	r7,a
      0042A7 7E 00            [12] 1669 	mov	r6,#0x00
                           00032C  1670 	C$goldseq.c$215$1$124 ==.
                                   1671 ;	..\COMMON\goldseq.c:215: flash_unlock();
      0042A9 C0 07            [24] 1672 	push	ar7
      0042AB C0 06            [24] 1673 	push	ar6
      0042AD 12 67 7A         [24] 1674 	lcall	_flash_unlock
                           000333  1675 	C$goldseq.c$217$1$124 ==.
                                   1676 ;	..\COMMON\goldseq.c:217: delay_ms(50);
      0042B0 90 00 32         [24] 1677 	mov	dptr,#0x0032
      0042B3 12 44 91         [24] 1678 	lcall	_delay_ms
      0042B6 D0 06            [24] 1679 	pop	ar6
      0042B8 D0 07            [24] 1680 	pop	ar7
                           00033D  1681 	C$goldseq.c$219$1$124 ==.
                                   1682 ;	..\COMMON\goldseq.c:219: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
      0042BA 75 64 21         [24] 1683 	mov	__moduint_PARM_2,#0x21
      0042BD 75 65 00         [24] 1684 	mov	(__moduint_PARM_2 + 1),#0x00
      0042C0 8F 82            [24] 1685 	mov	dpl,r7
      0042C2 8E 83            [24] 1686 	mov	dph,r6
      0042C4 12 6A 58         [24] 1687 	lcall	__moduint
      0042C7 AE 82            [24] 1688 	mov	r6,dpl
      0042C9 E5 83            [12] 1689 	mov	a,dph
      0042CB CE               [12] 1690 	xch	a,r6
      0042CC 25 E0            [12] 1691 	add	a,acc
      0042CE CE               [12] 1692 	xch	a,r6
      0042CF 33               [12] 1693 	rlc	a
      0042D0 CE               [12] 1694 	xch	a,r6
      0042D1 25 E0            [12] 1695 	add	a,acc
      0042D3 CE               [12] 1696 	xch	a,r6
      0042D4 33               [12] 1697 	rlc	a
      0042D5 FF               [12] 1698 	mov	r7,a
                           000359  1699 	C$goldseq.c$223$1$124 ==.
                                   1700 ;	..\COMMON\goldseq.c:223: RetVal = ((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd+2))<<16;
      0042D6 74 02            [12] 1701 	mov	a,#0x02
      0042D8 2E               [12] 1702 	add	a,r6
      0042D9 F5 82            [12] 1703 	mov	dpl,a
      0042DB 74 CC            [12] 1704 	mov	a,#0xcc
      0042DD 3F               [12] 1705 	addc	a,r7
      0042DE F5 83            [12] 1706 	mov	dph,a
      0042E0 C0 07            [24] 1707 	push	ar7
      0042E2 C0 06            [24] 1708 	push	ar6
      0042E4 12 6B DC         [24] 1709 	lcall	_flash_read
      0042E7 AC 82            [24] 1710 	mov	r4,dpl
      0042E9 AD 83            [24] 1711 	mov	r5,dph
      0042EB D0 06            [24] 1712 	pop	ar6
      0042ED D0 07            [24] 1713 	pop	ar7
      0042EF 7B 00            [12] 1714 	mov	r3,#0x00
      0042F1 8D 25            [24] 1715 	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 3),r5
      0042F3 8C 24            [24] 1716 	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 2),r4
                                   1717 ;	1-genFromRTrack replaced	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1),#0x00
      0042F5 8B 23            [24] 1718 	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1),r3
                                   1719 ;	1-genFromRTrack replaced	mov	_GOLDSEQUENCE_Obtain_sloc0_1_0,#0x00
      0042F7 8B 22            [24] 1720 	mov	_GOLDSEQUENCE_Obtain_sloc0_1_0,r3
                           00037C  1721 	C$goldseq.c$225$1$124 ==.
                                   1722 ;	..\COMMON\goldseq.c:225: RetVal |= (((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd)) & 0x0000FFFF)/*<<1*/;
      0042F9 8E 82            [24] 1723 	mov	dpl,r6
      0042FB 74 CC            [12] 1724 	mov	a,#0xcc
      0042FD 2F               [12] 1725 	add	a,r7
      0042FE F5 83            [12] 1726 	mov	dph,a
      004300 C0 07            [24] 1727 	push	ar7
      004302 C0 06            [24] 1728 	push	ar6
      004304 12 6B DC         [24] 1729 	lcall	_flash_read
      004307 A8 82            [24] 1730 	mov	r0,dpl
      004309 A9 83            [24] 1731 	mov	r1,dph
      00430B E4               [12] 1732 	clr	a
      00430C FC               [12] 1733 	mov	r4,a
      00430D FD               [12] 1734 	mov	r5,a
      00430E E5 22            [12] 1735 	mov	a,_GOLDSEQUENCE_Obtain_sloc0_1_0
      004310 42 00            [12] 1736 	orl	ar0,a
      004312 E5 23            [12] 1737 	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1)
      004314 42 01            [12] 1738 	orl	ar1,a
      004316 E5 24            [12] 1739 	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 2)
      004318 42 04            [12] 1740 	orl	ar4,a
      00431A E5 25            [12] 1741 	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 3)
      00431C 42 05            [12] 1742 	orl	ar5,a
                           0003A1  1743 	C$goldseq.c$229$1$124 ==.
                                   1744 ;	..\COMMON\goldseq.c:229: dbglink_writestr("gold rnd: ");
      00431E 90 76 71         [24] 1745 	mov	dptr,#___str_0
      004321 75 F0 80         [24] 1746 	mov	b,#0x80
      004324 C0 05            [24] 1747 	push	ar5
      004326 C0 04            [24] 1748 	push	ar4
      004328 C0 01            [24] 1749 	push	ar1
      00432A C0 00            [24] 1750 	push	ar0
      00432C 12 69 E1         [24] 1751 	lcall	_dbglink_writestr
      00432F D0 00            [24] 1752 	pop	ar0
      004331 D0 01            [24] 1753 	pop	ar1
      004333 D0 04            [24] 1754 	pop	ar4
      004335 D0 05            [24] 1755 	pop	ar5
      004337 D0 06            [24] 1756 	pop	ar6
      004339 D0 07            [24] 1757 	pop	ar7
                           0003BE  1758 	C$goldseq.c$230$1$124 ==.
                                   1759 ;	..\COMMON\goldseq.c:230: dbglink_writehex16(Rnd, 4, WRNUM_PADZERO);
      00433B C0 05            [24] 1760 	push	ar5
      00433D C0 04            [24] 1761 	push	ar4
      00433F C0 01            [24] 1762 	push	ar1
      004341 C0 00            [24] 1763 	push	ar0
      004343 74 08            [12] 1764 	mov	a,#0x08
      004345 C0 E0            [24] 1765 	push	acc
      004347 03               [12] 1766 	rr	a
      004348 C0 E0            [24] 1767 	push	acc
      00434A 8E 82            [24] 1768 	mov	dpl,r6
      00434C 8F 83            [24] 1769 	mov	dph,r7
      00434E 12 6E 24         [24] 1770 	lcall	_dbglink_writehex16
      004351 15 81            [12] 1771 	dec	sp
      004353 15 81            [12] 1772 	dec	sp
                           0003D8  1773 	C$goldseq.c$231$1$124 ==.
                                   1774 ;	..\COMMON\goldseq.c:231: dbglink_writestr(" ");
      004355 90 76 7C         [24] 1775 	mov	dptr,#___str_1
      004358 75 F0 80         [24] 1776 	mov	b,#0x80
      00435B 12 69 E1         [24] 1777 	lcall	_dbglink_writestr
      00435E D0 00            [24] 1778 	pop	ar0
      004360 D0 01            [24] 1779 	pop	ar1
      004362 D0 04            [24] 1780 	pop	ar4
      004364 D0 05            [24] 1781 	pop	ar5
                           0003E9  1782 	C$goldseq.c$232$1$124 ==.
                                   1783 ;	..\COMMON\goldseq.c:232: dbglink_writehex32(RetVal, 8, WRNUM_PADZERO);
      004366 C0 05            [24] 1784 	push	ar5
      004368 C0 04            [24] 1785 	push	ar4
      00436A C0 01            [24] 1786 	push	ar1
      00436C C0 00            [24] 1787 	push	ar0
      00436E 74 08            [12] 1788 	mov	a,#0x08
      004370 C0 E0            [24] 1789 	push	acc
      004372 C0 E0            [24] 1790 	push	acc
      004374 88 82            [24] 1791 	mov	dpl,r0
      004376 89 83            [24] 1792 	mov	dph,r1
      004378 8C F0            [24] 1793 	mov	b,r4
      00437A ED               [12] 1794 	mov	a,r5
      00437B 12 6C 06         [24] 1795 	lcall	_dbglink_writehex32
      00437E 15 81            [12] 1796 	dec	sp
      004380 15 81            [12] 1797 	dec	sp
                           000405  1798 	C$goldseq.c$233$1$124 ==.
                                   1799 ;	..\COMMON\goldseq.c:233: dbglink_tx('\n');
      004382 75 82 0A         [24] 1800 	mov	dpl,#0x0a
      004385 12 58 D1         [24] 1801 	lcall	_dbglink_tx
                           00040B  1802 	C$goldseq.c$236$1$124 ==.
                                   1803 ;	..\COMMON\goldseq.c:236: flash_lock();
      004388 12 70 EE         [24] 1804 	lcall	_flash_lock
      00438B D0 00            [24] 1805 	pop	ar0
      00438D D0 01            [24] 1806 	pop	ar1
      00438F D0 04            [24] 1807 	pop	ar4
      004391 D0 05            [24] 1808 	pop	ar5
                           000416  1809 	C$goldseq.c$237$1$124 ==.
                                   1810 ;	..\COMMON\goldseq.c:237: return(RetVal);
      004393 88 82            [24] 1811 	mov	dpl,r0
      004395 89 83            [24] 1812 	mov	dph,r1
      004397 8C F0            [24] 1813 	mov	b,r4
      004399 ED               [12] 1814 	mov	a,r5
                           00041D  1815 	C$goldseq.c$238$1$124 ==.
                           00041D  1816 	XG$GOLDSEQUENCE_Obtain$0$0 ==.
      00439A 22               [24] 1817 	ret
                                   1818 	.area CSEG    (CODE)
                                   1819 	.area CONST   (CODE)
                           000000  1820 Fgoldseq$__str_0$0$0 == .
      007671                       1821 ___str_0:
      007671 67 6F 6C 64 20 72 6E  1822 	.ascii "gold rnd: "
             64 3A 20
      00767B 00                    1823 	.db 0x00
                           00000B  1824 Fgoldseq$__str_1$0$0 == .
      00767C                       1825 ___str_1:
      00767C 20                    1826 	.ascii " "
      00767D 00                    1827 	.db 0x00
                                   1828 	.area XINIT   (CODE)
                                   1829 	.area CABS    (ABS,CODE)
