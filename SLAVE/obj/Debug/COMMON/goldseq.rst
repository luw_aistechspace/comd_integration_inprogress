                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module goldseq
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _GOLDSEQUENCE_Obtain
                                     12 	.globl _GOLDSEQUENCE_Init
                                     13 	.globl _delay_ms
                                     14 	.globl _flash_read
                                     15 	.globl _flash_write
                                     16 	.globl _flash_pageerase
                                     17 	.globl _flash_lock
                                     18 	.globl _flash_unlock
                                     19 	.globl _PORTC_7
                                     20 	.globl _PORTC_6
                                     21 	.globl _PORTC_5
                                     22 	.globl _PORTC_4
                                     23 	.globl _PORTC_3
                                     24 	.globl _PORTC_2
                                     25 	.globl _PORTC_1
                                     26 	.globl _PORTC_0
                                     27 	.globl _PORTB_7
                                     28 	.globl _PORTB_6
                                     29 	.globl _PORTB_5
                                     30 	.globl _PORTB_4
                                     31 	.globl _PORTB_3
                                     32 	.globl _PORTB_2
                                     33 	.globl _PORTB_1
                                     34 	.globl _PORTB_0
                                     35 	.globl _PORTA_7
                                     36 	.globl _PORTA_6
                                     37 	.globl _PORTA_5
                                     38 	.globl _PORTA_4
                                     39 	.globl _PORTA_3
                                     40 	.globl _PORTA_2
                                     41 	.globl _PORTA_1
                                     42 	.globl _PORTA_0
                                     43 	.globl _PINC_7
                                     44 	.globl _PINC_6
                                     45 	.globl _PINC_5
                                     46 	.globl _PINC_4
                                     47 	.globl _PINC_3
                                     48 	.globl _PINC_2
                                     49 	.globl _PINC_1
                                     50 	.globl _PINC_0
                                     51 	.globl _PINB_7
                                     52 	.globl _PINB_6
                                     53 	.globl _PINB_5
                                     54 	.globl _PINB_4
                                     55 	.globl _PINB_3
                                     56 	.globl _PINB_2
                                     57 	.globl _PINB_1
                                     58 	.globl _PINB_0
                                     59 	.globl _PINA_7
                                     60 	.globl _PINA_6
                                     61 	.globl _PINA_5
                                     62 	.globl _PINA_4
                                     63 	.globl _PINA_3
                                     64 	.globl _PINA_2
                                     65 	.globl _PINA_1
                                     66 	.globl _PINA_0
                                     67 	.globl _CY
                                     68 	.globl _AC
                                     69 	.globl _F0
                                     70 	.globl _RS1
                                     71 	.globl _RS0
                                     72 	.globl _OV
                                     73 	.globl _F1
                                     74 	.globl _P
                                     75 	.globl _IP_7
                                     76 	.globl _IP_6
                                     77 	.globl _IP_5
                                     78 	.globl _IP_4
                                     79 	.globl _IP_3
                                     80 	.globl _IP_2
                                     81 	.globl _IP_1
                                     82 	.globl _IP_0
                                     83 	.globl _EA
                                     84 	.globl _IE_7
                                     85 	.globl _IE_6
                                     86 	.globl _IE_5
                                     87 	.globl _IE_4
                                     88 	.globl _IE_3
                                     89 	.globl _IE_2
                                     90 	.globl _IE_1
                                     91 	.globl _IE_0
                                     92 	.globl _EIP_7
                                     93 	.globl _EIP_6
                                     94 	.globl _EIP_5
                                     95 	.globl _EIP_4
                                     96 	.globl _EIP_3
                                     97 	.globl _EIP_2
                                     98 	.globl _EIP_1
                                     99 	.globl _EIP_0
                                    100 	.globl _EIE_7
                                    101 	.globl _EIE_6
                                    102 	.globl _EIE_5
                                    103 	.globl _EIE_4
                                    104 	.globl _EIE_3
                                    105 	.globl _EIE_2
                                    106 	.globl _EIE_1
                                    107 	.globl _EIE_0
                                    108 	.globl _E2IP_7
                                    109 	.globl _E2IP_6
                                    110 	.globl _E2IP_5
                                    111 	.globl _E2IP_4
                                    112 	.globl _E2IP_3
                                    113 	.globl _E2IP_2
                                    114 	.globl _E2IP_1
                                    115 	.globl _E2IP_0
                                    116 	.globl _E2IE_7
                                    117 	.globl _E2IE_6
                                    118 	.globl _E2IE_5
                                    119 	.globl _E2IE_4
                                    120 	.globl _E2IE_3
                                    121 	.globl _E2IE_2
                                    122 	.globl _E2IE_1
                                    123 	.globl _E2IE_0
                                    124 	.globl _B_7
                                    125 	.globl _B_6
                                    126 	.globl _B_5
                                    127 	.globl _B_4
                                    128 	.globl _B_3
                                    129 	.globl _B_2
                                    130 	.globl _B_1
                                    131 	.globl _B_0
                                    132 	.globl _ACC_7
                                    133 	.globl _ACC_6
                                    134 	.globl _ACC_5
                                    135 	.globl _ACC_4
                                    136 	.globl _ACC_3
                                    137 	.globl _ACC_2
                                    138 	.globl _ACC_1
                                    139 	.globl _ACC_0
                                    140 	.globl _WTSTAT
                                    141 	.globl _WTIRQEN
                                    142 	.globl _WTEVTD
                                    143 	.globl _WTEVTD1
                                    144 	.globl _WTEVTD0
                                    145 	.globl _WTEVTC
                                    146 	.globl _WTEVTC1
                                    147 	.globl _WTEVTC0
                                    148 	.globl _WTEVTB
                                    149 	.globl _WTEVTB1
                                    150 	.globl _WTEVTB0
                                    151 	.globl _WTEVTA
                                    152 	.globl _WTEVTA1
                                    153 	.globl _WTEVTA0
                                    154 	.globl _WTCNTR1
                                    155 	.globl _WTCNTB
                                    156 	.globl _WTCNTB1
                                    157 	.globl _WTCNTB0
                                    158 	.globl _WTCNTA
                                    159 	.globl _WTCNTA1
                                    160 	.globl _WTCNTA0
                                    161 	.globl _WTCFGB
                                    162 	.globl _WTCFGA
                                    163 	.globl _WDTRESET
                                    164 	.globl _WDTCFG
                                    165 	.globl _U1STATUS
                                    166 	.globl _U1SHREG
                                    167 	.globl _U1MODE
                                    168 	.globl _U1CTRL
                                    169 	.globl _U0STATUS
                                    170 	.globl _U0SHREG
                                    171 	.globl _U0MODE
                                    172 	.globl _U0CTRL
                                    173 	.globl _T2STATUS
                                    174 	.globl _T2PERIOD
                                    175 	.globl _T2PERIOD1
                                    176 	.globl _T2PERIOD0
                                    177 	.globl _T2MODE
                                    178 	.globl _T2CNT
                                    179 	.globl _T2CNT1
                                    180 	.globl _T2CNT0
                                    181 	.globl _T2CLKSRC
                                    182 	.globl _T1STATUS
                                    183 	.globl _T1PERIOD
                                    184 	.globl _T1PERIOD1
                                    185 	.globl _T1PERIOD0
                                    186 	.globl _T1MODE
                                    187 	.globl _T1CNT
                                    188 	.globl _T1CNT1
                                    189 	.globl _T1CNT0
                                    190 	.globl _T1CLKSRC
                                    191 	.globl _T0STATUS
                                    192 	.globl _T0PERIOD
                                    193 	.globl _T0PERIOD1
                                    194 	.globl _T0PERIOD0
                                    195 	.globl _T0MODE
                                    196 	.globl _T0CNT
                                    197 	.globl _T0CNT1
                                    198 	.globl _T0CNT0
                                    199 	.globl _T0CLKSRC
                                    200 	.globl _SPSTATUS
                                    201 	.globl _SPSHREG
                                    202 	.globl _SPMODE
                                    203 	.globl _SPCLKSRC
                                    204 	.globl _RADIOSTAT
                                    205 	.globl _RADIOSTAT1
                                    206 	.globl _RADIOSTAT0
                                    207 	.globl _RADIODATA
                                    208 	.globl _RADIODATA3
                                    209 	.globl _RADIODATA2
                                    210 	.globl _RADIODATA1
                                    211 	.globl _RADIODATA0
                                    212 	.globl _RADIOADDR
                                    213 	.globl _RADIOADDR1
                                    214 	.globl _RADIOADDR0
                                    215 	.globl _RADIOACC
                                    216 	.globl _OC1STATUS
                                    217 	.globl _OC1PIN
                                    218 	.globl _OC1MODE
                                    219 	.globl _OC1COMP
                                    220 	.globl _OC1COMP1
                                    221 	.globl _OC1COMP0
                                    222 	.globl _OC0STATUS
                                    223 	.globl _OC0PIN
                                    224 	.globl _OC0MODE
                                    225 	.globl _OC0COMP
                                    226 	.globl _OC0COMP1
                                    227 	.globl _OC0COMP0
                                    228 	.globl _NVSTATUS
                                    229 	.globl _NVKEY
                                    230 	.globl _NVDATA
                                    231 	.globl _NVDATA1
                                    232 	.globl _NVDATA0
                                    233 	.globl _NVADDR
                                    234 	.globl _NVADDR1
                                    235 	.globl _NVADDR0
                                    236 	.globl _IC1STATUS
                                    237 	.globl _IC1MODE
                                    238 	.globl _IC1CAPT
                                    239 	.globl _IC1CAPT1
                                    240 	.globl _IC1CAPT0
                                    241 	.globl _IC0STATUS
                                    242 	.globl _IC0MODE
                                    243 	.globl _IC0CAPT
                                    244 	.globl _IC0CAPT1
                                    245 	.globl _IC0CAPT0
                                    246 	.globl _PORTR
                                    247 	.globl _PORTC
                                    248 	.globl _PORTB
                                    249 	.globl _PORTA
                                    250 	.globl _PINR
                                    251 	.globl _PINC
                                    252 	.globl _PINB
                                    253 	.globl _PINA
                                    254 	.globl _DIRR
                                    255 	.globl _DIRC
                                    256 	.globl _DIRB
                                    257 	.globl _DIRA
                                    258 	.globl _DBGLNKSTAT
                                    259 	.globl _DBGLNKBUF
                                    260 	.globl _CODECONFIG
                                    261 	.globl _CLKSTAT
                                    262 	.globl _CLKCON
                                    263 	.globl _ANALOGCOMP
                                    264 	.globl _ADCCONV
                                    265 	.globl _ADCCLKSRC
                                    266 	.globl _ADCCH3CONFIG
                                    267 	.globl _ADCCH2CONFIG
                                    268 	.globl _ADCCH1CONFIG
                                    269 	.globl _ADCCH0CONFIG
                                    270 	.globl __XPAGE
                                    271 	.globl _XPAGE
                                    272 	.globl _SP
                                    273 	.globl _PSW
                                    274 	.globl _PCON
                                    275 	.globl _IP
                                    276 	.globl _IE
                                    277 	.globl _EIP
                                    278 	.globl _EIE
                                    279 	.globl _E2IP
                                    280 	.globl _E2IE
                                    281 	.globl _DPS
                                    282 	.globl _DPTR1
                                    283 	.globl _DPTR0
                                    284 	.globl _DPL1
                                    285 	.globl _DPL
                                    286 	.globl _DPH1
                                    287 	.globl _DPH
                                    288 	.globl _B
                                    289 	.globl _ACC
                                    290 	.globl _RNGMODE
                                    291 	.globl _RNGCLKSRC1
                                    292 	.globl _RNGCLKSRC0
                                    293 	.globl _RNGBYTE
                                    294 	.globl _AESOUTADDR
                                    295 	.globl _AESOUTADDR1
                                    296 	.globl _AESOUTADDR0
                                    297 	.globl _AESMODE
                                    298 	.globl _AESKEYADDR
                                    299 	.globl _AESKEYADDR1
                                    300 	.globl _AESKEYADDR0
                                    301 	.globl _AESINADDR
                                    302 	.globl _AESINADDR1
                                    303 	.globl _AESINADDR0
                                    304 	.globl _AESCURBLOCK
                                    305 	.globl _AESCONFIG
                                    306 	.globl _XTALREADY
                                    307 	.globl _XTALOSC
                                    308 	.globl _XTALAMPL
                                    309 	.globl _SILICONREV
                                    310 	.globl _SCRATCH3
                                    311 	.globl _SCRATCH2
                                    312 	.globl _SCRATCH1
                                    313 	.globl _SCRATCH0
                                    314 	.globl _RADIOMUX
                                    315 	.globl _RADIOFSTATADDR
                                    316 	.globl _RADIOFSTATADDR1
                                    317 	.globl _RADIOFSTATADDR0
                                    318 	.globl _RADIOFDATAADDR
                                    319 	.globl _RADIOFDATAADDR1
                                    320 	.globl _RADIOFDATAADDR0
                                    321 	.globl _OSCRUN
                                    322 	.globl _OSCREADY
                                    323 	.globl _OSCFORCERUN
                                    324 	.globl _OSCCALIB
                                    325 	.globl _MISCCTRL
                                    326 	.globl _LPXOSCGM
                                    327 	.globl _LPOSCREF
                                    328 	.globl _LPOSCREF1
                                    329 	.globl _LPOSCREF0
                                    330 	.globl _LPOSCPER
                                    331 	.globl _LPOSCPER1
                                    332 	.globl _LPOSCPER0
                                    333 	.globl _LPOSCKFILT
                                    334 	.globl _LPOSCKFILT1
                                    335 	.globl _LPOSCKFILT0
                                    336 	.globl _LPOSCFREQ
                                    337 	.globl _LPOSCFREQ1
                                    338 	.globl _LPOSCFREQ0
                                    339 	.globl _LPOSCCONFIG
                                    340 	.globl _PINSEL
                                    341 	.globl _PINCHGC
                                    342 	.globl _PINCHGB
                                    343 	.globl _PINCHGA
                                    344 	.globl _PALTRADIO
                                    345 	.globl _PALTC
                                    346 	.globl _PALTB
                                    347 	.globl _PALTA
                                    348 	.globl _INTCHGC
                                    349 	.globl _INTCHGB
                                    350 	.globl _INTCHGA
                                    351 	.globl _EXTIRQ
                                    352 	.globl _GPIOENABLE
                                    353 	.globl _ANALOGA
                                    354 	.globl _FRCOSCREF
                                    355 	.globl _FRCOSCREF1
                                    356 	.globl _FRCOSCREF0
                                    357 	.globl _FRCOSCPER
                                    358 	.globl _FRCOSCPER1
                                    359 	.globl _FRCOSCPER0
                                    360 	.globl _FRCOSCKFILT
                                    361 	.globl _FRCOSCKFILT1
                                    362 	.globl _FRCOSCKFILT0
                                    363 	.globl _FRCOSCFREQ
                                    364 	.globl _FRCOSCFREQ1
                                    365 	.globl _FRCOSCFREQ0
                                    366 	.globl _FRCOSCCTRL
                                    367 	.globl _FRCOSCCONFIG
                                    368 	.globl _DMA1CONFIG
                                    369 	.globl _DMA1ADDR
                                    370 	.globl _DMA1ADDR1
                                    371 	.globl _DMA1ADDR0
                                    372 	.globl _DMA0CONFIG
                                    373 	.globl _DMA0ADDR
                                    374 	.globl _DMA0ADDR1
                                    375 	.globl _DMA0ADDR0
                                    376 	.globl _ADCTUNE2
                                    377 	.globl _ADCTUNE1
                                    378 	.globl _ADCTUNE0
                                    379 	.globl _ADCCH3VAL
                                    380 	.globl _ADCCH3VAL1
                                    381 	.globl _ADCCH3VAL0
                                    382 	.globl _ADCCH2VAL
                                    383 	.globl _ADCCH2VAL1
                                    384 	.globl _ADCCH2VAL0
                                    385 	.globl _ADCCH1VAL
                                    386 	.globl _ADCCH1VAL1
                                    387 	.globl _ADCCH1VAL0
                                    388 	.globl _ADCCH0VAL
                                    389 	.globl _ADCCH0VAL1
                                    390 	.globl _ADCCH0VAL0
                                    391 ;--------------------------------------------------------
                                    392 ; special function registers
                                    393 ;--------------------------------------------------------
                                    394 	.area RSEG    (ABS,DATA)
      000000                        395 	.org 0x0000
                           0000E0   396 G$ACC$0$0 == 0x00e0
                           0000E0   397 _ACC	=	0x00e0
                           0000F0   398 G$B$0$0 == 0x00f0
                           0000F0   399 _B	=	0x00f0
                           000083   400 G$DPH$0$0 == 0x0083
                           000083   401 _DPH	=	0x0083
                           000085   402 G$DPH1$0$0 == 0x0085
                           000085   403 _DPH1	=	0x0085
                           000082   404 G$DPL$0$0 == 0x0082
                           000082   405 _DPL	=	0x0082
                           000084   406 G$DPL1$0$0 == 0x0084
                           000084   407 _DPL1	=	0x0084
                           008382   408 G$DPTR0$0$0 == 0x8382
                           008382   409 _DPTR0	=	0x8382
                           008584   410 G$DPTR1$0$0 == 0x8584
                           008584   411 _DPTR1	=	0x8584
                           000086   412 G$DPS$0$0 == 0x0086
                           000086   413 _DPS	=	0x0086
                           0000A0   414 G$E2IE$0$0 == 0x00a0
                           0000A0   415 _E2IE	=	0x00a0
                           0000C0   416 G$E2IP$0$0 == 0x00c0
                           0000C0   417 _E2IP	=	0x00c0
                           000098   418 G$EIE$0$0 == 0x0098
                           000098   419 _EIE	=	0x0098
                           0000B0   420 G$EIP$0$0 == 0x00b0
                           0000B0   421 _EIP	=	0x00b0
                           0000A8   422 G$IE$0$0 == 0x00a8
                           0000A8   423 _IE	=	0x00a8
                           0000B8   424 G$IP$0$0 == 0x00b8
                           0000B8   425 _IP	=	0x00b8
                           000087   426 G$PCON$0$0 == 0x0087
                           000087   427 _PCON	=	0x0087
                           0000D0   428 G$PSW$0$0 == 0x00d0
                           0000D0   429 _PSW	=	0x00d0
                           000081   430 G$SP$0$0 == 0x0081
                           000081   431 _SP	=	0x0081
                           0000D9   432 G$XPAGE$0$0 == 0x00d9
                           0000D9   433 _XPAGE	=	0x00d9
                           0000D9   434 G$_XPAGE$0$0 == 0x00d9
                           0000D9   435 __XPAGE	=	0x00d9
                           0000CA   436 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   437 _ADCCH0CONFIG	=	0x00ca
                           0000CB   438 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   439 _ADCCH1CONFIG	=	0x00cb
                           0000D2   440 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   441 _ADCCH2CONFIG	=	0x00d2
                           0000D3   442 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   443 _ADCCH3CONFIG	=	0x00d3
                           0000D1   444 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   445 _ADCCLKSRC	=	0x00d1
                           0000C9   446 G$ADCCONV$0$0 == 0x00c9
                           0000C9   447 _ADCCONV	=	0x00c9
                           0000E1   448 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   449 _ANALOGCOMP	=	0x00e1
                           0000C6   450 G$CLKCON$0$0 == 0x00c6
                           0000C6   451 _CLKCON	=	0x00c6
                           0000C7   452 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   453 _CLKSTAT	=	0x00c7
                           000097   454 G$CODECONFIG$0$0 == 0x0097
                           000097   455 _CODECONFIG	=	0x0097
                           0000E3   456 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   457 _DBGLNKBUF	=	0x00e3
                           0000E2   458 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   459 _DBGLNKSTAT	=	0x00e2
                           000089   460 G$DIRA$0$0 == 0x0089
                           000089   461 _DIRA	=	0x0089
                           00008A   462 G$DIRB$0$0 == 0x008a
                           00008A   463 _DIRB	=	0x008a
                           00008B   464 G$DIRC$0$0 == 0x008b
                           00008B   465 _DIRC	=	0x008b
                           00008E   466 G$DIRR$0$0 == 0x008e
                           00008E   467 _DIRR	=	0x008e
                           0000C8   468 G$PINA$0$0 == 0x00c8
                           0000C8   469 _PINA	=	0x00c8
                           0000E8   470 G$PINB$0$0 == 0x00e8
                           0000E8   471 _PINB	=	0x00e8
                           0000F8   472 G$PINC$0$0 == 0x00f8
                           0000F8   473 _PINC	=	0x00f8
                           00008D   474 G$PINR$0$0 == 0x008d
                           00008D   475 _PINR	=	0x008d
                           000080   476 G$PORTA$0$0 == 0x0080
                           000080   477 _PORTA	=	0x0080
                           000088   478 G$PORTB$0$0 == 0x0088
                           000088   479 _PORTB	=	0x0088
                           000090   480 G$PORTC$0$0 == 0x0090
                           000090   481 _PORTC	=	0x0090
                           00008C   482 G$PORTR$0$0 == 0x008c
                           00008C   483 _PORTR	=	0x008c
                           0000CE   484 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   485 _IC0CAPT0	=	0x00ce
                           0000CF   486 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   487 _IC0CAPT1	=	0x00cf
                           00CFCE   488 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   489 _IC0CAPT	=	0xcfce
                           0000CC   490 G$IC0MODE$0$0 == 0x00cc
                           0000CC   491 _IC0MODE	=	0x00cc
                           0000CD   492 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   493 _IC0STATUS	=	0x00cd
                           0000D6   494 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   495 _IC1CAPT0	=	0x00d6
                           0000D7   496 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   497 _IC1CAPT1	=	0x00d7
                           00D7D6   498 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   499 _IC1CAPT	=	0xd7d6
                           0000D4   500 G$IC1MODE$0$0 == 0x00d4
                           0000D4   501 _IC1MODE	=	0x00d4
                           0000D5   502 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   503 _IC1STATUS	=	0x00d5
                           000092   504 G$NVADDR0$0$0 == 0x0092
                           000092   505 _NVADDR0	=	0x0092
                           000093   506 G$NVADDR1$0$0 == 0x0093
                           000093   507 _NVADDR1	=	0x0093
                           009392   508 G$NVADDR$0$0 == 0x9392
                           009392   509 _NVADDR	=	0x9392
                           000094   510 G$NVDATA0$0$0 == 0x0094
                           000094   511 _NVDATA0	=	0x0094
                           000095   512 G$NVDATA1$0$0 == 0x0095
                           000095   513 _NVDATA1	=	0x0095
                           009594   514 G$NVDATA$0$0 == 0x9594
                           009594   515 _NVDATA	=	0x9594
                           000096   516 G$NVKEY$0$0 == 0x0096
                           000096   517 _NVKEY	=	0x0096
                           000091   518 G$NVSTATUS$0$0 == 0x0091
                           000091   519 _NVSTATUS	=	0x0091
                           0000BC   520 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   521 _OC0COMP0	=	0x00bc
                           0000BD   522 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   523 _OC0COMP1	=	0x00bd
                           00BDBC   524 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   525 _OC0COMP	=	0xbdbc
                           0000B9   526 G$OC0MODE$0$0 == 0x00b9
                           0000B9   527 _OC0MODE	=	0x00b9
                           0000BA   528 G$OC0PIN$0$0 == 0x00ba
                           0000BA   529 _OC0PIN	=	0x00ba
                           0000BB   530 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   531 _OC0STATUS	=	0x00bb
                           0000C4   532 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   533 _OC1COMP0	=	0x00c4
                           0000C5   534 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   535 _OC1COMP1	=	0x00c5
                           00C5C4   536 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   537 _OC1COMP	=	0xc5c4
                           0000C1   538 G$OC1MODE$0$0 == 0x00c1
                           0000C1   539 _OC1MODE	=	0x00c1
                           0000C2   540 G$OC1PIN$0$0 == 0x00c2
                           0000C2   541 _OC1PIN	=	0x00c2
                           0000C3   542 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   543 _OC1STATUS	=	0x00c3
                           0000B1   544 G$RADIOACC$0$0 == 0x00b1
                           0000B1   545 _RADIOACC	=	0x00b1
                           0000B3   546 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   547 _RADIOADDR0	=	0x00b3
                           0000B2   548 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   549 _RADIOADDR1	=	0x00b2
                           00B2B3   550 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   551 _RADIOADDR	=	0xb2b3
                           0000B7   552 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   553 _RADIODATA0	=	0x00b7
                           0000B6   554 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   555 _RADIODATA1	=	0x00b6
                           0000B5   556 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   557 _RADIODATA2	=	0x00b5
                           0000B4   558 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   559 _RADIODATA3	=	0x00b4
                           B4B5B6B7   560 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   561 _RADIODATA	=	0xb4b5b6b7
                           0000BE   562 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   563 _RADIOSTAT0	=	0x00be
                           0000BF   564 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   565 _RADIOSTAT1	=	0x00bf
                           00BFBE   566 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   567 _RADIOSTAT	=	0xbfbe
                           0000DF   568 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   569 _SPCLKSRC	=	0x00df
                           0000DC   570 G$SPMODE$0$0 == 0x00dc
                           0000DC   571 _SPMODE	=	0x00dc
                           0000DE   572 G$SPSHREG$0$0 == 0x00de
                           0000DE   573 _SPSHREG	=	0x00de
                           0000DD   574 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   575 _SPSTATUS	=	0x00dd
                           00009A   576 G$T0CLKSRC$0$0 == 0x009a
                           00009A   577 _T0CLKSRC	=	0x009a
                           00009C   578 G$T0CNT0$0$0 == 0x009c
                           00009C   579 _T0CNT0	=	0x009c
                           00009D   580 G$T0CNT1$0$0 == 0x009d
                           00009D   581 _T0CNT1	=	0x009d
                           009D9C   582 G$T0CNT$0$0 == 0x9d9c
                           009D9C   583 _T0CNT	=	0x9d9c
                           000099   584 G$T0MODE$0$0 == 0x0099
                           000099   585 _T0MODE	=	0x0099
                           00009E   586 G$T0PERIOD0$0$0 == 0x009e
                           00009E   587 _T0PERIOD0	=	0x009e
                           00009F   588 G$T0PERIOD1$0$0 == 0x009f
                           00009F   589 _T0PERIOD1	=	0x009f
                           009F9E   590 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   591 _T0PERIOD	=	0x9f9e
                           00009B   592 G$T0STATUS$0$0 == 0x009b
                           00009B   593 _T0STATUS	=	0x009b
                           0000A2   594 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   595 _T1CLKSRC	=	0x00a2
                           0000A4   596 G$T1CNT0$0$0 == 0x00a4
                           0000A4   597 _T1CNT0	=	0x00a4
                           0000A5   598 G$T1CNT1$0$0 == 0x00a5
                           0000A5   599 _T1CNT1	=	0x00a5
                           00A5A4   600 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   601 _T1CNT	=	0xa5a4
                           0000A1   602 G$T1MODE$0$0 == 0x00a1
                           0000A1   603 _T1MODE	=	0x00a1
                           0000A6   604 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   605 _T1PERIOD0	=	0x00a6
                           0000A7   606 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   607 _T1PERIOD1	=	0x00a7
                           00A7A6   608 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   609 _T1PERIOD	=	0xa7a6
                           0000A3   610 G$T1STATUS$0$0 == 0x00a3
                           0000A3   611 _T1STATUS	=	0x00a3
                           0000AA   612 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   613 _T2CLKSRC	=	0x00aa
                           0000AC   614 G$T2CNT0$0$0 == 0x00ac
                           0000AC   615 _T2CNT0	=	0x00ac
                           0000AD   616 G$T2CNT1$0$0 == 0x00ad
                           0000AD   617 _T2CNT1	=	0x00ad
                           00ADAC   618 G$T2CNT$0$0 == 0xadac
                           00ADAC   619 _T2CNT	=	0xadac
                           0000A9   620 G$T2MODE$0$0 == 0x00a9
                           0000A9   621 _T2MODE	=	0x00a9
                           0000AE   622 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   623 _T2PERIOD0	=	0x00ae
                           0000AF   624 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   625 _T2PERIOD1	=	0x00af
                           00AFAE   626 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   627 _T2PERIOD	=	0xafae
                           0000AB   628 G$T2STATUS$0$0 == 0x00ab
                           0000AB   629 _T2STATUS	=	0x00ab
                           0000E4   630 G$U0CTRL$0$0 == 0x00e4
                           0000E4   631 _U0CTRL	=	0x00e4
                           0000E7   632 G$U0MODE$0$0 == 0x00e7
                           0000E7   633 _U0MODE	=	0x00e7
                           0000E6   634 G$U0SHREG$0$0 == 0x00e6
                           0000E6   635 _U0SHREG	=	0x00e6
                           0000E5   636 G$U0STATUS$0$0 == 0x00e5
                           0000E5   637 _U0STATUS	=	0x00e5
                           0000EC   638 G$U1CTRL$0$0 == 0x00ec
                           0000EC   639 _U1CTRL	=	0x00ec
                           0000EF   640 G$U1MODE$0$0 == 0x00ef
                           0000EF   641 _U1MODE	=	0x00ef
                           0000EE   642 G$U1SHREG$0$0 == 0x00ee
                           0000EE   643 _U1SHREG	=	0x00ee
                           0000ED   644 G$U1STATUS$0$0 == 0x00ed
                           0000ED   645 _U1STATUS	=	0x00ed
                           0000DA   646 G$WDTCFG$0$0 == 0x00da
                           0000DA   647 _WDTCFG	=	0x00da
                           0000DB   648 G$WDTRESET$0$0 == 0x00db
                           0000DB   649 _WDTRESET	=	0x00db
                           0000F1   650 G$WTCFGA$0$0 == 0x00f1
                           0000F1   651 _WTCFGA	=	0x00f1
                           0000F9   652 G$WTCFGB$0$0 == 0x00f9
                           0000F9   653 _WTCFGB	=	0x00f9
                           0000F2   654 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   655 _WTCNTA0	=	0x00f2
                           0000F3   656 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   657 _WTCNTA1	=	0x00f3
                           00F3F2   658 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   659 _WTCNTA	=	0xf3f2
                           0000FA   660 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   661 _WTCNTB0	=	0x00fa
                           0000FB   662 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   663 _WTCNTB1	=	0x00fb
                           00FBFA   664 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   665 _WTCNTB	=	0xfbfa
                           0000EB   666 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   667 _WTCNTR1	=	0x00eb
                           0000F4   668 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   669 _WTEVTA0	=	0x00f4
                           0000F5   670 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   671 _WTEVTA1	=	0x00f5
                           00F5F4   672 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   673 _WTEVTA	=	0xf5f4
                           0000F6   674 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   675 _WTEVTB0	=	0x00f6
                           0000F7   676 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   677 _WTEVTB1	=	0x00f7
                           00F7F6   678 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   679 _WTEVTB	=	0xf7f6
                           0000FC   680 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   681 _WTEVTC0	=	0x00fc
                           0000FD   682 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   683 _WTEVTC1	=	0x00fd
                           00FDFC   684 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   685 _WTEVTC	=	0xfdfc
                           0000FE   686 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   687 _WTEVTD0	=	0x00fe
                           0000FF   688 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   689 _WTEVTD1	=	0x00ff
                           00FFFE   690 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   691 _WTEVTD	=	0xfffe
                           0000E9   692 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   693 _WTIRQEN	=	0x00e9
                           0000EA   694 G$WTSTAT$0$0 == 0x00ea
                           0000EA   695 _WTSTAT	=	0x00ea
                                    696 ;--------------------------------------------------------
                                    697 ; special function bits
                                    698 ;--------------------------------------------------------
                                    699 	.area RSEG    (ABS,DATA)
      000000                        700 	.org 0x0000
                           0000E0   701 G$ACC_0$0$0 == 0x00e0
                           0000E0   702 _ACC_0	=	0x00e0
                           0000E1   703 G$ACC_1$0$0 == 0x00e1
                           0000E1   704 _ACC_1	=	0x00e1
                           0000E2   705 G$ACC_2$0$0 == 0x00e2
                           0000E2   706 _ACC_2	=	0x00e2
                           0000E3   707 G$ACC_3$0$0 == 0x00e3
                           0000E3   708 _ACC_3	=	0x00e3
                           0000E4   709 G$ACC_4$0$0 == 0x00e4
                           0000E4   710 _ACC_4	=	0x00e4
                           0000E5   711 G$ACC_5$0$0 == 0x00e5
                           0000E5   712 _ACC_5	=	0x00e5
                           0000E6   713 G$ACC_6$0$0 == 0x00e6
                           0000E6   714 _ACC_6	=	0x00e6
                           0000E7   715 G$ACC_7$0$0 == 0x00e7
                           0000E7   716 _ACC_7	=	0x00e7
                           0000F0   717 G$B_0$0$0 == 0x00f0
                           0000F0   718 _B_0	=	0x00f0
                           0000F1   719 G$B_1$0$0 == 0x00f1
                           0000F1   720 _B_1	=	0x00f1
                           0000F2   721 G$B_2$0$0 == 0x00f2
                           0000F2   722 _B_2	=	0x00f2
                           0000F3   723 G$B_3$0$0 == 0x00f3
                           0000F3   724 _B_3	=	0x00f3
                           0000F4   725 G$B_4$0$0 == 0x00f4
                           0000F4   726 _B_4	=	0x00f4
                           0000F5   727 G$B_5$0$0 == 0x00f5
                           0000F5   728 _B_5	=	0x00f5
                           0000F6   729 G$B_6$0$0 == 0x00f6
                           0000F6   730 _B_6	=	0x00f6
                           0000F7   731 G$B_7$0$0 == 0x00f7
                           0000F7   732 _B_7	=	0x00f7
                           0000A0   733 G$E2IE_0$0$0 == 0x00a0
                           0000A0   734 _E2IE_0	=	0x00a0
                           0000A1   735 G$E2IE_1$0$0 == 0x00a1
                           0000A1   736 _E2IE_1	=	0x00a1
                           0000A2   737 G$E2IE_2$0$0 == 0x00a2
                           0000A2   738 _E2IE_2	=	0x00a2
                           0000A3   739 G$E2IE_3$0$0 == 0x00a3
                           0000A3   740 _E2IE_3	=	0x00a3
                           0000A4   741 G$E2IE_4$0$0 == 0x00a4
                           0000A4   742 _E2IE_4	=	0x00a4
                           0000A5   743 G$E2IE_5$0$0 == 0x00a5
                           0000A5   744 _E2IE_5	=	0x00a5
                           0000A6   745 G$E2IE_6$0$0 == 0x00a6
                           0000A6   746 _E2IE_6	=	0x00a6
                           0000A7   747 G$E2IE_7$0$0 == 0x00a7
                           0000A7   748 _E2IE_7	=	0x00a7
                           0000C0   749 G$E2IP_0$0$0 == 0x00c0
                           0000C0   750 _E2IP_0	=	0x00c0
                           0000C1   751 G$E2IP_1$0$0 == 0x00c1
                           0000C1   752 _E2IP_1	=	0x00c1
                           0000C2   753 G$E2IP_2$0$0 == 0x00c2
                           0000C2   754 _E2IP_2	=	0x00c2
                           0000C3   755 G$E2IP_3$0$0 == 0x00c3
                           0000C3   756 _E2IP_3	=	0x00c3
                           0000C4   757 G$E2IP_4$0$0 == 0x00c4
                           0000C4   758 _E2IP_4	=	0x00c4
                           0000C5   759 G$E2IP_5$0$0 == 0x00c5
                           0000C5   760 _E2IP_5	=	0x00c5
                           0000C6   761 G$E2IP_6$0$0 == 0x00c6
                           0000C6   762 _E2IP_6	=	0x00c6
                           0000C7   763 G$E2IP_7$0$0 == 0x00c7
                           0000C7   764 _E2IP_7	=	0x00c7
                           000098   765 G$EIE_0$0$0 == 0x0098
                           000098   766 _EIE_0	=	0x0098
                           000099   767 G$EIE_1$0$0 == 0x0099
                           000099   768 _EIE_1	=	0x0099
                           00009A   769 G$EIE_2$0$0 == 0x009a
                           00009A   770 _EIE_2	=	0x009a
                           00009B   771 G$EIE_3$0$0 == 0x009b
                           00009B   772 _EIE_3	=	0x009b
                           00009C   773 G$EIE_4$0$0 == 0x009c
                           00009C   774 _EIE_4	=	0x009c
                           00009D   775 G$EIE_5$0$0 == 0x009d
                           00009D   776 _EIE_5	=	0x009d
                           00009E   777 G$EIE_6$0$0 == 0x009e
                           00009E   778 _EIE_6	=	0x009e
                           00009F   779 G$EIE_7$0$0 == 0x009f
                           00009F   780 _EIE_7	=	0x009f
                           0000B0   781 G$EIP_0$0$0 == 0x00b0
                           0000B0   782 _EIP_0	=	0x00b0
                           0000B1   783 G$EIP_1$0$0 == 0x00b1
                           0000B1   784 _EIP_1	=	0x00b1
                           0000B2   785 G$EIP_2$0$0 == 0x00b2
                           0000B2   786 _EIP_2	=	0x00b2
                           0000B3   787 G$EIP_3$0$0 == 0x00b3
                           0000B3   788 _EIP_3	=	0x00b3
                           0000B4   789 G$EIP_4$0$0 == 0x00b4
                           0000B4   790 _EIP_4	=	0x00b4
                           0000B5   791 G$EIP_5$0$0 == 0x00b5
                           0000B5   792 _EIP_5	=	0x00b5
                           0000B6   793 G$EIP_6$0$0 == 0x00b6
                           0000B6   794 _EIP_6	=	0x00b6
                           0000B7   795 G$EIP_7$0$0 == 0x00b7
                           0000B7   796 _EIP_7	=	0x00b7
                           0000A8   797 G$IE_0$0$0 == 0x00a8
                           0000A8   798 _IE_0	=	0x00a8
                           0000A9   799 G$IE_1$0$0 == 0x00a9
                           0000A9   800 _IE_1	=	0x00a9
                           0000AA   801 G$IE_2$0$0 == 0x00aa
                           0000AA   802 _IE_2	=	0x00aa
                           0000AB   803 G$IE_3$0$0 == 0x00ab
                           0000AB   804 _IE_3	=	0x00ab
                           0000AC   805 G$IE_4$0$0 == 0x00ac
                           0000AC   806 _IE_4	=	0x00ac
                           0000AD   807 G$IE_5$0$0 == 0x00ad
                           0000AD   808 _IE_5	=	0x00ad
                           0000AE   809 G$IE_6$0$0 == 0x00ae
                           0000AE   810 _IE_6	=	0x00ae
                           0000AF   811 G$IE_7$0$0 == 0x00af
                           0000AF   812 _IE_7	=	0x00af
                           0000AF   813 G$EA$0$0 == 0x00af
                           0000AF   814 _EA	=	0x00af
                           0000B8   815 G$IP_0$0$0 == 0x00b8
                           0000B8   816 _IP_0	=	0x00b8
                           0000B9   817 G$IP_1$0$0 == 0x00b9
                           0000B9   818 _IP_1	=	0x00b9
                           0000BA   819 G$IP_2$0$0 == 0x00ba
                           0000BA   820 _IP_2	=	0x00ba
                           0000BB   821 G$IP_3$0$0 == 0x00bb
                           0000BB   822 _IP_3	=	0x00bb
                           0000BC   823 G$IP_4$0$0 == 0x00bc
                           0000BC   824 _IP_4	=	0x00bc
                           0000BD   825 G$IP_5$0$0 == 0x00bd
                           0000BD   826 _IP_5	=	0x00bd
                           0000BE   827 G$IP_6$0$0 == 0x00be
                           0000BE   828 _IP_6	=	0x00be
                           0000BF   829 G$IP_7$0$0 == 0x00bf
                           0000BF   830 _IP_7	=	0x00bf
                           0000D0   831 G$P$0$0 == 0x00d0
                           0000D0   832 _P	=	0x00d0
                           0000D1   833 G$F1$0$0 == 0x00d1
                           0000D1   834 _F1	=	0x00d1
                           0000D2   835 G$OV$0$0 == 0x00d2
                           0000D2   836 _OV	=	0x00d2
                           0000D3   837 G$RS0$0$0 == 0x00d3
                           0000D3   838 _RS0	=	0x00d3
                           0000D4   839 G$RS1$0$0 == 0x00d4
                           0000D4   840 _RS1	=	0x00d4
                           0000D5   841 G$F0$0$0 == 0x00d5
                           0000D5   842 _F0	=	0x00d5
                           0000D6   843 G$AC$0$0 == 0x00d6
                           0000D6   844 _AC	=	0x00d6
                           0000D7   845 G$CY$0$0 == 0x00d7
                           0000D7   846 _CY	=	0x00d7
                           0000C8   847 G$PINA_0$0$0 == 0x00c8
                           0000C8   848 _PINA_0	=	0x00c8
                           0000C9   849 G$PINA_1$0$0 == 0x00c9
                           0000C9   850 _PINA_1	=	0x00c9
                           0000CA   851 G$PINA_2$0$0 == 0x00ca
                           0000CA   852 _PINA_2	=	0x00ca
                           0000CB   853 G$PINA_3$0$0 == 0x00cb
                           0000CB   854 _PINA_3	=	0x00cb
                           0000CC   855 G$PINA_4$0$0 == 0x00cc
                           0000CC   856 _PINA_4	=	0x00cc
                           0000CD   857 G$PINA_5$0$0 == 0x00cd
                           0000CD   858 _PINA_5	=	0x00cd
                           0000CE   859 G$PINA_6$0$0 == 0x00ce
                           0000CE   860 _PINA_6	=	0x00ce
                           0000CF   861 G$PINA_7$0$0 == 0x00cf
                           0000CF   862 _PINA_7	=	0x00cf
                           0000E8   863 G$PINB_0$0$0 == 0x00e8
                           0000E8   864 _PINB_0	=	0x00e8
                           0000E9   865 G$PINB_1$0$0 == 0x00e9
                           0000E9   866 _PINB_1	=	0x00e9
                           0000EA   867 G$PINB_2$0$0 == 0x00ea
                           0000EA   868 _PINB_2	=	0x00ea
                           0000EB   869 G$PINB_3$0$0 == 0x00eb
                           0000EB   870 _PINB_3	=	0x00eb
                           0000EC   871 G$PINB_4$0$0 == 0x00ec
                           0000EC   872 _PINB_4	=	0x00ec
                           0000ED   873 G$PINB_5$0$0 == 0x00ed
                           0000ED   874 _PINB_5	=	0x00ed
                           0000EE   875 G$PINB_6$0$0 == 0x00ee
                           0000EE   876 _PINB_6	=	0x00ee
                           0000EF   877 G$PINB_7$0$0 == 0x00ef
                           0000EF   878 _PINB_7	=	0x00ef
                           0000F8   879 G$PINC_0$0$0 == 0x00f8
                           0000F8   880 _PINC_0	=	0x00f8
                           0000F9   881 G$PINC_1$0$0 == 0x00f9
                           0000F9   882 _PINC_1	=	0x00f9
                           0000FA   883 G$PINC_2$0$0 == 0x00fa
                           0000FA   884 _PINC_2	=	0x00fa
                           0000FB   885 G$PINC_3$0$0 == 0x00fb
                           0000FB   886 _PINC_3	=	0x00fb
                           0000FC   887 G$PINC_4$0$0 == 0x00fc
                           0000FC   888 _PINC_4	=	0x00fc
                           0000FD   889 G$PINC_5$0$0 == 0x00fd
                           0000FD   890 _PINC_5	=	0x00fd
                           0000FE   891 G$PINC_6$0$0 == 0x00fe
                           0000FE   892 _PINC_6	=	0x00fe
                           0000FF   893 G$PINC_7$0$0 == 0x00ff
                           0000FF   894 _PINC_7	=	0x00ff
                           000080   895 G$PORTA_0$0$0 == 0x0080
                           000080   896 _PORTA_0	=	0x0080
                           000081   897 G$PORTA_1$0$0 == 0x0081
                           000081   898 _PORTA_1	=	0x0081
                           000082   899 G$PORTA_2$0$0 == 0x0082
                           000082   900 _PORTA_2	=	0x0082
                           000083   901 G$PORTA_3$0$0 == 0x0083
                           000083   902 _PORTA_3	=	0x0083
                           000084   903 G$PORTA_4$0$0 == 0x0084
                           000084   904 _PORTA_4	=	0x0084
                           000085   905 G$PORTA_5$0$0 == 0x0085
                           000085   906 _PORTA_5	=	0x0085
                           000086   907 G$PORTA_6$0$0 == 0x0086
                           000086   908 _PORTA_6	=	0x0086
                           000087   909 G$PORTA_7$0$0 == 0x0087
                           000087   910 _PORTA_7	=	0x0087
                           000088   911 G$PORTB_0$0$0 == 0x0088
                           000088   912 _PORTB_0	=	0x0088
                           000089   913 G$PORTB_1$0$0 == 0x0089
                           000089   914 _PORTB_1	=	0x0089
                           00008A   915 G$PORTB_2$0$0 == 0x008a
                           00008A   916 _PORTB_2	=	0x008a
                           00008B   917 G$PORTB_3$0$0 == 0x008b
                           00008B   918 _PORTB_3	=	0x008b
                           00008C   919 G$PORTB_4$0$0 == 0x008c
                           00008C   920 _PORTB_4	=	0x008c
                           00008D   921 G$PORTB_5$0$0 == 0x008d
                           00008D   922 _PORTB_5	=	0x008d
                           00008E   923 G$PORTB_6$0$0 == 0x008e
                           00008E   924 _PORTB_6	=	0x008e
                           00008F   925 G$PORTB_7$0$0 == 0x008f
                           00008F   926 _PORTB_7	=	0x008f
                           000090   927 G$PORTC_0$0$0 == 0x0090
                           000090   928 _PORTC_0	=	0x0090
                           000091   929 G$PORTC_1$0$0 == 0x0091
                           000091   930 _PORTC_1	=	0x0091
                           000092   931 G$PORTC_2$0$0 == 0x0092
                           000092   932 _PORTC_2	=	0x0092
                           000093   933 G$PORTC_3$0$0 == 0x0093
                           000093   934 _PORTC_3	=	0x0093
                           000094   935 G$PORTC_4$0$0 == 0x0094
                           000094   936 _PORTC_4	=	0x0094
                           000095   937 G$PORTC_5$0$0 == 0x0095
                           000095   938 _PORTC_5	=	0x0095
                           000096   939 G$PORTC_6$0$0 == 0x0096
                           000096   940 _PORTC_6	=	0x0096
                           000097   941 G$PORTC_7$0$0 == 0x0097
                           000097   942 _PORTC_7	=	0x0097
                                    943 ;--------------------------------------------------------
                                    944 ; overlayable register banks
                                    945 ;--------------------------------------------------------
                                    946 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        947 	.ds 8
                                    948 ;--------------------------------------------------------
                                    949 ; internal ram data
                                    950 ;--------------------------------------------------------
                                    951 	.area DSEG    (DATA)
                                    952 ;--------------------------------------------------------
                                    953 ; overlayable items in internal ram 
                                    954 ;--------------------------------------------------------
                                    955 ;--------------------------------------------------------
                                    956 ; indirectly addressable internal ram data
                                    957 ;--------------------------------------------------------
                                    958 	.area ISEG    (DATA)
                                    959 ;--------------------------------------------------------
                                    960 ; absolute internal ram data
                                    961 ;--------------------------------------------------------
                                    962 	.area IABS    (ABS,DATA)
                                    963 	.area IABS    (ABS,DATA)
                                    964 ;--------------------------------------------------------
                                    965 ; bit data
                                    966 ;--------------------------------------------------------
                                    967 	.area BSEG    (BIT)
                                    968 ;--------------------------------------------------------
                                    969 ; paged external ram data
                                    970 ;--------------------------------------------------------
                                    971 	.area PSEG    (PAG,XDATA)
                                    972 ;--------------------------------------------------------
                                    973 ; external ram data
                                    974 ;--------------------------------------------------------
                                    975 	.area XSEG    (XDATA)
                           007020   976 G$ADCCH0VAL0$0$0 == 0x7020
                           007020   977 _ADCCH0VAL0	=	0x7020
                           007021   978 G$ADCCH0VAL1$0$0 == 0x7021
                           007021   979 _ADCCH0VAL1	=	0x7021
                           007020   980 G$ADCCH0VAL$0$0 == 0x7020
                           007020   981 _ADCCH0VAL	=	0x7020
                           007022   982 G$ADCCH1VAL0$0$0 == 0x7022
                           007022   983 _ADCCH1VAL0	=	0x7022
                           007023   984 G$ADCCH1VAL1$0$0 == 0x7023
                           007023   985 _ADCCH1VAL1	=	0x7023
                           007022   986 G$ADCCH1VAL$0$0 == 0x7022
                           007022   987 _ADCCH1VAL	=	0x7022
                           007024   988 G$ADCCH2VAL0$0$0 == 0x7024
                           007024   989 _ADCCH2VAL0	=	0x7024
                           007025   990 G$ADCCH2VAL1$0$0 == 0x7025
                           007025   991 _ADCCH2VAL1	=	0x7025
                           007024   992 G$ADCCH2VAL$0$0 == 0x7024
                           007024   993 _ADCCH2VAL	=	0x7024
                           007026   994 G$ADCCH3VAL0$0$0 == 0x7026
                           007026   995 _ADCCH3VAL0	=	0x7026
                           007027   996 G$ADCCH3VAL1$0$0 == 0x7027
                           007027   997 _ADCCH3VAL1	=	0x7027
                           007026   998 G$ADCCH3VAL$0$0 == 0x7026
                           007026   999 _ADCCH3VAL	=	0x7026
                           007028  1000 G$ADCTUNE0$0$0 == 0x7028
                           007028  1001 _ADCTUNE0	=	0x7028
                           007029  1002 G$ADCTUNE1$0$0 == 0x7029
                           007029  1003 _ADCTUNE1	=	0x7029
                           00702A  1004 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1005 _ADCTUNE2	=	0x702a
                           007010  1006 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1007 _DMA0ADDR0	=	0x7010
                           007011  1008 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1009 _DMA0ADDR1	=	0x7011
                           007010  1010 G$DMA0ADDR$0$0 == 0x7010
                           007010  1011 _DMA0ADDR	=	0x7010
                           007014  1012 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1013 _DMA0CONFIG	=	0x7014
                           007012  1014 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1015 _DMA1ADDR0	=	0x7012
                           007013  1016 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1017 _DMA1ADDR1	=	0x7013
                           007012  1018 G$DMA1ADDR$0$0 == 0x7012
                           007012  1019 _DMA1ADDR	=	0x7012
                           007015  1020 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1021 _DMA1CONFIG	=	0x7015
                           007070  1022 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1023 _FRCOSCCONFIG	=	0x7070
                           007071  1024 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1025 _FRCOSCCTRL	=	0x7071
                           007076  1026 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1027 _FRCOSCFREQ0	=	0x7076
                           007077  1028 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1029 _FRCOSCFREQ1	=	0x7077
                           007076  1030 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1031 _FRCOSCFREQ	=	0x7076
                           007072  1032 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1033 _FRCOSCKFILT0	=	0x7072
                           007073  1034 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1035 _FRCOSCKFILT1	=	0x7073
                           007072  1036 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1037 _FRCOSCKFILT	=	0x7072
                           007078  1038 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1039 _FRCOSCPER0	=	0x7078
                           007079  1040 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1041 _FRCOSCPER1	=	0x7079
                           007078  1042 G$FRCOSCPER$0$0 == 0x7078
                           007078  1043 _FRCOSCPER	=	0x7078
                           007074  1044 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1045 _FRCOSCREF0	=	0x7074
                           007075  1046 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1047 _FRCOSCREF1	=	0x7075
                           007074  1048 G$FRCOSCREF$0$0 == 0x7074
                           007074  1049 _FRCOSCREF	=	0x7074
                           007007  1050 G$ANALOGA$0$0 == 0x7007
                           007007  1051 _ANALOGA	=	0x7007
                           00700C  1052 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1053 _GPIOENABLE	=	0x700c
                           007003  1054 G$EXTIRQ$0$0 == 0x7003
                           007003  1055 _EXTIRQ	=	0x7003
                           007000  1056 G$INTCHGA$0$0 == 0x7000
                           007000  1057 _INTCHGA	=	0x7000
                           007001  1058 G$INTCHGB$0$0 == 0x7001
                           007001  1059 _INTCHGB	=	0x7001
                           007002  1060 G$INTCHGC$0$0 == 0x7002
                           007002  1061 _INTCHGC	=	0x7002
                           007008  1062 G$PALTA$0$0 == 0x7008
                           007008  1063 _PALTA	=	0x7008
                           007009  1064 G$PALTB$0$0 == 0x7009
                           007009  1065 _PALTB	=	0x7009
                           00700A  1066 G$PALTC$0$0 == 0x700a
                           00700A  1067 _PALTC	=	0x700a
                           007046  1068 G$PALTRADIO$0$0 == 0x7046
                           007046  1069 _PALTRADIO	=	0x7046
                           007004  1070 G$PINCHGA$0$0 == 0x7004
                           007004  1071 _PINCHGA	=	0x7004
                           007005  1072 G$PINCHGB$0$0 == 0x7005
                           007005  1073 _PINCHGB	=	0x7005
                           007006  1074 G$PINCHGC$0$0 == 0x7006
                           007006  1075 _PINCHGC	=	0x7006
                           00700B  1076 G$PINSEL$0$0 == 0x700b
                           00700B  1077 _PINSEL	=	0x700b
                           007060  1078 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1079 _LPOSCCONFIG	=	0x7060
                           007066  1080 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1081 _LPOSCFREQ0	=	0x7066
                           007067  1082 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1083 _LPOSCFREQ1	=	0x7067
                           007066  1084 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1085 _LPOSCFREQ	=	0x7066
                           007062  1086 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1087 _LPOSCKFILT0	=	0x7062
                           007063  1088 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1089 _LPOSCKFILT1	=	0x7063
                           007062  1090 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1091 _LPOSCKFILT	=	0x7062
                           007068  1092 G$LPOSCPER0$0$0 == 0x7068
                           007068  1093 _LPOSCPER0	=	0x7068
                           007069  1094 G$LPOSCPER1$0$0 == 0x7069
                           007069  1095 _LPOSCPER1	=	0x7069
                           007068  1096 G$LPOSCPER$0$0 == 0x7068
                           007068  1097 _LPOSCPER	=	0x7068
                           007064  1098 G$LPOSCREF0$0$0 == 0x7064
                           007064  1099 _LPOSCREF0	=	0x7064
                           007065  1100 G$LPOSCREF1$0$0 == 0x7065
                           007065  1101 _LPOSCREF1	=	0x7065
                           007064  1102 G$LPOSCREF$0$0 == 0x7064
                           007064  1103 _LPOSCREF	=	0x7064
                           007054  1104 G$LPXOSCGM$0$0 == 0x7054
                           007054  1105 _LPXOSCGM	=	0x7054
                           007F01  1106 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1107 _MISCCTRL	=	0x7f01
                           007053  1108 G$OSCCALIB$0$0 == 0x7053
                           007053  1109 _OSCCALIB	=	0x7053
                           007050  1110 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1111 _OSCFORCERUN	=	0x7050
                           007052  1112 G$OSCREADY$0$0 == 0x7052
                           007052  1113 _OSCREADY	=	0x7052
                           007051  1114 G$OSCRUN$0$0 == 0x7051
                           007051  1115 _OSCRUN	=	0x7051
                           007040  1116 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1117 _RADIOFDATAADDR0	=	0x7040
                           007041  1118 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1119 _RADIOFDATAADDR1	=	0x7041
                           007040  1120 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1121 _RADIOFDATAADDR	=	0x7040
                           007042  1122 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1123 _RADIOFSTATADDR0	=	0x7042
                           007043  1124 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1125 _RADIOFSTATADDR1	=	0x7043
                           007042  1126 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1127 _RADIOFSTATADDR	=	0x7042
                           007044  1128 G$RADIOMUX$0$0 == 0x7044
                           007044  1129 _RADIOMUX	=	0x7044
                           007084  1130 G$SCRATCH0$0$0 == 0x7084
                           007084  1131 _SCRATCH0	=	0x7084
                           007085  1132 G$SCRATCH1$0$0 == 0x7085
                           007085  1133 _SCRATCH1	=	0x7085
                           007086  1134 G$SCRATCH2$0$0 == 0x7086
                           007086  1135 _SCRATCH2	=	0x7086
                           007087  1136 G$SCRATCH3$0$0 == 0x7087
                           007087  1137 _SCRATCH3	=	0x7087
                           007F00  1138 G$SILICONREV$0$0 == 0x7f00
                           007F00  1139 _SILICONREV	=	0x7f00
                           007F19  1140 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1141 _XTALAMPL	=	0x7f19
                           007F18  1142 G$XTALOSC$0$0 == 0x7f18
                           007F18  1143 _XTALOSC	=	0x7f18
                           007F1A  1144 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1145 _XTALREADY	=	0x7f1a
                           00FC06  1146 Fgoldseq$flash_deviceid$0$0 == 0xfc06
                           00FC06  1147 _flash_deviceid	=	0xfc06
                           007091  1148 G$AESCONFIG$0$0 == 0x7091
                           007091  1149 _AESCONFIG	=	0x7091
                           007098  1150 G$AESCURBLOCK$0$0 == 0x7098
                           007098  1151 _AESCURBLOCK	=	0x7098
                           007094  1152 G$AESINADDR0$0$0 == 0x7094
                           007094  1153 _AESINADDR0	=	0x7094
                           007095  1154 G$AESINADDR1$0$0 == 0x7095
                           007095  1155 _AESINADDR1	=	0x7095
                           007094  1156 G$AESINADDR$0$0 == 0x7094
                           007094  1157 _AESINADDR	=	0x7094
                           007092  1158 G$AESKEYADDR0$0$0 == 0x7092
                           007092  1159 _AESKEYADDR0	=	0x7092
                           007093  1160 G$AESKEYADDR1$0$0 == 0x7093
                           007093  1161 _AESKEYADDR1	=	0x7093
                           007092  1162 G$AESKEYADDR$0$0 == 0x7092
                           007092  1163 _AESKEYADDR	=	0x7092
                           007090  1164 G$AESMODE$0$0 == 0x7090
                           007090  1165 _AESMODE	=	0x7090
                           007096  1166 G$AESOUTADDR0$0$0 == 0x7096
                           007096  1167 _AESOUTADDR0	=	0x7096
                           007097  1168 G$AESOUTADDR1$0$0 == 0x7097
                           007097  1169 _AESOUTADDR1	=	0x7097
                           007096  1170 G$AESOUTADDR$0$0 == 0x7096
                           007096  1171 _AESOUTADDR	=	0x7096
                           007081  1172 G$RNGBYTE$0$0 == 0x7081
                           007081  1173 _RNGBYTE	=	0x7081
                           007082  1174 G$RNGCLKSRC0$0$0 == 0x7082
                           007082  1175 _RNGCLKSRC0	=	0x7082
                           007083  1176 G$RNGCLKSRC1$0$0 == 0x7083
                           007083  1177 _RNGCLKSRC1	=	0x7083
                           007080  1178 G$RNGMODE$0$0 == 0x7080
                           007080  1179 _RNGMODE	=	0x7080
                                   1180 ;--------------------------------------------------------
                                   1181 ; absolute external ram data
                                   1182 ;--------------------------------------------------------
                                   1183 	.area XABS    (ABS,XDATA)
                                   1184 ;--------------------------------------------------------
                                   1185 ; external initialized ram data
                                   1186 ;--------------------------------------------------------
                                   1187 	.area XISEG   (XDATA)
                                   1188 	.area HOME    (CODE)
                                   1189 	.area GSINIT0 (CODE)
                                   1190 	.area GSINIT1 (CODE)
                                   1191 	.area GSINIT2 (CODE)
                                   1192 	.area GSINIT3 (CODE)
                                   1193 	.area GSINIT4 (CODE)
                                   1194 	.area GSINIT5 (CODE)
                                   1195 	.area GSINIT  (CODE)
                                   1196 	.area GSFINAL (CODE)
                                   1197 	.area CSEG    (CODE)
                                   1198 ;--------------------------------------------------------
                                   1199 ; global & static initialisations
                                   1200 ;--------------------------------------------------------
                                   1201 	.area HOME    (CODE)
                                   1202 	.area GSINIT  (CODE)
                                   1203 	.area GSFINAL (CODE)
                                   1204 	.area GSINIT  (CODE)
                                   1205 ;--------------------------------------------------------
                                   1206 ; Home
                                   1207 ;--------------------------------------------------------
                                   1208 	.area HOME    (CODE)
                                   1209 	.area HOME    (CODE)
                                   1210 ;--------------------------------------------------------
                                   1211 ; code
                                   1212 ;--------------------------------------------------------
                                   1213 	.area CSEG    (CODE)
                                   1214 ;------------------------------------------------------------
                                   1215 ;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
                                   1216 ;------------------------------------------------------------
                                   1217 ;i                         Allocated to registers 
                                   1218 ;------------------------------------------------------------
                           000000  1219 	G$GOLDSEQUENCE_Init$0$0 ==.
                           000000  1220 	C$goldseq.c$18$0$0 ==.
                                   1221 ;	..\COMMON\goldseq.c:18: /*__xdata*/ void GOLDSEQUENCE_Init(void)
                                   1222 ;	-----------------------------------------
                                   1223 ;	 function GOLDSEQUENCE_Init
                                   1224 ;	-----------------------------------------
      003F7D                       1225 _GOLDSEQUENCE_Init:
                           000007  1226 	ar7 = 0x07
                           000006  1227 	ar6 = 0x06
                           000005  1228 	ar5 = 0x05
                           000004  1229 	ar4 = 0x04
                           000003  1230 	ar3 = 0x03
                           000002  1231 	ar2 = 0x02
                           000001  1232 	ar1 = 0x01
                           000000  1233 	ar0 = 0x00
                           000000  1234 	C$goldseq.c$23$1$122 ==.
                                   1235 ;	..\COMMON\goldseq.c:23: flash_unlock();
      003F7D 12 70 9E         [24] 1236 	lcall	_flash_unlock
                           000003  1237 	C$goldseq.c$27$1$122 ==.
                                   1238 ;	..\COMMON\goldseq.c:27: i = flash_pageerase(MEM_GOLD_SEQUENCES_START/* + 1*/);
      003F80 90 CC 00         [24] 1239 	mov	dptr,#0xcc00
      003F83 12 7A CA         [24] 1240 	lcall	_flash_pageerase
                           000009  1241 	C$goldseq.c$34$1$122 ==.
                                   1242 ;	..\COMMON\goldseq.c:34: flash_write(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100); // 0xF9A4
      003F86 75 67 A4         [24] 1243 	mov	_flash_write_PARM_2,#0xa4
      003F89 75 68 F9         [24] 1244 	mov	(_flash_write_PARM_2 + 1),#0xf9
      003F8C 90 CC 00         [24] 1245 	mov	dptr,#0xcc00
      003F8F 12 70 86         [24] 1246 	lcall	_flash_write
                           000015  1247 	C$goldseq.c$37$1$122 ==.
                                   1248 ;	..\COMMON\goldseq.c:37: flash_write(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000); // 0x15D8
      003F92 75 67 D8         [24] 1249 	mov	_flash_write_PARM_2,#0xd8
      003F95 75 68 15         [24] 1250 	mov	(_flash_write_PARM_2 + 1),#0x15
      003F98 90 CC 02         [24] 1251 	mov	dptr,#0xcc02
      003F9B 12 70 86         [24] 1252 	lcall	_flash_write
                           000021  1253 	C$goldseq.c$40$1$122 ==.
                                   1254 ;	..\COMMON\goldseq.c:40: flash_write(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000); // 0xF930
      003F9E 75 67 30         [24] 1255 	mov	_flash_write_PARM_2,#0x30
      003FA1 75 68 F9         [24] 1256 	mov	(_flash_write_PARM_2 + 1),#0xf9
      003FA4 90 CC 04         [24] 1257 	mov	dptr,#0xcc04
      003FA7 12 70 86         [24] 1258 	lcall	_flash_write
                           00002D  1259 	C$goldseq.c$42$1$122 ==.
                                   1260 ;	..\COMMON\goldseq.c:42: flash_write(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110); // 0x5A8E
      003FAA 75 67 8E         [24] 1261 	mov	_flash_write_PARM_2,#0x8e
      003FAD 75 68 5A         [24] 1262 	mov	(_flash_write_PARM_2 + 1),#0x5a
      003FB0 90 CC 06         [24] 1263 	mov	dptr,#0xcc06
      003FB3 12 70 86         [24] 1264 	lcall	_flash_write
                           000039  1265 	C$goldseq.c$45$1$122 ==.
                                   1266 ;	..\COMMON\goldseq.c:45: flash_write(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100); // 0x0094
      003FB6 75 67 94         [24] 1267 	mov	_flash_write_PARM_2,#0x94
      003FB9 75 68 00         [24] 1268 	mov	(_flash_write_PARM_2 + 1),#0x00
      003FBC 90 CC 08         [24] 1269 	mov	dptr,#0xcc08
      003FBF 12 70 86         [24] 1270 	lcall	_flash_write
                           000045  1271 	C$goldseq.c$47$1$122 ==.
                                   1272 ;	..\COMMON\goldseq.c:47: flash_write(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110); // 0x4F56
      003FC2 75 67 56         [24] 1273 	mov	_flash_write_PARM_2,#0x56
      003FC5 75 68 4F         [24] 1274 	mov	(_flash_write_PARM_2 + 1),#0x4f
      003FC8 90 CC 0A         [24] 1275 	mov	dptr,#0xcc0a
      003FCB 12 70 86         [24] 1276 	lcall	_flash_write
                           000051  1277 	C$goldseq.c$50$1$122 ==.
                                   1278 ;	..\COMMON\goldseq.c:50: flash_write(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100); // 0x853C
      003FCE 75 67 3C         [24] 1279 	mov	_flash_write_PARM_2,#0x3c
      003FD1 75 68 85         [24] 1280 	mov	(_flash_write_PARM_2 + 1),#0x85
      003FD4 90 CC 0C         [24] 1281 	mov	dptr,#0xcc0c
      003FD7 12 70 86         [24] 1282 	lcall	_flash_write
                           00005D  1283 	C$goldseq.c$52$1$122 ==.
                                   1284 ;	..\COMMON\goldseq.c:52: flash_write(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111); // 0x389F
      003FDA 75 67 9F         [24] 1285 	mov	_flash_write_PARM_2,#0x9f
      003FDD 75 68 38         [24] 1286 	mov	(_flash_write_PARM_2 + 1),#0x38
      003FE0 90 CC 0E         [24] 1287 	mov	dptr,#0xcc0e
      003FE3 12 70 86         [24] 1288 	lcall	_flash_write
                           000069  1289 	C$goldseq.c$55$1$122 ==.
                                   1290 ;	..\COMMON\goldseq.c:55: flash_write(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000); // 0x47E8
      003FE6 75 67 E8         [24] 1291 	mov	_flash_write_PARM_2,#0xe8
      003FE9 75 68 47         [24] 1292 	mov	(_flash_write_PARM_2 + 1),#0x47
      003FEC 90 CC 10         [24] 1293 	mov	dptr,#0xcc10
      003FEF 12 70 86         [24] 1294 	lcall	_flash_write
                           000075  1295 	C$goldseq.c$57$1$122 ==.
                                   1296 ;	..\COMMON\goldseq.c:57: flash_write(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011); // 0x037B
      003FF2 75 67 7B         [24] 1297 	mov	_flash_write_PARM_2,#0x7b
      003FF5 75 68 03         [24] 1298 	mov	(_flash_write_PARM_2 + 1),#0x03
      003FF8 90 CC 12         [24] 1299 	mov	dptr,#0xcc12
      003FFB 12 70 86         [24] 1300 	lcall	_flash_write
                           000081  1301 	C$goldseq.c$60$1$122 ==.
                                   1302 ;	..\COMMON\goldseq.c:60: flash_write(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010); // 0x2682
      003FFE 75 67 82         [24] 1303 	mov	_flash_write_PARM_2,#0x82
      004001 75 68 26         [24] 1304 	mov	(_flash_write_PARM_2 + 1),#0x26
      004004 90 CC 14         [24] 1305 	mov	dptr,#0xcc14
      004007 12 70 86         [24] 1306 	lcall	_flash_write
                           00008D  1307 	C$goldseq.c$62$1$122 ==.
                                   1308 ;	..\COMMON\goldseq.c:62: flash_write(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001); // 0x1E89
      00400A 75 67 89         [24] 1309 	mov	_flash_write_PARM_2,#0x89
      00400D 75 68 1E         [24] 1310 	mov	(_flash_write_PARM_2 + 1),#0x1e
      004010 90 CC 16         [24] 1311 	mov	dptr,#0xcc16
      004013 12 70 86         [24] 1312 	lcall	_flash_write
                           000099  1313 	C$goldseq.c$65$1$122 ==.
                                   1314 ;	..\COMMON\goldseq.c:65: flash_write(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111); //0x1637
      004016 75 67 37         [24] 1315 	mov	_flash_write_PARM_2,#0x37
      004019 75 68 16         [24] 1316 	mov	(_flash_write_PARM_2 + 1),#0x16
      00401C 90 CC 18         [24] 1317 	mov	dptr,#0xcc18
      00401F 12 70 86         [24] 1318 	lcall	_flash_write
                           0000A5  1319 	C$goldseq.c$67$1$122 ==.
                                   1320 ;	..\COMMON\goldseq.c:67: flash_write(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000); // 0x1070
      004022 75 67 70         [24] 1321 	mov	_flash_write_PARM_2,#0x70
      004025 75 68 10         [24] 1322 	mov	(_flash_write_PARM_2 + 1),#0x10
      004028 90 CC 1A         [24] 1323 	mov	dptr,#0xcc1a
      00402B 12 70 86         [24] 1324 	lcall	_flash_write
                           0000B1  1325 	C$goldseq.c$70$1$122 ==.
                                   1326 ;	..\COMMON\goldseq.c:70: flash_write(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101); // 0x8E6D
      00402E 75 67 6D         [24] 1327 	mov	_flash_write_PARM_2,#0x6d
      004031 75 68 8E         [24] 1328 	mov	(_flash_write_PARM_2 + 1),#0x8e
      004034 90 CC 1C         [24] 1329 	mov	dptr,#0xcc1c
      004037 12 70 86         [24] 1330 	lcall	_flash_write
                           0000BD  1331 	C$goldseq.c$72$1$122 ==.
                                   1332 ;	..\COMMON\goldseq.c:72: flash_write(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100); // 0x570C
      00403A 75 67 0C         [24] 1333 	mov	_flash_write_PARM_2,#0x0c
      00403D 75 68 57         [24] 1334 	mov	(_flash_write_PARM_2 + 1),#0x57
      004040 90 CC 1E         [24] 1335 	mov	dptr,#0xcc1e
      004043 12 70 86         [24] 1336 	lcall	_flash_write
                           0000C9  1337 	C$goldseq.c$75$1$122 ==.
                                   1338 ;	..\COMMON\goldseq.c:75: flash_write(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000); // 0xC240
      004046 75 67 40         [24] 1339 	mov	_flash_write_PARM_2,#0x40
      004049 75 68 C2         [24] 1340 	mov	(_flash_write_PARM_2 + 1),#0xc2
      00404C 90 CC 20         [24] 1341 	mov	dptr,#0xcc20
      00404F 12 70 86         [24] 1342 	lcall	_flash_write
                           0000D5  1343 	C$goldseq.c$77$1$122 ==.
                                   1344 ;	..\COMMON\goldseq.c:77: flash_write(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010); // 0x74B2
      004052 75 67 B2         [24] 1345 	mov	_flash_write_PARM_2,#0xb2
      004055 75 68 74         [24] 1346 	mov	(_flash_write_PARM_2 + 1),#0x74
      004058 90 CC 22         [24] 1347 	mov	dptr,#0xcc22
      00405B 12 70 86         [24] 1348 	lcall	_flash_write
                           0000E1  1349 	C$goldseq.c$80$1$122 ==.
                                   1350 ;	..\COMMON\goldseq.c:80: flash_write(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110); // 0xE456
      00405E 75 67 56         [24] 1351 	mov	_flash_write_PARM_2,#0x56
      004061 75 68 E4         [24] 1352 	mov	(_flash_write_PARM_2 + 1),#0xe4
      004064 90 CC 24         [24] 1353 	mov	dptr,#0xcc24
      004067 12 70 86         [24] 1354 	lcall	_flash_write
                           0000ED  1355 	C$goldseq.c$82$1$122 ==.
                                   1356 ;	..\COMMON\goldseq.c:82: flash_write(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101); // 0x256D
      00406A 75 67 6D         [24] 1357 	mov	_flash_write_PARM_2,#0x6d
      00406D 75 68 25         [24] 1358 	mov	(_flash_write_PARM_2 + 1),#0x25
      004070 90 CC 26         [24] 1359 	mov	dptr,#0xcc26
      004073 12 70 86         [24] 1360 	lcall	_flash_write
                           0000F9  1361 	C$goldseq.c$85$1$122 ==.
                                   1362 ;	..\COMMON\goldseq.c:85: flash_write(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101); // 0x775D
      004076 75 67 5D         [24] 1363 	mov	_flash_write_PARM_2,#0x5d
      004079 75 68 77         [24] 1364 	mov	(_flash_write_PARM_2 + 1),#0x77
      00407C 90 CC 28         [24] 1365 	mov	dptr,#0xcc28
      00407F 12 70 86         [24] 1366 	lcall	_flash_write
                           000105  1367 	C$goldseq.c$87$1$122 ==.
                                   1368 ;	..\COMMON\goldseq.c:87: flash_write(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010); // 0x0D82
      004082 75 67 82         [24] 1369 	mov	_flash_write_PARM_2,#0x82
      004085 75 68 0D         [24] 1370 	mov	(_flash_write_PARM_2 + 1),#0x0d
      004088 90 CC 2A         [24] 1371 	mov	dptr,#0xcc2a
      00408B 12 70 86         [24] 1372 	lcall	_flash_write
                           000111  1373 	C$goldseq.c$90$1$122 ==.
                                   1374 ;	..\COMMON\goldseq.c:90: flash_write(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000); // 0xBED8
      00408E 75 67 D8         [24] 1375 	mov	_flash_write_PARM_2,#0xd8
      004091 75 68 BE         [24] 1376 	mov	(_flash_write_PARM_2 + 1),#0xbe
      004094 90 CC 2C         [24] 1377 	mov	dptr,#0xcc2c
      004097 12 70 86         [24] 1378 	lcall	_flash_write
                           00011D  1379 	C$goldseq.c$92$1$122 ==.
                                   1380 ;	..\COMMON\goldseq.c:92: flash_write(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101); // 0x59F5
      00409A 75 67 F5         [24] 1381 	mov	_flash_write_PARM_2,#0xf5
      00409D 75 68 59         [24] 1382 	mov	(_flash_write_PARM_2 + 1),#0x59
      0040A0 90 CC 2E         [24] 1383 	mov	dptr,#0xcc2e
      0040A3 12 70 86         [24] 1384 	lcall	_flash_write
                           000129  1385 	C$goldseq.c$95$1$122 ==.
                                   1386 ;	..\COMMON\goldseq.c:95: flash_write(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010);
      0040A6 75 67 1A         [24] 1387 	mov	_flash_write_PARM_2,#0x1a
      0040A9 75 68 5A         [24] 1388 	mov	(_flash_write_PARM_2 + 1),#0x5a
      0040AC 90 CC 30         [24] 1389 	mov	dptr,#0xcc30
      0040AF 12 70 86         [24] 1390 	lcall	_flash_write
                           000135  1391 	C$goldseq.c$97$1$122 ==.
                                   1392 ;	..\COMMON\goldseq.c:97: flash_write(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110);
      0040B2 75 67 CE         [24] 1393 	mov	_flash_write_PARM_2,#0xce
      0040B5 75 68 33         [24] 1394 	mov	(_flash_write_PARM_2 + 1),#0x33
      0040B8 90 CC 32         [24] 1395 	mov	dptr,#0xcc32
      0040BB 12 70 86         [24] 1396 	lcall	_flash_write
                           000141  1397 	C$goldseq.c$100$1$122 ==.
                                   1398 ;	..\COMMON\goldseq.c:100: flash_write(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011);
      0040BE 75 67 7B         [24] 1399 	mov	_flash_write_PARM_2,#0x7b
      0040C1 75 68 A8         [24] 1400 	mov	(_flash_write_PARM_2 + 1),#0xa8
      0040C4 90 CC 34         [24] 1401 	mov	dptr,#0xcc34
      0040C7 12 70 86         [24] 1402 	lcall	_flash_write
                           00014D  1403 	C$goldseq.c$102$1$122 ==.
                                   1404 ;	..\COMMON\goldseq.c:102: flash_write(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011);
      0040CA 75 67 D3         [24] 1405 	mov	_flash_write_PARM_2,#0xd3
      0040CD 75 68 06         [24] 1406 	mov	(_flash_write_PARM_2 + 1),#0x06
      0040D0 90 CC 36         [24] 1407 	mov	dptr,#0xcc36
      0040D3 12 70 86         [24] 1408 	lcall	_flash_write
                           000159  1409 	C$goldseq.c$105$1$122 ==.
                                   1410 ;	..\COMMON\goldseq.c:105: flash_write(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011); // 0x514B
      0040D6 75 67 4B         [24] 1411 	mov	_flash_write_PARM_2,#0x4b
      0040D9 75 68 51         [24] 1412 	mov	(_flash_write_PARM_2 + 1),#0x51
      0040DC 90 CC 38         [24] 1413 	mov	dptr,#0xcc38
      0040DF 12 70 86         [24] 1414 	lcall	_flash_write
                           000165  1415 	C$goldseq.c$107$1$122 ==.
                                   1416 ;	..\COMMON\goldseq.c:107: flash_write(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101); // 0xB8BA
      0040E2 75 67 5D         [24] 1417 	mov	_flash_write_PARM_2,#0x5d
      0040E5 75 68 5C         [24] 1418 	mov	(_flash_write_PARM_2 + 1),#0x5c
      0040E8 90 CC 3A         [24] 1419 	mov	dptr,#0xcc3a
      0040EB 12 70 86         [24] 1420 	lcall	_flash_write
                           000171  1421 	C$goldseq.c$110$1$122 ==.
                                   1422 ;	..\COMMON\goldseq.c:110: flash_write(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011);
      0040EE 75 67 D3         [24] 1423 	mov	_flash_write_PARM_2,#0xd3
      0040F1 75 68 2D         [24] 1424 	mov	(_flash_write_PARM_2 + 1),#0x2d
      0040F4 90 CC 3C         [24] 1425 	mov	dptr,#0xcc3c
      0040F7 12 70 86         [24] 1426 	lcall	_flash_write
                           00017D  1427 	C$goldseq.c$112$1$122 ==.
                                   1428 ;	..\COMMON\goldseq.c:112: flash_write(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010);
      0040FA 75 67 1A         [24] 1429 	mov	_flash_write_PARM_2,#0x1a
      0040FD 75 68 71         [24] 1430 	mov	(_flash_write_PARM_2 + 1),#0x71
      004100 90 CC 3E         [24] 1431 	mov	dptr,#0xcc3e
      004103 12 70 86         [24] 1432 	lcall	_flash_write
                           000189  1433 	C$goldseq.c$115$1$122 ==.
                                   1434 ;	..\COMMON\goldseq.c:115: flash_write(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111);
      004106 75 67 9F         [24] 1435 	mov	_flash_write_PARM_2,#0x9f
      004109 75 68 93         [24] 1436 	mov	(_flash_write_PARM_2 + 1),#0x93
      00410C 90 CC 40         [24] 1437 	mov	dptr,#0xcc40
      00410F 12 70 86         [24] 1438 	lcall	_flash_write
                           000195  1439 	C$goldseq.c$117$1$122 ==.
                                   1440 ;	..\COMMON\goldseq.c:117: flash_write(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001);
      004112 75 67 B9         [24] 1441 	mov	_flash_write_PARM_2,#0xb9
      004115 75 68 67         [24] 1442 	mov	(_flash_write_PARM_2 + 1),#0x67
      004118 90 CC 42         [24] 1443 	mov	dptr,#0xcc42
      00411B 12 70 86         [24] 1444 	lcall	_flash_write
                           0001A1  1445 	C$goldseq.c$120$1$122 ==.
                                   1446 ;	..\COMMON\goldseq.c:120: flash_write(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001);
      00411E 75 67 B9         [24] 1447 	mov	_flash_write_PARM_2,#0xb9
      004121 75 68 4C         [24] 1448 	mov	(_flash_write_PARM_2 + 1),#0x4c
      004124 90 CC 44         [24] 1449 	mov	dptr,#0xcc44
      004127 12 70 86         [24] 1450 	lcall	_flash_write
                           0001AD  1451 	C$goldseq.c$122$1$122 ==.
                                   1452 ;	..\COMMON\goldseq.c:122: flash_write(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000);
      00412A 75 67 E8         [24] 1453 	mov	_flash_write_PARM_2,#0xe8
      00412D 75 68 6C         [24] 1454 	mov	(_flash_write_PARM_2 + 1),#0x6c
      004130 90 CC 46         [24] 1455 	mov	dptr,#0xcc46
      004133 12 70 86         [24] 1456 	lcall	_flash_write
                           0001B9  1457 	C$goldseq.c$125$1$122 ==.
                                   1458 ;	..\COMMON\goldseq.c:125: flash_write(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010); // 0xA32A
      004136 75 67 2A         [24] 1459 	mov	_flash_write_PARM_2,#0x2a
      004139 75 68 A3         [24] 1460 	mov	(_flash_write_PARM_2 + 1),#0xa3
      00413C 90 CC 48         [24] 1461 	mov	dptr,#0xcc48
      00413F 12 70 86         [24] 1462 	lcall	_flash_write
                           0001C5  1463 	C$goldseq.c$127$1$122 ==.
                                   1464 ;	..\COMMON\goldseq.c:127: flash_write(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000); // 0x6940
      004142 75 67 40         [24] 1465 	mov	_flash_write_PARM_2,#0x40
      004145 75 68 69         [24] 1466 	mov	(_flash_write_PARM_2 + 1),#0x69
      004148 90 CC 4A         [24] 1467 	mov	dptr,#0xcc4a
      00414B 12 70 86         [24] 1468 	lcall	_flash_write
                           0001D1  1469 	C$goldseq.c$130$1$122 ==.
                                   1470 ;	..\COMMON\goldseq.c:130: flash_write(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011);
      00414E 75 67 E3         [24] 1471 	mov	_flash_write_PARM_2,#0xe3
      004151 75 68 D4         [24] 1472 	mov	(_flash_write_PARM_2 + 1),#0xd4
      004154 90 CC 4C         [24] 1473 	mov	dptr,#0xcc4c
      004157 12 70 86         [24] 1474 	lcall	_flash_write
                           0001DD  1475 	C$goldseq.c$132$1$122 ==.
                                   1476 ;	..\COMMON\goldseq.c:132: flash_write(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100);
      00415A 75 67 94         [24] 1477 	mov	_flash_write_PARM_2,#0x94
      00415D 75 68 2B         [24] 1478 	mov	(_flash_write_PARM_2 + 1),#0x2b
      004160 90 CC 4E         [24] 1479 	mov	dptr,#0xcc4e
      004163 12 70 86         [24] 1480 	lcall	_flash_write
                           0001E9  1481 	C$goldseq.c$135$1$122 ==.
                                   1482 ;	..\COMMON\goldseq.c:135: flash_write(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111);
      004166 75 67 07         [24] 1483 	mov	_flash_write_PARM_2,#0x07
      004169 75 68 EF         [24] 1484 	mov	(_flash_write_PARM_2 + 1),#0xef
      00416C 90 CC 50         [24] 1485 	mov	dptr,#0xcc50
      00416F 12 70 86         [24] 1486 	lcall	_flash_write
                           0001F5  1487 	C$goldseq.c$137$1$122 ==.
                                   1488 ;	..\COMMON\goldseq.c:137: flash_write(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110); // 0x4AFE
      004172 75 67 FE         [24] 1489 	mov	_flash_write_PARM_2,#0xfe
      004175 75 68 4A         [24] 1490 	mov	(_flash_write_PARM_2 + 1),#0x4a
      004178 90 CC 52         [24] 1491 	mov	dptr,#0xcc52
      00417B 12 70 86         [24] 1492 	lcall	_flash_write
                           000201  1493 	C$goldseq.c$140$1$122 ==.
                                   1494 ;	..\COMMON\goldseq.c:140: flash_write(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101); // 0xF2F5
      00417E 75 67 F5         [24] 1495 	mov	_flash_write_PARM_2,#0xf5
      004181 75 68 F2         [24] 1496 	mov	(_flash_write_PARM_2 + 1),#0xf2
      004184 90 CC 54         [24] 1497 	mov	dptr,#0xcc54
      004187 12 70 86         [24] 1498 	lcall	_flash_write
                           00020D  1499 	C$goldseq.c$142$1$122 ==.
                                   1500 ;	..\COMMON\goldseq.c:142: flash_write(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011); // 0x7A4B
      00418A 75 67 4B         [24] 1501 	mov	_flash_write_PARM_2,#0x4b
      00418D 75 68 7A         [24] 1502 	mov	(_flash_write_PARM_2 + 1),#0x7a
      004190 90 CC 56         [24] 1503 	mov	dptr,#0xcc56
      004193 12 70 86         [24] 1504 	lcall	_flash_write
                           000219  1505 	C$goldseq.c$144$1$122 ==.
                                   1506 ;	..\COMMON\goldseq.c:144: flash_write(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100); // 0x7C0C
      004196 75 67 0C         [24] 1507 	mov	_flash_write_PARM_2,#0x0c
      004199 75 68 7C         [24] 1508 	mov	(_flash_write_PARM_2 + 1),#0x7c
      00419C 90 CC 58         [24] 1509 	mov	dptr,#0xcc58
      00419F 12 70 86         [24] 1510 	lcall	_flash_write
                           000225  1511 	C$goldseq.c$146$1$122 ==.
                                   1512 ;	..\COMMON\goldseq.c:146: flash_write(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001); //0x6211
      0041A2 75 67 11         [24] 1513 	mov	_flash_write_PARM_2,#0x11
      0041A5 75 68 62         [24] 1514 	mov	(_flash_write_PARM_2 + 1),#0x62
      0041A8 90 CC 5A         [24] 1515 	mov	dptr,#0xcc5a
      0041AB 12 70 86         [24] 1516 	lcall	_flash_write
                           000231  1517 	C$goldseq.c$149$1$122 ==.
                                   1518 ;	..\COMMON\goldseq.c:149: flash_write(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000); // 0x3B70
      0041AE 75 67 70         [24] 1519 	mov	_flash_write_PARM_2,#0x70
      0041B1 75 68 3B         [24] 1520 	mov	(_flash_write_PARM_2 + 1),#0x3b
      0041B4 90 CC 5C         [24] 1521 	mov	dptr,#0xcc5c
      0041B7 12 70 86         [24] 1522 	lcall	_flash_write
                           00023D  1523 	C$goldseq.c$151$1$122 ==.
                                   1524 ;	..\COMMON\goldseq.c:151: flash_write(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100); // 0x2E3C
      0041BA 75 67 3C         [24] 1525 	mov	_flash_write_PARM_2,#0x3c
      0041BD 75 68 2E         [24] 1526 	mov	(_flash_write_PARM_2 + 1),#0x2e
      0041C0 90 CC 5E         [24] 1527 	mov	dptr,#0xcc5e
      0041C3 12 70 86         [24] 1528 	lcall	_flash_write
                           000249  1529 	C$goldseq.c$154$1$122 ==.
                                   1530 ;	..\COMMON\goldseq.c:154: flash_write(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110); // 0x98CE
      0041C6 75 67 CE         [24] 1531 	mov	_flash_write_PARM_2,#0xce
      0041C9 75 68 98         [24] 1532 	mov	(_flash_write_PARM_2 + 1),#0x98
      0041CC 90 CC 60         [24] 1533 	mov	dptr,#0xcc60
      0041CF 12 70 86         [24] 1534 	lcall	_flash_write
                           000255  1535 	C$goldseq.c$156$1$122 ==.
                                   1536 ;	..\COMMON\goldseq.c:156: flash_write(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010);
      0041D2 75 67 2A         [24] 1537 	mov	_flash_write_PARM_2,#0x2a
      0041D5 75 68 08         [24] 1538 	mov	(_flash_write_PARM_2 + 1),#0x08
      0041D8 90 CC 62         [24] 1539 	mov	dptr,#0xcc62
      0041DB 12 70 86         [24] 1540 	lcall	_flash_write
                           000261  1541 	C$goldseq.c$159$1$122 ==.
                                   1542 ;	..\COMMON\goldseq.c:159: flash_write(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001); // 0xC911
      0041DE 75 67 11         [24] 1543 	mov	_flash_write_PARM_2,#0x11
      0041E1 75 68 C9         [24] 1544 	mov	(_flash_write_PARM_2 + 1),#0xc9
      0041E4 90 CC 64         [24] 1545 	mov	dptr,#0xcc64
      0041E7 12 70 86         [24] 1546 	lcall	_flash_write
                           00026D  1547 	C$goldseq.c$161$1$122 ==.
                                   1548 ;	..\COMMON\goldseq.c:161: flash_write(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001); // 0x1B21
      0041EA 75 67 21         [24] 1549 	mov	_flash_write_PARM_2,#0x21
      0041ED 75 68 1B         [24] 1550 	mov	(_flash_write_PARM_2 + 1),#0x1b
      0041F0 90 CC 66         [24] 1551 	mov	dptr,#0xcc66
      0041F3 12 70 86         [24] 1552 	lcall	_flash_write
                           000279  1553 	C$goldseq.c$164$1$122 ==.
                                   1554 ;	..\COMMON\goldseq.c:164: flash_write(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110); // 0x61FE
      0041F6 75 67 FE         [24] 1555 	mov	_flash_write_PARM_2,#0xfe
      0041F9 75 68 61         [24] 1556 	mov	(_flash_write_PARM_2 + 1),#0x61
      0041FC 90 CC 68         [24] 1557 	mov	dptr,#0xcc68
      0041FF 12 70 86         [24] 1558 	lcall	_flash_write
                           000285  1559 	C$goldseq.c$166$1$122 ==.
                                   1560 ;	..\COMMON\goldseq.c:166: flash_write(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100); // 0x52A4
      004202 75 67 A4         [24] 1561 	mov	_flash_write_PARM_2,#0xa4
      004205 75 68 52         [24] 1562 	mov	(_flash_write_PARM_2 + 1),#0x52
      004208 90 CC 6A         [24] 1563 	mov	dptr,#0xcc6a
      00420B 12 70 86         [24] 1564 	lcall	_flash_write
                           000291  1565 	C$goldseq.c$169$1$122 ==.
                                   1566 ;	..\COMMON\goldseq.c:169: flash_write(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001);
      00420E 75 67 89         [24] 1567 	mov	_flash_write_PARM_2,#0x89
      004211 75 68 B5         [24] 1568 	mov	(_flash_write_PARM_2 + 1),#0xb5
      004214 90 CC 6C         [24] 1569 	mov	dptr,#0xcc6c
      004217 12 70 86         [24] 1570 	lcall	_flash_write
                           00029D  1571 	C$goldseq.c$171$1$122 ==.
                                   1572 ;	..\COMMON\goldseq.c:171: flash_write(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110);
      00421A 75 67 66         [24] 1573 	mov	_flash_write_PARM_2,#0x66
      00421D 75 68 36         [24] 1574 	mov	(_flash_write_PARM_2 + 1),#0x36
      004220 90 CC 6E         [24] 1575 	mov	dptr,#0xcc6e
      004223 12 70 86         [24] 1576 	lcall	_flash_write
                           0002A9  1577 	C$goldseq.c$174$1$122 ==.
                                   1578 ;	..\COMMON\goldseq.c:174: flash_write(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010);
      004226 75 67 B2         [24] 1579 	mov	_flash_write_PARM_2,#0xb2
      004229 75 68 DF         [24] 1580 	mov	(_flash_write_PARM_2 + 1),#0xdf
      00422C 90 CC 70         [24] 1581 	mov	dptr,#0xcc70
      00422F 12 70 86         [24] 1582 	lcall	_flash_write
                           0002B5  1583 	C$goldseq.c$176$1$122 ==.
                                   1584 ;	..\COMMON\goldseq.c:176: flash_write(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111);
      004232 75 67 07         [24] 1585 	mov	_flash_write_PARM_2,#0x07
      004235 75 68 44         [24] 1586 	mov	(_flash_write_PARM_2 + 1),#0x44
      004238 90 CC 72         [24] 1587 	mov	dptr,#0xcc72
      00423B 12 70 86         [24] 1588 	lcall	_flash_write
                           0002C1  1589 	C$goldseq.c$179$1$122 ==.
                                   1590 ;	..\COMMON\goldseq.c:179: flash_write(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111);
      00423E 75 67 AF         [24] 1591 	mov	_flash_write_PARM_2,#0xaf
      004241 75 68 6A         [24] 1592 	mov	(_flash_write_PARM_2 + 1),#0x6a
      004244 90 CC 74         [24] 1593 	mov	dptr,#0xcc74
      004247 12 70 86         [24] 1594 	lcall	_flash_write
                           0002CD  1595 	C$goldseq.c$181$1$122 ==.
                                   1596 ;	..\COMMON\goldseq.c:181: flash_write(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111);
      00424A 75 67 37         [24] 1597 	mov	_flash_write_PARM_2,#0x37
      00424D 75 68 3D         [24] 1598 	mov	(_flash_write_PARM_2 + 1),#0x3d
      004250 90 CC 76         [24] 1599 	mov	dptr,#0xcc76
      004253 12 70 86         [24] 1600 	lcall	_flash_write
                           0002D9  1601 	C$goldseq.c$184$1$122 ==.
                                   1602 ;	..\COMMON\goldseq.c:184: flash_write(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001);
      004256 75 67 21         [24] 1603 	mov	_flash_write_PARM_2,#0x21
      004259 75 68 30         [24] 1604 	mov	(_flash_write_PARM_2 + 1),#0x30
      00425C 90 CC 78         [24] 1605 	mov	dptr,#0xcc78
      00425F 12 70 86         [24] 1606 	lcall	_flash_write
                           0002E5  1607 	C$goldseq.c$186$1$122 ==.
                                   1608 ;	..\COMMON\goldseq.c:186: flash_write(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111);
      004262 75 67 AF         [24] 1609 	mov	_flash_write_PARM_2,#0xaf
      004265 75 68 41         [24] 1610 	mov	(_flash_write_PARM_2 + 1),#0x41
      004268 90 CC 7A         [24] 1611 	mov	dptr,#0xcc7a
      00426B 12 70 86         [24] 1612 	lcall	_flash_write
                           0002F1  1613 	C$goldseq.c$189$1$122 ==.
                                   1614 ;	..\COMMON\goldseq.c:189: flash_write(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110);
      00426E 75 67 66         [24] 1615 	mov	_flash_write_PARM_2,#0x66
      004271 75 68 1D         [24] 1616 	mov	(_flash_write_PARM_2 + 1),#0x1d
      004274 90 CC 7C         [24] 1617 	mov	dptr,#0xcc7c
      004277 12 70 86         [24] 1618 	lcall	_flash_write
                           0002FD  1619 	C$goldseq.c$191$1$122 ==.
                                   1620 ;	..\COMMON\goldseq.c:191: flash_write(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011);
      00427A 75 67 E3         [24] 1621 	mov	_flash_write_PARM_2,#0xe3
      00427D 75 68 7F         [24] 1622 	mov	(_flash_write_PARM_2 + 1),#0x7f
      004280 90 CC 7E         [24] 1623 	mov	dptr,#0xcc7e
      004283 12 70 86         [24] 1624 	lcall	_flash_write
                           000309  1625 	C$goldseq.c$193$1$122 ==.
                                   1626 ;	..\COMMON\goldseq.c:193: flash_write(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101);
      004286 75 67 C5         [24] 1627 	mov	_flash_write_PARM_2,#0xc5
      004289 75 68 0B         [24] 1628 	mov	(_flash_write_PARM_2 + 1),#0x0b
      00428C 90 CC 80         [24] 1629 	mov	dptr,#0xcc80
      00428F 12 70 86         [24] 1630 	lcall	_flash_write
                           000315  1631 	C$goldseq.c$195$1$122 ==.
                                   1632 ;	..\COMMON\goldseq.c:195: flash_write(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101);
      004292 75 67 C5         [24] 1633 	mov	_flash_write_PARM_2,#0xc5
      004295 75 68 20         [24] 1634 	mov	(_flash_write_PARM_2 + 1),#0x20
      004298 90 CC 82         [24] 1635 	mov	dptr,#0xcc82
      00429B 12 70 86         [24] 1636 	lcall	_flash_write
                           000321  1637 	C$goldseq.c$197$1$122 ==.
                                   1638 ;	..\COMMON\goldseq.c:197: flash_lock();
      00429E 12 7A 6E         [24] 1639 	lcall	_flash_lock
                           000324  1640 	C$goldseq.c$200$1$122 ==.
                           000324  1641 	XG$GOLDSEQUENCE_Init$0$0 ==.
      0042A1 22               [24] 1642 	ret
                                   1643 ;------------------------------------------------------------
                                   1644 ;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
                                   1645 ;------------------------------------------------------------
                                   1646 ;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_124'
                                   1647 ;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_124'
                                   1648 ;------------------------------------------------------------
                           000325  1649 	G$GOLDSEQUENCE_Obtain$0$0 ==.
                           000325  1650 	C$goldseq.c$209$1$122 ==.
                                   1651 ;	..\COMMON\goldseq.c:209: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
                                   1652 ;	-----------------------------------------
                                   1653 ;	 function GOLDSEQUENCE_Obtain
                                   1654 ;	-----------------------------------------
      0042A2                       1655 _GOLDSEQUENCE_Obtain:
                           000325  1656 	C$goldseq.c$214$1$124 ==.
                                   1657 ;	..\COMMON\goldseq.c:214: Rnd = RNGBYTE;
      0042A2 90 70 81         [24] 1658 	mov	dptr,#_RNGBYTE
      0042A5 E0               [24] 1659 	movx	a,@dptr
      0042A6 FF               [12] 1660 	mov	r7,a
      0042A7 7E 00            [12] 1661 	mov	r6,#0x00
                           00032C  1662 	C$goldseq.c$215$1$124 ==.
                                   1663 ;	..\COMMON\goldseq.c:215: flash_unlock();
      0042A9 C0 07            [24] 1664 	push	ar7
      0042AB C0 06            [24] 1665 	push	ar6
      0042AD 12 70 9E         [24] 1666 	lcall	_flash_unlock
                           000333  1667 	C$goldseq.c$217$1$124 ==.
                                   1668 ;	..\COMMON\goldseq.c:217: delay_ms(50);
      0042B0 90 00 32         [24] 1669 	mov	dptr,#0x0032
      0042B3 12 44 39         [24] 1670 	lcall	_delay_ms
      0042B6 D0 06            [24] 1671 	pop	ar6
      0042B8 D0 07            [24] 1672 	pop	ar7
                           00033D  1673 	C$goldseq.c$219$1$124 ==.
                                   1674 ;	..\COMMON\goldseq.c:219: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
      0042BA 75 6E 21         [24] 1675 	mov	__moduint_PARM_2,#0x21
      0042BD 75 6F 00         [24] 1676 	mov	(__moduint_PARM_2 + 1),#0x00
      0042C0 8F 82            [24] 1677 	mov	dpl,r7
      0042C2 8E 83            [24] 1678 	mov	dph,r6
      0042C4 12 73 7C         [24] 1679 	lcall	__moduint
      0042C7 AE 82            [24] 1680 	mov	r6,dpl
      0042C9 E5 83            [12] 1681 	mov	a,dph
      0042CB CE               [12] 1682 	xch	a,r6
      0042CC 25 E0            [12] 1683 	add	a,acc
      0042CE CE               [12] 1684 	xch	a,r6
      0042CF 33               [12] 1685 	rlc	a
      0042D0 CE               [12] 1686 	xch	a,r6
      0042D1 25 E0            [12] 1687 	add	a,acc
      0042D3 CE               [12] 1688 	xch	a,r6
      0042D4 33               [12] 1689 	rlc	a
      0042D5 FF               [12] 1690 	mov	r7,a
                           000359  1691 	C$goldseq.c$223$1$124 ==.
                                   1692 ;	..\COMMON\goldseq.c:223: RetVal = ((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd+2))<<16;
      0042D6 74 02            [12] 1693 	mov	a,#0x02
      0042D8 2E               [12] 1694 	add	a,r6
      0042D9 F5 82            [12] 1695 	mov	dpl,a
      0042DB 74 CC            [12] 1696 	mov	a,#0xcc
      0042DD 3F               [12] 1697 	addc	a,r7
      0042DE F5 83            [12] 1698 	mov	dph,a
      0042E0 C0 07            [24] 1699 	push	ar7
      0042E2 C0 06            [24] 1700 	push	ar6
      0042E4 12 75 5E         [24] 1701 	lcall	_flash_read
      0042E7 AC 82            [24] 1702 	mov	r4,dpl
      0042E9 AD 83            [24] 1703 	mov	r5,dph
      0042EB D0 06            [24] 1704 	pop	ar6
      0042ED D0 07            [24] 1705 	pop	ar7
      0042EF 8D 02            [24] 1706 	mov	ar2,r5
      0042F1 8C 03            [24] 1707 	mov	ar3,r4
      0042F3 7D 00            [12] 1708 	mov	r5,#0x00
      0042F5 7C 00            [12] 1709 	mov	r4,#0x00
                           00037A  1710 	C$goldseq.c$225$1$124 ==.
                                   1711 ;	..\COMMON\goldseq.c:225: RetVal |= (((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd)) & 0x0000FFFF)/*<<1*/;
      0042F7 8E 82            [24] 1712 	mov	dpl,r6
      0042F9 74 CC            [12] 1713 	mov	a,#0xcc
      0042FB 2F               [12] 1714 	add	a,r7
      0042FC F5 83            [12] 1715 	mov	dph,a
      0042FE C0 05            [24] 1716 	push	ar5
      004300 C0 04            [24] 1717 	push	ar4
      004302 C0 03            [24] 1718 	push	ar3
      004304 C0 02            [24] 1719 	push	ar2
      004306 12 75 5E         [24] 1720 	lcall	_flash_read
      004309 AE 82            [24] 1721 	mov	r6,dpl
      00430B AF 83            [24] 1722 	mov	r7,dph
      00430D D0 02            [24] 1723 	pop	ar2
      00430F D0 03            [24] 1724 	pop	ar3
      004311 D0 04            [24] 1725 	pop	ar4
      004313 D0 05            [24] 1726 	pop	ar5
      004315 8E 00            [24] 1727 	mov	ar0,r6
      004317 8F 01            [24] 1728 	mov	ar1,r7
      004319 E4               [12] 1729 	clr	a
      00431A FE               [12] 1730 	mov	r6,a
      00431B FF               [12] 1731 	mov	r7,a
      00431C E8               [12] 1732 	mov	a,r0
      00431D 42 04            [12] 1733 	orl	ar4,a
      00431F E9               [12] 1734 	mov	a,r1
      004320 42 05            [12] 1735 	orl	ar5,a
      004322 EE               [12] 1736 	mov	a,r6
      004323 42 03            [12] 1737 	orl	ar3,a
      004325 EF               [12] 1738 	mov	a,r7
      004326 42 02            [12] 1739 	orl	ar2,a
                           0003AB  1740 	C$goldseq.c$236$1$124 ==.
                                   1741 ;	..\COMMON\goldseq.c:236: flash_lock();
      004328 C0 05            [24] 1742 	push	ar5
      00432A C0 04            [24] 1743 	push	ar4
      00432C C0 03            [24] 1744 	push	ar3
      00432E C0 02            [24] 1745 	push	ar2
      004330 12 7A 6E         [24] 1746 	lcall	_flash_lock
      004333 D0 02            [24] 1747 	pop	ar2
      004335 D0 03            [24] 1748 	pop	ar3
      004337 D0 04            [24] 1749 	pop	ar4
      004339 D0 05            [24] 1750 	pop	ar5
                           0003BE  1751 	C$goldseq.c$237$1$124 ==.
                                   1752 ;	..\COMMON\goldseq.c:237: return(RetVal);
      00433B 8C 82            [24] 1753 	mov	dpl,r4
      00433D 8D 83            [24] 1754 	mov	dph,r5
      00433F 8B F0            [24] 1755 	mov	b,r3
      004341 EA               [12] 1756 	mov	a,r2
                           0003C5  1757 	C$goldseq.c$238$1$124 ==.
                           0003C5  1758 	XG$GOLDSEQUENCE_Obtain$0$0 ==.
      004342 22               [24] 1759 	ret
                                   1760 	.area CSEG    (CODE)
                                   1761 	.area CONST   (CODE)
                                   1762 	.area XINIT   (CODE)
                                   1763 	.area CABS    (ABS,CODE)
