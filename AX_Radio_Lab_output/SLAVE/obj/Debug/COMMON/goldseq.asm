;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW64)
;--------------------------------------------------------
	.module goldseq
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _GOLDSEQUENCE_Obtain
	.globl _GOLDSEQUENCE_Init
	.globl _delay_ms
	.globl _dbglink_writehex32
	.globl _dbglink_writehex16
	.globl _dbglink_writestr
	.globl _dbglink_tx
	.globl _flash_read
	.globl _flash_write
	.globl _flash_pageerase
	.globl _flash_lock
	.globl _flash_unlock
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _RNGMODE
	.globl _RNGCLKSRC1
	.globl _RNGCLKSRC0
	.globl _RNGBYTE
	.globl _AESOUTADDR
	.globl _AESOUTADDR1
	.globl _AESOUTADDR0
	.globl _AESMODE
	.globl _AESKEYADDR
	.globl _AESKEYADDR1
	.globl _AESKEYADDR0
	.globl _AESINADDR
	.globl _AESINADDR1
	.globl _AESINADDR0
	.globl _AESCURBLOCK
	.globl _AESCONFIG
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$DPH1$0$0 == 0x0085
_DPH1	=	0x0085
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPL1$0$0 == 0x0084
_DPL1	=	0x0084
G$DPTR0$0$0 == 0x8382
_DPTR0	=	0x8382
G$DPTR1$0$0 == 0x8584
_DPTR1	=	0x8584
G$DPS$0$0 == 0x0086
_DPS	=	0x0086
G$E2IE$0$0 == 0x00a0
_E2IE	=	0x00a0
G$E2IP$0$0 == 0x00c0
_E2IP	=	0x00c0
G$EIE$0$0 == 0x0098
_EIE	=	0x0098
G$EIP$0$0 == 0x00b0
_EIP	=	0x00b0
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$XPAGE$0$0 == 0x00d9
_XPAGE	=	0x00d9
G$_XPAGE$0$0 == 0x00d9
__XPAGE	=	0x00d9
G$ADCCH0CONFIG$0$0 == 0x00ca
_ADCCH0CONFIG	=	0x00ca
G$ADCCH1CONFIG$0$0 == 0x00cb
_ADCCH1CONFIG	=	0x00cb
G$ADCCH2CONFIG$0$0 == 0x00d2
_ADCCH2CONFIG	=	0x00d2
G$ADCCH3CONFIG$0$0 == 0x00d3
_ADCCH3CONFIG	=	0x00d3
G$ADCCLKSRC$0$0 == 0x00d1
_ADCCLKSRC	=	0x00d1
G$ADCCONV$0$0 == 0x00c9
_ADCCONV	=	0x00c9
G$ANALOGCOMP$0$0 == 0x00e1
_ANALOGCOMP	=	0x00e1
G$CLKCON$0$0 == 0x00c6
_CLKCON	=	0x00c6
G$CLKSTAT$0$0 == 0x00c7
_CLKSTAT	=	0x00c7
G$CODECONFIG$0$0 == 0x0097
_CODECONFIG	=	0x0097
G$DBGLNKBUF$0$0 == 0x00e3
_DBGLNKBUF	=	0x00e3
G$DBGLNKSTAT$0$0 == 0x00e2
_DBGLNKSTAT	=	0x00e2
G$DIRA$0$0 == 0x0089
_DIRA	=	0x0089
G$DIRB$0$0 == 0x008a
_DIRB	=	0x008a
G$DIRC$0$0 == 0x008b
_DIRC	=	0x008b
G$DIRR$0$0 == 0x008e
_DIRR	=	0x008e
G$PINA$0$0 == 0x00c8
_PINA	=	0x00c8
G$PINB$0$0 == 0x00e8
_PINB	=	0x00e8
G$PINC$0$0 == 0x00f8
_PINC	=	0x00f8
G$PINR$0$0 == 0x008d
_PINR	=	0x008d
G$PORTA$0$0 == 0x0080
_PORTA	=	0x0080
G$PORTB$0$0 == 0x0088
_PORTB	=	0x0088
G$PORTC$0$0 == 0x0090
_PORTC	=	0x0090
G$PORTR$0$0 == 0x008c
_PORTR	=	0x008c
G$IC0CAPT0$0$0 == 0x00ce
_IC0CAPT0	=	0x00ce
G$IC0CAPT1$0$0 == 0x00cf
_IC0CAPT1	=	0x00cf
G$IC0CAPT$0$0 == 0xcfce
_IC0CAPT	=	0xcfce
G$IC0MODE$0$0 == 0x00cc
_IC0MODE	=	0x00cc
G$IC0STATUS$0$0 == 0x00cd
_IC0STATUS	=	0x00cd
G$IC1CAPT0$0$0 == 0x00d6
_IC1CAPT0	=	0x00d6
G$IC1CAPT1$0$0 == 0x00d7
_IC1CAPT1	=	0x00d7
G$IC1CAPT$0$0 == 0xd7d6
_IC1CAPT	=	0xd7d6
G$IC1MODE$0$0 == 0x00d4
_IC1MODE	=	0x00d4
G$IC1STATUS$0$0 == 0x00d5
_IC1STATUS	=	0x00d5
G$NVADDR0$0$0 == 0x0092
_NVADDR0	=	0x0092
G$NVADDR1$0$0 == 0x0093
_NVADDR1	=	0x0093
G$NVADDR$0$0 == 0x9392
_NVADDR	=	0x9392
G$NVDATA0$0$0 == 0x0094
_NVDATA0	=	0x0094
G$NVDATA1$0$0 == 0x0095
_NVDATA1	=	0x0095
G$NVDATA$0$0 == 0x9594
_NVDATA	=	0x9594
G$NVKEY$0$0 == 0x0096
_NVKEY	=	0x0096
G$NVSTATUS$0$0 == 0x0091
_NVSTATUS	=	0x0091
G$OC0COMP0$0$0 == 0x00bc
_OC0COMP0	=	0x00bc
G$OC0COMP1$0$0 == 0x00bd
_OC0COMP1	=	0x00bd
G$OC0COMP$0$0 == 0xbdbc
_OC0COMP	=	0xbdbc
G$OC0MODE$0$0 == 0x00b9
_OC0MODE	=	0x00b9
G$OC0PIN$0$0 == 0x00ba
_OC0PIN	=	0x00ba
G$OC0STATUS$0$0 == 0x00bb
_OC0STATUS	=	0x00bb
G$OC1COMP0$0$0 == 0x00c4
_OC1COMP0	=	0x00c4
G$OC1COMP1$0$0 == 0x00c5
_OC1COMP1	=	0x00c5
G$OC1COMP$0$0 == 0xc5c4
_OC1COMP	=	0xc5c4
G$OC1MODE$0$0 == 0x00c1
_OC1MODE	=	0x00c1
G$OC1PIN$0$0 == 0x00c2
_OC1PIN	=	0x00c2
G$OC1STATUS$0$0 == 0x00c3
_OC1STATUS	=	0x00c3
G$RADIOACC$0$0 == 0x00b1
_RADIOACC	=	0x00b1
G$RADIOADDR0$0$0 == 0x00b3
_RADIOADDR0	=	0x00b3
G$RADIOADDR1$0$0 == 0x00b2
_RADIOADDR1	=	0x00b2
G$RADIOADDR$0$0 == 0xb2b3
_RADIOADDR	=	0xb2b3
G$RADIODATA0$0$0 == 0x00b7
_RADIODATA0	=	0x00b7
G$RADIODATA1$0$0 == 0x00b6
_RADIODATA1	=	0x00b6
G$RADIODATA2$0$0 == 0x00b5
_RADIODATA2	=	0x00b5
G$RADIODATA3$0$0 == 0x00b4
_RADIODATA3	=	0x00b4
G$RADIODATA$0$0 == 0xb4b5b6b7
_RADIODATA	=	0xb4b5b6b7
G$RADIOSTAT0$0$0 == 0x00be
_RADIOSTAT0	=	0x00be
G$RADIOSTAT1$0$0 == 0x00bf
_RADIOSTAT1	=	0x00bf
G$RADIOSTAT$0$0 == 0xbfbe
_RADIOSTAT	=	0xbfbe
G$SPCLKSRC$0$0 == 0x00df
_SPCLKSRC	=	0x00df
G$SPMODE$0$0 == 0x00dc
_SPMODE	=	0x00dc
G$SPSHREG$0$0 == 0x00de
_SPSHREG	=	0x00de
G$SPSTATUS$0$0 == 0x00dd
_SPSTATUS	=	0x00dd
G$T0CLKSRC$0$0 == 0x009a
_T0CLKSRC	=	0x009a
G$T0CNT0$0$0 == 0x009c
_T0CNT0	=	0x009c
G$T0CNT1$0$0 == 0x009d
_T0CNT1	=	0x009d
G$T0CNT$0$0 == 0x9d9c
_T0CNT	=	0x9d9c
G$T0MODE$0$0 == 0x0099
_T0MODE	=	0x0099
G$T0PERIOD0$0$0 == 0x009e
_T0PERIOD0	=	0x009e
G$T0PERIOD1$0$0 == 0x009f
_T0PERIOD1	=	0x009f
G$T0PERIOD$0$0 == 0x9f9e
_T0PERIOD	=	0x9f9e
G$T0STATUS$0$0 == 0x009b
_T0STATUS	=	0x009b
G$T1CLKSRC$0$0 == 0x00a2
_T1CLKSRC	=	0x00a2
G$T1CNT0$0$0 == 0x00a4
_T1CNT0	=	0x00a4
G$T1CNT1$0$0 == 0x00a5
_T1CNT1	=	0x00a5
G$T1CNT$0$0 == 0xa5a4
_T1CNT	=	0xa5a4
G$T1MODE$0$0 == 0x00a1
_T1MODE	=	0x00a1
G$T1PERIOD0$0$0 == 0x00a6
_T1PERIOD0	=	0x00a6
G$T1PERIOD1$0$0 == 0x00a7
_T1PERIOD1	=	0x00a7
G$T1PERIOD$0$0 == 0xa7a6
_T1PERIOD	=	0xa7a6
G$T1STATUS$0$0 == 0x00a3
_T1STATUS	=	0x00a3
G$T2CLKSRC$0$0 == 0x00aa
_T2CLKSRC	=	0x00aa
G$T2CNT0$0$0 == 0x00ac
_T2CNT0	=	0x00ac
G$T2CNT1$0$0 == 0x00ad
_T2CNT1	=	0x00ad
G$T2CNT$0$0 == 0xadac
_T2CNT	=	0xadac
G$T2MODE$0$0 == 0x00a9
_T2MODE	=	0x00a9
G$T2PERIOD0$0$0 == 0x00ae
_T2PERIOD0	=	0x00ae
G$T2PERIOD1$0$0 == 0x00af
_T2PERIOD1	=	0x00af
G$T2PERIOD$0$0 == 0xafae
_T2PERIOD	=	0xafae
G$T2STATUS$0$0 == 0x00ab
_T2STATUS	=	0x00ab
G$U0CTRL$0$0 == 0x00e4
_U0CTRL	=	0x00e4
G$U0MODE$0$0 == 0x00e7
_U0MODE	=	0x00e7
G$U0SHREG$0$0 == 0x00e6
_U0SHREG	=	0x00e6
G$U0STATUS$0$0 == 0x00e5
_U0STATUS	=	0x00e5
G$U1CTRL$0$0 == 0x00ec
_U1CTRL	=	0x00ec
G$U1MODE$0$0 == 0x00ef
_U1MODE	=	0x00ef
G$U1SHREG$0$0 == 0x00ee
_U1SHREG	=	0x00ee
G$U1STATUS$0$0 == 0x00ed
_U1STATUS	=	0x00ed
G$WDTCFG$0$0 == 0x00da
_WDTCFG	=	0x00da
G$WDTRESET$0$0 == 0x00db
_WDTRESET	=	0x00db
G$WTCFGA$0$0 == 0x00f1
_WTCFGA	=	0x00f1
G$WTCFGB$0$0 == 0x00f9
_WTCFGB	=	0x00f9
G$WTCNTA0$0$0 == 0x00f2
_WTCNTA0	=	0x00f2
G$WTCNTA1$0$0 == 0x00f3
_WTCNTA1	=	0x00f3
G$WTCNTA$0$0 == 0xf3f2
_WTCNTA	=	0xf3f2
G$WTCNTB0$0$0 == 0x00fa
_WTCNTB0	=	0x00fa
G$WTCNTB1$0$0 == 0x00fb
_WTCNTB1	=	0x00fb
G$WTCNTB$0$0 == 0xfbfa
_WTCNTB	=	0xfbfa
G$WTCNTR1$0$0 == 0x00eb
_WTCNTR1	=	0x00eb
G$WTEVTA0$0$0 == 0x00f4
_WTEVTA0	=	0x00f4
G$WTEVTA1$0$0 == 0x00f5
_WTEVTA1	=	0x00f5
G$WTEVTA$0$0 == 0xf5f4
_WTEVTA	=	0xf5f4
G$WTEVTB0$0$0 == 0x00f6
_WTEVTB0	=	0x00f6
G$WTEVTB1$0$0 == 0x00f7
_WTEVTB1	=	0x00f7
G$WTEVTB$0$0 == 0xf7f6
_WTEVTB	=	0xf7f6
G$WTEVTC0$0$0 == 0x00fc
_WTEVTC0	=	0x00fc
G$WTEVTC1$0$0 == 0x00fd
_WTEVTC1	=	0x00fd
G$WTEVTC$0$0 == 0xfdfc
_WTEVTC	=	0xfdfc
G$WTEVTD0$0$0 == 0x00fe
_WTEVTD0	=	0x00fe
G$WTEVTD1$0$0 == 0x00ff
_WTEVTD1	=	0x00ff
G$WTEVTD$0$0 == 0xfffe
_WTEVTD	=	0xfffe
G$WTIRQEN$0$0 == 0x00e9
_WTIRQEN	=	0x00e9
G$WTSTAT$0$0 == 0x00ea
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$ACC_0$0$0 == 0x00e0
_ACC_0	=	0x00e0
G$ACC_1$0$0 == 0x00e1
_ACC_1	=	0x00e1
G$ACC_2$0$0 == 0x00e2
_ACC_2	=	0x00e2
G$ACC_3$0$0 == 0x00e3
_ACC_3	=	0x00e3
G$ACC_4$0$0 == 0x00e4
_ACC_4	=	0x00e4
G$ACC_5$0$0 == 0x00e5
_ACC_5	=	0x00e5
G$ACC_6$0$0 == 0x00e6
_ACC_6	=	0x00e6
G$ACC_7$0$0 == 0x00e7
_ACC_7	=	0x00e7
G$B_0$0$0 == 0x00f0
_B_0	=	0x00f0
G$B_1$0$0 == 0x00f1
_B_1	=	0x00f1
G$B_2$0$0 == 0x00f2
_B_2	=	0x00f2
G$B_3$0$0 == 0x00f3
_B_3	=	0x00f3
G$B_4$0$0 == 0x00f4
_B_4	=	0x00f4
G$B_5$0$0 == 0x00f5
_B_5	=	0x00f5
G$B_6$0$0 == 0x00f6
_B_6	=	0x00f6
G$B_7$0$0 == 0x00f7
_B_7	=	0x00f7
G$E2IE_0$0$0 == 0x00a0
_E2IE_0	=	0x00a0
G$E2IE_1$0$0 == 0x00a1
_E2IE_1	=	0x00a1
G$E2IE_2$0$0 == 0x00a2
_E2IE_2	=	0x00a2
G$E2IE_3$0$0 == 0x00a3
_E2IE_3	=	0x00a3
G$E2IE_4$0$0 == 0x00a4
_E2IE_4	=	0x00a4
G$E2IE_5$0$0 == 0x00a5
_E2IE_5	=	0x00a5
G$E2IE_6$0$0 == 0x00a6
_E2IE_6	=	0x00a6
G$E2IE_7$0$0 == 0x00a7
_E2IE_7	=	0x00a7
G$E2IP_0$0$0 == 0x00c0
_E2IP_0	=	0x00c0
G$E2IP_1$0$0 == 0x00c1
_E2IP_1	=	0x00c1
G$E2IP_2$0$0 == 0x00c2
_E2IP_2	=	0x00c2
G$E2IP_3$0$0 == 0x00c3
_E2IP_3	=	0x00c3
G$E2IP_4$0$0 == 0x00c4
_E2IP_4	=	0x00c4
G$E2IP_5$0$0 == 0x00c5
_E2IP_5	=	0x00c5
G$E2IP_6$0$0 == 0x00c6
_E2IP_6	=	0x00c6
G$E2IP_7$0$0 == 0x00c7
_E2IP_7	=	0x00c7
G$EIE_0$0$0 == 0x0098
_EIE_0	=	0x0098
G$EIE_1$0$0 == 0x0099
_EIE_1	=	0x0099
G$EIE_2$0$0 == 0x009a
_EIE_2	=	0x009a
G$EIE_3$0$0 == 0x009b
_EIE_3	=	0x009b
G$EIE_4$0$0 == 0x009c
_EIE_4	=	0x009c
G$EIE_5$0$0 == 0x009d
_EIE_5	=	0x009d
G$EIE_6$0$0 == 0x009e
_EIE_6	=	0x009e
G$EIE_7$0$0 == 0x009f
_EIE_7	=	0x009f
G$EIP_0$0$0 == 0x00b0
_EIP_0	=	0x00b0
G$EIP_1$0$0 == 0x00b1
_EIP_1	=	0x00b1
G$EIP_2$0$0 == 0x00b2
_EIP_2	=	0x00b2
G$EIP_3$0$0 == 0x00b3
_EIP_3	=	0x00b3
G$EIP_4$0$0 == 0x00b4
_EIP_4	=	0x00b4
G$EIP_5$0$0 == 0x00b5
_EIP_5	=	0x00b5
G$EIP_6$0$0 == 0x00b6
_EIP_6	=	0x00b6
G$EIP_7$0$0 == 0x00b7
_EIP_7	=	0x00b7
G$IE_0$0$0 == 0x00a8
_IE_0	=	0x00a8
G$IE_1$0$0 == 0x00a9
_IE_1	=	0x00a9
G$IE_2$0$0 == 0x00aa
_IE_2	=	0x00aa
G$IE_3$0$0 == 0x00ab
_IE_3	=	0x00ab
G$IE_4$0$0 == 0x00ac
_IE_4	=	0x00ac
G$IE_5$0$0 == 0x00ad
_IE_5	=	0x00ad
G$IE_6$0$0 == 0x00ae
_IE_6	=	0x00ae
G$IE_7$0$0 == 0x00af
_IE_7	=	0x00af
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$IP_0$0$0 == 0x00b8
_IP_0	=	0x00b8
G$IP_1$0$0 == 0x00b9
_IP_1	=	0x00b9
G$IP_2$0$0 == 0x00ba
_IP_2	=	0x00ba
G$IP_3$0$0 == 0x00bb
_IP_3	=	0x00bb
G$IP_4$0$0 == 0x00bc
_IP_4	=	0x00bc
G$IP_5$0$0 == 0x00bd
_IP_5	=	0x00bd
G$IP_6$0$0 == 0x00be
_IP_6	=	0x00be
G$IP_7$0$0 == 0x00bf
_IP_7	=	0x00bf
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$PINA_0$0$0 == 0x00c8
_PINA_0	=	0x00c8
G$PINA_1$0$0 == 0x00c9
_PINA_1	=	0x00c9
G$PINA_2$0$0 == 0x00ca
_PINA_2	=	0x00ca
G$PINA_3$0$0 == 0x00cb
_PINA_3	=	0x00cb
G$PINA_4$0$0 == 0x00cc
_PINA_4	=	0x00cc
G$PINA_5$0$0 == 0x00cd
_PINA_5	=	0x00cd
G$PINA_6$0$0 == 0x00ce
_PINA_6	=	0x00ce
G$PINA_7$0$0 == 0x00cf
_PINA_7	=	0x00cf
G$PINB_0$0$0 == 0x00e8
_PINB_0	=	0x00e8
G$PINB_1$0$0 == 0x00e9
_PINB_1	=	0x00e9
G$PINB_2$0$0 == 0x00ea
_PINB_2	=	0x00ea
G$PINB_3$0$0 == 0x00eb
_PINB_3	=	0x00eb
G$PINB_4$0$0 == 0x00ec
_PINB_4	=	0x00ec
G$PINB_5$0$0 == 0x00ed
_PINB_5	=	0x00ed
G$PINB_6$0$0 == 0x00ee
_PINB_6	=	0x00ee
G$PINB_7$0$0 == 0x00ef
_PINB_7	=	0x00ef
G$PINC_0$0$0 == 0x00f8
_PINC_0	=	0x00f8
G$PINC_1$0$0 == 0x00f9
_PINC_1	=	0x00f9
G$PINC_2$0$0 == 0x00fa
_PINC_2	=	0x00fa
G$PINC_3$0$0 == 0x00fb
_PINC_3	=	0x00fb
G$PINC_4$0$0 == 0x00fc
_PINC_4	=	0x00fc
G$PINC_5$0$0 == 0x00fd
_PINC_5	=	0x00fd
G$PINC_6$0$0 == 0x00fe
_PINC_6	=	0x00fe
G$PINC_7$0$0 == 0x00ff
_PINC_7	=	0x00ff
G$PORTA_0$0$0 == 0x0080
_PORTA_0	=	0x0080
G$PORTA_1$0$0 == 0x0081
_PORTA_1	=	0x0081
G$PORTA_2$0$0 == 0x0082
_PORTA_2	=	0x0082
G$PORTA_3$0$0 == 0x0083
_PORTA_3	=	0x0083
G$PORTA_4$0$0 == 0x0084
_PORTA_4	=	0x0084
G$PORTA_5$0$0 == 0x0085
_PORTA_5	=	0x0085
G$PORTA_6$0$0 == 0x0086
_PORTA_6	=	0x0086
G$PORTA_7$0$0 == 0x0087
_PORTA_7	=	0x0087
G$PORTB_0$0$0 == 0x0088
_PORTB_0	=	0x0088
G$PORTB_1$0$0 == 0x0089
_PORTB_1	=	0x0089
G$PORTB_2$0$0 == 0x008a
_PORTB_2	=	0x008a
G$PORTB_3$0$0 == 0x008b
_PORTB_3	=	0x008b
G$PORTB_4$0$0 == 0x008c
_PORTB_4	=	0x008c
G$PORTB_5$0$0 == 0x008d
_PORTB_5	=	0x008d
G$PORTB_6$0$0 == 0x008e
_PORTB_6	=	0x008e
G$PORTB_7$0$0 == 0x008f
_PORTB_7	=	0x008f
G$PORTC_0$0$0 == 0x0090
_PORTC_0	=	0x0090
G$PORTC_1$0$0 == 0x0091
_PORTC_1	=	0x0091
G$PORTC_2$0$0 == 0x0092
_PORTC_2	=	0x0092
G$PORTC_3$0$0 == 0x0093
_PORTC_3	=	0x0093
G$PORTC_4$0$0 == 0x0094
_PORTC_4	=	0x0094
G$PORTC_5$0$0 == 0x0095
_PORTC_5	=	0x0095
G$PORTC_6$0$0 == 0x0096
_PORTC_6	=	0x0096
G$PORTC_7$0$0 == 0x0097
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
Lgoldseq.GOLDSEQUENCE_Obtain$sloc0$1$0==.
_GOLDSEQUENCE_Obtain_sloc0_1_0:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
G$ADCCH0VAL0$0$0 == 0x7020
_ADCCH0VAL0	=	0x7020
G$ADCCH0VAL1$0$0 == 0x7021
_ADCCH0VAL1	=	0x7021
G$ADCCH0VAL$0$0 == 0x7020
_ADCCH0VAL	=	0x7020
G$ADCCH1VAL0$0$0 == 0x7022
_ADCCH1VAL0	=	0x7022
G$ADCCH1VAL1$0$0 == 0x7023
_ADCCH1VAL1	=	0x7023
G$ADCCH1VAL$0$0 == 0x7022
_ADCCH1VAL	=	0x7022
G$ADCCH2VAL0$0$0 == 0x7024
_ADCCH2VAL0	=	0x7024
G$ADCCH2VAL1$0$0 == 0x7025
_ADCCH2VAL1	=	0x7025
G$ADCCH2VAL$0$0 == 0x7024
_ADCCH2VAL	=	0x7024
G$ADCCH3VAL0$0$0 == 0x7026
_ADCCH3VAL0	=	0x7026
G$ADCCH3VAL1$0$0 == 0x7027
_ADCCH3VAL1	=	0x7027
G$ADCCH3VAL$0$0 == 0x7026
_ADCCH3VAL	=	0x7026
G$ADCTUNE0$0$0 == 0x7028
_ADCTUNE0	=	0x7028
G$ADCTUNE1$0$0 == 0x7029
_ADCTUNE1	=	0x7029
G$ADCTUNE2$0$0 == 0x702a
_ADCTUNE2	=	0x702a
G$DMA0ADDR0$0$0 == 0x7010
_DMA0ADDR0	=	0x7010
G$DMA0ADDR1$0$0 == 0x7011
_DMA0ADDR1	=	0x7011
G$DMA0ADDR$0$0 == 0x7010
_DMA0ADDR	=	0x7010
G$DMA0CONFIG$0$0 == 0x7014
_DMA0CONFIG	=	0x7014
G$DMA1ADDR0$0$0 == 0x7012
_DMA1ADDR0	=	0x7012
G$DMA1ADDR1$0$0 == 0x7013
_DMA1ADDR1	=	0x7013
G$DMA1ADDR$0$0 == 0x7012
_DMA1ADDR	=	0x7012
G$DMA1CONFIG$0$0 == 0x7015
_DMA1CONFIG	=	0x7015
G$FRCOSCCONFIG$0$0 == 0x7070
_FRCOSCCONFIG	=	0x7070
G$FRCOSCCTRL$0$0 == 0x7071
_FRCOSCCTRL	=	0x7071
G$FRCOSCFREQ0$0$0 == 0x7076
_FRCOSCFREQ0	=	0x7076
G$FRCOSCFREQ1$0$0 == 0x7077
_FRCOSCFREQ1	=	0x7077
G$FRCOSCFREQ$0$0 == 0x7076
_FRCOSCFREQ	=	0x7076
G$FRCOSCKFILT0$0$0 == 0x7072
_FRCOSCKFILT0	=	0x7072
G$FRCOSCKFILT1$0$0 == 0x7073
_FRCOSCKFILT1	=	0x7073
G$FRCOSCKFILT$0$0 == 0x7072
_FRCOSCKFILT	=	0x7072
G$FRCOSCPER0$0$0 == 0x7078
_FRCOSCPER0	=	0x7078
G$FRCOSCPER1$0$0 == 0x7079
_FRCOSCPER1	=	0x7079
G$FRCOSCPER$0$0 == 0x7078
_FRCOSCPER	=	0x7078
G$FRCOSCREF0$0$0 == 0x7074
_FRCOSCREF0	=	0x7074
G$FRCOSCREF1$0$0 == 0x7075
_FRCOSCREF1	=	0x7075
G$FRCOSCREF$0$0 == 0x7074
_FRCOSCREF	=	0x7074
G$ANALOGA$0$0 == 0x7007
_ANALOGA	=	0x7007
G$GPIOENABLE$0$0 == 0x700c
_GPIOENABLE	=	0x700c
G$EXTIRQ$0$0 == 0x7003
_EXTIRQ	=	0x7003
G$INTCHGA$0$0 == 0x7000
_INTCHGA	=	0x7000
G$INTCHGB$0$0 == 0x7001
_INTCHGB	=	0x7001
G$INTCHGC$0$0 == 0x7002
_INTCHGC	=	0x7002
G$PALTA$0$0 == 0x7008
_PALTA	=	0x7008
G$PALTB$0$0 == 0x7009
_PALTB	=	0x7009
G$PALTC$0$0 == 0x700a
_PALTC	=	0x700a
G$PALTRADIO$0$0 == 0x7046
_PALTRADIO	=	0x7046
G$PINCHGA$0$0 == 0x7004
_PINCHGA	=	0x7004
G$PINCHGB$0$0 == 0x7005
_PINCHGB	=	0x7005
G$PINCHGC$0$0 == 0x7006
_PINCHGC	=	0x7006
G$PINSEL$0$0 == 0x700b
_PINSEL	=	0x700b
G$LPOSCCONFIG$0$0 == 0x7060
_LPOSCCONFIG	=	0x7060
G$LPOSCFREQ0$0$0 == 0x7066
_LPOSCFREQ0	=	0x7066
G$LPOSCFREQ1$0$0 == 0x7067
_LPOSCFREQ1	=	0x7067
G$LPOSCFREQ$0$0 == 0x7066
_LPOSCFREQ	=	0x7066
G$LPOSCKFILT0$0$0 == 0x7062
_LPOSCKFILT0	=	0x7062
G$LPOSCKFILT1$0$0 == 0x7063
_LPOSCKFILT1	=	0x7063
G$LPOSCKFILT$0$0 == 0x7062
_LPOSCKFILT	=	0x7062
G$LPOSCPER0$0$0 == 0x7068
_LPOSCPER0	=	0x7068
G$LPOSCPER1$0$0 == 0x7069
_LPOSCPER1	=	0x7069
G$LPOSCPER$0$0 == 0x7068
_LPOSCPER	=	0x7068
G$LPOSCREF0$0$0 == 0x7064
_LPOSCREF0	=	0x7064
G$LPOSCREF1$0$0 == 0x7065
_LPOSCREF1	=	0x7065
G$LPOSCREF$0$0 == 0x7064
_LPOSCREF	=	0x7064
G$LPXOSCGM$0$0 == 0x7054
_LPXOSCGM	=	0x7054
G$MISCCTRL$0$0 == 0x7f01
_MISCCTRL	=	0x7f01
G$OSCCALIB$0$0 == 0x7053
_OSCCALIB	=	0x7053
G$OSCFORCERUN$0$0 == 0x7050
_OSCFORCERUN	=	0x7050
G$OSCREADY$0$0 == 0x7052
_OSCREADY	=	0x7052
G$OSCRUN$0$0 == 0x7051
_OSCRUN	=	0x7051
G$RADIOFDATAADDR0$0$0 == 0x7040
_RADIOFDATAADDR0	=	0x7040
G$RADIOFDATAADDR1$0$0 == 0x7041
_RADIOFDATAADDR1	=	0x7041
G$RADIOFDATAADDR$0$0 == 0x7040
_RADIOFDATAADDR	=	0x7040
G$RADIOFSTATADDR0$0$0 == 0x7042
_RADIOFSTATADDR0	=	0x7042
G$RADIOFSTATADDR1$0$0 == 0x7043
_RADIOFSTATADDR1	=	0x7043
G$RADIOFSTATADDR$0$0 == 0x7042
_RADIOFSTATADDR	=	0x7042
G$RADIOMUX$0$0 == 0x7044
_RADIOMUX	=	0x7044
G$SCRATCH0$0$0 == 0x7084
_SCRATCH0	=	0x7084
G$SCRATCH1$0$0 == 0x7085
_SCRATCH1	=	0x7085
G$SCRATCH2$0$0 == 0x7086
_SCRATCH2	=	0x7086
G$SCRATCH3$0$0 == 0x7087
_SCRATCH3	=	0x7087
G$SILICONREV$0$0 == 0x7f00
_SILICONREV	=	0x7f00
G$XTALAMPL$0$0 == 0x7f19
_XTALAMPL	=	0x7f19
G$XTALOSC$0$0 == 0x7f18
_XTALOSC	=	0x7f18
G$XTALREADY$0$0 == 0x7f1a
_XTALREADY	=	0x7f1a
Fgoldseq$flash_deviceid$0$0 == 0xfc06
_flash_deviceid	=	0xfc06
G$AESCONFIG$0$0 == 0x7091
_AESCONFIG	=	0x7091
G$AESCURBLOCK$0$0 == 0x7098
_AESCURBLOCK	=	0x7098
G$AESINADDR0$0$0 == 0x7094
_AESINADDR0	=	0x7094
G$AESINADDR1$0$0 == 0x7095
_AESINADDR1	=	0x7095
G$AESINADDR$0$0 == 0x7094
_AESINADDR	=	0x7094
G$AESKEYADDR0$0$0 == 0x7092
_AESKEYADDR0	=	0x7092
G$AESKEYADDR1$0$0 == 0x7093
_AESKEYADDR1	=	0x7093
G$AESKEYADDR$0$0 == 0x7092
_AESKEYADDR	=	0x7092
G$AESMODE$0$0 == 0x7090
_AESMODE	=	0x7090
G$AESOUTADDR0$0$0 == 0x7096
_AESOUTADDR0	=	0x7096
G$AESOUTADDR1$0$0 == 0x7097
_AESOUTADDR1	=	0x7097
G$AESOUTADDR$0$0 == 0x7096
_AESOUTADDR	=	0x7096
G$RNGBYTE$0$0 == 0x7081
_RNGBYTE	=	0x7081
G$RNGCLKSRC0$0$0 == 0x7082
_RNGCLKSRC0	=	0x7082
G$RNGCLKSRC1$0$0 == 0x7083
_RNGCLKSRC1	=	0x7083
G$RNGMODE$0$0 == 0x7080
_RNGMODE	=	0x7080
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'GOLDSEQUENCE_Init'
;------------------------------------------------------------
;i                         Allocated to registers 
;------------------------------------------------------------
	G$GOLDSEQUENCE_Init$0$0 ==.
	C$goldseq.c$18$0$0 ==.
;	..\COMMON\goldseq.c:18: /*__xdata*/ void GOLDSEQUENCE_Init(void)
;	-----------------------------------------
;	 function GOLDSEQUENCE_Init
;	-----------------------------------------
_GOLDSEQUENCE_Init:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	C$goldseq.c$23$1$122 ==.
;	..\COMMON\goldseq.c:23: flash_unlock();
	lcall	_flash_unlock
	C$goldseq.c$27$1$122 ==.
;	..\COMMON\goldseq.c:27: i = flash_pageerase(MEM_GOLD_SEQUENCES_START/* + 1*/);
	mov	dptr,#0xcc00
	lcall	_flash_pageerase
	C$goldseq.c$34$1$122 ==.
;	..\COMMON\goldseq.c:34: flash_write(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100); // 0xF9A4
	mov	_flash_write_PARM_2,#0xa4
	mov	(_flash_write_PARM_2 + 1),#0xf9
	mov	dptr,#0xcc00
	lcall	_flash_write
	C$goldseq.c$37$1$122 ==.
;	..\COMMON\goldseq.c:37: flash_write(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000); // 0x15D8
	mov	_flash_write_PARM_2,#0xd8
	mov	(_flash_write_PARM_2 + 1),#0x15
	mov	dptr,#0xcc02
	lcall	_flash_write
	C$goldseq.c$40$1$122 ==.
;	..\COMMON\goldseq.c:40: flash_write(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000); // 0xF930
	mov	_flash_write_PARM_2,#0x30
	mov	(_flash_write_PARM_2 + 1),#0xf9
	mov	dptr,#0xcc04
	lcall	_flash_write
	C$goldseq.c$42$1$122 ==.
;	..\COMMON\goldseq.c:42: flash_write(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110); // 0x5A8E
	mov	_flash_write_PARM_2,#0x8e
	mov	(_flash_write_PARM_2 + 1),#0x5a
	mov	dptr,#0xcc06
	lcall	_flash_write
	C$goldseq.c$45$1$122 ==.
;	..\COMMON\goldseq.c:45: flash_write(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100); // 0x0094
	mov	_flash_write_PARM_2,#0x94
	mov	(_flash_write_PARM_2 + 1),#0x00
	mov	dptr,#0xcc08
	lcall	_flash_write
	C$goldseq.c$47$1$122 ==.
;	..\COMMON\goldseq.c:47: flash_write(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110); // 0x4F56
	mov	_flash_write_PARM_2,#0x56
	mov	(_flash_write_PARM_2 + 1),#0x4f
	mov	dptr,#0xcc0a
	lcall	_flash_write
	C$goldseq.c$50$1$122 ==.
;	..\COMMON\goldseq.c:50: flash_write(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100); // 0x853C
	mov	_flash_write_PARM_2,#0x3c
	mov	(_flash_write_PARM_2 + 1),#0x85
	mov	dptr,#0xcc0c
	lcall	_flash_write
	C$goldseq.c$52$1$122 ==.
;	..\COMMON\goldseq.c:52: flash_write(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111); // 0x389F
	mov	_flash_write_PARM_2,#0x9f
	mov	(_flash_write_PARM_2 + 1),#0x38
	mov	dptr,#0xcc0e
	lcall	_flash_write
	C$goldseq.c$55$1$122 ==.
;	..\COMMON\goldseq.c:55: flash_write(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000); // 0x47E8
	mov	_flash_write_PARM_2,#0xe8
	mov	(_flash_write_PARM_2 + 1),#0x47
	mov	dptr,#0xcc10
	lcall	_flash_write
	C$goldseq.c$57$1$122 ==.
;	..\COMMON\goldseq.c:57: flash_write(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011); // 0x037B
	mov	_flash_write_PARM_2,#0x7b
	mov	(_flash_write_PARM_2 + 1),#0x03
	mov	dptr,#0xcc12
	lcall	_flash_write
	C$goldseq.c$60$1$122 ==.
;	..\COMMON\goldseq.c:60: flash_write(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010); // 0x2682
	mov	_flash_write_PARM_2,#0x82
	mov	(_flash_write_PARM_2 + 1),#0x26
	mov	dptr,#0xcc14
	lcall	_flash_write
	C$goldseq.c$62$1$122 ==.
;	..\COMMON\goldseq.c:62: flash_write(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001); // 0x1E89
	mov	_flash_write_PARM_2,#0x89
	mov	(_flash_write_PARM_2 + 1),#0x1e
	mov	dptr,#0xcc16
	lcall	_flash_write
	C$goldseq.c$65$1$122 ==.
;	..\COMMON\goldseq.c:65: flash_write(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111); //0x1637
	mov	_flash_write_PARM_2,#0x37
	mov	(_flash_write_PARM_2 + 1),#0x16
	mov	dptr,#0xcc18
	lcall	_flash_write
	C$goldseq.c$67$1$122 ==.
;	..\COMMON\goldseq.c:67: flash_write(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000); // 0x1070
	mov	_flash_write_PARM_2,#0x70
	mov	(_flash_write_PARM_2 + 1),#0x10
	mov	dptr,#0xcc1a
	lcall	_flash_write
	C$goldseq.c$70$1$122 ==.
;	..\COMMON\goldseq.c:70: flash_write(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101); // 0x8E6D
	mov	_flash_write_PARM_2,#0x6d
	mov	(_flash_write_PARM_2 + 1),#0x8e
	mov	dptr,#0xcc1c
	lcall	_flash_write
	C$goldseq.c$72$1$122 ==.
;	..\COMMON\goldseq.c:72: flash_write(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100); // 0x570C
	mov	_flash_write_PARM_2,#0x0c
	mov	(_flash_write_PARM_2 + 1),#0x57
	mov	dptr,#0xcc1e
	lcall	_flash_write
	C$goldseq.c$75$1$122 ==.
;	..\COMMON\goldseq.c:75: flash_write(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000); // 0xC240
	mov	_flash_write_PARM_2,#0x40
	mov	(_flash_write_PARM_2 + 1),#0xc2
	mov	dptr,#0xcc20
	lcall	_flash_write
	C$goldseq.c$77$1$122 ==.
;	..\COMMON\goldseq.c:77: flash_write(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010); // 0x74B2
	mov	_flash_write_PARM_2,#0xb2
	mov	(_flash_write_PARM_2 + 1),#0x74
	mov	dptr,#0xcc22
	lcall	_flash_write
	C$goldseq.c$80$1$122 ==.
;	..\COMMON\goldseq.c:80: flash_write(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110); // 0xE456
	mov	_flash_write_PARM_2,#0x56
	mov	(_flash_write_PARM_2 + 1),#0xe4
	mov	dptr,#0xcc24
	lcall	_flash_write
	C$goldseq.c$82$1$122 ==.
;	..\COMMON\goldseq.c:82: flash_write(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101); // 0x256D
	mov	_flash_write_PARM_2,#0x6d
	mov	(_flash_write_PARM_2 + 1),#0x25
	mov	dptr,#0xcc26
	lcall	_flash_write
	C$goldseq.c$85$1$122 ==.
;	..\COMMON\goldseq.c:85: flash_write(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101); // 0x775D
	mov	_flash_write_PARM_2,#0x5d
	mov	(_flash_write_PARM_2 + 1),#0x77
	mov	dptr,#0xcc28
	lcall	_flash_write
	C$goldseq.c$87$1$122 ==.
;	..\COMMON\goldseq.c:87: flash_write(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010); // 0x0D82
	mov	_flash_write_PARM_2,#0x82
	mov	(_flash_write_PARM_2 + 1),#0x0d
	mov	dptr,#0xcc2a
	lcall	_flash_write
	C$goldseq.c$90$1$122 ==.
;	..\COMMON\goldseq.c:90: flash_write(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000); // 0xBED8
	mov	_flash_write_PARM_2,#0xd8
	mov	(_flash_write_PARM_2 + 1),#0xbe
	mov	dptr,#0xcc2c
	lcall	_flash_write
	C$goldseq.c$92$1$122 ==.
;	..\COMMON\goldseq.c:92: flash_write(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101); // 0x59F5
	mov	_flash_write_PARM_2,#0xf5
	mov	(_flash_write_PARM_2 + 1),#0x59
	mov	dptr,#0xcc2e
	lcall	_flash_write
	C$goldseq.c$95$1$122 ==.
;	..\COMMON\goldseq.c:95: flash_write(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010);
	mov	_flash_write_PARM_2,#0x1a
	mov	(_flash_write_PARM_2 + 1),#0x5a
	mov	dptr,#0xcc30
	lcall	_flash_write
	C$goldseq.c$97$1$122 ==.
;	..\COMMON\goldseq.c:97: flash_write(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110);
	mov	_flash_write_PARM_2,#0xce
	mov	(_flash_write_PARM_2 + 1),#0x33
	mov	dptr,#0xcc32
	lcall	_flash_write
	C$goldseq.c$100$1$122 ==.
;	..\COMMON\goldseq.c:100: flash_write(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011);
	mov	_flash_write_PARM_2,#0x7b
	mov	(_flash_write_PARM_2 + 1),#0xa8
	mov	dptr,#0xcc34
	lcall	_flash_write
	C$goldseq.c$102$1$122 ==.
;	..\COMMON\goldseq.c:102: flash_write(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011);
	mov	_flash_write_PARM_2,#0xd3
	mov	(_flash_write_PARM_2 + 1),#0x06
	mov	dptr,#0xcc36
	lcall	_flash_write
	C$goldseq.c$105$1$122 ==.
;	..\COMMON\goldseq.c:105: flash_write(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011); // 0x514B
	mov	_flash_write_PARM_2,#0x4b
	mov	(_flash_write_PARM_2 + 1),#0x51
	mov	dptr,#0xcc38
	lcall	_flash_write
	C$goldseq.c$107$1$122 ==.
;	..\COMMON\goldseq.c:107: flash_write(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101); // 0xB8BA
	mov	_flash_write_PARM_2,#0x5d
	mov	(_flash_write_PARM_2 + 1),#0x5c
	mov	dptr,#0xcc3a
	lcall	_flash_write
	C$goldseq.c$110$1$122 ==.
;	..\COMMON\goldseq.c:110: flash_write(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011);
	mov	_flash_write_PARM_2,#0xd3
	mov	(_flash_write_PARM_2 + 1),#0x2d
	mov	dptr,#0xcc3c
	lcall	_flash_write
	C$goldseq.c$112$1$122 ==.
;	..\COMMON\goldseq.c:112: flash_write(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010);
	mov	_flash_write_PARM_2,#0x1a
	mov	(_flash_write_PARM_2 + 1),#0x71
	mov	dptr,#0xcc3e
	lcall	_flash_write
	C$goldseq.c$115$1$122 ==.
;	..\COMMON\goldseq.c:115: flash_write(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111);
	mov	_flash_write_PARM_2,#0x9f
	mov	(_flash_write_PARM_2 + 1),#0x93
	mov	dptr,#0xcc40
	lcall	_flash_write
	C$goldseq.c$117$1$122 ==.
;	..\COMMON\goldseq.c:117: flash_write(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001);
	mov	_flash_write_PARM_2,#0xb9
	mov	(_flash_write_PARM_2 + 1),#0x67
	mov	dptr,#0xcc42
	lcall	_flash_write
	C$goldseq.c$120$1$122 ==.
;	..\COMMON\goldseq.c:120: flash_write(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001);
	mov	_flash_write_PARM_2,#0xb9
	mov	(_flash_write_PARM_2 + 1),#0x4c
	mov	dptr,#0xcc44
	lcall	_flash_write
	C$goldseq.c$122$1$122 ==.
;	..\COMMON\goldseq.c:122: flash_write(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000);
	mov	_flash_write_PARM_2,#0xe8
	mov	(_flash_write_PARM_2 + 1),#0x6c
	mov	dptr,#0xcc46
	lcall	_flash_write
	C$goldseq.c$125$1$122 ==.
;	..\COMMON\goldseq.c:125: flash_write(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010); // 0xA32A
	mov	_flash_write_PARM_2,#0x2a
	mov	(_flash_write_PARM_2 + 1),#0xa3
	mov	dptr,#0xcc48
	lcall	_flash_write
	C$goldseq.c$127$1$122 ==.
;	..\COMMON\goldseq.c:127: flash_write(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000); // 0x6940
	mov	_flash_write_PARM_2,#0x40
	mov	(_flash_write_PARM_2 + 1),#0x69
	mov	dptr,#0xcc4a
	lcall	_flash_write
	C$goldseq.c$130$1$122 ==.
;	..\COMMON\goldseq.c:130: flash_write(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011);
	mov	_flash_write_PARM_2,#0xe3
	mov	(_flash_write_PARM_2 + 1),#0xd4
	mov	dptr,#0xcc4c
	lcall	_flash_write
	C$goldseq.c$132$1$122 ==.
;	..\COMMON\goldseq.c:132: flash_write(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100);
	mov	_flash_write_PARM_2,#0x94
	mov	(_flash_write_PARM_2 + 1),#0x2b
	mov	dptr,#0xcc4e
	lcall	_flash_write
	C$goldseq.c$135$1$122 ==.
;	..\COMMON\goldseq.c:135: flash_write(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111);
	mov	_flash_write_PARM_2,#0x07
	mov	(_flash_write_PARM_2 + 1),#0xef
	mov	dptr,#0xcc50
	lcall	_flash_write
	C$goldseq.c$137$1$122 ==.
;	..\COMMON\goldseq.c:137: flash_write(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110); // 0x4AFE
	mov	_flash_write_PARM_2,#0xfe
	mov	(_flash_write_PARM_2 + 1),#0x4a
	mov	dptr,#0xcc52
	lcall	_flash_write
	C$goldseq.c$140$1$122 ==.
;	..\COMMON\goldseq.c:140: flash_write(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101); // 0xF2F5
	mov	_flash_write_PARM_2,#0xf5
	mov	(_flash_write_PARM_2 + 1),#0xf2
	mov	dptr,#0xcc54
	lcall	_flash_write
	C$goldseq.c$142$1$122 ==.
;	..\COMMON\goldseq.c:142: flash_write(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011); // 0x7A4B
	mov	_flash_write_PARM_2,#0x4b
	mov	(_flash_write_PARM_2 + 1),#0x7a
	mov	dptr,#0xcc56
	lcall	_flash_write
	C$goldseq.c$144$1$122 ==.
;	..\COMMON\goldseq.c:144: flash_write(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100); // 0x7C0C
	mov	_flash_write_PARM_2,#0x0c
	mov	(_flash_write_PARM_2 + 1),#0x7c
	mov	dptr,#0xcc58
	lcall	_flash_write
	C$goldseq.c$146$1$122 ==.
;	..\COMMON\goldseq.c:146: flash_write(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001); //0x6211
	mov	_flash_write_PARM_2,#0x11
	mov	(_flash_write_PARM_2 + 1),#0x62
	mov	dptr,#0xcc5a
	lcall	_flash_write
	C$goldseq.c$149$1$122 ==.
;	..\COMMON\goldseq.c:149: flash_write(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000); // 0x3B70
	mov	_flash_write_PARM_2,#0x70
	mov	(_flash_write_PARM_2 + 1),#0x3b
	mov	dptr,#0xcc5c
	lcall	_flash_write
	C$goldseq.c$151$1$122 ==.
;	..\COMMON\goldseq.c:151: flash_write(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100); // 0x2E3C
	mov	_flash_write_PARM_2,#0x3c
	mov	(_flash_write_PARM_2 + 1),#0x2e
	mov	dptr,#0xcc5e
	lcall	_flash_write
	C$goldseq.c$154$1$122 ==.
;	..\COMMON\goldseq.c:154: flash_write(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110); // 0x98CE
	mov	_flash_write_PARM_2,#0xce
	mov	(_flash_write_PARM_2 + 1),#0x98
	mov	dptr,#0xcc60
	lcall	_flash_write
	C$goldseq.c$156$1$122 ==.
;	..\COMMON\goldseq.c:156: flash_write(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010);
	mov	_flash_write_PARM_2,#0x2a
	mov	(_flash_write_PARM_2 + 1),#0x08
	mov	dptr,#0xcc62
	lcall	_flash_write
	C$goldseq.c$159$1$122 ==.
;	..\COMMON\goldseq.c:159: flash_write(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001); // 0xC911
	mov	_flash_write_PARM_2,#0x11
	mov	(_flash_write_PARM_2 + 1),#0xc9
	mov	dptr,#0xcc64
	lcall	_flash_write
	C$goldseq.c$161$1$122 ==.
;	..\COMMON\goldseq.c:161: flash_write(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001); // 0x1B21
	mov	_flash_write_PARM_2,#0x21
	mov	(_flash_write_PARM_2 + 1),#0x1b
	mov	dptr,#0xcc66
	lcall	_flash_write
	C$goldseq.c$164$1$122 ==.
;	..\COMMON\goldseq.c:164: flash_write(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110); // 0x61FE
	mov	_flash_write_PARM_2,#0xfe
	mov	(_flash_write_PARM_2 + 1),#0x61
	mov	dptr,#0xcc68
	lcall	_flash_write
	C$goldseq.c$166$1$122 ==.
;	..\COMMON\goldseq.c:166: flash_write(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100); // 0x52A4
	mov	_flash_write_PARM_2,#0xa4
	mov	(_flash_write_PARM_2 + 1),#0x52
	mov	dptr,#0xcc6a
	lcall	_flash_write
	C$goldseq.c$169$1$122 ==.
;	..\COMMON\goldseq.c:169: flash_write(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001);
	mov	_flash_write_PARM_2,#0x89
	mov	(_flash_write_PARM_2 + 1),#0xb5
	mov	dptr,#0xcc6c
	lcall	_flash_write
	C$goldseq.c$171$1$122 ==.
;	..\COMMON\goldseq.c:171: flash_write(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110);
	mov	_flash_write_PARM_2,#0x66
	mov	(_flash_write_PARM_2 + 1),#0x36
	mov	dptr,#0xcc6e
	lcall	_flash_write
	C$goldseq.c$174$1$122 ==.
;	..\COMMON\goldseq.c:174: flash_write(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010);
	mov	_flash_write_PARM_2,#0xb2
	mov	(_flash_write_PARM_2 + 1),#0xdf
	mov	dptr,#0xcc70
	lcall	_flash_write
	C$goldseq.c$176$1$122 ==.
;	..\COMMON\goldseq.c:176: flash_write(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111);
	mov	_flash_write_PARM_2,#0x07
	mov	(_flash_write_PARM_2 + 1),#0x44
	mov	dptr,#0xcc72
	lcall	_flash_write
	C$goldseq.c$179$1$122 ==.
;	..\COMMON\goldseq.c:179: flash_write(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111);
	mov	_flash_write_PARM_2,#0xaf
	mov	(_flash_write_PARM_2 + 1),#0x6a
	mov	dptr,#0xcc74
	lcall	_flash_write
	C$goldseq.c$181$1$122 ==.
;	..\COMMON\goldseq.c:181: flash_write(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111);
	mov	_flash_write_PARM_2,#0x37
	mov	(_flash_write_PARM_2 + 1),#0x3d
	mov	dptr,#0xcc76
	lcall	_flash_write
	C$goldseq.c$184$1$122 ==.
;	..\COMMON\goldseq.c:184: flash_write(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001);
	mov	_flash_write_PARM_2,#0x21
	mov	(_flash_write_PARM_2 + 1),#0x30
	mov	dptr,#0xcc78
	lcall	_flash_write
	C$goldseq.c$186$1$122 ==.
;	..\COMMON\goldseq.c:186: flash_write(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111);
	mov	_flash_write_PARM_2,#0xaf
	mov	(_flash_write_PARM_2 + 1),#0x41
	mov	dptr,#0xcc7a
	lcall	_flash_write
	C$goldseq.c$189$1$122 ==.
;	..\COMMON\goldseq.c:189: flash_write(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110);
	mov	_flash_write_PARM_2,#0x66
	mov	(_flash_write_PARM_2 + 1),#0x1d
	mov	dptr,#0xcc7c
	lcall	_flash_write
	C$goldseq.c$191$1$122 ==.
;	..\COMMON\goldseq.c:191: flash_write(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011);
	mov	_flash_write_PARM_2,#0xe3
	mov	(_flash_write_PARM_2 + 1),#0x7f
	mov	dptr,#0xcc7e
	lcall	_flash_write
	C$goldseq.c$193$1$122 ==.
;	..\COMMON\goldseq.c:193: flash_write(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101);
	mov	_flash_write_PARM_2,#0xc5
	mov	(_flash_write_PARM_2 + 1),#0x0b
	mov	dptr,#0xcc80
	lcall	_flash_write
	C$goldseq.c$195$1$122 ==.
;	..\COMMON\goldseq.c:195: flash_write(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101);
	mov	_flash_write_PARM_2,#0xc5
	mov	(_flash_write_PARM_2 + 1),#0x20
	mov	dptr,#0xcc82
	lcall	_flash_write
	C$goldseq.c$197$1$122 ==.
;	..\COMMON\goldseq.c:197: flash_lock();
	lcall	_flash_lock
	C$goldseq.c$200$1$122 ==.
	XG$GOLDSEQUENCE_Init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'GOLDSEQUENCE_Obtain'
;------------------------------------------------------------
;sloc0                     Allocated with name '_GOLDSEQUENCE_Obtain_sloc0_1_0'
;Rnd                       Allocated with name '_GOLDSEQUENCE_Obtain_Rnd_1_124'
;RetVal                    Allocated with name '_GOLDSEQUENCE_Obtain_RetVal_1_124'
;------------------------------------------------------------
	G$GOLDSEQUENCE_Obtain$0$0 ==.
	C$goldseq.c$209$1$122 ==.
;	..\COMMON\goldseq.c:209: __xdata uint32_t GOLDSEQUENCE_Obtain(void)
;	-----------------------------------------
;	 function GOLDSEQUENCE_Obtain
;	-----------------------------------------
_GOLDSEQUENCE_Obtain:
	C$goldseq.c$214$1$124 ==.
;	..\COMMON\goldseq.c:214: Rnd = RNGBYTE;
	mov	dptr,#_RNGBYTE
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	C$goldseq.c$215$1$124 ==.
;	..\COMMON\goldseq.c:215: flash_unlock();
	push	ar7
	push	ar6
	lcall	_flash_unlock
	C$goldseq.c$217$1$124 ==.
;	..\COMMON\goldseq.c:217: delay_ms(50);
	mov	dptr,#0x0032
	lcall	_delay_ms
	pop	ar6
	pop	ar7
	C$goldseq.c$219$1$124 ==.
;	..\COMMON\goldseq.c:219: Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address
	mov	__moduint_PARM_2,#0x21
	mov	(__moduint_PARM_2 + 1),#0x00
	mov	dpl,r7
	mov	dph,r6
	lcall	__moduint
	mov	r6,dpl
	mov	a,dph
	xch	a,r6
	add	a,acc
	xch	a,r6
	rlc	a
	xch	a,r6
	add	a,acc
	xch	a,r6
	rlc	a
	mov	r7,a
	C$goldseq.c$223$1$124 ==.
;	..\COMMON\goldseq.c:223: RetVal = ((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd+2))<<16;
	mov	a,#0x02
	add	a,r6
	mov	dpl,a
	mov	a,#0xcc
	addc	a,r7
	mov	dph,a
	push	ar7
	push	ar6
	lcall	_flash_read
	mov	r4,dpl
	mov	r5,dph
	pop	ar6
	pop	ar7
	mov	r3,#0x00
	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 3),r5
	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 2),r4
;	1-genFromRTrack replaced	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1),#0x00
	mov	(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1),r3
;	1-genFromRTrack replaced	mov	_GOLDSEQUENCE_Obtain_sloc0_1_0,#0x00
	mov	_GOLDSEQUENCE_Obtain_sloc0_1_0,r3
	C$goldseq.c$225$1$124 ==.
;	..\COMMON\goldseq.c:225: RetVal |= (((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd)) & 0x0000FFFF)/*<<1*/;
	mov	dpl,r6
	mov	a,#0xcc
	add	a,r7
	mov	dph,a
	push	ar7
	push	ar6
	lcall	_flash_read
	mov	r0,dpl
	mov	r1,dph
	clr	a
	mov	r4,a
	mov	r5,a
	mov	a,_GOLDSEQUENCE_Obtain_sloc0_1_0
	orl	ar0,a
	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 1)
	orl	ar1,a
	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 2)
	orl	ar4,a
	mov	a,(_GOLDSEQUENCE_Obtain_sloc0_1_0 + 3)
	orl	ar5,a
	C$goldseq.c$229$1$124 ==.
;	..\COMMON\goldseq.c:229: dbglink_writestr("gold rnd: ");
	mov	dptr,#___str_0
	mov	b,#0x80
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	lcall	_dbglink_writestr
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	C$goldseq.c$230$1$124 ==.
;	..\COMMON\goldseq.c:230: dbglink_writehex16(Rnd, 4, WRNUM_PADZERO);
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	mov	a,#0x08
	push	acc
	rr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$goldseq.c$231$1$124 ==.
;	..\COMMON\goldseq.c:231: dbglink_writestr(" ");
	mov	dptr,#___str_1
	mov	b,#0x80
	lcall	_dbglink_writestr
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	C$goldseq.c$232$1$124 ==.
;	..\COMMON\goldseq.c:232: dbglink_writehex32(RetVal, 8, WRNUM_PADZERO);
	push	ar5
	push	ar4
	push	ar1
	push	ar0
	mov	a,#0x08
	push	acc
	push	acc
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	mov	a,r5
	lcall	_dbglink_writehex32
	dec	sp
	dec	sp
	C$goldseq.c$233$1$124 ==.
;	..\COMMON\goldseq.c:233: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
	C$goldseq.c$236$1$124 ==.
;	..\COMMON\goldseq.c:236: flash_lock();
	lcall	_flash_lock
	pop	ar0
	pop	ar1
	pop	ar4
	pop	ar5
	C$goldseq.c$237$1$124 ==.
;	..\COMMON\goldseq.c:237: return(RetVal);
	mov	dpl,r0
	mov	dph,r1
	mov	b,r4
	mov	a,r5
	C$goldseq.c$238$1$124 ==.
	XG$GOLDSEQUENCE_Obtain$0$0 ==.
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
Fgoldseq$__str_0$0$0 == .
___str_0:
	.ascii "gold rnd: "
	.db 0x00
Fgoldseq$__str_1$0$0 == .
___str_1:
	.ascii " "
	.db 0x00
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
