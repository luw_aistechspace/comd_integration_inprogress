/**
 * @brief configurations of user packet and timing parameters
 * @author luw@aistechspace
 * @internal
 *
 */


#define nRANDOM                 3
#define rangeRANDOM           30

#define slotStdWIDTH_MS      80

#define slotPremWIDTH_MS    slotStdWIDTH_MS*2
#define nslotPrem                 10

#define slotPremBASE            2 // time buffer between received beacon and premium slots in unit of premium slot
#define slotBufPremStd          4 // time buffer between premium slots and standard slots in unit of standard slot
#define slotStdBASE              (slotPremBASE + nslotPrem)* (slotPremWIDTH_MS/slotStdWIDTH_MS) + slotBufPremStd
                                        // time buffer between received beacon and standard slots in unit of standard slot

#define pktLENGTH 45 // on condition that length of GNSS payload is 28 bytes
#define pktBufferLENGTH nRANDOM * pktLENGTH


#define rngDELAY_MS 10
//#define FRAM1

const uint32_t __xdata trmID = 0x100001; // 0x100001 for id1, 0x100003 for id3
const uint8_t __xdata StdPrem = 0x1; // 0x0 for Premium user; 0x1 for Standard user; 0x2 for timing test
