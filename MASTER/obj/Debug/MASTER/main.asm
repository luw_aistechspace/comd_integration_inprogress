;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW64)
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl __sdcc_external_startup
	.globl _wait_dbglink_free
	.globl _dbglink_display_radio_error
	.globl _com0_display_radio_error
	.globl _dbglink_received_packet
	.globl _memcpy
	.globl _com0_writestr
	.globl _com0_setpos
	.globl _com0_init
	.globl _com0_portinit
	.globl _dbglink_writehex32
	.globl _dbglink_writehex16
	.globl _dbglink_writenum16
	.globl _dbglink_writestr
	.globl _dbglink_tx
	.globl _dbglink_init
	.globl _dbglink_txidle
	.globl _uart0_txidle
	.globl _wtimer0_correctinterval
	.globl _wtimer0_addrelative
	.globl _wtimer0_addabsolute
	.globl _wtimer0_curtime
	.globl _wtimer_runcallbacks
	.globl _wtimer_idle
	.globl _wtimer_init
	.globl _wtimer1_setconfig
	.globl _wtimer0_setconfig
	.globl _flash_apply_calibration
	.globl _axradio_commsleepexit
	.globl _axradio_setup_pincfg2
	.globl _axradio_setup_pincfg1
	.globl _axradio_transmit
	.globl _axradio_set_default_remote_address
	.globl _axradio_set_local_address
	.globl _axradio_get_pllvcoi
	.globl _axradio_get_pllrange
	.globl _axradio_set_mode
	.globl _axradio_cansleep
	.globl _axradio_init
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _wt0_beacon
	.globl _rcv_flg_id3
	.globl _rcv_flg_id1
	.globl _pkts_id3
	.globl _pkts_id1
	.globl _rcv_flg
	.globl _bc_counter
	.globl _pkts_missing
	.globl _pkts_received
	.globl _ack_packet
	.globl _beacon_packet
	.globl _wakeup_desc
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
	.globl _coldstart
	.globl _pkt_counter
	.globl _axradio_statuschange
	.globl _enable_radio_interrupt_in_mcu_pin
	.globl _disable_radio_interrupt_in_mcu_pin
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$DPH1$0$0 == 0x0085
_DPH1	=	0x0085
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPL1$0$0 == 0x0084
_DPL1	=	0x0084
G$DPTR0$0$0 == 0x8382
_DPTR0	=	0x8382
G$DPTR1$0$0 == 0x8584
_DPTR1	=	0x8584
G$DPS$0$0 == 0x0086
_DPS	=	0x0086
G$E2IE$0$0 == 0x00a0
_E2IE	=	0x00a0
G$E2IP$0$0 == 0x00c0
_E2IP	=	0x00c0
G$EIE$0$0 == 0x0098
_EIE	=	0x0098
G$EIP$0$0 == 0x00b0
_EIP	=	0x00b0
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$XPAGE$0$0 == 0x00d9
_XPAGE	=	0x00d9
G$_XPAGE$0$0 == 0x00d9
__XPAGE	=	0x00d9
G$ADCCH0CONFIG$0$0 == 0x00ca
_ADCCH0CONFIG	=	0x00ca
G$ADCCH1CONFIG$0$0 == 0x00cb
_ADCCH1CONFIG	=	0x00cb
G$ADCCH2CONFIG$0$0 == 0x00d2
_ADCCH2CONFIG	=	0x00d2
G$ADCCH3CONFIG$0$0 == 0x00d3
_ADCCH3CONFIG	=	0x00d3
G$ADCCLKSRC$0$0 == 0x00d1
_ADCCLKSRC	=	0x00d1
G$ADCCONV$0$0 == 0x00c9
_ADCCONV	=	0x00c9
G$ANALOGCOMP$0$0 == 0x00e1
_ANALOGCOMP	=	0x00e1
G$CLKCON$0$0 == 0x00c6
_CLKCON	=	0x00c6
G$CLKSTAT$0$0 == 0x00c7
_CLKSTAT	=	0x00c7
G$CODECONFIG$0$0 == 0x0097
_CODECONFIG	=	0x0097
G$DBGLNKBUF$0$0 == 0x00e3
_DBGLNKBUF	=	0x00e3
G$DBGLNKSTAT$0$0 == 0x00e2
_DBGLNKSTAT	=	0x00e2
G$DIRA$0$0 == 0x0089
_DIRA	=	0x0089
G$DIRB$0$0 == 0x008a
_DIRB	=	0x008a
G$DIRC$0$0 == 0x008b
_DIRC	=	0x008b
G$DIRR$0$0 == 0x008e
_DIRR	=	0x008e
G$PINA$0$0 == 0x00c8
_PINA	=	0x00c8
G$PINB$0$0 == 0x00e8
_PINB	=	0x00e8
G$PINC$0$0 == 0x00f8
_PINC	=	0x00f8
G$PINR$0$0 == 0x008d
_PINR	=	0x008d
G$PORTA$0$0 == 0x0080
_PORTA	=	0x0080
G$PORTB$0$0 == 0x0088
_PORTB	=	0x0088
G$PORTC$0$0 == 0x0090
_PORTC	=	0x0090
G$PORTR$0$0 == 0x008c
_PORTR	=	0x008c
G$IC0CAPT0$0$0 == 0x00ce
_IC0CAPT0	=	0x00ce
G$IC0CAPT1$0$0 == 0x00cf
_IC0CAPT1	=	0x00cf
G$IC0CAPT$0$0 == 0xcfce
_IC0CAPT	=	0xcfce
G$IC0MODE$0$0 == 0x00cc
_IC0MODE	=	0x00cc
G$IC0STATUS$0$0 == 0x00cd
_IC0STATUS	=	0x00cd
G$IC1CAPT0$0$0 == 0x00d6
_IC1CAPT0	=	0x00d6
G$IC1CAPT1$0$0 == 0x00d7
_IC1CAPT1	=	0x00d7
G$IC1CAPT$0$0 == 0xd7d6
_IC1CAPT	=	0xd7d6
G$IC1MODE$0$0 == 0x00d4
_IC1MODE	=	0x00d4
G$IC1STATUS$0$0 == 0x00d5
_IC1STATUS	=	0x00d5
G$NVADDR0$0$0 == 0x0092
_NVADDR0	=	0x0092
G$NVADDR1$0$0 == 0x0093
_NVADDR1	=	0x0093
G$NVADDR$0$0 == 0x9392
_NVADDR	=	0x9392
G$NVDATA0$0$0 == 0x0094
_NVDATA0	=	0x0094
G$NVDATA1$0$0 == 0x0095
_NVDATA1	=	0x0095
G$NVDATA$0$0 == 0x9594
_NVDATA	=	0x9594
G$NVKEY$0$0 == 0x0096
_NVKEY	=	0x0096
G$NVSTATUS$0$0 == 0x0091
_NVSTATUS	=	0x0091
G$OC0COMP0$0$0 == 0x00bc
_OC0COMP0	=	0x00bc
G$OC0COMP1$0$0 == 0x00bd
_OC0COMP1	=	0x00bd
G$OC0COMP$0$0 == 0xbdbc
_OC0COMP	=	0xbdbc
G$OC0MODE$0$0 == 0x00b9
_OC0MODE	=	0x00b9
G$OC0PIN$0$0 == 0x00ba
_OC0PIN	=	0x00ba
G$OC0STATUS$0$0 == 0x00bb
_OC0STATUS	=	0x00bb
G$OC1COMP0$0$0 == 0x00c4
_OC1COMP0	=	0x00c4
G$OC1COMP1$0$0 == 0x00c5
_OC1COMP1	=	0x00c5
G$OC1COMP$0$0 == 0xc5c4
_OC1COMP	=	0xc5c4
G$OC1MODE$0$0 == 0x00c1
_OC1MODE	=	0x00c1
G$OC1PIN$0$0 == 0x00c2
_OC1PIN	=	0x00c2
G$OC1STATUS$0$0 == 0x00c3
_OC1STATUS	=	0x00c3
G$RADIOACC$0$0 == 0x00b1
_RADIOACC	=	0x00b1
G$RADIOADDR0$0$0 == 0x00b3
_RADIOADDR0	=	0x00b3
G$RADIOADDR1$0$0 == 0x00b2
_RADIOADDR1	=	0x00b2
G$RADIOADDR$0$0 == 0xb2b3
_RADIOADDR	=	0xb2b3
G$RADIODATA0$0$0 == 0x00b7
_RADIODATA0	=	0x00b7
G$RADIODATA1$0$0 == 0x00b6
_RADIODATA1	=	0x00b6
G$RADIODATA2$0$0 == 0x00b5
_RADIODATA2	=	0x00b5
G$RADIODATA3$0$0 == 0x00b4
_RADIODATA3	=	0x00b4
G$RADIODATA$0$0 == 0xb4b5b6b7
_RADIODATA	=	0xb4b5b6b7
G$RADIOSTAT0$0$0 == 0x00be
_RADIOSTAT0	=	0x00be
G$RADIOSTAT1$0$0 == 0x00bf
_RADIOSTAT1	=	0x00bf
G$RADIOSTAT$0$0 == 0xbfbe
_RADIOSTAT	=	0xbfbe
G$SPCLKSRC$0$0 == 0x00df
_SPCLKSRC	=	0x00df
G$SPMODE$0$0 == 0x00dc
_SPMODE	=	0x00dc
G$SPSHREG$0$0 == 0x00de
_SPSHREG	=	0x00de
G$SPSTATUS$0$0 == 0x00dd
_SPSTATUS	=	0x00dd
G$T0CLKSRC$0$0 == 0x009a
_T0CLKSRC	=	0x009a
G$T0CNT0$0$0 == 0x009c
_T0CNT0	=	0x009c
G$T0CNT1$0$0 == 0x009d
_T0CNT1	=	0x009d
G$T0CNT$0$0 == 0x9d9c
_T0CNT	=	0x9d9c
G$T0MODE$0$0 == 0x0099
_T0MODE	=	0x0099
G$T0PERIOD0$0$0 == 0x009e
_T0PERIOD0	=	0x009e
G$T0PERIOD1$0$0 == 0x009f
_T0PERIOD1	=	0x009f
G$T0PERIOD$0$0 == 0x9f9e
_T0PERIOD	=	0x9f9e
G$T0STATUS$0$0 == 0x009b
_T0STATUS	=	0x009b
G$T1CLKSRC$0$0 == 0x00a2
_T1CLKSRC	=	0x00a2
G$T1CNT0$0$0 == 0x00a4
_T1CNT0	=	0x00a4
G$T1CNT1$0$0 == 0x00a5
_T1CNT1	=	0x00a5
G$T1CNT$0$0 == 0xa5a4
_T1CNT	=	0xa5a4
G$T1MODE$0$0 == 0x00a1
_T1MODE	=	0x00a1
G$T1PERIOD0$0$0 == 0x00a6
_T1PERIOD0	=	0x00a6
G$T1PERIOD1$0$0 == 0x00a7
_T1PERIOD1	=	0x00a7
G$T1PERIOD$0$0 == 0xa7a6
_T1PERIOD	=	0xa7a6
G$T1STATUS$0$0 == 0x00a3
_T1STATUS	=	0x00a3
G$T2CLKSRC$0$0 == 0x00aa
_T2CLKSRC	=	0x00aa
G$T2CNT0$0$0 == 0x00ac
_T2CNT0	=	0x00ac
G$T2CNT1$0$0 == 0x00ad
_T2CNT1	=	0x00ad
G$T2CNT$0$0 == 0xadac
_T2CNT	=	0xadac
G$T2MODE$0$0 == 0x00a9
_T2MODE	=	0x00a9
G$T2PERIOD0$0$0 == 0x00ae
_T2PERIOD0	=	0x00ae
G$T2PERIOD1$0$0 == 0x00af
_T2PERIOD1	=	0x00af
G$T2PERIOD$0$0 == 0xafae
_T2PERIOD	=	0xafae
G$T2STATUS$0$0 == 0x00ab
_T2STATUS	=	0x00ab
G$U0CTRL$0$0 == 0x00e4
_U0CTRL	=	0x00e4
G$U0MODE$0$0 == 0x00e7
_U0MODE	=	0x00e7
G$U0SHREG$0$0 == 0x00e6
_U0SHREG	=	0x00e6
G$U0STATUS$0$0 == 0x00e5
_U0STATUS	=	0x00e5
G$U1CTRL$0$0 == 0x00ec
_U1CTRL	=	0x00ec
G$U1MODE$0$0 == 0x00ef
_U1MODE	=	0x00ef
G$U1SHREG$0$0 == 0x00ee
_U1SHREG	=	0x00ee
G$U1STATUS$0$0 == 0x00ed
_U1STATUS	=	0x00ed
G$WDTCFG$0$0 == 0x00da
_WDTCFG	=	0x00da
G$WDTRESET$0$0 == 0x00db
_WDTRESET	=	0x00db
G$WTCFGA$0$0 == 0x00f1
_WTCFGA	=	0x00f1
G$WTCFGB$0$0 == 0x00f9
_WTCFGB	=	0x00f9
G$WTCNTA0$0$0 == 0x00f2
_WTCNTA0	=	0x00f2
G$WTCNTA1$0$0 == 0x00f3
_WTCNTA1	=	0x00f3
G$WTCNTA$0$0 == 0xf3f2
_WTCNTA	=	0xf3f2
G$WTCNTB0$0$0 == 0x00fa
_WTCNTB0	=	0x00fa
G$WTCNTB1$0$0 == 0x00fb
_WTCNTB1	=	0x00fb
G$WTCNTB$0$0 == 0xfbfa
_WTCNTB	=	0xfbfa
G$WTCNTR1$0$0 == 0x00eb
_WTCNTR1	=	0x00eb
G$WTEVTA0$0$0 == 0x00f4
_WTEVTA0	=	0x00f4
G$WTEVTA1$0$0 == 0x00f5
_WTEVTA1	=	0x00f5
G$WTEVTA$0$0 == 0xf5f4
_WTEVTA	=	0xf5f4
G$WTEVTB0$0$0 == 0x00f6
_WTEVTB0	=	0x00f6
G$WTEVTB1$0$0 == 0x00f7
_WTEVTB1	=	0x00f7
G$WTEVTB$0$0 == 0xf7f6
_WTEVTB	=	0xf7f6
G$WTEVTC0$0$0 == 0x00fc
_WTEVTC0	=	0x00fc
G$WTEVTC1$0$0 == 0x00fd
_WTEVTC1	=	0x00fd
G$WTEVTC$0$0 == 0xfdfc
_WTEVTC	=	0xfdfc
G$WTEVTD0$0$0 == 0x00fe
_WTEVTD0	=	0x00fe
G$WTEVTD1$0$0 == 0x00ff
_WTEVTD1	=	0x00ff
G$WTEVTD$0$0 == 0xfffe
_WTEVTD	=	0xfffe
G$WTIRQEN$0$0 == 0x00e9
_WTIRQEN	=	0x00e9
G$WTSTAT$0$0 == 0x00ea
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$ACC_0$0$0 == 0x00e0
_ACC_0	=	0x00e0
G$ACC_1$0$0 == 0x00e1
_ACC_1	=	0x00e1
G$ACC_2$0$0 == 0x00e2
_ACC_2	=	0x00e2
G$ACC_3$0$0 == 0x00e3
_ACC_3	=	0x00e3
G$ACC_4$0$0 == 0x00e4
_ACC_4	=	0x00e4
G$ACC_5$0$0 == 0x00e5
_ACC_5	=	0x00e5
G$ACC_6$0$0 == 0x00e6
_ACC_6	=	0x00e6
G$ACC_7$0$0 == 0x00e7
_ACC_7	=	0x00e7
G$B_0$0$0 == 0x00f0
_B_0	=	0x00f0
G$B_1$0$0 == 0x00f1
_B_1	=	0x00f1
G$B_2$0$0 == 0x00f2
_B_2	=	0x00f2
G$B_3$0$0 == 0x00f3
_B_3	=	0x00f3
G$B_4$0$0 == 0x00f4
_B_4	=	0x00f4
G$B_5$0$0 == 0x00f5
_B_5	=	0x00f5
G$B_6$0$0 == 0x00f6
_B_6	=	0x00f6
G$B_7$0$0 == 0x00f7
_B_7	=	0x00f7
G$E2IE_0$0$0 == 0x00a0
_E2IE_0	=	0x00a0
G$E2IE_1$0$0 == 0x00a1
_E2IE_1	=	0x00a1
G$E2IE_2$0$0 == 0x00a2
_E2IE_2	=	0x00a2
G$E2IE_3$0$0 == 0x00a3
_E2IE_3	=	0x00a3
G$E2IE_4$0$0 == 0x00a4
_E2IE_4	=	0x00a4
G$E2IE_5$0$0 == 0x00a5
_E2IE_5	=	0x00a5
G$E2IE_6$0$0 == 0x00a6
_E2IE_6	=	0x00a6
G$E2IE_7$0$0 == 0x00a7
_E2IE_7	=	0x00a7
G$E2IP_0$0$0 == 0x00c0
_E2IP_0	=	0x00c0
G$E2IP_1$0$0 == 0x00c1
_E2IP_1	=	0x00c1
G$E2IP_2$0$0 == 0x00c2
_E2IP_2	=	0x00c2
G$E2IP_3$0$0 == 0x00c3
_E2IP_3	=	0x00c3
G$E2IP_4$0$0 == 0x00c4
_E2IP_4	=	0x00c4
G$E2IP_5$0$0 == 0x00c5
_E2IP_5	=	0x00c5
G$E2IP_6$0$0 == 0x00c6
_E2IP_6	=	0x00c6
G$E2IP_7$0$0 == 0x00c7
_E2IP_7	=	0x00c7
G$EIE_0$0$0 == 0x0098
_EIE_0	=	0x0098
G$EIE_1$0$0 == 0x0099
_EIE_1	=	0x0099
G$EIE_2$0$0 == 0x009a
_EIE_2	=	0x009a
G$EIE_3$0$0 == 0x009b
_EIE_3	=	0x009b
G$EIE_4$0$0 == 0x009c
_EIE_4	=	0x009c
G$EIE_5$0$0 == 0x009d
_EIE_5	=	0x009d
G$EIE_6$0$0 == 0x009e
_EIE_6	=	0x009e
G$EIE_7$0$0 == 0x009f
_EIE_7	=	0x009f
G$EIP_0$0$0 == 0x00b0
_EIP_0	=	0x00b0
G$EIP_1$0$0 == 0x00b1
_EIP_1	=	0x00b1
G$EIP_2$0$0 == 0x00b2
_EIP_2	=	0x00b2
G$EIP_3$0$0 == 0x00b3
_EIP_3	=	0x00b3
G$EIP_4$0$0 == 0x00b4
_EIP_4	=	0x00b4
G$EIP_5$0$0 == 0x00b5
_EIP_5	=	0x00b5
G$EIP_6$0$0 == 0x00b6
_EIP_6	=	0x00b6
G$EIP_7$0$0 == 0x00b7
_EIP_7	=	0x00b7
G$IE_0$0$0 == 0x00a8
_IE_0	=	0x00a8
G$IE_1$0$0 == 0x00a9
_IE_1	=	0x00a9
G$IE_2$0$0 == 0x00aa
_IE_2	=	0x00aa
G$IE_3$0$0 == 0x00ab
_IE_3	=	0x00ab
G$IE_4$0$0 == 0x00ac
_IE_4	=	0x00ac
G$IE_5$0$0 == 0x00ad
_IE_5	=	0x00ad
G$IE_6$0$0 == 0x00ae
_IE_6	=	0x00ae
G$IE_7$0$0 == 0x00af
_IE_7	=	0x00af
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$IP_0$0$0 == 0x00b8
_IP_0	=	0x00b8
G$IP_1$0$0 == 0x00b9
_IP_1	=	0x00b9
G$IP_2$0$0 == 0x00ba
_IP_2	=	0x00ba
G$IP_3$0$0 == 0x00bb
_IP_3	=	0x00bb
G$IP_4$0$0 == 0x00bc
_IP_4	=	0x00bc
G$IP_5$0$0 == 0x00bd
_IP_5	=	0x00bd
G$IP_6$0$0 == 0x00be
_IP_6	=	0x00be
G$IP_7$0$0 == 0x00bf
_IP_7	=	0x00bf
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$PINA_0$0$0 == 0x00c8
_PINA_0	=	0x00c8
G$PINA_1$0$0 == 0x00c9
_PINA_1	=	0x00c9
G$PINA_2$0$0 == 0x00ca
_PINA_2	=	0x00ca
G$PINA_3$0$0 == 0x00cb
_PINA_3	=	0x00cb
G$PINA_4$0$0 == 0x00cc
_PINA_4	=	0x00cc
G$PINA_5$0$0 == 0x00cd
_PINA_5	=	0x00cd
G$PINA_6$0$0 == 0x00ce
_PINA_6	=	0x00ce
G$PINA_7$0$0 == 0x00cf
_PINA_7	=	0x00cf
G$PINB_0$0$0 == 0x00e8
_PINB_0	=	0x00e8
G$PINB_1$0$0 == 0x00e9
_PINB_1	=	0x00e9
G$PINB_2$0$0 == 0x00ea
_PINB_2	=	0x00ea
G$PINB_3$0$0 == 0x00eb
_PINB_3	=	0x00eb
G$PINB_4$0$0 == 0x00ec
_PINB_4	=	0x00ec
G$PINB_5$0$0 == 0x00ed
_PINB_5	=	0x00ed
G$PINB_6$0$0 == 0x00ee
_PINB_6	=	0x00ee
G$PINB_7$0$0 == 0x00ef
_PINB_7	=	0x00ef
G$PINC_0$0$0 == 0x00f8
_PINC_0	=	0x00f8
G$PINC_1$0$0 == 0x00f9
_PINC_1	=	0x00f9
G$PINC_2$0$0 == 0x00fa
_PINC_2	=	0x00fa
G$PINC_3$0$0 == 0x00fb
_PINC_3	=	0x00fb
G$PINC_4$0$0 == 0x00fc
_PINC_4	=	0x00fc
G$PINC_5$0$0 == 0x00fd
_PINC_5	=	0x00fd
G$PINC_6$0$0 == 0x00fe
_PINC_6	=	0x00fe
G$PINC_7$0$0 == 0x00ff
_PINC_7	=	0x00ff
G$PORTA_0$0$0 == 0x0080
_PORTA_0	=	0x0080
G$PORTA_1$0$0 == 0x0081
_PORTA_1	=	0x0081
G$PORTA_2$0$0 == 0x0082
_PORTA_2	=	0x0082
G$PORTA_3$0$0 == 0x0083
_PORTA_3	=	0x0083
G$PORTA_4$0$0 == 0x0084
_PORTA_4	=	0x0084
G$PORTA_5$0$0 == 0x0085
_PORTA_5	=	0x0085
G$PORTA_6$0$0 == 0x0086
_PORTA_6	=	0x0086
G$PORTA_7$0$0 == 0x0087
_PORTA_7	=	0x0087
G$PORTB_0$0$0 == 0x0088
_PORTB_0	=	0x0088
G$PORTB_1$0$0 == 0x0089
_PORTB_1	=	0x0089
G$PORTB_2$0$0 == 0x008a
_PORTB_2	=	0x008a
G$PORTB_3$0$0 == 0x008b
_PORTB_3	=	0x008b
G$PORTB_4$0$0 == 0x008c
_PORTB_4	=	0x008c
G$PORTB_5$0$0 == 0x008d
_PORTB_5	=	0x008d
G$PORTB_6$0$0 == 0x008e
_PORTB_6	=	0x008e
G$PORTB_7$0$0 == 0x008f
_PORTB_7	=	0x008f
G$PORTC_0$0$0 == 0x0090
_PORTC_0	=	0x0090
G$PORTC_1$0$0 == 0x0091
_PORTC_1	=	0x0091
G$PORTC_2$0$0 == 0x0092
_PORTC_2	=	0x0092
G$PORTC_3$0$0 == 0x0093
_PORTC_3	=	0x0093
G$PORTC_4$0$0 == 0x0094
_PORTC_4	=	0x0094
G$PORTC_5$0$0 == 0x0095
_PORTC_5	=	0x0095
G$PORTC_6$0$0 == 0x0096
_PORTC_6	=	0x0096
G$PORTC_7$0$0 == 0x0097
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
G$pkt_counter$0$0==.
_pkt_counter::
	.ds 2
G$coldstart$0$0==.
_coldstart::
	.ds 1
Lmain.axradio_statuschange$st$1$351==.
_axradio_statuschange_st_1_351:
	.ds 2
Lmain.axradio_statuschange$pktdata$3$358==.
_axradio_statuschange_pktdata_3_358:
	.ds 2
Lmain.main$saved_button_state$1$378==.
_main_saved_button_state_1_378:
	.ds 1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
Lmain._sdcc_external_startup$sloc0$1$0==.
__sdcc_external_startup_sloc0_1_0:
	.ds 1
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
G$ADCCH0VAL0$0$0 == 0x7020
_ADCCH0VAL0	=	0x7020
G$ADCCH0VAL1$0$0 == 0x7021
_ADCCH0VAL1	=	0x7021
G$ADCCH0VAL$0$0 == 0x7020
_ADCCH0VAL	=	0x7020
G$ADCCH1VAL0$0$0 == 0x7022
_ADCCH1VAL0	=	0x7022
G$ADCCH1VAL1$0$0 == 0x7023
_ADCCH1VAL1	=	0x7023
G$ADCCH1VAL$0$0 == 0x7022
_ADCCH1VAL	=	0x7022
G$ADCCH2VAL0$0$0 == 0x7024
_ADCCH2VAL0	=	0x7024
G$ADCCH2VAL1$0$0 == 0x7025
_ADCCH2VAL1	=	0x7025
G$ADCCH2VAL$0$0 == 0x7024
_ADCCH2VAL	=	0x7024
G$ADCCH3VAL0$0$0 == 0x7026
_ADCCH3VAL0	=	0x7026
G$ADCCH3VAL1$0$0 == 0x7027
_ADCCH3VAL1	=	0x7027
G$ADCCH3VAL$0$0 == 0x7026
_ADCCH3VAL	=	0x7026
G$ADCTUNE0$0$0 == 0x7028
_ADCTUNE0	=	0x7028
G$ADCTUNE1$0$0 == 0x7029
_ADCTUNE1	=	0x7029
G$ADCTUNE2$0$0 == 0x702a
_ADCTUNE2	=	0x702a
G$DMA0ADDR0$0$0 == 0x7010
_DMA0ADDR0	=	0x7010
G$DMA0ADDR1$0$0 == 0x7011
_DMA0ADDR1	=	0x7011
G$DMA0ADDR$0$0 == 0x7010
_DMA0ADDR	=	0x7010
G$DMA0CONFIG$0$0 == 0x7014
_DMA0CONFIG	=	0x7014
G$DMA1ADDR0$0$0 == 0x7012
_DMA1ADDR0	=	0x7012
G$DMA1ADDR1$0$0 == 0x7013
_DMA1ADDR1	=	0x7013
G$DMA1ADDR$0$0 == 0x7012
_DMA1ADDR	=	0x7012
G$DMA1CONFIG$0$0 == 0x7015
_DMA1CONFIG	=	0x7015
G$FRCOSCCONFIG$0$0 == 0x7070
_FRCOSCCONFIG	=	0x7070
G$FRCOSCCTRL$0$0 == 0x7071
_FRCOSCCTRL	=	0x7071
G$FRCOSCFREQ0$0$0 == 0x7076
_FRCOSCFREQ0	=	0x7076
G$FRCOSCFREQ1$0$0 == 0x7077
_FRCOSCFREQ1	=	0x7077
G$FRCOSCFREQ$0$0 == 0x7076
_FRCOSCFREQ	=	0x7076
G$FRCOSCKFILT0$0$0 == 0x7072
_FRCOSCKFILT0	=	0x7072
G$FRCOSCKFILT1$0$0 == 0x7073
_FRCOSCKFILT1	=	0x7073
G$FRCOSCKFILT$0$0 == 0x7072
_FRCOSCKFILT	=	0x7072
G$FRCOSCPER0$0$0 == 0x7078
_FRCOSCPER0	=	0x7078
G$FRCOSCPER1$0$0 == 0x7079
_FRCOSCPER1	=	0x7079
G$FRCOSCPER$0$0 == 0x7078
_FRCOSCPER	=	0x7078
G$FRCOSCREF0$0$0 == 0x7074
_FRCOSCREF0	=	0x7074
G$FRCOSCREF1$0$0 == 0x7075
_FRCOSCREF1	=	0x7075
G$FRCOSCREF$0$0 == 0x7074
_FRCOSCREF	=	0x7074
G$ANALOGA$0$0 == 0x7007
_ANALOGA	=	0x7007
G$GPIOENABLE$0$0 == 0x700c
_GPIOENABLE	=	0x700c
G$EXTIRQ$0$0 == 0x7003
_EXTIRQ	=	0x7003
G$INTCHGA$0$0 == 0x7000
_INTCHGA	=	0x7000
G$INTCHGB$0$0 == 0x7001
_INTCHGB	=	0x7001
G$INTCHGC$0$0 == 0x7002
_INTCHGC	=	0x7002
G$PALTA$0$0 == 0x7008
_PALTA	=	0x7008
G$PALTB$0$0 == 0x7009
_PALTB	=	0x7009
G$PALTC$0$0 == 0x700a
_PALTC	=	0x700a
G$PALTRADIO$0$0 == 0x7046
_PALTRADIO	=	0x7046
G$PINCHGA$0$0 == 0x7004
_PINCHGA	=	0x7004
G$PINCHGB$0$0 == 0x7005
_PINCHGB	=	0x7005
G$PINCHGC$0$0 == 0x7006
_PINCHGC	=	0x7006
G$PINSEL$0$0 == 0x700b
_PINSEL	=	0x700b
G$LPOSCCONFIG$0$0 == 0x7060
_LPOSCCONFIG	=	0x7060
G$LPOSCFREQ0$0$0 == 0x7066
_LPOSCFREQ0	=	0x7066
G$LPOSCFREQ1$0$0 == 0x7067
_LPOSCFREQ1	=	0x7067
G$LPOSCFREQ$0$0 == 0x7066
_LPOSCFREQ	=	0x7066
G$LPOSCKFILT0$0$0 == 0x7062
_LPOSCKFILT0	=	0x7062
G$LPOSCKFILT1$0$0 == 0x7063
_LPOSCKFILT1	=	0x7063
G$LPOSCKFILT$0$0 == 0x7062
_LPOSCKFILT	=	0x7062
G$LPOSCPER0$0$0 == 0x7068
_LPOSCPER0	=	0x7068
G$LPOSCPER1$0$0 == 0x7069
_LPOSCPER1	=	0x7069
G$LPOSCPER$0$0 == 0x7068
_LPOSCPER	=	0x7068
G$LPOSCREF0$0$0 == 0x7064
_LPOSCREF0	=	0x7064
G$LPOSCREF1$0$0 == 0x7065
_LPOSCREF1	=	0x7065
G$LPOSCREF$0$0 == 0x7064
_LPOSCREF	=	0x7064
G$LPXOSCGM$0$0 == 0x7054
_LPXOSCGM	=	0x7054
G$MISCCTRL$0$0 == 0x7f01
_MISCCTRL	=	0x7f01
G$OSCCALIB$0$0 == 0x7053
_OSCCALIB	=	0x7053
G$OSCFORCERUN$0$0 == 0x7050
_OSCFORCERUN	=	0x7050
G$OSCREADY$0$0 == 0x7052
_OSCREADY	=	0x7052
G$OSCRUN$0$0 == 0x7051
_OSCRUN	=	0x7051
G$RADIOFDATAADDR0$0$0 == 0x7040
_RADIOFDATAADDR0	=	0x7040
G$RADIOFDATAADDR1$0$0 == 0x7041
_RADIOFDATAADDR1	=	0x7041
G$RADIOFDATAADDR$0$0 == 0x7040
_RADIOFDATAADDR	=	0x7040
G$RADIOFSTATADDR0$0$0 == 0x7042
_RADIOFSTATADDR0	=	0x7042
G$RADIOFSTATADDR1$0$0 == 0x7043
_RADIOFSTATADDR1	=	0x7043
G$RADIOFSTATADDR$0$0 == 0x7042
_RADIOFSTATADDR	=	0x7042
G$RADIOMUX$0$0 == 0x7044
_RADIOMUX	=	0x7044
G$SCRATCH0$0$0 == 0x7084
_SCRATCH0	=	0x7084
G$SCRATCH1$0$0 == 0x7085
_SCRATCH1	=	0x7085
G$SCRATCH2$0$0 == 0x7086
_SCRATCH2	=	0x7086
G$SCRATCH3$0$0 == 0x7087
_SCRATCH3	=	0x7087
G$SILICONREV$0$0 == 0x7f00
_SILICONREV	=	0x7f00
G$XTALAMPL$0$0 == 0x7f19
_XTALAMPL	=	0x7f19
G$XTALOSC$0$0 == 0x7f18
_XTALOSC	=	0x7f18
G$XTALREADY$0$0 == 0x7f1a
_XTALREADY	=	0x7f1a
Fmain$flash_deviceid$0$0 == 0xfc06
_flash_deviceid	=	0xfc06
Fmain$flash_calsector$0$0 == 0xfc00
_flash_calsector	=	0xfc00
G$wakeup_desc$0$0==.
_wakeup_desc::
	.ds 8
Lmain.transmit_packet$demo_packet_$1$342==.
_transmit_packet_demo_packet__1_342:
	.ds 1
Lmain.transmit_beacon$beacon_packet_$1$345==.
_transmit_beacon_beacon_packet__1_345:
	.ds 8
Lmain.transmit_ack$ack_packet_$1$348==.
_transmit_ack_ack_packet__1_348:
	.ds 4
Lmain.wakeup_callback$timer0Period$1$369==.
_wakeup_callback_timer0Period_1_369:
	.ds 2
Lmain.wakeup_callback$bc_flg$1$369==.
_wakeup_callback_bc_flg_1_369:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
G$beacon_packet$0$0==.
_beacon_packet::
	.ds 8
G$ack_packet$0$0==.
_ack_packet::
	.ds 4
G$pkts_received$0$0==.
_pkts_received::
	.ds 2
G$pkts_missing$0$0==.
_pkts_missing::
	.ds 2
G$bc_counter$0$0==.
_bc_counter::
	.ds 2
G$rcv_flg$0$0==.
_rcv_flg::
	.ds 1
G$pkts_id1$0$0==.
_pkts_id1::
	.ds 2
G$pkts_id3$0$0==.
_pkts_id3::
	.ds 2
G$rcv_flg_id1$0$0==.
_rcv_flg_id1::
	.ds 1
G$rcv_flg_id3$0$0==.
_rcv_flg_id3::
	.ds 1
G$wt0_beacon$0$0==.
_wt0_beacon::
	.ds 4
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	ljmp	_wtimer_irq
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_axradio_isr
	.ds	5
	reti
	.ds	7
	ljmp	_pwrmgmt_irq
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_uart0_irq
	.ds	5
	ljmp	_uart1_irq
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_dbglink_irq
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;saved_button_state        Allocated with name '_main_saved_button_state_1_378'
;i                         Allocated to registers r7 
;x                         Allocated to registers r6 
;flg                       Allocated to registers r6 
;flg                       Allocated to registers r7 
;------------------------------------------------------------
	G$main$0$0 ==.
	C$main.c$428$1$378 ==.
;	main.c:428: static uint8_t __data saved_button_state = 0xFF;
	mov	_main_saved_button_state_1_378,#0xff
	C$main.c$80$1$378 ==.
;	main.c:80: uint16_t __data pkt_counter = 0;
	clr	a
	mov	_pkt_counter,a
	mov	(_pkt_counter + 1),a
	C$main.c$81$1$378 ==.
;	main.c:81: uint8_t __data coldstart = 1; /* caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */
	mov	_coldstart,#0x01
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
__sdcc_program_startup:
	ljmp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'pwrmgmt_irq'
;------------------------------------------------------------
;pc                        Allocated to registers r7 
;------------------------------------------------------------
	Fmain$pwrmgmt_irq$0$0 ==.
	C$main.c$98$0$0 ==.
;	main.c:98: static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
;	-----------------------------------------
;	 function pwrmgmt_irq
;	-----------------------------------------
_pwrmgmt_irq:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	push	acc
	push	dpl
	push	dph
	push	ar7
	push	psw
	mov	psw,#0x00
	C$main.c$100$1$0 ==.
;	main.c:100: uint8_t pc = PCON;
	C$main.c$102$1$340 ==.
;	main.c:102: if (!(pc & 0x80))
	mov	a,_PCON
	mov	r7,a
	jb	acc.7,00102$
	C$main.c$103$1$340 ==.
;	main.c:103: return;
	sjmp	00106$
00102$:
	C$main.c$105$1$340 ==.
;	main.c:105: GPIOENABLE = 0;
	mov	dptr,#_GPIOENABLE
	clr	a
	movx	@dptr,a
	C$main.c$106$1$340 ==.
;	main.c:106: IE = EIE = E2IE = 0;
;	1-genFromRTrack replaced	mov	_E2IE,#0x00
	mov	_E2IE,a
;	1-genFromRTrack replaced	mov	_EIE,#0x00
	mov	_EIE,a
;	1-genFromRTrack replaced	mov	_IE,#0x00
	mov	_IE,a
00104$:
	C$main.c$109$1$340 ==.
;	main.c:109: PCON |= 0x01;
	orl	_PCON,#0x01
	sjmp	00104$
00106$:
	pop	psw
	pop	ar7
	pop	dph
	pop	dpl
	pop	acc
	C$main.c$110$1$340 ==.
	XFmain$pwrmgmt_irq$0$0 ==.
	reti
;	eliminated unneeded push/pop b
;------------------------------------------------------------
;Allocation info for local variables in function 'transmit_packet'
;------------------------------------------------------------
;demo_packet_              Allocated with name '_transmit_packet_demo_packet__1_342'
;------------------------------------------------------------
	Fmain$transmit_packet$0$0 ==.
	C$main.c$112$1$340 ==.
;	main.c:112: static void transmit_packet(void)
;	-----------------------------------------
;	 function transmit_packet
;	-----------------------------------------
_transmit_packet:
	C$main.c$116$1$342 ==.
;	main.c:116: ++pkt_counter;
	inc	_pkt_counter
	clr	a
	cjne	a,_pkt_counter,00108$
	inc	(_pkt_counter + 1)
00108$:
	C$main.c$117$1$342 ==.
;	main.c:117: memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
	mov	_memcpy_PARM_2,#_demo_packet
	mov	(_memcpy_PARM_2 + 1),#(_demo_packet >> 8)
	mov	(_memcpy_PARM_2 + 2),#0x80
	mov	_memcpy_PARM_3,#0x01
	mov	(_memcpy_PARM_3 + 1),#0x00
	mov	dptr,#_transmit_packet_demo_packet__1_342
	mov	b,#0x00
	lcall	_memcpy
	C$main.c$119$1$342 ==.
;	main.c:119: if (framing_insert_counter)
	mov	dptr,#_framing_insert_counter
	clr	a
	movc	a,@a+dptr
	jz	00102$
	C$main.c$121$2$343 ==.
;	main.c:121: demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
	mov	dptr,#_framing_counter_pos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	add	a,#_transmit_packet_demo_packet__1_342
	mov	dpl,a
	clr	a
	addc	a,#(_transmit_packet_demo_packet__1_342 >> 8)
	mov	dph,a
	mov	r5,_pkt_counter
	mov	r6,#0x00
	mov	a,r5
	movx	@dptr,a
	C$main.c$122$2$343 ==.
;	main.c:122: demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
	mov	a,r7
	inc	a
	add	a,#_transmit_packet_demo_packet__1_342
	mov	dpl,a
	clr	a
	addc	a,#(_transmit_packet_demo_packet__1_342 >> 8)
	mov	dph,a
	mov	a,(_pkt_counter + 1)
	mov	r7,a
	movx	@dptr,a
00102$:
	C$main.c$125$1$342 ==.
;	main.c:125: axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));
	mov	_axradio_transmit_PARM_2,#_transmit_packet_demo_packet__1_342
	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_packet_demo_packet__1_342 >> 8)
	mov	(_axradio_transmit_PARM_2 + 2),#0x00
	mov	_axradio_transmit_PARM_3,#0x01
	mov	(_axradio_transmit_PARM_3 + 1),#0x00
	mov	dptr,#_remoteaddr
	mov	b,#0x80
	lcall	_axradio_transmit
	C$main.c$126$1$342 ==.
	XFmain$transmit_packet$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'transmit_beacon'
;------------------------------------------------------------
;beacon_packet_            Allocated with name '_transmit_beacon_beacon_packet__1_345'
;------------------------------------------------------------
	Fmain$transmit_beacon$0$0 ==.
	C$main.c$142$1$342 ==.
;	main.c:142: static void transmit_beacon(void)
;	-----------------------------------------
;	 function transmit_beacon
;	-----------------------------------------
_transmit_beacon:
	C$main.c$148$1$345 ==.
;	main.c:148: ++bc_counter;
	mov	dptr,#_bc_counter
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
	inc	dptr
	movx	a,@dptr
	addc	a,#0x00
	movx	@dptr,a
	C$main.c$150$1$345 ==.
;	main.c:150: memcpy(beacon_packet_, beacon_packet, sizeof(beacon_packet));
	mov	_memcpy_PARM_2,#_beacon_packet
	mov	(_memcpy_PARM_2 + 1),#(_beacon_packet >> 8)
	mov	(_memcpy_PARM_2 + 2),#0x00
	mov	_memcpy_PARM_3,#0x08
	mov	(_memcpy_PARM_3 + 1),#0x00
	mov	dptr,#_transmit_beacon_beacon_packet__1_345
	mov	b,#0x00
	lcall	_memcpy
	C$main.c$152$1$345 ==.
;	main.c:152: if (framing_insert_counter)
	mov	dptr,#_framing_insert_counter
	clr	a
	movc	a,@a+dptr
	jz	00102$
	C$main.c$154$2$346 ==.
;	main.c:154: beacon_packet_[framing_counter_pos] = bc_counter & 0xFF;
	mov	dptr,#_framing_counter_pos
	clr	a
	movc	a,@a+dptr
	mov	r7,a
	add	a,#_transmit_beacon_beacon_packet__1_345
	mov	r5,a
	clr	a
	addc	a,#(_transmit_beacon_beacon_packet__1_345 >> 8)
	mov	r6,a
	mov	dptr,#_bc_counter
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r5
	mov	dph,r6
	mov	a,r3
	movx	@dptr,a
	C$main.c$155$2$346 ==.
;	main.c:155: beacon_packet_[framing_counter_pos + 1] = (bc_counter >> 8) & 0xFF;
	mov	a,r7
	inc	a
	add	a,#_transmit_beacon_beacon_packet__1_345
	mov	r6,a
	clr	a
	addc	a,#(_transmit_beacon_beacon_packet__1_345 >> 8)
	mov	r7,a
	mov	dptr,#_bc_counter
	movx	a,@dptr
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	mov	r4,a
	mov	dpl,r6
	mov	dph,r7
	movx	@dptr,a
00102$:
	C$main.c$157$1$345 ==.
;	main.c:157: wt0_beacon = wtimer0_curtime();
	lcall	_wtimer0_curtime
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	dptr,#_wt0_beacon
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	C$main.c$158$1$345 ==.
;	main.c:158: axradio_transmit(&remoteaddr, beacon_packet_, sizeof(beacon_packet));
	mov	_axradio_transmit_PARM_2,#_transmit_beacon_beacon_packet__1_345
	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_beacon_beacon_packet__1_345 >> 8)
	mov	(_axradio_transmit_PARM_2 + 2),#0x00
	mov	_axradio_transmit_PARM_3,#0x08
	mov	(_axradio_transmit_PARM_3 + 1),#0x00
	mov	dptr,#_remoteaddr
	mov	b,#0x80
	lcall	_axradio_transmit
	C$main.c$161$1$345 ==.
;	main.c:161: dbglink_writestr("Beacon: ");
	mov	dptr,#___str_0
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$162$1$345 ==.
;	main.c:162: dbglink_writehex16(bc_counter, 4, WRNUM_PADZERO);
	mov	dptr,#_bc_counter
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x08
	push	acc
	rr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$163$1$345 ==.
;	main.c:163: dbglink_writestr("  ");
	mov	dptr,#___str_1
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$165$1$345 ==.
;	main.c:165: dbglink_writestr("\n");
	mov	dptr,#___str_2
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$167$1$345 ==.
	XFmain$transmit_beacon$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'transmit_ack'
;------------------------------------------------------------
;ack_packet_               Allocated with name '_transmit_ack_ack_packet__1_348'
;------------------------------------------------------------
	Fmain$transmit_ack$0$0 ==.
	C$main.c$175$1$345 ==.
;	main.c:175: static void transmit_ack(void)
;	-----------------------------------------
;	 function transmit_ack
;	-----------------------------------------
_transmit_ack:
	C$main.c$179$1$348 ==.
;	main.c:179: memcpy(ack_packet_, ack_packet, sizeof(ack_packet));
	mov	_memcpy_PARM_2,#_ack_packet
	mov	(_memcpy_PARM_2 + 1),#(_ack_packet >> 8)
	mov	(_memcpy_PARM_2 + 2),#0x00
	mov	_memcpy_PARM_3,#0x04
	mov	(_memcpy_PARM_3 + 1),#0x00
	mov	dptr,#_transmit_ack_ack_packet__1_348
	mov	b,#0x00
	lcall	_memcpy
	C$main.c$180$1$348 ==.
;	main.c:180: if(rcv_flg_id1 > 0)
	mov	dptr,#_rcv_flg_id1
	movx	a,@dptr
	jz	00102$
	C$main.c$184$2$349 ==.
;	main.c:184: ack_packet_[0] = ack_packet_[0]|0x01;
	mov	dptr,#_transmit_ack_ack_packet__1_348
	movx	a,@dptr
	orl	a,#0x01
	mov	r7,a
	movx	@dptr,a
00102$:
	C$main.c$186$1$348 ==.
;	main.c:186: if(rcv_flg_id3 > 0)
	mov	dptr,#_rcv_flg_id3
	movx	a,@dptr
	jz	00104$
	C$main.c$189$2$350 ==.
;	main.c:189: ack_packet_[0] = ack_packet_[0]|0x04;
	mov	dptr,#_transmit_ack_ack_packet__1_348
	movx	a,@dptr
	orl	a,#0x04
	mov	r7,a
	movx	@dptr,a
00104$:
	C$main.c$196$1$348 ==.
;	main.c:196: axradio_transmit(&remoteaddr, ack_packet_, sizeof(ack_packet));
	mov	_axradio_transmit_PARM_2,#_transmit_ack_ack_packet__1_348
	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_ack_ack_packet__1_348 >> 8)
	mov	(_axradio_transmit_PARM_2 + 2),#0x00
	mov	_axradio_transmit_PARM_3,#0x04
	mov	(_axradio_transmit_PARM_3 + 1),#0x00
	mov	dptr,#_remoteaddr
	mov	b,#0x80
	lcall	_axradio_transmit
	C$main.c$199$1$348 ==.
;	main.c:199: wait_dbglink_free();
	lcall	_wait_dbglink_free
	C$main.c$200$1$348 ==.
;	main.c:200: dbglink_writestr("ACK: ");
	mov	dptr,#___str_3
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$201$1$348 ==.
;	main.c:201: dbglink_writehex16(rcv_flg, 2, WRNUM_SIGNED);
	mov	dptr,#_rcv_flg
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	mov	a,#0x01
	push	acc
	inc	a
	push	acc
	mov	dpl,r7
	mov	dph,r6
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$202$1$348 ==.
;	main.c:202: dbglink_writestr(" ");
	mov	dptr,#___str_4
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$203$1$348 ==.
;	main.c:203: dbglink_writehex16(pkts_received, 4, WRNUM_PADZERO);
	mov	dptr,#_pkts_received
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x08
	push	acc
	rr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$204$1$348 ==.
;	main.c:204: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
	C$main.c$205$1$348 ==.
;	main.c:205: dbglink_writestr("ID1: ");
	mov	dptr,#___str_5
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$206$1$348 ==.
;	main.c:206: dbglink_writehex16(rcv_flg_id1, 2, WRNUM_SIGNED);
	mov	dptr,#_rcv_flg_id1
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	mov	a,#0x01
	push	acc
	inc	a
	push	acc
	mov	dpl,r7
	mov	dph,r6
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$207$1$348 ==.
;	main.c:207: dbglink_writestr(" ");
	mov	dptr,#___str_4
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$208$1$348 ==.
;	main.c:208: dbglink_writehex16(pkts_id1, 4, WRNUM_PADZERO);
	mov	dptr,#_pkts_id1
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x08
	push	acc
	rr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$209$1$348 ==.
;	main.c:209: dbglink_writestr("\nID3: ");
	mov	dptr,#___str_6
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$211$1$348 ==.
;	main.c:211: dbglink_writehex16(rcv_flg_id3, 2, WRNUM_SIGNED);
	mov	dptr,#_rcv_flg_id3
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	mov	a,#0x01
	push	acc
	inc	a
	push	acc
	mov	dpl,r7
	mov	dph,r6
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$212$1$348 ==.
;	main.c:212: dbglink_writestr(" ");
	mov	dptr,#___str_4
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$213$1$348 ==.
;	main.c:213: dbglink_writehex16(pkts_id3, 4, WRNUM_PADZERO);
	mov	dptr,#_pkts_id3
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,#0x08
	push	acc
	rr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
	C$main.c$215$1$348 ==.
;	main.c:215: dbglink_writestr("\n\n");
	mov	dptr,#___str_7
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$217$1$348 ==.
	XFmain$transmit_ack$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'axradio_statuschange'
;------------------------------------------------------------
;st                        Allocated with name '_axradio_statuschange_st_1_351'
;pktdata                   Allocated with name '_axradio_statuschange_pktdata_3_358'
;------------------------------------------------------------
	G$axradio_statuschange$0$0 ==.
	C$main.c$221$1$348 ==.
;	main.c:221: void axradio_statuschange(struct axradio_status __xdata *st)
;	-----------------------------------------
;	 function axradio_statuschange
;	-----------------------------------------
_axradio_statuschange:
	C$main.c$235$1$352 ==.
;	main.c:235: switch (st->status)
	mov	_axradio_statuschange_st_1_351,dpl
	mov  (_axradio_statuschange_st_1_351 + 1),dph
	movx	a,@dptr
	mov	r5,a
	jz	00116$
	cjne	r5,#0x02,00156$
	ljmp	00129$
00156$:
	cjne	r5,#0x03,00157$
	sjmp	00105$
00157$:
	cjne	r5,#0x04,00158$
	sjmp	00112$
00158$:
	ljmp	00129$
	C$main.c$238$2$353 ==.
;	main.c:238: led0_on();
00105$:
	setb	_PORTB_1
	C$main.c$239$2$353 ==.
;	main.c:239: break;
	ljmp	00129$
	C$main.c$242$2$353 ==.
;	main.c:242: led0_off();
00112$:
	clr	_PORTB_1
	C$main.c$243$2$353 ==.
;	main.c:243: break;
	ljmp	00129$
	C$main.c$248$2$353 ==.
;	main.c:248: case AXRADIO_STAT_RECEIVE:
00116$:
	C$main.c$251$3$358 ==.
;	main.c:251: const uint8_t __xdata *pktdata = st->u.rx.mac.raw;
	mov	a,#0x06
	add	a,_axradio_statuschange_st_1_351
	mov	r4,a
	clr	a
	addc	a,(_axradio_statuschange_st_1_351 + 1)
	mov	r5,a
	mov	a,#0x14
	add	a,r4
	mov	dpl,a
	clr	a
	addc	a,r5
	mov	dph,a
	movx	a,@dptr
	mov	_axradio_statuschange_pktdata_3_358,a
	inc	dptr
	movx	a,@dptr
	mov	(_axradio_statuschange_pktdata_3_358 + 1),a
	C$main.c$253$3$358 ==.
;	main.c:253: if (st->error == AXRADIO_ERR_NOERROR)
	mov	a,#0x01
	add	a,_axradio_statuschange_st_1_351
	mov	r2,a
	clr	a
	addc	a,(_axradio_statuschange_st_1_351 + 1)
	mov	r3,a
	mov	dpl,r2
	mov	dph,r3
	movx	a,@dptr
	jz	00159$
	ljmp	00125$
00159$:
	C$main.c$256$4$359 ==.
;	main.c:256: dbglink_writestr("40_PKT_WT0:");
	mov	dptr,#___str_8
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$257$4$359 ==.
;	main.c:257: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
	lcall	_wtimer0_curtime
	mov	r0,dpl
	mov	r1,dph
	mov	r2,b
	mov	r3,a
	mov	a,#0x08
	push	acc
	push	acc
	mov	dpl,r0
	mov	dph,r1
	mov	b,r2
	mov	a,r3
	lcall	_dbglink_writehex32
	dec	sp
	dec	sp
	C$main.c$258$4$359 ==.
;	main.c:258: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
	C$main.c$259$4$359 ==.
;	main.c:259: dbglink_writestr("41_PKT_delta:");
	mov	dptr,#___str_9
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$260$4$359 ==.
;	main.c:260: dbglink_writehex32(wtimer0_curtime()-wt0_beacon, 4, WRNUM_PADZERO);
	lcall	_wtimer0_curtime
	mov	r0,dpl
	mov	r1,dph
	mov	r2,b
	mov	r3,a
	mov	dptr,#_wt0_beacon
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	a,r0
	clr	c
	subb	a,r4
	mov	r0,a
	mov	a,r1
	subb	a,r5
	mov	r1,a
	mov	a,r2
	subb	a,r6
	mov	r2,a
	mov	a,r3
	subb	a,r7
	mov	r3,a
	mov	a,#0x08
	push	acc
	rr	a
	push	acc
	mov	dpl,r0
	mov	dph,r1
	mov	b,r2
	mov	a,r3
	lcall	_dbglink_writehex32
	dec	sp
	dec	sp
	C$main.c$261$4$359 ==.
;	main.c:261: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
	C$main.c$262$4$359 ==.
;	main.c:262: dbglink_received_packet(st);
	mov	dpl,_axradio_statuschange_st_1_351
	mov	dph,(_axradio_statuschange_st_1_351 + 1)
	lcall	_dbglink_received_packet
	C$main.c$268$4$359 ==.
;	main.c:268: if (pktdata[0] == 0x32) // changed from 0x2E on 2021-08-22
	mov	dpl,_axradio_statuschange_pktdata_3_358
	mov	dph,(_axradio_statuschange_pktdata_3_358 + 1)
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x32,00160$
	sjmp	00161$
00160$:
	sjmp	00129$
00161$:
	C$main.c$271$5$360 ==.
;	main.c:271: ++pkts_received;
	mov	dptr,#_pkts_received
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
	inc	dptr
	movx	a,@dptr
	addc	a,#0x00
	movx	@dptr,a
	C$main.c$272$5$360 ==.
;	main.c:272: rcv_flg += 1;
	mov	dptr,#_rcv_flg
	movx	a,@dptr
	inc	a
	movx	@dptr,a
	C$main.c$274$5$360 ==.
;	main.c:274: if ((pktdata[10] & 0xF0) >> 4 ==  0x3) // changed from 6 to 10 on 2021-08-22
	mov	a,#0x0a
	add	a,_axradio_statuschange_pktdata_3_358
	mov	dpl,a
	clr	a
	addc	a,(_axradio_statuschange_pktdata_3_358 + 1)
	mov	dph,a
	movx	a,@dptr
	mov	r7,a
	mov	a,#0xf0
	anl	a,r7
	swap	a
	anl	a,#0x0f
	mov	r6,a
	cjne	r6,#0x03,00120$
	C$main.c$276$6$361 ==.
;	main.c:276: pkts_id3 += 1;
	mov	dptr,#_pkts_id3
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	dptr,#_pkts_id3
	mov	a,#0x01
	add	a,r5
	movx	@dptr,a
	clr	a
	addc	a,r6
	inc	dptr
	movx	@dptr,a
	C$main.c$277$6$361 ==.
;	main.c:277: rcv_flg_id3 += 1;
	mov	dptr,#_rcv_flg_id3
	movx	a,@dptr
	mov	r6,a
	inc	a
	movx	@dptr,a
	sjmp	00129$
00120$:
	C$main.c$279$5$360 ==.
;	main.c:279: else if ((pktdata[10] & 0xF0) >> 4 == 0x1) // changed from 6 to 10 on 2021-08-22
	anl	ar7,#0xf0
	mov	a,r7
	swap	a
	anl	a,#0x0f
	mov	r7,a
	cjne	r7,#0x01,00129$
	C$main.c$281$6$362 ==.
;	main.c:281: pkts_id1 += 1;
	mov	dptr,#_pkts_id1
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_pkts_id1
	mov	a,#0x01
	add	a,r6
	movx	@dptr,a
	clr	a
	addc	a,r7
	inc	dptr
	movx	@dptr,a
	C$main.c$282$6$362 ==.
;	main.c:282: rcv_flg_id1 += 1;
	mov	dptr,#_rcv_flg_id1
	movx	a,@dptr
	mov	r7,a
	inc	a
	movx	@dptr,a
	sjmp	00129$
00125$:
	C$main.c$295$4$363 ==.
;	main.c:295: rcv_flg = 0;
	mov	dptr,#_rcv_flg
	clr	a
	movx	@dptr,a
	C$main.c$297$4$363 ==.
;	main.c:297: dbglink_writestr("Invalid packet.\n\n");
	mov	dptr,#___str_10
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$325$1$352 ==.
;	main.c:325: }
00129$:
	C$main.c$326$1$352 ==.
	XG$axradio_statuschange$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'enable_radio_interrupt_in_mcu_pin'
;------------------------------------------------------------
	G$enable_radio_interrupt_in_mcu_pin$0$0 ==.
	C$main.c$328$1$352 ==.
;	main.c:328: void enable_radio_interrupt_in_mcu_pin(void)
;	-----------------------------------------
;	 function enable_radio_interrupt_in_mcu_pin
;	-----------------------------------------
_enable_radio_interrupt_in_mcu_pin:
	C$main.c$330$1$365 ==.
;	main.c:330: IE_4 = 1;
	setb	_IE_4
	C$main.c$331$1$365 ==.
	XG$enable_radio_interrupt_in_mcu_pin$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'disable_radio_interrupt_in_mcu_pin'
;------------------------------------------------------------
	G$disable_radio_interrupt_in_mcu_pin$0$0 ==.
	C$main.c$333$1$365 ==.
;	main.c:333: void disable_radio_interrupt_in_mcu_pin(void)
;	-----------------------------------------
;	 function disable_radio_interrupt_in_mcu_pin
;	-----------------------------------------
_disable_radio_interrupt_in_mcu_pin:
	C$main.c$335$1$367 ==.
;	main.c:335: IE_4 = 0;
	clr	_IE_4
	C$main.c$336$1$367 ==.
	XG$disable_radio_interrupt_in_mcu_pin$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'wakeup_callback'
;------------------------------------------------------------
;desc                      Allocated to registers 
;timer0Period              Allocated with name '_wakeup_callback_timer0Period_1_369'
;bc_flg                    Allocated with name '_wakeup_callback_bc_flg_1_369'
;------------------------------------------------------------
	Fmain$wakeup_callback$0$0 ==.
	C$main.c$338$1$367 ==.
;	main.c:338: static void wakeup_callback(struct wtimer_desc __xdata *desc)
;	-----------------------------------------
;	 function wakeup_callback
;	-----------------------------------------
_wakeup_callback:
	C$main.c$349$1$369 ==.
;	main.c:349: if ((rcv_flg == 0)&&(bc_flg == 0))
	mov	dptr,#_rcv_flg
	movx	a,@dptr
	mov	r7,a
	jnz	00109$
	mov	dptr,#_wakeup_callback_bc_flg_1_369
	movx	a,@dptr
	jnz	00109$
	C$main.c$351$2$370 ==.
;	main.c:351: timer0Period = WTIMER0_UPLINKRCV;
	mov	dptr,#_wakeup_callback_timer0Period_1_369
	mov	a,#0x80
	movx	@dptr,a
	mov	a,#0x11
	inc	dptr
	movx	@dptr,a
	C$main.c$352$2$370 ==.
;	main.c:352: transmit_beacon();
	lcall	_transmit_beacon
	C$main.c$353$2$370 ==.
;	main.c:353: bc_flg = 1;
	mov	dptr,#_wakeup_callback_bc_flg_1_369
	mov	a,#0x01
	movx	@dptr,a
	C$main.c$356$2$370 ==.
;	main.c:356: dbglink_writestr("1_Beacon_WT0: ");
	mov	dptr,#___str_11
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$357$2$370 ==.
;	main.c:357: dbglink_writehex32(/*wtimer0_curtime()*/wt0_beacon, 8, WRNUM_PADZERO);
	mov	dptr,#_wt0_beacon
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	mov	a,#0x08
	push	acc
	push	acc
	mov	dpl,r3
	mov	dph,r4
	mov	b,r5
	mov	a,r6
	lcall	_dbglink_writehex32
	dec	sp
	dec	sp
	C$main.c$358$2$370 ==.
;	main.c:358: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
	ljmp	00110$
00109$:
	C$main.c$362$1$369 ==.
;	main.c:362: else if ((rcv_flg >= 1)&&(bc_flg == 1)) // received data packets from COM-D terminals
	cjne	r7,#0x01,00134$
00134$:
	jc	00105$
	mov	dptr,#_wakeup_callback_bc_flg_1_369
	movx	a,@dptr
	mov	r6,a
	cjne	r6,#0x01,00105$
	C$main.c$364$2$371 ==.
;	main.c:364: timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
	mov	dptr,#_wakeup_callback_timer0Period_1_369
	mov	a,#0x80
	movx	@dptr,a
	mov	a,#0x07
	inc	dptr
	movx	@dptr,a
	C$main.c$365$2$371 ==.
;	main.c:365: transmit_ack();
	lcall	_transmit_ack
	C$main.c$366$2$371 ==.
;	main.c:366: rcv_flg = 0;
	mov	dptr,#_rcv_flg
	clr	a
	movx	@dptr,a
	C$main.c$367$2$371 ==.
;	main.c:367: bc_flg = 0;
	mov	dptr,#_wakeup_callback_bc_flg_1_369
	movx	@dptr,a
	C$main.c$368$2$371 ==.
;	main.c:368: rcv_flg_id1 = 0;
	mov	dptr,#_rcv_flg_id1
	movx	@dptr,a
	C$main.c$369$2$371 ==.
;	main.c:369: rcv_flg_id3 = 0;
	mov	dptr,#_rcv_flg_id3
	movx	@dptr,a
	C$main.c$372$2$371 ==.
;	main.c:372: dbglink_writestr("2_ACK_WT0: ");
	mov	dptr,#___str_12
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$373$2$371 ==.
;	main.c:373: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
	lcall	_wtimer0_curtime
	mov	r3,dpl
	mov	r4,dph
	mov	r5,b
	mov	r6,a
	mov	a,#0x08
	push	acc
	push	acc
	mov	dpl,r3
	mov	dph,r4
	mov	b,r5
	mov	a,r6
	lcall	_dbglink_writehex32
	dec	sp
	dec	sp
	C$main.c$374$2$371 ==.
;	main.c:374: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
	sjmp	00110$
00105$:
	C$main.c$378$1$369 ==.
;	main.c:378: else if ((rcv_flg == 0)&&(bc_flg == 1))
	mov	a,r7
	jnz	00110$
	mov	dptr,#_wakeup_callback_bc_flg_1_369
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x01,00110$
	C$main.c$380$2$372 ==.
;	main.c:380: timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
	mov	dptr,#_wakeup_callback_timer0Period_1_369
	mov	a,#0x80
	movx	@dptr,a
	mov	a,#0x07
	inc	dptr
	movx	@dptr,a
	C$main.c$381$2$372 ==.
;	main.c:381: bc_flg = 0;
	mov	dptr,#_wakeup_callback_bc_flg_1_369
	clr	a
	movx	@dptr,a
	C$main.c$384$2$372 ==.
;	main.c:384: dbglink_writestr("3_NoACK_WT0: ");
	mov	dptr,#___str_13
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$385$2$372 ==.
;	main.c:385: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
	lcall	_wtimer0_curtime
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,#0x08
	push	acc
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	_dbglink_writehex32
	dec	sp
	dec	sp
	C$main.c$386$2$372 ==.
;	main.c:386: dbglink_tx('\n');
	mov	dpl,#0x0a
	lcall	_dbglink_tx
00110$:
	C$main.c$390$1$369 ==.
;	main.c:390: wakeup_desc.time += wtimer0_correctinterval(timer0Period);
	mov	dptr,#(_wakeup_desc + 0x0004)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	mov	dptr,#_wakeup_callback_timer0Period_1_369
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	mov	ar0,r2
	mov	ar1,r3
	mov	r2,#0x00
	mov	r3,#0x00
	mov	dpl,r0
	mov	dph,r1
	mov	b,r2
	mov	a,r3
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_wtimer0_correctinterval
	mov	r0,dpl
	mov	r1,dph
	mov	r2,b
	mov	r3,a
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	a,r0
	add	a,r4
	mov	r4,a
	mov	a,r1
	addc	a,r5
	mov	r5,a
	mov	a,r2
	addc	a,r6
	mov	r6,a
	mov	a,r3
	addc	a,r7
	mov	r7,a
	mov	dptr,#(_wakeup_desc + 0x0004)
	mov	a,r4
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
	C$main.c$391$1$369 ==.
;	main.c:391: wtimer0_addabsolute(&wakeup_desc);
	mov	dptr,#_wakeup_desc
	lcall	_wtimer0_addabsolute
	C$main.c$392$1$369 ==.
	XFmain$wakeup_callback$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;c                         Allocated to registers 
;p                         Allocated to registers 
;c                         Allocated to registers 
;p                         Allocated to registers 
;------------------------------------------------------------
	G$_sdcc_external_startup$0$0 ==.
	C$main.c$394$1$369 ==.
;	main.c:394: uint8_t _sdcc_external_startup(void)
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
	C$main.c$396$1$374 ==.
;	main.c:396: LPXOSCGM = 0x8A;
	mov	dptr,#_LPXOSCGM
	mov	a,#0x8a
	movx	@dptr,a
	C$main.c$397$2$375 ==.
;	main.c:397: wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
	mov	dpl,#0x09
	lcall	_wtimer0_setconfig
	C$main.c$398$2$376 ==.
;	main.c:398: wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
	mov	dpl,#0x38
	lcall	_wtimer1_setconfig
	C$main.c$400$1$374 ==.
;	main.c:400: LPOSCCONFIG = 0x09; /* Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() ) */
	mov	dptr,#_LPOSCCONFIG
	mov	a,#0x09
	movx	@dptr,a
	C$main.c$402$1$374 ==.
;	main.c:402: coldstart = !(PCON & 0x40);
	mov	a,_PCON
	mov	c,acc[6]
	cpl	c
	mov	__sdcc_external_startup_sloc0_1_0,c
	clr	a
	rlc	a
	mov	_coldstart,a
	C$main.c$404$1$374 ==.
;	main.c:404: ANALOGA = 0x18; /* PA[3,4] LPXOSC, other PA are used as digital pins */
	mov	dptr,#_ANALOGA
	mov	a,#0x18
	movx	@dptr,a
	C$main.c$405$1$374 ==.
;	main.c:405: PORTA = 0xE7; /* pull ups except for LPXOSC pin PA[3,4]; */
	mov	_PORTA,#0xe7
	C$main.c$406$1$374 ==.
;	main.c:406: PORTB = 0xFD | (PINB & 0x02); /* init LEDs to previous (frozen) state */
	mov	a,#0x02
	anl	a,_PINB
	orl	a,#0xfd
	mov	_PORTB,a
	C$main.c$407$1$374 ==.
;	main.c:407: PORTC = 0xFF;
	mov	_PORTC,#0xff
	C$main.c$408$1$374 ==.
;	main.c:408: PORTR = 0x0B;
	mov	_PORTR,#0x0b
	C$main.c$410$1$374 ==.
;	main.c:410: DIRA = 0x00;
	mov	_DIRA,#0x00
	C$main.c$411$1$374 ==.
;	main.c:411: DIRB = 0x0e; /*  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used) */
	mov	_DIRB,#0x0e
	C$main.c$412$1$374 ==.
;	main.c:412: DIRC = 0x00; /*  PC4 = button */
	mov	_DIRC,#0x00
	C$main.c$413$1$374 ==.
;	main.c:413: DIRR = 0x15;
	mov	_DIRR,#0x15
	C$main.c$414$1$374 ==.
;	main.c:414: axradio_setup_pincfg1();
	lcall	_axradio_setup_pincfg1
	C$main.c$415$1$374 ==.
;	main.c:415: DPS = 0;
	mov	_DPS,#0x00
	C$main.c$416$1$374 ==.
;	main.c:416: IE = 0x40;
	mov	_IE,#0x40
	C$main.c$417$1$374 ==.
;	main.c:417: EIE = 0x00;
	mov	_EIE,#0x00
	C$main.c$418$1$374 ==.
;	main.c:418: E2IE = 0x00;
	mov	_E2IE,#0x00
	C$main.c$420$1$374 ==.
;	main.c:420: display_portinit();
	lcall	_com0_portinit
	C$main.c$421$1$374 ==.
;	main.c:421: GPIOENABLE = 1; /* unfreeze GPIO */
	mov	dptr,#_GPIOENABLE
	mov	a,#0x01
	movx	@dptr,a
	C$main.c$422$1$374 ==.
;	main.c:422: return !coldstart; /* coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization */
	mov	a,_coldstart
	cjne	a,#0x01,00109$
00109$:
	mov  __sdcc_external_startup_sloc0_1_0,c
	clr	a
	rlc	a
	mov	dpl,a
	C$main.c$423$1$374 ==.
	XG$_sdcc_external_startup$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;saved_button_state        Allocated with name '_main_saved_button_state_1_378'
;i                         Allocated to registers r7 
;x                         Allocated to registers r6 
;flg                       Allocated to registers r6 
;flg                       Allocated to registers r7 
;------------------------------------------------------------
	G$main$0$0 ==.
	C$main.c$425$1$374 ==.
;	main.c:425: int main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	C$main.c$432$1$378 ==.
;	main.c:432: __endasm;
	G$_start__stack$0$0	= __start__stack
	.globl	G$_start__stack$0$0
	C$main.c$434$1$378 ==.
;	main.c:434: dbglink_init();
	lcall	_dbglink_init
	C$libmftypes.h$368$4$394 ==.
;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
	setb	_EA
	C$main.c$437$1$378 ==.
;	main.c:437: flash_apply_calibration();
	lcall	_flash_apply_calibration
	C$main.c$438$1$378 ==.
;	main.c:438: CLKCON = 0x00;
	mov	_CLKCON,#0x00
	C$main.c$439$1$378 ==.
;	main.c:439: wtimer_init();
	lcall	_wtimer_init
	C$main.c$441$1$378 ==.
;	main.c:441: if (coldstart)
	mov	a,_coldstart
	jnz	00193$
	ljmp	00122$
00193$:
	C$main.c$443$4$381 ==.
;	main.c:443: led0_off();
	clr	_PORTB_1
	C$main.c$445$2$379 ==.
;	main.c:445: wakeup_desc.handler = wakeup_callback;
	mov	dptr,#(_wakeup_desc + 0x0002)
	mov	a,#_wakeup_callback
	movx	@dptr,a
	mov	a,#(_wakeup_callback >> 8)
	inc	dptr
	movx	@dptr,a
	C$main.c$447$2$379 ==.
;	main.c:447: display_init();
	lcall	_com0_init
	C$main.c$448$2$379 ==.
;	main.c:448: display_setpos(0);
	mov	dpl,#0x00
	lcall	_com0_setpos
	C$main.c$449$2$379 ==.
;	main.c:449: i = axradio_init();
	lcall	_axradio_init
	C$main.c$451$2$379 ==.
;	main.c:451: if (i != AXRADIO_ERR_NOERROR)
	mov	a,dpl
	mov	r7,a
	jz	00112$
	C$main.c$453$3$382 ==.
;	main.c:453: if (i == AXRADIO_ERR_NOCHIP)
	cjne	r7,#0x05,00195$
	sjmp	00196$
00195$:
	ljmp	00129$
00196$:
	C$main.c$455$4$383 ==.
;	main.c:455: display_writestr(radio_not_found_lcd_display);
	mov	dptr,#_radio_not_found_lcd_display
	mov	b,#0x00
	lcall	_com0_writestr
	C$main.c$458$4$383 ==.
;	main.c:458: if(DBGLNKSTAT & 0x10)
	mov	a,_DBGLNKSTAT
	jb	acc.4,00197$
	ljmp	00141$
00197$:
	C$main.c$459$4$383 ==.
;	main.c:459: dbglink_writestr(radio_not_found_lcd_display);
	mov	dptr,#_radio_not_found_lcd_display
	mov	b,#0x00
	lcall	_dbglink_writestr
	C$main.c$462$4$383 ==.
;	main.c:462: goto terminate_error;
	ljmp	00141$
	C$main.c$465$2$379 ==.
;	main.c:465: goto terminate_radio_error;
00112$:
	C$main.c$468$2$379 ==.
;	main.c:468: display_writestr(radio_lcd_display);
	mov	dptr,#_radio_lcd_display
	mov	b,#0x00
	lcall	_com0_writestr
	C$main.c$471$2$379 ==.
;	main.c:471: if (DBGLNKSTAT & 0x10)
	mov	a,_DBGLNKSTAT
	jnb	acc.4,00114$
	C$main.c$472$2$379 ==.
;	main.c:472: dbglink_writestr(radio_lcd_display);
	mov	dptr,#_radio_lcd_display
	mov	b,#0x00
	lcall	_dbglink_writestr
00114$:
	C$main.c$475$2$379 ==.
;	main.c:475: axradio_set_local_address(&localaddr);
	mov	dptr,#_localaddr
	mov	b,#0x80
	lcall	_axradio_set_local_address
	C$main.c$476$2$379 ==.
;	main.c:476: axradio_set_default_remote_address(&remoteaddr);
	mov	dptr,#_remoteaddr
	mov	b,#0x80
	lcall	_axradio_set_default_remote_address
	C$main.c$480$2$379 ==.
;	main.c:480: if (DBGLNKSTAT & 0x10)
	mov	a,_DBGLNKSTAT
	jnb	acc.4,00118$
	C$main.c$482$3$384 ==.
;	main.c:482: dbglink_writestr("RNG = ");
	mov	dptr,#___str_14
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$main.c$483$3$384 ==.
;	main.c:483: dbglink_writenum16(axradio_get_pllrange(), 2, 0);
	lcall	_axradio_get_pllrange
	clr	a
	push	acc
	mov	a,#0x02
	push	acc
	lcall	_dbglink_writenum16
	dec	sp
	dec	sp
	C$main.c$485$4$385 ==.
;	main.c:485: uint8_t x = axradio_get_pllvcoi();
	lcall	_axradio_get_pllvcoi
	C$main.c$487$4$385 ==.
;	main.c:487: if (x & 0x80)
	mov	a,dpl
	mov	r6,a
	jnb	acc.7,00116$
	C$main.c$489$5$386 ==.
;	main.c:489: dbglink_writestr("\nVCOI = ");
	mov	dptr,#___str_15
	mov	b,#0x80
	push	ar6
	lcall	_dbglink_writestr
	pop	ar6
	C$main.c$490$5$386 ==.
;	main.c:490: dbglink_writehex16(x, 2, 0);
	clr	a
	mov	r5,a
	push	acc
	mov	a,#0x02
	push	acc
	mov	dpl,r6
	mov	dph,r5
	lcall	_dbglink_writehex16
	dec	sp
	dec	sp
00116$:
	C$main.c$493$3$384 ==.
;	main.c:493: dbglink_writestr("\n\nMASTER\n");
	mov	dptr,#___str_16
	mov	b,#0x80
	lcall	_dbglink_writestr
00118$:
	C$main.c$497$2$379 ==.
;	main.c:497: i = axradio_set_mode(AXRADIO_MODE_ASYNC_RECEIVE);
	mov	dpl,#0x20
	lcall	_axradio_set_mode
	C$main.c$499$2$379 ==.
;	main.c:499: if (i != AXRADIO_ERR_NOERROR)
	mov	a,dpl
	mov	r7,a
	jnz	00129$
	C$main.c$503$2$379 ==.
;	main.c:503: wakeup_desc.time = wtimer0_correctinterval(WTIMER0_PERIOD);
	mov	dptr,#0x1900
	clr	a
	mov	b,a
	lcall	_wtimer0_correctinterval
	mov	r3,dpl
	mov	r4,dph
	mov	r5,b
	mov	r6,a
	mov	dptr,#(_wakeup_desc + 0x0004)
	mov	a,r3
	movx	@dptr,a
	mov	a,r4
	inc	dptr
	movx	@dptr,a
	mov	a,r5
	inc	dptr
	movx	@dptr,a
	mov	a,r6
	inc	dptr
	movx	@dptr,a
	C$main.c$504$2$379 ==.
;	main.c:504: wtimer0_addrelative(&wakeup_desc);
	mov	dptr,#_wakeup_desc
	lcall	_wtimer0_addrelative
	sjmp	00123$
00122$:
	C$main.c$510$2$387 ==.
;	main.c:510: axradio_commsleepexit();
	lcall	_axradio_commsleepexit
	C$main.c$511$2$387 ==.
;	main.c:511: IE_4 = 1; /* enable radio interrupt */
	setb	_IE_4
00123$:
	C$main.c$514$1$378 ==.
;	main.c:514: axradio_setup_pincfg2();
	lcall	_axradio_setup_pincfg2
00139$:
	C$main.c$530$2$388 ==.
;	main.c:530: wtimer_runcallbacks();
	lcall	_wtimer_runcallbacks
	C$libmftypes.h$373$5$397 ==.
;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:373: EA = 0;
	clr	_EA
	C$main.c$533$3$388 ==.
;	main.c:533: uint8_t flg = WTFLAG_CANSTANDBY;
	mov	r6,#0x02
	C$main.c$536$3$389 ==.
;	main.c:536: if (axradio_cansleep()
	push	ar6
	lcall	_axradio_cansleep
	mov	a,dpl
	pop	ar6
	jz	00125$
	C$main.c$538$3$389 ==.
;	main.c:538: && dbglink_txidle()
	lcall	_dbglink_txidle
	mov	a,dpl
	jz	00125$
	C$main.c$540$3$389 ==.
;	main.c:540: && display_txidle())
	lcall	_uart0_txidle
	mov	a,dpl
	jz	00125$
	C$main.c$541$3$389 ==.
;	main.c:541: flg |= WTFLAG_CANSLEEP;
	mov	r6,#0x03
00125$:
	C$main.c$543$3$389 ==.
;	main.c:543: wtimer_idle(flg);
	mov	dpl,r6
	lcall	_wtimer_idle
	C$main.c$545$2$388 ==.
;	main.c:545: IE_3 = 0; /* no ISR! */
	clr	_IE_3
	C$libmftypes.h$368$5$400 ==.
;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
	setb	_EA
	C$main.c$546$4$399 ==.
;	main.c:546: __enable_irq();
	C$main.c$549$1$378 ==.
;	main.c:549: terminate_radio_error:
	sjmp	00139$
00129$:
	C$main.c$550$1$378 ==.
;	main.c:550: display_radio_error(i);
	mov	dpl,r7
	push	ar7
	lcall	_com0_display_radio_error
	pop	ar7
	C$main.c$552$1$378 ==.
;	main.c:552: dbglink_display_radio_error(i);
	mov	dpl,r7
	lcall	_dbglink_display_radio_error
	C$main.c$554$1$378 ==.
;	main.c:554: terminate_error:
00141$:
	C$main.c$558$2$390 ==.
;	main.c:558: wtimer_runcallbacks();
	lcall	_wtimer_runcallbacks
	C$main.c$560$3$390 ==.
;	main.c:560: uint8_t flg = WTFLAG_CANSTANDBY;
	mov	r7,#0x02
	C$main.c$563$3$391 ==.
;	main.c:563: if (axradio_cansleep()
	push	ar7
	lcall	_axradio_cansleep
	mov	a,dpl
	pop	ar7
	jz	00132$
	C$main.c$565$3$391 ==.
;	main.c:565: && dbglink_txidle()
	lcall	_dbglink_txidle
	mov	a,dpl
	jz	00132$
	C$main.c$567$3$391 ==.
;	main.c:567: && display_txidle())
	lcall	_uart0_txidle
	mov	a,dpl
	jz	00132$
	C$main.c$568$3$391 ==.
;	main.c:568: flg |= WTFLAG_CANSLEEP;
	mov	r7,#0x03
00132$:
	C$main.c$570$3$391 ==.
;	main.c:570: wtimer_idle(flg);
	mov	dpl,r7
	lcall	_wtimer_idle
	sjmp	00141$
	C$main.c$573$1$378 ==.
	XG$main$0$0 ==.
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
Fmain$__str_0$0$0 == .
___str_0:
	.ascii "Beacon: "
	.db 0x00
Fmain$__str_1$0$0 == .
___str_1:
	.ascii "  "
	.db 0x00
Fmain$__str_2$0$0 == .
___str_2:
	.db 0x0a
	.db 0x00
Fmain$__str_3$0$0 == .
___str_3:
	.ascii "ACK: "
	.db 0x00
Fmain$__str_4$0$0 == .
___str_4:
	.ascii " "
	.db 0x00
Fmain$__str_5$0$0 == .
___str_5:
	.ascii "ID1: "
	.db 0x00
Fmain$__str_6$0$0 == .
___str_6:
	.db 0x0a
	.ascii "ID3: "
	.db 0x00
Fmain$__str_7$0$0 == .
___str_7:
	.db 0x0a
	.db 0x0a
	.db 0x00
Fmain$__str_8$0$0 == .
___str_8:
	.ascii "40_PKT_WT0:"
	.db 0x00
Fmain$__str_9$0$0 == .
___str_9:
	.ascii "41_PKT_delta:"
	.db 0x00
Fmain$__str_10$0$0 == .
___str_10:
	.ascii "Invalid packet."
	.db 0x0a
	.db 0x0a
	.db 0x00
Fmain$__str_11$0$0 == .
___str_11:
	.ascii "1_Beacon_WT0: "
	.db 0x00
Fmain$__str_12$0$0 == .
___str_12:
	.ascii "2_ACK_WT0: "
	.db 0x00
Fmain$__str_13$0$0 == .
___str_13:
	.ascii "3_NoACK_WT0: "
	.db 0x00
Fmain$__str_14$0$0 == .
___str_14:
	.ascii "RNG = "
	.db 0x00
Fmain$__str_15$0$0 == .
___str_15:
	.db 0x0a
	.ascii "VCOI = "
	.db 0x00
Fmain$__str_16$0$0 == .
___str_16:
	.db 0x0a
	.db 0x0a
	.ascii "MASTER"
	.db 0x0a
	.db 0x00
	.area XINIT   (CODE)
Fmain$__xinit_beacon_packet$0$0 == .
__xinit__beacon_packet:
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x11	; 17
	.db #0x22	; 34
	.db #0x33	; 51	'3'
	.db #0x44	; 68	'D'
	.db #0x55	; 85	'U'
	.db #0x66	; 102	'f'
Fmain$__xinit_ack_packet$0$0 == .
__xinit__ack_packet:
	.db #0x00	; 0
	.db #0x77	; 119	'w'
	.db #0x88	; 136
	.db #0x99	; 153
Fmain$__xinit_pkts_received$0$0 == .
__xinit__pkts_received:
	.byte #0x00,#0x00	; 0
Fmain$__xinit_pkts_missing$0$0 == .
__xinit__pkts_missing:
	.byte #0x00,#0x00	; 0
Fmain$__xinit_bc_counter$0$0 == .
__xinit__bc_counter:
	.byte #0x00,#0x00	; 0
Fmain$__xinit_rcv_flg$0$0 == .
__xinit__rcv_flg:
	.db #0x00	; 0
Fmain$__xinit_pkts_id1$0$0 == .
__xinit__pkts_id1:
	.byte #0x00,#0x00	; 0
Fmain$__xinit_pkts_id3$0$0 == .
__xinit__pkts_id3:
	.byte #0x00,#0x00	; 0
Fmain$__xinit_rcv_flg_id1$0$0 == .
__xinit__rcv_flg_id1:
	.db #0x00	; 0
Fmain$__xinit_rcv_flg_id3$0$0 == .
__xinit__rcv_flg_id3:
	.db #0x00	; 0
Fmain$__xinit_wt0_beacon$0$0 == .
__xinit__wt0_beacon:
	.byte #0x00,#0x00,#0x00,#0x00	; 0
	.area CABS    (ABS,CODE)
