                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl __sdcc_external_startup
                                     13 	.globl _wait_dbglink_free
                                     14 	.globl _dbglink_display_radio_error
                                     15 	.globl _com0_display_radio_error
                                     16 	.globl _dbglink_received_packet
                                     17 	.globl _memcpy
                                     18 	.globl _com0_writestr
                                     19 	.globl _com0_setpos
                                     20 	.globl _com0_init
                                     21 	.globl _com0_portinit
                                     22 	.globl _dbglink_writehex32
                                     23 	.globl _dbglink_writehex16
                                     24 	.globl _dbglink_writenum16
                                     25 	.globl _dbglink_writestr
                                     26 	.globl _dbglink_tx
                                     27 	.globl _dbglink_init
                                     28 	.globl _dbglink_txidle
                                     29 	.globl _uart0_txidle
                                     30 	.globl _wtimer0_correctinterval
                                     31 	.globl _wtimer0_addrelative
                                     32 	.globl _wtimer0_addabsolute
                                     33 	.globl _wtimer0_curtime
                                     34 	.globl _wtimer_runcallbacks
                                     35 	.globl _wtimer_idle
                                     36 	.globl _wtimer_init
                                     37 	.globl _wtimer1_setconfig
                                     38 	.globl _wtimer0_setconfig
                                     39 	.globl _flash_apply_calibration
                                     40 	.globl _axradio_commsleepexit
                                     41 	.globl _axradio_setup_pincfg2
                                     42 	.globl _axradio_setup_pincfg1
                                     43 	.globl _axradio_transmit
                                     44 	.globl _axradio_set_default_remote_address
                                     45 	.globl _axradio_set_local_address
                                     46 	.globl _axradio_get_pllvcoi
                                     47 	.globl _axradio_get_pllrange
                                     48 	.globl _axradio_set_mode
                                     49 	.globl _axradio_cansleep
                                     50 	.globl _axradio_init
                                     51 	.globl _PORTC_7
                                     52 	.globl _PORTC_6
                                     53 	.globl _PORTC_5
                                     54 	.globl _PORTC_4
                                     55 	.globl _PORTC_3
                                     56 	.globl _PORTC_2
                                     57 	.globl _PORTC_1
                                     58 	.globl _PORTC_0
                                     59 	.globl _PORTB_7
                                     60 	.globl _PORTB_6
                                     61 	.globl _PORTB_5
                                     62 	.globl _PORTB_4
                                     63 	.globl _PORTB_3
                                     64 	.globl _PORTB_2
                                     65 	.globl _PORTB_1
                                     66 	.globl _PORTB_0
                                     67 	.globl _PORTA_7
                                     68 	.globl _PORTA_6
                                     69 	.globl _PORTA_5
                                     70 	.globl _PORTA_4
                                     71 	.globl _PORTA_3
                                     72 	.globl _PORTA_2
                                     73 	.globl _PORTA_1
                                     74 	.globl _PORTA_0
                                     75 	.globl _PINC_7
                                     76 	.globl _PINC_6
                                     77 	.globl _PINC_5
                                     78 	.globl _PINC_4
                                     79 	.globl _PINC_3
                                     80 	.globl _PINC_2
                                     81 	.globl _PINC_1
                                     82 	.globl _PINC_0
                                     83 	.globl _PINB_7
                                     84 	.globl _PINB_6
                                     85 	.globl _PINB_5
                                     86 	.globl _PINB_4
                                     87 	.globl _PINB_3
                                     88 	.globl _PINB_2
                                     89 	.globl _PINB_1
                                     90 	.globl _PINB_0
                                     91 	.globl _PINA_7
                                     92 	.globl _PINA_6
                                     93 	.globl _PINA_5
                                     94 	.globl _PINA_4
                                     95 	.globl _PINA_3
                                     96 	.globl _PINA_2
                                     97 	.globl _PINA_1
                                     98 	.globl _PINA_0
                                     99 	.globl _CY
                                    100 	.globl _AC
                                    101 	.globl _F0
                                    102 	.globl _RS1
                                    103 	.globl _RS0
                                    104 	.globl _OV
                                    105 	.globl _F1
                                    106 	.globl _P
                                    107 	.globl _IP_7
                                    108 	.globl _IP_6
                                    109 	.globl _IP_5
                                    110 	.globl _IP_4
                                    111 	.globl _IP_3
                                    112 	.globl _IP_2
                                    113 	.globl _IP_1
                                    114 	.globl _IP_0
                                    115 	.globl _EA
                                    116 	.globl _IE_7
                                    117 	.globl _IE_6
                                    118 	.globl _IE_5
                                    119 	.globl _IE_4
                                    120 	.globl _IE_3
                                    121 	.globl _IE_2
                                    122 	.globl _IE_1
                                    123 	.globl _IE_0
                                    124 	.globl _EIP_7
                                    125 	.globl _EIP_6
                                    126 	.globl _EIP_5
                                    127 	.globl _EIP_4
                                    128 	.globl _EIP_3
                                    129 	.globl _EIP_2
                                    130 	.globl _EIP_1
                                    131 	.globl _EIP_0
                                    132 	.globl _EIE_7
                                    133 	.globl _EIE_6
                                    134 	.globl _EIE_5
                                    135 	.globl _EIE_4
                                    136 	.globl _EIE_3
                                    137 	.globl _EIE_2
                                    138 	.globl _EIE_1
                                    139 	.globl _EIE_0
                                    140 	.globl _E2IP_7
                                    141 	.globl _E2IP_6
                                    142 	.globl _E2IP_5
                                    143 	.globl _E2IP_4
                                    144 	.globl _E2IP_3
                                    145 	.globl _E2IP_2
                                    146 	.globl _E2IP_1
                                    147 	.globl _E2IP_0
                                    148 	.globl _E2IE_7
                                    149 	.globl _E2IE_6
                                    150 	.globl _E2IE_5
                                    151 	.globl _E2IE_4
                                    152 	.globl _E2IE_3
                                    153 	.globl _E2IE_2
                                    154 	.globl _E2IE_1
                                    155 	.globl _E2IE_0
                                    156 	.globl _B_7
                                    157 	.globl _B_6
                                    158 	.globl _B_5
                                    159 	.globl _B_4
                                    160 	.globl _B_3
                                    161 	.globl _B_2
                                    162 	.globl _B_1
                                    163 	.globl _B_0
                                    164 	.globl _ACC_7
                                    165 	.globl _ACC_6
                                    166 	.globl _ACC_5
                                    167 	.globl _ACC_4
                                    168 	.globl _ACC_3
                                    169 	.globl _ACC_2
                                    170 	.globl _ACC_1
                                    171 	.globl _ACC_0
                                    172 	.globl _WTSTAT
                                    173 	.globl _WTIRQEN
                                    174 	.globl _WTEVTD
                                    175 	.globl _WTEVTD1
                                    176 	.globl _WTEVTD0
                                    177 	.globl _WTEVTC
                                    178 	.globl _WTEVTC1
                                    179 	.globl _WTEVTC0
                                    180 	.globl _WTEVTB
                                    181 	.globl _WTEVTB1
                                    182 	.globl _WTEVTB0
                                    183 	.globl _WTEVTA
                                    184 	.globl _WTEVTA1
                                    185 	.globl _WTEVTA0
                                    186 	.globl _WTCNTR1
                                    187 	.globl _WTCNTB
                                    188 	.globl _WTCNTB1
                                    189 	.globl _WTCNTB0
                                    190 	.globl _WTCNTA
                                    191 	.globl _WTCNTA1
                                    192 	.globl _WTCNTA0
                                    193 	.globl _WTCFGB
                                    194 	.globl _WTCFGA
                                    195 	.globl _WDTRESET
                                    196 	.globl _WDTCFG
                                    197 	.globl _U1STATUS
                                    198 	.globl _U1SHREG
                                    199 	.globl _U1MODE
                                    200 	.globl _U1CTRL
                                    201 	.globl _U0STATUS
                                    202 	.globl _U0SHREG
                                    203 	.globl _U0MODE
                                    204 	.globl _U0CTRL
                                    205 	.globl _T2STATUS
                                    206 	.globl _T2PERIOD
                                    207 	.globl _T2PERIOD1
                                    208 	.globl _T2PERIOD0
                                    209 	.globl _T2MODE
                                    210 	.globl _T2CNT
                                    211 	.globl _T2CNT1
                                    212 	.globl _T2CNT0
                                    213 	.globl _T2CLKSRC
                                    214 	.globl _T1STATUS
                                    215 	.globl _T1PERIOD
                                    216 	.globl _T1PERIOD1
                                    217 	.globl _T1PERIOD0
                                    218 	.globl _T1MODE
                                    219 	.globl _T1CNT
                                    220 	.globl _T1CNT1
                                    221 	.globl _T1CNT0
                                    222 	.globl _T1CLKSRC
                                    223 	.globl _T0STATUS
                                    224 	.globl _T0PERIOD
                                    225 	.globl _T0PERIOD1
                                    226 	.globl _T0PERIOD0
                                    227 	.globl _T0MODE
                                    228 	.globl _T0CNT
                                    229 	.globl _T0CNT1
                                    230 	.globl _T0CNT0
                                    231 	.globl _T0CLKSRC
                                    232 	.globl _SPSTATUS
                                    233 	.globl _SPSHREG
                                    234 	.globl _SPMODE
                                    235 	.globl _SPCLKSRC
                                    236 	.globl _RADIOSTAT
                                    237 	.globl _RADIOSTAT1
                                    238 	.globl _RADIOSTAT0
                                    239 	.globl _RADIODATA
                                    240 	.globl _RADIODATA3
                                    241 	.globl _RADIODATA2
                                    242 	.globl _RADIODATA1
                                    243 	.globl _RADIODATA0
                                    244 	.globl _RADIOADDR
                                    245 	.globl _RADIOADDR1
                                    246 	.globl _RADIOADDR0
                                    247 	.globl _RADIOACC
                                    248 	.globl _OC1STATUS
                                    249 	.globl _OC1PIN
                                    250 	.globl _OC1MODE
                                    251 	.globl _OC1COMP
                                    252 	.globl _OC1COMP1
                                    253 	.globl _OC1COMP0
                                    254 	.globl _OC0STATUS
                                    255 	.globl _OC0PIN
                                    256 	.globl _OC0MODE
                                    257 	.globl _OC0COMP
                                    258 	.globl _OC0COMP1
                                    259 	.globl _OC0COMP0
                                    260 	.globl _NVSTATUS
                                    261 	.globl _NVKEY
                                    262 	.globl _NVDATA
                                    263 	.globl _NVDATA1
                                    264 	.globl _NVDATA0
                                    265 	.globl _NVADDR
                                    266 	.globl _NVADDR1
                                    267 	.globl _NVADDR0
                                    268 	.globl _IC1STATUS
                                    269 	.globl _IC1MODE
                                    270 	.globl _IC1CAPT
                                    271 	.globl _IC1CAPT1
                                    272 	.globl _IC1CAPT0
                                    273 	.globl _IC0STATUS
                                    274 	.globl _IC0MODE
                                    275 	.globl _IC0CAPT
                                    276 	.globl _IC0CAPT1
                                    277 	.globl _IC0CAPT0
                                    278 	.globl _PORTR
                                    279 	.globl _PORTC
                                    280 	.globl _PORTB
                                    281 	.globl _PORTA
                                    282 	.globl _PINR
                                    283 	.globl _PINC
                                    284 	.globl _PINB
                                    285 	.globl _PINA
                                    286 	.globl _DIRR
                                    287 	.globl _DIRC
                                    288 	.globl _DIRB
                                    289 	.globl _DIRA
                                    290 	.globl _DBGLNKSTAT
                                    291 	.globl _DBGLNKBUF
                                    292 	.globl _CODECONFIG
                                    293 	.globl _CLKSTAT
                                    294 	.globl _CLKCON
                                    295 	.globl _ANALOGCOMP
                                    296 	.globl _ADCCONV
                                    297 	.globl _ADCCLKSRC
                                    298 	.globl _ADCCH3CONFIG
                                    299 	.globl _ADCCH2CONFIG
                                    300 	.globl _ADCCH1CONFIG
                                    301 	.globl _ADCCH0CONFIG
                                    302 	.globl __XPAGE
                                    303 	.globl _XPAGE
                                    304 	.globl _SP
                                    305 	.globl _PSW
                                    306 	.globl _PCON
                                    307 	.globl _IP
                                    308 	.globl _IE
                                    309 	.globl _EIP
                                    310 	.globl _EIE
                                    311 	.globl _E2IP
                                    312 	.globl _E2IE
                                    313 	.globl _DPS
                                    314 	.globl _DPTR1
                                    315 	.globl _DPTR0
                                    316 	.globl _DPL1
                                    317 	.globl _DPL
                                    318 	.globl _DPH1
                                    319 	.globl _DPH
                                    320 	.globl _B
                                    321 	.globl _ACC
                                    322 	.globl _rcv_flg_id3
                                    323 	.globl _rcv_flg_id1
                                    324 	.globl _pkts_id3
                                    325 	.globl _pkts_id1
                                    326 	.globl _rcv_flg
                                    327 	.globl _bc_counter
                                    328 	.globl _pkts_missing
                                    329 	.globl _pkts_received
                                    330 	.globl _ack_packet
                                    331 	.globl _beacon_packet
                                    332 	.globl _wakeup_desc
                                    333 	.globl _XTALREADY
                                    334 	.globl _XTALOSC
                                    335 	.globl _XTALAMPL
                                    336 	.globl _SILICONREV
                                    337 	.globl _SCRATCH3
                                    338 	.globl _SCRATCH2
                                    339 	.globl _SCRATCH1
                                    340 	.globl _SCRATCH0
                                    341 	.globl _RADIOMUX
                                    342 	.globl _RADIOFSTATADDR
                                    343 	.globl _RADIOFSTATADDR1
                                    344 	.globl _RADIOFSTATADDR0
                                    345 	.globl _RADIOFDATAADDR
                                    346 	.globl _RADIOFDATAADDR1
                                    347 	.globl _RADIOFDATAADDR0
                                    348 	.globl _OSCRUN
                                    349 	.globl _OSCREADY
                                    350 	.globl _OSCFORCERUN
                                    351 	.globl _OSCCALIB
                                    352 	.globl _MISCCTRL
                                    353 	.globl _LPXOSCGM
                                    354 	.globl _LPOSCREF
                                    355 	.globl _LPOSCREF1
                                    356 	.globl _LPOSCREF0
                                    357 	.globl _LPOSCPER
                                    358 	.globl _LPOSCPER1
                                    359 	.globl _LPOSCPER0
                                    360 	.globl _LPOSCKFILT
                                    361 	.globl _LPOSCKFILT1
                                    362 	.globl _LPOSCKFILT0
                                    363 	.globl _LPOSCFREQ
                                    364 	.globl _LPOSCFREQ1
                                    365 	.globl _LPOSCFREQ0
                                    366 	.globl _LPOSCCONFIG
                                    367 	.globl _PINSEL
                                    368 	.globl _PINCHGC
                                    369 	.globl _PINCHGB
                                    370 	.globl _PINCHGA
                                    371 	.globl _PALTRADIO
                                    372 	.globl _PALTC
                                    373 	.globl _PALTB
                                    374 	.globl _PALTA
                                    375 	.globl _INTCHGC
                                    376 	.globl _INTCHGB
                                    377 	.globl _INTCHGA
                                    378 	.globl _EXTIRQ
                                    379 	.globl _GPIOENABLE
                                    380 	.globl _ANALOGA
                                    381 	.globl _FRCOSCREF
                                    382 	.globl _FRCOSCREF1
                                    383 	.globl _FRCOSCREF0
                                    384 	.globl _FRCOSCPER
                                    385 	.globl _FRCOSCPER1
                                    386 	.globl _FRCOSCPER0
                                    387 	.globl _FRCOSCKFILT
                                    388 	.globl _FRCOSCKFILT1
                                    389 	.globl _FRCOSCKFILT0
                                    390 	.globl _FRCOSCFREQ
                                    391 	.globl _FRCOSCFREQ1
                                    392 	.globl _FRCOSCFREQ0
                                    393 	.globl _FRCOSCCTRL
                                    394 	.globl _FRCOSCCONFIG
                                    395 	.globl _DMA1CONFIG
                                    396 	.globl _DMA1ADDR
                                    397 	.globl _DMA1ADDR1
                                    398 	.globl _DMA1ADDR0
                                    399 	.globl _DMA0CONFIG
                                    400 	.globl _DMA0ADDR
                                    401 	.globl _DMA0ADDR1
                                    402 	.globl _DMA0ADDR0
                                    403 	.globl _ADCTUNE2
                                    404 	.globl _ADCTUNE1
                                    405 	.globl _ADCTUNE0
                                    406 	.globl _ADCCH3VAL
                                    407 	.globl _ADCCH3VAL1
                                    408 	.globl _ADCCH3VAL0
                                    409 	.globl _ADCCH2VAL
                                    410 	.globl _ADCCH2VAL1
                                    411 	.globl _ADCCH2VAL0
                                    412 	.globl _ADCCH1VAL
                                    413 	.globl _ADCCH1VAL1
                                    414 	.globl _ADCCH1VAL0
                                    415 	.globl _ADCCH0VAL
                                    416 	.globl _ADCCH0VAL1
                                    417 	.globl _ADCCH0VAL0
                                    418 	.globl _coldstart
                                    419 	.globl _pkt_counter
                                    420 	.globl _axradio_statuschange
                                    421 	.globl _enable_radio_interrupt_in_mcu_pin
                                    422 	.globl _disable_radio_interrupt_in_mcu_pin
                                    423 ;--------------------------------------------------------
                                    424 ; special function registers
                                    425 ;--------------------------------------------------------
                                    426 	.area RSEG    (ABS,DATA)
      000000                        427 	.org 0x0000
                           0000E0   428 G$ACC$0$0 == 0x00e0
                           0000E0   429 _ACC	=	0x00e0
                           0000F0   430 G$B$0$0 == 0x00f0
                           0000F0   431 _B	=	0x00f0
                           000083   432 G$DPH$0$0 == 0x0083
                           000083   433 _DPH	=	0x0083
                           000085   434 G$DPH1$0$0 == 0x0085
                           000085   435 _DPH1	=	0x0085
                           000082   436 G$DPL$0$0 == 0x0082
                           000082   437 _DPL	=	0x0082
                           000084   438 G$DPL1$0$0 == 0x0084
                           000084   439 _DPL1	=	0x0084
                           008382   440 G$DPTR0$0$0 == 0x8382
                           008382   441 _DPTR0	=	0x8382
                           008584   442 G$DPTR1$0$0 == 0x8584
                           008584   443 _DPTR1	=	0x8584
                           000086   444 G$DPS$0$0 == 0x0086
                           000086   445 _DPS	=	0x0086
                           0000A0   446 G$E2IE$0$0 == 0x00a0
                           0000A0   447 _E2IE	=	0x00a0
                           0000C0   448 G$E2IP$0$0 == 0x00c0
                           0000C0   449 _E2IP	=	0x00c0
                           000098   450 G$EIE$0$0 == 0x0098
                           000098   451 _EIE	=	0x0098
                           0000B0   452 G$EIP$0$0 == 0x00b0
                           0000B0   453 _EIP	=	0x00b0
                           0000A8   454 G$IE$0$0 == 0x00a8
                           0000A8   455 _IE	=	0x00a8
                           0000B8   456 G$IP$0$0 == 0x00b8
                           0000B8   457 _IP	=	0x00b8
                           000087   458 G$PCON$0$0 == 0x0087
                           000087   459 _PCON	=	0x0087
                           0000D0   460 G$PSW$0$0 == 0x00d0
                           0000D0   461 _PSW	=	0x00d0
                           000081   462 G$SP$0$0 == 0x0081
                           000081   463 _SP	=	0x0081
                           0000D9   464 G$XPAGE$0$0 == 0x00d9
                           0000D9   465 _XPAGE	=	0x00d9
                           0000D9   466 G$_XPAGE$0$0 == 0x00d9
                           0000D9   467 __XPAGE	=	0x00d9
                           0000CA   468 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   469 _ADCCH0CONFIG	=	0x00ca
                           0000CB   470 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   471 _ADCCH1CONFIG	=	0x00cb
                           0000D2   472 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   473 _ADCCH2CONFIG	=	0x00d2
                           0000D3   474 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   475 _ADCCH3CONFIG	=	0x00d3
                           0000D1   476 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   477 _ADCCLKSRC	=	0x00d1
                           0000C9   478 G$ADCCONV$0$0 == 0x00c9
                           0000C9   479 _ADCCONV	=	0x00c9
                           0000E1   480 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   481 _ANALOGCOMP	=	0x00e1
                           0000C6   482 G$CLKCON$0$0 == 0x00c6
                           0000C6   483 _CLKCON	=	0x00c6
                           0000C7   484 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   485 _CLKSTAT	=	0x00c7
                           000097   486 G$CODECONFIG$0$0 == 0x0097
                           000097   487 _CODECONFIG	=	0x0097
                           0000E3   488 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   489 _DBGLNKBUF	=	0x00e3
                           0000E2   490 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   491 _DBGLNKSTAT	=	0x00e2
                           000089   492 G$DIRA$0$0 == 0x0089
                           000089   493 _DIRA	=	0x0089
                           00008A   494 G$DIRB$0$0 == 0x008a
                           00008A   495 _DIRB	=	0x008a
                           00008B   496 G$DIRC$0$0 == 0x008b
                           00008B   497 _DIRC	=	0x008b
                           00008E   498 G$DIRR$0$0 == 0x008e
                           00008E   499 _DIRR	=	0x008e
                           0000C8   500 G$PINA$0$0 == 0x00c8
                           0000C8   501 _PINA	=	0x00c8
                           0000E8   502 G$PINB$0$0 == 0x00e8
                           0000E8   503 _PINB	=	0x00e8
                           0000F8   504 G$PINC$0$0 == 0x00f8
                           0000F8   505 _PINC	=	0x00f8
                           00008D   506 G$PINR$0$0 == 0x008d
                           00008D   507 _PINR	=	0x008d
                           000080   508 G$PORTA$0$0 == 0x0080
                           000080   509 _PORTA	=	0x0080
                           000088   510 G$PORTB$0$0 == 0x0088
                           000088   511 _PORTB	=	0x0088
                           000090   512 G$PORTC$0$0 == 0x0090
                           000090   513 _PORTC	=	0x0090
                           00008C   514 G$PORTR$0$0 == 0x008c
                           00008C   515 _PORTR	=	0x008c
                           0000CE   516 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   517 _IC0CAPT0	=	0x00ce
                           0000CF   518 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   519 _IC0CAPT1	=	0x00cf
                           00CFCE   520 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   521 _IC0CAPT	=	0xcfce
                           0000CC   522 G$IC0MODE$0$0 == 0x00cc
                           0000CC   523 _IC0MODE	=	0x00cc
                           0000CD   524 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   525 _IC0STATUS	=	0x00cd
                           0000D6   526 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   527 _IC1CAPT0	=	0x00d6
                           0000D7   528 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   529 _IC1CAPT1	=	0x00d7
                           00D7D6   530 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   531 _IC1CAPT	=	0xd7d6
                           0000D4   532 G$IC1MODE$0$0 == 0x00d4
                           0000D4   533 _IC1MODE	=	0x00d4
                           0000D5   534 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   535 _IC1STATUS	=	0x00d5
                           000092   536 G$NVADDR0$0$0 == 0x0092
                           000092   537 _NVADDR0	=	0x0092
                           000093   538 G$NVADDR1$0$0 == 0x0093
                           000093   539 _NVADDR1	=	0x0093
                           009392   540 G$NVADDR$0$0 == 0x9392
                           009392   541 _NVADDR	=	0x9392
                           000094   542 G$NVDATA0$0$0 == 0x0094
                           000094   543 _NVDATA0	=	0x0094
                           000095   544 G$NVDATA1$0$0 == 0x0095
                           000095   545 _NVDATA1	=	0x0095
                           009594   546 G$NVDATA$0$0 == 0x9594
                           009594   547 _NVDATA	=	0x9594
                           000096   548 G$NVKEY$0$0 == 0x0096
                           000096   549 _NVKEY	=	0x0096
                           000091   550 G$NVSTATUS$0$0 == 0x0091
                           000091   551 _NVSTATUS	=	0x0091
                           0000BC   552 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   553 _OC0COMP0	=	0x00bc
                           0000BD   554 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   555 _OC0COMP1	=	0x00bd
                           00BDBC   556 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   557 _OC0COMP	=	0xbdbc
                           0000B9   558 G$OC0MODE$0$0 == 0x00b9
                           0000B9   559 _OC0MODE	=	0x00b9
                           0000BA   560 G$OC0PIN$0$0 == 0x00ba
                           0000BA   561 _OC0PIN	=	0x00ba
                           0000BB   562 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   563 _OC0STATUS	=	0x00bb
                           0000C4   564 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   565 _OC1COMP0	=	0x00c4
                           0000C5   566 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   567 _OC1COMP1	=	0x00c5
                           00C5C4   568 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   569 _OC1COMP	=	0xc5c4
                           0000C1   570 G$OC1MODE$0$0 == 0x00c1
                           0000C1   571 _OC1MODE	=	0x00c1
                           0000C2   572 G$OC1PIN$0$0 == 0x00c2
                           0000C2   573 _OC1PIN	=	0x00c2
                           0000C3   574 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   575 _OC1STATUS	=	0x00c3
                           0000B1   576 G$RADIOACC$0$0 == 0x00b1
                           0000B1   577 _RADIOACC	=	0x00b1
                           0000B3   578 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   579 _RADIOADDR0	=	0x00b3
                           0000B2   580 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   581 _RADIOADDR1	=	0x00b2
                           00B2B3   582 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   583 _RADIOADDR	=	0xb2b3
                           0000B7   584 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   585 _RADIODATA0	=	0x00b7
                           0000B6   586 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   587 _RADIODATA1	=	0x00b6
                           0000B5   588 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   589 _RADIODATA2	=	0x00b5
                           0000B4   590 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   591 _RADIODATA3	=	0x00b4
                           B4B5B6B7   592 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   593 _RADIODATA	=	0xb4b5b6b7
                           0000BE   594 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   595 _RADIOSTAT0	=	0x00be
                           0000BF   596 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   597 _RADIOSTAT1	=	0x00bf
                           00BFBE   598 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   599 _RADIOSTAT	=	0xbfbe
                           0000DF   600 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   601 _SPCLKSRC	=	0x00df
                           0000DC   602 G$SPMODE$0$0 == 0x00dc
                           0000DC   603 _SPMODE	=	0x00dc
                           0000DE   604 G$SPSHREG$0$0 == 0x00de
                           0000DE   605 _SPSHREG	=	0x00de
                           0000DD   606 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   607 _SPSTATUS	=	0x00dd
                           00009A   608 G$T0CLKSRC$0$0 == 0x009a
                           00009A   609 _T0CLKSRC	=	0x009a
                           00009C   610 G$T0CNT0$0$0 == 0x009c
                           00009C   611 _T0CNT0	=	0x009c
                           00009D   612 G$T0CNT1$0$0 == 0x009d
                           00009D   613 _T0CNT1	=	0x009d
                           009D9C   614 G$T0CNT$0$0 == 0x9d9c
                           009D9C   615 _T0CNT	=	0x9d9c
                           000099   616 G$T0MODE$0$0 == 0x0099
                           000099   617 _T0MODE	=	0x0099
                           00009E   618 G$T0PERIOD0$0$0 == 0x009e
                           00009E   619 _T0PERIOD0	=	0x009e
                           00009F   620 G$T0PERIOD1$0$0 == 0x009f
                           00009F   621 _T0PERIOD1	=	0x009f
                           009F9E   622 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   623 _T0PERIOD	=	0x9f9e
                           00009B   624 G$T0STATUS$0$0 == 0x009b
                           00009B   625 _T0STATUS	=	0x009b
                           0000A2   626 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   627 _T1CLKSRC	=	0x00a2
                           0000A4   628 G$T1CNT0$0$0 == 0x00a4
                           0000A4   629 _T1CNT0	=	0x00a4
                           0000A5   630 G$T1CNT1$0$0 == 0x00a5
                           0000A5   631 _T1CNT1	=	0x00a5
                           00A5A4   632 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   633 _T1CNT	=	0xa5a4
                           0000A1   634 G$T1MODE$0$0 == 0x00a1
                           0000A1   635 _T1MODE	=	0x00a1
                           0000A6   636 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   637 _T1PERIOD0	=	0x00a6
                           0000A7   638 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   639 _T1PERIOD1	=	0x00a7
                           00A7A6   640 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   641 _T1PERIOD	=	0xa7a6
                           0000A3   642 G$T1STATUS$0$0 == 0x00a3
                           0000A3   643 _T1STATUS	=	0x00a3
                           0000AA   644 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   645 _T2CLKSRC	=	0x00aa
                           0000AC   646 G$T2CNT0$0$0 == 0x00ac
                           0000AC   647 _T2CNT0	=	0x00ac
                           0000AD   648 G$T2CNT1$0$0 == 0x00ad
                           0000AD   649 _T2CNT1	=	0x00ad
                           00ADAC   650 G$T2CNT$0$0 == 0xadac
                           00ADAC   651 _T2CNT	=	0xadac
                           0000A9   652 G$T2MODE$0$0 == 0x00a9
                           0000A9   653 _T2MODE	=	0x00a9
                           0000AE   654 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   655 _T2PERIOD0	=	0x00ae
                           0000AF   656 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   657 _T2PERIOD1	=	0x00af
                           00AFAE   658 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   659 _T2PERIOD	=	0xafae
                           0000AB   660 G$T2STATUS$0$0 == 0x00ab
                           0000AB   661 _T2STATUS	=	0x00ab
                           0000E4   662 G$U0CTRL$0$0 == 0x00e4
                           0000E4   663 _U0CTRL	=	0x00e4
                           0000E7   664 G$U0MODE$0$0 == 0x00e7
                           0000E7   665 _U0MODE	=	0x00e7
                           0000E6   666 G$U0SHREG$0$0 == 0x00e6
                           0000E6   667 _U0SHREG	=	0x00e6
                           0000E5   668 G$U0STATUS$0$0 == 0x00e5
                           0000E5   669 _U0STATUS	=	0x00e5
                           0000EC   670 G$U1CTRL$0$0 == 0x00ec
                           0000EC   671 _U1CTRL	=	0x00ec
                           0000EF   672 G$U1MODE$0$0 == 0x00ef
                           0000EF   673 _U1MODE	=	0x00ef
                           0000EE   674 G$U1SHREG$0$0 == 0x00ee
                           0000EE   675 _U1SHREG	=	0x00ee
                           0000ED   676 G$U1STATUS$0$0 == 0x00ed
                           0000ED   677 _U1STATUS	=	0x00ed
                           0000DA   678 G$WDTCFG$0$0 == 0x00da
                           0000DA   679 _WDTCFG	=	0x00da
                           0000DB   680 G$WDTRESET$0$0 == 0x00db
                           0000DB   681 _WDTRESET	=	0x00db
                           0000F1   682 G$WTCFGA$0$0 == 0x00f1
                           0000F1   683 _WTCFGA	=	0x00f1
                           0000F9   684 G$WTCFGB$0$0 == 0x00f9
                           0000F9   685 _WTCFGB	=	0x00f9
                           0000F2   686 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   687 _WTCNTA0	=	0x00f2
                           0000F3   688 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   689 _WTCNTA1	=	0x00f3
                           00F3F2   690 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   691 _WTCNTA	=	0xf3f2
                           0000FA   692 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   693 _WTCNTB0	=	0x00fa
                           0000FB   694 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   695 _WTCNTB1	=	0x00fb
                           00FBFA   696 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   697 _WTCNTB	=	0xfbfa
                           0000EB   698 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   699 _WTCNTR1	=	0x00eb
                           0000F4   700 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   701 _WTEVTA0	=	0x00f4
                           0000F5   702 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   703 _WTEVTA1	=	0x00f5
                           00F5F4   704 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   705 _WTEVTA	=	0xf5f4
                           0000F6   706 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   707 _WTEVTB0	=	0x00f6
                           0000F7   708 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   709 _WTEVTB1	=	0x00f7
                           00F7F6   710 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   711 _WTEVTB	=	0xf7f6
                           0000FC   712 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   713 _WTEVTC0	=	0x00fc
                           0000FD   714 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   715 _WTEVTC1	=	0x00fd
                           00FDFC   716 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   717 _WTEVTC	=	0xfdfc
                           0000FE   718 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   719 _WTEVTD0	=	0x00fe
                           0000FF   720 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   721 _WTEVTD1	=	0x00ff
                           00FFFE   722 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   723 _WTEVTD	=	0xfffe
                           0000E9   724 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   725 _WTIRQEN	=	0x00e9
                           0000EA   726 G$WTSTAT$0$0 == 0x00ea
                           0000EA   727 _WTSTAT	=	0x00ea
                                    728 ;--------------------------------------------------------
                                    729 ; special function bits
                                    730 ;--------------------------------------------------------
                                    731 	.area RSEG    (ABS,DATA)
      000000                        732 	.org 0x0000
                           0000E0   733 G$ACC_0$0$0 == 0x00e0
                           0000E0   734 _ACC_0	=	0x00e0
                           0000E1   735 G$ACC_1$0$0 == 0x00e1
                           0000E1   736 _ACC_1	=	0x00e1
                           0000E2   737 G$ACC_2$0$0 == 0x00e2
                           0000E2   738 _ACC_2	=	0x00e2
                           0000E3   739 G$ACC_3$0$0 == 0x00e3
                           0000E3   740 _ACC_3	=	0x00e3
                           0000E4   741 G$ACC_4$0$0 == 0x00e4
                           0000E4   742 _ACC_4	=	0x00e4
                           0000E5   743 G$ACC_5$0$0 == 0x00e5
                           0000E5   744 _ACC_5	=	0x00e5
                           0000E6   745 G$ACC_6$0$0 == 0x00e6
                           0000E6   746 _ACC_6	=	0x00e6
                           0000E7   747 G$ACC_7$0$0 == 0x00e7
                           0000E7   748 _ACC_7	=	0x00e7
                           0000F0   749 G$B_0$0$0 == 0x00f0
                           0000F0   750 _B_0	=	0x00f0
                           0000F1   751 G$B_1$0$0 == 0x00f1
                           0000F1   752 _B_1	=	0x00f1
                           0000F2   753 G$B_2$0$0 == 0x00f2
                           0000F2   754 _B_2	=	0x00f2
                           0000F3   755 G$B_3$0$0 == 0x00f3
                           0000F3   756 _B_3	=	0x00f3
                           0000F4   757 G$B_4$0$0 == 0x00f4
                           0000F4   758 _B_4	=	0x00f4
                           0000F5   759 G$B_5$0$0 == 0x00f5
                           0000F5   760 _B_5	=	0x00f5
                           0000F6   761 G$B_6$0$0 == 0x00f6
                           0000F6   762 _B_6	=	0x00f6
                           0000F7   763 G$B_7$0$0 == 0x00f7
                           0000F7   764 _B_7	=	0x00f7
                           0000A0   765 G$E2IE_0$0$0 == 0x00a0
                           0000A0   766 _E2IE_0	=	0x00a0
                           0000A1   767 G$E2IE_1$0$0 == 0x00a1
                           0000A1   768 _E2IE_1	=	0x00a1
                           0000A2   769 G$E2IE_2$0$0 == 0x00a2
                           0000A2   770 _E2IE_2	=	0x00a2
                           0000A3   771 G$E2IE_3$0$0 == 0x00a3
                           0000A3   772 _E2IE_3	=	0x00a3
                           0000A4   773 G$E2IE_4$0$0 == 0x00a4
                           0000A4   774 _E2IE_4	=	0x00a4
                           0000A5   775 G$E2IE_5$0$0 == 0x00a5
                           0000A5   776 _E2IE_5	=	0x00a5
                           0000A6   777 G$E2IE_6$0$0 == 0x00a6
                           0000A6   778 _E2IE_6	=	0x00a6
                           0000A7   779 G$E2IE_7$0$0 == 0x00a7
                           0000A7   780 _E2IE_7	=	0x00a7
                           0000C0   781 G$E2IP_0$0$0 == 0x00c0
                           0000C0   782 _E2IP_0	=	0x00c0
                           0000C1   783 G$E2IP_1$0$0 == 0x00c1
                           0000C1   784 _E2IP_1	=	0x00c1
                           0000C2   785 G$E2IP_2$0$0 == 0x00c2
                           0000C2   786 _E2IP_2	=	0x00c2
                           0000C3   787 G$E2IP_3$0$0 == 0x00c3
                           0000C3   788 _E2IP_3	=	0x00c3
                           0000C4   789 G$E2IP_4$0$0 == 0x00c4
                           0000C4   790 _E2IP_4	=	0x00c4
                           0000C5   791 G$E2IP_5$0$0 == 0x00c5
                           0000C5   792 _E2IP_5	=	0x00c5
                           0000C6   793 G$E2IP_6$0$0 == 0x00c6
                           0000C6   794 _E2IP_6	=	0x00c6
                           0000C7   795 G$E2IP_7$0$0 == 0x00c7
                           0000C7   796 _E2IP_7	=	0x00c7
                           000098   797 G$EIE_0$0$0 == 0x0098
                           000098   798 _EIE_0	=	0x0098
                           000099   799 G$EIE_1$0$0 == 0x0099
                           000099   800 _EIE_1	=	0x0099
                           00009A   801 G$EIE_2$0$0 == 0x009a
                           00009A   802 _EIE_2	=	0x009a
                           00009B   803 G$EIE_3$0$0 == 0x009b
                           00009B   804 _EIE_3	=	0x009b
                           00009C   805 G$EIE_4$0$0 == 0x009c
                           00009C   806 _EIE_4	=	0x009c
                           00009D   807 G$EIE_5$0$0 == 0x009d
                           00009D   808 _EIE_5	=	0x009d
                           00009E   809 G$EIE_6$0$0 == 0x009e
                           00009E   810 _EIE_6	=	0x009e
                           00009F   811 G$EIE_7$0$0 == 0x009f
                           00009F   812 _EIE_7	=	0x009f
                           0000B0   813 G$EIP_0$0$0 == 0x00b0
                           0000B0   814 _EIP_0	=	0x00b0
                           0000B1   815 G$EIP_1$0$0 == 0x00b1
                           0000B1   816 _EIP_1	=	0x00b1
                           0000B2   817 G$EIP_2$0$0 == 0x00b2
                           0000B2   818 _EIP_2	=	0x00b2
                           0000B3   819 G$EIP_3$0$0 == 0x00b3
                           0000B3   820 _EIP_3	=	0x00b3
                           0000B4   821 G$EIP_4$0$0 == 0x00b4
                           0000B4   822 _EIP_4	=	0x00b4
                           0000B5   823 G$EIP_5$0$0 == 0x00b5
                           0000B5   824 _EIP_5	=	0x00b5
                           0000B6   825 G$EIP_6$0$0 == 0x00b6
                           0000B6   826 _EIP_6	=	0x00b6
                           0000B7   827 G$EIP_7$0$0 == 0x00b7
                           0000B7   828 _EIP_7	=	0x00b7
                           0000A8   829 G$IE_0$0$0 == 0x00a8
                           0000A8   830 _IE_0	=	0x00a8
                           0000A9   831 G$IE_1$0$0 == 0x00a9
                           0000A9   832 _IE_1	=	0x00a9
                           0000AA   833 G$IE_2$0$0 == 0x00aa
                           0000AA   834 _IE_2	=	0x00aa
                           0000AB   835 G$IE_3$0$0 == 0x00ab
                           0000AB   836 _IE_3	=	0x00ab
                           0000AC   837 G$IE_4$0$0 == 0x00ac
                           0000AC   838 _IE_4	=	0x00ac
                           0000AD   839 G$IE_5$0$0 == 0x00ad
                           0000AD   840 _IE_5	=	0x00ad
                           0000AE   841 G$IE_6$0$0 == 0x00ae
                           0000AE   842 _IE_6	=	0x00ae
                           0000AF   843 G$IE_7$0$0 == 0x00af
                           0000AF   844 _IE_7	=	0x00af
                           0000AF   845 G$EA$0$0 == 0x00af
                           0000AF   846 _EA	=	0x00af
                           0000B8   847 G$IP_0$0$0 == 0x00b8
                           0000B8   848 _IP_0	=	0x00b8
                           0000B9   849 G$IP_1$0$0 == 0x00b9
                           0000B9   850 _IP_1	=	0x00b9
                           0000BA   851 G$IP_2$0$0 == 0x00ba
                           0000BA   852 _IP_2	=	0x00ba
                           0000BB   853 G$IP_3$0$0 == 0x00bb
                           0000BB   854 _IP_3	=	0x00bb
                           0000BC   855 G$IP_4$0$0 == 0x00bc
                           0000BC   856 _IP_4	=	0x00bc
                           0000BD   857 G$IP_5$0$0 == 0x00bd
                           0000BD   858 _IP_5	=	0x00bd
                           0000BE   859 G$IP_6$0$0 == 0x00be
                           0000BE   860 _IP_6	=	0x00be
                           0000BF   861 G$IP_7$0$0 == 0x00bf
                           0000BF   862 _IP_7	=	0x00bf
                           0000D0   863 G$P$0$0 == 0x00d0
                           0000D0   864 _P	=	0x00d0
                           0000D1   865 G$F1$0$0 == 0x00d1
                           0000D1   866 _F1	=	0x00d1
                           0000D2   867 G$OV$0$0 == 0x00d2
                           0000D2   868 _OV	=	0x00d2
                           0000D3   869 G$RS0$0$0 == 0x00d3
                           0000D3   870 _RS0	=	0x00d3
                           0000D4   871 G$RS1$0$0 == 0x00d4
                           0000D4   872 _RS1	=	0x00d4
                           0000D5   873 G$F0$0$0 == 0x00d5
                           0000D5   874 _F0	=	0x00d5
                           0000D6   875 G$AC$0$0 == 0x00d6
                           0000D6   876 _AC	=	0x00d6
                           0000D7   877 G$CY$0$0 == 0x00d7
                           0000D7   878 _CY	=	0x00d7
                           0000C8   879 G$PINA_0$0$0 == 0x00c8
                           0000C8   880 _PINA_0	=	0x00c8
                           0000C9   881 G$PINA_1$0$0 == 0x00c9
                           0000C9   882 _PINA_1	=	0x00c9
                           0000CA   883 G$PINA_2$0$0 == 0x00ca
                           0000CA   884 _PINA_2	=	0x00ca
                           0000CB   885 G$PINA_3$0$0 == 0x00cb
                           0000CB   886 _PINA_3	=	0x00cb
                           0000CC   887 G$PINA_4$0$0 == 0x00cc
                           0000CC   888 _PINA_4	=	0x00cc
                           0000CD   889 G$PINA_5$0$0 == 0x00cd
                           0000CD   890 _PINA_5	=	0x00cd
                           0000CE   891 G$PINA_6$0$0 == 0x00ce
                           0000CE   892 _PINA_6	=	0x00ce
                           0000CF   893 G$PINA_7$0$0 == 0x00cf
                           0000CF   894 _PINA_7	=	0x00cf
                           0000E8   895 G$PINB_0$0$0 == 0x00e8
                           0000E8   896 _PINB_0	=	0x00e8
                           0000E9   897 G$PINB_1$0$0 == 0x00e9
                           0000E9   898 _PINB_1	=	0x00e9
                           0000EA   899 G$PINB_2$0$0 == 0x00ea
                           0000EA   900 _PINB_2	=	0x00ea
                           0000EB   901 G$PINB_3$0$0 == 0x00eb
                           0000EB   902 _PINB_3	=	0x00eb
                           0000EC   903 G$PINB_4$0$0 == 0x00ec
                           0000EC   904 _PINB_4	=	0x00ec
                           0000ED   905 G$PINB_5$0$0 == 0x00ed
                           0000ED   906 _PINB_5	=	0x00ed
                           0000EE   907 G$PINB_6$0$0 == 0x00ee
                           0000EE   908 _PINB_6	=	0x00ee
                           0000EF   909 G$PINB_7$0$0 == 0x00ef
                           0000EF   910 _PINB_7	=	0x00ef
                           0000F8   911 G$PINC_0$0$0 == 0x00f8
                           0000F8   912 _PINC_0	=	0x00f8
                           0000F9   913 G$PINC_1$0$0 == 0x00f9
                           0000F9   914 _PINC_1	=	0x00f9
                           0000FA   915 G$PINC_2$0$0 == 0x00fa
                           0000FA   916 _PINC_2	=	0x00fa
                           0000FB   917 G$PINC_3$0$0 == 0x00fb
                           0000FB   918 _PINC_3	=	0x00fb
                           0000FC   919 G$PINC_4$0$0 == 0x00fc
                           0000FC   920 _PINC_4	=	0x00fc
                           0000FD   921 G$PINC_5$0$0 == 0x00fd
                           0000FD   922 _PINC_5	=	0x00fd
                           0000FE   923 G$PINC_6$0$0 == 0x00fe
                           0000FE   924 _PINC_6	=	0x00fe
                           0000FF   925 G$PINC_7$0$0 == 0x00ff
                           0000FF   926 _PINC_7	=	0x00ff
                           000080   927 G$PORTA_0$0$0 == 0x0080
                           000080   928 _PORTA_0	=	0x0080
                           000081   929 G$PORTA_1$0$0 == 0x0081
                           000081   930 _PORTA_1	=	0x0081
                           000082   931 G$PORTA_2$0$0 == 0x0082
                           000082   932 _PORTA_2	=	0x0082
                           000083   933 G$PORTA_3$0$0 == 0x0083
                           000083   934 _PORTA_3	=	0x0083
                           000084   935 G$PORTA_4$0$0 == 0x0084
                           000084   936 _PORTA_4	=	0x0084
                           000085   937 G$PORTA_5$0$0 == 0x0085
                           000085   938 _PORTA_5	=	0x0085
                           000086   939 G$PORTA_6$0$0 == 0x0086
                           000086   940 _PORTA_6	=	0x0086
                           000087   941 G$PORTA_7$0$0 == 0x0087
                           000087   942 _PORTA_7	=	0x0087
                           000088   943 G$PORTB_0$0$0 == 0x0088
                           000088   944 _PORTB_0	=	0x0088
                           000089   945 G$PORTB_1$0$0 == 0x0089
                           000089   946 _PORTB_1	=	0x0089
                           00008A   947 G$PORTB_2$0$0 == 0x008a
                           00008A   948 _PORTB_2	=	0x008a
                           00008B   949 G$PORTB_3$0$0 == 0x008b
                           00008B   950 _PORTB_3	=	0x008b
                           00008C   951 G$PORTB_4$0$0 == 0x008c
                           00008C   952 _PORTB_4	=	0x008c
                           00008D   953 G$PORTB_5$0$0 == 0x008d
                           00008D   954 _PORTB_5	=	0x008d
                           00008E   955 G$PORTB_6$0$0 == 0x008e
                           00008E   956 _PORTB_6	=	0x008e
                           00008F   957 G$PORTB_7$0$0 == 0x008f
                           00008F   958 _PORTB_7	=	0x008f
                           000090   959 G$PORTC_0$0$0 == 0x0090
                           000090   960 _PORTC_0	=	0x0090
                           000091   961 G$PORTC_1$0$0 == 0x0091
                           000091   962 _PORTC_1	=	0x0091
                           000092   963 G$PORTC_2$0$0 == 0x0092
                           000092   964 _PORTC_2	=	0x0092
                           000093   965 G$PORTC_3$0$0 == 0x0093
                           000093   966 _PORTC_3	=	0x0093
                           000094   967 G$PORTC_4$0$0 == 0x0094
                           000094   968 _PORTC_4	=	0x0094
                           000095   969 G$PORTC_5$0$0 == 0x0095
                           000095   970 _PORTC_5	=	0x0095
                           000096   971 G$PORTC_6$0$0 == 0x0096
                           000096   972 _PORTC_6	=	0x0096
                           000097   973 G$PORTC_7$0$0 == 0x0097
                           000097   974 _PORTC_7	=	0x0097
                                    975 ;--------------------------------------------------------
                                    976 ; overlayable register banks
                                    977 ;--------------------------------------------------------
                                    978 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        979 	.ds 8
                                    980 ;--------------------------------------------------------
                                    981 ; internal ram data
                                    982 ;--------------------------------------------------------
                                    983 	.area DSEG    (DATA)
                           000000   984 G$pkt_counter$0$0==.
      000022                        985 _pkt_counter::
      000022                        986 	.ds 2
                           000002   987 G$coldstart$0$0==.
      000024                        988 _coldstart::
      000024                        989 	.ds 1
                           000003   990 Lmain.main$saved_button_state$1$375==.
      000025                        991 _main_saved_button_state_1_375:
      000025                        992 	.ds 1
                                    993 ;--------------------------------------------------------
                                    994 ; overlayable items in internal ram 
                                    995 ;--------------------------------------------------------
                                    996 ;--------------------------------------------------------
                                    997 ; Stack segment in internal ram 
                                    998 ;--------------------------------------------------------
                                    999 	.area	SSEG
      00003E                       1000 __start__stack:
      00003E                       1001 	.ds	1
                                   1002 
                                   1003 ;--------------------------------------------------------
                                   1004 ; indirectly addressable internal ram data
                                   1005 ;--------------------------------------------------------
                                   1006 	.area ISEG    (DATA)
                                   1007 ;--------------------------------------------------------
                                   1008 ; absolute internal ram data
                                   1009 ;--------------------------------------------------------
                                   1010 	.area IABS    (ABS,DATA)
                                   1011 	.area IABS    (ABS,DATA)
                                   1012 ;--------------------------------------------------------
                                   1013 ; bit data
                                   1014 ;--------------------------------------------------------
                                   1015 	.area BSEG    (BIT)
                           000000  1016 Lmain._sdcc_external_startup$sloc0$1$0==.
      000001                       1017 __sdcc_external_startup_sloc0_1_0:
      000001                       1018 	.ds 1
                                   1019 ;--------------------------------------------------------
                                   1020 ; paged external ram data
                                   1021 ;--------------------------------------------------------
                                   1022 	.area PSEG    (PAG,XDATA)
                                   1023 ;--------------------------------------------------------
                                   1024 ; external ram data
                                   1025 ;--------------------------------------------------------
                                   1026 	.area XSEG    (XDATA)
                           007020  1027 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1028 _ADCCH0VAL0	=	0x7020
                           007021  1029 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1030 _ADCCH0VAL1	=	0x7021
                           007020  1031 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1032 _ADCCH0VAL	=	0x7020
                           007022  1033 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1034 _ADCCH1VAL0	=	0x7022
                           007023  1035 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1036 _ADCCH1VAL1	=	0x7023
                           007022  1037 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1038 _ADCCH1VAL	=	0x7022
                           007024  1039 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1040 _ADCCH2VAL0	=	0x7024
                           007025  1041 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1042 _ADCCH2VAL1	=	0x7025
                           007024  1043 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1044 _ADCCH2VAL	=	0x7024
                           007026  1045 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1046 _ADCCH3VAL0	=	0x7026
                           007027  1047 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1048 _ADCCH3VAL1	=	0x7027
                           007026  1049 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1050 _ADCCH3VAL	=	0x7026
                           007028  1051 G$ADCTUNE0$0$0 == 0x7028
                           007028  1052 _ADCTUNE0	=	0x7028
                           007029  1053 G$ADCTUNE1$0$0 == 0x7029
                           007029  1054 _ADCTUNE1	=	0x7029
                           00702A  1055 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1056 _ADCTUNE2	=	0x702a
                           007010  1057 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1058 _DMA0ADDR0	=	0x7010
                           007011  1059 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1060 _DMA0ADDR1	=	0x7011
                           007010  1061 G$DMA0ADDR$0$0 == 0x7010
                           007010  1062 _DMA0ADDR	=	0x7010
                           007014  1063 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1064 _DMA0CONFIG	=	0x7014
                           007012  1065 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1066 _DMA1ADDR0	=	0x7012
                           007013  1067 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1068 _DMA1ADDR1	=	0x7013
                           007012  1069 G$DMA1ADDR$0$0 == 0x7012
                           007012  1070 _DMA1ADDR	=	0x7012
                           007015  1071 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1072 _DMA1CONFIG	=	0x7015
                           007070  1073 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1074 _FRCOSCCONFIG	=	0x7070
                           007071  1075 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1076 _FRCOSCCTRL	=	0x7071
                           007076  1077 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1078 _FRCOSCFREQ0	=	0x7076
                           007077  1079 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1080 _FRCOSCFREQ1	=	0x7077
                           007076  1081 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1082 _FRCOSCFREQ	=	0x7076
                           007072  1083 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1084 _FRCOSCKFILT0	=	0x7072
                           007073  1085 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1086 _FRCOSCKFILT1	=	0x7073
                           007072  1087 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1088 _FRCOSCKFILT	=	0x7072
                           007078  1089 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1090 _FRCOSCPER0	=	0x7078
                           007079  1091 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1092 _FRCOSCPER1	=	0x7079
                           007078  1093 G$FRCOSCPER$0$0 == 0x7078
                           007078  1094 _FRCOSCPER	=	0x7078
                           007074  1095 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1096 _FRCOSCREF0	=	0x7074
                           007075  1097 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1098 _FRCOSCREF1	=	0x7075
                           007074  1099 G$FRCOSCREF$0$0 == 0x7074
                           007074  1100 _FRCOSCREF	=	0x7074
                           007007  1101 G$ANALOGA$0$0 == 0x7007
                           007007  1102 _ANALOGA	=	0x7007
                           00700C  1103 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1104 _GPIOENABLE	=	0x700c
                           007003  1105 G$EXTIRQ$0$0 == 0x7003
                           007003  1106 _EXTIRQ	=	0x7003
                           007000  1107 G$INTCHGA$0$0 == 0x7000
                           007000  1108 _INTCHGA	=	0x7000
                           007001  1109 G$INTCHGB$0$0 == 0x7001
                           007001  1110 _INTCHGB	=	0x7001
                           007002  1111 G$INTCHGC$0$0 == 0x7002
                           007002  1112 _INTCHGC	=	0x7002
                           007008  1113 G$PALTA$0$0 == 0x7008
                           007008  1114 _PALTA	=	0x7008
                           007009  1115 G$PALTB$0$0 == 0x7009
                           007009  1116 _PALTB	=	0x7009
                           00700A  1117 G$PALTC$0$0 == 0x700a
                           00700A  1118 _PALTC	=	0x700a
                           007046  1119 G$PALTRADIO$0$0 == 0x7046
                           007046  1120 _PALTRADIO	=	0x7046
                           007004  1121 G$PINCHGA$0$0 == 0x7004
                           007004  1122 _PINCHGA	=	0x7004
                           007005  1123 G$PINCHGB$0$0 == 0x7005
                           007005  1124 _PINCHGB	=	0x7005
                           007006  1125 G$PINCHGC$0$0 == 0x7006
                           007006  1126 _PINCHGC	=	0x7006
                           00700B  1127 G$PINSEL$0$0 == 0x700b
                           00700B  1128 _PINSEL	=	0x700b
                           007060  1129 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1130 _LPOSCCONFIG	=	0x7060
                           007066  1131 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1132 _LPOSCFREQ0	=	0x7066
                           007067  1133 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1134 _LPOSCFREQ1	=	0x7067
                           007066  1135 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1136 _LPOSCFREQ	=	0x7066
                           007062  1137 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1138 _LPOSCKFILT0	=	0x7062
                           007063  1139 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1140 _LPOSCKFILT1	=	0x7063
                           007062  1141 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1142 _LPOSCKFILT	=	0x7062
                           007068  1143 G$LPOSCPER0$0$0 == 0x7068
                           007068  1144 _LPOSCPER0	=	0x7068
                           007069  1145 G$LPOSCPER1$0$0 == 0x7069
                           007069  1146 _LPOSCPER1	=	0x7069
                           007068  1147 G$LPOSCPER$0$0 == 0x7068
                           007068  1148 _LPOSCPER	=	0x7068
                           007064  1149 G$LPOSCREF0$0$0 == 0x7064
                           007064  1150 _LPOSCREF0	=	0x7064
                           007065  1151 G$LPOSCREF1$0$0 == 0x7065
                           007065  1152 _LPOSCREF1	=	0x7065
                           007064  1153 G$LPOSCREF$0$0 == 0x7064
                           007064  1154 _LPOSCREF	=	0x7064
                           007054  1155 G$LPXOSCGM$0$0 == 0x7054
                           007054  1156 _LPXOSCGM	=	0x7054
                           007F01  1157 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1158 _MISCCTRL	=	0x7f01
                           007053  1159 G$OSCCALIB$0$0 == 0x7053
                           007053  1160 _OSCCALIB	=	0x7053
                           007050  1161 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1162 _OSCFORCERUN	=	0x7050
                           007052  1163 G$OSCREADY$0$0 == 0x7052
                           007052  1164 _OSCREADY	=	0x7052
                           007051  1165 G$OSCRUN$0$0 == 0x7051
                           007051  1166 _OSCRUN	=	0x7051
                           007040  1167 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1168 _RADIOFDATAADDR0	=	0x7040
                           007041  1169 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1170 _RADIOFDATAADDR1	=	0x7041
                           007040  1171 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1172 _RADIOFDATAADDR	=	0x7040
                           007042  1173 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1174 _RADIOFSTATADDR0	=	0x7042
                           007043  1175 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1176 _RADIOFSTATADDR1	=	0x7043
                           007042  1177 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1178 _RADIOFSTATADDR	=	0x7042
                           007044  1179 G$RADIOMUX$0$0 == 0x7044
                           007044  1180 _RADIOMUX	=	0x7044
                           007084  1181 G$SCRATCH0$0$0 == 0x7084
                           007084  1182 _SCRATCH0	=	0x7084
                           007085  1183 G$SCRATCH1$0$0 == 0x7085
                           007085  1184 _SCRATCH1	=	0x7085
                           007086  1185 G$SCRATCH2$0$0 == 0x7086
                           007086  1186 _SCRATCH2	=	0x7086
                           007087  1187 G$SCRATCH3$0$0 == 0x7087
                           007087  1188 _SCRATCH3	=	0x7087
                           007F00  1189 G$SILICONREV$0$0 == 0x7f00
                           007F00  1190 _SILICONREV	=	0x7f00
                           007F19  1191 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1192 _XTALAMPL	=	0x7f19
                           007F18  1193 G$XTALOSC$0$0 == 0x7f18
                           007F18  1194 _XTALOSC	=	0x7f18
                           007F1A  1195 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1196 _XTALREADY	=	0x7f1a
                           00FC06  1197 Fmain$flash_deviceid$0$0 == 0xfc06
                           00FC06  1198 _flash_deviceid	=	0xfc06
                           00FC00  1199 Fmain$flash_calsector$0$0 == 0xfc00
                           00FC00  1200 _flash_calsector	=	0xfc00
                           000000  1201 G$wakeup_desc$0$0==.
      0002A0                       1202 _wakeup_desc::
      0002A0                       1203 	.ds 8
                           000008  1204 Lmain.transmit_packet$demo_packet_$1$342==.
      0002A8                       1205 _transmit_packet_demo_packet__1_342:
      0002A8                       1206 	.ds 1
                           000009  1207 Lmain.transmit_beacon$beacon_packet_$1$345==.
      0002A9                       1208 _transmit_beacon_beacon_packet__1_345:
      0002A9                       1209 	.ds 8
                           000011  1210 Lmain.transmit_ack$ack_packet_$1$348==.
      0002B1                       1211 _transmit_ack_ack_packet__1_348:
      0002B1                       1212 	.ds 4
                           000015  1213 Lmain.wakeup_callback$timer0Period$1$366==.
      0002B5                       1214 _wakeup_callback_timer0Period_1_366:
      0002B5                       1215 	.ds 2
                           000017  1216 Lmain.wakeup_callback$bc_flg$1$366==.
      0002B7                       1217 _wakeup_callback_bc_flg_1_366:
      0002B7                       1218 	.ds 1
                                   1219 ;--------------------------------------------------------
                                   1220 ; absolute external ram data
                                   1221 ;--------------------------------------------------------
                                   1222 	.area XABS    (ABS,XDATA)
                                   1223 ;--------------------------------------------------------
                                   1224 ; external initialized ram data
                                   1225 ;--------------------------------------------------------
                                   1226 	.area XISEG   (XDATA)
                           000000  1227 G$beacon_packet$0$0==.
      000470                       1228 _beacon_packet::
      000470                       1229 	.ds 8
                           000008  1230 G$ack_packet$0$0==.
      000478                       1231 _ack_packet::
      000478                       1232 	.ds 4
                           00000C  1233 G$pkts_received$0$0==.
      00047C                       1234 _pkts_received::
      00047C                       1235 	.ds 2
                           00000E  1236 G$pkts_missing$0$0==.
      00047E                       1237 _pkts_missing::
      00047E                       1238 	.ds 2
                           000010  1239 G$bc_counter$0$0==.
      000480                       1240 _bc_counter::
      000480                       1241 	.ds 2
                           000012  1242 G$rcv_flg$0$0==.
      000482                       1243 _rcv_flg::
      000482                       1244 	.ds 1
                           000013  1245 G$pkts_id1$0$0==.
      000483                       1246 _pkts_id1::
      000483                       1247 	.ds 2
                           000015  1248 G$pkts_id3$0$0==.
      000485                       1249 _pkts_id3::
      000485                       1250 	.ds 2
                           000017  1251 G$rcv_flg_id1$0$0==.
      000487                       1252 _rcv_flg_id1::
      000487                       1253 	.ds 1
                           000018  1254 G$rcv_flg_id3$0$0==.
      000488                       1255 _rcv_flg_id3::
      000488                       1256 	.ds 1
                                   1257 	.area HOME    (CODE)
                                   1258 	.area GSINIT0 (CODE)
                                   1259 	.area GSINIT1 (CODE)
                                   1260 	.area GSINIT2 (CODE)
                                   1261 	.area GSINIT3 (CODE)
                                   1262 	.area GSINIT4 (CODE)
                                   1263 	.area GSINIT5 (CODE)
                                   1264 	.area GSINIT  (CODE)
                                   1265 	.area GSFINAL (CODE)
                                   1266 	.area CSEG    (CODE)
                                   1267 ;--------------------------------------------------------
                                   1268 ; interrupt vector 
                                   1269 ;--------------------------------------------------------
                                   1270 	.area HOME    (CODE)
      000000                       1271 __interrupt_vect:
      000000 02 03 11         [24] 1272 	ljmp	__sdcc_gsinit_startup
      000003 32               [24] 1273 	reti
      000004                       1274 	.ds	7
      00000B 02 00 B1         [24] 1275 	ljmp	_wtimer_irq
      00000E                       1276 	.ds	5
      000013 32               [24] 1277 	reti
      000014                       1278 	.ds	7
      00001B 32               [24] 1279 	reti
      00001C                       1280 	.ds	7
      000023 02 16 D5         [24] 1281 	ljmp	_axradio_isr
      000026                       1282 	.ds	5
      00002B 32               [24] 1283 	reti
      00002C                       1284 	.ds	7
      000033 02 41 E0         [24] 1285 	ljmp	_pwrmgmt_irq
      000036                       1286 	.ds	5
      00003B 32               [24] 1287 	reti
      00003C                       1288 	.ds	7
      000043 32               [24] 1289 	reti
      000044                       1290 	.ds	7
      00004B 32               [24] 1291 	reti
      00004C                       1292 	.ds	7
      000053 32               [24] 1293 	reti
      000054                       1294 	.ds	7
      00005B 02 02 A3         [24] 1295 	ljmp	_uart0_irq
      00005E                       1296 	.ds	5
      000063 02 02 DA         [24] 1297 	ljmp	_uart1_irq
      000066                       1298 	.ds	5
      00006B 32               [24] 1299 	reti
      00006C                       1300 	.ds	7
      000073 32               [24] 1301 	reti
      000074                       1302 	.ds	7
      00007B 32               [24] 1303 	reti
      00007C                       1304 	.ds	7
      000083 32               [24] 1305 	reti
      000084                       1306 	.ds	7
      00008B 32               [24] 1307 	reti
      00008C                       1308 	.ds	7
      000093 32               [24] 1309 	reti
      000094                       1310 	.ds	7
      00009B 32               [24] 1311 	reti
      00009C                       1312 	.ds	7
      0000A3 32               [24] 1313 	reti
      0000A4                       1314 	.ds	7
      0000AB 02 02 6C         [24] 1315 	ljmp	_dbglink_irq
                                   1316 ;--------------------------------------------------------
                                   1317 ; global & static initialisations
                                   1318 ;--------------------------------------------------------
                                   1319 	.area HOME    (CODE)
                                   1320 	.area GSINIT  (CODE)
                                   1321 	.area GSFINAL (CODE)
                                   1322 	.area GSINIT  (CODE)
                                   1323 	.globl __sdcc_gsinit_startup
                                   1324 	.globl __sdcc_program_startup
                                   1325 	.globl __start__stack
                                   1326 	.globl __mcs51_genXINIT
                                   1327 	.globl __mcs51_genXRAMCLEAR
                                   1328 	.globl __mcs51_genRAMCLEAR
                                   1329 ;------------------------------------------------------------
                                   1330 ;Allocation info for local variables in function 'main'
                                   1331 ;------------------------------------------------------------
                                   1332 ;saved_button_state        Allocated with name '_main_saved_button_state_1_375'
                                   1333 ;i                         Allocated to registers r7 
                                   1334 ;x                         Allocated to registers r6 
                                   1335 ;flg                       Allocated to registers r6 
                                   1336 ;flg                       Allocated to registers r7 
                                   1337 ;------------------------------------------------------------
                           000000  1338 	G$main$0$0 ==.
                           000000  1339 	C$main.c$374$1$375 ==.
                                   1340 ;	main.c:374: static uint8_t __data saved_button_state = 0xFF;
      000398 75 25 FF         [24] 1341 	mov	_main_saved_button_state_1_375,#0xff
                           000003  1342 	C$main.c$79$1$375 ==.
                                   1343 ;	main.c:79: uint16_t __data pkt_counter = 0;
      00039B E4               [12] 1344 	clr	a
      00039C F5 22            [12] 1345 	mov	_pkt_counter,a
      00039E F5 23            [12] 1346 	mov	(_pkt_counter + 1),a
                           000008  1347 	C$main.c$80$1$375 ==.
                                   1348 ;	main.c:80: uint8_t __data coldstart = 1; /* caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */
      0003A0 75 24 01         [24] 1349 	mov	_coldstart,#0x01
                                   1350 	.area GSFINAL (CODE)
      0003A3 02 00 AE         [24] 1351 	ljmp	__sdcc_program_startup
                                   1352 ;--------------------------------------------------------
                                   1353 ; Home
                                   1354 ;--------------------------------------------------------
                                   1355 	.area HOME    (CODE)
                                   1356 	.area HOME    (CODE)
      0000AE                       1357 __sdcc_program_startup:
      0000AE 02 46 AB         [24] 1358 	ljmp	_main
                                   1359 ;	return from main will return to caller
                                   1360 ;--------------------------------------------------------
                                   1361 ; code
                                   1362 ;--------------------------------------------------------
                                   1363 	.area CSEG    (CODE)
                                   1364 ;------------------------------------------------------------
                                   1365 ;Allocation info for local variables in function 'pwrmgmt_irq'
                                   1366 ;------------------------------------------------------------
                                   1367 ;pc                        Allocated to registers r7 
                                   1368 ;------------------------------------------------------------
                           000000  1369 	Fmain$pwrmgmt_irq$0$0 ==.
                           000000  1370 	C$main.c$97$0$0 ==.
                                   1371 ;	main.c:97: static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
                                   1372 ;	-----------------------------------------
                                   1373 ;	 function pwrmgmt_irq
                                   1374 ;	-----------------------------------------
      0041E0                       1375 _pwrmgmt_irq:
                           000007  1376 	ar7 = 0x07
                           000006  1377 	ar6 = 0x06
                           000005  1378 	ar5 = 0x05
                           000004  1379 	ar4 = 0x04
                           000003  1380 	ar3 = 0x03
                           000002  1381 	ar2 = 0x02
                           000001  1382 	ar1 = 0x01
                           000000  1383 	ar0 = 0x00
      0041E0 C0 E0            [24] 1384 	push	acc
      0041E2 C0 82            [24] 1385 	push	dpl
      0041E4 C0 83            [24] 1386 	push	dph
      0041E6 C0 07            [24] 1387 	push	ar7
      0041E8 C0 D0            [24] 1388 	push	psw
      0041EA 75 D0 00         [24] 1389 	mov	psw,#0x00
                           00000D  1390 	C$main.c$99$1$0 ==.
                                   1391 ;	main.c:99: uint8_t pc = PCON;
                           00000D  1392 	C$main.c$101$1$340 ==.
                                   1393 ;	main.c:101: if (!(pc & 0x80))
      0041ED E5 87            [12] 1394 	mov	a,_PCON
      0041EF FF               [12] 1395 	mov	r7,a
      0041F0 20 E7 02         [24] 1396 	jb	acc.7,00102$
                           000013  1397 	C$main.c$102$1$340 ==.
                                   1398 ;	main.c:102: return;
      0041F3 80 10            [24] 1399 	sjmp	00106$
      0041F5                       1400 00102$:
                           000015  1401 	C$main.c$104$1$340 ==.
                                   1402 ;	main.c:104: GPIOENABLE = 0;
      0041F5 90 70 0C         [24] 1403 	mov	dptr,#_GPIOENABLE
      0041F8 E4               [12] 1404 	clr	a
      0041F9 F0               [24] 1405 	movx	@dptr,a
                           00001A  1406 	C$main.c$105$1$340 ==.
                                   1407 ;	main.c:105: IE = EIE = E2IE = 0;
                                   1408 ;	1-genFromRTrack replaced	mov	_E2IE,#0x00
      0041FA F5 A0            [12] 1409 	mov	_E2IE,a
                                   1410 ;	1-genFromRTrack replaced	mov	_EIE,#0x00
      0041FC F5 98            [12] 1411 	mov	_EIE,a
                                   1412 ;	1-genFromRTrack replaced	mov	_IE,#0x00
      0041FE F5 A8            [12] 1413 	mov	_IE,a
      004200                       1414 00104$:
                           000020  1415 	C$main.c$108$1$340 ==.
                                   1416 ;	main.c:108: PCON |= 0x01;
      004200 43 87 01         [24] 1417 	orl	_PCON,#0x01
      004203 80 FB            [24] 1418 	sjmp	00104$
      004205                       1419 00106$:
      004205 D0 D0            [24] 1420 	pop	psw
      004207 D0 07            [24] 1421 	pop	ar7
      004209 D0 83            [24] 1422 	pop	dph
      00420B D0 82            [24] 1423 	pop	dpl
      00420D D0 E0            [24] 1424 	pop	acc
                           00002F  1425 	C$main.c$109$1$340 ==.
                           00002F  1426 	XFmain$pwrmgmt_irq$0$0 ==.
      00420F 32               [24] 1427 	reti
                                   1428 ;	eliminated unneeded push/pop b
                                   1429 ;------------------------------------------------------------
                                   1430 ;Allocation info for local variables in function 'transmit_packet'
                                   1431 ;------------------------------------------------------------
                                   1432 ;demo_packet_              Allocated with name '_transmit_packet_demo_packet__1_342'
                                   1433 ;------------------------------------------------------------
                           000030  1434 	Fmain$transmit_packet$0$0 ==.
                           000030  1435 	C$main.c$111$1$340 ==.
                                   1436 ;	main.c:111: static void transmit_packet(void)
                                   1437 ;	-----------------------------------------
                                   1438 ;	 function transmit_packet
                                   1439 ;	-----------------------------------------
      004210                       1440 _transmit_packet:
                           000030  1441 	C$main.c$115$1$342 ==.
                                   1442 ;	main.c:115: ++pkt_counter;
      004210 05 22            [12] 1443 	inc	_pkt_counter
      004212 E4               [12] 1444 	clr	a
      004213 B5 22 02         [24] 1445 	cjne	a,_pkt_counter,00108$
      004216 05 23            [12] 1446 	inc	(_pkt_counter + 1)
      004218                       1447 00108$:
                           000038  1448 	C$main.c$116$1$342 ==.
                                   1449 ;	main.c:116: memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
      004218 75 33 F7         [24] 1450 	mov	_memcpy_PARM_2,#_demo_packet
      00421B 75 34 63         [24] 1451 	mov	(_memcpy_PARM_2 + 1),#(_demo_packet >> 8)
      00421E 75 35 80         [24] 1452 	mov	(_memcpy_PARM_2 + 2),#0x80
      004221 75 36 01         [24] 1453 	mov	_memcpy_PARM_3,#0x01
      004224 75 37 00         [24] 1454 	mov	(_memcpy_PARM_3 + 1),#0x00
      004227 90 02 A8         [24] 1455 	mov	dptr,#_transmit_packet_demo_packet__1_342
      00422A 75 F0 00         [24] 1456 	mov	b,#0x00
      00422D 12 4F F0         [24] 1457 	lcall	_memcpy
                           000050  1458 	C$main.c$118$1$342 ==.
                                   1459 ;	main.c:118: if (framing_insert_counter)
      004230 90 63 F5         [24] 1460 	mov	dptr,#_framing_insert_counter
      004233 E4               [12] 1461 	clr	a
      004234 93               [24] 1462 	movc	a,@a+dptr
      004235 60 24            [24] 1463 	jz	00102$
                           000057  1464 	C$main.c$120$2$343 ==.
                                   1465 ;	main.c:120: demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
      004237 90 63 F6         [24] 1466 	mov	dptr,#_framing_counter_pos
      00423A E4               [12] 1467 	clr	a
      00423B 93               [24] 1468 	movc	a,@a+dptr
      00423C FF               [12] 1469 	mov	r7,a
      00423D 24 A8            [12] 1470 	add	a,#_transmit_packet_demo_packet__1_342
      00423F F5 82            [12] 1471 	mov	dpl,a
      004241 E4               [12] 1472 	clr	a
      004242 34 02            [12] 1473 	addc	a,#(_transmit_packet_demo_packet__1_342 >> 8)
      004244 F5 83            [12] 1474 	mov	dph,a
      004246 AD 22            [24] 1475 	mov	r5,_pkt_counter
      004248 7E 00            [12] 1476 	mov	r6,#0x00
      00424A ED               [12] 1477 	mov	a,r5
      00424B F0               [24] 1478 	movx	@dptr,a
                           00006C  1479 	C$main.c$121$2$343 ==.
                                   1480 ;	main.c:121: demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
      00424C EF               [12] 1481 	mov	a,r7
      00424D 04               [12] 1482 	inc	a
      00424E 24 A8            [12] 1483 	add	a,#_transmit_packet_demo_packet__1_342
      004250 F5 82            [12] 1484 	mov	dpl,a
      004252 E4               [12] 1485 	clr	a
      004253 34 02            [12] 1486 	addc	a,#(_transmit_packet_demo_packet__1_342 >> 8)
      004255 F5 83            [12] 1487 	mov	dph,a
      004257 E5 23            [12] 1488 	mov	a,(_pkt_counter + 1)
      004259 FF               [12] 1489 	mov	r7,a
      00425A F0               [24] 1490 	movx	@dptr,a
      00425B                       1491 00102$:
                           00007B  1492 	C$main.c$124$1$342 ==.
                                   1493 ;	main.c:124: axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));
      00425B 75 1B A8         [24] 1494 	mov	_axradio_transmit_PARM_2,#_transmit_packet_demo_packet__1_342
      00425E 75 1C 02         [24] 1495 	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_packet_demo_packet__1_342 >> 8)
      004261 75 1D 00         [24] 1496 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004264 75 1E 01         [24] 1497 	mov	_axradio_transmit_PARM_3,#0x01
      004267 75 1F 00         [24] 1498 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      00426A 90 63 E6         [24] 1499 	mov	dptr,#_remoteaddr
      00426D 75 F0 80         [24] 1500 	mov	b,#0x80
      004270 12 3A DA         [24] 1501 	lcall	_axradio_transmit
                           000093  1502 	C$main.c$125$1$342 ==.
                           000093  1503 	XFmain$transmit_packet$0$0 ==.
      004273 22               [24] 1504 	ret
                                   1505 ;------------------------------------------------------------
                                   1506 ;Allocation info for local variables in function 'transmit_beacon'
                                   1507 ;------------------------------------------------------------
                                   1508 ;beacon_packet_            Allocated with name '_transmit_beacon_beacon_packet__1_345'
                                   1509 ;------------------------------------------------------------
                           000094  1510 	Fmain$transmit_beacon$0$0 ==.
                           000094  1511 	C$main.c$141$1$342 ==.
                                   1512 ;	main.c:141: static void transmit_beacon(void)
                                   1513 ;	-----------------------------------------
                                   1514 ;	 function transmit_beacon
                                   1515 ;	-----------------------------------------
      004274                       1516 _transmit_beacon:
                           000094  1517 	C$main.c$147$1$345 ==.
                                   1518 ;	main.c:147: ++bc_counter;
      004274 90 04 80         [24] 1519 	mov	dptr,#_bc_counter
      004277 E0               [24] 1520 	movx	a,@dptr
      004278 24 01            [12] 1521 	add	a,#0x01
      00427A F0               [24] 1522 	movx	@dptr,a
      00427B A3               [24] 1523 	inc	dptr
      00427C E0               [24] 1524 	movx	a,@dptr
      00427D 34 00            [12] 1525 	addc	a,#0x00
      00427F F0               [24] 1526 	movx	@dptr,a
                           0000A0  1527 	C$main.c$149$1$345 ==.
                                   1528 ;	main.c:149: memcpy(beacon_packet_, beacon_packet, sizeof(beacon_packet));
      004280 75 33 70         [24] 1529 	mov	_memcpy_PARM_2,#_beacon_packet
      004283 75 34 04         [24] 1530 	mov	(_memcpy_PARM_2 + 1),#(_beacon_packet >> 8)
      004286 75 35 00         [24] 1531 	mov	(_memcpy_PARM_2 + 2),#0x00
      004289 75 36 08         [24] 1532 	mov	_memcpy_PARM_3,#0x08
      00428C 75 37 00         [24] 1533 	mov	(_memcpy_PARM_3 + 1),#0x00
      00428F 90 02 A9         [24] 1534 	mov	dptr,#_transmit_beacon_beacon_packet__1_345
      004292 75 F0 00         [24] 1535 	mov	b,#0x00
      004295 12 4F F0         [24] 1536 	lcall	_memcpy
                           0000B8  1537 	C$main.c$151$1$345 ==.
                                   1538 ;	main.c:151: if (framing_insert_counter)
      004298 90 63 F5         [24] 1539 	mov	dptr,#_framing_insert_counter
      00429B E4               [12] 1540 	clr	a
      00429C 93               [24] 1541 	movc	a,@a+dptr
      00429D 60 30            [24] 1542 	jz	00102$
                           0000BF  1543 	C$main.c$153$2$346 ==.
                                   1544 ;	main.c:153: beacon_packet_[framing_counter_pos] = bc_counter & 0xFF;
      00429F 90 63 F6         [24] 1545 	mov	dptr,#_framing_counter_pos
      0042A2 E4               [12] 1546 	clr	a
      0042A3 93               [24] 1547 	movc	a,@a+dptr
      0042A4 FF               [12] 1548 	mov	r7,a
      0042A5 24 A9            [12] 1549 	add	a,#_transmit_beacon_beacon_packet__1_345
      0042A7 FD               [12] 1550 	mov	r5,a
      0042A8 E4               [12] 1551 	clr	a
      0042A9 34 02            [12] 1552 	addc	a,#(_transmit_beacon_beacon_packet__1_345 >> 8)
      0042AB FE               [12] 1553 	mov	r6,a
      0042AC 90 04 80         [24] 1554 	mov	dptr,#_bc_counter
      0042AF E0               [24] 1555 	movx	a,@dptr
      0042B0 FB               [12] 1556 	mov	r3,a
      0042B1 A3               [24] 1557 	inc	dptr
      0042B2 E0               [24] 1558 	movx	a,@dptr
      0042B3 8D 82            [24] 1559 	mov	dpl,r5
      0042B5 8E 83            [24] 1560 	mov	dph,r6
      0042B7 EB               [12] 1561 	mov	a,r3
      0042B8 F0               [24] 1562 	movx	@dptr,a
                           0000D9  1563 	C$main.c$154$2$346 ==.
                                   1564 ;	main.c:154: beacon_packet_[framing_counter_pos + 1] = (bc_counter >> 8) & 0xFF;
      0042B9 EF               [12] 1565 	mov	a,r7
      0042BA 04               [12] 1566 	inc	a
      0042BB 24 A9            [12] 1567 	add	a,#_transmit_beacon_beacon_packet__1_345
      0042BD FE               [12] 1568 	mov	r6,a
      0042BE E4               [12] 1569 	clr	a
      0042BF 34 02            [12] 1570 	addc	a,#(_transmit_beacon_beacon_packet__1_345 >> 8)
      0042C1 FF               [12] 1571 	mov	r7,a
      0042C2 90 04 80         [24] 1572 	mov	dptr,#_bc_counter
      0042C5 E0               [24] 1573 	movx	a,@dptr
      0042C6 A3               [24] 1574 	inc	dptr
      0042C7 E0               [24] 1575 	movx	a,@dptr
      0042C8 FD               [12] 1576 	mov	r5,a
      0042C9 FC               [12] 1577 	mov	r4,a
      0042CA 8E 82            [24] 1578 	mov	dpl,r6
      0042CC 8F 83            [24] 1579 	mov	dph,r7
      0042CE F0               [24] 1580 	movx	@dptr,a
      0042CF                       1581 00102$:
                           0000EF  1582 	C$main.c$156$1$345 ==.
                                   1583 ;	main.c:156: axradio_transmit(&remoteaddr, beacon_packet_, sizeof(beacon_packet));
      0042CF 75 1B A9         [24] 1584 	mov	_axradio_transmit_PARM_2,#_transmit_beacon_beacon_packet__1_345
      0042D2 75 1C 02         [24] 1585 	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_beacon_beacon_packet__1_345 >> 8)
      0042D5 75 1D 00         [24] 1586 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      0042D8 75 1E 08         [24] 1587 	mov	_axradio_transmit_PARM_3,#0x08
      0042DB 75 1F 00         [24] 1588 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      0042DE 90 63 E6         [24] 1589 	mov	dptr,#_remoteaddr
      0042E1 75 F0 80         [24] 1590 	mov	b,#0x80
      0042E4 12 3A DA         [24] 1591 	lcall	_axradio_transmit
                           000107  1592 	C$main.c$159$1$345 ==.
                                   1593 ;	main.c:159: dbglink_writestr("Beacon: ");
      0042E7 90 65 67         [24] 1594 	mov	dptr,#___str_0
      0042EA 75 F0 80         [24] 1595 	mov	b,#0x80
      0042ED 12 59 1E         [24] 1596 	lcall	_dbglink_writestr
                           000110  1597 	C$main.c$160$1$345 ==.
                                   1598 ;	main.c:160: dbglink_writehex16(bc_counter, 4, WRNUM_PADZERO);
      0042F0 90 04 80         [24] 1599 	mov	dptr,#_bc_counter
      0042F3 E0               [24] 1600 	movx	a,@dptr
      0042F4 FE               [12] 1601 	mov	r6,a
      0042F5 A3               [24] 1602 	inc	dptr
      0042F6 E0               [24] 1603 	movx	a,@dptr
      0042F7 FF               [12] 1604 	mov	r7,a
      0042F8 74 08            [12] 1605 	mov	a,#0x08
      0042FA C0 E0            [24] 1606 	push	acc
      0042FC 03               [12] 1607 	rr	a
      0042FD C0 E0            [24] 1608 	push	acc
      0042FF 8E 82            [24] 1609 	mov	dpl,r6
      004301 8F 83            [24] 1610 	mov	dph,r7
      004303 12 5D 1B         [24] 1611 	lcall	_dbglink_writehex16
      004306 15 81            [12] 1612 	dec	sp
      004308 15 81            [12] 1613 	dec	sp
                           00012A  1614 	C$main.c$161$1$345 ==.
                                   1615 ;	main.c:161: dbglink_writestr("  ");
      00430A 90 65 70         [24] 1616 	mov	dptr,#___str_1
      00430D 75 F0 80         [24] 1617 	mov	b,#0x80
      004310 12 59 1E         [24] 1618 	lcall	_dbglink_writestr
                           000133  1619 	C$main.c$163$1$345 ==.
                                   1620 ;	main.c:163: dbglink_writestr("\n");
      004313 90 65 73         [24] 1621 	mov	dptr,#___str_2
      004316 75 F0 80         [24] 1622 	mov	b,#0x80
      004319 12 59 1E         [24] 1623 	lcall	_dbglink_writestr
                           00013C  1624 	C$main.c$165$1$345 ==.
                           00013C  1625 	XFmain$transmit_beacon$0$0 ==.
      00431C 22               [24] 1626 	ret
                                   1627 ;------------------------------------------------------------
                                   1628 ;Allocation info for local variables in function 'transmit_ack'
                                   1629 ;------------------------------------------------------------
                                   1630 ;ack_packet_               Allocated with name '_transmit_ack_ack_packet__1_348'
                                   1631 ;------------------------------------------------------------
                           00013D  1632 	Fmain$transmit_ack$0$0 ==.
                           00013D  1633 	C$main.c$173$1$345 ==.
                                   1634 ;	main.c:173: static void transmit_ack(void)
                                   1635 ;	-----------------------------------------
                                   1636 ;	 function transmit_ack
                                   1637 ;	-----------------------------------------
      00431D                       1638 _transmit_ack:
                           00013D  1639 	C$main.c$177$1$348 ==.
                                   1640 ;	main.c:177: memcpy(ack_packet_, ack_packet, sizeof(ack_packet));
      00431D 75 33 78         [24] 1641 	mov	_memcpy_PARM_2,#_ack_packet
      004320 75 34 04         [24] 1642 	mov	(_memcpy_PARM_2 + 1),#(_ack_packet >> 8)
      004323 75 35 00         [24] 1643 	mov	(_memcpy_PARM_2 + 2),#0x00
      004326 75 36 04         [24] 1644 	mov	_memcpy_PARM_3,#0x04
      004329 75 37 00         [24] 1645 	mov	(_memcpy_PARM_3 + 1),#0x00
      00432C 90 02 B1         [24] 1646 	mov	dptr,#_transmit_ack_ack_packet__1_348
      00432F 75 F0 00         [24] 1647 	mov	b,#0x00
      004332 12 4F F0         [24] 1648 	lcall	_memcpy
                           000155  1649 	C$main.c$178$1$348 ==.
                                   1650 ;	main.c:178: if(rcv_flg_id1 > 0)
      004335 90 04 87         [24] 1651 	mov	dptr,#_rcv_flg_id1
      004338 E0               [24] 1652 	movx	a,@dptr
      004339 60 0F            [24] 1653 	jz	00102$
                           00015B  1654 	C$main.c$180$2$349 ==.
                                   1655 ;	main.c:180: ack_packet_[0] = (ack_packet[0] & (~(1 << 0)))| (1 << 0);
      00433B 90 04 78         [24] 1656 	mov	dptr,#_ack_packet
      00433E E0               [24] 1657 	movx	a,@dptr
      00433F FF               [12] 1658 	mov	r7,a
      004340 74 FE            [12] 1659 	mov	a,#0xfe
      004342 5F               [12] 1660 	anl	a,r7
      004343 44 01            [12] 1661 	orl	a,#0x01
      004345 FF               [12] 1662 	mov	r7,a
      004346 90 02 B1         [24] 1663 	mov	dptr,#_transmit_ack_ack_packet__1_348
      004349 F0               [24] 1664 	movx	@dptr,a
      00434A                       1665 00102$:
                           00016A  1666 	C$main.c$182$1$348 ==.
                                   1667 ;	main.c:182: if(rcv_flg_id3 > 0)
      00434A 90 04 88         [24] 1668 	mov	dptr,#_rcv_flg_id3
      00434D E0               [24] 1669 	movx	a,@dptr
      00434E 60 0F            [24] 1670 	jz	00104$
                           000170  1671 	C$main.c$184$2$350 ==.
                                   1672 ;	main.c:184: ack_packet_[0] = (ack_packet[0] & (~(1 << 2)))| (1 << 2);
      004350 90 04 78         [24] 1673 	mov	dptr,#_ack_packet
      004353 E0               [24] 1674 	movx	a,@dptr
      004354 FF               [12] 1675 	mov	r7,a
      004355 74 FB            [12] 1676 	mov	a,#0xfb
      004357 5F               [12] 1677 	anl	a,r7
      004358 44 04            [12] 1678 	orl	a,#0x04
      00435A FF               [12] 1679 	mov	r7,a
      00435B 90 02 B1         [24] 1680 	mov	dptr,#_transmit_ack_ack_packet__1_348
      00435E F0               [24] 1681 	movx	@dptr,a
      00435F                       1682 00104$:
                           00017F  1683 	C$main.c$186$1$348 ==.
                                   1684 ;	main.c:186: axradio_transmit(&remoteaddr, ack_packet_, sizeof(ack_packet));
      00435F 75 1B B1         [24] 1685 	mov	_axradio_transmit_PARM_2,#_transmit_ack_ack_packet__1_348
      004362 75 1C 02         [24] 1686 	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_ack_ack_packet__1_348 >> 8)
      004365 75 1D 00         [24] 1687 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004368 75 1E 04         [24] 1688 	mov	_axradio_transmit_PARM_3,#0x04
      00436B 75 1F 00         [24] 1689 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      00436E 90 63 E6         [24] 1690 	mov	dptr,#_remoteaddr
      004371 75 F0 80         [24] 1691 	mov	b,#0x80
      004374 12 3A DA         [24] 1692 	lcall	_axradio_transmit
                           000197  1693 	C$main.c$189$1$348 ==.
                                   1694 ;	main.c:189: wait_dbglink_free();
      004377 12 0B 99         [24] 1695 	lcall	_wait_dbglink_free
                           00019A  1696 	C$main.c$190$1$348 ==.
                                   1697 ;	main.c:190: dbglink_writestr("ACK: ");
      00437A 90 65 75         [24] 1698 	mov	dptr,#___str_3
      00437D 75 F0 80         [24] 1699 	mov	b,#0x80
      004380 12 59 1E         [24] 1700 	lcall	_dbglink_writestr
                           0001A3  1701 	C$main.c$191$1$348 ==.
                                   1702 ;	main.c:191: dbglink_writehex16(rcv_flg, 2, WRNUM_SIGNED);
      004383 90 04 82         [24] 1703 	mov	dptr,#_rcv_flg
      004386 E0               [24] 1704 	movx	a,@dptr
      004387 FF               [12] 1705 	mov	r7,a
      004388 7E 00            [12] 1706 	mov	r6,#0x00
      00438A 74 01            [12] 1707 	mov	a,#0x01
      00438C C0 E0            [24] 1708 	push	acc
      00438E 04               [12] 1709 	inc	a
      00438F C0 E0            [24] 1710 	push	acc
      004391 8F 82            [24] 1711 	mov	dpl,r7
      004393 8E 83            [24] 1712 	mov	dph,r6
      004395 12 5D 1B         [24] 1713 	lcall	_dbglink_writehex16
      004398 15 81            [12] 1714 	dec	sp
      00439A 15 81            [12] 1715 	dec	sp
                           0001BC  1716 	C$main.c$192$1$348 ==.
                                   1717 ;	main.c:192: dbglink_writestr(" ");
      00439C 90 65 7B         [24] 1718 	mov	dptr,#___str_4
      00439F 75 F0 80         [24] 1719 	mov	b,#0x80
      0043A2 12 59 1E         [24] 1720 	lcall	_dbglink_writestr
                           0001C5  1721 	C$main.c$193$1$348 ==.
                                   1722 ;	main.c:193: dbglink_writehex16(pkts_received, 4, WRNUM_PADZERO);
      0043A5 90 04 7C         [24] 1723 	mov	dptr,#_pkts_received
      0043A8 E0               [24] 1724 	movx	a,@dptr
      0043A9 FE               [12] 1725 	mov	r6,a
      0043AA A3               [24] 1726 	inc	dptr
      0043AB E0               [24] 1727 	movx	a,@dptr
      0043AC FF               [12] 1728 	mov	r7,a
      0043AD 74 08            [12] 1729 	mov	a,#0x08
      0043AF C0 E0            [24] 1730 	push	acc
      0043B1 03               [12] 1731 	rr	a
      0043B2 C0 E0            [24] 1732 	push	acc
      0043B4 8E 82            [24] 1733 	mov	dpl,r6
      0043B6 8F 83            [24] 1734 	mov	dph,r7
      0043B8 12 5D 1B         [24] 1735 	lcall	_dbglink_writehex16
      0043BB 15 81            [12] 1736 	dec	sp
      0043BD 15 81            [12] 1737 	dec	sp
                           0001DF  1738 	C$main.c$194$1$348 ==.
                                   1739 ;	main.c:194: dbglink_tx('\n');
      0043BF 75 82 0A         [24] 1740 	mov	dpl,#0x0a
      0043C2 12 49 80         [24] 1741 	lcall	_dbglink_tx
                           0001E5  1742 	C$main.c$195$1$348 ==.
                                   1743 ;	main.c:195: dbglink_writestr("ID1: ");
      0043C5 90 65 7D         [24] 1744 	mov	dptr,#___str_5
      0043C8 75 F0 80         [24] 1745 	mov	b,#0x80
      0043CB 12 59 1E         [24] 1746 	lcall	_dbglink_writestr
                           0001EE  1747 	C$main.c$196$1$348 ==.
                                   1748 ;	main.c:196: dbglink_writehex16(rcv_flg_id1, 2, WRNUM_SIGNED);
      0043CE 90 04 87         [24] 1749 	mov	dptr,#_rcv_flg_id1
      0043D1 E0               [24] 1750 	movx	a,@dptr
      0043D2 FF               [12] 1751 	mov	r7,a
      0043D3 7E 00            [12] 1752 	mov	r6,#0x00
      0043D5 74 01            [12] 1753 	mov	a,#0x01
      0043D7 C0 E0            [24] 1754 	push	acc
      0043D9 04               [12] 1755 	inc	a
      0043DA C0 E0            [24] 1756 	push	acc
      0043DC 8F 82            [24] 1757 	mov	dpl,r7
      0043DE 8E 83            [24] 1758 	mov	dph,r6
      0043E0 12 5D 1B         [24] 1759 	lcall	_dbglink_writehex16
      0043E3 15 81            [12] 1760 	dec	sp
      0043E5 15 81            [12] 1761 	dec	sp
                           000207  1762 	C$main.c$197$1$348 ==.
                                   1763 ;	main.c:197: dbglink_writestr(" ");
      0043E7 90 65 7B         [24] 1764 	mov	dptr,#___str_4
      0043EA 75 F0 80         [24] 1765 	mov	b,#0x80
      0043ED 12 59 1E         [24] 1766 	lcall	_dbglink_writestr
                           000210  1767 	C$main.c$198$1$348 ==.
                                   1768 ;	main.c:198: dbglink_writehex16(pkts_id1, 4, WRNUM_PADZERO);
      0043F0 90 04 83         [24] 1769 	mov	dptr,#_pkts_id1
      0043F3 E0               [24] 1770 	movx	a,@dptr
      0043F4 FE               [12] 1771 	mov	r6,a
      0043F5 A3               [24] 1772 	inc	dptr
      0043F6 E0               [24] 1773 	movx	a,@dptr
      0043F7 FF               [12] 1774 	mov	r7,a
      0043F8 74 08            [12] 1775 	mov	a,#0x08
      0043FA C0 E0            [24] 1776 	push	acc
      0043FC 03               [12] 1777 	rr	a
      0043FD C0 E0            [24] 1778 	push	acc
      0043FF 8E 82            [24] 1779 	mov	dpl,r6
      004401 8F 83            [24] 1780 	mov	dph,r7
      004403 12 5D 1B         [24] 1781 	lcall	_dbglink_writehex16
      004406 15 81            [12] 1782 	dec	sp
      004408 15 81            [12] 1783 	dec	sp
                           00022A  1784 	C$main.c$199$1$348 ==.
                                   1785 ;	main.c:199: dbglink_writestr("\nID3: ");
      00440A 90 65 83         [24] 1786 	mov	dptr,#___str_6
      00440D 75 F0 80         [24] 1787 	mov	b,#0x80
      004410 12 59 1E         [24] 1788 	lcall	_dbglink_writestr
                           000233  1789 	C$main.c$201$1$348 ==.
                                   1790 ;	main.c:201: dbglink_writehex16(rcv_flg_id3, 2, WRNUM_SIGNED);
      004413 90 04 88         [24] 1791 	mov	dptr,#_rcv_flg_id3
      004416 E0               [24] 1792 	movx	a,@dptr
      004417 FF               [12] 1793 	mov	r7,a
      004418 7E 00            [12] 1794 	mov	r6,#0x00
      00441A 74 01            [12] 1795 	mov	a,#0x01
      00441C C0 E0            [24] 1796 	push	acc
      00441E 04               [12] 1797 	inc	a
      00441F C0 E0            [24] 1798 	push	acc
      004421 8F 82            [24] 1799 	mov	dpl,r7
      004423 8E 83            [24] 1800 	mov	dph,r6
      004425 12 5D 1B         [24] 1801 	lcall	_dbglink_writehex16
      004428 15 81            [12] 1802 	dec	sp
      00442A 15 81            [12] 1803 	dec	sp
                           00024C  1804 	C$main.c$202$1$348 ==.
                                   1805 ;	main.c:202: dbglink_writestr(" ");
      00442C 90 65 7B         [24] 1806 	mov	dptr,#___str_4
      00442F 75 F0 80         [24] 1807 	mov	b,#0x80
      004432 12 59 1E         [24] 1808 	lcall	_dbglink_writestr
                           000255  1809 	C$main.c$203$1$348 ==.
                                   1810 ;	main.c:203: dbglink_writehex16(pkts_id3, 4, WRNUM_PADZERO);
      004435 90 04 85         [24] 1811 	mov	dptr,#_pkts_id3
      004438 E0               [24] 1812 	movx	a,@dptr
      004439 FE               [12] 1813 	mov	r6,a
      00443A A3               [24] 1814 	inc	dptr
      00443B E0               [24] 1815 	movx	a,@dptr
      00443C FF               [12] 1816 	mov	r7,a
      00443D 74 08            [12] 1817 	mov	a,#0x08
      00443F C0 E0            [24] 1818 	push	acc
      004441 03               [12] 1819 	rr	a
      004442 C0 E0            [24] 1820 	push	acc
      004444 8E 82            [24] 1821 	mov	dpl,r6
      004446 8F 83            [24] 1822 	mov	dph,r7
      004448 12 5D 1B         [24] 1823 	lcall	_dbglink_writehex16
      00444B 15 81            [12] 1824 	dec	sp
      00444D 15 81            [12] 1825 	dec	sp
                           00026F  1826 	C$main.c$205$1$348 ==.
                                   1827 ;	main.c:205: dbglink_writestr("\n\n");
      00444F 90 65 8A         [24] 1828 	mov	dptr,#___str_7
      004452 75 F0 80         [24] 1829 	mov	b,#0x80
      004455 12 59 1E         [24] 1830 	lcall	_dbglink_writestr
                           000278  1831 	C$main.c$207$1$348 ==.
                           000278  1832 	XFmain$transmit_ack$0$0 ==.
      004458 22               [24] 1833 	ret
                                   1834 ;------------------------------------------------------------
                                   1835 ;Allocation info for local variables in function 'axradio_statuschange'
                                   1836 ;------------------------------------------------------------
                                   1837 ;st                        Allocated to registers r6 r7 
                                   1838 ;pktdata                   Allocated to registers 
                                   1839 ;------------------------------------------------------------
                           000279  1840 	G$axradio_statuschange$0$0 ==.
                           000279  1841 	C$main.c$237$1$348 ==.
                                   1842 ;	main.c:237: void axradio_statuschange(struct axradio_status __xdata *st)
                                   1843 ;	-----------------------------------------
                                   1844 ;	 function axradio_statuschange
                                   1845 ;	-----------------------------------------
      004459                       1846 _axradio_statuschange:
      004459 AE 82            [24] 1847 	mov	r6,dpl
      00445B AF 83            [24] 1848 	mov	r7,dph
                           00027D  1849 	C$main.c$240$1$352 ==.
                                   1850 ;	main.c:240: if (DBGLNKSTAT & 0x10)
      00445D E5 E2            [12] 1851 	mov	a,_DBGLNKSTAT
      00445F 30 E4 6A         [24] 1852 	jnb	acc.4,00102$
                           000282  1853 	C$main.c$242$2$353 ==.
                                   1854 ;	main.c:242: wait_dbglink_free();
      004462 C0 07            [24] 1855 	push	ar7
      004464 C0 06            [24] 1856 	push	ar6
      004466 12 0B 99         [24] 1857 	lcall	_wait_dbglink_free
                           000289  1858 	C$main.c$243$2$353 ==.
                                   1859 ;	main.c:243: dbglink_writestr("ST: 0x");
      004469 90 65 8D         [24] 1860 	mov	dptr,#___str_8
      00446C 75 F0 80         [24] 1861 	mov	b,#0x80
      00446F 12 59 1E         [24] 1862 	lcall	_dbglink_writestr
      004472 D0 06            [24] 1863 	pop	ar6
      004474 D0 07            [24] 1864 	pop	ar7
                           000296  1865 	C$main.c$244$2$353 ==.
                                   1866 ;	main.c:244: dbglink_writehex16(st->status, 2, WRNUM_PADZERO);
      004476 8E 82            [24] 1867 	mov	dpl,r6
      004478 8F 83            [24] 1868 	mov	dph,r7
      00447A E0               [24] 1869 	movx	a,@dptr
      00447B FD               [12] 1870 	mov	r5,a
      00447C 7C 00            [12] 1871 	mov	r4,#0x00
      00447E C0 07            [24] 1872 	push	ar7
      004480 C0 06            [24] 1873 	push	ar6
      004482 74 08            [12] 1874 	mov	a,#0x08
      004484 C0 E0            [24] 1875 	push	acc
      004486 74 02            [12] 1876 	mov	a,#0x02
      004488 C0 E0            [24] 1877 	push	acc
      00448A 8D 82            [24] 1878 	mov	dpl,r5
      00448C 8C 83            [24] 1879 	mov	dph,r4
      00448E 12 5D 1B         [24] 1880 	lcall	_dbglink_writehex16
      004491 15 81            [12] 1881 	dec	sp
      004493 15 81            [12] 1882 	dec	sp
                           0002B5  1883 	C$main.c$245$2$353 ==.
                                   1884 ;	main.c:245: dbglink_writestr(" ERR: 0x");
      004495 90 65 94         [24] 1885 	mov	dptr,#___str_9
      004498 75 F0 80         [24] 1886 	mov	b,#0x80
      00449B 12 59 1E         [24] 1887 	lcall	_dbglink_writestr
      00449E D0 06            [24] 1888 	pop	ar6
      0044A0 D0 07            [24] 1889 	pop	ar7
                           0002C2  1890 	C$main.c$246$2$353 ==.
                                   1891 ;	main.c:246: dbglink_writehex16(st->error, 2, WRNUM_PADZERO);
      0044A2 8E 82            [24] 1892 	mov	dpl,r6
      0044A4 8F 83            [24] 1893 	mov	dph,r7
      0044A6 A3               [24] 1894 	inc	dptr
      0044A7 E0               [24] 1895 	movx	a,@dptr
      0044A8 FD               [12] 1896 	mov	r5,a
      0044A9 7C 00            [12] 1897 	mov	r4,#0x00
      0044AB C0 07            [24] 1898 	push	ar7
      0044AD C0 06            [24] 1899 	push	ar6
      0044AF 74 08            [12] 1900 	mov	a,#0x08
      0044B1 C0 E0            [24] 1901 	push	acc
      0044B3 74 02            [12] 1902 	mov	a,#0x02
      0044B5 C0 E0            [24] 1903 	push	acc
      0044B7 8D 82            [24] 1904 	mov	dpl,r5
      0044B9 8C 83            [24] 1905 	mov	dph,r4
      0044BB 12 5D 1B         [24] 1906 	lcall	_dbglink_writehex16
      0044BE 15 81            [12] 1907 	dec	sp
      0044C0 15 81            [12] 1908 	dec	sp
      0044C2 D0 06            [24] 1909 	pop	ar6
      0044C4 D0 07            [24] 1910 	pop	ar7
                           0002E6  1911 	C$main.c$247$2$353 ==.
                                   1912 ;	main.c:247: dbglink_tx('\n');
      0044C6 75 82 0A         [24] 1913 	mov	dpl,#0x0a
      0044C9 12 49 80         [24] 1914 	lcall	_dbglink_tx
      0044CC                       1915 00102$:
                           0002EC  1916 	C$main.c$251$1$352 ==.
                                   1917 ;	main.c:251: switch (st->status)
      0044CC 8E 82            [24] 1918 	mov	dpl,r6
      0044CE 8F 83            [24] 1919 	mov	dph,r7
      0044D0 E0               [24] 1920 	movx	a,@dptr
      0044D1 FD               [12] 1921 	mov	r5,a
      0044D2 60 17            [24] 1922 	jz	00118$
      0044D4 BD 02 02         [24] 1923 	cjne	r5,#0x02,00145$
      0044D7 80 6E            [24] 1924 	sjmp	00123$
      0044D9                       1925 00145$:
      0044D9 BD 03 02         [24] 1926 	cjne	r5,#0x03,00146$
      0044DC 80 05            [24] 1927 	sjmp	00107$
      0044DE                       1928 00146$:
                           0002FE  1929 	C$main.c$254$2$354 ==.
                                   1930 ;	main.c:254: led0_on();
      0044DE BD 04 66         [24] 1931 	cjne	r5,#0x04,00123$
      0044E1 80 04            [24] 1932 	sjmp	00114$
      0044E3                       1933 00107$:
      0044E3 D2 89            [12] 1934 	setb	_PORTB_1
                           000305  1935 	C$main.c$255$2$354 ==.
                                   1936 ;	main.c:255: break;
                           000305  1937 	C$main.c$258$2$354 ==.
                                   1938 ;	main.c:258: led0_off();
      0044E5 80 60            [24] 1939 	sjmp	00123$
      0044E7                       1940 00114$:
      0044E7 C2 89            [12] 1941 	clr	_PORTB_1
                           000309  1942 	C$main.c$259$2$354 ==.
                                   1943 ;	main.c:259: break;
                           000309  1944 	C$main.c$264$2$354 ==.
                                   1945 ;	main.c:264: case AXRADIO_STAT_RECEIVE:
      0044E9 80 5C            [24] 1946 	sjmp	00123$
      0044EB                       1947 00118$:
                           00030B  1948 	C$main.c$269$3$359 ==.
                                   1949 ;	main.c:269: if (st->error == AXRADIO_ERR_NOERROR)
      0044EB 74 01            [12] 1950 	mov	a,#0x01
      0044ED 2E               [12] 1951 	add	a,r6
      0044EE FC               [12] 1952 	mov	r4,a
      0044EF E4               [12] 1953 	clr	a
      0044F0 3F               [12] 1954 	addc	a,r7
      0044F1 FD               [12] 1955 	mov	r5,a
      0044F2 8C 82            [24] 1956 	mov	dpl,r4
      0044F4 8D 83            [24] 1957 	mov	dph,r5
      0044F6 E0               [24] 1958 	movx	a,@dptr
      0044F7 70 4E            [24] 1959 	jnz	00123$
                           000319  1960 	C$main.c$272$4$360 ==.
                                   1961 ;	main.c:272: ++pkts_received;
      0044F9 90 04 7C         [24] 1962 	mov	dptr,#_pkts_received
      0044FC E0               [24] 1963 	movx	a,@dptr
      0044FD 24 01            [12] 1964 	add	a,#0x01
      0044FF F0               [24] 1965 	movx	@dptr,a
      004500 A3               [24] 1966 	inc	dptr
      004501 E0               [24] 1967 	movx	a,@dptr
      004502 34 00            [12] 1968 	addc	a,#0x00
      004504 F0               [24] 1969 	movx	@dptr,a
                           000325  1970 	C$main.c$273$4$360 ==.
                                   1971 ;	main.c:273: rcv_flg += 1;
      004505 90 04 82         [24] 1972 	mov	dptr,#_rcv_flg
      004508 E0               [24] 1973 	movx	a,@dptr
      004509 04               [12] 1974 	inc	a
      00450A F0               [24] 1975 	movx	@dptr,a
                           00032B  1976 	C$main.c$275$4$360 ==.
                                   1977 ;	main.c:275: dbglink_writestr("pkt T0 ");
      00450B 90 65 9D         [24] 1978 	mov	dptr,#___str_10
      00450E 75 F0 80         [24] 1979 	mov	b,#0x80
      004511 C0 07            [24] 1980 	push	ar7
      004513 C0 06            [24] 1981 	push	ar6
      004515 12 59 1E         [24] 1982 	lcall	_dbglink_writestr
                           000338  1983 	C$main.c$276$4$360 ==.
                                   1984 ;	main.c:276: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004518 12 62 70         [24] 1985 	lcall	_wtimer0_curtime
      00451B AA 82            [24] 1986 	mov	r2,dpl
      00451D AB 83            [24] 1987 	mov	r3,dph
      00451F AC F0            [24] 1988 	mov	r4,b
      004521 FD               [12] 1989 	mov	r5,a
      004522 74 08            [12] 1990 	mov	a,#0x08
      004524 C0 E0            [24] 1991 	push	acc
      004526 C0 E0            [24] 1992 	push	acc
      004528 8A 82            [24] 1993 	mov	dpl,r2
      00452A 8B 83            [24] 1994 	mov	dph,r3
      00452C 8C F0            [24] 1995 	mov	b,r4
      00452E ED               [12] 1996 	mov	a,r5
      00452F 12 5A FD         [24] 1997 	lcall	_dbglink_writehex32
      004532 15 81            [12] 1998 	dec	sp
      004534 15 81            [12] 1999 	dec	sp
      004536 D0 06            [24] 2000 	pop	ar6
      004538 D0 07            [24] 2001 	pop	ar7
                           00035A  2002 	C$main.c$277$4$360 ==.
                                   2003 ;	main.c:277: dbglink_tx('\n');
      00453A 75 82 0A         [24] 2004 	mov	dpl,#0x0a
      00453D 12 49 80         [24] 2005 	lcall	_dbglink_tx
                           000360  2006 	C$main.c$278$4$360 ==.
                                   2007 ;	main.c:278: dbglink_received_packet(st);
      004540 8E 82            [24] 2008 	mov	dpl,r6
      004542 8F 83            [24] 2009 	mov	dph,r7
      004544 12 0B AF         [24] 2010 	lcall	_dbglink_received_packet
                           000367  2011 	C$main.c$292$1$352 ==.
                                   2012 ;	main.c:292: }
      004547                       2013 00123$:
                           000367  2014 	C$main.c$293$1$352 ==.
                           000367  2015 	XG$axradio_statuschange$0$0 ==.
      004547 22               [24] 2016 	ret
                                   2017 ;------------------------------------------------------------
                                   2018 ;Allocation info for local variables in function 'enable_radio_interrupt_in_mcu_pin'
                                   2019 ;------------------------------------------------------------
                           000368  2020 	G$enable_radio_interrupt_in_mcu_pin$0$0 ==.
                           000368  2021 	C$main.c$295$1$352 ==.
                                   2022 ;	main.c:295: void enable_radio_interrupt_in_mcu_pin(void)
                                   2023 ;	-----------------------------------------
                                   2024 ;	 function enable_radio_interrupt_in_mcu_pin
                                   2025 ;	-----------------------------------------
      004548                       2026 _enable_radio_interrupt_in_mcu_pin:
                           000368  2027 	C$main.c$297$1$362 ==.
                                   2028 ;	main.c:297: IE_4 = 1;
      004548 D2 AC            [12] 2029 	setb	_IE_4
                           00036A  2030 	C$main.c$298$1$362 ==.
                           00036A  2031 	XG$enable_radio_interrupt_in_mcu_pin$0$0 ==.
      00454A 22               [24] 2032 	ret
                                   2033 ;------------------------------------------------------------
                                   2034 ;Allocation info for local variables in function 'disable_radio_interrupt_in_mcu_pin'
                                   2035 ;------------------------------------------------------------
                           00036B  2036 	G$disable_radio_interrupt_in_mcu_pin$0$0 ==.
                           00036B  2037 	C$main.c$300$1$362 ==.
                                   2038 ;	main.c:300: void disable_radio_interrupt_in_mcu_pin(void)
                                   2039 ;	-----------------------------------------
                                   2040 ;	 function disable_radio_interrupt_in_mcu_pin
                                   2041 ;	-----------------------------------------
      00454B                       2042 _disable_radio_interrupt_in_mcu_pin:
                           00036B  2043 	C$main.c$302$1$364 ==.
                                   2044 ;	main.c:302: IE_4 = 0;
      00454B C2 AC            [12] 2045 	clr	_IE_4
                           00036D  2046 	C$main.c$303$1$364 ==.
                           00036D  2047 	XG$disable_radio_interrupt_in_mcu_pin$0$0 ==.
      00454D 22               [24] 2048 	ret
                                   2049 ;------------------------------------------------------------
                                   2050 ;Allocation info for local variables in function 'wakeup_callback'
                                   2051 ;------------------------------------------------------------
                                   2052 ;desc                      Allocated to registers 
                                   2053 ;timer0Period              Allocated with name '_wakeup_callback_timer0Period_1_366'
                                   2054 ;bc_flg                    Allocated with name '_wakeup_callback_bc_flg_1_366'
                                   2055 ;------------------------------------------------------------
                           00036E  2056 	Fmain$wakeup_callback$0$0 ==.
                           00036E  2057 	C$main.c$305$1$364 ==.
                                   2058 ;	main.c:305: static void wakeup_callback(struct wtimer_desc __xdata *desc)
                                   2059 ;	-----------------------------------------
                                   2060 ;	 function wakeup_callback
                                   2061 ;	-----------------------------------------
      00454E                       2062 _wakeup_callback:
                           00036E  2063 	C$main.c$312$1$366 ==.
                                   2064 ;	main.c:312: dbglink_writestr("TX time: ");
      00454E 90 65 A5         [24] 2065 	mov	dptr,#___str_11
      004551 75 F0 80         [24] 2066 	mov	b,#0x80
      004554 12 59 1E         [24] 2067 	lcall	_dbglink_writestr
                           000377  2068 	C$main.c$313$1$366 ==.
                                   2069 ;	main.c:313: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004557 12 62 70         [24] 2070 	lcall	_wtimer0_curtime
      00455A AC 82            [24] 2071 	mov	r4,dpl
      00455C AD 83            [24] 2072 	mov	r5,dph
      00455E AE F0            [24] 2073 	mov	r6,b
      004560 FF               [12] 2074 	mov	r7,a
      004561 74 08            [12] 2075 	mov	a,#0x08
      004563 C0 E0            [24] 2076 	push	acc
      004565 C0 E0            [24] 2077 	push	acc
      004567 8C 82            [24] 2078 	mov	dpl,r4
      004569 8D 83            [24] 2079 	mov	dph,r5
      00456B 8E F0            [24] 2080 	mov	b,r6
      00456D EF               [12] 2081 	mov	a,r7
      00456E 12 5A FD         [24] 2082 	lcall	_dbglink_writehex32
      004571 15 81            [12] 2083 	dec	sp
      004573 15 81            [12] 2084 	dec	sp
                           000395  2085 	C$main.c$314$1$366 ==.
                                   2086 ;	main.c:314: dbglink_tx('\n');
      004575 75 82 0A         [24] 2087 	mov	dpl,#0x0a
      004578 12 49 80         [24] 2088 	lcall	_dbglink_tx
                           00039B  2089 	C$main.c$316$1$366 ==.
                                   2090 ;	main.c:316: if ((rcv_flg == 0)&&(bc_flg == 0))
      00457B 90 04 82         [24] 2091 	mov	dptr,#_rcv_flg
      00457E E0               [24] 2092 	movx	a,@dptr
      00457F FF               [12] 2093 	mov	r7,a
      004580 70 1B            [24] 2094 	jnz	00109$
      004582 90 02 B7         [24] 2095 	mov	dptr,#_wakeup_callback_bc_flg_1_366
      004585 E0               [24] 2096 	movx	a,@dptr
      004586 70 15            [24] 2097 	jnz	00109$
                           0003A8  2098 	C$main.c$318$2$367 ==.
                                   2099 ;	main.c:318: timer0Period = WTIMER0_UPLINKRCV;
      004588 90 02 B5         [24] 2100 	mov	dptr,#_wakeup_callback_timer0Period_1_366
      00458B 74 80            [12] 2101 	mov	a,#0x80
      00458D F0               [24] 2102 	movx	@dptr,a
      00458E 74 11            [12] 2103 	mov	a,#0x11
      004590 A3               [24] 2104 	inc	dptr
      004591 F0               [24] 2105 	movx	@dptr,a
                           0003B2  2106 	C$main.c$319$2$367 ==.
                                   2107 ;	main.c:319: transmit_beacon();
      004592 12 42 74         [24] 2108 	lcall	_transmit_beacon
                           0003B5  2109 	C$main.c$320$2$367 ==.
                                   2110 ;	main.c:320: bc_flg = 1;
      004595 90 02 B7         [24] 2111 	mov	dptr,#_wakeup_callback_bc_flg_1_366
      004598 74 01            [12] 2112 	mov	a,#0x01
      00459A F0               [24] 2113 	movx	@dptr,a
      00459B 80 44            [24] 2114 	sjmp	00110$
      00459D                       2115 00109$:
                           0003BD  2116 	C$main.c$322$1$366 ==.
                                   2117 ;	main.c:322: else if ((rcv_flg >=1)&&(bc_flg == 1)) // received data packets from COM-D terminals
      00459D BF 01 00         [24] 2118 	cjne	r7,#0x01,00134$
      0045A0                       2119 00134$:
      0045A0 40 25            [24] 2120 	jc	00105$
      0045A2 90 02 B7         [24] 2121 	mov	dptr,#_wakeup_callback_bc_flg_1_366
      0045A5 E0               [24] 2122 	movx	a,@dptr
      0045A6 FE               [12] 2123 	mov	r6,a
      0045A7 BE 01 1D         [24] 2124 	cjne	r6,#0x01,00105$
                           0003CA  2125 	C$main.c$324$2$368 ==.
                                   2126 ;	main.c:324: timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
      0045AA 90 02 B5         [24] 2127 	mov	dptr,#_wakeup_callback_timer0Period_1_366
      0045AD 74 80            [12] 2128 	mov	a,#0x80
      0045AF F0               [24] 2129 	movx	@dptr,a
      0045B0 74 07            [12] 2130 	mov	a,#0x07
      0045B2 A3               [24] 2131 	inc	dptr
      0045B3 F0               [24] 2132 	movx	@dptr,a
                           0003D4  2133 	C$main.c$326$2$368 ==.
                                   2134 ;	main.c:326: rcv_flg = 0;
      0045B4 90 04 82         [24] 2135 	mov	dptr,#_rcv_flg
      0045B7 E4               [12] 2136 	clr	a
      0045B8 F0               [24] 2137 	movx	@dptr,a
                           0003D9  2138 	C$main.c$327$2$368 ==.
                                   2139 ;	main.c:327: bc_flg = 0;
      0045B9 90 02 B7         [24] 2140 	mov	dptr,#_wakeup_callback_bc_flg_1_366
      0045BC F0               [24] 2141 	movx	@dptr,a
                           0003DD  2142 	C$main.c$328$2$368 ==.
                                   2143 ;	main.c:328: rcv_flg_id1 = 0;
      0045BD 90 04 87         [24] 2144 	mov	dptr,#_rcv_flg_id1
      0045C0 F0               [24] 2145 	movx	@dptr,a
                           0003E1  2146 	C$main.c$329$2$368 ==.
                                   2147 ;	main.c:329: rcv_flg_id3 = 0;
      0045C1 90 04 88         [24] 2148 	mov	dptr,#_rcv_flg_id3
      0045C4 F0               [24] 2149 	movx	@dptr,a
      0045C5 80 1A            [24] 2150 	sjmp	00110$
      0045C7                       2151 00105$:
                           0003E7  2152 	C$main.c$331$1$366 ==.
                                   2153 ;	main.c:331: else if ((rcv_flg == 0)&&(bc_flg == 1))
      0045C7 EF               [12] 2154 	mov	a,r7
      0045C8 70 17            [24] 2155 	jnz	00110$
      0045CA 90 02 B7         [24] 2156 	mov	dptr,#_wakeup_callback_bc_flg_1_366
      0045CD E0               [24] 2157 	movx	a,@dptr
      0045CE FF               [12] 2158 	mov	r7,a
      0045CF BF 01 0F         [24] 2159 	cjne	r7,#0x01,00110$
                           0003F2  2160 	C$main.c$333$2$369 ==.
                                   2161 ;	main.c:333: timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
      0045D2 90 02 B5         [24] 2162 	mov	dptr,#_wakeup_callback_timer0Period_1_366
      0045D5 74 80            [12] 2163 	mov	a,#0x80
      0045D7 F0               [24] 2164 	movx	@dptr,a
      0045D8 74 07            [12] 2165 	mov	a,#0x07
      0045DA A3               [24] 2166 	inc	dptr
      0045DB F0               [24] 2167 	movx	@dptr,a
                           0003FC  2168 	C$main.c$334$2$369 ==.
                                   2169 ;	main.c:334: bc_flg = 0;
      0045DC 90 02 B7         [24] 2170 	mov	dptr,#_wakeup_callback_bc_flg_1_366
      0045DF E4               [12] 2171 	clr	a
      0045E0 F0               [24] 2172 	movx	@dptr,a
      0045E1                       2173 00110$:
                           000401  2174 	C$main.c$336$1$366 ==.
                                   2175 ;	main.c:336: wakeup_desc.time += wtimer0_correctinterval(timer0Period);
      0045E1 90 02 A4         [24] 2176 	mov	dptr,#(_wakeup_desc + 0x0004)
      0045E4 E0               [24] 2177 	movx	a,@dptr
      0045E5 FC               [12] 2178 	mov	r4,a
      0045E6 A3               [24] 2179 	inc	dptr
      0045E7 E0               [24] 2180 	movx	a,@dptr
      0045E8 FD               [12] 2181 	mov	r5,a
      0045E9 A3               [24] 2182 	inc	dptr
      0045EA E0               [24] 2183 	movx	a,@dptr
      0045EB FE               [12] 2184 	mov	r6,a
      0045EC A3               [24] 2185 	inc	dptr
      0045ED E0               [24] 2186 	movx	a,@dptr
      0045EE FF               [12] 2187 	mov	r7,a
      0045EF 90 02 B5         [24] 2188 	mov	dptr,#_wakeup_callback_timer0Period_1_366
      0045F2 E0               [24] 2189 	movx	a,@dptr
      0045F3 FA               [12] 2190 	mov	r2,a
      0045F4 A3               [24] 2191 	inc	dptr
      0045F5 E0               [24] 2192 	movx	a,@dptr
      0045F6 FB               [12] 2193 	mov	r3,a
      0045F7 8A 00            [24] 2194 	mov	ar0,r2
      0045F9 8B 01            [24] 2195 	mov	ar1,r3
      0045FB 7A 00            [12] 2196 	mov	r2,#0x00
      0045FD 7B 00            [12] 2197 	mov	r3,#0x00
      0045FF 88 82            [24] 2198 	mov	dpl,r0
      004601 89 83            [24] 2199 	mov	dph,r1
      004603 8A F0            [24] 2200 	mov	b,r2
      004605 EB               [12] 2201 	mov	a,r3
      004606 C0 07            [24] 2202 	push	ar7
      004608 C0 06            [24] 2203 	push	ar6
      00460A C0 05            [24] 2204 	push	ar5
      00460C C0 04            [24] 2205 	push	ar4
      00460E 12 56 9B         [24] 2206 	lcall	_wtimer0_correctinterval
      004611 A8 82            [24] 2207 	mov	r0,dpl
      004613 A9 83            [24] 2208 	mov	r1,dph
      004615 AA F0            [24] 2209 	mov	r2,b
      004617 FB               [12] 2210 	mov	r3,a
      004618 D0 04            [24] 2211 	pop	ar4
      00461A D0 05            [24] 2212 	pop	ar5
      00461C D0 06            [24] 2213 	pop	ar6
      00461E D0 07            [24] 2214 	pop	ar7
      004620 E8               [12] 2215 	mov	a,r0
      004621 2C               [12] 2216 	add	a,r4
      004622 FC               [12] 2217 	mov	r4,a
      004623 E9               [12] 2218 	mov	a,r1
      004624 3D               [12] 2219 	addc	a,r5
      004625 FD               [12] 2220 	mov	r5,a
      004626 EA               [12] 2221 	mov	a,r2
      004627 3E               [12] 2222 	addc	a,r6
      004628 FE               [12] 2223 	mov	r6,a
      004629 EB               [12] 2224 	mov	a,r3
      00462A 3F               [12] 2225 	addc	a,r7
      00462B FF               [12] 2226 	mov	r7,a
      00462C 90 02 A4         [24] 2227 	mov	dptr,#(_wakeup_desc + 0x0004)
      00462F EC               [12] 2228 	mov	a,r4
      004630 F0               [24] 2229 	movx	@dptr,a
      004631 ED               [12] 2230 	mov	a,r5
      004632 A3               [24] 2231 	inc	dptr
      004633 F0               [24] 2232 	movx	@dptr,a
      004634 EE               [12] 2233 	mov	a,r6
      004635 A3               [24] 2234 	inc	dptr
      004636 F0               [24] 2235 	movx	@dptr,a
      004637 EF               [12] 2236 	mov	a,r7
      004638 A3               [24] 2237 	inc	dptr
      004639 F0               [24] 2238 	movx	@dptr,a
                           00045A  2239 	C$main.c$337$1$366 ==.
                                   2240 ;	main.c:337: wtimer0_addabsolute(&wakeup_desc);
      00463A 90 02 A0         [24] 2241 	mov	dptr,#_wakeup_desc
      00463D 12 52 40         [24] 2242 	lcall	_wtimer0_addabsolute
                           000460  2243 	C$main.c$338$1$366 ==.
                           000460  2244 	XFmain$wakeup_callback$0$0 ==.
      004640 22               [24] 2245 	ret
                                   2246 ;------------------------------------------------------------
                                   2247 ;Allocation info for local variables in function '_sdcc_external_startup'
                                   2248 ;------------------------------------------------------------
                                   2249 ;c                         Allocated to registers 
                                   2250 ;p                         Allocated to registers 
                                   2251 ;c                         Allocated to registers 
                                   2252 ;p                         Allocated to registers 
                                   2253 ;------------------------------------------------------------
                           000461  2254 	G$_sdcc_external_startup$0$0 ==.
                           000461  2255 	C$main.c$340$1$366 ==.
                                   2256 ;	main.c:340: uint8_t _sdcc_external_startup(void)
                                   2257 ;	-----------------------------------------
                                   2258 ;	 function _sdcc_external_startup
                                   2259 ;	-----------------------------------------
      004641                       2260 __sdcc_external_startup:
                           000461  2261 	C$main.c$342$1$371 ==.
                                   2262 ;	main.c:342: LPXOSCGM = 0x8A;
      004641 90 70 54         [24] 2263 	mov	dptr,#_LPXOSCGM
      004644 74 8A            [12] 2264 	mov	a,#0x8a
      004646 F0               [24] 2265 	movx	@dptr,a
                           000467  2266 	C$main.c$343$2$372 ==.
                                   2267 ;	main.c:343: wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
      004647 75 82 09         [24] 2268 	mov	dpl,#0x09
      00464A 12 4C CF         [24] 2269 	lcall	_wtimer0_setconfig
                           00046D  2270 	C$main.c$344$2$373 ==.
                                   2271 ;	main.c:344: wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
      00464D 75 82 38         [24] 2272 	mov	dpl,#0x38
      004650 12 4C E9         [24] 2273 	lcall	_wtimer1_setconfig
                           000473  2274 	C$main.c$346$1$371 ==.
                                   2275 ;	main.c:346: LPOSCCONFIG = 0x09; /* Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() ) */
      004653 90 70 60         [24] 2276 	mov	dptr,#_LPOSCCONFIG
      004656 74 09            [12] 2277 	mov	a,#0x09
      004658 F0               [24] 2278 	movx	@dptr,a
                           000479  2279 	C$main.c$348$1$371 ==.
                                   2280 ;	main.c:348: coldstart = !(PCON & 0x40);
      004659 E5 87            [12] 2281 	mov	a,_PCON
      00465B A2 E6            [12] 2282 	mov	c,acc[6]
      00465D B3               [12] 2283 	cpl	c
      00465E 92 01            [24] 2284 	mov	__sdcc_external_startup_sloc0_1_0,c
      004660 E4               [12] 2285 	clr	a
      004661 33               [12] 2286 	rlc	a
      004662 F5 24            [12] 2287 	mov	_coldstart,a
                           000484  2288 	C$main.c$350$1$371 ==.
                                   2289 ;	main.c:350: ANALOGA = 0x18; /* PA[3,4] LPXOSC, other PA are used as digital pins */
      004664 90 70 07         [24] 2290 	mov	dptr,#_ANALOGA
      004667 74 18            [12] 2291 	mov	a,#0x18
      004669 F0               [24] 2292 	movx	@dptr,a
                           00048A  2293 	C$main.c$351$1$371 ==.
                                   2294 ;	main.c:351: PORTA = 0xE7; /* pull ups except for LPXOSC pin PA[3,4]; */
      00466A 75 80 E7         [24] 2295 	mov	_PORTA,#0xe7
                           00048D  2296 	C$main.c$352$1$371 ==.
                                   2297 ;	main.c:352: PORTB = 0xFD | (PINB & 0x02); /* init LEDs to previous (frozen) state */
      00466D 74 02            [12] 2298 	mov	a,#0x02
      00466F 55 E8            [12] 2299 	anl	a,_PINB
      004671 44 FD            [12] 2300 	orl	a,#0xfd
      004673 F5 88            [12] 2301 	mov	_PORTB,a
                           000495  2302 	C$main.c$353$1$371 ==.
                                   2303 ;	main.c:353: PORTC = 0xFF;
      004675 75 90 FF         [24] 2304 	mov	_PORTC,#0xff
                           000498  2305 	C$main.c$354$1$371 ==.
                                   2306 ;	main.c:354: PORTR = 0x0B;
      004678 75 8C 0B         [24] 2307 	mov	_PORTR,#0x0b
                           00049B  2308 	C$main.c$356$1$371 ==.
                                   2309 ;	main.c:356: DIRA = 0x00;
      00467B 75 89 00         [24] 2310 	mov	_DIRA,#0x00
                           00049E  2311 	C$main.c$357$1$371 ==.
                                   2312 ;	main.c:357: DIRB = 0x0e; /*  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used) */
      00467E 75 8A 0E         [24] 2313 	mov	_DIRB,#0x0e
                           0004A1  2314 	C$main.c$358$1$371 ==.
                                   2315 ;	main.c:358: DIRC = 0x00; /*  PC4 = button */
      004681 75 8B 00         [24] 2316 	mov	_DIRC,#0x00
                           0004A4  2317 	C$main.c$359$1$371 ==.
                                   2318 ;	main.c:359: DIRR = 0x15;
      004684 75 8E 15         [24] 2319 	mov	_DIRR,#0x15
                           0004A7  2320 	C$main.c$360$1$371 ==.
                                   2321 ;	main.c:360: axradio_setup_pincfg1();
      004687 12 06 E3         [24] 2322 	lcall	_axradio_setup_pincfg1
                           0004AA  2323 	C$main.c$361$1$371 ==.
                                   2324 ;	main.c:361: DPS = 0;
      00468A 75 86 00         [24] 2325 	mov	_DPS,#0x00
                           0004AD  2326 	C$main.c$362$1$371 ==.
                                   2327 ;	main.c:362: IE = 0x40;
      00468D 75 A8 40         [24] 2328 	mov	_IE,#0x40
                           0004B0  2329 	C$main.c$363$1$371 ==.
                                   2330 ;	main.c:363: EIE = 0x00;
      004690 75 98 00         [24] 2331 	mov	_EIE,#0x00
                           0004B3  2332 	C$main.c$364$1$371 ==.
                                   2333 ;	main.c:364: E2IE = 0x00;
      004693 75 A0 00         [24] 2334 	mov	_E2IE,#0x00
                           0004B6  2335 	C$main.c$366$1$371 ==.
                                   2336 ;	main.c:366: display_portinit();
      004696 12 0D CB         [24] 2337 	lcall	_com0_portinit
                           0004B9  2338 	C$main.c$367$1$371 ==.
                                   2339 ;	main.c:367: GPIOENABLE = 1; /* unfreeze GPIO */
      004699 90 70 0C         [24] 2340 	mov	dptr,#_GPIOENABLE
      00469C 74 01            [12] 2341 	mov	a,#0x01
      00469E F0               [24] 2342 	movx	@dptr,a
                           0004BF  2343 	C$main.c$368$1$371 ==.
                                   2344 ;	main.c:368: return !coldstart; /* coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization */
      00469F E5 24            [12] 2345 	mov	a,_coldstart
      0046A1 B4 01 00         [24] 2346 	cjne	a,#0x01,00109$
      0046A4                       2347 00109$:
      0046A4 92 01            [24] 2348 	mov  __sdcc_external_startup_sloc0_1_0,c
      0046A6 E4               [12] 2349 	clr	a
      0046A7 33               [12] 2350 	rlc	a
      0046A8 F5 82            [12] 2351 	mov	dpl,a
                           0004CA  2352 	C$main.c$369$1$371 ==.
                           0004CA  2353 	XG$_sdcc_external_startup$0$0 ==.
      0046AA 22               [24] 2354 	ret
                                   2355 ;------------------------------------------------------------
                                   2356 ;Allocation info for local variables in function 'main'
                                   2357 ;------------------------------------------------------------
                                   2358 ;saved_button_state        Allocated with name '_main_saved_button_state_1_375'
                                   2359 ;i                         Allocated to registers r7 
                                   2360 ;x                         Allocated to registers r6 
                                   2361 ;flg                       Allocated to registers r6 
                                   2362 ;flg                       Allocated to registers r7 
                                   2363 ;------------------------------------------------------------
                           0004CB  2364 	G$main$0$0 ==.
                           0004CB  2365 	C$main.c$371$1$371 ==.
                                   2366 ;	main.c:371: int main(void)
                                   2367 ;	-----------------------------------------
                                   2368 ;	 function main
                                   2369 ;	-----------------------------------------
      0046AB                       2370 _main:
                           0004CB  2371 	C$main.c$378$1$375 ==.
                                   2372 ;	main.c:378: __endasm;
                           000000  2373 	G$_start__stack$0$0	= __start__stack
                                   2374 	.globl	G$_start__stack$0$0
                           0004CB  2375 	C$main.c$380$1$375 ==.
                                   2376 ;	main.c:380: dbglink_init();
      0046AB 12 52 2D         [24] 2377 	lcall	_dbglink_init
                           0004CE  2378 	C$libmftypes.h$368$4$391 ==.
                                   2379 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
      0046AE D2 AF            [12] 2380 	setb	_EA
                           0004D0  2381 	C$main.c$383$1$375 ==.
                                   2382 ;	main.c:383: flash_apply_calibration();
      0046B0 12 55 06         [24] 2383 	lcall	_flash_apply_calibration
                           0004D3  2384 	C$main.c$384$1$375 ==.
                                   2385 ;	main.c:384: CLKCON = 0x00;
      0046B3 75 C6 00         [24] 2386 	mov	_CLKCON,#0x00
                           0004D6  2387 	C$main.c$385$1$375 ==.
                                   2388 ;	main.c:385: wtimer_init();
      0046B6 12 4D 98         [24] 2389 	lcall	_wtimer_init
                           0004D9  2390 	C$main.c$387$1$375 ==.
                                   2391 ;	main.c:387: if (coldstart)
      0046B9 E5 24            [12] 2392 	mov	a,_coldstart
      0046BB 70 03            [24] 2393 	jnz	00193$
      0046BD 02 47 AD         [24] 2394 	ljmp	00122$
      0046C0                       2395 00193$:
                           0004E0  2396 	C$main.c$389$4$378 ==.
                                   2397 ;	main.c:389: led0_off();
      0046C0 C2 89            [12] 2398 	clr	_PORTB_1
                           0004E2  2399 	C$main.c$391$2$376 ==.
                                   2400 ;	main.c:391: wakeup_desc.handler = wakeup_callback;
      0046C2 90 02 A2         [24] 2401 	mov	dptr,#(_wakeup_desc + 0x0002)
      0046C5 74 4E            [12] 2402 	mov	a,#_wakeup_callback
      0046C7 F0               [24] 2403 	movx	@dptr,a
      0046C8 74 45            [12] 2404 	mov	a,#(_wakeup_callback >> 8)
      0046CA A3               [24] 2405 	inc	dptr
      0046CB F0               [24] 2406 	movx	@dptr,a
                           0004EC  2407 	C$main.c$393$2$376 ==.
                                   2408 ;	main.c:393: display_init();
      0046CC 12 0E 19         [24] 2409 	lcall	_com0_init
                           0004EF  2410 	C$main.c$394$2$376 ==.
                                   2411 ;	main.c:394: display_setpos(0);
      0046CF 75 82 00         [24] 2412 	mov	dpl,#0x00
      0046D2 12 0E C1         [24] 2413 	lcall	_com0_setpos
                           0004F5  2414 	C$main.c$395$2$376 ==.
                                   2415 ;	main.c:395: i = axradio_init();
      0046D5 12 2F AB         [24] 2416 	lcall	_axradio_init
                           0004F8  2417 	C$main.c$397$2$376 ==.
                                   2418 ;	main.c:397: if (i != AXRADIO_ERR_NOERROR)
      0046D8 E5 82            [12] 2419 	mov	a,dpl
      0046DA FF               [12] 2420 	mov	r7,a
      0046DB 60 25            [24] 2421 	jz	00112$
                           0004FD  2422 	C$main.c$399$3$379 ==.
                                   2423 ;	main.c:399: if (i == AXRADIO_ERR_NOCHIP)
      0046DD BF 05 02         [24] 2424 	cjne	r7,#0x05,00195$
      0046E0 80 03            [24] 2425 	sjmp	00196$
      0046E2                       2426 00195$:
      0046E2 02 47 E2         [24] 2427 	ljmp	00129$
      0046E5                       2428 00196$:
                           000505  2429 	C$main.c$401$4$380 ==.
                                   2430 ;	main.c:401: display_writestr(radio_not_found_lcd_display);
      0046E5 90 04 5C         [24] 2431 	mov	dptr,#_radio_not_found_lcd_display
      0046E8 75 F0 00         [24] 2432 	mov	b,#0x00
      0046EB 12 0E DD         [24] 2433 	lcall	_com0_writestr
                           00050E  2434 	C$main.c$404$4$380 ==.
                                   2435 ;	main.c:404: if(DBGLNKSTAT & 0x10)
      0046EE E5 E2            [12] 2436 	mov	a,_DBGLNKSTAT
      0046F0 20 E4 03         [24] 2437 	jb	acc.4,00197$
      0046F3 02 47 F0         [24] 2438 	ljmp	00141$
      0046F6                       2439 00197$:
                           000516  2440 	C$main.c$405$4$380 ==.
                                   2441 ;	main.c:405: dbglink_writestr(radio_not_found_lcd_display);
      0046F6 90 04 5C         [24] 2442 	mov	dptr,#_radio_not_found_lcd_display
      0046F9 75 F0 00         [24] 2443 	mov	b,#0x00
      0046FC 12 59 1E         [24] 2444 	lcall	_dbglink_writestr
                           00051F  2445 	C$main.c$408$4$380 ==.
                                   2446 ;	main.c:408: goto terminate_error;
      0046FF 02 47 F0         [24] 2447 	ljmp	00141$
                           000522  2448 	C$main.c$411$2$376 ==.
                                   2449 ;	main.c:411: goto terminate_radio_error;
      004702                       2450 00112$:
                           000522  2451 	C$main.c$414$2$376 ==.
                                   2452 ;	main.c:414: display_writestr(radio_lcd_display);
      004702 90 04 4E         [24] 2453 	mov	dptr,#_radio_lcd_display
      004705 75 F0 00         [24] 2454 	mov	b,#0x00
      004708 12 0E DD         [24] 2455 	lcall	_com0_writestr
                           00052B  2456 	C$main.c$417$2$376 ==.
                                   2457 ;	main.c:417: if (DBGLNKSTAT & 0x10)
      00470B E5 E2            [12] 2458 	mov	a,_DBGLNKSTAT
      00470D 30 E4 09         [24] 2459 	jnb	acc.4,00114$
                           000530  2460 	C$main.c$418$2$376 ==.
                                   2461 ;	main.c:418: dbglink_writestr(radio_lcd_display);
      004710 90 04 4E         [24] 2462 	mov	dptr,#_radio_lcd_display
      004713 75 F0 00         [24] 2463 	mov	b,#0x00
      004716 12 59 1E         [24] 2464 	lcall	_dbglink_writestr
      004719                       2465 00114$:
                           000539  2466 	C$main.c$421$2$376 ==.
                                   2467 ;	main.c:421: axradio_set_local_address(&localaddr);
      004719 90 63 EB         [24] 2468 	mov	dptr,#_localaddr
      00471C 75 F0 80         [24] 2469 	mov	b,#0x80
      00471F 12 3A 61         [24] 2470 	lcall	_axradio_set_local_address
                           000542  2471 	C$main.c$422$2$376 ==.
                                   2472 ;	main.c:422: axradio_set_default_remote_address(&remoteaddr);
      004722 90 63 E6         [24] 2473 	mov	dptr,#_remoteaddr
      004725 75 F0 80         [24] 2474 	mov	b,#0x80
      004728 12 3A 9F         [24] 2475 	lcall	_axradio_set_default_remote_address
                           00054B  2476 	C$main.c$426$2$376 ==.
                                   2477 ;	main.c:426: if (DBGLNKSTAT & 0x10)
      00472B E5 E2            [12] 2478 	mov	a,_DBGLNKSTAT
      00472D 30 E4 4C         [24] 2479 	jnb	acc.4,00118$
                           000550  2480 	C$main.c$428$3$381 ==.
                                   2481 ;	main.c:428: dbglink_writestr("RNG = ");
      004730 90 65 AF         [24] 2482 	mov	dptr,#___str_12
      004733 75 F0 80         [24] 2483 	mov	b,#0x80
      004736 12 59 1E         [24] 2484 	lcall	_dbglink_writestr
                           000559  2485 	C$main.c$429$3$381 ==.
                                   2486 ;	main.c:429: dbglink_writenum16(axradio_get_pllrange(), 2, 0);
      004739 12 38 F3         [24] 2487 	lcall	_axradio_get_pllrange
      00473C E4               [12] 2488 	clr	a
      00473D C0 E0            [24] 2489 	push	acc
      00473F 74 02            [12] 2490 	mov	a,#0x02
      004741 C0 E0            [24] 2491 	push	acc
      004743 12 61 74         [24] 2492 	lcall	_dbglink_writenum16
      004746 15 81            [12] 2493 	dec	sp
      004748 15 81            [12] 2494 	dec	sp
                           00056A  2495 	C$main.c$431$4$382 ==.
                                   2496 ;	main.c:431: uint8_t x = axradio_get_pllvcoi();
      00474A 12 39 12         [24] 2497 	lcall	_axradio_get_pllvcoi
                           00056D  2498 	C$main.c$433$4$382 ==.
                                   2499 ;	main.c:433: if (x & 0x80)
      00474D E5 82            [12] 2500 	mov	a,dpl
      00474F FE               [12] 2501 	mov	r6,a
      004750 30 E7 20         [24] 2502 	jnb	acc.7,00116$
                           000573  2503 	C$main.c$435$5$383 ==.
                                   2504 ;	main.c:435: dbglink_writestr("\nVCOI = ");
      004753 90 65 B6         [24] 2505 	mov	dptr,#___str_13
      004756 75 F0 80         [24] 2506 	mov	b,#0x80
      004759 C0 06            [24] 2507 	push	ar6
      00475B 12 59 1E         [24] 2508 	lcall	_dbglink_writestr
      00475E D0 06            [24] 2509 	pop	ar6
                           000580  2510 	C$main.c$436$5$383 ==.
                                   2511 ;	main.c:436: dbglink_writehex16(x, 2, 0);
      004760 E4               [12] 2512 	clr	a
      004761 FD               [12] 2513 	mov	r5,a
      004762 C0 E0            [24] 2514 	push	acc
      004764 74 02            [12] 2515 	mov	a,#0x02
      004766 C0 E0            [24] 2516 	push	acc
      004768 8E 82            [24] 2517 	mov	dpl,r6
      00476A 8D 83            [24] 2518 	mov	dph,r5
      00476C 12 5D 1B         [24] 2519 	lcall	_dbglink_writehex16
      00476F 15 81            [12] 2520 	dec	sp
      004771 15 81            [12] 2521 	dec	sp
      004773                       2522 00116$:
                           000593  2523 	C$main.c$439$3$381 ==.
                                   2524 ;	main.c:439: dbglink_writestr("\n\nMASTER\n");
      004773 90 65 BF         [24] 2525 	mov	dptr,#___str_14
      004776 75 F0 80         [24] 2526 	mov	b,#0x80
      004779 12 59 1E         [24] 2527 	lcall	_dbglink_writestr
      00477C                       2528 00118$:
                           00059C  2529 	C$main.c$443$2$376 ==.
                                   2530 ;	main.c:443: i = axradio_set_mode(AXRADIO_MODE_ASYNC_RECEIVE);
      00477C 75 82 20         [24] 2531 	mov	dpl,#0x20
      00477F 12 33 AF         [24] 2532 	lcall	_axradio_set_mode
                           0005A2  2533 	C$main.c$445$2$376 ==.
                                   2534 ;	main.c:445: if (i != AXRADIO_ERR_NOERROR)
      004782 E5 82            [12] 2535 	mov	a,dpl
      004784 FF               [12] 2536 	mov	r7,a
      004785 70 5B            [24] 2537 	jnz	00129$
                           0005A7  2538 	C$main.c$449$2$376 ==.
                                   2539 ;	main.c:449: wakeup_desc.time = wtimer0_correctinterval(WTIMER0_PERIOD);
      004787 90 19 00         [24] 2540 	mov	dptr,#0x1900
      00478A E4               [12] 2541 	clr	a
      00478B F5 F0            [12] 2542 	mov	b,a
      00478D 12 56 9B         [24] 2543 	lcall	_wtimer0_correctinterval
      004790 AB 82            [24] 2544 	mov	r3,dpl
      004792 AC 83            [24] 2545 	mov	r4,dph
      004794 AD F0            [24] 2546 	mov	r5,b
      004796 FE               [12] 2547 	mov	r6,a
      004797 90 02 A4         [24] 2548 	mov	dptr,#(_wakeup_desc + 0x0004)
      00479A EB               [12] 2549 	mov	a,r3
      00479B F0               [24] 2550 	movx	@dptr,a
      00479C EC               [12] 2551 	mov	a,r4
      00479D A3               [24] 2552 	inc	dptr
      00479E F0               [24] 2553 	movx	@dptr,a
      00479F ED               [12] 2554 	mov	a,r5
      0047A0 A3               [24] 2555 	inc	dptr
      0047A1 F0               [24] 2556 	movx	@dptr,a
      0047A2 EE               [12] 2557 	mov	a,r6
      0047A3 A3               [24] 2558 	inc	dptr
      0047A4 F0               [24] 2559 	movx	@dptr,a
                           0005C5  2560 	C$main.c$450$2$376 ==.
                                   2561 ;	main.c:450: wtimer0_addrelative(&wakeup_desc);
      0047A5 90 02 A0         [24] 2562 	mov	dptr,#_wakeup_desc
      0047A8 12 50 5F         [24] 2563 	lcall	_wtimer0_addrelative
      0047AB 80 05            [24] 2564 	sjmp	00123$
      0047AD                       2565 00122$:
                           0005CD  2566 	C$main.c$456$2$384 ==.
                                   2567 ;	main.c:456: axradio_commsleepexit();
      0047AD 12 3F 62         [24] 2568 	lcall	_axradio_commsleepexit
                           0005D0  2569 	C$main.c$457$2$384 ==.
                                   2570 ;	main.c:457: IE_4 = 1; /* enable radio interrupt */
      0047B0 D2 AC            [12] 2571 	setb	_IE_4
      0047B2                       2572 00123$:
                           0005D2  2573 	C$main.c$460$1$375 ==.
                                   2574 ;	main.c:460: axradio_setup_pincfg2();
      0047B2 12 06 EA         [24] 2575 	lcall	_axradio_setup_pincfg2
      0047B5                       2576 00139$:
                           0005D5  2577 	C$main.c$464$2$385 ==.
                                   2578 ;	main.c:464: wtimer_runcallbacks();
      0047B5 12 4F 50         [24] 2579 	lcall	_wtimer_runcallbacks
                           0005D8  2580 	C$libmftypes.h$373$5$394 ==.
                                   2581 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:373: EA = 0;
      0047B8 C2 AF            [12] 2582 	clr	_EA
                           0005DA  2583 	C$main.c$467$3$385 ==.
                                   2584 ;	main.c:467: uint8_t flg = WTFLAG_CANSTANDBY;
      0047BA 7E 02            [12] 2585 	mov	r6,#0x02
                           0005DC  2586 	C$main.c$470$3$386 ==.
                                   2587 ;	main.c:470: if (axradio_cansleep()
      0047BC C0 06            [24] 2588 	push	ar6
      0047BE 12 33 9D         [24] 2589 	lcall	_axradio_cansleep
      0047C1 E5 82            [12] 2590 	mov	a,dpl
      0047C3 D0 06            [24] 2591 	pop	ar6
      0047C5 60 10            [24] 2592 	jz	00125$
                           0005E7  2593 	C$main.c$472$3$386 ==.
                                   2594 ;	main.c:472: && dbglink_txidle()
      0047C7 12 52 0F         [24] 2595 	lcall	_dbglink_txidle
      0047CA E5 82            [12] 2596 	mov	a,dpl
      0047CC 60 09            [24] 2597 	jz	00125$
                           0005EE  2598 	C$main.c$474$3$386 ==.
                                   2599 ;	main.c:474: && display_txidle())
      0047CE 12 54 98         [24] 2600 	lcall	_uart0_txidle
      0047D1 E5 82            [12] 2601 	mov	a,dpl
      0047D3 60 02            [24] 2602 	jz	00125$
                           0005F5  2603 	C$main.c$475$3$386 ==.
                                   2604 ;	main.c:475: flg |= WTFLAG_CANSLEEP;
      0047D5 7E 03            [12] 2605 	mov	r6,#0x03
      0047D7                       2606 00125$:
                           0005F7  2607 	C$main.c$477$3$386 ==.
                                   2608 ;	main.c:477: wtimer_idle(flg);
      0047D7 8E 82            [24] 2609 	mov	dpl,r6
      0047D9 12 4E CC         [24] 2610 	lcall	_wtimer_idle
                           0005FC  2611 	C$main.c$479$2$385 ==.
                                   2612 ;	main.c:479: IE_3 = 0; /* no ISR! */
      0047DC C2 AB            [12] 2613 	clr	_IE_3
                           0005FE  2614 	C$libmftypes.h$368$5$397 ==.
                                   2615 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
      0047DE D2 AF            [12] 2616 	setb	_EA
                           000600  2617 	C$main.c$480$4$396 ==.
                                   2618 ;	main.c:480: __enable_irq();
                           000600  2619 	C$main.c$483$1$375 ==.
                                   2620 ;	main.c:483: terminate_radio_error:
      0047E0 80 D3            [24] 2621 	sjmp	00139$
      0047E2                       2622 00129$:
                           000602  2623 	C$main.c$484$1$375 ==.
                                   2624 ;	main.c:484: display_radio_error(i);
      0047E2 8F 82            [24] 2625 	mov	dpl,r7
      0047E4 C0 07            [24] 2626 	push	ar7
      0047E6 12 3F D4         [24] 2627 	lcall	_com0_display_radio_error
      0047E9 D0 07            [24] 2628 	pop	ar7
                           00060B  2629 	C$main.c$486$1$375 ==.
                                   2630 ;	main.c:486: dbglink_display_radio_error(i);
      0047EB 8F 82            [24] 2631 	mov	dpl,r7
      0047ED 12 40 23         [24] 2632 	lcall	_dbglink_display_radio_error
                           000610  2633 	C$main.c$488$1$375 ==.
                                   2634 ;	main.c:488: terminate_error:
      0047F0                       2635 00141$:
                           000610  2636 	C$main.c$492$2$387 ==.
                                   2637 ;	main.c:492: wtimer_runcallbacks();
      0047F0 12 4F 50         [24] 2638 	lcall	_wtimer_runcallbacks
                           000613  2639 	C$main.c$494$3$387 ==.
                                   2640 ;	main.c:494: uint8_t flg = WTFLAG_CANSTANDBY;
      0047F3 7F 02            [12] 2641 	mov	r7,#0x02
                           000615  2642 	C$main.c$497$3$388 ==.
                                   2643 ;	main.c:497: if (axradio_cansleep()
      0047F5 C0 07            [24] 2644 	push	ar7
      0047F7 12 33 9D         [24] 2645 	lcall	_axradio_cansleep
      0047FA E5 82            [12] 2646 	mov	a,dpl
      0047FC D0 07            [24] 2647 	pop	ar7
      0047FE 60 10            [24] 2648 	jz	00132$
                           000620  2649 	C$main.c$499$3$388 ==.
                                   2650 ;	main.c:499: && dbglink_txidle()
      004800 12 52 0F         [24] 2651 	lcall	_dbglink_txidle
      004803 E5 82            [12] 2652 	mov	a,dpl
      004805 60 09            [24] 2653 	jz	00132$
                           000627  2654 	C$main.c$501$3$388 ==.
                                   2655 ;	main.c:501: && display_txidle())
      004807 12 54 98         [24] 2656 	lcall	_uart0_txidle
      00480A E5 82            [12] 2657 	mov	a,dpl
      00480C 60 02            [24] 2658 	jz	00132$
                           00062E  2659 	C$main.c$502$3$388 ==.
                                   2660 ;	main.c:502: flg |= WTFLAG_CANSLEEP;
      00480E 7F 03            [12] 2661 	mov	r7,#0x03
      004810                       2662 00132$:
                           000630  2663 	C$main.c$504$3$388 ==.
                                   2664 ;	main.c:504: wtimer_idle(flg);
      004810 8F 82            [24] 2665 	mov	dpl,r7
      004812 12 4E CC         [24] 2666 	lcall	_wtimer_idle
      004815 80 D9            [24] 2667 	sjmp	00141$
                           000637  2668 	C$main.c$507$1$375 ==.
                           000637  2669 	XG$main$0$0 ==.
      004817 22               [24] 2670 	ret
                                   2671 	.area CSEG    (CODE)
                                   2672 	.area CONST   (CODE)
                           000000  2673 Fmain$__str_0$0$0 == .
      006567                       2674 ___str_0:
      006567 42 65 61 63 6F 6E 3A  2675 	.ascii "Beacon: "
             20
      00656F 00                    2676 	.db 0x00
                           000009  2677 Fmain$__str_1$0$0 == .
      006570                       2678 ___str_1:
      006570 20 20                 2679 	.ascii "  "
      006572 00                    2680 	.db 0x00
                           00000C  2681 Fmain$__str_2$0$0 == .
      006573                       2682 ___str_2:
      006573 0A                    2683 	.db 0x0a
      006574 00                    2684 	.db 0x00
                           00000E  2685 Fmain$__str_3$0$0 == .
      006575                       2686 ___str_3:
      006575 41 43 4B 3A 20        2687 	.ascii "ACK: "
      00657A 00                    2688 	.db 0x00
                           000014  2689 Fmain$__str_4$0$0 == .
      00657B                       2690 ___str_4:
      00657B 20                    2691 	.ascii " "
      00657C 00                    2692 	.db 0x00
                           000016  2693 Fmain$__str_5$0$0 == .
      00657D                       2694 ___str_5:
      00657D 49 44 31 3A 20        2695 	.ascii "ID1: "
      006582 00                    2696 	.db 0x00
                           00001C  2697 Fmain$__str_6$0$0 == .
      006583                       2698 ___str_6:
      006583 0A                    2699 	.db 0x0a
      006584 49 44 33 3A 20        2700 	.ascii "ID3: "
      006589 00                    2701 	.db 0x00
                           000023  2702 Fmain$__str_7$0$0 == .
      00658A                       2703 ___str_7:
      00658A 0A                    2704 	.db 0x0a
      00658B 0A                    2705 	.db 0x0a
      00658C 00                    2706 	.db 0x00
                           000026  2707 Fmain$__str_8$0$0 == .
      00658D                       2708 ___str_8:
      00658D 53 54 3A 20 30 78     2709 	.ascii "ST: 0x"
      006593 00                    2710 	.db 0x00
                           00002D  2711 Fmain$__str_9$0$0 == .
      006594                       2712 ___str_9:
      006594 20 45 52 52 3A 20 30  2713 	.ascii " ERR: 0x"
             78
      00659C 00                    2714 	.db 0x00
                           000036  2715 Fmain$__str_10$0$0 == .
      00659D                       2716 ___str_10:
      00659D 70 6B 74 20 54 30 20  2717 	.ascii "pkt T0 "
      0065A4 00                    2718 	.db 0x00
                           00003E  2719 Fmain$__str_11$0$0 == .
      0065A5                       2720 ___str_11:
      0065A5 54 58 20 74 69 6D 65  2721 	.ascii "TX time: "
             3A 20
      0065AE 00                    2722 	.db 0x00
                           000048  2723 Fmain$__str_12$0$0 == .
      0065AF                       2724 ___str_12:
      0065AF 52 4E 47 20 3D 20     2725 	.ascii "RNG = "
      0065B5 00                    2726 	.db 0x00
                           00004F  2727 Fmain$__str_13$0$0 == .
      0065B6                       2728 ___str_13:
      0065B6 0A                    2729 	.db 0x0a
      0065B7 56 43 4F 49 20 3D 20  2730 	.ascii "VCOI = "
      0065BE 00                    2731 	.db 0x00
                           000058  2732 Fmain$__str_14$0$0 == .
      0065BF                       2733 ___str_14:
      0065BF 0A                    2734 	.db 0x0a
      0065C0 0A                    2735 	.db 0x0a
      0065C1 4D 41 53 54 45 52     2736 	.ascii "MASTER"
      0065C7 0A                    2737 	.db 0x0a
      0065C8 00                    2738 	.db 0x00
                                   2739 	.area XINIT   (CODE)
                           000000  2740 Fmain$__xinit_beacon_packet$0$0 == .
      0067C3                       2741 __xinit__beacon_packet:
      0067C3 00                    2742 	.db #0x00	; 0
      0067C4 00                    2743 	.db #0x00	; 0
      0067C5 11                    2744 	.db #0x11	; 17
      0067C6 22                    2745 	.db #0x22	; 34
      0067C7 33                    2746 	.db #0x33	; 51	'3'
      0067C8 44                    2747 	.db #0x44	; 68	'D'
      0067C9 55                    2748 	.db #0x55	; 85	'U'
      0067CA 66                    2749 	.db #0x66	; 102	'f'
                           000008  2750 Fmain$__xinit_ack_packet$0$0 == .
      0067CB                       2751 __xinit__ack_packet:
      0067CB 00                    2752 	.db #0x00	; 0
      0067CC 77                    2753 	.db #0x77	; 119	'w'
      0067CD 88                    2754 	.db #0x88	; 136
      0067CE 99                    2755 	.db #0x99	; 153
                           00000C  2756 Fmain$__xinit_pkts_received$0$0 == .
      0067CF                       2757 __xinit__pkts_received:
      0067CF 00 00                 2758 	.byte #0x00,#0x00	; 0
                           00000E  2759 Fmain$__xinit_pkts_missing$0$0 == .
      0067D1                       2760 __xinit__pkts_missing:
      0067D1 00 00                 2761 	.byte #0x00,#0x00	; 0
                           000010  2762 Fmain$__xinit_bc_counter$0$0 == .
      0067D3                       2763 __xinit__bc_counter:
      0067D3 00 00                 2764 	.byte #0x00,#0x00	; 0
                           000012  2765 Fmain$__xinit_rcv_flg$0$0 == .
      0067D5                       2766 __xinit__rcv_flg:
      0067D5 00                    2767 	.db #0x00	; 0
                           000013  2768 Fmain$__xinit_pkts_id1$0$0 == .
      0067D6                       2769 __xinit__pkts_id1:
      0067D6 00 00                 2770 	.byte #0x00,#0x00	; 0
                           000015  2771 Fmain$__xinit_pkts_id3$0$0 == .
      0067D8                       2772 __xinit__pkts_id3:
      0067D8 00 00                 2773 	.byte #0x00,#0x00	; 0
                           000017  2774 Fmain$__xinit_rcv_flg_id1$0$0 == .
      0067DA                       2775 __xinit__rcv_flg_id1:
      0067DA 00                    2776 	.db #0x00	; 0
                           000018  2777 Fmain$__xinit_rcv_flg_id3$0$0 == .
      0067DB                       2778 __xinit__rcv_flg_id3:
      0067DB 00                    2779 	.db #0x00	; 0
                                   2780 	.area CABS    (ABS,CODE)
