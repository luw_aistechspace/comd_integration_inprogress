;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW64)
;--------------------------------------------------------
	.module ublox_gnss
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _UBLOX_GPS_FletcherChecksum8_PARM_4
	.globl _UBLOX_GPS_FletcherChecksum8_PARM_3
	.globl _UBLOX_GPS_FletcherChecksum8_PARM_2
	.globl _aligned_alloc_PARM_2
	.globl _memcpy
	.globl _free
	.globl _malloc
	.globl _uart1_tx
	.globl _uart1_rx
	.globl _uart1_init
	.globl _uart1_rxcount
	.globl _uart_timer1_baud
	.globl _dbglink_writestr
	.globl _delay
	.globl _PORTC_7
	.globl _PORTC_6
	.globl _PORTC_5
	.globl _PORTC_4
	.globl _PORTC_3
	.globl _PORTC_2
	.globl _PORTC_1
	.globl _PORTC_0
	.globl _PORTB_7
	.globl _PORTB_6
	.globl _PORTB_5
	.globl _PORTB_4
	.globl _PORTB_3
	.globl _PORTB_2
	.globl _PORTB_1
	.globl _PORTB_0
	.globl _PORTA_7
	.globl _PORTA_6
	.globl _PORTA_5
	.globl _PORTA_4
	.globl _PORTA_3
	.globl _PORTA_2
	.globl _PORTA_1
	.globl _PORTA_0
	.globl _PINC_7
	.globl _PINC_6
	.globl _PINC_5
	.globl _PINC_4
	.globl _PINC_3
	.globl _PINC_2
	.globl _PINC_1
	.globl _PINC_0
	.globl _PINB_7
	.globl _PINB_6
	.globl _PINB_5
	.globl _PINB_4
	.globl _PINB_3
	.globl _PINB_2
	.globl _PINB_1
	.globl _PINB_0
	.globl _PINA_7
	.globl _PINA_6
	.globl _PINA_5
	.globl _PINA_4
	.globl _PINA_3
	.globl _PINA_2
	.globl _PINA_1
	.globl _PINA_0
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _IP_7
	.globl _IP_6
	.globl _IP_5
	.globl _IP_4
	.globl _IP_3
	.globl _IP_2
	.globl _IP_1
	.globl _IP_0
	.globl _EA
	.globl _IE_7
	.globl _IE_6
	.globl _IE_5
	.globl _IE_4
	.globl _IE_3
	.globl _IE_2
	.globl _IE_1
	.globl _IE_0
	.globl _EIP_7
	.globl _EIP_6
	.globl _EIP_5
	.globl _EIP_4
	.globl _EIP_3
	.globl _EIP_2
	.globl _EIP_1
	.globl _EIP_0
	.globl _EIE_7
	.globl _EIE_6
	.globl _EIE_5
	.globl _EIE_4
	.globl _EIE_3
	.globl _EIE_2
	.globl _EIE_1
	.globl _EIE_0
	.globl _E2IP_7
	.globl _E2IP_6
	.globl _E2IP_5
	.globl _E2IP_4
	.globl _E2IP_3
	.globl _E2IP_2
	.globl _E2IP_1
	.globl _E2IP_0
	.globl _E2IE_7
	.globl _E2IE_6
	.globl _E2IE_5
	.globl _E2IE_4
	.globl _E2IE_3
	.globl _E2IE_2
	.globl _E2IE_1
	.globl _E2IE_0
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _WTSTAT
	.globl _WTIRQEN
	.globl _WTEVTD
	.globl _WTEVTD1
	.globl _WTEVTD0
	.globl _WTEVTC
	.globl _WTEVTC1
	.globl _WTEVTC0
	.globl _WTEVTB
	.globl _WTEVTB1
	.globl _WTEVTB0
	.globl _WTEVTA
	.globl _WTEVTA1
	.globl _WTEVTA0
	.globl _WTCNTR1
	.globl _WTCNTB
	.globl _WTCNTB1
	.globl _WTCNTB0
	.globl _WTCNTA
	.globl _WTCNTA1
	.globl _WTCNTA0
	.globl _WTCFGB
	.globl _WTCFGA
	.globl _WDTRESET
	.globl _WDTCFG
	.globl _U1STATUS
	.globl _U1SHREG
	.globl _U1MODE
	.globl _U1CTRL
	.globl _U0STATUS
	.globl _U0SHREG
	.globl _U0MODE
	.globl _U0CTRL
	.globl _T2STATUS
	.globl _T2PERIOD
	.globl _T2PERIOD1
	.globl _T2PERIOD0
	.globl _T2MODE
	.globl _T2CNT
	.globl _T2CNT1
	.globl _T2CNT0
	.globl _T2CLKSRC
	.globl _T1STATUS
	.globl _T1PERIOD
	.globl _T1PERIOD1
	.globl _T1PERIOD0
	.globl _T1MODE
	.globl _T1CNT
	.globl _T1CNT1
	.globl _T1CNT0
	.globl _T1CLKSRC
	.globl _T0STATUS
	.globl _T0PERIOD
	.globl _T0PERIOD1
	.globl _T0PERIOD0
	.globl _T0MODE
	.globl _T0CNT
	.globl _T0CNT1
	.globl _T0CNT0
	.globl _T0CLKSRC
	.globl _SPSTATUS
	.globl _SPSHREG
	.globl _SPMODE
	.globl _SPCLKSRC
	.globl _RADIOSTAT
	.globl _RADIOSTAT1
	.globl _RADIOSTAT0
	.globl _RADIODATA
	.globl _RADIODATA3
	.globl _RADIODATA2
	.globl _RADIODATA1
	.globl _RADIODATA0
	.globl _RADIOADDR
	.globl _RADIOADDR1
	.globl _RADIOADDR0
	.globl _RADIOACC
	.globl _OC1STATUS
	.globl _OC1PIN
	.globl _OC1MODE
	.globl _OC1COMP
	.globl _OC1COMP1
	.globl _OC1COMP0
	.globl _OC0STATUS
	.globl _OC0PIN
	.globl _OC0MODE
	.globl _OC0COMP
	.globl _OC0COMP1
	.globl _OC0COMP0
	.globl _NVSTATUS
	.globl _NVKEY
	.globl _NVDATA
	.globl _NVDATA1
	.globl _NVDATA0
	.globl _NVADDR
	.globl _NVADDR1
	.globl _NVADDR0
	.globl _IC1STATUS
	.globl _IC1MODE
	.globl _IC1CAPT
	.globl _IC1CAPT1
	.globl _IC1CAPT0
	.globl _IC0STATUS
	.globl _IC0MODE
	.globl _IC0CAPT
	.globl _IC0CAPT1
	.globl _IC0CAPT0
	.globl _PORTR
	.globl _PORTC
	.globl _PORTB
	.globl _PORTA
	.globl _PINR
	.globl _PINC
	.globl _PINB
	.globl _PINA
	.globl _DIRR
	.globl _DIRC
	.globl _DIRB
	.globl _DIRA
	.globl _DBGLNKSTAT
	.globl _DBGLNKBUF
	.globl _CODECONFIG
	.globl _CLKSTAT
	.globl _CLKCON
	.globl _ANALOGCOMP
	.globl _ADCCONV
	.globl _ADCCLKSRC
	.globl _ADCCH3CONFIG
	.globl _ADCCH2CONFIG
	.globl _ADCCH1CONFIG
	.globl _ADCCH0CONFIG
	.globl __XPAGE
	.globl _XPAGE
	.globl _SP
	.globl _PSW
	.globl _PCON
	.globl _IP
	.globl _IE
	.globl _EIP
	.globl _EIE
	.globl _E2IP
	.globl _E2IE
	.globl _DPS
	.globl _DPTR1
	.globl _DPTR0
	.globl _DPL1
	.globl _DPL
	.globl _DPH1
	.globl _DPH
	.globl _B
	.globl _ACC
	.globl _XTALREADY
	.globl _XTALOSC
	.globl _XTALAMPL
	.globl _SILICONREV
	.globl _SCRATCH3
	.globl _SCRATCH2
	.globl _SCRATCH1
	.globl _SCRATCH0
	.globl _RADIOMUX
	.globl _RADIOFSTATADDR
	.globl _RADIOFSTATADDR1
	.globl _RADIOFSTATADDR0
	.globl _RADIOFDATAADDR
	.globl _RADIOFDATAADDR1
	.globl _RADIOFDATAADDR0
	.globl _OSCRUN
	.globl _OSCREADY
	.globl _OSCFORCERUN
	.globl _OSCCALIB
	.globl _MISCCTRL
	.globl _LPXOSCGM
	.globl _LPOSCREF
	.globl _LPOSCREF1
	.globl _LPOSCREF0
	.globl _LPOSCPER
	.globl _LPOSCPER1
	.globl _LPOSCPER0
	.globl _LPOSCKFILT
	.globl _LPOSCKFILT1
	.globl _LPOSCKFILT0
	.globl _LPOSCFREQ
	.globl _LPOSCFREQ1
	.globl _LPOSCFREQ0
	.globl _LPOSCCONFIG
	.globl _PINSEL
	.globl _PINCHGC
	.globl _PINCHGB
	.globl _PINCHGA
	.globl _PALTRADIO
	.globl _PALTC
	.globl _PALTB
	.globl _PALTA
	.globl _INTCHGC
	.globl _INTCHGB
	.globl _INTCHGA
	.globl _EXTIRQ
	.globl _GPIOENABLE
	.globl _ANALOGA
	.globl _FRCOSCREF
	.globl _FRCOSCREF1
	.globl _FRCOSCREF0
	.globl _FRCOSCPER
	.globl _FRCOSCPER1
	.globl _FRCOSCPER0
	.globl _FRCOSCKFILT
	.globl _FRCOSCKFILT1
	.globl _FRCOSCKFILT0
	.globl _FRCOSCFREQ
	.globl _FRCOSCFREQ1
	.globl _FRCOSCFREQ0
	.globl _FRCOSCCTRL
	.globl _FRCOSCCONFIG
	.globl _DMA1CONFIG
	.globl _DMA1ADDR
	.globl _DMA1ADDR1
	.globl _DMA1ADDR0
	.globl _DMA0CONFIG
	.globl _DMA0ADDR
	.globl _DMA0ADDR1
	.globl _DMA0ADDR0
	.globl _ADCTUNE2
	.globl _ADCTUNE1
	.globl _ADCTUNE0
	.globl _ADCCH3VAL
	.globl _ADCCH3VAL1
	.globl _ADCCH3VAL0
	.globl _ADCCH2VAL
	.globl _ADCCH2VAL1
	.globl _ADCCH2VAL0
	.globl _ADCCH1VAL
	.globl _ADCCH1VAL1
	.globl _ADCCH1VAL0
	.globl _ADCCH0VAL
	.globl _ADCCH0VAL1
	.globl _ADCCH0VAL0
	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_6
	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_5
	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_4
	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_3
	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_2
	.globl _UBLOX_GPS_FletcherChecksum8
	.globl _UBLOX_GPS_SendCommand_WaitACK
	.globl _UBLOX_GPS_PortInit
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$DPH1$0$0 == 0x0085
_DPH1	=	0x0085
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPL1$0$0 == 0x0084
_DPL1	=	0x0084
G$DPTR0$0$0 == 0x8382
_DPTR0	=	0x8382
G$DPTR1$0$0 == 0x8584
_DPTR1	=	0x8584
G$DPS$0$0 == 0x0086
_DPS	=	0x0086
G$E2IE$0$0 == 0x00a0
_E2IE	=	0x00a0
G$E2IP$0$0 == 0x00c0
_E2IP	=	0x00c0
G$EIE$0$0 == 0x0098
_EIE	=	0x0098
G$EIP$0$0 == 0x00b0
_EIP	=	0x00b0
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$XPAGE$0$0 == 0x00d9
_XPAGE	=	0x00d9
G$_XPAGE$0$0 == 0x00d9
__XPAGE	=	0x00d9
G$ADCCH0CONFIG$0$0 == 0x00ca
_ADCCH0CONFIG	=	0x00ca
G$ADCCH1CONFIG$0$0 == 0x00cb
_ADCCH1CONFIG	=	0x00cb
G$ADCCH2CONFIG$0$0 == 0x00d2
_ADCCH2CONFIG	=	0x00d2
G$ADCCH3CONFIG$0$0 == 0x00d3
_ADCCH3CONFIG	=	0x00d3
G$ADCCLKSRC$0$0 == 0x00d1
_ADCCLKSRC	=	0x00d1
G$ADCCONV$0$0 == 0x00c9
_ADCCONV	=	0x00c9
G$ANALOGCOMP$0$0 == 0x00e1
_ANALOGCOMP	=	0x00e1
G$CLKCON$0$0 == 0x00c6
_CLKCON	=	0x00c6
G$CLKSTAT$0$0 == 0x00c7
_CLKSTAT	=	0x00c7
G$CODECONFIG$0$0 == 0x0097
_CODECONFIG	=	0x0097
G$DBGLNKBUF$0$0 == 0x00e3
_DBGLNKBUF	=	0x00e3
G$DBGLNKSTAT$0$0 == 0x00e2
_DBGLNKSTAT	=	0x00e2
G$DIRA$0$0 == 0x0089
_DIRA	=	0x0089
G$DIRB$0$0 == 0x008a
_DIRB	=	0x008a
G$DIRC$0$0 == 0x008b
_DIRC	=	0x008b
G$DIRR$0$0 == 0x008e
_DIRR	=	0x008e
G$PINA$0$0 == 0x00c8
_PINA	=	0x00c8
G$PINB$0$0 == 0x00e8
_PINB	=	0x00e8
G$PINC$0$0 == 0x00f8
_PINC	=	0x00f8
G$PINR$0$0 == 0x008d
_PINR	=	0x008d
G$PORTA$0$0 == 0x0080
_PORTA	=	0x0080
G$PORTB$0$0 == 0x0088
_PORTB	=	0x0088
G$PORTC$0$0 == 0x0090
_PORTC	=	0x0090
G$PORTR$0$0 == 0x008c
_PORTR	=	0x008c
G$IC0CAPT0$0$0 == 0x00ce
_IC0CAPT0	=	0x00ce
G$IC0CAPT1$0$0 == 0x00cf
_IC0CAPT1	=	0x00cf
G$IC0CAPT$0$0 == 0xcfce
_IC0CAPT	=	0xcfce
G$IC0MODE$0$0 == 0x00cc
_IC0MODE	=	0x00cc
G$IC0STATUS$0$0 == 0x00cd
_IC0STATUS	=	0x00cd
G$IC1CAPT0$0$0 == 0x00d6
_IC1CAPT0	=	0x00d6
G$IC1CAPT1$0$0 == 0x00d7
_IC1CAPT1	=	0x00d7
G$IC1CAPT$0$0 == 0xd7d6
_IC1CAPT	=	0xd7d6
G$IC1MODE$0$0 == 0x00d4
_IC1MODE	=	0x00d4
G$IC1STATUS$0$0 == 0x00d5
_IC1STATUS	=	0x00d5
G$NVADDR0$0$0 == 0x0092
_NVADDR0	=	0x0092
G$NVADDR1$0$0 == 0x0093
_NVADDR1	=	0x0093
G$NVADDR$0$0 == 0x9392
_NVADDR	=	0x9392
G$NVDATA0$0$0 == 0x0094
_NVDATA0	=	0x0094
G$NVDATA1$0$0 == 0x0095
_NVDATA1	=	0x0095
G$NVDATA$0$0 == 0x9594
_NVDATA	=	0x9594
G$NVKEY$0$0 == 0x0096
_NVKEY	=	0x0096
G$NVSTATUS$0$0 == 0x0091
_NVSTATUS	=	0x0091
G$OC0COMP0$0$0 == 0x00bc
_OC0COMP0	=	0x00bc
G$OC0COMP1$0$0 == 0x00bd
_OC0COMP1	=	0x00bd
G$OC0COMP$0$0 == 0xbdbc
_OC0COMP	=	0xbdbc
G$OC0MODE$0$0 == 0x00b9
_OC0MODE	=	0x00b9
G$OC0PIN$0$0 == 0x00ba
_OC0PIN	=	0x00ba
G$OC0STATUS$0$0 == 0x00bb
_OC0STATUS	=	0x00bb
G$OC1COMP0$0$0 == 0x00c4
_OC1COMP0	=	0x00c4
G$OC1COMP1$0$0 == 0x00c5
_OC1COMP1	=	0x00c5
G$OC1COMP$0$0 == 0xc5c4
_OC1COMP	=	0xc5c4
G$OC1MODE$0$0 == 0x00c1
_OC1MODE	=	0x00c1
G$OC1PIN$0$0 == 0x00c2
_OC1PIN	=	0x00c2
G$OC1STATUS$0$0 == 0x00c3
_OC1STATUS	=	0x00c3
G$RADIOACC$0$0 == 0x00b1
_RADIOACC	=	0x00b1
G$RADIOADDR0$0$0 == 0x00b3
_RADIOADDR0	=	0x00b3
G$RADIOADDR1$0$0 == 0x00b2
_RADIOADDR1	=	0x00b2
G$RADIOADDR$0$0 == 0xb2b3
_RADIOADDR	=	0xb2b3
G$RADIODATA0$0$0 == 0x00b7
_RADIODATA0	=	0x00b7
G$RADIODATA1$0$0 == 0x00b6
_RADIODATA1	=	0x00b6
G$RADIODATA2$0$0 == 0x00b5
_RADIODATA2	=	0x00b5
G$RADIODATA3$0$0 == 0x00b4
_RADIODATA3	=	0x00b4
G$RADIODATA$0$0 == 0xb4b5b6b7
_RADIODATA	=	0xb4b5b6b7
G$RADIOSTAT0$0$0 == 0x00be
_RADIOSTAT0	=	0x00be
G$RADIOSTAT1$0$0 == 0x00bf
_RADIOSTAT1	=	0x00bf
G$RADIOSTAT$0$0 == 0xbfbe
_RADIOSTAT	=	0xbfbe
G$SPCLKSRC$0$0 == 0x00df
_SPCLKSRC	=	0x00df
G$SPMODE$0$0 == 0x00dc
_SPMODE	=	0x00dc
G$SPSHREG$0$0 == 0x00de
_SPSHREG	=	0x00de
G$SPSTATUS$0$0 == 0x00dd
_SPSTATUS	=	0x00dd
G$T0CLKSRC$0$0 == 0x009a
_T0CLKSRC	=	0x009a
G$T0CNT0$0$0 == 0x009c
_T0CNT0	=	0x009c
G$T0CNT1$0$0 == 0x009d
_T0CNT1	=	0x009d
G$T0CNT$0$0 == 0x9d9c
_T0CNT	=	0x9d9c
G$T0MODE$0$0 == 0x0099
_T0MODE	=	0x0099
G$T0PERIOD0$0$0 == 0x009e
_T0PERIOD0	=	0x009e
G$T0PERIOD1$0$0 == 0x009f
_T0PERIOD1	=	0x009f
G$T0PERIOD$0$0 == 0x9f9e
_T0PERIOD	=	0x9f9e
G$T0STATUS$0$0 == 0x009b
_T0STATUS	=	0x009b
G$T1CLKSRC$0$0 == 0x00a2
_T1CLKSRC	=	0x00a2
G$T1CNT0$0$0 == 0x00a4
_T1CNT0	=	0x00a4
G$T1CNT1$0$0 == 0x00a5
_T1CNT1	=	0x00a5
G$T1CNT$0$0 == 0xa5a4
_T1CNT	=	0xa5a4
G$T1MODE$0$0 == 0x00a1
_T1MODE	=	0x00a1
G$T1PERIOD0$0$0 == 0x00a6
_T1PERIOD0	=	0x00a6
G$T1PERIOD1$0$0 == 0x00a7
_T1PERIOD1	=	0x00a7
G$T1PERIOD$0$0 == 0xa7a6
_T1PERIOD	=	0xa7a6
G$T1STATUS$0$0 == 0x00a3
_T1STATUS	=	0x00a3
G$T2CLKSRC$0$0 == 0x00aa
_T2CLKSRC	=	0x00aa
G$T2CNT0$0$0 == 0x00ac
_T2CNT0	=	0x00ac
G$T2CNT1$0$0 == 0x00ad
_T2CNT1	=	0x00ad
G$T2CNT$0$0 == 0xadac
_T2CNT	=	0xadac
G$T2MODE$0$0 == 0x00a9
_T2MODE	=	0x00a9
G$T2PERIOD0$0$0 == 0x00ae
_T2PERIOD0	=	0x00ae
G$T2PERIOD1$0$0 == 0x00af
_T2PERIOD1	=	0x00af
G$T2PERIOD$0$0 == 0xafae
_T2PERIOD	=	0xafae
G$T2STATUS$0$0 == 0x00ab
_T2STATUS	=	0x00ab
G$U0CTRL$0$0 == 0x00e4
_U0CTRL	=	0x00e4
G$U0MODE$0$0 == 0x00e7
_U0MODE	=	0x00e7
G$U0SHREG$0$0 == 0x00e6
_U0SHREG	=	0x00e6
G$U0STATUS$0$0 == 0x00e5
_U0STATUS	=	0x00e5
G$U1CTRL$0$0 == 0x00ec
_U1CTRL	=	0x00ec
G$U1MODE$0$0 == 0x00ef
_U1MODE	=	0x00ef
G$U1SHREG$0$0 == 0x00ee
_U1SHREG	=	0x00ee
G$U1STATUS$0$0 == 0x00ed
_U1STATUS	=	0x00ed
G$WDTCFG$0$0 == 0x00da
_WDTCFG	=	0x00da
G$WDTRESET$0$0 == 0x00db
_WDTRESET	=	0x00db
G$WTCFGA$0$0 == 0x00f1
_WTCFGA	=	0x00f1
G$WTCFGB$0$0 == 0x00f9
_WTCFGB	=	0x00f9
G$WTCNTA0$0$0 == 0x00f2
_WTCNTA0	=	0x00f2
G$WTCNTA1$0$0 == 0x00f3
_WTCNTA1	=	0x00f3
G$WTCNTA$0$0 == 0xf3f2
_WTCNTA	=	0xf3f2
G$WTCNTB0$0$0 == 0x00fa
_WTCNTB0	=	0x00fa
G$WTCNTB1$0$0 == 0x00fb
_WTCNTB1	=	0x00fb
G$WTCNTB$0$0 == 0xfbfa
_WTCNTB	=	0xfbfa
G$WTCNTR1$0$0 == 0x00eb
_WTCNTR1	=	0x00eb
G$WTEVTA0$0$0 == 0x00f4
_WTEVTA0	=	0x00f4
G$WTEVTA1$0$0 == 0x00f5
_WTEVTA1	=	0x00f5
G$WTEVTA$0$0 == 0xf5f4
_WTEVTA	=	0xf5f4
G$WTEVTB0$0$0 == 0x00f6
_WTEVTB0	=	0x00f6
G$WTEVTB1$0$0 == 0x00f7
_WTEVTB1	=	0x00f7
G$WTEVTB$0$0 == 0xf7f6
_WTEVTB	=	0xf7f6
G$WTEVTC0$0$0 == 0x00fc
_WTEVTC0	=	0x00fc
G$WTEVTC1$0$0 == 0x00fd
_WTEVTC1	=	0x00fd
G$WTEVTC$0$0 == 0xfdfc
_WTEVTC	=	0xfdfc
G$WTEVTD0$0$0 == 0x00fe
_WTEVTD0	=	0x00fe
G$WTEVTD1$0$0 == 0x00ff
_WTEVTD1	=	0x00ff
G$WTEVTD$0$0 == 0xfffe
_WTEVTD	=	0xfffe
G$WTIRQEN$0$0 == 0x00e9
_WTIRQEN	=	0x00e9
G$WTSTAT$0$0 == 0x00ea
_WTSTAT	=	0x00ea
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$ACC_0$0$0 == 0x00e0
_ACC_0	=	0x00e0
G$ACC_1$0$0 == 0x00e1
_ACC_1	=	0x00e1
G$ACC_2$0$0 == 0x00e2
_ACC_2	=	0x00e2
G$ACC_3$0$0 == 0x00e3
_ACC_3	=	0x00e3
G$ACC_4$0$0 == 0x00e4
_ACC_4	=	0x00e4
G$ACC_5$0$0 == 0x00e5
_ACC_5	=	0x00e5
G$ACC_6$0$0 == 0x00e6
_ACC_6	=	0x00e6
G$ACC_7$0$0 == 0x00e7
_ACC_7	=	0x00e7
G$B_0$0$0 == 0x00f0
_B_0	=	0x00f0
G$B_1$0$0 == 0x00f1
_B_1	=	0x00f1
G$B_2$0$0 == 0x00f2
_B_2	=	0x00f2
G$B_3$0$0 == 0x00f3
_B_3	=	0x00f3
G$B_4$0$0 == 0x00f4
_B_4	=	0x00f4
G$B_5$0$0 == 0x00f5
_B_5	=	0x00f5
G$B_6$0$0 == 0x00f6
_B_6	=	0x00f6
G$B_7$0$0 == 0x00f7
_B_7	=	0x00f7
G$E2IE_0$0$0 == 0x00a0
_E2IE_0	=	0x00a0
G$E2IE_1$0$0 == 0x00a1
_E2IE_1	=	0x00a1
G$E2IE_2$0$0 == 0x00a2
_E2IE_2	=	0x00a2
G$E2IE_3$0$0 == 0x00a3
_E2IE_3	=	0x00a3
G$E2IE_4$0$0 == 0x00a4
_E2IE_4	=	0x00a4
G$E2IE_5$0$0 == 0x00a5
_E2IE_5	=	0x00a5
G$E2IE_6$0$0 == 0x00a6
_E2IE_6	=	0x00a6
G$E2IE_7$0$0 == 0x00a7
_E2IE_7	=	0x00a7
G$E2IP_0$0$0 == 0x00c0
_E2IP_0	=	0x00c0
G$E2IP_1$0$0 == 0x00c1
_E2IP_1	=	0x00c1
G$E2IP_2$0$0 == 0x00c2
_E2IP_2	=	0x00c2
G$E2IP_3$0$0 == 0x00c3
_E2IP_3	=	0x00c3
G$E2IP_4$0$0 == 0x00c4
_E2IP_4	=	0x00c4
G$E2IP_5$0$0 == 0x00c5
_E2IP_5	=	0x00c5
G$E2IP_6$0$0 == 0x00c6
_E2IP_6	=	0x00c6
G$E2IP_7$0$0 == 0x00c7
_E2IP_7	=	0x00c7
G$EIE_0$0$0 == 0x0098
_EIE_0	=	0x0098
G$EIE_1$0$0 == 0x0099
_EIE_1	=	0x0099
G$EIE_2$0$0 == 0x009a
_EIE_2	=	0x009a
G$EIE_3$0$0 == 0x009b
_EIE_3	=	0x009b
G$EIE_4$0$0 == 0x009c
_EIE_4	=	0x009c
G$EIE_5$0$0 == 0x009d
_EIE_5	=	0x009d
G$EIE_6$0$0 == 0x009e
_EIE_6	=	0x009e
G$EIE_7$0$0 == 0x009f
_EIE_7	=	0x009f
G$EIP_0$0$0 == 0x00b0
_EIP_0	=	0x00b0
G$EIP_1$0$0 == 0x00b1
_EIP_1	=	0x00b1
G$EIP_2$0$0 == 0x00b2
_EIP_2	=	0x00b2
G$EIP_3$0$0 == 0x00b3
_EIP_3	=	0x00b3
G$EIP_4$0$0 == 0x00b4
_EIP_4	=	0x00b4
G$EIP_5$0$0 == 0x00b5
_EIP_5	=	0x00b5
G$EIP_6$0$0 == 0x00b6
_EIP_6	=	0x00b6
G$EIP_7$0$0 == 0x00b7
_EIP_7	=	0x00b7
G$IE_0$0$0 == 0x00a8
_IE_0	=	0x00a8
G$IE_1$0$0 == 0x00a9
_IE_1	=	0x00a9
G$IE_2$0$0 == 0x00aa
_IE_2	=	0x00aa
G$IE_3$0$0 == 0x00ab
_IE_3	=	0x00ab
G$IE_4$0$0 == 0x00ac
_IE_4	=	0x00ac
G$IE_5$0$0 == 0x00ad
_IE_5	=	0x00ad
G$IE_6$0$0 == 0x00ae
_IE_6	=	0x00ae
G$IE_7$0$0 == 0x00af
_IE_7	=	0x00af
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$IP_0$0$0 == 0x00b8
_IP_0	=	0x00b8
G$IP_1$0$0 == 0x00b9
_IP_1	=	0x00b9
G$IP_2$0$0 == 0x00ba
_IP_2	=	0x00ba
G$IP_3$0$0 == 0x00bb
_IP_3	=	0x00bb
G$IP_4$0$0 == 0x00bc
_IP_4	=	0x00bc
G$IP_5$0$0 == 0x00bd
_IP_5	=	0x00bd
G$IP_6$0$0 == 0x00be
_IP_6	=	0x00be
G$IP_7$0$0 == 0x00bf
_IP_7	=	0x00bf
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$PINA_0$0$0 == 0x00c8
_PINA_0	=	0x00c8
G$PINA_1$0$0 == 0x00c9
_PINA_1	=	0x00c9
G$PINA_2$0$0 == 0x00ca
_PINA_2	=	0x00ca
G$PINA_3$0$0 == 0x00cb
_PINA_3	=	0x00cb
G$PINA_4$0$0 == 0x00cc
_PINA_4	=	0x00cc
G$PINA_5$0$0 == 0x00cd
_PINA_5	=	0x00cd
G$PINA_6$0$0 == 0x00ce
_PINA_6	=	0x00ce
G$PINA_7$0$0 == 0x00cf
_PINA_7	=	0x00cf
G$PINB_0$0$0 == 0x00e8
_PINB_0	=	0x00e8
G$PINB_1$0$0 == 0x00e9
_PINB_1	=	0x00e9
G$PINB_2$0$0 == 0x00ea
_PINB_2	=	0x00ea
G$PINB_3$0$0 == 0x00eb
_PINB_3	=	0x00eb
G$PINB_4$0$0 == 0x00ec
_PINB_4	=	0x00ec
G$PINB_5$0$0 == 0x00ed
_PINB_5	=	0x00ed
G$PINB_6$0$0 == 0x00ee
_PINB_6	=	0x00ee
G$PINB_7$0$0 == 0x00ef
_PINB_7	=	0x00ef
G$PINC_0$0$0 == 0x00f8
_PINC_0	=	0x00f8
G$PINC_1$0$0 == 0x00f9
_PINC_1	=	0x00f9
G$PINC_2$0$0 == 0x00fa
_PINC_2	=	0x00fa
G$PINC_3$0$0 == 0x00fb
_PINC_3	=	0x00fb
G$PINC_4$0$0 == 0x00fc
_PINC_4	=	0x00fc
G$PINC_5$0$0 == 0x00fd
_PINC_5	=	0x00fd
G$PINC_6$0$0 == 0x00fe
_PINC_6	=	0x00fe
G$PINC_7$0$0 == 0x00ff
_PINC_7	=	0x00ff
G$PORTA_0$0$0 == 0x0080
_PORTA_0	=	0x0080
G$PORTA_1$0$0 == 0x0081
_PORTA_1	=	0x0081
G$PORTA_2$0$0 == 0x0082
_PORTA_2	=	0x0082
G$PORTA_3$0$0 == 0x0083
_PORTA_3	=	0x0083
G$PORTA_4$0$0 == 0x0084
_PORTA_4	=	0x0084
G$PORTA_5$0$0 == 0x0085
_PORTA_5	=	0x0085
G$PORTA_6$0$0 == 0x0086
_PORTA_6	=	0x0086
G$PORTA_7$0$0 == 0x0087
_PORTA_7	=	0x0087
G$PORTB_0$0$0 == 0x0088
_PORTB_0	=	0x0088
G$PORTB_1$0$0 == 0x0089
_PORTB_1	=	0x0089
G$PORTB_2$0$0 == 0x008a
_PORTB_2	=	0x008a
G$PORTB_3$0$0 == 0x008b
_PORTB_3	=	0x008b
G$PORTB_4$0$0 == 0x008c
_PORTB_4	=	0x008c
G$PORTB_5$0$0 == 0x008d
_PORTB_5	=	0x008d
G$PORTB_6$0$0 == 0x008e
_PORTB_6	=	0x008e
G$PORTB_7$0$0 == 0x008f
_PORTB_7	=	0x008f
G$PORTC_0$0$0 == 0x0090
_PORTC_0	=	0x0090
G$PORTC_1$0$0 == 0x0091
_PORTC_1	=	0x0091
G$PORTC_2$0$0 == 0x0092
_PORTC_2	=	0x0092
G$PORTC_3$0$0 == 0x0093
_PORTC_3	=	0x0093
G$PORTC_4$0$0 == 0x0094
_PORTC_4	=	0x0094
G$PORTC_5$0$0 == 0x0095
_PORTC_5	=	0x0095
G$PORTC_6$0$0 == 0x0096
_PORTC_6	=	0x0096
G$PORTC_7$0$0 == 0x0097
_PORTC_7	=	0x0097
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_id$1$184==.
_UBLOX_GPS_SendCommand_WaitACK_PARM_2:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_length$1$184==.
_UBLOX_GPS_SendCommand_WaitACK_PARM_3:
	.ds 2
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$payload$1$184==.
_UBLOX_GPS_SendCommand_WaitACK_PARM_4:
	.ds 3
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$AnsOrAck$1$184==.
_UBLOX_GPS_SendCommand_WaitACK_PARM_5:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$RxLength$1$184==.
_UBLOX_GPS_SendCommand_WaitACK_PARM_6:
	.ds 3
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_class$1$184==.
_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$k$1$185==.
_UBLOX_GPS_SendCommand_WaitACK_k_1_185:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$RetVal$1$185==.
_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$buffer_aux$1$185==.
_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185:
	.ds 2
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$rx_buffer$1$185==.
_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185:
	.ds 2
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$CK_A$1$185==.
_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$CK_B$1$185==.
_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185:
	.ds 1
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$sloc0$1$0==.
_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0:
	.ds 2
Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$sloc1$1$0==.
_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0:
	.ds 3
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
Lublox_gnss.aligned_alloc$size$1$150==.
_aligned_alloc_PARM_2:
	.ds 2
Lublox_gnss.UBLOX_GPS_FletcherChecksum8$CK_A$1$181==.
_UBLOX_GPS_FletcherChecksum8_PARM_2:
	.ds 3
Lublox_gnss.UBLOX_GPS_FletcherChecksum8$CK_B$1$181==.
_UBLOX_GPS_FletcherChecksum8_PARM_3:
	.ds 3
Lublox_gnss.UBLOX_GPS_FletcherChecksum8$length$1$181==.
_UBLOX_GPS_FletcherChecksum8_PARM_4:
	.ds 2
Lublox_gnss.UBLOX_GPS_FletcherChecksum8$buffer$1$181==.
_UBLOX_GPS_FletcherChecksum8_buffer_1_181:
	.ds 3
Lublox_gnss.UBLOX_GPS_FletcherChecksum8$sloc0$1$0==.
_UBLOX_GPS_FletcherChecksum8_sloc0_1_0:
	.ds 3
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
G$ADCCH0VAL0$0$0 == 0x7020
_ADCCH0VAL0	=	0x7020
G$ADCCH0VAL1$0$0 == 0x7021
_ADCCH0VAL1	=	0x7021
G$ADCCH0VAL$0$0 == 0x7020
_ADCCH0VAL	=	0x7020
G$ADCCH1VAL0$0$0 == 0x7022
_ADCCH1VAL0	=	0x7022
G$ADCCH1VAL1$0$0 == 0x7023
_ADCCH1VAL1	=	0x7023
G$ADCCH1VAL$0$0 == 0x7022
_ADCCH1VAL	=	0x7022
G$ADCCH2VAL0$0$0 == 0x7024
_ADCCH2VAL0	=	0x7024
G$ADCCH2VAL1$0$0 == 0x7025
_ADCCH2VAL1	=	0x7025
G$ADCCH2VAL$0$0 == 0x7024
_ADCCH2VAL	=	0x7024
G$ADCCH3VAL0$0$0 == 0x7026
_ADCCH3VAL0	=	0x7026
G$ADCCH3VAL1$0$0 == 0x7027
_ADCCH3VAL1	=	0x7027
G$ADCCH3VAL$0$0 == 0x7026
_ADCCH3VAL	=	0x7026
G$ADCTUNE0$0$0 == 0x7028
_ADCTUNE0	=	0x7028
G$ADCTUNE1$0$0 == 0x7029
_ADCTUNE1	=	0x7029
G$ADCTUNE2$0$0 == 0x702a
_ADCTUNE2	=	0x702a
G$DMA0ADDR0$0$0 == 0x7010
_DMA0ADDR0	=	0x7010
G$DMA0ADDR1$0$0 == 0x7011
_DMA0ADDR1	=	0x7011
G$DMA0ADDR$0$0 == 0x7010
_DMA0ADDR	=	0x7010
G$DMA0CONFIG$0$0 == 0x7014
_DMA0CONFIG	=	0x7014
G$DMA1ADDR0$0$0 == 0x7012
_DMA1ADDR0	=	0x7012
G$DMA1ADDR1$0$0 == 0x7013
_DMA1ADDR1	=	0x7013
G$DMA1ADDR$0$0 == 0x7012
_DMA1ADDR	=	0x7012
G$DMA1CONFIG$0$0 == 0x7015
_DMA1CONFIG	=	0x7015
G$FRCOSCCONFIG$0$0 == 0x7070
_FRCOSCCONFIG	=	0x7070
G$FRCOSCCTRL$0$0 == 0x7071
_FRCOSCCTRL	=	0x7071
G$FRCOSCFREQ0$0$0 == 0x7076
_FRCOSCFREQ0	=	0x7076
G$FRCOSCFREQ1$0$0 == 0x7077
_FRCOSCFREQ1	=	0x7077
G$FRCOSCFREQ$0$0 == 0x7076
_FRCOSCFREQ	=	0x7076
G$FRCOSCKFILT0$0$0 == 0x7072
_FRCOSCKFILT0	=	0x7072
G$FRCOSCKFILT1$0$0 == 0x7073
_FRCOSCKFILT1	=	0x7073
G$FRCOSCKFILT$0$0 == 0x7072
_FRCOSCKFILT	=	0x7072
G$FRCOSCPER0$0$0 == 0x7078
_FRCOSCPER0	=	0x7078
G$FRCOSCPER1$0$0 == 0x7079
_FRCOSCPER1	=	0x7079
G$FRCOSCPER$0$0 == 0x7078
_FRCOSCPER	=	0x7078
G$FRCOSCREF0$0$0 == 0x7074
_FRCOSCREF0	=	0x7074
G$FRCOSCREF1$0$0 == 0x7075
_FRCOSCREF1	=	0x7075
G$FRCOSCREF$0$0 == 0x7074
_FRCOSCREF	=	0x7074
G$ANALOGA$0$0 == 0x7007
_ANALOGA	=	0x7007
G$GPIOENABLE$0$0 == 0x700c
_GPIOENABLE	=	0x700c
G$EXTIRQ$0$0 == 0x7003
_EXTIRQ	=	0x7003
G$INTCHGA$0$0 == 0x7000
_INTCHGA	=	0x7000
G$INTCHGB$0$0 == 0x7001
_INTCHGB	=	0x7001
G$INTCHGC$0$0 == 0x7002
_INTCHGC	=	0x7002
G$PALTA$0$0 == 0x7008
_PALTA	=	0x7008
G$PALTB$0$0 == 0x7009
_PALTB	=	0x7009
G$PALTC$0$0 == 0x700a
_PALTC	=	0x700a
G$PALTRADIO$0$0 == 0x7046
_PALTRADIO	=	0x7046
G$PINCHGA$0$0 == 0x7004
_PINCHGA	=	0x7004
G$PINCHGB$0$0 == 0x7005
_PINCHGB	=	0x7005
G$PINCHGC$0$0 == 0x7006
_PINCHGC	=	0x7006
G$PINSEL$0$0 == 0x700b
_PINSEL	=	0x700b
G$LPOSCCONFIG$0$0 == 0x7060
_LPOSCCONFIG	=	0x7060
G$LPOSCFREQ0$0$0 == 0x7066
_LPOSCFREQ0	=	0x7066
G$LPOSCFREQ1$0$0 == 0x7067
_LPOSCFREQ1	=	0x7067
G$LPOSCFREQ$0$0 == 0x7066
_LPOSCFREQ	=	0x7066
G$LPOSCKFILT0$0$0 == 0x7062
_LPOSCKFILT0	=	0x7062
G$LPOSCKFILT1$0$0 == 0x7063
_LPOSCKFILT1	=	0x7063
G$LPOSCKFILT$0$0 == 0x7062
_LPOSCKFILT	=	0x7062
G$LPOSCPER0$0$0 == 0x7068
_LPOSCPER0	=	0x7068
G$LPOSCPER1$0$0 == 0x7069
_LPOSCPER1	=	0x7069
G$LPOSCPER$0$0 == 0x7068
_LPOSCPER	=	0x7068
G$LPOSCREF0$0$0 == 0x7064
_LPOSCREF0	=	0x7064
G$LPOSCREF1$0$0 == 0x7065
_LPOSCREF1	=	0x7065
G$LPOSCREF$0$0 == 0x7064
_LPOSCREF	=	0x7064
G$LPXOSCGM$0$0 == 0x7054
_LPXOSCGM	=	0x7054
G$MISCCTRL$0$0 == 0x7f01
_MISCCTRL	=	0x7f01
G$OSCCALIB$0$0 == 0x7053
_OSCCALIB	=	0x7053
G$OSCFORCERUN$0$0 == 0x7050
_OSCFORCERUN	=	0x7050
G$OSCREADY$0$0 == 0x7052
_OSCREADY	=	0x7052
G$OSCRUN$0$0 == 0x7051
_OSCRUN	=	0x7051
G$RADIOFDATAADDR0$0$0 == 0x7040
_RADIOFDATAADDR0	=	0x7040
G$RADIOFDATAADDR1$0$0 == 0x7041
_RADIOFDATAADDR1	=	0x7041
G$RADIOFDATAADDR$0$0 == 0x7040
_RADIOFDATAADDR	=	0x7040
G$RADIOFSTATADDR0$0$0 == 0x7042
_RADIOFSTATADDR0	=	0x7042
G$RADIOFSTATADDR1$0$0 == 0x7043
_RADIOFSTATADDR1	=	0x7043
G$RADIOFSTATADDR$0$0 == 0x7042
_RADIOFSTATADDR	=	0x7042
G$RADIOMUX$0$0 == 0x7044
_RADIOMUX	=	0x7044
G$SCRATCH0$0$0 == 0x7084
_SCRATCH0	=	0x7084
G$SCRATCH1$0$0 == 0x7085
_SCRATCH1	=	0x7085
G$SCRATCH2$0$0 == 0x7086
_SCRATCH2	=	0x7086
G$SCRATCH3$0$0 == 0x7087
_SCRATCH3	=	0x7087
G$SILICONREV$0$0 == 0x7f00
_SILICONREV	=	0x7f00
G$XTALAMPL$0$0 == 0x7f19
_XTALAMPL	=	0x7f19
G$XTALOSC$0$0 == 0x7f18
_XTALOSC	=	0x7f18
G$XTALREADY$0$0 == 0x7f1a
_XTALREADY	=	0x7f1a
Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav1$1$196==.
_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196:
	.ds 3
Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav2$1$196==.
_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196:
	.ds 8
Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav3$1$196==.
_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196:
	.ds 8
Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav5$1$196==.
_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196:
	.ds 36
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'UBLOX_GPS_FletcherChecksum8'
;------------------------------------------------------------
;CK_A                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_2'
;CK_B                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_3'
;length                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_4'
;buffer                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_buffer_1_181'
;i                         Allocated to registers r1 
;sloc0                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc0_1_0'
;------------------------------------------------------------
	G$UBLOX_GPS_FletcherChecksum8$0$0 ==.
	C$ublox_gnss.c$31$0$0 ==.
;	..\COMMON\ublox_gnss.c:31: void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
;	-----------------------------------------
;	 function UBLOX_GPS_FletcherChecksum8
;	-----------------------------------------
_UBLOX_GPS_FletcherChecksum8:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	mov	_UBLOX_GPS_FletcherChecksum8_buffer_1_181,dpl
	mov	(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 1),dph
	mov	(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 2),b
	C$ublox_gnss.c$34$1$182 ==.
;	..\COMMON\ublox_gnss.c:34: *CK_A = 0;
	mov	r2,_UBLOX_GPS_FletcherChecksum8_PARM_2
	mov	r3,(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1)
	mov	r4,(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	clr	a
	lcall	__gptrput
	C$ublox_gnss.c$35$1$182 ==.
;	..\COMMON\ublox_gnss.c:35: *CK_B = 0;
	mov	_UBLOX_GPS_FletcherChecksum8_sloc0_1_0,_UBLOX_GPS_FletcherChecksum8_PARM_3
	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1),(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1)
	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2),(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2)
	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
	lcall	__gptrput
	C$ublox_gnss.c$36$1$182 ==.
;	..\COMMON\ublox_gnss.c:36: for (i = 0; i < length; i++)
	mov	r1,#0x00
00103$:
	mov	ar0,r1
	mov	r7,#0x00
	clr	c
	mov	a,r0
	subb	a,_UBLOX_GPS_FletcherChecksum8_PARM_4
	mov	a,r7
	subb	a,(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1)
	jnc	00101$
	C$ublox_gnss.c$38$2$183 ==.
;	..\COMMON\ublox_gnss.c:38: *CK_A += buffer[i];
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r7,a
	mov	a,r1
	add	a,_UBLOX_GPS_FletcherChecksum8_buffer_1_181
	mov	r0,a
	clr	a
	addc	a,(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 1)
	mov	r5,a
	mov	r6,(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 2)
	mov	dpl,r0
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
	mov	r0,a
	add	a,r7
	mov	r7,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrput
	C$ublox_gnss.c$40$2$183 ==.
;	..\COMMON\ublox_gnss.c:40: *CK_B += *CK_A;
	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
	lcall	__gptrget
	mov	r6,a
	add	a,r7
	mov	r7,a
	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
	lcall	__gptrput
	C$ublox_gnss.c$36$1$182 ==.
;	..\COMMON\ublox_gnss.c:36: for (i = 0; i < length; i++)
	inc	r1
	sjmp	00103$
00101$:
	C$ublox_gnss.c$42$1$182 ==.
;	..\COMMON\ublox_gnss.c:42: return;
	C$ublox_gnss.c$43$1$182 ==.
	XG$UBLOX_GPS_FletcherChecksum8$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'UBLOX_GPS_SendCommand_WaitACK'
;------------------------------------------------------------
;msg_id                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_2'
;msg_length                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_3'
;payload                   Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_4'
;AnsOrAck                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_5'
;RxLength                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_6'
;msg_class                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184'
;i                         Allocated to registers r1 
;k                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_k_1_185'
;RetVal                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185'
;buffer_aux                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185'
;rx_buffer                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185'
;CK_A                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185'
;CK_B                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185'
;sloc0                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0'
;sloc1                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0'
;------------------------------------------------------------
	G$UBLOX_GPS_SendCommand_WaitACK$0$0 ==.
	C$ublox_gnss.c$56$1$182 ==.
;	..\COMMON\ublox_gnss.c:56: uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
;	-----------------------------------------
;	 function UBLOX_GPS_SendCommand_WaitACK
;	-----------------------------------------
_UBLOX_GPS_SendCommand_WaitACK:
	mov	_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184,dpl
	C$ublox_gnss.c$59$1$182 ==.
;	..\COMMON\ublox_gnss.c:59: uint8_t RetVal = 0;
	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x00
	C$ublox_gnss.c$63$1$185 ==.
;	..\COMMON\ublox_gnss.c:63: buffer_aux =(uint8_t *) malloc((msg_length)+10);
	mov	a,#0x0a
	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
	mov	dpl,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
	mov	dph,a
	lcall	_malloc
	mov	r4,dpl
	mov	r5,dph
	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,r4
	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),r5
	C$ublox_gnss.c$64$1$185 ==.
;	..\COMMON\ublox_gnss.c:64: rx_buffer =(uint8_t *) malloc(40);
	mov	dptr,#0x0028
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
	mov	_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185,r2
	mov	(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1),r3
	C$ublox_gnss.c$65$1$185 ==.
;	..\COMMON\ublox_gnss.c:65: for(i = 0; i<40;i++)
	mov	r1,#0x00
00125$:
	C$ublox_gnss.c$67$2$186 ==.
;	..\COMMON\ublox_gnss.c:67: *(rx_buffer+i)=0;
	mov	a,r1
	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dpl,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	dph,a
	clr	a
	movx	@dptr,a
	C$ublox_gnss.c$65$1$185 ==.
;	..\COMMON\ublox_gnss.c:65: for(i = 0; i<40;i++)
	inc	r1
	cjne	r1,#0x28,00186$
00186$:
	jc	00125$
	C$ublox_gnss.c$70$1$185 ==.
;	..\COMMON\ublox_gnss.c:70: *buffer_aux = msg_class;
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184
	movx	@dptr,a
	inc	dptr
	C$ublox_gnss.c$71$1$185 ==.
;	..\COMMON\ublox_gnss.c:71: buffer_aux++;
	C$ublox_gnss.c$72$1$185 ==.
;	..\COMMON\ublox_gnss.c:72: *buffer_aux = msg_id;
	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,dpl
	mov  (_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),dph
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_2
	movx	@dptr,a
	inc	dptr
	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,dpl
	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),dph
	C$ublox_gnss.c$73$1$185 ==.
;	..\COMMON\ublox_gnss.c:73: buffer_aux++;
	C$ublox_gnss.c$74$1$185 ==.
;	..\COMMON\ublox_gnss.c:74: *buffer_aux = msg_length;
	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	mov	a,r1
	movx	@dptr,a
	C$ublox_gnss.c$75$1$185 ==.
;	..\COMMON\ublox_gnss.c:75: buffer_aux +=2;
	mov	a,#0x02
	add	a,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),a
	C$ublox_gnss.c$76$1$185 ==.
;	..\COMMON\ublox_gnss.c:76: memcpy(buffer_aux,payload,msg_length);
	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	mov	r1,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	mov	r6,#0x00
	mov	_memcpy_PARM_2,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
	mov	(_memcpy_PARM_2 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
	mov	(_memcpy_PARM_2 + 2),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
	mov	_memcpy_PARM_3,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
	mov	(_memcpy_PARM_3 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
	mov	dpl,r0
	mov	dph,r1
	mov	b,r6
	lcall	_memcpy
	C$ublox_gnss.c$77$1$185 ==.
;	..\COMMON\ublox_gnss.c:77: buffer_aux -=4;
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	add	a,#0xfc
	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,a
	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	addc	a,#0xff
	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),a
	C$ublox_gnss.c$78$1$185 ==.
;	..\COMMON\ublox_gnss.c:78: UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);
	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	mov	r1,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	mov	r6,#0x00
	mov	_UBLOX_GPS_FletcherChecksum8_PARM_2,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),#0x00
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),r6
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2),#0x40
	mov	_UBLOX_GPS_FletcherChecksum8_PARM_3,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),#0x00
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),r6
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2),#0x40
	mov	a,#0x04
	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
	mov	_UBLOX_GPS_FletcherChecksum8_PARM_4,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1),a
	mov	dpl,r0
	mov	dph,r1
	mov	b,r6
	lcall	_UBLOX_GPS_FletcherChecksum8
	C$ublox_gnss.c$80$1$185 ==.
;	..\COMMON\ublox_gnss.c:80: uart1_tx(UBX_HEADER1_VAL);
	mov	dpl,#0xb5
	lcall	_uart1_tx
	C$ublox_gnss.c$81$1$185 ==.
;	..\COMMON\ublox_gnss.c:81: uart1_tx(UBX_HEADER2_VAL);
	mov	dpl,#0x62
	lcall	_uart1_tx
	C$ublox_gnss.c$82$1$185 ==.
;	..\COMMON\ublox_gnss.c:82: uart1_tx(msg_class);
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184
	lcall	_uart1_tx
	C$ublox_gnss.c$83$1$185 ==.
;	..\COMMON\ublox_gnss.c:83: uart1_tx(msg_id);
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_PARM_2
	lcall	_uart1_tx
	C$ublox_gnss.c$84$1$185 ==.
;	..\COMMON\ublox_gnss.c:84: uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
	mov	dpl,r1
	lcall	_uart1_tx
	C$ublox_gnss.c$85$1$185 ==.
;	..\COMMON\ublox_gnss.c:85: uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB
	mov	r1,#0x00
	mov	r6,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
	mov	dpl,r6
	lcall	_uart1_tx
	C$ublox_gnss.c$88$1$185 ==.
;	..\COMMON\ublox_gnss.c:88: for(i = 0;i<msg_length;i++)
	mov	r6,#0x00
00128$:
	mov	ar0,r6
	mov	r1,#0x00
	clr	c
	mov	a,r0
	subb	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
	mov	a,r1
	subb	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
	jnc	00102$
	C$ublox_gnss.c$90$2$187 ==.
;	..\COMMON\ublox_gnss.c:90: uart1_tx(*(payload+i));
	mov	a,r6
	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
	mov	r0,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
	mov	r1,a
	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
	mov	dpl,r0
	mov	dph,r1
	mov	b,r7
	lcall	__gptrget
	mov	r0,a
	mov	dpl,a
	lcall	_uart1_tx
	C$ublox_gnss.c$88$1$185 ==.
;	..\COMMON\ublox_gnss.c:88: for(i = 0;i<msg_length;i++)
	inc	r6
	sjmp	00128$
00102$:
	C$ublox_gnss.c$93$1$185 ==.
;	..\COMMON\ublox_gnss.c:93: uart1_tx(CK_A);
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185
	lcall	_uart1_tx
	C$ublox_gnss.c$94$1$185 ==.
;	..\COMMON\ublox_gnss.c:94: uart1_tx(CK_B);
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185
	lcall	_uart1_tx
	C$ublox_gnss.c$96$1$185 ==.
;	..\COMMON\ublox_gnss.c:96: dbglink_writestr("command sent\n");
	mov	dptr,#___str_0
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$99$1$185 ==.
;	..\COMMON\ublox_gnss.c:99: do{
00103$:
	C$ublox_gnss.c$100$2$188 ==.
;	..\COMMON\ublox_gnss.c:100: delay(2500);
	mov	dptr,#0x09c4
	lcall	_delay
	C$ublox_gnss.c$105$1$185 ==.
;	..\COMMON\ublox_gnss.c:105: }while(!uart1_rxcount());
	lcall	_uart1_rxcount
	mov	a,dpl
	jz	00103$
	C$ublox_gnss.c$106$1$185 ==.
;	..\COMMON\ublox_gnss.c:106: delay(25000);
	mov	dptr,#0x61a8
	lcall	_delay
	C$ublox_gnss.c$109$1$185 ==.
;	..\COMMON\ublox_gnss.c:109: do
	mov	_UBLOX_GPS_SendCommand_WaitACK_k_1_185,#0x00
00106$:
	C$ublox_gnss.c$112$2$189 ==.
;	..\COMMON\ublox_gnss.c:112: *(rx_buffer+k)=uart1_rx();
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_k_1_185
	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	r1,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	r6,a
	lcall	_uart1_rx
	mov	r0,dpl
	mov	dpl,r1
	mov	dph,r6
	mov	a,r0
	movx	@dptr,a
	C$ublox_gnss.c$113$2$189 ==.
;	..\COMMON\ublox_gnss.c:113: k++;
	inc	_UBLOX_GPS_SendCommand_WaitACK_k_1_185
	C$ublox_gnss.c$115$2$189 ==.
;	..\COMMON\ublox_gnss.c:115: delay(2500);
	mov	dptr,#0x09c4
	lcall	_delay
	C$ublox_gnss.c$116$1$185 ==.
;	..\COMMON\ublox_gnss.c:116: }while(uart1_rxcount());
	lcall	_uart1_rxcount
	mov	a,dpl
	jnz	00106$
	C$ublox_gnss.c$120$1$185 ==.
;	..\COMMON\ublox_gnss.c:120: dbglink_writestr("msg rcv\n");
	mov	dptr,#___str_1
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$124$1$185 ==.
;	..\COMMON\ublox_gnss.c:124: if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	movx	a,@dptr
	mov	r6,a
	cjne	r6,#0xb5,00191$
	sjmp	00192$
00191$:
	ljmp	00123$
00192$:
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	inc	dptr
	movx	a,@dptr
	mov	r6,a
	cjne	r6,#0x62,00193$
	sjmp	00194$
00193$:
	ljmp	00123$
00194$:
	C$ublox_gnss.c$126$2$190 ==.
;	..\COMMON\ublox_gnss.c:126: CK_A = 0;
	C$ublox_gnss.c$127$2$190 ==.
;	..\COMMON\ublox_gnss.c:127: CK_B = 0;
	C$ublox_gnss.c$128$2$190 ==.
;	..\COMMON\ublox_gnss.c:128: UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185,a
	mov	_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185,a
	mov	a,#0x02
	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	r1,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	r6,a
	mov	ar0,r1
	mov	ar5,r6
	mov	r7,#0x00
	mov	_UBLOX_GPS_FletcherChecksum8_PARM_2,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),#0x00
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),r7
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2),#0x40
	mov	_UBLOX_GPS_FletcherChecksum8_PARM_3,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),#0x00
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),r7
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2),#0x40
	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,_UBLOX_GPS_SendCommand_WaitACK_k_1_185
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),r7
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
	add	a,#0xfc
	mov	_UBLOX_GPS_FletcherChecksum8_PARM_4,a
	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
	addc	a,#0xff
	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1),a
	mov	dpl,r0
	mov	dph,r5
	mov	b,r7
	push	ar6
	push	ar1
	lcall	_UBLOX_GPS_FletcherChecksum8
	pop	ar1
	pop	ar6
	C$ublox_gnss.c$129$2$190 ==.
;	..\COMMON\ublox_gnss.c:129: if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
	add	a,#0xfe
	mov	r5,a
	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
	addc	a,#0xff
	mov	r7,a
	mov	a,r5
	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dpl,a
	mov	a,r7
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	dph,a
	movx	a,@dptr
	mov	r7,a
	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185,00195$
	sjmp	00196$
00195$:
	ljmp	00123$
00196$:
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
	add	a,#0xff
	mov	r5,a
	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
	addc	a,#0xff
	mov	r7,a
	mov	a,r5
	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dpl,a
	mov	a,r7
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	dph,a
	movx	a,@dptr
	mov	r7,a
	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185,00197$
	sjmp	00198$
00197$:
	ljmp	00123$
00198$:
	C$ublox_gnss.c$132$3$191 ==.
;	..\COMMON\ublox_gnss.c:132: if(!AnsOrAck)
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_5
	jz	00199$
	ljmp	00117$
00199$:
	C$ublox_gnss.c$135$4$192 ==.
;	..\COMMON\ublox_gnss.c:135: if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
	mov	dpl,r1
	mov	dph,r6
	movx	a,@dptr
	mov	r7,a
	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184,00200$
	sjmp	00201$
00200$:
	ljmp	00123$
00201$:
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	inc	dptr
	inc	dptr
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_2,00202$
	sjmp	00203$
00202$:
	ljmp	00123$
00203$:
	C$ublox_gnss.c$137$5$193 ==.
;	..\COMMON\ublox_gnss.c:137: *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_PARM_6
	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1)
	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2)
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	movx	a,@dptr
	mov	r0,a
	mov	r3,#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),r0
;	1-genFromRTrack replaced	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,r3
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	inc	dptr
	inc	dptr
	inc	dptr
	inc	dptr
	movx	a,@dptr
	mov	r2,a
	mov	r3,#0x00
	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
	orl	ar2,a
	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
	orl	ar3,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r3
	lcall	__gptrput
	C$ublox_gnss.c$138$5$193 ==.
;	..\COMMON\ublox_gnss.c:138: memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
	mov	a,#0x06
	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	r2,a
	clr	a
	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	r3,a
	mov	_memcpy_PARM_2,r2
	mov	(_memcpy_PARM_2 + 1),r3
	mov	(_memcpy_PARM_2 + 2),#0x00
	mov	dpl,r4
	mov	dph,r5
	mov	b,r7
	lcall	__gptrget
	mov	_memcpy_PARM_3,a
	inc	dptr
	lcall	__gptrget
	mov	(_memcpy_PARM_3 + 1),a
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
	lcall	_memcpy
	C$ublox_gnss.c$139$5$193 ==.
;	..\COMMON\ublox_gnss.c:139: RetVal = OK;
	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x01
	sjmp	00123$
00117$:
	C$ublox_gnss.c$145$4$194 ==.
;	..\COMMON\ublox_gnss.c:145: if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
	mov	dpl,r1
	mov	dph,r6
	movx	a,@dptr
	mov	r1,a
	cjne	r1,#0x05,00113$
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	inc	dptr
	inc	dptr
	inc	dptr
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x01,00113$
	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x01
	sjmp	00123$
00113$:
	C$ublox_gnss.c$146$4$194 ==.
;	..\COMMON\ublox_gnss.c:146: else RetVal = NAK;
	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x00
00123$:
	C$ublox_gnss.c$150$1$185 ==.
;	..\COMMON\ublox_gnss.c:150: free(buffer_aux);
	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
	mov	r7,#0x00
	mov	dpl,r4
	mov	dph,r5
	mov	b,r7
	lcall	_free
	C$ublox_gnss.c$151$1$185 ==.
;	..\COMMON\ublox_gnss.c:151: free(rx_buffer);
	mov	r2,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
	mov	r3,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
	mov	r7,#0x00
	mov	dpl,r2
	mov	dph,r3
	mov	b,r7
	lcall	_free
	C$ublox_gnss.c$152$1$185 ==.
;	..\COMMON\ublox_gnss.c:152: return RetVal;
	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185
	C$ublox_gnss.c$153$1$185 ==.
	XG$UBLOX_GPS_SendCommand_WaitACK$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'UBLOX_GPS_PortInit'
;------------------------------------------------------------
;UBLOX_setNav1             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196'
;UBLOX_setNav2             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196'
;UBLOX_setNav3             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196'
;UBLOX_setNav5             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196'
;------------------------------------------------------------
	G$UBLOX_GPS_PortInit$0$0 ==.
	C$ublox_gnss.c$160$1$185 ==.
;	..\COMMON\ublox_gnss.c:160: void UBLOX_GPS_PortInit(void)
;	-----------------------------------------
;	 function UBLOX_GPS_PortInit
;	-----------------------------------------
_UBLOX_GPS_PortInit:
	C$ublox_gnss.c$162$1$185 ==.
;	..\COMMON\ublox_gnss.c:162: __xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
	mov	a,#0xf0
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
	mov	a,#0x04
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0002)
	clr	a
	movx	@dptr,a
	C$ublox_gnss.c$163$1$185 ==.
;	..\COMMON\ublox_gnss.c:163: __xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196
	mov	a,#0xf0
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0001)
	mov	a,#0x01
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0002)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0003)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0004)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0005)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0006)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0007)
	inc	a
	movx	@dptr,a
	C$ublox_gnss.c$164$1$185 ==.
;	..\COMMON\ublox_gnss.c:164: __xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196
	mov	a,#0xf0
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0001)
	mov	a,#0x04
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0002)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0003)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0004)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0005)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0006)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0007)
	inc	a
	movx	@dptr,a
	C$ublox_gnss.c$165$1$185 ==.
;	..\COMMON\ublox_gnss.c:165: __xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196
	mov	a,#0xff
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0001)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0002)
	mov	a,#0x06
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0003)
	rr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0004)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0005)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0006)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0007)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0008)
	mov	a,#0x10
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0009)
	mov	a,#0x27
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000a)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000b)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000c)
	mov	a,#0x05
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000d)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000e)
	mov	a,#0xfa
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000f)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0010)
	mov	a,#0xfa
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0011)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0012)
	mov	a,#0x64
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0013)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0014)
	mov	a,#0x2c
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0015)
	mov	a,#0x01
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0016)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0017)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0018)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0019)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001a)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001b)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001c)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001d)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001e)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001f)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0020)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0021)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0022)
	movx	@dptr,a
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0023)
	movx	@dptr,a
	C$ublox_gnss.c$168$1$196 ==.
;	..\COMMON\ublox_gnss.c:168: PORTA_1 = 1;
	setb	_PORTA_1
	C$ublox_gnss.c$169$1$196 ==.
;	..\COMMON\ublox_gnss.c:169: uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
	clr	a
	push	acc
	mov	a,#0x2d
	push	acc
	mov	a,#0x31
	push	acc
	mov	a,#0x01
	push	acc
	rr	a
	push	acc
	mov	a,#0x25
	push	acc
	clr	a
	push	acc
	push	acc
	mov	dpl,#0x00
	lcall	_uart_timer1_baud
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
	C$ublox_gnss.c$170$1$196 ==.
;	..\COMMON\ublox_gnss.c:170: uart1_init(1, 8, 1);
	mov	_uart1_init_PARM_2,#0x08
	mov	_uart1_init_PARM_3,#0x01
	mov	dpl,#0x01
	lcall	_uart1_init
	C$ublox_gnss.c$175$1$196 ==.
;	..\COMMON\ublox_gnss.c:175: dbglink_writestr(" GPS init start\n ");
	mov	dptr,#___str_2
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$177$1$196 ==.
;	..\COMMON\ublox_gnss.c:177: UBLOX_setNav1[1]=0x02;
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
	mov	a,#0x02
	movx	@dptr,a
	C$ublox_gnss.c$178$1$196 ==.
;	..\COMMON\ublox_gnss.c:178: do{
00101$:
	C$ublox_gnss.c$179$2$197 ==.
;	..\COMMON\ublox_gnss.c:179: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$180$1$196 ==.
;	..\COMMON\ublox_gnss.c:180: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00101$
	C$ublox_gnss.c$182$1$196 ==.
;	..\COMMON\ublox_gnss.c:182: dbglink_writestr(" GPS init 1\n ");
	mov	dptr,#___str_3
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$185$1$196 ==.
;	..\COMMON\ublox_gnss.c:185: UBLOX_setNav1[1]=0x01;
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
	mov	a,#0x01
	movx	@dptr,a
	C$ublox_gnss.c$186$1$196 ==.
;	..\COMMON\ublox_gnss.c:186: do{
00104$:
	C$ublox_gnss.c$187$2$198 ==.
;	..\COMMON\ublox_gnss.c:187: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$188$1$196 ==.
;	..\COMMON\ublox_gnss.c:188: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00104$
	C$ublox_gnss.c$191$1$196 ==.
;	..\COMMON\ublox_gnss.c:191: dbglink_writestr(" GPS init 2\n ");
	mov	dptr,#___str_4
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$194$1$196 ==.
;	..\COMMON\ublox_gnss.c:194: UBLOX_setNav1[1]=0x00;
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
	clr	a
	movx	@dptr,a
	C$ublox_gnss.c$195$1$196 ==.
;	..\COMMON\ublox_gnss.c:195: do{
00107$:
	C$ublox_gnss.c$196$2$199 ==.
;	..\COMMON\ublox_gnss.c:196: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$197$1$196 ==.
;	..\COMMON\ublox_gnss.c:197: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00107$
	C$ublox_gnss.c$200$1$196 ==.
;	..\COMMON\ublox_gnss.c:200: dbglink_writestr(" GPS init 3\n ");
	mov	dptr,#___str_5
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$203$1$196 ==.
;	..\COMMON\ublox_gnss.c:203: UBLOX_setNav1[1]=0x03;
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
	mov	a,#0x03
	movx	@dptr,a
	C$ublox_gnss.c$204$1$196 ==.
;	..\COMMON\ublox_gnss.c:204: do{
00110$:
	C$ublox_gnss.c$205$2$200 ==.
;	..\COMMON\ublox_gnss.c:205: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$206$1$196 ==.
;	..\COMMON\ublox_gnss.c:206: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00110$
	C$ublox_gnss.c$209$1$196 ==.
;	..\COMMON\ublox_gnss.c:209: dbglink_writestr(" GPS init 4\n ");
	mov	dptr,#___str_6
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$212$1$196 ==.
;	..\COMMON\ublox_gnss.c:212: UBLOX_setNav1[1]=0x05;
	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
	mov	a,#0x05
	movx	@dptr,a
	C$ublox_gnss.c$213$1$196 ==.
;	..\COMMON\ublox_gnss.c:213: do{
00113$:
	C$ublox_gnss.c$214$2$201 ==.
;	..\COMMON\ublox_gnss.c:214: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$215$1$196 ==.
;	..\COMMON\ublox_gnss.c:215: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00113$
	C$ublox_gnss.c$217$1$196 ==.
;	..\COMMON\ublox_gnss.c:217: do{
00116$:
	C$ublox_gnss.c$218$2$202 ==.
;	..\COMMON\ublox_gnss.c:218: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$219$1$196 ==.
;	..\COMMON\ublox_gnss.c:219: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav2,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x08
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00116$
	C$ublox_gnss.c$221$1$196 ==.
;	..\COMMON\ublox_gnss.c:221: do{
00119$:
	C$ublox_gnss.c$222$2$203 ==.
;	..\COMMON\ublox_gnss.c:222: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$223$1$196 ==.
;	..\COMMON\ublox_gnss.c:223: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav3,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x08
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00119$
	C$ublox_gnss.c$225$1$196 ==.
;	..\COMMON\ublox_gnss.c:225: do{
00122$:
	C$ublox_gnss.c$226$2$204 ==.
;	..\COMMON\ublox_gnss.c:226: delay(5000);
	mov	dptr,#0x1388
	lcall	_delay
	C$ublox_gnss.c$227$1$196 ==.
;	..\COMMON\ublox_gnss.c:227: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x24,sizeof(UBLOX_setNav5),UBLOX_setNav5,ACK,NULL));
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 >> 8)
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x24
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x24
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
	clr	a
	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
	mov	dpl,#0x06
	lcall	_UBLOX_GPS_SendCommand_WaitACK
	mov	a,dpl
	jz	00122$
	C$ublox_gnss.c$229$1$196 ==.
;	..\COMMON\ublox_gnss.c:229: dbglink_writestr("GPS init END\n");
	mov	dptr,#___str_7
	mov	b,#0x80
	lcall	_dbglink_writestr
	C$ublox_gnss.c$231$1$196 ==.
	XG$UBLOX_GPS_PortInit$0$0 ==.
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
Fublox_gnss$__str_0$0$0 == .
___str_0:
	.ascii "command sent"
	.db 0x0a
	.db 0x00
Fublox_gnss$__str_1$0$0 == .
___str_1:
	.ascii "msg rcv"
	.db 0x0a
	.db 0x00
Fublox_gnss$__str_2$0$0 == .
___str_2:
	.ascii " GPS init start"
	.db 0x0a
	.ascii " "
	.db 0x00
Fublox_gnss$__str_3$0$0 == .
___str_3:
	.ascii " GPS init 1"
	.db 0x0a
	.ascii " "
	.db 0x00
Fublox_gnss$__str_4$0$0 == .
___str_4:
	.ascii " GPS init 2"
	.db 0x0a
	.ascii " "
	.db 0x00
Fublox_gnss$__str_5$0$0 == .
___str_5:
	.ascii " GPS init 3"
	.db 0x0a
	.ascii " "
	.db 0x00
Fublox_gnss$__str_6$0$0 == .
___str_6:
	.ascii " GPS init 4"
	.db 0x0a
	.ascii " "
	.db 0x00
Fublox_gnss$__str_7$0$0 == .
___str_7:
	.ascii "GPS init END"
	.db 0x0a
	.db 0x00
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
