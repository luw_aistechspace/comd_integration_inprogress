// libraries
#include "../AX_Radio_Lab_output/configslave.h"
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include "libmfosc.h"
#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#include "../COMMON/display_com0.h"
#include <libminikitleds.h>
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"
#include "../COMMON/easyax5043.h"
#include "../COMMON/pktframing.h"

// macros
#define USE_DBGLINK

// functions

// variables
extern uint8_t __xdata randomSet[nRANDOM];
extern uint8_t __xdata demoPacketBuffer[pktBufferLENGTH];
struct wtimer_desc __xdata msg1_tmr;
struct wtimer_desc __xdata msg2_tmr;
struct wtimer_desc __xdata msg3_tmr;


/**
*\brief using wtimer functions to regulate time intervals
*/

void msg1_callback(struct wtimer_desc __xdata *desc)
{
    desc;
/*#ifdef USE_DBGLINK
        dbglink_writestr("msg1_tx\n");
#endif // USE_DBGLINK*/
    axradio_transmit(&remoteaddr, demoPacketBuffer, pktLENGTH);
#ifdef USE_DBGLINK
            dbglink_writestr("6_TX1: ");
            dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
            dbglink_tx('\n');
#endif // USE_DBGLINK

}
void msg2_callback(struct wtimer_desc __xdata *desc)
{
    desc;
    axradio_transmit(&remoteaddr, demoPacketBuffer + pktLENGTH, pktLENGTH);
#ifdef USE_DBGLINK
            dbglink_writestr("7_TX2: ");
            dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
            dbglink_tx('\n');
#endif // USE_DBGLINK

}
void msg3_callback(struct wtimer_desc __xdata *desc)
{
    desc;
    axradio_transmit(&remoteaddr, demoPacketBuffer + (2*pktLENGTH), pktLENGTH);
#ifdef USE_DBGLINK
            dbglink_writestr("8_TX3: ");
            dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
            dbglink_tx('\n');
#endif // USE_DBGLINK

}
void transmit_copies_wtimer(void)
{
    uint32_t __xdata time1_wtimer, time2_wtimer, time3_wtimer;

//    randomset_generate();
//    simple_framing();

    time1_wtimer = randomSet[0] * slotStdWIDTH_MS * 0.64; // warnings of overflow
    time2_wtimer = randomSet[1] * slotStdWIDTH_MS * 0.64;
    time3_wtimer = randomSet[2] * slotStdWIDTH_MS * 0.64;

/*#ifdef USE_DBGLINK
    dbglink_writehex16(time1_wtimer, 4, WRNUM_PADZERO);
    dbglink_writestr(" ");
    dbglink_writehex16(time2_wtimer, 4, WRNUM_PADZERO);
    dbglink_writestr(" ");
    dbglink_writehex16(time3_wtimer, 4, WRNUM_PADZERO);
    dbglink_tx('\n');
#endif // USE_DBGLINK*/

/*    radio_write8(AX5043_REG_PWRMODE, AX5043_PWRSTATE_XTAL_ON);
	radio_write8(AX5043_REG_PWRMODE, AX5043_PWRSTATE_FIFO_ON);
	radio_write8(AX5043_REG_PWRMODE, AX5043_PWRSTATE_FULL_TX); */

    wtimer0_remove(&msg1_tmr);
    msg1_tmr.handler = msg1_callback;
    msg1_tmr.time = time1_wtimer;
    wtimer0_addrelative(&msg1_tmr);

    wtimer0_remove(&msg2_tmr);
    msg2_tmr.handler = msg2_callback;
    msg2_tmr.time = time2_wtimer;
    wtimer0_addrelative(&msg2_tmr);

    wtimer0_remove(&msg3_tmr);
    msg3_tmr.handler = msg3_callback;
    msg3_tmr.time = time3_wtimer;
    wtimer0_addrelative(&msg3_tmr);

 /*    delay_ms(50);
     radio_write8(AX5043_REG_PWRMODE, AX5043_PWRSTATE_XTAL_ON);
	 radio_write8(AX5043_REG_PWRMODE, AX5043_PWRSTATE_FIFO_ON);
    radio_write8(AX5043_REG_PWRMODE, AX5043_PWRSTATE_FULL_RX);*/
}
