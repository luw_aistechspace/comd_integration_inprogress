/**
*******************************************************************************************************
* @file MASTER\main.c
* @brief Code skeleton for MASTER module, illustrating periodic or on-demand transmission of packets.
*        The packet format is determined by AX-RadioLAB_output\config.c, produced by the AX-RadioLab GUI
* @internal
* @author   Thomas Sailer, Janani Chellappan, Srinivasan Tamilarasan, Pradeep Kumar GR
* $Rev: $
* $Date: $
********************************************************************************************************
* Copyright 2016 Semiconductor Components Industries LLC (d/b/a �ON Semiconductor�).
* All rights reserved.  This software and/or documentation is licensed by ON Semiconductor
* under limited terms and conditions.  The terms and conditions pertaining to the software
* and/or documentation are available at http://www.onsemi.com/site/pdf/ONSEMI_T&C.pdf
* (�ON Semiconductor Standard Terms and Conditions of Sale, Section 8 Software�) and
* if applicable the software license agreement.  Do not use this software and/or
* documentation unless you have carefully read and you agree to the limited terms and
* conditions.  By using this software and/or documentation, you agree to the limited
* terms and conditions.
*
* THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
* ON SEMICONDUCTOR SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL,
* INCIDENTAL, OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
* @endinternal
*
* @ingroup TEMPLATE_FIRMWARE_504x
*
* @details
*/

#include "../AX_Radio_Lab_output/configmaster.h"
/* axm0_uart req for both; remove warnings */
#include <ax8052.h>
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include "libmfosc.h"
#ifdef USE_COM0
#include <libmfuart0.h>
#endif /* USE_COM0 */
#ifdef USE_LCD
#include <libaxlcd2.h>
#endif /* USE_LCD */
#ifdef USE_DBGLINK
#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#endif /* USE_DBGLINK */
#if defined(USE_LCD) || defined(USE_COM0)
#define USE_DISPLAY
#endif /* defined(USE_LCD) || defined(USE_COM0) */
#include "../COMMON/display_com0.h"
#include <libminikitleds.h>
#define BUTTON_INTCHG INTCHGC
#define BUTTON_PIN    PINC
#define BUTTON_MASK   0x10
#include <string.h>
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"

//#define RADIO_MODE AXRADIO_MODE_ASYNC_RECEIVE
#define UPLINK_rcvpkt_MS 7000
#define FXTAL   48000000
//#define FREQOFFS_K  3
#define WTIMER0_1sec 0x280
#define WTIMER0_UPLINKRCV (WTIMER0_1sec*7)

// function declarations
extern void wait_dbglink_free();
static void transmit_beacon(void);
static void transmit_ack(void);

// variable declarations
extern uint8_t _start__stack[];
uint16_t __data pkt_counter = 0;
uint8_t __data coldstart = 1; /* caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */

uint8_t __xdata beacon_packet[8] = {0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66};
uint8_t __xdata ack_packet[4] = {0x00, 0x77, 0x88, 0x99};
uint16_t __xdata pkts_received = 0;
uint16_t __xdata pkts_missing = 0;
uint16_t __xdata bc_counter = 0;
uint8_t __xdata rcv_flg = 0;
uint16_t __xdata pkts_id1 = 0; // accumulated number of received packets from terminal ID1
uint16_t __xdata pkts_id3 = 0; // accumulated number of received packets from terminal ID3
uint8_t __xdata rcv_flg_id1 = 0; // number of received packets from terminal ID1 per beacon
uint8_t __xdata rcv_flg_id3 = 0; // number of received packets from terminal ID3 per beacon


struct wtimer_desc __xdata wakeup_desc;


static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
{
    uint8_t pc = PCON;

    if (!(pc & 0x80))
        return;

    GPIOENABLE = 0;
    IE = EIE = E2IE = 0;

    for (;;)
        PCON |= 0x01;
}

static void transmit_packet(void)
{
    static uint8_t __xdata demo_packet_[sizeof(demo_packet)];

    ++pkt_counter;
    memcpy(demo_packet_, demo_packet, sizeof(demo_packet));

    if (framing_insert_counter)
    {
        demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
        demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
    }

    axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));
}

/*static void wait_dbglink_free(void)
{
    for (;;) {
        if (dbglink_txfree() >= 56)
            break;
        wtimer_runcallbacks();
        wtimer_idle(WTFLAG_CANSTANDBY);
    }
}*/

/*
 *  Transmit the beacon packet that contains a counter.
 *  Display the beacon counter in debug message.
 */
static void transmit_beacon(void)
{
     static uint8_t __xdata beacon_packet_[sizeof(beacon_packet)];
 //   uint8_t i;

 //    ax5043_prepare_tx();
     ++bc_counter;

     memcpy(beacon_packet_, beacon_packet, sizeof(beacon_packet));

     if (framing_insert_counter)
     {
        beacon_packet_[framing_counter_pos] = bc_counter & 0xFF;
        beacon_packet_[framing_counter_pos + 1] = (bc_counter >> 8) & 0xFF;
     }
     axradio_transmit(&remoteaddr, beacon_packet_, sizeof(beacon_packet));

#ifdef USE_DBGLINK
        dbglink_writestr("Beacon: ");
        dbglink_writehex16(bc_counter, 4, WRNUM_PADZERO);
        dbglink_writestr("  ");
//        dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
        dbglink_writestr("\n");
#endif // USE_DBGLINK
 }
 /***************************/

 /*
  *  Transmit ACK packet and display the counter in debug message
  *  More functions to be included.
  *  Add one more byte to denote data packet source (terminal ID number)
  */
static void transmit_ack(void)
{
    static uint8_t __xdata ack_packet_[sizeof(ack_packet)];

    memcpy(ack_packet_, ack_packet, sizeof(ack_packet));
    if(rcv_flg_id1 > 0)
    {
        ack_packet_[0] = (ack_packet[0] & (~(1 << 0)))| (1 << 0);
    }
    if(rcv_flg_id3 > 0)
    {
        ack_packet_[0] = (ack_packet[0] & (~(1 << 2)))| (1 << 2);
    }
    axradio_transmit(&remoteaddr, ack_packet_, sizeof(ack_packet));

#ifdef USE_DBGLINK
       wait_dbglink_free();
        dbglink_writestr("ACK: ");
        dbglink_writehex16(rcv_flg, 2, WRNUM_SIGNED);
        dbglink_writestr(" ");
        dbglink_writehex16(pkts_received, 4, WRNUM_PADZERO);
        dbglink_tx('\n');
        dbglink_writestr("ID1: ");
        dbglink_writehex16(rcv_flg_id1, 2, WRNUM_SIGNED);
        dbglink_writestr(" ");
        dbglink_writehex16(pkts_id1, 4, WRNUM_PADZERO);
        dbglink_writestr("\nID3: ");

        dbglink_writehex16(rcv_flg_id3, 2, WRNUM_SIGNED);
        dbglink_writestr(" ");
        dbglink_writehex16(pkts_id3, 4, WRNUM_PADZERO);

        dbglink_writestr("\n\n");
#endif // USE_DBGLINK
}
/***************************/

/*static void display_transmit_packet(void)
{
    if (pkt_counter == 1)
    {
        display_setpos(0x40);
        display_writestr("TX    ");
#ifdef USE_DBGLINK

        if (DBGLNKSTAT & 0x10)
            dbglink_writestr("TX : \n");

#endif // USE_DBGLINK
    }

    display_setpos(0x4c);
    display_writehex16(pkt_counter, 4, WRNUM_PADZERO);
#ifdef USE_DBGLINK

    if (DBGLNKSTAT & 0x10)
    {
        dbglink_writehex16(pkt_counter, 4, WRNUM_PADZERO);
        dbglink_tx('\n');
    }

#endif // USE_DBGLINK
}*/

void axradio_statuschange(struct axradio_status __xdata *st)
{
#if defined(USE_DBGLINK) && defined(DEBUGMSG)
    if (DBGLNKSTAT & 0x10)
    {
        wait_dbglink_free();
        dbglink_writestr("ST: 0x");
        dbglink_writehex16(st->status, 2, WRNUM_PADZERO);
        dbglink_writestr(" ERR: 0x");
        dbglink_writehex16(st->error, 2, WRNUM_PADZERO);
        dbglink_tx('\n');
    }

#endif
    switch (st->status)
    {
    case AXRADIO_STAT_TRANSMITSTART:
        led0_on();
        break;

    case AXRADIO_STAT_TRANSMITEND:
        led0_off();
        break;

    case AXRADIO_STAT_CHANNELSTATE:
        break;

    case AXRADIO_STAT_RECEIVE:
    {

        const uint8_t __xdata *pktdata = st->u.rx.mac.raw;

        if (st->error == AXRADIO_ERR_NOERROR)
        {
// to be added later            if (pktdata[axradio_framing_maclen] == 0x)
            ++pkts_received;
            rcv_flg += 1;
#ifdef USE_DBGLINK
                dbglink_writestr("pkt T0 ");
                dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
                dbglink_tx('\n');
                dbglink_received_packet(st);
#endif // USE_DBGLINK
        }
/*        else
        {
                //rcv_flg = 0;
#ifdef USE_DBGLINK
            dbglink_writestr("Invalid packet.\n\n");
#endif // USE_DBGLINK
        } */
        break;
    }
    default:
        break;
    }
}

void enable_radio_interrupt_in_mcu_pin(void)
{
	IE_4 = 1;
}

void disable_radio_interrupt_in_mcu_pin(void)
{
	IE_4 = 0;
}

static void wakeup_callback(struct wtimer_desc __xdata *desc)
{
    static uint16_t __xdata timer0Period;
    static uint8_t __xdata bc_flg;
    desc;

#ifdef USE_DBGLINK
        dbglink_writestr("TX time: ");
        dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK
    if ((rcv_flg == 0)&&(bc_flg == 0))
    {
        timer0Period = WTIMER0_UPLINKRCV;
        transmit_beacon();
        bc_flg = 1;
    }
    else if ((rcv_flg >=1)&&(bc_flg == 1)) // received data packets from COM-D terminals
    {
        timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
        //transmit_ack();
        rcv_flg = 0;
        bc_flg = 0;
        rcv_flg_id1 = 0;
        rcv_flg_id3 = 0;
    }
    else if ((rcv_flg == 0)&&(bc_flg == 1))
    {
        timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
        bc_flg = 0;
    }
    wakeup_desc.time += wtimer0_correctinterval(timer0Period);
    wtimer0_addabsolute(&wakeup_desc);
}

uint8_t _sdcc_external_startup(void)
{
    LPXOSCGM = 0x8A;
    wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
    wtimer1_setclksrc(CLKSRC_FRCOSC, 7);

    LPOSCCONFIG = 0x09; /* Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() ) */

    coldstart = !(PCON & 0x40);

    ANALOGA = 0x18; /* PA[3,4] LPXOSC, other PA are used as digital pins */
    PORTA = 0xE7; /* pull ups except for LPXOSC pin PA[3,4]; */
    PORTB = 0xFD | (PINB & 0x02); /* init LEDs to previous (frozen) state */
    PORTC = 0xFF;
    PORTR = 0x0B;

    DIRA = 0x00;
    DIRB = 0x0e; /*  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used) */
    DIRC = 0x00; /*  PC4 = button */
    DIRR = 0x15;
    axradio_setup_pincfg1();
    DPS = 0;
    IE = 0x40;
    EIE = 0x00;
    E2IE = 0x00;

    display_portinit();
    GPIOENABLE = 1; /* unfreeze GPIO */
    return !coldstart; /* coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization */
}

int main(void)
{
    uint8_t i;
    static uint8_t __data saved_button_state = 0xFF;
    __asm
    G$_start__stack$0$0 = __start__stack
                          .globl G$_start__stack$0$0
                          __endasm;
#ifdef USE_DBGLINK
    dbglink_init();
#endif
    __enable_irq();
    flash_apply_calibration();
    CLKCON = 0x00;
    wtimer_init();

    if (coldstart)
    {
        led0_off();

        wakeup_desc.handler = wakeup_callback;

        display_init();
        display_setpos(0);
        i = axradio_init();

        if (i != AXRADIO_ERR_NOERROR)
        {
            if (i == AXRADIO_ERR_NOCHIP)
            {
                display_writestr(radio_not_found_lcd_display);
#ifdef USE_DBGLINK

                if(DBGLNKSTAT & 0x10)
                    dbglink_writestr(radio_not_found_lcd_display);

#endif /* USE_DBGLINK */
                goto terminate_error;
            }

            goto terminate_radio_error;
        }

        display_writestr(radio_lcd_display);
#ifdef USE_DBGLINK

        if (DBGLNKSTAT & 0x10)
            dbglink_writestr(radio_lcd_display);

#endif /* USE_DBGLINK */
        axradio_set_local_address(&localaddr);
        axradio_set_default_remote_address(&remoteaddr);

#ifdef USE_DBGLINK

        if (DBGLNKSTAT & 0x10)
        {
            dbglink_writestr("RNG = ");
            dbglink_writenum16(axradio_get_pllrange(), 2, 0);
            {
                uint8_t x = axradio_get_pllvcoi();

                if (x & 0x80)
                {
                    dbglink_writestr("\nVCOI = ");
                    dbglink_writehex16(x, 2, 0);
                }
            }
            dbglink_writestr("\n\nMASTER\n");
        }

#endif /* USE_DBGLINK */
        i = axradio_set_mode(AXRADIO_MODE_ASYNC_RECEIVE);

        if (i != AXRADIO_ERR_NOERROR)
            goto terminate_radio_error;

#if defined(WTIMER0_PERIOD)
        wakeup_desc.time = wtimer0_correctinterval(WTIMER0_PERIOD);
        wtimer0_addrelative(&wakeup_desc);
#endif
    }
    else
    {
        /*  Warm Start */
        axradio_commsleepexit();
        IE_4 = 1; /* enable radio interrupt */
    }

    axradio_setup_pincfg2();

    for(;;)
    {
        wtimer_runcallbacks();
        __disable_irq();
        {
            uint8_t flg = WTFLAG_CANSTANDBY;
#ifdef MCU_SLEEP

            if (axradio_cansleep()
#ifdef USE_DBGLINK
                    && dbglink_txidle()
#endif
                    && display_txidle())
                flg |= WTFLAG_CANSLEEP;
#endif /* MCU_SLEEP */
            wtimer_idle(flg);
        }
        IE_3 = 0; /* no ISR! */
        __enable_irq();
    }

terminate_radio_error:
    display_radio_error(i);
#ifdef USE_DBGLINK
    dbglink_display_radio_error(i);
#endif /* USE_DBGLINK */
terminate_error:

    for (;;)
    {
        wtimer_runcallbacks();
        {
            uint8_t flg = WTFLAG_CANSTANDBY;
#ifdef MCU_SLEEP

            if (axradio_cansleep()
#ifdef USE_DBGLINK
                    && dbglink_txidle()
#endif
                    && display_txidle())
                flg |= WTFLAG_CANSLEEP;
#endif
            wtimer_idle(flg);
        }
    }
}
