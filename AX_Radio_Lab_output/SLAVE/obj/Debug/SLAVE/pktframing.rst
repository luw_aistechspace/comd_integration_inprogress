                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module pktframing
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _swap_variables_PARM_2
                                     12 	.globl _GOLDSEQUENCE_Obtain
                                     13 	.globl _memcpy
                                     14 	.globl _delay_ms
                                     15 	.globl _crc_crc32_msb
                                     16 	.globl _dbglink_writehex32
                                     17 	.globl _dbglink_writehex16
                                     18 	.globl _dbglink_writenum16
                                     19 	.globl _dbglink_writestr
                                     20 	.globl _dbglink_tx
                                     21 	.globl _wtimer0_curtime
                                     22 	.globl _PORTC_7
                                     23 	.globl _PORTC_6
                                     24 	.globl _PORTC_5
                                     25 	.globl _PORTC_4
                                     26 	.globl _PORTC_3
                                     27 	.globl _PORTC_2
                                     28 	.globl _PORTC_1
                                     29 	.globl _PORTC_0
                                     30 	.globl _PORTB_7
                                     31 	.globl _PORTB_6
                                     32 	.globl _PORTB_5
                                     33 	.globl _PORTB_4
                                     34 	.globl _PORTB_3
                                     35 	.globl _PORTB_2
                                     36 	.globl _PORTB_1
                                     37 	.globl _PORTB_0
                                     38 	.globl _PORTA_7
                                     39 	.globl _PORTA_6
                                     40 	.globl _PORTA_5
                                     41 	.globl _PORTA_4
                                     42 	.globl _PORTA_3
                                     43 	.globl _PORTA_2
                                     44 	.globl _PORTA_1
                                     45 	.globl _PORTA_0
                                     46 	.globl _PINC_7
                                     47 	.globl _PINC_6
                                     48 	.globl _PINC_5
                                     49 	.globl _PINC_4
                                     50 	.globl _PINC_3
                                     51 	.globl _PINC_2
                                     52 	.globl _PINC_1
                                     53 	.globl _PINC_0
                                     54 	.globl _PINB_7
                                     55 	.globl _PINB_6
                                     56 	.globl _PINB_5
                                     57 	.globl _PINB_4
                                     58 	.globl _PINB_3
                                     59 	.globl _PINB_2
                                     60 	.globl _PINB_1
                                     61 	.globl _PINB_0
                                     62 	.globl _PINA_7
                                     63 	.globl _PINA_6
                                     64 	.globl _PINA_5
                                     65 	.globl _PINA_4
                                     66 	.globl _PINA_3
                                     67 	.globl _PINA_2
                                     68 	.globl _PINA_1
                                     69 	.globl _PINA_0
                                     70 	.globl _CY
                                     71 	.globl _AC
                                     72 	.globl _F0
                                     73 	.globl _RS1
                                     74 	.globl _RS0
                                     75 	.globl _OV
                                     76 	.globl _F1
                                     77 	.globl _P
                                     78 	.globl _IP_7
                                     79 	.globl _IP_6
                                     80 	.globl _IP_5
                                     81 	.globl _IP_4
                                     82 	.globl _IP_3
                                     83 	.globl _IP_2
                                     84 	.globl _IP_1
                                     85 	.globl _IP_0
                                     86 	.globl _EA
                                     87 	.globl _IE_7
                                     88 	.globl _IE_6
                                     89 	.globl _IE_5
                                     90 	.globl _IE_4
                                     91 	.globl _IE_3
                                     92 	.globl _IE_2
                                     93 	.globl _IE_1
                                     94 	.globl _IE_0
                                     95 	.globl _EIP_7
                                     96 	.globl _EIP_6
                                     97 	.globl _EIP_5
                                     98 	.globl _EIP_4
                                     99 	.globl _EIP_3
                                    100 	.globl _EIP_2
                                    101 	.globl _EIP_1
                                    102 	.globl _EIP_0
                                    103 	.globl _EIE_7
                                    104 	.globl _EIE_6
                                    105 	.globl _EIE_5
                                    106 	.globl _EIE_4
                                    107 	.globl _EIE_3
                                    108 	.globl _EIE_2
                                    109 	.globl _EIE_1
                                    110 	.globl _EIE_0
                                    111 	.globl _E2IP_7
                                    112 	.globl _E2IP_6
                                    113 	.globl _E2IP_5
                                    114 	.globl _E2IP_4
                                    115 	.globl _E2IP_3
                                    116 	.globl _E2IP_2
                                    117 	.globl _E2IP_1
                                    118 	.globl _E2IP_0
                                    119 	.globl _E2IE_7
                                    120 	.globl _E2IE_6
                                    121 	.globl _E2IE_5
                                    122 	.globl _E2IE_4
                                    123 	.globl _E2IE_3
                                    124 	.globl _E2IE_2
                                    125 	.globl _E2IE_1
                                    126 	.globl _E2IE_0
                                    127 	.globl _B_7
                                    128 	.globl _B_6
                                    129 	.globl _B_5
                                    130 	.globl _B_4
                                    131 	.globl _B_3
                                    132 	.globl _B_2
                                    133 	.globl _B_1
                                    134 	.globl _B_0
                                    135 	.globl _ACC_7
                                    136 	.globl _ACC_6
                                    137 	.globl _ACC_5
                                    138 	.globl _ACC_4
                                    139 	.globl _ACC_3
                                    140 	.globl _ACC_2
                                    141 	.globl _ACC_1
                                    142 	.globl _ACC_0
                                    143 	.globl _WTSTAT
                                    144 	.globl _WTIRQEN
                                    145 	.globl _WTEVTD
                                    146 	.globl _WTEVTD1
                                    147 	.globl _WTEVTD0
                                    148 	.globl _WTEVTC
                                    149 	.globl _WTEVTC1
                                    150 	.globl _WTEVTC0
                                    151 	.globl _WTEVTB
                                    152 	.globl _WTEVTB1
                                    153 	.globl _WTEVTB0
                                    154 	.globl _WTEVTA
                                    155 	.globl _WTEVTA1
                                    156 	.globl _WTEVTA0
                                    157 	.globl _WTCNTR1
                                    158 	.globl _WTCNTB
                                    159 	.globl _WTCNTB1
                                    160 	.globl _WTCNTB0
                                    161 	.globl _WTCNTA
                                    162 	.globl _WTCNTA1
                                    163 	.globl _WTCNTA0
                                    164 	.globl _WTCFGB
                                    165 	.globl _WTCFGA
                                    166 	.globl _WDTRESET
                                    167 	.globl _WDTCFG
                                    168 	.globl _U1STATUS
                                    169 	.globl _U1SHREG
                                    170 	.globl _U1MODE
                                    171 	.globl _U1CTRL
                                    172 	.globl _U0STATUS
                                    173 	.globl _U0SHREG
                                    174 	.globl _U0MODE
                                    175 	.globl _U0CTRL
                                    176 	.globl _T2STATUS
                                    177 	.globl _T2PERIOD
                                    178 	.globl _T2PERIOD1
                                    179 	.globl _T2PERIOD0
                                    180 	.globl _T2MODE
                                    181 	.globl _T2CNT
                                    182 	.globl _T2CNT1
                                    183 	.globl _T2CNT0
                                    184 	.globl _T2CLKSRC
                                    185 	.globl _T1STATUS
                                    186 	.globl _T1PERIOD
                                    187 	.globl _T1PERIOD1
                                    188 	.globl _T1PERIOD0
                                    189 	.globl _T1MODE
                                    190 	.globl _T1CNT
                                    191 	.globl _T1CNT1
                                    192 	.globl _T1CNT0
                                    193 	.globl _T1CLKSRC
                                    194 	.globl _T0STATUS
                                    195 	.globl _T0PERIOD
                                    196 	.globl _T0PERIOD1
                                    197 	.globl _T0PERIOD0
                                    198 	.globl _T0MODE
                                    199 	.globl _T0CNT
                                    200 	.globl _T0CNT1
                                    201 	.globl _T0CNT0
                                    202 	.globl _T0CLKSRC
                                    203 	.globl _SPSTATUS
                                    204 	.globl _SPSHREG
                                    205 	.globl _SPMODE
                                    206 	.globl _SPCLKSRC
                                    207 	.globl _RADIOSTAT
                                    208 	.globl _RADIOSTAT1
                                    209 	.globl _RADIOSTAT0
                                    210 	.globl _RADIODATA
                                    211 	.globl _RADIODATA3
                                    212 	.globl _RADIODATA2
                                    213 	.globl _RADIODATA1
                                    214 	.globl _RADIODATA0
                                    215 	.globl _RADIOADDR
                                    216 	.globl _RADIOADDR1
                                    217 	.globl _RADIOADDR0
                                    218 	.globl _RADIOACC
                                    219 	.globl _OC1STATUS
                                    220 	.globl _OC1PIN
                                    221 	.globl _OC1MODE
                                    222 	.globl _OC1COMP
                                    223 	.globl _OC1COMP1
                                    224 	.globl _OC1COMP0
                                    225 	.globl _OC0STATUS
                                    226 	.globl _OC0PIN
                                    227 	.globl _OC0MODE
                                    228 	.globl _OC0COMP
                                    229 	.globl _OC0COMP1
                                    230 	.globl _OC0COMP0
                                    231 	.globl _NVSTATUS
                                    232 	.globl _NVKEY
                                    233 	.globl _NVDATA
                                    234 	.globl _NVDATA1
                                    235 	.globl _NVDATA0
                                    236 	.globl _NVADDR
                                    237 	.globl _NVADDR1
                                    238 	.globl _NVADDR0
                                    239 	.globl _IC1STATUS
                                    240 	.globl _IC1MODE
                                    241 	.globl _IC1CAPT
                                    242 	.globl _IC1CAPT1
                                    243 	.globl _IC1CAPT0
                                    244 	.globl _IC0STATUS
                                    245 	.globl _IC0MODE
                                    246 	.globl _IC0CAPT
                                    247 	.globl _IC0CAPT1
                                    248 	.globl _IC0CAPT0
                                    249 	.globl _PORTR
                                    250 	.globl _PORTC
                                    251 	.globl _PORTB
                                    252 	.globl _PORTA
                                    253 	.globl _PINR
                                    254 	.globl _PINC
                                    255 	.globl _PINB
                                    256 	.globl _PINA
                                    257 	.globl _DIRR
                                    258 	.globl _DIRC
                                    259 	.globl _DIRB
                                    260 	.globl _DIRA
                                    261 	.globl _DBGLNKSTAT
                                    262 	.globl _DBGLNKBUF
                                    263 	.globl _CODECONFIG
                                    264 	.globl _CLKSTAT
                                    265 	.globl _CLKCON
                                    266 	.globl _ANALOGCOMP
                                    267 	.globl _ADCCONV
                                    268 	.globl _ADCCLKSRC
                                    269 	.globl _ADCCH3CONFIG
                                    270 	.globl _ADCCH2CONFIG
                                    271 	.globl _ADCCH1CONFIG
                                    272 	.globl _ADCCH0CONFIG
                                    273 	.globl __XPAGE
                                    274 	.globl _XPAGE
                                    275 	.globl _SP
                                    276 	.globl _PSW
                                    277 	.globl _PCON
                                    278 	.globl _IP
                                    279 	.globl _IE
                                    280 	.globl _EIP
                                    281 	.globl _EIE
                                    282 	.globl _E2IP
                                    283 	.globl _E2IE
                                    284 	.globl _DPS
                                    285 	.globl _DPTR1
                                    286 	.globl _DPTR0
                                    287 	.globl _DPL1
                                    288 	.globl _DPL
                                    289 	.globl _DPH1
                                    290 	.globl _DPH
                                    291 	.globl _B
                                    292 	.globl _ACC
                                    293 	.globl _dummyHMAC
                                    294 	.globl _trmID
                                    295 	.globl _msg3_tmr
                                    296 	.globl _msg2_tmr
                                    297 	.globl _msg1_tmr
                                    298 	.globl _demoPacketBuffer
                                    299 	.globl _siblingSlots
                                    300 	.globl _randomSet
                                    301 	.globl _RNGMODE
                                    302 	.globl _RNGCLKSRC1
                                    303 	.globl _RNGCLKSRC0
                                    304 	.globl _RNGBYTE
                                    305 	.globl _AESOUTADDR
                                    306 	.globl _AESOUTADDR1
                                    307 	.globl _AESOUTADDR0
                                    308 	.globl _AESMODE
                                    309 	.globl _AESKEYADDR
                                    310 	.globl _AESKEYADDR1
                                    311 	.globl _AESKEYADDR0
                                    312 	.globl _AESINADDR
                                    313 	.globl _AESINADDR1
                                    314 	.globl _AESINADDR0
                                    315 	.globl _AESCURBLOCK
                                    316 	.globl _AESCONFIG
                                    317 	.globl _AX5043_TIMEGAIN3NB
                                    318 	.globl _AX5043_TIMEGAIN2NB
                                    319 	.globl _AX5043_TIMEGAIN1NB
                                    320 	.globl _AX5043_TIMEGAIN0NB
                                    321 	.globl _AX5043_RXPARAMSETSNB
                                    322 	.globl _AX5043_RXPARAMCURSETNB
                                    323 	.globl _AX5043_PKTMAXLENNB
                                    324 	.globl _AX5043_PKTLENOFFSETNB
                                    325 	.globl _AX5043_PKTLENCFGNB
                                    326 	.globl _AX5043_PKTADDRMASK3NB
                                    327 	.globl _AX5043_PKTADDRMASK2NB
                                    328 	.globl _AX5043_PKTADDRMASK1NB
                                    329 	.globl _AX5043_PKTADDRMASK0NB
                                    330 	.globl _AX5043_PKTADDRCFGNB
                                    331 	.globl _AX5043_PKTADDR3NB
                                    332 	.globl _AX5043_PKTADDR2NB
                                    333 	.globl _AX5043_PKTADDR1NB
                                    334 	.globl _AX5043_PKTADDR0NB
                                    335 	.globl _AX5043_PHASEGAIN3NB
                                    336 	.globl _AX5043_PHASEGAIN2NB
                                    337 	.globl _AX5043_PHASEGAIN1NB
                                    338 	.globl _AX5043_PHASEGAIN0NB
                                    339 	.globl _AX5043_FREQUENCYLEAKNB
                                    340 	.globl _AX5043_FREQUENCYGAIND3NB
                                    341 	.globl _AX5043_FREQUENCYGAIND2NB
                                    342 	.globl _AX5043_FREQUENCYGAIND1NB
                                    343 	.globl _AX5043_FREQUENCYGAIND0NB
                                    344 	.globl _AX5043_FREQUENCYGAINC3NB
                                    345 	.globl _AX5043_FREQUENCYGAINC2NB
                                    346 	.globl _AX5043_FREQUENCYGAINC1NB
                                    347 	.globl _AX5043_FREQUENCYGAINC0NB
                                    348 	.globl _AX5043_FREQUENCYGAINB3NB
                                    349 	.globl _AX5043_FREQUENCYGAINB2NB
                                    350 	.globl _AX5043_FREQUENCYGAINB1NB
                                    351 	.globl _AX5043_FREQUENCYGAINB0NB
                                    352 	.globl _AX5043_FREQUENCYGAINA3NB
                                    353 	.globl _AX5043_FREQUENCYGAINA2NB
                                    354 	.globl _AX5043_FREQUENCYGAINA1NB
                                    355 	.globl _AX5043_FREQUENCYGAINA0NB
                                    356 	.globl _AX5043_FREQDEV13NB
                                    357 	.globl _AX5043_FREQDEV12NB
                                    358 	.globl _AX5043_FREQDEV11NB
                                    359 	.globl _AX5043_FREQDEV10NB
                                    360 	.globl _AX5043_FREQDEV03NB
                                    361 	.globl _AX5043_FREQDEV02NB
                                    362 	.globl _AX5043_FREQDEV01NB
                                    363 	.globl _AX5043_FREQDEV00NB
                                    364 	.globl _AX5043_FOURFSK3NB
                                    365 	.globl _AX5043_FOURFSK2NB
                                    366 	.globl _AX5043_FOURFSK1NB
                                    367 	.globl _AX5043_FOURFSK0NB
                                    368 	.globl _AX5043_DRGAIN3NB
                                    369 	.globl _AX5043_DRGAIN2NB
                                    370 	.globl _AX5043_DRGAIN1NB
                                    371 	.globl _AX5043_DRGAIN0NB
                                    372 	.globl _AX5043_BBOFFSRES3NB
                                    373 	.globl _AX5043_BBOFFSRES2NB
                                    374 	.globl _AX5043_BBOFFSRES1NB
                                    375 	.globl _AX5043_BBOFFSRES0NB
                                    376 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    377 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    378 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    379 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    380 	.globl _AX5043_AGCTARGET3NB
                                    381 	.globl _AX5043_AGCTARGET2NB
                                    382 	.globl _AX5043_AGCTARGET1NB
                                    383 	.globl _AX5043_AGCTARGET0NB
                                    384 	.globl _AX5043_AGCMINMAX3NB
                                    385 	.globl _AX5043_AGCMINMAX2NB
                                    386 	.globl _AX5043_AGCMINMAX1NB
                                    387 	.globl _AX5043_AGCMINMAX0NB
                                    388 	.globl _AX5043_AGCGAIN3NB
                                    389 	.globl _AX5043_AGCGAIN2NB
                                    390 	.globl _AX5043_AGCGAIN1NB
                                    391 	.globl _AX5043_AGCGAIN0NB
                                    392 	.globl _AX5043_AGCAHYST3NB
                                    393 	.globl _AX5043_AGCAHYST2NB
                                    394 	.globl _AX5043_AGCAHYST1NB
                                    395 	.globl _AX5043_AGCAHYST0NB
                                    396 	.globl _AX5043_0xF44NB
                                    397 	.globl _AX5043_0xF35NB
                                    398 	.globl _AX5043_0xF34NB
                                    399 	.globl _AX5043_0xF33NB
                                    400 	.globl _AX5043_0xF32NB
                                    401 	.globl _AX5043_0xF31NB
                                    402 	.globl _AX5043_0xF30NB
                                    403 	.globl _AX5043_0xF26NB
                                    404 	.globl _AX5043_0xF23NB
                                    405 	.globl _AX5043_0xF22NB
                                    406 	.globl _AX5043_0xF21NB
                                    407 	.globl _AX5043_0xF1CNB
                                    408 	.globl _AX5043_0xF18NB
                                    409 	.globl _AX5043_0xF0CNB
                                    410 	.globl _AX5043_0xF00NB
                                    411 	.globl _AX5043_XTALSTATUSNB
                                    412 	.globl _AX5043_XTALOSCNB
                                    413 	.globl _AX5043_XTALCAPNB
                                    414 	.globl _AX5043_XTALAMPLNB
                                    415 	.globl _AX5043_WAKEUPXOEARLYNB
                                    416 	.globl _AX5043_WAKEUPTIMER1NB
                                    417 	.globl _AX5043_WAKEUPTIMER0NB
                                    418 	.globl _AX5043_WAKEUPFREQ1NB
                                    419 	.globl _AX5043_WAKEUPFREQ0NB
                                    420 	.globl _AX5043_WAKEUP1NB
                                    421 	.globl _AX5043_WAKEUP0NB
                                    422 	.globl _AX5043_TXRATE2NB
                                    423 	.globl _AX5043_TXRATE1NB
                                    424 	.globl _AX5043_TXRATE0NB
                                    425 	.globl _AX5043_TXPWRCOEFFE1NB
                                    426 	.globl _AX5043_TXPWRCOEFFE0NB
                                    427 	.globl _AX5043_TXPWRCOEFFD1NB
                                    428 	.globl _AX5043_TXPWRCOEFFD0NB
                                    429 	.globl _AX5043_TXPWRCOEFFC1NB
                                    430 	.globl _AX5043_TXPWRCOEFFC0NB
                                    431 	.globl _AX5043_TXPWRCOEFFB1NB
                                    432 	.globl _AX5043_TXPWRCOEFFB0NB
                                    433 	.globl _AX5043_TXPWRCOEFFA1NB
                                    434 	.globl _AX5043_TXPWRCOEFFA0NB
                                    435 	.globl _AX5043_TRKRFFREQ2NB
                                    436 	.globl _AX5043_TRKRFFREQ1NB
                                    437 	.globl _AX5043_TRKRFFREQ0NB
                                    438 	.globl _AX5043_TRKPHASE1NB
                                    439 	.globl _AX5043_TRKPHASE0NB
                                    440 	.globl _AX5043_TRKFSKDEMOD1NB
                                    441 	.globl _AX5043_TRKFSKDEMOD0NB
                                    442 	.globl _AX5043_TRKFREQ1NB
                                    443 	.globl _AX5043_TRKFREQ0NB
                                    444 	.globl _AX5043_TRKDATARATE2NB
                                    445 	.globl _AX5043_TRKDATARATE1NB
                                    446 	.globl _AX5043_TRKDATARATE0NB
                                    447 	.globl _AX5043_TRKAMPLITUDE1NB
                                    448 	.globl _AX5043_TRKAMPLITUDE0NB
                                    449 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    450 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    451 	.globl _AX5043_TMGTXSETTLENB
                                    452 	.globl _AX5043_TMGTXBOOSTNB
                                    453 	.globl _AX5043_TMGRXSETTLENB
                                    454 	.globl _AX5043_TMGRXRSSINB
                                    455 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    456 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    457 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    458 	.globl _AX5043_TMGRXOFFSACQNB
                                    459 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    460 	.globl _AX5043_TMGRXBOOSTNB
                                    461 	.globl _AX5043_TMGRXAGCNB
                                    462 	.globl _AX5043_TIMER2NB
                                    463 	.globl _AX5043_TIMER1NB
                                    464 	.globl _AX5043_TIMER0NB
                                    465 	.globl _AX5043_SILICONREVISIONNB
                                    466 	.globl _AX5043_SCRATCHNB
                                    467 	.globl _AX5043_RXDATARATE2NB
                                    468 	.globl _AX5043_RXDATARATE1NB
                                    469 	.globl _AX5043_RXDATARATE0NB
                                    470 	.globl _AX5043_RSSIREFERENCENB
                                    471 	.globl _AX5043_RSSIABSTHRNB
                                    472 	.globl _AX5043_RSSINB
                                    473 	.globl _AX5043_REFNB
                                    474 	.globl _AX5043_RADIOSTATENB
                                    475 	.globl _AX5043_RADIOEVENTREQ1NB
                                    476 	.globl _AX5043_RADIOEVENTREQ0NB
                                    477 	.globl _AX5043_RADIOEVENTMASK1NB
                                    478 	.globl _AX5043_RADIOEVENTMASK0NB
                                    479 	.globl _AX5043_PWRMODENB
                                    480 	.globl _AX5043_PWRAMPNB
                                    481 	.globl _AX5043_POWSTICKYSTATNB
                                    482 	.globl _AX5043_POWSTATNB
                                    483 	.globl _AX5043_POWIRQMASKNB
                                    484 	.globl _AX5043_POWCTRL1NB
                                    485 	.globl _AX5043_PLLVCOIRNB
                                    486 	.globl _AX5043_PLLVCOINB
                                    487 	.globl _AX5043_PLLVCODIVNB
                                    488 	.globl _AX5043_PLLRNGCLKNB
                                    489 	.globl _AX5043_PLLRANGINGBNB
                                    490 	.globl _AX5043_PLLRANGINGANB
                                    491 	.globl _AX5043_PLLLOOPBOOSTNB
                                    492 	.globl _AX5043_PLLLOOPNB
                                    493 	.globl _AX5043_PLLLOCKDETNB
                                    494 	.globl _AX5043_PLLCPIBOOSTNB
                                    495 	.globl _AX5043_PLLCPINB
                                    496 	.globl _AX5043_PKTSTOREFLAGSNB
                                    497 	.globl _AX5043_PKTMISCFLAGSNB
                                    498 	.globl _AX5043_PKTCHUNKSIZENB
                                    499 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    500 	.globl _AX5043_PINSTATENB
                                    501 	.globl _AX5043_PINFUNCSYSCLKNB
                                    502 	.globl _AX5043_PINFUNCPWRAMPNB
                                    503 	.globl _AX5043_PINFUNCIRQNB
                                    504 	.globl _AX5043_PINFUNCDCLKNB
                                    505 	.globl _AX5043_PINFUNCDATANB
                                    506 	.globl _AX5043_PINFUNCANTSELNB
                                    507 	.globl _AX5043_MODULATIONNB
                                    508 	.globl _AX5043_MODCFGPNB
                                    509 	.globl _AX5043_MODCFGFNB
                                    510 	.globl _AX5043_MODCFGANB
                                    511 	.globl _AX5043_MAXRFOFFSET2NB
                                    512 	.globl _AX5043_MAXRFOFFSET1NB
                                    513 	.globl _AX5043_MAXRFOFFSET0NB
                                    514 	.globl _AX5043_MAXDROFFSET2NB
                                    515 	.globl _AX5043_MAXDROFFSET1NB
                                    516 	.globl _AX5043_MAXDROFFSET0NB
                                    517 	.globl _AX5043_MATCH1PAT1NB
                                    518 	.globl _AX5043_MATCH1PAT0NB
                                    519 	.globl _AX5043_MATCH1MINNB
                                    520 	.globl _AX5043_MATCH1MAXNB
                                    521 	.globl _AX5043_MATCH1LENNB
                                    522 	.globl _AX5043_MATCH0PAT3NB
                                    523 	.globl _AX5043_MATCH0PAT2NB
                                    524 	.globl _AX5043_MATCH0PAT1NB
                                    525 	.globl _AX5043_MATCH0PAT0NB
                                    526 	.globl _AX5043_MATCH0MINNB
                                    527 	.globl _AX5043_MATCH0MAXNB
                                    528 	.globl _AX5043_MATCH0LENNB
                                    529 	.globl _AX5043_LPOSCSTATUSNB
                                    530 	.globl _AX5043_LPOSCREF1NB
                                    531 	.globl _AX5043_LPOSCREF0NB
                                    532 	.globl _AX5043_LPOSCPER1NB
                                    533 	.globl _AX5043_LPOSCPER0NB
                                    534 	.globl _AX5043_LPOSCKFILT1NB
                                    535 	.globl _AX5043_LPOSCKFILT0NB
                                    536 	.globl _AX5043_LPOSCFREQ1NB
                                    537 	.globl _AX5043_LPOSCFREQ0NB
                                    538 	.globl _AX5043_LPOSCCONFIGNB
                                    539 	.globl _AX5043_IRQREQUEST1NB
                                    540 	.globl _AX5043_IRQREQUEST0NB
                                    541 	.globl _AX5043_IRQMASK1NB
                                    542 	.globl _AX5043_IRQMASK0NB
                                    543 	.globl _AX5043_IRQINVERSION1NB
                                    544 	.globl _AX5043_IRQINVERSION0NB
                                    545 	.globl _AX5043_IFFREQ1NB
                                    546 	.globl _AX5043_IFFREQ0NB
                                    547 	.globl _AX5043_GPADCPERIODNB
                                    548 	.globl _AX5043_GPADCCTRLNB
                                    549 	.globl _AX5043_GPADC13VALUE1NB
                                    550 	.globl _AX5043_GPADC13VALUE0NB
                                    551 	.globl _AX5043_FSKDMIN1NB
                                    552 	.globl _AX5043_FSKDMIN0NB
                                    553 	.globl _AX5043_FSKDMAX1NB
                                    554 	.globl _AX5043_FSKDMAX0NB
                                    555 	.globl _AX5043_FSKDEV2NB
                                    556 	.globl _AX5043_FSKDEV1NB
                                    557 	.globl _AX5043_FSKDEV0NB
                                    558 	.globl _AX5043_FREQB3NB
                                    559 	.globl _AX5043_FREQB2NB
                                    560 	.globl _AX5043_FREQB1NB
                                    561 	.globl _AX5043_FREQB0NB
                                    562 	.globl _AX5043_FREQA3NB
                                    563 	.globl _AX5043_FREQA2NB
                                    564 	.globl _AX5043_FREQA1NB
                                    565 	.globl _AX5043_FREQA0NB
                                    566 	.globl _AX5043_FRAMINGNB
                                    567 	.globl _AX5043_FIFOTHRESH1NB
                                    568 	.globl _AX5043_FIFOTHRESH0NB
                                    569 	.globl _AX5043_FIFOSTATNB
                                    570 	.globl _AX5043_FIFOFREE1NB
                                    571 	.globl _AX5043_FIFOFREE0NB
                                    572 	.globl _AX5043_FIFODATANB
                                    573 	.globl _AX5043_FIFOCOUNT1NB
                                    574 	.globl _AX5043_FIFOCOUNT0NB
                                    575 	.globl _AX5043_FECSYNCNB
                                    576 	.globl _AX5043_FECSTATUSNB
                                    577 	.globl _AX5043_FECNB
                                    578 	.globl _AX5043_ENCODINGNB
                                    579 	.globl _AX5043_DIVERSITYNB
                                    580 	.globl _AX5043_DECIMATIONNB
                                    581 	.globl _AX5043_DACVALUE1NB
                                    582 	.globl _AX5043_DACVALUE0NB
                                    583 	.globl _AX5043_DACCONFIGNB
                                    584 	.globl _AX5043_CRCINIT3NB
                                    585 	.globl _AX5043_CRCINIT2NB
                                    586 	.globl _AX5043_CRCINIT1NB
                                    587 	.globl _AX5043_CRCINIT0NB
                                    588 	.globl _AX5043_BGNDRSSITHRNB
                                    589 	.globl _AX5043_BGNDRSSIGAINNB
                                    590 	.globl _AX5043_BGNDRSSINB
                                    591 	.globl _AX5043_BBTUNENB
                                    592 	.globl _AX5043_BBOFFSCAPNB
                                    593 	.globl _AX5043_AMPLFILTERNB
                                    594 	.globl _AX5043_AGCCOUNTERNB
                                    595 	.globl _AX5043_AFSKSPACE1NB
                                    596 	.globl _AX5043_AFSKSPACE0NB
                                    597 	.globl _AX5043_AFSKMARK1NB
                                    598 	.globl _AX5043_AFSKMARK0NB
                                    599 	.globl _AX5043_AFSKCTRLNB
                                    600 	.globl _AX5043_TIMEGAIN3
                                    601 	.globl _AX5043_TIMEGAIN2
                                    602 	.globl _AX5043_TIMEGAIN1
                                    603 	.globl _AX5043_TIMEGAIN0
                                    604 	.globl _AX5043_RXPARAMSETS
                                    605 	.globl _AX5043_RXPARAMCURSET
                                    606 	.globl _AX5043_PKTMAXLEN
                                    607 	.globl _AX5043_PKTLENOFFSET
                                    608 	.globl _AX5043_PKTLENCFG
                                    609 	.globl _AX5043_PKTADDRMASK3
                                    610 	.globl _AX5043_PKTADDRMASK2
                                    611 	.globl _AX5043_PKTADDRMASK1
                                    612 	.globl _AX5043_PKTADDRMASK0
                                    613 	.globl _AX5043_PKTADDRCFG
                                    614 	.globl _AX5043_PKTADDR3
                                    615 	.globl _AX5043_PKTADDR2
                                    616 	.globl _AX5043_PKTADDR1
                                    617 	.globl _AX5043_PKTADDR0
                                    618 	.globl _AX5043_PHASEGAIN3
                                    619 	.globl _AX5043_PHASEGAIN2
                                    620 	.globl _AX5043_PHASEGAIN1
                                    621 	.globl _AX5043_PHASEGAIN0
                                    622 	.globl _AX5043_FREQUENCYLEAK
                                    623 	.globl _AX5043_FREQUENCYGAIND3
                                    624 	.globl _AX5043_FREQUENCYGAIND2
                                    625 	.globl _AX5043_FREQUENCYGAIND1
                                    626 	.globl _AX5043_FREQUENCYGAIND0
                                    627 	.globl _AX5043_FREQUENCYGAINC3
                                    628 	.globl _AX5043_FREQUENCYGAINC2
                                    629 	.globl _AX5043_FREQUENCYGAINC1
                                    630 	.globl _AX5043_FREQUENCYGAINC0
                                    631 	.globl _AX5043_FREQUENCYGAINB3
                                    632 	.globl _AX5043_FREQUENCYGAINB2
                                    633 	.globl _AX5043_FREQUENCYGAINB1
                                    634 	.globl _AX5043_FREQUENCYGAINB0
                                    635 	.globl _AX5043_FREQUENCYGAINA3
                                    636 	.globl _AX5043_FREQUENCYGAINA2
                                    637 	.globl _AX5043_FREQUENCYGAINA1
                                    638 	.globl _AX5043_FREQUENCYGAINA0
                                    639 	.globl _AX5043_FREQDEV13
                                    640 	.globl _AX5043_FREQDEV12
                                    641 	.globl _AX5043_FREQDEV11
                                    642 	.globl _AX5043_FREQDEV10
                                    643 	.globl _AX5043_FREQDEV03
                                    644 	.globl _AX5043_FREQDEV02
                                    645 	.globl _AX5043_FREQDEV01
                                    646 	.globl _AX5043_FREQDEV00
                                    647 	.globl _AX5043_FOURFSK3
                                    648 	.globl _AX5043_FOURFSK2
                                    649 	.globl _AX5043_FOURFSK1
                                    650 	.globl _AX5043_FOURFSK0
                                    651 	.globl _AX5043_DRGAIN3
                                    652 	.globl _AX5043_DRGAIN2
                                    653 	.globl _AX5043_DRGAIN1
                                    654 	.globl _AX5043_DRGAIN0
                                    655 	.globl _AX5043_BBOFFSRES3
                                    656 	.globl _AX5043_BBOFFSRES2
                                    657 	.globl _AX5043_BBOFFSRES1
                                    658 	.globl _AX5043_BBOFFSRES0
                                    659 	.globl _AX5043_AMPLITUDEGAIN3
                                    660 	.globl _AX5043_AMPLITUDEGAIN2
                                    661 	.globl _AX5043_AMPLITUDEGAIN1
                                    662 	.globl _AX5043_AMPLITUDEGAIN0
                                    663 	.globl _AX5043_AGCTARGET3
                                    664 	.globl _AX5043_AGCTARGET2
                                    665 	.globl _AX5043_AGCTARGET1
                                    666 	.globl _AX5043_AGCTARGET0
                                    667 	.globl _AX5043_AGCMINMAX3
                                    668 	.globl _AX5043_AGCMINMAX2
                                    669 	.globl _AX5043_AGCMINMAX1
                                    670 	.globl _AX5043_AGCMINMAX0
                                    671 	.globl _AX5043_AGCGAIN3
                                    672 	.globl _AX5043_AGCGAIN2
                                    673 	.globl _AX5043_AGCGAIN1
                                    674 	.globl _AX5043_AGCGAIN0
                                    675 	.globl _AX5043_AGCAHYST3
                                    676 	.globl _AX5043_AGCAHYST2
                                    677 	.globl _AX5043_AGCAHYST1
                                    678 	.globl _AX5043_AGCAHYST0
                                    679 	.globl _AX5043_0xF44
                                    680 	.globl _AX5043_0xF35
                                    681 	.globl _AX5043_0xF34
                                    682 	.globl _AX5043_0xF33
                                    683 	.globl _AX5043_0xF32
                                    684 	.globl _AX5043_0xF31
                                    685 	.globl _AX5043_0xF30
                                    686 	.globl _AX5043_0xF26
                                    687 	.globl _AX5043_0xF23
                                    688 	.globl _AX5043_0xF22
                                    689 	.globl _AX5043_0xF21
                                    690 	.globl _AX5043_0xF1C
                                    691 	.globl _AX5043_0xF18
                                    692 	.globl _AX5043_0xF0C
                                    693 	.globl _AX5043_0xF00
                                    694 	.globl _AX5043_XTALSTATUS
                                    695 	.globl _AX5043_XTALOSC
                                    696 	.globl _AX5043_XTALCAP
                                    697 	.globl _AX5043_XTALAMPL
                                    698 	.globl _AX5043_WAKEUPXOEARLY
                                    699 	.globl _AX5043_WAKEUPTIMER1
                                    700 	.globl _AX5043_WAKEUPTIMER0
                                    701 	.globl _AX5043_WAKEUPFREQ1
                                    702 	.globl _AX5043_WAKEUPFREQ0
                                    703 	.globl _AX5043_WAKEUP1
                                    704 	.globl _AX5043_WAKEUP0
                                    705 	.globl _AX5043_TXRATE2
                                    706 	.globl _AX5043_TXRATE1
                                    707 	.globl _AX5043_TXRATE0
                                    708 	.globl _AX5043_TXPWRCOEFFE1
                                    709 	.globl _AX5043_TXPWRCOEFFE0
                                    710 	.globl _AX5043_TXPWRCOEFFD1
                                    711 	.globl _AX5043_TXPWRCOEFFD0
                                    712 	.globl _AX5043_TXPWRCOEFFC1
                                    713 	.globl _AX5043_TXPWRCOEFFC0
                                    714 	.globl _AX5043_TXPWRCOEFFB1
                                    715 	.globl _AX5043_TXPWRCOEFFB0
                                    716 	.globl _AX5043_TXPWRCOEFFA1
                                    717 	.globl _AX5043_TXPWRCOEFFA0
                                    718 	.globl _AX5043_TRKRFFREQ2
                                    719 	.globl _AX5043_TRKRFFREQ1
                                    720 	.globl _AX5043_TRKRFFREQ0
                                    721 	.globl _AX5043_TRKPHASE1
                                    722 	.globl _AX5043_TRKPHASE0
                                    723 	.globl _AX5043_TRKFSKDEMOD1
                                    724 	.globl _AX5043_TRKFSKDEMOD0
                                    725 	.globl _AX5043_TRKFREQ1
                                    726 	.globl _AX5043_TRKFREQ0
                                    727 	.globl _AX5043_TRKDATARATE2
                                    728 	.globl _AX5043_TRKDATARATE1
                                    729 	.globl _AX5043_TRKDATARATE0
                                    730 	.globl _AX5043_TRKAMPLITUDE1
                                    731 	.globl _AX5043_TRKAMPLITUDE0
                                    732 	.globl _AX5043_TRKAFSKDEMOD1
                                    733 	.globl _AX5043_TRKAFSKDEMOD0
                                    734 	.globl _AX5043_TMGTXSETTLE
                                    735 	.globl _AX5043_TMGTXBOOST
                                    736 	.globl _AX5043_TMGRXSETTLE
                                    737 	.globl _AX5043_TMGRXRSSI
                                    738 	.globl _AX5043_TMGRXPREAMBLE3
                                    739 	.globl _AX5043_TMGRXPREAMBLE2
                                    740 	.globl _AX5043_TMGRXPREAMBLE1
                                    741 	.globl _AX5043_TMGRXOFFSACQ
                                    742 	.globl _AX5043_TMGRXCOARSEAGC
                                    743 	.globl _AX5043_TMGRXBOOST
                                    744 	.globl _AX5043_TMGRXAGC
                                    745 	.globl _AX5043_TIMER2
                                    746 	.globl _AX5043_TIMER1
                                    747 	.globl _AX5043_TIMER0
                                    748 	.globl _AX5043_SILICONREVISION
                                    749 	.globl _AX5043_SCRATCH
                                    750 	.globl _AX5043_RXDATARATE2
                                    751 	.globl _AX5043_RXDATARATE1
                                    752 	.globl _AX5043_RXDATARATE0
                                    753 	.globl _AX5043_RSSIREFERENCE
                                    754 	.globl _AX5043_RSSIABSTHR
                                    755 	.globl _AX5043_RSSI
                                    756 	.globl _AX5043_REF
                                    757 	.globl _AX5043_RADIOSTATE
                                    758 	.globl _AX5043_RADIOEVENTREQ1
                                    759 	.globl _AX5043_RADIOEVENTREQ0
                                    760 	.globl _AX5043_RADIOEVENTMASK1
                                    761 	.globl _AX5043_RADIOEVENTMASK0
                                    762 	.globl _AX5043_PWRMODE
                                    763 	.globl _AX5043_PWRAMP
                                    764 	.globl _AX5043_POWSTICKYSTAT
                                    765 	.globl _AX5043_POWSTAT
                                    766 	.globl _AX5043_POWIRQMASK
                                    767 	.globl _AX5043_POWCTRL1
                                    768 	.globl _AX5043_PLLVCOIR
                                    769 	.globl _AX5043_PLLVCOI
                                    770 	.globl _AX5043_PLLVCODIV
                                    771 	.globl _AX5043_PLLRNGCLK
                                    772 	.globl _AX5043_PLLRANGINGB
                                    773 	.globl _AX5043_PLLRANGINGA
                                    774 	.globl _AX5043_PLLLOOPBOOST
                                    775 	.globl _AX5043_PLLLOOP
                                    776 	.globl _AX5043_PLLLOCKDET
                                    777 	.globl _AX5043_PLLCPIBOOST
                                    778 	.globl _AX5043_PLLCPI
                                    779 	.globl _AX5043_PKTSTOREFLAGS
                                    780 	.globl _AX5043_PKTMISCFLAGS
                                    781 	.globl _AX5043_PKTCHUNKSIZE
                                    782 	.globl _AX5043_PKTACCEPTFLAGS
                                    783 	.globl _AX5043_PINSTATE
                                    784 	.globl _AX5043_PINFUNCSYSCLK
                                    785 	.globl _AX5043_PINFUNCPWRAMP
                                    786 	.globl _AX5043_PINFUNCIRQ
                                    787 	.globl _AX5043_PINFUNCDCLK
                                    788 	.globl _AX5043_PINFUNCDATA
                                    789 	.globl _AX5043_PINFUNCANTSEL
                                    790 	.globl _AX5043_MODULATION
                                    791 	.globl _AX5043_MODCFGP
                                    792 	.globl _AX5043_MODCFGF
                                    793 	.globl _AX5043_MODCFGA
                                    794 	.globl _AX5043_MAXRFOFFSET2
                                    795 	.globl _AX5043_MAXRFOFFSET1
                                    796 	.globl _AX5043_MAXRFOFFSET0
                                    797 	.globl _AX5043_MAXDROFFSET2
                                    798 	.globl _AX5043_MAXDROFFSET1
                                    799 	.globl _AX5043_MAXDROFFSET0
                                    800 	.globl _AX5043_MATCH1PAT1
                                    801 	.globl _AX5043_MATCH1PAT0
                                    802 	.globl _AX5043_MATCH1MIN
                                    803 	.globl _AX5043_MATCH1MAX
                                    804 	.globl _AX5043_MATCH1LEN
                                    805 	.globl _AX5043_MATCH0PAT3
                                    806 	.globl _AX5043_MATCH0PAT2
                                    807 	.globl _AX5043_MATCH0PAT1
                                    808 	.globl _AX5043_MATCH0PAT0
                                    809 	.globl _AX5043_MATCH0MIN
                                    810 	.globl _AX5043_MATCH0MAX
                                    811 	.globl _AX5043_MATCH0LEN
                                    812 	.globl _AX5043_LPOSCSTATUS
                                    813 	.globl _AX5043_LPOSCREF1
                                    814 	.globl _AX5043_LPOSCREF0
                                    815 	.globl _AX5043_LPOSCPER1
                                    816 	.globl _AX5043_LPOSCPER0
                                    817 	.globl _AX5043_LPOSCKFILT1
                                    818 	.globl _AX5043_LPOSCKFILT0
                                    819 	.globl _AX5043_LPOSCFREQ1
                                    820 	.globl _AX5043_LPOSCFREQ0
                                    821 	.globl _AX5043_LPOSCCONFIG
                                    822 	.globl _AX5043_IRQREQUEST1
                                    823 	.globl _AX5043_IRQREQUEST0
                                    824 	.globl _AX5043_IRQMASK1
                                    825 	.globl _AX5043_IRQMASK0
                                    826 	.globl _AX5043_IRQINVERSION1
                                    827 	.globl _AX5043_IRQINVERSION0
                                    828 	.globl _AX5043_IFFREQ1
                                    829 	.globl _AX5043_IFFREQ0
                                    830 	.globl _AX5043_GPADCPERIOD
                                    831 	.globl _AX5043_GPADCCTRL
                                    832 	.globl _AX5043_GPADC13VALUE1
                                    833 	.globl _AX5043_GPADC13VALUE0
                                    834 	.globl _AX5043_FSKDMIN1
                                    835 	.globl _AX5043_FSKDMIN0
                                    836 	.globl _AX5043_FSKDMAX1
                                    837 	.globl _AX5043_FSKDMAX0
                                    838 	.globl _AX5043_FSKDEV2
                                    839 	.globl _AX5043_FSKDEV1
                                    840 	.globl _AX5043_FSKDEV0
                                    841 	.globl _AX5043_FREQB3
                                    842 	.globl _AX5043_FREQB2
                                    843 	.globl _AX5043_FREQB1
                                    844 	.globl _AX5043_FREQB0
                                    845 	.globl _AX5043_FREQA3
                                    846 	.globl _AX5043_FREQA2
                                    847 	.globl _AX5043_FREQA1
                                    848 	.globl _AX5043_FREQA0
                                    849 	.globl _AX5043_FRAMING
                                    850 	.globl _AX5043_FIFOTHRESH1
                                    851 	.globl _AX5043_FIFOTHRESH0
                                    852 	.globl _AX5043_FIFOSTAT
                                    853 	.globl _AX5043_FIFOFREE1
                                    854 	.globl _AX5043_FIFOFREE0
                                    855 	.globl _AX5043_FIFODATA
                                    856 	.globl _AX5043_FIFOCOUNT1
                                    857 	.globl _AX5043_FIFOCOUNT0
                                    858 	.globl _AX5043_FECSYNC
                                    859 	.globl _AX5043_FECSTATUS
                                    860 	.globl _AX5043_FEC
                                    861 	.globl _AX5043_ENCODING
                                    862 	.globl _AX5043_DIVERSITY
                                    863 	.globl _AX5043_DECIMATION
                                    864 	.globl _AX5043_DACVALUE1
                                    865 	.globl _AX5043_DACVALUE0
                                    866 	.globl _AX5043_DACCONFIG
                                    867 	.globl _AX5043_CRCINIT3
                                    868 	.globl _AX5043_CRCINIT2
                                    869 	.globl _AX5043_CRCINIT1
                                    870 	.globl _AX5043_CRCINIT0
                                    871 	.globl _AX5043_BGNDRSSITHR
                                    872 	.globl _AX5043_BGNDRSSIGAIN
                                    873 	.globl _AX5043_BGNDRSSI
                                    874 	.globl _AX5043_BBTUNE
                                    875 	.globl _AX5043_BBOFFSCAP
                                    876 	.globl _AX5043_AMPLFILTER
                                    877 	.globl _AX5043_AGCCOUNTER
                                    878 	.globl _AX5043_AFSKSPACE1
                                    879 	.globl _AX5043_AFSKSPACE0
                                    880 	.globl _AX5043_AFSKMARK1
                                    881 	.globl _AX5043_AFSKMARK0
                                    882 	.globl _AX5043_AFSKCTRL
                                    883 	.globl _XTALREADY
                                    884 	.globl _XTALOSC
                                    885 	.globl _XTALAMPL
                                    886 	.globl _SILICONREV
                                    887 	.globl _SCRATCH3
                                    888 	.globl _SCRATCH2
                                    889 	.globl _SCRATCH1
                                    890 	.globl _SCRATCH0
                                    891 	.globl _RADIOMUX
                                    892 	.globl _RADIOFSTATADDR
                                    893 	.globl _RADIOFSTATADDR1
                                    894 	.globl _RADIOFSTATADDR0
                                    895 	.globl _RADIOFDATAADDR
                                    896 	.globl _RADIOFDATAADDR1
                                    897 	.globl _RADIOFDATAADDR0
                                    898 	.globl _OSCRUN
                                    899 	.globl _OSCREADY
                                    900 	.globl _OSCFORCERUN
                                    901 	.globl _OSCCALIB
                                    902 	.globl _MISCCTRL
                                    903 	.globl _LPXOSCGM
                                    904 	.globl _LPOSCREF
                                    905 	.globl _LPOSCREF1
                                    906 	.globl _LPOSCREF0
                                    907 	.globl _LPOSCPER
                                    908 	.globl _LPOSCPER1
                                    909 	.globl _LPOSCPER0
                                    910 	.globl _LPOSCKFILT
                                    911 	.globl _LPOSCKFILT1
                                    912 	.globl _LPOSCKFILT0
                                    913 	.globl _LPOSCFREQ
                                    914 	.globl _LPOSCFREQ1
                                    915 	.globl _LPOSCFREQ0
                                    916 	.globl _LPOSCCONFIG
                                    917 	.globl _PINSEL
                                    918 	.globl _PINCHGC
                                    919 	.globl _PINCHGB
                                    920 	.globl _PINCHGA
                                    921 	.globl _PALTRADIO
                                    922 	.globl _PALTC
                                    923 	.globl _PALTB
                                    924 	.globl _PALTA
                                    925 	.globl _INTCHGC
                                    926 	.globl _INTCHGB
                                    927 	.globl _INTCHGA
                                    928 	.globl _EXTIRQ
                                    929 	.globl _GPIOENABLE
                                    930 	.globl _ANALOGA
                                    931 	.globl _FRCOSCREF
                                    932 	.globl _FRCOSCREF1
                                    933 	.globl _FRCOSCREF0
                                    934 	.globl _FRCOSCPER
                                    935 	.globl _FRCOSCPER1
                                    936 	.globl _FRCOSCPER0
                                    937 	.globl _FRCOSCKFILT
                                    938 	.globl _FRCOSCKFILT1
                                    939 	.globl _FRCOSCKFILT0
                                    940 	.globl _FRCOSCFREQ
                                    941 	.globl _FRCOSCFREQ1
                                    942 	.globl _FRCOSCFREQ0
                                    943 	.globl _FRCOSCCTRL
                                    944 	.globl _FRCOSCCONFIG
                                    945 	.globl _DMA1CONFIG
                                    946 	.globl _DMA1ADDR
                                    947 	.globl _DMA1ADDR1
                                    948 	.globl _DMA1ADDR0
                                    949 	.globl _DMA0CONFIG
                                    950 	.globl _DMA0ADDR
                                    951 	.globl _DMA0ADDR1
                                    952 	.globl _DMA0ADDR0
                                    953 	.globl _ADCTUNE2
                                    954 	.globl _ADCTUNE1
                                    955 	.globl _ADCTUNE0
                                    956 	.globl _ADCCH3VAL
                                    957 	.globl _ADCCH3VAL1
                                    958 	.globl _ADCCH3VAL0
                                    959 	.globl _ADCCH2VAL
                                    960 	.globl _ADCCH2VAL1
                                    961 	.globl _ADCCH2VAL0
                                    962 	.globl _ADCCH1VAL
                                    963 	.globl _ADCCH1VAL1
                                    964 	.globl _ADCCH1VAL0
                                    965 	.globl _ADCCH0VAL
                                    966 	.globl _ADCCH0VAL1
                                    967 	.globl _ADCCH0VAL0
                                    968 	.globl _swap_variables
                                    969 	.globl _randomset_generate
                                    970 	.globl _packetFraming
                                    971 ;--------------------------------------------------------
                                    972 ; special function registers
                                    973 ;--------------------------------------------------------
                                    974 	.area RSEG    (ABS,DATA)
      000000                        975 	.org 0x0000
                           0000E0   976 G$ACC$0$0 == 0x00e0
                           0000E0   977 _ACC	=	0x00e0
                           0000F0   978 G$B$0$0 == 0x00f0
                           0000F0   979 _B	=	0x00f0
                           000083   980 G$DPH$0$0 == 0x0083
                           000083   981 _DPH	=	0x0083
                           000085   982 G$DPH1$0$0 == 0x0085
                           000085   983 _DPH1	=	0x0085
                           000082   984 G$DPL$0$0 == 0x0082
                           000082   985 _DPL	=	0x0082
                           000084   986 G$DPL1$0$0 == 0x0084
                           000084   987 _DPL1	=	0x0084
                           008382   988 G$DPTR0$0$0 == 0x8382
                           008382   989 _DPTR0	=	0x8382
                           008584   990 G$DPTR1$0$0 == 0x8584
                           008584   991 _DPTR1	=	0x8584
                           000086   992 G$DPS$0$0 == 0x0086
                           000086   993 _DPS	=	0x0086
                           0000A0   994 G$E2IE$0$0 == 0x00a0
                           0000A0   995 _E2IE	=	0x00a0
                           0000C0   996 G$E2IP$0$0 == 0x00c0
                           0000C0   997 _E2IP	=	0x00c0
                           000098   998 G$EIE$0$0 == 0x0098
                           000098   999 _EIE	=	0x0098
                           0000B0  1000 G$EIP$0$0 == 0x00b0
                           0000B0  1001 _EIP	=	0x00b0
                           0000A8  1002 G$IE$0$0 == 0x00a8
                           0000A8  1003 _IE	=	0x00a8
                           0000B8  1004 G$IP$0$0 == 0x00b8
                           0000B8  1005 _IP	=	0x00b8
                           000087  1006 G$PCON$0$0 == 0x0087
                           000087  1007 _PCON	=	0x0087
                           0000D0  1008 G$PSW$0$0 == 0x00d0
                           0000D0  1009 _PSW	=	0x00d0
                           000081  1010 G$SP$0$0 == 0x0081
                           000081  1011 _SP	=	0x0081
                           0000D9  1012 G$XPAGE$0$0 == 0x00d9
                           0000D9  1013 _XPAGE	=	0x00d9
                           0000D9  1014 G$_XPAGE$0$0 == 0x00d9
                           0000D9  1015 __XPAGE	=	0x00d9
                           0000CA  1016 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA  1017 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1018 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB  1019 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1020 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2  1021 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1022 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3  1023 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1024 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1  1025 _ADCCLKSRC	=	0x00d1
                           0000C9  1026 G$ADCCONV$0$0 == 0x00c9
                           0000C9  1027 _ADCCONV	=	0x00c9
                           0000E1  1028 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1  1029 _ANALOGCOMP	=	0x00e1
                           0000C6  1030 G$CLKCON$0$0 == 0x00c6
                           0000C6  1031 _CLKCON	=	0x00c6
                           0000C7  1032 G$CLKSTAT$0$0 == 0x00c7
                           0000C7  1033 _CLKSTAT	=	0x00c7
                           000097  1034 G$CODECONFIG$0$0 == 0x0097
                           000097  1035 _CODECONFIG	=	0x0097
                           0000E3  1036 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3  1037 _DBGLNKBUF	=	0x00e3
                           0000E2  1038 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2  1039 _DBGLNKSTAT	=	0x00e2
                           000089  1040 G$DIRA$0$0 == 0x0089
                           000089  1041 _DIRA	=	0x0089
                           00008A  1042 G$DIRB$0$0 == 0x008a
                           00008A  1043 _DIRB	=	0x008a
                           00008B  1044 G$DIRC$0$0 == 0x008b
                           00008B  1045 _DIRC	=	0x008b
                           00008E  1046 G$DIRR$0$0 == 0x008e
                           00008E  1047 _DIRR	=	0x008e
                           0000C8  1048 G$PINA$0$0 == 0x00c8
                           0000C8  1049 _PINA	=	0x00c8
                           0000E8  1050 G$PINB$0$0 == 0x00e8
                           0000E8  1051 _PINB	=	0x00e8
                           0000F8  1052 G$PINC$0$0 == 0x00f8
                           0000F8  1053 _PINC	=	0x00f8
                           00008D  1054 G$PINR$0$0 == 0x008d
                           00008D  1055 _PINR	=	0x008d
                           000080  1056 G$PORTA$0$0 == 0x0080
                           000080  1057 _PORTA	=	0x0080
                           000088  1058 G$PORTB$0$0 == 0x0088
                           000088  1059 _PORTB	=	0x0088
                           000090  1060 G$PORTC$0$0 == 0x0090
                           000090  1061 _PORTC	=	0x0090
                           00008C  1062 G$PORTR$0$0 == 0x008c
                           00008C  1063 _PORTR	=	0x008c
                           0000CE  1064 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE  1065 _IC0CAPT0	=	0x00ce
                           0000CF  1066 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF  1067 _IC0CAPT1	=	0x00cf
                           00CFCE  1068 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE  1069 _IC0CAPT	=	0xcfce
                           0000CC  1070 G$IC0MODE$0$0 == 0x00cc
                           0000CC  1071 _IC0MODE	=	0x00cc
                           0000CD  1072 G$IC0STATUS$0$0 == 0x00cd
                           0000CD  1073 _IC0STATUS	=	0x00cd
                           0000D6  1074 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6  1075 _IC1CAPT0	=	0x00d6
                           0000D7  1076 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7  1077 _IC1CAPT1	=	0x00d7
                           00D7D6  1078 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6  1079 _IC1CAPT	=	0xd7d6
                           0000D4  1080 G$IC1MODE$0$0 == 0x00d4
                           0000D4  1081 _IC1MODE	=	0x00d4
                           0000D5  1082 G$IC1STATUS$0$0 == 0x00d5
                           0000D5  1083 _IC1STATUS	=	0x00d5
                           000092  1084 G$NVADDR0$0$0 == 0x0092
                           000092  1085 _NVADDR0	=	0x0092
                           000093  1086 G$NVADDR1$0$0 == 0x0093
                           000093  1087 _NVADDR1	=	0x0093
                           009392  1088 G$NVADDR$0$0 == 0x9392
                           009392  1089 _NVADDR	=	0x9392
                           000094  1090 G$NVDATA0$0$0 == 0x0094
                           000094  1091 _NVDATA0	=	0x0094
                           000095  1092 G$NVDATA1$0$0 == 0x0095
                           000095  1093 _NVDATA1	=	0x0095
                           009594  1094 G$NVDATA$0$0 == 0x9594
                           009594  1095 _NVDATA	=	0x9594
                           000096  1096 G$NVKEY$0$0 == 0x0096
                           000096  1097 _NVKEY	=	0x0096
                           000091  1098 G$NVSTATUS$0$0 == 0x0091
                           000091  1099 _NVSTATUS	=	0x0091
                           0000BC  1100 G$OC0COMP0$0$0 == 0x00bc
                           0000BC  1101 _OC0COMP0	=	0x00bc
                           0000BD  1102 G$OC0COMP1$0$0 == 0x00bd
                           0000BD  1103 _OC0COMP1	=	0x00bd
                           00BDBC  1104 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC  1105 _OC0COMP	=	0xbdbc
                           0000B9  1106 G$OC0MODE$0$0 == 0x00b9
                           0000B9  1107 _OC0MODE	=	0x00b9
                           0000BA  1108 G$OC0PIN$0$0 == 0x00ba
                           0000BA  1109 _OC0PIN	=	0x00ba
                           0000BB  1110 G$OC0STATUS$0$0 == 0x00bb
                           0000BB  1111 _OC0STATUS	=	0x00bb
                           0000C4  1112 G$OC1COMP0$0$0 == 0x00c4
                           0000C4  1113 _OC1COMP0	=	0x00c4
                           0000C5  1114 G$OC1COMP1$0$0 == 0x00c5
                           0000C5  1115 _OC1COMP1	=	0x00c5
                           00C5C4  1116 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4  1117 _OC1COMP	=	0xc5c4
                           0000C1  1118 G$OC1MODE$0$0 == 0x00c1
                           0000C1  1119 _OC1MODE	=	0x00c1
                           0000C2  1120 G$OC1PIN$0$0 == 0x00c2
                           0000C2  1121 _OC1PIN	=	0x00c2
                           0000C3  1122 G$OC1STATUS$0$0 == 0x00c3
                           0000C3  1123 _OC1STATUS	=	0x00c3
                           0000B1  1124 G$RADIOACC$0$0 == 0x00b1
                           0000B1  1125 _RADIOACC	=	0x00b1
                           0000B3  1126 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3  1127 _RADIOADDR0	=	0x00b3
                           0000B2  1128 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2  1129 _RADIOADDR1	=	0x00b2
                           00B2B3  1130 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3  1131 _RADIOADDR	=	0xb2b3
                           0000B7  1132 G$RADIODATA0$0$0 == 0x00b7
                           0000B7  1133 _RADIODATA0	=	0x00b7
                           0000B6  1134 G$RADIODATA1$0$0 == 0x00b6
                           0000B6  1135 _RADIODATA1	=	0x00b6
                           0000B5  1136 G$RADIODATA2$0$0 == 0x00b5
                           0000B5  1137 _RADIODATA2	=	0x00b5
                           0000B4  1138 G$RADIODATA3$0$0 == 0x00b4
                           0000B4  1139 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1140 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7  1141 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1142 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE  1143 _RADIOSTAT0	=	0x00be
                           0000BF  1144 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF  1145 _RADIOSTAT1	=	0x00bf
                           00BFBE  1146 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE  1147 _RADIOSTAT	=	0xbfbe
                           0000DF  1148 G$SPCLKSRC$0$0 == 0x00df
                           0000DF  1149 _SPCLKSRC	=	0x00df
                           0000DC  1150 G$SPMODE$0$0 == 0x00dc
                           0000DC  1151 _SPMODE	=	0x00dc
                           0000DE  1152 G$SPSHREG$0$0 == 0x00de
                           0000DE  1153 _SPSHREG	=	0x00de
                           0000DD  1154 G$SPSTATUS$0$0 == 0x00dd
                           0000DD  1155 _SPSTATUS	=	0x00dd
                           00009A  1156 G$T0CLKSRC$0$0 == 0x009a
                           00009A  1157 _T0CLKSRC	=	0x009a
                           00009C  1158 G$T0CNT0$0$0 == 0x009c
                           00009C  1159 _T0CNT0	=	0x009c
                           00009D  1160 G$T0CNT1$0$0 == 0x009d
                           00009D  1161 _T0CNT1	=	0x009d
                           009D9C  1162 G$T0CNT$0$0 == 0x9d9c
                           009D9C  1163 _T0CNT	=	0x9d9c
                           000099  1164 G$T0MODE$0$0 == 0x0099
                           000099  1165 _T0MODE	=	0x0099
                           00009E  1166 G$T0PERIOD0$0$0 == 0x009e
                           00009E  1167 _T0PERIOD0	=	0x009e
                           00009F  1168 G$T0PERIOD1$0$0 == 0x009f
                           00009F  1169 _T0PERIOD1	=	0x009f
                           009F9E  1170 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E  1171 _T0PERIOD	=	0x9f9e
                           00009B  1172 G$T0STATUS$0$0 == 0x009b
                           00009B  1173 _T0STATUS	=	0x009b
                           0000A2  1174 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2  1175 _T1CLKSRC	=	0x00a2
                           0000A4  1176 G$T1CNT0$0$0 == 0x00a4
                           0000A4  1177 _T1CNT0	=	0x00a4
                           0000A5  1178 G$T1CNT1$0$0 == 0x00a5
                           0000A5  1179 _T1CNT1	=	0x00a5
                           00A5A4  1180 G$T1CNT$0$0 == 0xa5a4
                           00A5A4  1181 _T1CNT	=	0xa5a4
                           0000A1  1182 G$T1MODE$0$0 == 0x00a1
                           0000A1  1183 _T1MODE	=	0x00a1
                           0000A6  1184 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6  1185 _T1PERIOD0	=	0x00a6
                           0000A7  1186 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7  1187 _T1PERIOD1	=	0x00a7
                           00A7A6  1188 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6  1189 _T1PERIOD	=	0xa7a6
                           0000A3  1190 G$T1STATUS$0$0 == 0x00a3
                           0000A3  1191 _T1STATUS	=	0x00a3
                           0000AA  1192 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA  1193 _T2CLKSRC	=	0x00aa
                           0000AC  1194 G$T2CNT0$0$0 == 0x00ac
                           0000AC  1195 _T2CNT0	=	0x00ac
                           0000AD  1196 G$T2CNT1$0$0 == 0x00ad
                           0000AD  1197 _T2CNT1	=	0x00ad
                           00ADAC  1198 G$T2CNT$0$0 == 0xadac
                           00ADAC  1199 _T2CNT	=	0xadac
                           0000A9  1200 G$T2MODE$0$0 == 0x00a9
                           0000A9  1201 _T2MODE	=	0x00a9
                           0000AE  1202 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE  1203 _T2PERIOD0	=	0x00ae
                           0000AF  1204 G$T2PERIOD1$0$0 == 0x00af
                           0000AF  1205 _T2PERIOD1	=	0x00af
                           00AFAE  1206 G$T2PERIOD$0$0 == 0xafae
                           00AFAE  1207 _T2PERIOD	=	0xafae
                           0000AB  1208 G$T2STATUS$0$0 == 0x00ab
                           0000AB  1209 _T2STATUS	=	0x00ab
                           0000E4  1210 G$U0CTRL$0$0 == 0x00e4
                           0000E4  1211 _U0CTRL	=	0x00e4
                           0000E7  1212 G$U0MODE$0$0 == 0x00e7
                           0000E7  1213 _U0MODE	=	0x00e7
                           0000E6  1214 G$U0SHREG$0$0 == 0x00e6
                           0000E6  1215 _U0SHREG	=	0x00e6
                           0000E5  1216 G$U0STATUS$0$0 == 0x00e5
                           0000E5  1217 _U0STATUS	=	0x00e5
                           0000EC  1218 G$U1CTRL$0$0 == 0x00ec
                           0000EC  1219 _U1CTRL	=	0x00ec
                           0000EF  1220 G$U1MODE$0$0 == 0x00ef
                           0000EF  1221 _U1MODE	=	0x00ef
                           0000EE  1222 G$U1SHREG$0$0 == 0x00ee
                           0000EE  1223 _U1SHREG	=	0x00ee
                           0000ED  1224 G$U1STATUS$0$0 == 0x00ed
                           0000ED  1225 _U1STATUS	=	0x00ed
                           0000DA  1226 G$WDTCFG$0$0 == 0x00da
                           0000DA  1227 _WDTCFG	=	0x00da
                           0000DB  1228 G$WDTRESET$0$0 == 0x00db
                           0000DB  1229 _WDTRESET	=	0x00db
                           0000F1  1230 G$WTCFGA$0$0 == 0x00f1
                           0000F1  1231 _WTCFGA	=	0x00f1
                           0000F9  1232 G$WTCFGB$0$0 == 0x00f9
                           0000F9  1233 _WTCFGB	=	0x00f9
                           0000F2  1234 G$WTCNTA0$0$0 == 0x00f2
                           0000F2  1235 _WTCNTA0	=	0x00f2
                           0000F3  1236 G$WTCNTA1$0$0 == 0x00f3
                           0000F3  1237 _WTCNTA1	=	0x00f3
                           00F3F2  1238 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2  1239 _WTCNTA	=	0xf3f2
                           0000FA  1240 G$WTCNTB0$0$0 == 0x00fa
                           0000FA  1241 _WTCNTB0	=	0x00fa
                           0000FB  1242 G$WTCNTB1$0$0 == 0x00fb
                           0000FB  1243 _WTCNTB1	=	0x00fb
                           00FBFA  1244 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA  1245 _WTCNTB	=	0xfbfa
                           0000EB  1246 G$WTCNTR1$0$0 == 0x00eb
                           0000EB  1247 _WTCNTR1	=	0x00eb
                           0000F4  1248 G$WTEVTA0$0$0 == 0x00f4
                           0000F4  1249 _WTEVTA0	=	0x00f4
                           0000F5  1250 G$WTEVTA1$0$0 == 0x00f5
                           0000F5  1251 _WTEVTA1	=	0x00f5
                           00F5F4  1252 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4  1253 _WTEVTA	=	0xf5f4
                           0000F6  1254 G$WTEVTB0$0$0 == 0x00f6
                           0000F6  1255 _WTEVTB0	=	0x00f6
                           0000F7  1256 G$WTEVTB1$0$0 == 0x00f7
                           0000F7  1257 _WTEVTB1	=	0x00f7
                           00F7F6  1258 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6  1259 _WTEVTB	=	0xf7f6
                           0000FC  1260 G$WTEVTC0$0$0 == 0x00fc
                           0000FC  1261 _WTEVTC0	=	0x00fc
                           0000FD  1262 G$WTEVTC1$0$0 == 0x00fd
                           0000FD  1263 _WTEVTC1	=	0x00fd
                           00FDFC  1264 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC  1265 _WTEVTC	=	0xfdfc
                           0000FE  1266 G$WTEVTD0$0$0 == 0x00fe
                           0000FE  1267 _WTEVTD0	=	0x00fe
                           0000FF  1268 G$WTEVTD1$0$0 == 0x00ff
                           0000FF  1269 _WTEVTD1	=	0x00ff
                           00FFFE  1270 G$WTEVTD$0$0 == 0xfffe
                           00FFFE  1271 _WTEVTD	=	0xfffe
                           0000E9  1272 G$WTIRQEN$0$0 == 0x00e9
                           0000E9  1273 _WTIRQEN	=	0x00e9
                           0000EA  1274 G$WTSTAT$0$0 == 0x00ea
                           0000EA  1275 _WTSTAT	=	0x00ea
                                   1276 ;--------------------------------------------------------
                                   1277 ; special function bits
                                   1278 ;--------------------------------------------------------
                                   1279 	.area RSEG    (ABS,DATA)
      000000                       1280 	.org 0x0000
                           0000E0  1281 G$ACC_0$0$0 == 0x00e0
                           0000E0  1282 _ACC_0	=	0x00e0
                           0000E1  1283 G$ACC_1$0$0 == 0x00e1
                           0000E1  1284 _ACC_1	=	0x00e1
                           0000E2  1285 G$ACC_2$0$0 == 0x00e2
                           0000E2  1286 _ACC_2	=	0x00e2
                           0000E3  1287 G$ACC_3$0$0 == 0x00e3
                           0000E3  1288 _ACC_3	=	0x00e3
                           0000E4  1289 G$ACC_4$0$0 == 0x00e4
                           0000E4  1290 _ACC_4	=	0x00e4
                           0000E5  1291 G$ACC_5$0$0 == 0x00e5
                           0000E5  1292 _ACC_5	=	0x00e5
                           0000E6  1293 G$ACC_6$0$0 == 0x00e6
                           0000E6  1294 _ACC_6	=	0x00e6
                           0000E7  1295 G$ACC_7$0$0 == 0x00e7
                           0000E7  1296 _ACC_7	=	0x00e7
                           0000F0  1297 G$B_0$0$0 == 0x00f0
                           0000F0  1298 _B_0	=	0x00f0
                           0000F1  1299 G$B_1$0$0 == 0x00f1
                           0000F1  1300 _B_1	=	0x00f1
                           0000F2  1301 G$B_2$0$0 == 0x00f2
                           0000F2  1302 _B_2	=	0x00f2
                           0000F3  1303 G$B_3$0$0 == 0x00f3
                           0000F3  1304 _B_3	=	0x00f3
                           0000F4  1305 G$B_4$0$0 == 0x00f4
                           0000F4  1306 _B_4	=	0x00f4
                           0000F5  1307 G$B_5$0$0 == 0x00f5
                           0000F5  1308 _B_5	=	0x00f5
                           0000F6  1309 G$B_6$0$0 == 0x00f6
                           0000F6  1310 _B_6	=	0x00f6
                           0000F7  1311 G$B_7$0$0 == 0x00f7
                           0000F7  1312 _B_7	=	0x00f7
                           0000A0  1313 G$E2IE_0$0$0 == 0x00a0
                           0000A0  1314 _E2IE_0	=	0x00a0
                           0000A1  1315 G$E2IE_1$0$0 == 0x00a1
                           0000A1  1316 _E2IE_1	=	0x00a1
                           0000A2  1317 G$E2IE_2$0$0 == 0x00a2
                           0000A2  1318 _E2IE_2	=	0x00a2
                           0000A3  1319 G$E2IE_3$0$0 == 0x00a3
                           0000A3  1320 _E2IE_3	=	0x00a3
                           0000A4  1321 G$E2IE_4$0$0 == 0x00a4
                           0000A4  1322 _E2IE_4	=	0x00a4
                           0000A5  1323 G$E2IE_5$0$0 == 0x00a5
                           0000A5  1324 _E2IE_5	=	0x00a5
                           0000A6  1325 G$E2IE_6$0$0 == 0x00a6
                           0000A6  1326 _E2IE_6	=	0x00a6
                           0000A7  1327 G$E2IE_7$0$0 == 0x00a7
                           0000A7  1328 _E2IE_7	=	0x00a7
                           0000C0  1329 G$E2IP_0$0$0 == 0x00c0
                           0000C0  1330 _E2IP_0	=	0x00c0
                           0000C1  1331 G$E2IP_1$0$0 == 0x00c1
                           0000C1  1332 _E2IP_1	=	0x00c1
                           0000C2  1333 G$E2IP_2$0$0 == 0x00c2
                           0000C2  1334 _E2IP_2	=	0x00c2
                           0000C3  1335 G$E2IP_3$0$0 == 0x00c3
                           0000C3  1336 _E2IP_3	=	0x00c3
                           0000C4  1337 G$E2IP_4$0$0 == 0x00c4
                           0000C4  1338 _E2IP_4	=	0x00c4
                           0000C5  1339 G$E2IP_5$0$0 == 0x00c5
                           0000C5  1340 _E2IP_5	=	0x00c5
                           0000C6  1341 G$E2IP_6$0$0 == 0x00c6
                           0000C6  1342 _E2IP_6	=	0x00c6
                           0000C7  1343 G$E2IP_7$0$0 == 0x00c7
                           0000C7  1344 _E2IP_7	=	0x00c7
                           000098  1345 G$EIE_0$0$0 == 0x0098
                           000098  1346 _EIE_0	=	0x0098
                           000099  1347 G$EIE_1$0$0 == 0x0099
                           000099  1348 _EIE_1	=	0x0099
                           00009A  1349 G$EIE_2$0$0 == 0x009a
                           00009A  1350 _EIE_2	=	0x009a
                           00009B  1351 G$EIE_3$0$0 == 0x009b
                           00009B  1352 _EIE_3	=	0x009b
                           00009C  1353 G$EIE_4$0$0 == 0x009c
                           00009C  1354 _EIE_4	=	0x009c
                           00009D  1355 G$EIE_5$0$0 == 0x009d
                           00009D  1356 _EIE_5	=	0x009d
                           00009E  1357 G$EIE_6$0$0 == 0x009e
                           00009E  1358 _EIE_6	=	0x009e
                           00009F  1359 G$EIE_7$0$0 == 0x009f
                           00009F  1360 _EIE_7	=	0x009f
                           0000B0  1361 G$EIP_0$0$0 == 0x00b0
                           0000B0  1362 _EIP_0	=	0x00b0
                           0000B1  1363 G$EIP_1$0$0 == 0x00b1
                           0000B1  1364 _EIP_1	=	0x00b1
                           0000B2  1365 G$EIP_2$0$0 == 0x00b2
                           0000B2  1366 _EIP_2	=	0x00b2
                           0000B3  1367 G$EIP_3$0$0 == 0x00b3
                           0000B3  1368 _EIP_3	=	0x00b3
                           0000B4  1369 G$EIP_4$0$0 == 0x00b4
                           0000B4  1370 _EIP_4	=	0x00b4
                           0000B5  1371 G$EIP_5$0$0 == 0x00b5
                           0000B5  1372 _EIP_5	=	0x00b5
                           0000B6  1373 G$EIP_6$0$0 == 0x00b6
                           0000B6  1374 _EIP_6	=	0x00b6
                           0000B7  1375 G$EIP_7$0$0 == 0x00b7
                           0000B7  1376 _EIP_7	=	0x00b7
                           0000A8  1377 G$IE_0$0$0 == 0x00a8
                           0000A8  1378 _IE_0	=	0x00a8
                           0000A9  1379 G$IE_1$0$0 == 0x00a9
                           0000A9  1380 _IE_1	=	0x00a9
                           0000AA  1381 G$IE_2$0$0 == 0x00aa
                           0000AA  1382 _IE_2	=	0x00aa
                           0000AB  1383 G$IE_3$0$0 == 0x00ab
                           0000AB  1384 _IE_3	=	0x00ab
                           0000AC  1385 G$IE_4$0$0 == 0x00ac
                           0000AC  1386 _IE_4	=	0x00ac
                           0000AD  1387 G$IE_5$0$0 == 0x00ad
                           0000AD  1388 _IE_5	=	0x00ad
                           0000AE  1389 G$IE_6$0$0 == 0x00ae
                           0000AE  1390 _IE_6	=	0x00ae
                           0000AF  1391 G$IE_7$0$0 == 0x00af
                           0000AF  1392 _IE_7	=	0x00af
                           0000AF  1393 G$EA$0$0 == 0x00af
                           0000AF  1394 _EA	=	0x00af
                           0000B8  1395 G$IP_0$0$0 == 0x00b8
                           0000B8  1396 _IP_0	=	0x00b8
                           0000B9  1397 G$IP_1$0$0 == 0x00b9
                           0000B9  1398 _IP_1	=	0x00b9
                           0000BA  1399 G$IP_2$0$0 == 0x00ba
                           0000BA  1400 _IP_2	=	0x00ba
                           0000BB  1401 G$IP_3$0$0 == 0x00bb
                           0000BB  1402 _IP_3	=	0x00bb
                           0000BC  1403 G$IP_4$0$0 == 0x00bc
                           0000BC  1404 _IP_4	=	0x00bc
                           0000BD  1405 G$IP_5$0$0 == 0x00bd
                           0000BD  1406 _IP_5	=	0x00bd
                           0000BE  1407 G$IP_6$0$0 == 0x00be
                           0000BE  1408 _IP_6	=	0x00be
                           0000BF  1409 G$IP_7$0$0 == 0x00bf
                           0000BF  1410 _IP_7	=	0x00bf
                           0000D0  1411 G$P$0$0 == 0x00d0
                           0000D0  1412 _P	=	0x00d0
                           0000D1  1413 G$F1$0$0 == 0x00d1
                           0000D1  1414 _F1	=	0x00d1
                           0000D2  1415 G$OV$0$0 == 0x00d2
                           0000D2  1416 _OV	=	0x00d2
                           0000D3  1417 G$RS0$0$0 == 0x00d3
                           0000D3  1418 _RS0	=	0x00d3
                           0000D4  1419 G$RS1$0$0 == 0x00d4
                           0000D4  1420 _RS1	=	0x00d4
                           0000D5  1421 G$F0$0$0 == 0x00d5
                           0000D5  1422 _F0	=	0x00d5
                           0000D6  1423 G$AC$0$0 == 0x00d6
                           0000D6  1424 _AC	=	0x00d6
                           0000D7  1425 G$CY$0$0 == 0x00d7
                           0000D7  1426 _CY	=	0x00d7
                           0000C8  1427 G$PINA_0$0$0 == 0x00c8
                           0000C8  1428 _PINA_0	=	0x00c8
                           0000C9  1429 G$PINA_1$0$0 == 0x00c9
                           0000C9  1430 _PINA_1	=	0x00c9
                           0000CA  1431 G$PINA_2$0$0 == 0x00ca
                           0000CA  1432 _PINA_2	=	0x00ca
                           0000CB  1433 G$PINA_3$0$0 == 0x00cb
                           0000CB  1434 _PINA_3	=	0x00cb
                           0000CC  1435 G$PINA_4$0$0 == 0x00cc
                           0000CC  1436 _PINA_4	=	0x00cc
                           0000CD  1437 G$PINA_5$0$0 == 0x00cd
                           0000CD  1438 _PINA_5	=	0x00cd
                           0000CE  1439 G$PINA_6$0$0 == 0x00ce
                           0000CE  1440 _PINA_6	=	0x00ce
                           0000CF  1441 G$PINA_7$0$0 == 0x00cf
                           0000CF  1442 _PINA_7	=	0x00cf
                           0000E8  1443 G$PINB_0$0$0 == 0x00e8
                           0000E8  1444 _PINB_0	=	0x00e8
                           0000E9  1445 G$PINB_1$0$0 == 0x00e9
                           0000E9  1446 _PINB_1	=	0x00e9
                           0000EA  1447 G$PINB_2$0$0 == 0x00ea
                           0000EA  1448 _PINB_2	=	0x00ea
                           0000EB  1449 G$PINB_3$0$0 == 0x00eb
                           0000EB  1450 _PINB_3	=	0x00eb
                           0000EC  1451 G$PINB_4$0$0 == 0x00ec
                           0000EC  1452 _PINB_4	=	0x00ec
                           0000ED  1453 G$PINB_5$0$0 == 0x00ed
                           0000ED  1454 _PINB_5	=	0x00ed
                           0000EE  1455 G$PINB_6$0$0 == 0x00ee
                           0000EE  1456 _PINB_6	=	0x00ee
                           0000EF  1457 G$PINB_7$0$0 == 0x00ef
                           0000EF  1458 _PINB_7	=	0x00ef
                           0000F8  1459 G$PINC_0$0$0 == 0x00f8
                           0000F8  1460 _PINC_0	=	0x00f8
                           0000F9  1461 G$PINC_1$0$0 == 0x00f9
                           0000F9  1462 _PINC_1	=	0x00f9
                           0000FA  1463 G$PINC_2$0$0 == 0x00fa
                           0000FA  1464 _PINC_2	=	0x00fa
                           0000FB  1465 G$PINC_3$0$0 == 0x00fb
                           0000FB  1466 _PINC_3	=	0x00fb
                           0000FC  1467 G$PINC_4$0$0 == 0x00fc
                           0000FC  1468 _PINC_4	=	0x00fc
                           0000FD  1469 G$PINC_5$0$0 == 0x00fd
                           0000FD  1470 _PINC_5	=	0x00fd
                           0000FE  1471 G$PINC_6$0$0 == 0x00fe
                           0000FE  1472 _PINC_6	=	0x00fe
                           0000FF  1473 G$PINC_7$0$0 == 0x00ff
                           0000FF  1474 _PINC_7	=	0x00ff
                           000080  1475 G$PORTA_0$0$0 == 0x0080
                           000080  1476 _PORTA_0	=	0x0080
                           000081  1477 G$PORTA_1$0$0 == 0x0081
                           000081  1478 _PORTA_1	=	0x0081
                           000082  1479 G$PORTA_2$0$0 == 0x0082
                           000082  1480 _PORTA_2	=	0x0082
                           000083  1481 G$PORTA_3$0$0 == 0x0083
                           000083  1482 _PORTA_3	=	0x0083
                           000084  1483 G$PORTA_4$0$0 == 0x0084
                           000084  1484 _PORTA_4	=	0x0084
                           000085  1485 G$PORTA_5$0$0 == 0x0085
                           000085  1486 _PORTA_5	=	0x0085
                           000086  1487 G$PORTA_6$0$0 == 0x0086
                           000086  1488 _PORTA_6	=	0x0086
                           000087  1489 G$PORTA_7$0$0 == 0x0087
                           000087  1490 _PORTA_7	=	0x0087
                           000088  1491 G$PORTB_0$0$0 == 0x0088
                           000088  1492 _PORTB_0	=	0x0088
                           000089  1493 G$PORTB_1$0$0 == 0x0089
                           000089  1494 _PORTB_1	=	0x0089
                           00008A  1495 G$PORTB_2$0$0 == 0x008a
                           00008A  1496 _PORTB_2	=	0x008a
                           00008B  1497 G$PORTB_3$0$0 == 0x008b
                           00008B  1498 _PORTB_3	=	0x008b
                           00008C  1499 G$PORTB_4$0$0 == 0x008c
                           00008C  1500 _PORTB_4	=	0x008c
                           00008D  1501 G$PORTB_5$0$0 == 0x008d
                           00008D  1502 _PORTB_5	=	0x008d
                           00008E  1503 G$PORTB_6$0$0 == 0x008e
                           00008E  1504 _PORTB_6	=	0x008e
                           00008F  1505 G$PORTB_7$0$0 == 0x008f
                           00008F  1506 _PORTB_7	=	0x008f
                           000090  1507 G$PORTC_0$0$0 == 0x0090
                           000090  1508 _PORTC_0	=	0x0090
                           000091  1509 G$PORTC_1$0$0 == 0x0091
                           000091  1510 _PORTC_1	=	0x0091
                           000092  1511 G$PORTC_2$0$0 == 0x0092
                           000092  1512 _PORTC_2	=	0x0092
                           000093  1513 G$PORTC_3$0$0 == 0x0093
                           000093  1514 _PORTC_3	=	0x0093
                           000094  1515 G$PORTC_4$0$0 == 0x0094
                           000094  1516 _PORTC_4	=	0x0094
                           000095  1517 G$PORTC_5$0$0 == 0x0095
                           000095  1518 _PORTC_5	=	0x0095
                           000096  1519 G$PORTC_6$0$0 == 0x0096
                           000096  1520 _PORTC_6	=	0x0096
                           000097  1521 G$PORTC_7$0$0 == 0x0097
                           000097  1522 _PORTC_7	=	0x0097
                                   1523 ;--------------------------------------------------------
                                   1524 ; overlayable register banks
                                   1525 ;--------------------------------------------------------
                                   1526 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1527 	.ds 8
                                   1528 ;--------------------------------------------------------
                                   1529 ; internal ram data
                                   1530 ;--------------------------------------------------------
                                   1531 	.area DSEG    (DATA)
                           000000  1532 Lpktframing.packetFraming$sloc0$1$0==.
      000041                       1533 _packetFraming_sloc0_1_0:
      000041                       1534 	.ds 4
                           000004  1535 Lpktframing.packetFraming$sloc1$1$0==.
      000045                       1536 _packetFraming_sloc1_1_0:
      000045                       1537 	.ds 1
                           000005  1538 Lpktframing.packetFraming$sloc2$1$0==.
      000046                       1539 _packetFraming_sloc2_1_0:
      000046                       1540 	.ds 1
                           000006  1541 Lpktframing.packetFraming$sloc3$1$0==.
      000047                       1542 _packetFraming_sloc3_1_0:
      000047                       1543 	.ds 1
                           000007  1544 Lpktframing.packetFraming$sloc4$1$0==.
      000048                       1545 _packetFraming_sloc4_1_0:
      000048                       1546 	.ds 1
                           000008  1547 Lpktframing.packetFraming$sloc5$1$0==.
      000049                       1548 _packetFraming_sloc5_1_0:
      000049                       1549 	.ds 2
                           00000A  1550 Lpktframing.packetFraming$sloc6$1$0==.
      00004B                       1551 _packetFraming_sloc6_1_0:
      00004B                       1552 	.ds 2
                                   1553 ;--------------------------------------------------------
                                   1554 ; overlayable items in internal ram 
                                   1555 ;--------------------------------------------------------
                                   1556 	.area	OSEG    (OVR,DATA)
                           000000  1557 Lpktframing.swap_variables$b$1$370==.
      000064                       1558 _swap_variables_PARM_2:
      000064                       1559 	.ds 3
                                   1560 ;--------------------------------------------------------
                                   1561 ; indirectly addressable internal ram data
                                   1562 ;--------------------------------------------------------
                                   1563 	.area ISEG    (DATA)
                                   1564 ;--------------------------------------------------------
                                   1565 ; absolute internal ram data
                                   1566 ;--------------------------------------------------------
                                   1567 	.area IABS    (ABS,DATA)
                                   1568 	.area IABS    (ABS,DATA)
                                   1569 ;--------------------------------------------------------
                                   1570 ; bit data
                                   1571 ;--------------------------------------------------------
                                   1572 	.area BSEG    (BIT)
                                   1573 ;--------------------------------------------------------
                                   1574 ; paged external ram data
                                   1575 ;--------------------------------------------------------
                                   1576 	.area PSEG    (PAG,XDATA)
                                   1577 ;--------------------------------------------------------
                                   1578 ; external ram data
                                   1579 ;--------------------------------------------------------
                                   1580 	.area XSEG    (XDATA)
                           007020  1581 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1582 _ADCCH0VAL0	=	0x7020
                           007021  1583 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1584 _ADCCH0VAL1	=	0x7021
                           007020  1585 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1586 _ADCCH0VAL	=	0x7020
                           007022  1587 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1588 _ADCCH1VAL0	=	0x7022
                           007023  1589 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1590 _ADCCH1VAL1	=	0x7023
                           007022  1591 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1592 _ADCCH1VAL	=	0x7022
                           007024  1593 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1594 _ADCCH2VAL0	=	0x7024
                           007025  1595 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1596 _ADCCH2VAL1	=	0x7025
                           007024  1597 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1598 _ADCCH2VAL	=	0x7024
                           007026  1599 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1600 _ADCCH3VAL0	=	0x7026
                           007027  1601 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1602 _ADCCH3VAL1	=	0x7027
                           007026  1603 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1604 _ADCCH3VAL	=	0x7026
                           007028  1605 G$ADCTUNE0$0$0 == 0x7028
                           007028  1606 _ADCTUNE0	=	0x7028
                           007029  1607 G$ADCTUNE1$0$0 == 0x7029
                           007029  1608 _ADCTUNE1	=	0x7029
                           00702A  1609 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1610 _ADCTUNE2	=	0x702a
                           007010  1611 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1612 _DMA0ADDR0	=	0x7010
                           007011  1613 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1614 _DMA0ADDR1	=	0x7011
                           007010  1615 G$DMA0ADDR$0$0 == 0x7010
                           007010  1616 _DMA0ADDR	=	0x7010
                           007014  1617 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1618 _DMA0CONFIG	=	0x7014
                           007012  1619 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1620 _DMA1ADDR0	=	0x7012
                           007013  1621 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1622 _DMA1ADDR1	=	0x7013
                           007012  1623 G$DMA1ADDR$0$0 == 0x7012
                           007012  1624 _DMA1ADDR	=	0x7012
                           007015  1625 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1626 _DMA1CONFIG	=	0x7015
                           007070  1627 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1628 _FRCOSCCONFIG	=	0x7070
                           007071  1629 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1630 _FRCOSCCTRL	=	0x7071
                           007076  1631 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1632 _FRCOSCFREQ0	=	0x7076
                           007077  1633 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1634 _FRCOSCFREQ1	=	0x7077
                           007076  1635 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1636 _FRCOSCFREQ	=	0x7076
                           007072  1637 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1638 _FRCOSCKFILT0	=	0x7072
                           007073  1639 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1640 _FRCOSCKFILT1	=	0x7073
                           007072  1641 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1642 _FRCOSCKFILT	=	0x7072
                           007078  1643 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1644 _FRCOSCPER0	=	0x7078
                           007079  1645 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1646 _FRCOSCPER1	=	0x7079
                           007078  1647 G$FRCOSCPER$0$0 == 0x7078
                           007078  1648 _FRCOSCPER	=	0x7078
                           007074  1649 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1650 _FRCOSCREF0	=	0x7074
                           007075  1651 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1652 _FRCOSCREF1	=	0x7075
                           007074  1653 G$FRCOSCREF$0$0 == 0x7074
                           007074  1654 _FRCOSCREF	=	0x7074
                           007007  1655 G$ANALOGA$0$0 == 0x7007
                           007007  1656 _ANALOGA	=	0x7007
                           00700C  1657 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1658 _GPIOENABLE	=	0x700c
                           007003  1659 G$EXTIRQ$0$0 == 0x7003
                           007003  1660 _EXTIRQ	=	0x7003
                           007000  1661 G$INTCHGA$0$0 == 0x7000
                           007000  1662 _INTCHGA	=	0x7000
                           007001  1663 G$INTCHGB$0$0 == 0x7001
                           007001  1664 _INTCHGB	=	0x7001
                           007002  1665 G$INTCHGC$0$0 == 0x7002
                           007002  1666 _INTCHGC	=	0x7002
                           007008  1667 G$PALTA$0$0 == 0x7008
                           007008  1668 _PALTA	=	0x7008
                           007009  1669 G$PALTB$0$0 == 0x7009
                           007009  1670 _PALTB	=	0x7009
                           00700A  1671 G$PALTC$0$0 == 0x700a
                           00700A  1672 _PALTC	=	0x700a
                           007046  1673 G$PALTRADIO$0$0 == 0x7046
                           007046  1674 _PALTRADIO	=	0x7046
                           007004  1675 G$PINCHGA$0$0 == 0x7004
                           007004  1676 _PINCHGA	=	0x7004
                           007005  1677 G$PINCHGB$0$0 == 0x7005
                           007005  1678 _PINCHGB	=	0x7005
                           007006  1679 G$PINCHGC$0$0 == 0x7006
                           007006  1680 _PINCHGC	=	0x7006
                           00700B  1681 G$PINSEL$0$0 == 0x700b
                           00700B  1682 _PINSEL	=	0x700b
                           007060  1683 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1684 _LPOSCCONFIG	=	0x7060
                           007066  1685 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1686 _LPOSCFREQ0	=	0x7066
                           007067  1687 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1688 _LPOSCFREQ1	=	0x7067
                           007066  1689 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1690 _LPOSCFREQ	=	0x7066
                           007062  1691 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1692 _LPOSCKFILT0	=	0x7062
                           007063  1693 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1694 _LPOSCKFILT1	=	0x7063
                           007062  1695 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1696 _LPOSCKFILT	=	0x7062
                           007068  1697 G$LPOSCPER0$0$0 == 0x7068
                           007068  1698 _LPOSCPER0	=	0x7068
                           007069  1699 G$LPOSCPER1$0$0 == 0x7069
                           007069  1700 _LPOSCPER1	=	0x7069
                           007068  1701 G$LPOSCPER$0$0 == 0x7068
                           007068  1702 _LPOSCPER	=	0x7068
                           007064  1703 G$LPOSCREF0$0$0 == 0x7064
                           007064  1704 _LPOSCREF0	=	0x7064
                           007065  1705 G$LPOSCREF1$0$0 == 0x7065
                           007065  1706 _LPOSCREF1	=	0x7065
                           007064  1707 G$LPOSCREF$0$0 == 0x7064
                           007064  1708 _LPOSCREF	=	0x7064
                           007054  1709 G$LPXOSCGM$0$0 == 0x7054
                           007054  1710 _LPXOSCGM	=	0x7054
                           007F01  1711 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1712 _MISCCTRL	=	0x7f01
                           007053  1713 G$OSCCALIB$0$0 == 0x7053
                           007053  1714 _OSCCALIB	=	0x7053
                           007050  1715 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1716 _OSCFORCERUN	=	0x7050
                           007052  1717 G$OSCREADY$0$0 == 0x7052
                           007052  1718 _OSCREADY	=	0x7052
                           007051  1719 G$OSCRUN$0$0 == 0x7051
                           007051  1720 _OSCRUN	=	0x7051
                           007040  1721 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1722 _RADIOFDATAADDR0	=	0x7040
                           007041  1723 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1724 _RADIOFDATAADDR1	=	0x7041
                           007040  1725 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1726 _RADIOFDATAADDR	=	0x7040
                           007042  1727 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1728 _RADIOFSTATADDR0	=	0x7042
                           007043  1729 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1730 _RADIOFSTATADDR1	=	0x7043
                           007042  1731 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1732 _RADIOFSTATADDR	=	0x7042
                           007044  1733 G$RADIOMUX$0$0 == 0x7044
                           007044  1734 _RADIOMUX	=	0x7044
                           007084  1735 G$SCRATCH0$0$0 == 0x7084
                           007084  1736 _SCRATCH0	=	0x7084
                           007085  1737 G$SCRATCH1$0$0 == 0x7085
                           007085  1738 _SCRATCH1	=	0x7085
                           007086  1739 G$SCRATCH2$0$0 == 0x7086
                           007086  1740 _SCRATCH2	=	0x7086
                           007087  1741 G$SCRATCH3$0$0 == 0x7087
                           007087  1742 _SCRATCH3	=	0x7087
                           007F00  1743 G$SILICONREV$0$0 == 0x7f00
                           007F00  1744 _SILICONREV	=	0x7f00
                           007F19  1745 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1746 _XTALAMPL	=	0x7f19
                           007F18  1747 G$XTALOSC$0$0 == 0x7f18
                           007F18  1748 _XTALOSC	=	0x7f18
                           007F1A  1749 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1750 _XTALREADY	=	0x7f1a
                           00FC06  1751 Fpktframing$flash_deviceid$0$0 == 0xfc06
                           00FC06  1752 _flash_deviceid	=	0xfc06
                           00FC00  1753 Fpktframing$flash_calsector$0$0 == 0xfc00
                           00FC00  1754 _flash_calsector	=	0xfc00
                           004114  1755 G$AX5043_AFSKCTRL$0$0 == 0x4114
                           004114  1756 _AX5043_AFSKCTRL	=	0x4114
                           004113  1757 G$AX5043_AFSKMARK0$0$0 == 0x4113
                           004113  1758 _AX5043_AFSKMARK0	=	0x4113
                           004112  1759 G$AX5043_AFSKMARK1$0$0 == 0x4112
                           004112  1760 _AX5043_AFSKMARK1	=	0x4112
                           004111  1761 G$AX5043_AFSKSPACE0$0$0 == 0x4111
                           004111  1762 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1763 G$AX5043_AFSKSPACE1$0$0 == 0x4110
                           004110  1764 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1765 G$AX5043_AGCCOUNTER$0$0 == 0x4043
                           004043  1766 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1767 G$AX5043_AMPLFILTER$0$0 == 0x4115
                           004115  1768 _AX5043_AMPLFILTER	=	0x4115
                           004189  1769 G$AX5043_BBOFFSCAP$0$0 == 0x4189
                           004189  1770 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1771 G$AX5043_BBTUNE$0$0 == 0x4188
                           004188  1772 _AX5043_BBTUNE	=	0x4188
                           004041  1773 G$AX5043_BGNDRSSI$0$0 == 0x4041
                           004041  1774 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1775 G$AX5043_BGNDRSSIGAIN$0$0 == 0x422e
                           00422E  1776 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1777 G$AX5043_BGNDRSSITHR$0$0 == 0x422f
                           00422F  1778 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1779 G$AX5043_CRCINIT0$0$0 == 0x4017
                           004017  1780 _AX5043_CRCINIT0	=	0x4017
                           004016  1781 G$AX5043_CRCINIT1$0$0 == 0x4016
                           004016  1782 _AX5043_CRCINIT1	=	0x4016
                           004015  1783 G$AX5043_CRCINIT2$0$0 == 0x4015
                           004015  1784 _AX5043_CRCINIT2	=	0x4015
                           004014  1785 G$AX5043_CRCINIT3$0$0 == 0x4014
                           004014  1786 _AX5043_CRCINIT3	=	0x4014
                           004332  1787 G$AX5043_DACCONFIG$0$0 == 0x4332
                           004332  1788 _AX5043_DACCONFIG	=	0x4332
                           004331  1789 G$AX5043_DACVALUE0$0$0 == 0x4331
                           004331  1790 _AX5043_DACVALUE0	=	0x4331
                           004330  1791 G$AX5043_DACVALUE1$0$0 == 0x4330
                           004330  1792 _AX5043_DACVALUE1	=	0x4330
                           004102  1793 G$AX5043_DECIMATION$0$0 == 0x4102
                           004102  1794 _AX5043_DECIMATION	=	0x4102
                           004042  1795 G$AX5043_DIVERSITY$0$0 == 0x4042
                           004042  1796 _AX5043_DIVERSITY	=	0x4042
                           004011  1797 G$AX5043_ENCODING$0$0 == 0x4011
                           004011  1798 _AX5043_ENCODING	=	0x4011
                           004018  1799 G$AX5043_FEC$0$0 == 0x4018
                           004018  1800 _AX5043_FEC	=	0x4018
                           00401A  1801 G$AX5043_FECSTATUS$0$0 == 0x401a
                           00401A  1802 _AX5043_FECSTATUS	=	0x401a
                           004019  1803 G$AX5043_FECSYNC$0$0 == 0x4019
                           004019  1804 _AX5043_FECSYNC	=	0x4019
                           00402B  1805 G$AX5043_FIFOCOUNT0$0$0 == 0x402b
                           00402B  1806 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1807 G$AX5043_FIFOCOUNT1$0$0 == 0x402a
                           00402A  1808 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1809 G$AX5043_FIFODATA$0$0 == 0x4029
                           004029  1810 _AX5043_FIFODATA	=	0x4029
                           00402D  1811 G$AX5043_FIFOFREE0$0$0 == 0x402d
                           00402D  1812 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1813 G$AX5043_FIFOFREE1$0$0 == 0x402c
                           00402C  1814 _AX5043_FIFOFREE1	=	0x402c
                           004028  1815 G$AX5043_FIFOSTAT$0$0 == 0x4028
                           004028  1816 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1817 G$AX5043_FIFOTHRESH0$0$0 == 0x402f
                           00402F  1818 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1819 G$AX5043_FIFOTHRESH1$0$0 == 0x402e
                           00402E  1820 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1821 G$AX5043_FRAMING$0$0 == 0x4012
                           004012  1822 _AX5043_FRAMING	=	0x4012
                           004037  1823 G$AX5043_FREQA0$0$0 == 0x4037
                           004037  1824 _AX5043_FREQA0	=	0x4037
                           004036  1825 G$AX5043_FREQA1$0$0 == 0x4036
                           004036  1826 _AX5043_FREQA1	=	0x4036
                           004035  1827 G$AX5043_FREQA2$0$0 == 0x4035
                           004035  1828 _AX5043_FREQA2	=	0x4035
                           004034  1829 G$AX5043_FREQA3$0$0 == 0x4034
                           004034  1830 _AX5043_FREQA3	=	0x4034
                           00403F  1831 G$AX5043_FREQB0$0$0 == 0x403f
                           00403F  1832 _AX5043_FREQB0	=	0x403f
                           00403E  1833 G$AX5043_FREQB1$0$0 == 0x403e
                           00403E  1834 _AX5043_FREQB1	=	0x403e
                           00403D  1835 G$AX5043_FREQB2$0$0 == 0x403d
                           00403D  1836 _AX5043_FREQB2	=	0x403d
                           00403C  1837 G$AX5043_FREQB3$0$0 == 0x403c
                           00403C  1838 _AX5043_FREQB3	=	0x403c
                           004163  1839 G$AX5043_FSKDEV0$0$0 == 0x4163
                           004163  1840 _AX5043_FSKDEV0	=	0x4163
                           004162  1841 G$AX5043_FSKDEV1$0$0 == 0x4162
                           004162  1842 _AX5043_FSKDEV1	=	0x4162
                           004161  1843 G$AX5043_FSKDEV2$0$0 == 0x4161
                           004161  1844 _AX5043_FSKDEV2	=	0x4161
                           00410D  1845 G$AX5043_FSKDMAX0$0$0 == 0x410d
                           00410D  1846 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1847 G$AX5043_FSKDMAX1$0$0 == 0x410c
                           00410C  1848 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1849 G$AX5043_FSKDMIN0$0$0 == 0x410f
                           00410F  1850 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1851 G$AX5043_FSKDMIN1$0$0 == 0x410e
                           00410E  1852 _AX5043_FSKDMIN1	=	0x410e
                           004309  1853 G$AX5043_GPADC13VALUE0$0$0 == 0x4309
                           004309  1854 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1855 G$AX5043_GPADC13VALUE1$0$0 == 0x4308
                           004308  1856 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1857 G$AX5043_GPADCCTRL$0$0 == 0x4300
                           004300  1858 _AX5043_GPADCCTRL	=	0x4300
                           004301  1859 G$AX5043_GPADCPERIOD$0$0 == 0x4301
                           004301  1860 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1861 G$AX5043_IFFREQ0$0$0 == 0x4101
                           004101  1862 _AX5043_IFFREQ0	=	0x4101
                           004100  1863 G$AX5043_IFFREQ1$0$0 == 0x4100
                           004100  1864 _AX5043_IFFREQ1	=	0x4100
                           00400B  1865 G$AX5043_IRQINVERSION0$0$0 == 0x400b
                           00400B  1866 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1867 G$AX5043_IRQINVERSION1$0$0 == 0x400a
                           00400A  1868 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1869 G$AX5043_IRQMASK0$0$0 == 0x4007
                           004007  1870 _AX5043_IRQMASK0	=	0x4007
                           004006  1871 G$AX5043_IRQMASK1$0$0 == 0x4006
                           004006  1872 _AX5043_IRQMASK1	=	0x4006
                           00400D  1873 G$AX5043_IRQREQUEST0$0$0 == 0x400d
                           00400D  1874 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1875 G$AX5043_IRQREQUEST1$0$0 == 0x400c
                           00400C  1876 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1877 G$AX5043_LPOSCCONFIG$0$0 == 0x4310
                           004310  1878 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1879 G$AX5043_LPOSCFREQ0$0$0 == 0x4317
                           004317  1880 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1881 G$AX5043_LPOSCFREQ1$0$0 == 0x4316
                           004316  1882 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1883 G$AX5043_LPOSCKFILT0$0$0 == 0x4313
                           004313  1884 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1885 G$AX5043_LPOSCKFILT1$0$0 == 0x4312
                           004312  1886 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1887 G$AX5043_LPOSCPER0$0$0 == 0x4319
                           004319  1888 _AX5043_LPOSCPER0	=	0x4319
                           004318  1889 G$AX5043_LPOSCPER1$0$0 == 0x4318
                           004318  1890 _AX5043_LPOSCPER1	=	0x4318
                           004315  1891 G$AX5043_LPOSCREF0$0$0 == 0x4315
                           004315  1892 _AX5043_LPOSCREF0	=	0x4315
                           004314  1893 G$AX5043_LPOSCREF1$0$0 == 0x4314
                           004314  1894 _AX5043_LPOSCREF1	=	0x4314
                           004311  1895 G$AX5043_LPOSCSTATUS$0$0 == 0x4311
                           004311  1896 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1897 G$AX5043_MATCH0LEN$0$0 == 0x4214
                           004214  1898 _AX5043_MATCH0LEN	=	0x4214
                           004216  1899 G$AX5043_MATCH0MAX$0$0 == 0x4216
                           004216  1900 _AX5043_MATCH0MAX	=	0x4216
                           004215  1901 G$AX5043_MATCH0MIN$0$0 == 0x4215
                           004215  1902 _AX5043_MATCH0MIN	=	0x4215
                           004213  1903 G$AX5043_MATCH0PAT0$0$0 == 0x4213
                           004213  1904 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1905 G$AX5043_MATCH0PAT1$0$0 == 0x4212
                           004212  1906 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1907 G$AX5043_MATCH0PAT2$0$0 == 0x4211
                           004211  1908 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1909 G$AX5043_MATCH0PAT3$0$0 == 0x4210
                           004210  1910 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1911 G$AX5043_MATCH1LEN$0$0 == 0x421c
                           00421C  1912 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1913 G$AX5043_MATCH1MAX$0$0 == 0x421e
                           00421E  1914 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1915 G$AX5043_MATCH1MIN$0$0 == 0x421d
                           00421D  1916 _AX5043_MATCH1MIN	=	0x421d
                           004219  1917 G$AX5043_MATCH1PAT0$0$0 == 0x4219
                           004219  1918 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1919 G$AX5043_MATCH1PAT1$0$0 == 0x4218
                           004218  1920 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1921 G$AX5043_MAXDROFFSET0$0$0 == 0x4108
                           004108  1922 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1923 G$AX5043_MAXDROFFSET1$0$0 == 0x4107
                           004107  1924 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1925 G$AX5043_MAXDROFFSET2$0$0 == 0x4106
                           004106  1926 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1927 G$AX5043_MAXRFOFFSET0$0$0 == 0x410b
                           00410B  1928 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1929 G$AX5043_MAXRFOFFSET1$0$0 == 0x410a
                           00410A  1930 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1931 G$AX5043_MAXRFOFFSET2$0$0 == 0x4109
                           004109  1932 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1933 G$AX5043_MODCFGA$0$0 == 0x4164
                           004164  1934 _AX5043_MODCFGA	=	0x4164
                           004160  1935 G$AX5043_MODCFGF$0$0 == 0x4160
                           004160  1936 _AX5043_MODCFGF	=	0x4160
                           004F5F  1937 G$AX5043_MODCFGP$0$0 == 0x4f5f
                           004F5F  1938 _AX5043_MODCFGP	=	0x4f5f
                           004010  1939 G$AX5043_MODULATION$0$0 == 0x4010
                           004010  1940 _AX5043_MODULATION	=	0x4010
                           004025  1941 G$AX5043_PINFUNCANTSEL$0$0 == 0x4025
                           004025  1942 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1943 G$AX5043_PINFUNCDATA$0$0 == 0x4023
                           004023  1944 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1945 G$AX5043_PINFUNCDCLK$0$0 == 0x4022
                           004022  1946 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1947 G$AX5043_PINFUNCIRQ$0$0 == 0x4024
                           004024  1948 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1949 G$AX5043_PINFUNCPWRAMP$0$0 == 0x4026
                           004026  1950 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1951 G$AX5043_PINFUNCSYSCLK$0$0 == 0x4021
                           004021  1952 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1953 G$AX5043_PINSTATE$0$0 == 0x4020
                           004020  1954 _AX5043_PINSTATE	=	0x4020
                           004233  1955 G$AX5043_PKTACCEPTFLAGS$0$0 == 0x4233
                           004233  1956 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1957 G$AX5043_PKTCHUNKSIZE$0$0 == 0x4230
                           004230  1958 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1959 G$AX5043_PKTMISCFLAGS$0$0 == 0x4231
                           004231  1960 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1961 G$AX5043_PKTSTOREFLAGS$0$0 == 0x4232
                           004232  1962 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1963 G$AX5043_PLLCPI$0$0 == 0x4031
                           004031  1964 _AX5043_PLLCPI	=	0x4031
                           004039  1965 G$AX5043_PLLCPIBOOST$0$0 == 0x4039
                           004039  1966 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1967 G$AX5043_PLLLOCKDET$0$0 == 0x4182
                           004182  1968 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1969 G$AX5043_PLLLOOP$0$0 == 0x4030
                           004030  1970 _AX5043_PLLLOOP	=	0x4030
                           004038  1971 G$AX5043_PLLLOOPBOOST$0$0 == 0x4038
                           004038  1972 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1973 G$AX5043_PLLRANGINGA$0$0 == 0x4033
                           004033  1974 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1975 G$AX5043_PLLRANGINGB$0$0 == 0x403b
                           00403B  1976 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1977 G$AX5043_PLLRNGCLK$0$0 == 0x4183
                           004183  1978 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1979 G$AX5043_PLLVCODIV$0$0 == 0x4032
                           004032  1980 _AX5043_PLLVCODIV	=	0x4032
                           004180  1981 G$AX5043_PLLVCOI$0$0 == 0x4180
                           004180  1982 _AX5043_PLLVCOI	=	0x4180
                           004181  1983 G$AX5043_PLLVCOIR$0$0 == 0x4181
                           004181  1984 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1985 G$AX5043_POWCTRL1$0$0 == 0x4f08
                           004F08  1986 _AX5043_POWCTRL1	=	0x4f08
                           004005  1987 G$AX5043_POWIRQMASK$0$0 == 0x4005
                           004005  1988 _AX5043_POWIRQMASK	=	0x4005
                           004003  1989 G$AX5043_POWSTAT$0$0 == 0x4003
                           004003  1990 _AX5043_POWSTAT	=	0x4003
                           004004  1991 G$AX5043_POWSTICKYSTAT$0$0 == 0x4004
                           004004  1992 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1993 G$AX5043_PWRAMP$0$0 == 0x4027
                           004027  1994 _AX5043_PWRAMP	=	0x4027
                           004002  1995 G$AX5043_PWRMODE$0$0 == 0x4002
                           004002  1996 _AX5043_PWRMODE	=	0x4002
                           004009  1997 G$AX5043_RADIOEVENTMASK0$0$0 == 0x4009
                           004009  1998 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1999 G$AX5043_RADIOEVENTMASK1$0$0 == 0x4008
                           004008  2000 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  2001 G$AX5043_RADIOEVENTREQ0$0$0 == 0x400f
                           00400F  2002 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  2003 G$AX5043_RADIOEVENTREQ1$0$0 == 0x400e
                           00400E  2004 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  2005 G$AX5043_RADIOSTATE$0$0 == 0x401c
                           00401C  2006 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  2007 G$AX5043_REF$0$0 == 0x4f0d
                           004F0D  2008 _AX5043_REF	=	0x4f0d
                           004040  2009 G$AX5043_RSSI$0$0 == 0x4040
                           004040  2010 _AX5043_RSSI	=	0x4040
                           00422D  2011 G$AX5043_RSSIABSTHR$0$0 == 0x422d
                           00422D  2012 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  2013 G$AX5043_RSSIREFERENCE$0$0 == 0x422c
                           00422C  2014 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  2015 G$AX5043_RXDATARATE0$0$0 == 0x4105
                           004105  2016 _AX5043_RXDATARATE0	=	0x4105
                           004104  2017 G$AX5043_RXDATARATE1$0$0 == 0x4104
                           004104  2018 _AX5043_RXDATARATE1	=	0x4104
                           004103  2019 G$AX5043_RXDATARATE2$0$0 == 0x4103
                           004103  2020 _AX5043_RXDATARATE2	=	0x4103
                           004001  2021 G$AX5043_SCRATCH$0$0 == 0x4001
                           004001  2022 _AX5043_SCRATCH	=	0x4001
                           004000  2023 G$AX5043_SILICONREVISION$0$0 == 0x4000
                           004000  2024 _AX5043_SILICONREVISION	=	0x4000
                           00405B  2025 G$AX5043_TIMER0$0$0 == 0x405b
                           00405B  2026 _AX5043_TIMER0	=	0x405b
                           00405A  2027 G$AX5043_TIMER1$0$0 == 0x405a
                           00405A  2028 _AX5043_TIMER1	=	0x405a
                           004059  2029 G$AX5043_TIMER2$0$0 == 0x4059
                           004059  2030 _AX5043_TIMER2	=	0x4059
                           004227  2031 G$AX5043_TMGRXAGC$0$0 == 0x4227
                           004227  2032 _AX5043_TMGRXAGC	=	0x4227
                           004223  2033 G$AX5043_TMGRXBOOST$0$0 == 0x4223
                           004223  2034 _AX5043_TMGRXBOOST	=	0x4223
                           004226  2035 G$AX5043_TMGRXCOARSEAGC$0$0 == 0x4226
                           004226  2036 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  2037 G$AX5043_TMGRXOFFSACQ$0$0 == 0x4225
                           004225  2038 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  2039 G$AX5043_TMGRXPREAMBLE1$0$0 == 0x4229
                           004229  2040 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  2041 G$AX5043_TMGRXPREAMBLE2$0$0 == 0x422a
                           00422A  2042 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  2043 G$AX5043_TMGRXPREAMBLE3$0$0 == 0x422b
                           00422B  2044 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  2045 G$AX5043_TMGRXRSSI$0$0 == 0x4228
                           004228  2046 _AX5043_TMGRXRSSI	=	0x4228
                           004224  2047 G$AX5043_TMGRXSETTLE$0$0 == 0x4224
                           004224  2048 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  2049 G$AX5043_TMGTXBOOST$0$0 == 0x4220
                           004220  2050 _AX5043_TMGTXBOOST	=	0x4220
                           004221  2051 G$AX5043_TMGTXSETTLE$0$0 == 0x4221
                           004221  2052 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  2053 G$AX5043_TRKAFSKDEMOD0$0$0 == 0x4055
                           004055  2054 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  2055 G$AX5043_TRKAFSKDEMOD1$0$0 == 0x4054
                           004054  2056 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  2057 G$AX5043_TRKAMPLITUDE0$0$0 == 0x4049
                           004049  2058 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  2059 G$AX5043_TRKAMPLITUDE1$0$0 == 0x4048
                           004048  2060 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  2061 G$AX5043_TRKDATARATE0$0$0 == 0x4047
                           004047  2062 _AX5043_TRKDATARATE0	=	0x4047
                           004046  2063 G$AX5043_TRKDATARATE1$0$0 == 0x4046
                           004046  2064 _AX5043_TRKDATARATE1	=	0x4046
                           004045  2065 G$AX5043_TRKDATARATE2$0$0 == 0x4045
                           004045  2066 _AX5043_TRKDATARATE2	=	0x4045
                           004051  2067 G$AX5043_TRKFREQ0$0$0 == 0x4051
                           004051  2068 _AX5043_TRKFREQ0	=	0x4051
                           004050  2069 G$AX5043_TRKFREQ1$0$0 == 0x4050
                           004050  2070 _AX5043_TRKFREQ1	=	0x4050
                           004053  2071 G$AX5043_TRKFSKDEMOD0$0$0 == 0x4053
                           004053  2072 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  2073 G$AX5043_TRKFSKDEMOD1$0$0 == 0x4052
                           004052  2074 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  2075 G$AX5043_TRKPHASE0$0$0 == 0x404b
                           00404B  2076 _AX5043_TRKPHASE0	=	0x404b
                           00404A  2077 G$AX5043_TRKPHASE1$0$0 == 0x404a
                           00404A  2078 _AX5043_TRKPHASE1	=	0x404a
                           00404F  2079 G$AX5043_TRKRFFREQ0$0$0 == 0x404f
                           00404F  2080 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  2081 G$AX5043_TRKRFFREQ1$0$0 == 0x404e
                           00404E  2082 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  2083 G$AX5043_TRKRFFREQ2$0$0 == 0x404d
                           00404D  2084 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  2085 G$AX5043_TXPWRCOEFFA0$0$0 == 0x4169
                           004169  2086 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  2087 G$AX5043_TXPWRCOEFFA1$0$0 == 0x4168
                           004168  2088 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  2089 G$AX5043_TXPWRCOEFFB0$0$0 == 0x416b
                           00416B  2090 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  2091 G$AX5043_TXPWRCOEFFB1$0$0 == 0x416a
                           00416A  2092 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  2093 G$AX5043_TXPWRCOEFFC0$0$0 == 0x416d
                           00416D  2094 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  2095 G$AX5043_TXPWRCOEFFC1$0$0 == 0x416c
                           00416C  2096 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  2097 G$AX5043_TXPWRCOEFFD0$0$0 == 0x416f
                           00416F  2098 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  2099 G$AX5043_TXPWRCOEFFD1$0$0 == 0x416e
                           00416E  2100 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  2101 G$AX5043_TXPWRCOEFFE0$0$0 == 0x4171
                           004171  2102 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  2103 G$AX5043_TXPWRCOEFFE1$0$0 == 0x4170
                           004170  2104 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  2105 G$AX5043_TXRATE0$0$0 == 0x4167
                           004167  2106 _AX5043_TXRATE0	=	0x4167
                           004166  2107 G$AX5043_TXRATE1$0$0 == 0x4166
                           004166  2108 _AX5043_TXRATE1	=	0x4166
                           004165  2109 G$AX5043_TXRATE2$0$0 == 0x4165
                           004165  2110 _AX5043_TXRATE2	=	0x4165
                           00406B  2111 G$AX5043_WAKEUP0$0$0 == 0x406b
                           00406B  2112 _AX5043_WAKEUP0	=	0x406b
                           00406A  2113 G$AX5043_WAKEUP1$0$0 == 0x406a
                           00406A  2114 _AX5043_WAKEUP1	=	0x406a
                           00406D  2115 G$AX5043_WAKEUPFREQ0$0$0 == 0x406d
                           00406D  2116 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  2117 G$AX5043_WAKEUPFREQ1$0$0 == 0x406c
                           00406C  2118 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  2119 G$AX5043_WAKEUPTIMER0$0$0 == 0x4069
                           004069  2120 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  2121 G$AX5043_WAKEUPTIMER1$0$0 == 0x4068
                           004068  2122 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  2123 G$AX5043_WAKEUPXOEARLY$0$0 == 0x406e
                           00406E  2124 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  2125 G$AX5043_XTALAMPL$0$0 == 0x4f11
                           004F11  2126 _AX5043_XTALAMPL	=	0x4f11
                           004184  2127 G$AX5043_XTALCAP$0$0 == 0x4184
                           004184  2128 _AX5043_XTALCAP	=	0x4184
                           004F10  2129 G$AX5043_XTALOSC$0$0 == 0x4f10
                           004F10  2130 _AX5043_XTALOSC	=	0x4f10
                           00401D  2131 G$AX5043_XTALSTATUS$0$0 == 0x401d
                           00401D  2132 _AX5043_XTALSTATUS	=	0x401d
                           004F00  2133 G$AX5043_0xF00$0$0 == 0x4f00
                           004F00  2134 _AX5043_0xF00	=	0x4f00
                           004F0C  2135 G$AX5043_0xF0C$0$0 == 0x4f0c
                           004F0C  2136 _AX5043_0xF0C	=	0x4f0c
                           004F18  2137 G$AX5043_0xF18$0$0 == 0x4f18
                           004F18  2138 _AX5043_0xF18	=	0x4f18
                           004F1C  2139 G$AX5043_0xF1C$0$0 == 0x4f1c
                           004F1C  2140 _AX5043_0xF1C	=	0x4f1c
                           004F21  2141 G$AX5043_0xF21$0$0 == 0x4f21
                           004F21  2142 _AX5043_0xF21	=	0x4f21
                           004F22  2143 G$AX5043_0xF22$0$0 == 0x4f22
                           004F22  2144 _AX5043_0xF22	=	0x4f22
                           004F23  2145 G$AX5043_0xF23$0$0 == 0x4f23
                           004F23  2146 _AX5043_0xF23	=	0x4f23
                           004F26  2147 G$AX5043_0xF26$0$0 == 0x4f26
                           004F26  2148 _AX5043_0xF26	=	0x4f26
                           004F30  2149 G$AX5043_0xF30$0$0 == 0x4f30
                           004F30  2150 _AX5043_0xF30	=	0x4f30
                           004F31  2151 G$AX5043_0xF31$0$0 == 0x4f31
                           004F31  2152 _AX5043_0xF31	=	0x4f31
                           004F32  2153 G$AX5043_0xF32$0$0 == 0x4f32
                           004F32  2154 _AX5043_0xF32	=	0x4f32
                           004F33  2155 G$AX5043_0xF33$0$0 == 0x4f33
                           004F33  2156 _AX5043_0xF33	=	0x4f33
                           004F34  2157 G$AX5043_0xF34$0$0 == 0x4f34
                           004F34  2158 _AX5043_0xF34	=	0x4f34
                           004F35  2159 G$AX5043_0xF35$0$0 == 0x4f35
                           004F35  2160 _AX5043_0xF35	=	0x4f35
                           004F44  2161 G$AX5043_0xF44$0$0 == 0x4f44
                           004F44  2162 _AX5043_0xF44	=	0x4f44
                           004122  2163 G$AX5043_AGCAHYST0$0$0 == 0x4122
                           004122  2164 _AX5043_AGCAHYST0	=	0x4122
                           004132  2165 G$AX5043_AGCAHYST1$0$0 == 0x4132
                           004132  2166 _AX5043_AGCAHYST1	=	0x4132
                           004142  2167 G$AX5043_AGCAHYST2$0$0 == 0x4142
                           004142  2168 _AX5043_AGCAHYST2	=	0x4142
                           004152  2169 G$AX5043_AGCAHYST3$0$0 == 0x4152
                           004152  2170 _AX5043_AGCAHYST3	=	0x4152
                           004120  2171 G$AX5043_AGCGAIN0$0$0 == 0x4120
                           004120  2172 _AX5043_AGCGAIN0	=	0x4120
                           004130  2173 G$AX5043_AGCGAIN1$0$0 == 0x4130
                           004130  2174 _AX5043_AGCGAIN1	=	0x4130
                           004140  2175 G$AX5043_AGCGAIN2$0$0 == 0x4140
                           004140  2176 _AX5043_AGCGAIN2	=	0x4140
                           004150  2177 G$AX5043_AGCGAIN3$0$0 == 0x4150
                           004150  2178 _AX5043_AGCGAIN3	=	0x4150
                           004123  2179 G$AX5043_AGCMINMAX0$0$0 == 0x4123
                           004123  2180 _AX5043_AGCMINMAX0	=	0x4123
                           004133  2181 G$AX5043_AGCMINMAX1$0$0 == 0x4133
                           004133  2182 _AX5043_AGCMINMAX1	=	0x4133
                           004143  2183 G$AX5043_AGCMINMAX2$0$0 == 0x4143
                           004143  2184 _AX5043_AGCMINMAX2	=	0x4143
                           004153  2185 G$AX5043_AGCMINMAX3$0$0 == 0x4153
                           004153  2186 _AX5043_AGCMINMAX3	=	0x4153
                           004121  2187 G$AX5043_AGCTARGET0$0$0 == 0x4121
                           004121  2188 _AX5043_AGCTARGET0	=	0x4121
                           004131  2189 G$AX5043_AGCTARGET1$0$0 == 0x4131
                           004131  2190 _AX5043_AGCTARGET1	=	0x4131
                           004141  2191 G$AX5043_AGCTARGET2$0$0 == 0x4141
                           004141  2192 _AX5043_AGCTARGET2	=	0x4141
                           004151  2193 G$AX5043_AGCTARGET3$0$0 == 0x4151
                           004151  2194 _AX5043_AGCTARGET3	=	0x4151
                           00412B  2195 G$AX5043_AMPLITUDEGAIN0$0$0 == 0x412b
                           00412B  2196 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  2197 G$AX5043_AMPLITUDEGAIN1$0$0 == 0x413b
                           00413B  2198 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  2199 G$AX5043_AMPLITUDEGAIN2$0$0 == 0x414b
                           00414B  2200 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  2201 G$AX5043_AMPLITUDEGAIN3$0$0 == 0x415b
                           00415B  2202 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  2203 G$AX5043_BBOFFSRES0$0$0 == 0x412f
                           00412F  2204 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  2205 G$AX5043_BBOFFSRES1$0$0 == 0x413f
                           00413F  2206 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  2207 G$AX5043_BBOFFSRES2$0$0 == 0x414f
                           00414F  2208 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  2209 G$AX5043_BBOFFSRES3$0$0 == 0x415f
                           00415F  2210 _AX5043_BBOFFSRES3	=	0x415f
                           004125  2211 G$AX5043_DRGAIN0$0$0 == 0x4125
                           004125  2212 _AX5043_DRGAIN0	=	0x4125
                           004135  2213 G$AX5043_DRGAIN1$0$0 == 0x4135
                           004135  2214 _AX5043_DRGAIN1	=	0x4135
                           004145  2215 G$AX5043_DRGAIN2$0$0 == 0x4145
                           004145  2216 _AX5043_DRGAIN2	=	0x4145
                           004155  2217 G$AX5043_DRGAIN3$0$0 == 0x4155
                           004155  2218 _AX5043_DRGAIN3	=	0x4155
                           00412E  2219 G$AX5043_FOURFSK0$0$0 == 0x412e
                           00412E  2220 _AX5043_FOURFSK0	=	0x412e
                           00413E  2221 G$AX5043_FOURFSK1$0$0 == 0x413e
                           00413E  2222 _AX5043_FOURFSK1	=	0x413e
                           00414E  2223 G$AX5043_FOURFSK2$0$0 == 0x414e
                           00414E  2224 _AX5043_FOURFSK2	=	0x414e
                           00415E  2225 G$AX5043_FOURFSK3$0$0 == 0x415e
                           00415E  2226 _AX5043_FOURFSK3	=	0x415e
                           00412D  2227 G$AX5043_FREQDEV00$0$0 == 0x412d
                           00412D  2228 _AX5043_FREQDEV00	=	0x412d
                           00413D  2229 G$AX5043_FREQDEV01$0$0 == 0x413d
                           00413D  2230 _AX5043_FREQDEV01	=	0x413d
                           00414D  2231 G$AX5043_FREQDEV02$0$0 == 0x414d
                           00414D  2232 _AX5043_FREQDEV02	=	0x414d
                           00415D  2233 G$AX5043_FREQDEV03$0$0 == 0x415d
                           00415D  2234 _AX5043_FREQDEV03	=	0x415d
                           00412C  2235 G$AX5043_FREQDEV10$0$0 == 0x412c
                           00412C  2236 _AX5043_FREQDEV10	=	0x412c
                           00413C  2237 G$AX5043_FREQDEV11$0$0 == 0x413c
                           00413C  2238 _AX5043_FREQDEV11	=	0x413c
                           00414C  2239 G$AX5043_FREQDEV12$0$0 == 0x414c
                           00414C  2240 _AX5043_FREQDEV12	=	0x414c
                           00415C  2241 G$AX5043_FREQDEV13$0$0 == 0x415c
                           00415C  2242 _AX5043_FREQDEV13	=	0x415c
                           004127  2243 G$AX5043_FREQUENCYGAINA0$0$0 == 0x4127
                           004127  2244 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  2245 G$AX5043_FREQUENCYGAINA1$0$0 == 0x4137
                           004137  2246 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  2247 G$AX5043_FREQUENCYGAINA2$0$0 == 0x4147
                           004147  2248 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  2249 G$AX5043_FREQUENCYGAINA3$0$0 == 0x4157
                           004157  2250 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  2251 G$AX5043_FREQUENCYGAINB0$0$0 == 0x4128
                           004128  2252 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  2253 G$AX5043_FREQUENCYGAINB1$0$0 == 0x4138
                           004138  2254 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  2255 G$AX5043_FREQUENCYGAINB2$0$0 == 0x4148
                           004148  2256 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  2257 G$AX5043_FREQUENCYGAINB3$0$0 == 0x4158
                           004158  2258 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  2259 G$AX5043_FREQUENCYGAINC0$0$0 == 0x4129
                           004129  2260 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  2261 G$AX5043_FREQUENCYGAINC1$0$0 == 0x4139
                           004139  2262 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  2263 G$AX5043_FREQUENCYGAINC2$0$0 == 0x4149
                           004149  2264 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  2265 G$AX5043_FREQUENCYGAINC3$0$0 == 0x4159
                           004159  2266 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  2267 G$AX5043_FREQUENCYGAIND0$0$0 == 0x412a
                           00412A  2268 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  2269 G$AX5043_FREQUENCYGAIND1$0$0 == 0x413a
                           00413A  2270 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  2271 G$AX5043_FREQUENCYGAIND2$0$0 == 0x414a
                           00414A  2272 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  2273 G$AX5043_FREQUENCYGAIND3$0$0 == 0x415a
                           00415A  2274 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  2275 G$AX5043_FREQUENCYLEAK$0$0 == 0x4116
                           004116  2276 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  2277 G$AX5043_PHASEGAIN0$0$0 == 0x4126
                           004126  2278 _AX5043_PHASEGAIN0	=	0x4126
                           004136  2279 G$AX5043_PHASEGAIN1$0$0 == 0x4136
                           004136  2280 _AX5043_PHASEGAIN1	=	0x4136
                           004146  2281 G$AX5043_PHASEGAIN2$0$0 == 0x4146
                           004146  2282 _AX5043_PHASEGAIN2	=	0x4146
                           004156  2283 G$AX5043_PHASEGAIN3$0$0 == 0x4156
                           004156  2284 _AX5043_PHASEGAIN3	=	0x4156
                           004207  2285 G$AX5043_PKTADDR0$0$0 == 0x4207
                           004207  2286 _AX5043_PKTADDR0	=	0x4207
                           004206  2287 G$AX5043_PKTADDR1$0$0 == 0x4206
                           004206  2288 _AX5043_PKTADDR1	=	0x4206
                           004205  2289 G$AX5043_PKTADDR2$0$0 == 0x4205
                           004205  2290 _AX5043_PKTADDR2	=	0x4205
                           004204  2291 G$AX5043_PKTADDR3$0$0 == 0x4204
                           004204  2292 _AX5043_PKTADDR3	=	0x4204
                           004200  2293 G$AX5043_PKTADDRCFG$0$0 == 0x4200
                           004200  2294 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  2295 G$AX5043_PKTADDRMASK0$0$0 == 0x420b
                           00420B  2296 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  2297 G$AX5043_PKTADDRMASK1$0$0 == 0x420a
                           00420A  2298 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  2299 G$AX5043_PKTADDRMASK2$0$0 == 0x4209
                           004209  2300 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  2301 G$AX5043_PKTADDRMASK3$0$0 == 0x4208
                           004208  2302 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  2303 G$AX5043_PKTLENCFG$0$0 == 0x4201
                           004201  2304 _AX5043_PKTLENCFG	=	0x4201
                           004202  2305 G$AX5043_PKTLENOFFSET$0$0 == 0x4202
                           004202  2306 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  2307 G$AX5043_PKTMAXLEN$0$0 == 0x4203
                           004203  2308 _AX5043_PKTMAXLEN	=	0x4203
                           004118  2309 G$AX5043_RXPARAMCURSET$0$0 == 0x4118
                           004118  2310 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  2311 G$AX5043_RXPARAMSETS$0$0 == 0x4117
                           004117  2312 _AX5043_RXPARAMSETS	=	0x4117
                           004124  2313 G$AX5043_TIMEGAIN0$0$0 == 0x4124
                           004124  2314 _AX5043_TIMEGAIN0	=	0x4124
                           004134  2315 G$AX5043_TIMEGAIN1$0$0 == 0x4134
                           004134  2316 _AX5043_TIMEGAIN1	=	0x4134
                           004144  2317 G$AX5043_TIMEGAIN2$0$0 == 0x4144
                           004144  2318 _AX5043_TIMEGAIN2	=	0x4144
                           004154  2319 G$AX5043_TIMEGAIN3$0$0 == 0x4154
                           004154  2320 _AX5043_TIMEGAIN3	=	0x4154
                           005114  2321 G$AX5043_AFSKCTRLNB$0$0 == 0x5114
                           005114  2322 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  2323 G$AX5043_AFSKMARK0NB$0$0 == 0x5113
                           005113  2324 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  2325 G$AX5043_AFSKMARK1NB$0$0 == 0x5112
                           005112  2326 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  2327 G$AX5043_AFSKSPACE0NB$0$0 == 0x5111
                           005111  2328 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  2329 G$AX5043_AFSKSPACE1NB$0$0 == 0x5110
                           005110  2330 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  2331 G$AX5043_AGCCOUNTERNB$0$0 == 0x5043
                           005043  2332 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  2333 G$AX5043_AMPLFILTERNB$0$0 == 0x5115
                           005115  2334 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  2335 G$AX5043_BBOFFSCAPNB$0$0 == 0x5189
                           005189  2336 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  2337 G$AX5043_BBTUNENB$0$0 == 0x5188
                           005188  2338 _AX5043_BBTUNENB	=	0x5188
                           005041  2339 G$AX5043_BGNDRSSINB$0$0 == 0x5041
                           005041  2340 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  2341 G$AX5043_BGNDRSSIGAINNB$0$0 == 0x522e
                           00522E  2342 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  2343 G$AX5043_BGNDRSSITHRNB$0$0 == 0x522f
                           00522F  2344 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2345 G$AX5043_CRCINIT0NB$0$0 == 0x5017
                           005017  2346 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2347 G$AX5043_CRCINIT1NB$0$0 == 0x5016
                           005016  2348 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2349 G$AX5043_CRCINIT2NB$0$0 == 0x5015
                           005015  2350 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2351 G$AX5043_CRCINIT3NB$0$0 == 0x5014
                           005014  2352 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2353 G$AX5043_DACCONFIGNB$0$0 == 0x5332
                           005332  2354 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2355 G$AX5043_DACVALUE0NB$0$0 == 0x5331
                           005331  2356 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2357 G$AX5043_DACVALUE1NB$0$0 == 0x5330
                           005330  2358 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2359 G$AX5043_DECIMATIONNB$0$0 == 0x5102
                           005102  2360 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2361 G$AX5043_DIVERSITYNB$0$0 == 0x5042
                           005042  2362 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2363 G$AX5043_ENCODINGNB$0$0 == 0x5011
                           005011  2364 _AX5043_ENCODINGNB	=	0x5011
                           005018  2365 G$AX5043_FECNB$0$0 == 0x5018
                           005018  2366 _AX5043_FECNB	=	0x5018
                           00501A  2367 G$AX5043_FECSTATUSNB$0$0 == 0x501a
                           00501A  2368 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2369 G$AX5043_FECSYNCNB$0$0 == 0x5019
                           005019  2370 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2371 G$AX5043_FIFOCOUNT0NB$0$0 == 0x502b
                           00502B  2372 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2373 G$AX5043_FIFOCOUNT1NB$0$0 == 0x502a
                           00502A  2374 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2375 G$AX5043_FIFODATANB$0$0 == 0x5029
                           005029  2376 _AX5043_FIFODATANB	=	0x5029
                           00502D  2377 G$AX5043_FIFOFREE0NB$0$0 == 0x502d
                           00502D  2378 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2379 G$AX5043_FIFOFREE1NB$0$0 == 0x502c
                           00502C  2380 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2381 G$AX5043_FIFOSTATNB$0$0 == 0x5028
                           005028  2382 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2383 G$AX5043_FIFOTHRESH0NB$0$0 == 0x502f
                           00502F  2384 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2385 G$AX5043_FIFOTHRESH1NB$0$0 == 0x502e
                           00502E  2386 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2387 G$AX5043_FRAMINGNB$0$0 == 0x5012
                           005012  2388 _AX5043_FRAMINGNB	=	0x5012
                           005037  2389 G$AX5043_FREQA0NB$0$0 == 0x5037
                           005037  2390 _AX5043_FREQA0NB	=	0x5037
                           005036  2391 G$AX5043_FREQA1NB$0$0 == 0x5036
                           005036  2392 _AX5043_FREQA1NB	=	0x5036
                           005035  2393 G$AX5043_FREQA2NB$0$0 == 0x5035
                           005035  2394 _AX5043_FREQA2NB	=	0x5035
                           005034  2395 G$AX5043_FREQA3NB$0$0 == 0x5034
                           005034  2396 _AX5043_FREQA3NB	=	0x5034
                           00503F  2397 G$AX5043_FREQB0NB$0$0 == 0x503f
                           00503F  2398 _AX5043_FREQB0NB	=	0x503f
                           00503E  2399 G$AX5043_FREQB1NB$0$0 == 0x503e
                           00503E  2400 _AX5043_FREQB1NB	=	0x503e
                           00503D  2401 G$AX5043_FREQB2NB$0$0 == 0x503d
                           00503D  2402 _AX5043_FREQB2NB	=	0x503d
                           00503C  2403 G$AX5043_FREQB3NB$0$0 == 0x503c
                           00503C  2404 _AX5043_FREQB3NB	=	0x503c
                           005163  2405 G$AX5043_FSKDEV0NB$0$0 == 0x5163
                           005163  2406 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2407 G$AX5043_FSKDEV1NB$0$0 == 0x5162
                           005162  2408 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2409 G$AX5043_FSKDEV2NB$0$0 == 0x5161
                           005161  2410 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2411 G$AX5043_FSKDMAX0NB$0$0 == 0x510d
                           00510D  2412 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2413 G$AX5043_FSKDMAX1NB$0$0 == 0x510c
                           00510C  2414 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2415 G$AX5043_FSKDMIN0NB$0$0 == 0x510f
                           00510F  2416 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2417 G$AX5043_FSKDMIN1NB$0$0 == 0x510e
                           00510E  2418 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2419 G$AX5043_GPADC13VALUE0NB$0$0 == 0x5309
                           005309  2420 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2421 G$AX5043_GPADC13VALUE1NB$0$0 == 0x5308
                           005308  2422 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2423 G$AX5043_GPADCCTRLNB$0$0 == 0x5300
                           005300  2424 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2425 G$AX5043_GPADCPERIODNB$0$0 == 0x5301
                           005301  2426 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2427 G$AX5043_IFFREQ0NB$0$0 == 0x5101
                           005101  2428 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2429 G$AX5043_IFFREQ1NB$0$0 == 0x5100
                           005100  2430 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2431 G$AX5043_IRQINVERSION0NB$0$0 == 0x500b
                           00500B  2432 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2433 G$AX5043_IRQINVERSION1NB$0$0 == 0x500a
                           00500A  2434 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2435 G$AX5043_IRQMASK0NB$0$0 == 0x5007
                           005007  2436 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2437 G$AX5043_IRQMASK1NB$0$0 == 0x5006
                           005006  2438 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2439 G$AX5043_IRQREQUEST0NB$0$0 == 0x500d
                           00500D  2440 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2441 G$AX5043_IRQREQUEST1NB$0$0 == 0x500c
                           00500C  2442 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2443 G$AX5043_LPOSCCONFIGNB$0$0 == 0x5310
                           005310  2444 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2445 G$AX5043_LPOSCFREQ0NB$0$0 == 0x5317
                           005317  2446 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2447 G$AX5043_LPOSCFREQ1NB$0$0 == 0x5316
                           005316  2448 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2449 G$AX5043_LPOSCKFILT0NB$0$0 == 0x5313
                           005313  2450 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2451 G$AX5043_LPOSCKFILT1NB$0$0 == 0x5312
                           005312  2452 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2453 G$AX5043_LPOSCPER0NB$0$0 == 0x5319
                           005319  2454 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2455 G$AX5043_LPOSCPER1NB$0$0 == 0x5318
                           005318  2456 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2457 G$AX5043_LPOSCREF0NB$0$0 == 0x5315
                           005315  2458 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2459 G$AX5043_LPOSCREF1NB$0$0 == 0x5314
                           005314  2460 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2461 G$AX5043_LPOSCSTATUSNB$0$0 == 0x5311
                           005311  2462 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2463 G$AX5043_MATCH0LENNB$0$0 == 0x5214
                           005214  2464 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2465 G$AX5043_MATCH0MAXNB$0$0 == 0x5216
                           005216  2466 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2467 G$AX5043_MATCH0MINNB$0$0 == 0x5215
                           005215  2468 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2469 G$AX5043_MATCH0PAT0NB$0$0 == 0x5213
                           005213  2470 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2471 G$AX5043_MATCH0PAT1NB$0$0 == 0x5212
                           005212  2472 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2473 G$AX5043_MATCH0PAT2NB$0$0 == 0x5211
                           005211  2474 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2475 G$AX5043_MATCH0PAT3NB$0$0 == 0x5210
                           005210  2476 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2477 G$AX5043_MATCH1LENNB$0$0 == 0x521c
                           00521C  2478 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2479 G$AX5043_MATCH1MAXNB$0$0 == 0x521e
                           00521E  2480 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2481 G$AX5043_MATCH1MINNB$0$0 == 0x521d
                           00521D  2482 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2483 G$AX5043_MATCH1PAT0NB$0$0 == 0x5219
                           005219  2484 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2485 G$AX5043_MATCH1PAT1NB$0$0 == 0x5218
                           005218  2486 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2487 G$AX5043_MAXDROFFSET0NB$0$0 == 0x5108
                           005108  2488 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2489 G$AX5043_MAXDROFFSET1NB$0$0 == 0x5107
                           005107  2490 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2491 G$AX5043_MAXDROFFSET2NB$0$0 == 0x5106
                           005106  2492 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2493 G$AX5043_MAXRFOFFSET0NB$0$0 == 0x510b
                           00510B  2494 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2495 G$AX5043_MAXRFOFFSET1NB$0$0 == 0x510a
                           00510A  2496 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2497 G$AX5043_MAXRFOFFSET2NB$0$0 == 0x5109
                           005109  2498 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2499 G$AX5043_MODCFGANB$0$0 == 0x5164
                           005164  2500 _AX5043_MODCFGANB	=	0x5164
                           005160  2501 G$AX5043_MODCFGFNB$0$0 == 0x5160
                           005160  2502 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2503 G$AX5043_MODCFGPNB$0$0 == 0x5f5f
                           005F5F  2504 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2505 G$AX5043_MODULATIONNB$0$0 == 0x5010
                           005010  2506 _AX5043_MODULATIONNB	=	0x5010
                           005025  2507 G$AX5043_PINFUNCANTSELNB$0$0 == 0x5025
                           005025  2508 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2509 G$AX5043_PINFUNCDATANB$0$0 == 0x5023
                           005023  2510 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2511 G$AX5043_PINFUNCDCLKNB$0$0 == 0x5022
                           005022  2512 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2513 G$AX5043_PINFUNCIRQNB$0$0 == 0x5024
                           005024  2514 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2515 G$AX5043_PINFUNCPWRAMPNB$0$0 == 0x5026
                           005026  2516 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2517 G$AX5043_PINFUNCSYSCLKNB$0$0 == 0x5021
                           005021  2518 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2519 G$AX5043_PINSTATENB$0$0 == 0x5020
                           005020  2520 _AX5043_PINSTATENB	=	0x5020
                           005233  2521 G$AX5043_PKTACCEPTFLAGSNB$0$0 == 0x5233
                           005233  2522 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2523 G$AX5043_PKTCHUNKSIZENB$0$0 == 0x5230
                           005230  2524 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2525 G$AX5043_PKTMISCFLAGSNB$0$0 == 0x5231
                           005231  2526 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2527 G$AX5043_PKTSTOREFLAGSNB$0$0 == 0x5232
                           005232  2528 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2529 G$AX5043_PLLCPINB$0$0 == 0x5031
                           005031  2530 _AX5043_PLLCPINB	=	0x5031
                           005039  2531 G$AX5043_PLLCPIBOOSTNB$0$0 == 0x5039
                           005039  2532 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2533 G$AX5043_PLLLOCKDETNB$0$0 == 0x5182
                           005182  2534 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2535 G$AX5043_PLLLOOPNB$0$0 == 0x5030
                           005030  2536 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2537 G$AX5043_PLLLOOPBOOSTNB$0$0 == 0x5038
                           005038  2538 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2539 G$AX5043_PLLRANGINGANB$0$0 == 0x5033
                           005033  2540 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2541 G$AX5043_PLLRANGINGBNB$0$0 == 0x503b
                           00503B  2542 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2543 G$AX5043_PLLRNGCLKNB$0$0 == 0x5183
                           005183  2544 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2545 G$AX5043_PLLVCODIVNB$0$0 == 0x5032
                           005032  2546 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2547 G$AX5043_PLLVCOINB$0$0 == 0x5180
                           005180  2548 _AX5043_PLLVCOINB	=	0x5180
                           005181  2549 G$AX5043_PLLVCOIRNB$0$0 == 0x5181
                           005181  2550 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2551 G$AX5043_POWCTRL1NB$0$0 == 0x5f08
                           005F08  2552 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2553 G$AX5043_POWIRQMASKNB$0$0 == 0x5005
                           005005  2554 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2555 G$AX5043_POWSTATNB$0$0 == 0x5003
                           005003  2556 _AX5043_POWSTATNB	=	0x5003
                           005004  2557 G$AX5043_POWSTICKYSTATNB$0$0 == 0x5004
                           005004  2558 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2559 G$AX5043_PWRAMPNB$0$0 == 0x5027
                           005027  2560 _AX5043_PWRAMPNB	=	0x5027
                           005002  2561 G$AX5043_PWRMODENB$0$0 == 0x5002
                           005002  2562 _AX5043_PWRMODENB	=	0x5002
                           005009  2563 G$AX5043_RADIOEVENTMASK0NB$0$0 == 0x5009
                           005009  2564 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2565 G$AX5043_RADIOEVENTMASK1NB$0$0 == 0x5008
                           005008  2566 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2567 G$AX5043_RADIOEVENTREQ0NB$0$0 == 0x500f
                           00500F  2568 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2569 G$AX5043_RADIOEVENTREQ1NB$0$0 == 0x500e
                           00500E  2570 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2571 G$AX5043_RADIOSTATENB$0$0 == 0x501c
                           00501C  2572 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2573 G$AX5043_REFNB$0$0 == 0x5f0d
                           005F0D  2574 _AX5043_REFNB	=	0x5f0d
                           005040  2575 G$AX5043_RSSINB$0$0 == 0x5040
                           005040  2576 _AX5043_RSSINB	=	0x5040
                           00522D  2577 G$AX5043_RSSIABSTHRNB$0$0 == 0x522d
                           00522D  2578 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2579 G$AX5043_RSSIREFERENCENB$0$0 == 0x522c
                           00522C  2580 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2581 G$AX5043_RXDATARATE0NB$0$0 == 0x5105
                           005105  2582 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2583 G$AX5043_RXDATARATE1NB$0$0 == 0x5104
                           005104  2584 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2585 G$AX5043_RXDATARATE2NB$0$0 == 0x5103
                           005103  2586 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2587 G$AX5043_SCRATCHNB$0$0 == 0x5001
                           005001  2588 _AX5043_SCRATCHNB	=	0x5001
                           005000  2589 G$AX5043_SILICONREVISIONNB$0$0 == 0x5000
                           005000  2590 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2591 G$AX5043_TIMER0NB$0$0 == 0x505b
                           00505B  2592 _AX5043_TIMER0NB	=	0x505b
                           00505A  2593 G$AX5043_TIMER1NB$0$0 == 0x505a
                           00505A  2594 _AX5043_TIMER1NB	=	0x505a
                           005059  2595 G$AX5043_TIMER2NB$0$0 == 0x5059
                           005059  2596 _AX5043_TIMER2NB	=	0x5059
                           005227  2597 G$AX5043_TMGRXAGCNB$0$0 == 0x5227
                           005227  2598 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2599 G$AX5043_TMGRXBOOSTNB$0$0 == 0x5223
                           005223  2600 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2601 G$AX5043_TMGRXCOARSEAGCNB$0$0 == 0x5226
                           005226  2602 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2603 G$AX5043_TMGRXOFFSACQNB$0$0 == 0x5225
                           005225  2604 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2605 G$AX5043_TMGRXPREAMBLE1NB$0$0 == 0x5229
                           005229  2606 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2607 G$AX5043_TMGRXPREAMBLE2NB$0$0 == 0x522a
                           00522A  2608 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2609 G$AX5043_TMGRXPREAMBLE3NB$0$0 == 0x522b
                           00522B  2610 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2611 G$AX5043_TMGRXRSSINB$0$0 == 0x5228
                           005228  2612 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2613 G$AX5043_TMGRXSETTLENB$0$0 == 0x5224
                           005224  2614 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2615 G$AX5043_TMGTXBOOSTNB$0$0 == 0x5220
                           005220  2616 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2617 G$AX5043_TMGTXSETTLENB$0$0 == 0x5221
                           005221  2618 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2619 G$AX5043_TRKAFSKDEMOD0NB$0$0 == 0x5055
                           005055  2620 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2621 G$AX5043_TRKAFSKDEMOD1NB$0$0 == 0x5054
                           005054  2622 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2623 G$AX5043_TRKAMPLITUDE0NB$0$0 == 0x5049
                           005049  2624 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2625 G$AX5043_TRKAMPLITUDE1NB$0$0 == 0x5048
                           005048  2626 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2627 G$AX5043_TRKDATARATE0NB$0$0 == 0x5047
                           005047  2628 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2629 G$AX5043_TRKDATARATE1NB$0$0 == 0x5046
                           005046  2630 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2631 G$AX5043_TRKDATARATE2NB$0$0 == 0x5045
                           005045  2632 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2633 G$AX5043_TRKFREQ0NB$0$0 == 0x5051
                           005051  2634 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2635 G$AX5043_TRKFREQ1NB$0$0 == 0x5050
                           005050  2636 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2637 G$AX5043_TRKFSKDEMOD0NB$0$0 == 0x5053
                           005053  2638 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2639 G$AX5043_TRKFSKDEMOD1NB$0$0 == 0x5052
                           005052  2640 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2641 G$AX5043_TRKPHASE0NB$0$0 == 0x504b
                           00504B  2642 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2643 G$AX5043_TRKPHASE1NB$0$0 == 0x504a
                           00504A  2644 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2645 G$AX5043_TRKRFFREQ0NB$0$0 == 0x504f
                           00504F  2646 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2647 G$AX5043_TRKRFFREQ1NB$0$0 == 0x504e
                           00504E  2648 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2649 G$AX5043_TRKRFFREQ2NB$0$0 == 0x504d
                           00504D  2650 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2651 G$AX5043_TXPWRCOEFFA0NB$0$0 == 0x5169
                           005169  2652 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2653 G$AX5043_TXPWRCOEFFA1NB$0$0 == 0x5168
                           005168  2654 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2655 G$AX5043_TXPWRCOEFFB0NB$0$0 == 0x516b
                           00516B  2656 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2657 G$AX5043_TXPWRCOEFFB1NB$0$0 == 0x516a
                           00516A  2658 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2659 G$AX5043_TXPWRCOEFFC0NB$0$0 == 0x516d
                           00516D  2660 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2661 G$AX5043_TXPWRCOEFFC1NB$0$0 == 0x516c
                           00516C  2662 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2663 G$AX5043_TXPWRCOEFFD0NB$0$0 == 0x516f
                           00516F  2664 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2665 G$AX5043_TXPWRCOEFFD1NB$0$0 == 0x516e
                           00516E  2666 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2667 G$AX5043_TXPWRCOEFFE0NB$0$0 == 0x5171
                           005171  2668 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2669 G$AX5043_TXPWRCOEFFE1NB$0$0 == 0x5170
                           005170  2670 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2671 G$AX5043_TXRATE0NB$0$0 == 0x5167
                           005167  2672 _AX5043_TXRATE0NB	=	0x5167
                           005166  2673 G$AX5043_TXRATE1NB$0$0 == 0x5166
                           005166  2674 _AX5043_TXRATE1NB	=	0x5166
                           005165  2675 G$AX5043_TXRATE2NB$0$0 == 0x5165
                           005165  2676 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2677 G$AX5043_WAKEUP0NB$0$0 == 0x506b
                           00506B  2678 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2679 G$AX5043_WAKEUP1NB$0$0 == 0x506a
                           00506A  2680 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2681 G$AX5043_WAKEUPFREQ0NB$0$0 == 0x506d
                           00506D  2682 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2683 G$AX5043_WAKEUPFREQ1NB$0$0 == 0x506c
                           00506C  2684 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2685 G$AX5043_WAKEUPTIMER0NB$0$0 == 0x5069
                           005069  2686 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2687 G$AX5043_WAKEUPTIMER1NB$0$0 == 0x5068
                           005068  2688 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2689 G$AX5043_WAKEUPXOEARLYNB$0$0 == 0x506e
                           00506E  2690 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2691 G$AX5043_XTALAMPLNB$0$0 == 0x5f11
                           005F11  2692 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2693 G$AX5043_XTALCAPNB$0$0 == 0x5184
                           005184  2694 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2695 G$AX5043_XTALOSCNB$0$0 == 0x5f10
                           005F10  2696 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2697 G$AX5043_XTALSTATUSNB$0$0 == 0x501d
                           00501D  2698 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2699 G$AX5043_0xF00NB$0$0 == 0x5f00
                           005F00  2700 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2701 G$AX5043_0xF0CNB$0$0 == 0x5f0c
                           005F0C  2702 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2703 G$AX5043_0xF18NB$0$0 == 0x5f18
                           005F18  2704 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2705 G$AX5043_0xF1CNB$0$0 == 0x5f1c
                           005F1C  2706 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2707 G$AX5043_0xF21NB$0$0 == 0x5f21
                           005F21  2708 _AX5043_0xF21NB	=	0x5f21
                           005F22  2709 G$AX5043_0xF22NB$0$0 == 0x5f22
                           005F22  2710 _AX5043_0xF22NB	=	0x5f22
                           005F23  2711 G$AX5043_0xF23NB$0$0 == 0x5f23
                           005F23  2712 _AX5043_0xF23NB	=	0x5f23
                           005F26  2713 G$AX5043_0xF26NB$0$0 == 0x5f26
                           005F26  2714 _AX5043_0xF26NB	=	0x5f26
                           005F30  2715 G$AX5043_0xF30NB$0$0 == 0x5f30
                           005F30  2716 _AX5043_0xF30NB	=	0x5f30
                           005F31  2717 G$AX5043_0xF31NB$0$0 == 0x5f31
                           005F31  2718 _AX5043_0xF31NB	=	0x5f31
                           005F32  2719 G$AX5043_0xF32NB$0$0 == 0x5f32
                           005F32  2720 _AX5043_0xF32NB	=	0x5f32
                           005F33  2721 G$AX5043_0xF33NB$0$0 == 0x5f33
                           005F33  2722 _AX5043_0xF33NB	=	0x5f33
                           005F34  2723 G$AX5043_0xF34NB$0$0 == 0x5f34
                           005F34  2724 _AX5043_0xF34NB	=	0x5f34
                           005F35  2725 G$AX5043_0xF35NB$0$0 == 0x5f35
                           005F35  2726 _AX5043_0xF35NB	=	0x5f35
                           005F44  2727 G$AX5043_0xF44NB$0$0 == 0x5f44
                           005F44  2728 _AX5043_0xF44NB	=	0x5f44
                           005122  2729 G$AX5043_AGCAHYST0NB$0$0 == 0x5122
                           005122  2730 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2731 G$AX5043_AGCAHYST1NB$0$0 == 0x5132
                           005132  2732 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2733 G$AX5043_AGCAHYST2NB$0$0 == 0x5142
                           005142  2734 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2735 G$AX5043_AGCAHYST3NB$0$0 == 0x5152
                           005152  2736 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2737 G$AX5043_AGCGAIN0NB$0$0 == 0x5120
                           005120  2738 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2739 G$AX5043_AGCGAIN1NB$0$0 == 0x5130
                           005130  2740 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2741 G$AX5043_AGCGAIN2NB$0$0 == 0x5140
                           005140  2742 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2743 G$AX5043_AGCGAIN3NB$0$0 == 0x5150
                           005150  2744 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2745 G$AX5043_AGCMINMAX0NB$0$0 == 0x5123
                           005123  2746 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2747 G$AX5043_AGCMINMAX1NB$0$0 == 0x5133
                           005133  2748 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2749 G$AX5043_AGCMINMAX2NB$0$0 == 0x5143
                           005143  2750 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2751 G$AX5043_AGCMINMAX3NB$0$0 == 0x5153
                           005153  2752 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2753 G$AX5043_AGCTARGET0NB$0$0 == 0x5121
                           005121  2754 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2755 G$AX5043_AGCTARGET1NB$0$0 == 0x5131
                           005131  2756 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2757 G$AX5043_AGCTARGET2NB$0$0 == 0x5141
                           005141  2758 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2759 G$AX5043_AGCTARGET3NB$0$0 == 0x5151
                           005151  2760 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2761 G$AX5043_AMPLITUDEGAIN0NB$0$0 == 0x512b
                           00512B  2762 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2763 G$AX5043_AMPLITUDEGAIN1NB$0$0 == 0x513b
                           00513B  2764 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2765 G$AX5043_AMPLITUDEGAIN2NB$0$0 == 0x514b
                           00514B  2766 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2767 G$AX5043_AMPLITUDEGAIN3NB$0$0 == 0x515b
                           00515B  2768 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2769 G$AX5043_BBOFFSRES0NB$0$0 == 0x512f
                           00512F  2770 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2771 G$AX5043_BBOFFSRES1NB$0$0 == 0x513f
                           00513F  2772 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2773 G$AX5043_BBOFFSRES2NB$0$0 == 0x514f
                           00514F  2774 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2775 G$AX5043_BBOFFSRES3NB$0$0 == 0x515f
                           00515F  2776 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2777 G$AX5043_DRGAIN0NB$0$0 == 0x5125
                           005125  2778 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2779 G$AX5043_DRGAIN1NB$0$0 == 0x5135
                           005135  2780 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2781 G$AX5043_DRGAIN2NB$0$0 == 0x5145
                           005145  2782 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2783 G$AX5043_DRGAIN3NB$0$0 == 0x5155
                           005155  2784 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2785 G$AX5043_FOURFSK0NB$0$0 == 0x512e
                           00512E  2786 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2787 G$AX5043_FOURFSK1NB$0$0 == 0x513e
                           00513E  2788 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2789 G$AX5043_FOURFSK2NB$0$0 == 0x514e
                           00514E  2790 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2791 G$AX5043_FOURFSK3NB$0$0 == 0x515e
                           00515E  2792 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2793 G$AX5043_FREQDEV00NB$0$0 == 0x512d
                           00512D  2794 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2795 G$AX5043_FREQDEV01NB$0$0 == 0x513d
                           00513D  2796 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2797 G$AX5043_FREQDEV02NB$0$0 == 0x514d
                           00514D  2798 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2799 G$AX5043_FREQDEV03NB$0$0 == 0x515d
                           00515D  2800 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2801 G$AX5043_FREQDEV10NB$0$0 == 0x512c
                           00512C  2802 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2803 G$AX5043_FREQDEV11NB$0$0 == 0x513c
                           00513C  2804 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2805 G$AX5043_FREQDEV12NB$0$0 == 0x514c
                           00514C  2806 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2807 G$AX5043_FREQDEV13NB$0$0 == 0x515c
                           00515C  2808 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2809 G$AX5043_FREQUENCYGAINA0NB$0$0 == 0x5127
                           005127  2810 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2811 G$AX5043_FREQUENCYGAINA1NB$0$0 == 0x5137
                           005137  2812 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2813 G$AX5043_FREQUENCYGAINA2NB$0$0 == 0x5147
                           005147  2814 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2815 G$AX5043_FREQUENCYGAINA3NB$0$0 == 0x5157
                           005157  2816 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2817 G$AX5043_FREQUENCYGAINB0NB$0$0 == 0x5128
                           005128  2818 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2819 G$AX5043_FREQUENCYGAINB1NB$0$0 == 0x5138
                           005138  2820 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2821 G$AX5043_FREQUENCYGAINB2NB$0$0 == 0x5148
                           005148  2822 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2823 G$AX5043_FREQUENCYGAINB3NB$0$0 == 0x5158
                           005158  2824 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2825 G$AX5043_FREQUENCYGAINC0NB$0$0 == 0x5129
                           005129  2826 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2827 G$AX5043_FREQUENCYGAINC1NB$0$0 == 0x5139
                           005139  2828 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2829 G$AX5043_FREQUENCYGAINC2NB$0$0 == 0x5149
                           005149  2830 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2831 G$AX5043_FREQUENCYGAINC3NB$0$0 == 0x5159
                           005159  2832 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2833 G$AX5043_FREQUENCYGAIND0NB$0$0 == 0x512a
                           00512A  2834 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2835 G$AX5043_FREQUENCYGAIND1NB$0$0 == 0x513a
                           00513A  2836 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2837 G$AX5043_FREQUENCYGAIND2NB$0$0 == 0x514a
                           00514A  2838 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2839 G$AX5043_FREQUENCYGAIND3NB$0$0 == 0x515a
                           00515A  2840 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2841 G$AX5043_FREQUENCYLEAKNB$0$0 == 0x5116
                           005116  2842 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2843 G$AX5043_PHASEGAIN0NB$0$0 == 0x5126
                           005126  2844 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2845 G$AX5043_PHASEGAIN1NB$0$0 == 0x5136
                           005136  2846 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2847 G$AX5043_PHASEGAIN2NB$0$0 == 0x5146
                           005146  2848 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2849 G$AX5043_PHASEGAIN3NB$0$0 == 0x5156
                           005156  2850 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2851 G$AX5043_PKTADDR0NB$0$0 == 0x5207
                           005207  2852 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2853 G$AX5043_PKTADDR1NB$0$0 == 0x5206
                           005206  2854 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2855 G$AX5043_PKTADDR2NB$0$0 == 0x5205
                           005205  2856 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2857 G$AX5043_PKTADDR3NB$0$0 == 0x5204
                           005204  2858 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2859 G$AX5043_PKTADDRCFGNB$0$0 == 0x5200
                           005200  2860 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2861 G$AX5043_PKTADDRMASK0NB$0$0 == 0x520b
                           00520B  2862 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2863 G$AX5043_PKTADDRMASK1NB$0$0 == 0x520a
                           00520A  2864 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2865 G$AX5043_PKTADDRMASK2NB$0$0 == 0x5209
                           005209  2866 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2867 G$AX5043_PKTADDRMASK3NB$0$0 == 0x5208
                           005208  2868 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2869 G$AX5043_PKTLENCFGNB$0$0 == 0x5201
                           005201  2870 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2871 G$AX5043_PKTLENOFFSETNB$0$0 == 0x5202
                           005202  2872 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2873 G$AX5043_PKTMAXLENNB$0$0 == 0x5203
                           005203  2874 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2875 G$AX5043_RXPARAMCURSETNB$0$0 == 0x5118
                           005118  2876 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2877 G$AX5043_RXPARAMSETSNB$0$0 == 0x5117
                           005117  2878 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2879 G$AX5043_TIMEGAIN0NB$0$0 == 0x5124
                           005124  2880 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2881 G$AX5043_TIMEGAIN1NB$0$0 == 0x5134
                           005134  2882 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2883 G$AX5043_TIMEGAIN2NB$0$0 == 0x5144
                           005144  2884 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2885 G$AX5043_TIMEGAIN3NB$0$0 == 0x5154
                           005154  2886 _AX5043_TIMEGAIN3NB	=	0x5154
                           007091  2887 G$AESCONFIG$0$0 == 0x7091
                           007091  2888 _AESCONFIG	=	0x7091
                           007098  2889 G$AESCURBLOCK$0$0 == 0x7098
                           007098  2890 _AESCURBLOCK	=	0x7098
                           007094  2891 G$AESINADDR0$0$0 == 0x7094
                           007094  2892 _AESINADDR0	=	0x7094
                           007095  2893 G$AESINADDR1$0$0 == 0x7095
                           007095  2894 _AESINADDR1	=	0x7095
                           007094  2895 G$AESINADDR$0$0 == 0x7094
                           007094  2896 _AESINADDR	=	0x7094
                           007092  2897 G$AESKEYADDR0$0$0 == 0x7092
                           007092  2898 _AESKEYADDR0	=	0x7092
                           007093  2899 G$AESKEYADDR1$0$0 == 0x7093
                           007093  2900 _AESKEYADDR1	=	0x7093
                           007092  2901 G$AESKEYADDR$0$0 == 0x7092
                           007092  2902 _AESKEYADDR	=	0x7092
                           007090  2903 G$AESMODE$0$0 == 0x7090
                           007090  2904 _AESMODE	=	0x7090
                           007096  2905 G$AESOUTADDR0$0$0 == 0x7096
                           007096  2906 _AESOUTADDR0	=	0x7096
                           007097  2907 G$AESOUTADDR1$0$0 == 0x7097
                           007097  2908 _AESOUTADDR1	=	0x7097
                           007096  2909 G$AESOUTADDR$0$0 == 0x7096
                           007096  2910 _AESOUTADDR	=	0x7096
                           007081  2911 G$RNGBYTE$0$0 == 0x7081
                           007081  2912 _RNGBYTE	=	0x7081
                           007082  2913 G$RNGCLKSRC0$0$0 == 0x7082
                           007082  2914 _RNGCLKSRC0	=	0x7082
                           007083  2915 G$RNGCLKSRC1$0$0 == 0x7083
                           007083  2916 _RNGCLKSRC1	=	0x7083
                           007080  2917 G$RNGMODE$0$0 == 0x7080
                           007080  2918 _RNGMODE	=	0x7080
                           000000  2919 G$randomSet$0$0==.
      0002D9                       2920 _randomSet::
      0002D9                       2921 	.ds 3
                           000003  2922 G$siblingSlots$0$0==.
      0002DC                       2923 _siblingSlots::
      0002DC                       2924 	.ds 2
                           000005  2925 G$demoPacketBuffer$0$0==.
      0002DE                       2926 _demoPacketBuffer::
      0002DE                       2927 	.ds 135
                           00008C  2928 G$msg1_tmr$0$0==.
      000365                       2929 _msg1_tmr::
      000365                       2930 	.ds 8
                           000094  2931 G$msg2_tmr$0$0==.
      00036D                       2932 _msg2_tmr::
      00036D                       2933 	.ds 8
                           00009C  2934 G$msg3_tmr$0$0==.
      000375                       2935 _msg3_tmr::
      000375                       2936 	.ds 8
                           0000A4  2937 Lpktframing.packetFraming$sib1$1$377==.
      00037D                       2938 _packetFraming_sib1_1_377:
      00037D                       2939 	.ds 1
                           0000A5  2940 Lpktframing.packetFraming$sib2$1$377==.
      00037E                       2941 _packetFraming_sib2_1_377:
      00037E                       2942 	.ds 1
                           0000A6  2943 Lpktframing.packetFraming$CRC32$1$377==.
      00037F                       2944 _packetFraming_CRC32_1_377:
      00037F                       2945 	.ds 4
                                   2946 ;--------------------------------------------------------
                                   2947 ; absolute external ram data
                                   2948 ;--------------------------------------------------------
                                   2949 	.area XABS    (ABS,XDATA)
                                   2950 ;--------------------------------------------------------
                                   2951 ; external initialized ram data
                                   2952 ;--------------------------------------------------------
                                   2953 	.area XISEG   (XDATA)
                           000000  2954 G$trmID$0$0==.
      00094F                       2955 _trmID::
      00094F                       2956 	.ds 4
                           000004  2957 G$dummyHMAC$0$0==.
      000953                       2958 _dummyHMAC::
      000953                       2959 	.ds 4
                                   2960 	.area HOME    (CODE)
                                   2961 	.area GSINIT0 (CODE)
                                   2962 	.area GSINIT1 (CODE)
                                   2963 	.area GSINIT2 (CODE)
                                   2964 	.area GSINIT3 (CODE)
                                   2965 	.area GSINIT4 (CODE)
                                   2966 	.area GSINIT5 (CODE)
                                   2967 	.area GSINIT  (CODE)
                                   2968 	.area GSFINAL (CODE)
                                   2969 	.area CSEG    (CODE)
                                   2970 ;--------------------------------------------------------
                                   2971 ; global & static initialisations
                                   2972 ;--------------------------------------------------------
                                   2973 	.area HOME    (CODE)
                                   2974 	.area GSINIT  (CODE)
                                   2975 	.area GSFINAL (CODE)
                                   2976 	.area GSINIT  (CODE)
                                   2977 ;--------------------------------------------------------
                                   2978 ; Home
                                   2979 ;--------------------------------------------------------
                                   2980 	.area HOME    (CODE)
                                   2981 	.area HOME    (CODE)
                                   2982 ;--------------------------------------------------------
                                   2983 ; code
                                   2984 ;--------------------------------------------------------
                                   2985 	.area CSEG    (CODE)
                                   2986 ;------------------------------------------------------------
                                   2987 ;Allocation info for local variables in function 'swap_variables'
                                   2988 ;------------------------------------------------------------
                                   2989 ;b                         Allocated with name '_swap_variables_PARM_2'
                                   2990 ;a                         Allocated to registers r5 r6 r7 
                                   2991 ;------------------------------------------------------------
                           000000  2992 	G$swap_variables$0$0 ==.
                           000000  2993 	C$pktframing.c$88$0$0 ==.
                                   2994 ;	pktframing.c:88: void swap_variables(uint8_t *a, uint8_t *b)
                                   2995 ;	-----------------------------------------
                                   2996 ;	 function swap_variables
                                   2997 ;	-----------------------------------------
      0050FE                       2998 _swap_variables:
                           000007  2999 	ar7 = 0x07
                           000006  3000 	ar6 = 0x06
                           000005  3001 	ar5 = 0x05
                           000004  3002 	ar4 = 0x04
                           000003  3003 	ar3 = 0x03
                           000002  3004 	ar2 = 0x02
                           000001  3005 	ar1 = 0x01
                           000000  3006 	ar0 = 0x00
                           000000  3007 	C$pktframing.c$90$1$371 ==.
                                   3008 ;	pktframing.c:90: *a = *a + *b;
      0050FE AD 82            [24] 3009 	mov	r5,dpl
      005100 AE 83            [24] 3010 	mov	r6,dph
      005102 AF F0            [24] 3011 	mov	r7,b
      005104 12 72 CA         [24] 3012 	lcall	__gptrget
      005107 FC               [12] 3013 	mov	r4,a
      005108 A9 64            [24] 3014 	mov	r1,_swap_variables_PARM_2
      00510A AA 65            [24] 3015 	mov	r2,(_swap_variables_PARM_2 + 1)
      00510C AB 66            [24] 3016 	mov	r3,(_swap_variables_PARM_2 + 2)
      00510E 89 82            [24] 3017 	mov	dpl,r1
      005110 8A 83            [24] 3018 	mov	dph,r2
      005112 8B F0            [24] 3019 	mov	b,r3
      005114 12 72 CA         [24] 3020 	lcall	__gptrget
      005117 2C               [12] 3021 	add	a,r4
      005118 FC               [12] 3022 	mov	r4,a
      005119 8D 82            [24] 3023 	mov	dpl,r5
      00511B 8E 83            [24] 3024 	mov	dph,r6
      00511D 8F F0            [24] 3025 	mov	b,r7
      00511F 12 64 AE         [24] 3026 	lcall	__gptrput
                           000024  3027 	C$pktframing.c$91$1$371 ==.
                                   3028 ;	pktframing.c:91: *b = *a - *b;
      005122 89 82            [24] 3029 	mov	dpl,r1
      005124 8A 83            [24] 3030 	mov	dph,r2
      005126 8B F0            [24] 3031 	mov	b,r3
      005128 12 72 CA         [24] 3032 	lcall	__gptrget
      00512B F8               [12] 3033 	mov	r0,a
      00512C EC               [12] 3034 	mov	a,r4
      00512D C3               [12] 3035 	clr	c
      00512E 98               [12] 3036 	subb	a,r0
      00512F F8               [12] 3037 	mov	r0,a
      005130 89 82            [24] 3038 	mov	dpl,r1
      005132 8A 83            [24] 3039 	mov	dph,r2
      005134 8B F0            [24] 3040 	mov	b,r3
      005136 12 64 AE         [24] 3041 	lcall	__gptrput
                           00003B  3042 	C$pktframing.c$92$1$371 ==.
                                   3043 ;	pktframing.c:92: *a = *a - *b;
      005139 EC               [12] 3044 	mov	a,r4
      00513A C3               [12] 3045 	clr	c
      00513B 98               [12] 3046 	subb	a,r0
      00513C 8D 82            [24] 3047 	mov	dpl,r5
      00513E 8E 83            [24] 3048 	mov	dph,r6
      005140 8F F0            [24] 3049 	mov	b,r7
      005142 12 64 AE         [24] 3050 	lcall	__gptrput
                           000047  3051 	C$pktframing.c$93$1$371 ==.
                           000047  3052 	XG$swap_variables$0$0 ==.
      005145 22               [24] 3053 	ret
                                   3054 ;------------------------------------------------------------
                                   3055 ;Allocation info for local variables in function 'randomset_generate'
                                   3056 ;------------------------------------------------------------
                           000048  3057 	G$randomset_generate$0$0 ==.
                           000048  3058 	C$pktframing.c$97$1$371 ==.
                                   3059 ;	pktframing.c:97: void randomset_generate(void)
                                   3060 ;	-----------------------------------------
                                   3061 ;	 function randomset_generate
                                   3062 ;	-----------------------------------------
      005146                       3063 _randomset_generate:
                           000048  3064 	C$pktframing.c$103$1$373 ==.
                                   3065 ;	pktframing.c:103: randomSet[0] = RNGBYTE % rangeRANDOM + slotBASE;
      005146 90 70 81         [24] 3066 	mov	dptr,#_RNGBYTE
      005149 E0               [24] 3067 	movx	a,@dptr
      00514A 75 F0 1E         [24] 3068 	mov	b,#0x1e
      00514D 84               [48] 3069 	div	ab
      00514E E5 F0            [12] 3070 	mov	a,b
      005150 24 05            [12] 3071 	add	a,#0x05
      005152 90 02 D9         [24] 3072 	mov	dptr,#_randomSet
      005155 F0               [24] 3073 	movx	@dptr,a
                           000058  3074 	C$pktframing.c$104$1$373 ==.
                                   3075 ;	pktframing.c:104: delay_ms(rngDELAY_MS);
      005156 90 00 0A         [24] 3076 	mov	dptr,#0x000a
      005159 12 44 91         [24] 3077 	lcall	_delay_ms
                           00005E  3078 	C$pktframing.c$105$1$373 ==.
                                   3079 ;	pktframing.c:105: randomSet[1] = RNGBYTE % rangeRANDOM + slotBASE;
      00515C 90 70 81         [24] 3080 	mov	dptr,#_RNGBYTE
      00515F E0               [24] 3081 	movx	a,@dptr
      005160 75 F0 1E         [24] 3082 	mov	b,#0x1e
      005163 84               [48] 3083 	div	ab
      005164 E5 F0            [12] 3084 	mov	a,b
      005166 24 05            [12] 3085 	add	a,#0x05
      005168 90 02 DA         [24] 3086 	mov	dptr,#(_randomSet + 0x0001)
      00516B F0               [24] 3087 	movx	@dptr,a
                           00006E  3088 	C$pktframing.c$106$1$373 ==.
                                   3089 ;	pktframing.c:106: delay_ms(rngDELAY_MS);
      00516C 90 00 0A         [24] 3090 	mov	dptr,#0x000a
      00516F 12 44 91         [24] 3091 	lcall	_delay_ms
                           000074  3092 	C$pktframing.c$107$1$373 ==.
                                   3093 ;	pktframing.c:107: randomSet[2] = RNGBYTE % rangeRANDOM + slotBASE;
      005172 90 70 81         [24] 3094 	mov	dptr,#_RNGBYTE
      005175 E0               [24] 3095 	movx	a,@dptr
      005176 75 F0 1E         [24] 3096 	mov	b,#0x1e
      005179 84               [48] 3097 	div	ab
      00517A E5 F0            [12] 3098 	mov	a,b
      00517C 24 05            [12] 3099 	add	a,#0x05
      00517E FF               [12] 3100 	mov	r7,a
      00517F 90 02 DB         [24] 3101 	mov	dptr,#(_randomSet + 0x0002)
      005182 F0               [24] 3102 	movx	@dptr,a
                           000085  3103 	C$pktframing.c$109$1$373 ==.
                                   3104 ;	pktframing.c:109: while(randomSet[0] == randomSet[1])
      005183                       3105 00101$:
      005183 90 02 D9         [24] 3106 	mov	dptr,#_randomSet
      005186 E0               [24] 3107 	movx	a,@dptr
      005187 FF               [12] 3108 	mov	r7,a
      005188 90 02 DA         [24] 3109 	mov	dptr,#(_randomSet + 0x0001)
      00518B E0               [24] 3110 	movx	a,@dptr
      00518C FE               [12] 3111 	mov	r6,a
      00518D EF               [12] 3112 	mov	a,r7
      00518E B5 06 19         [24] 3113 	cjne	a,ar6,00105$
                           000093  3114 	C$pktframing.c$111$2$374 ==.
                                   3115 ;	pktframing.c:111: delay_ms(rngDELAY_MS);
      005191 90 00 0A         [24] 3116 	mov	dptr,#0x000a
      005194 12 44 91         [24] 3117 	lcall	_delay_ms
                           000099  3118 	C$pktframing.c$112$2$374 ==.
                                   3119 ;	pktframing.c:112: randomSet[1] = RNGBYTE % rangeRANDOM + slotBASE;
      005197 90 70 81         [24] 3120 	mov	dptr,#_RNGBYTE
      00519A E0               [24] 3121 	movx	a,@dptr
      00519B 75 F0 1E         [24] 3122 	mov	b,#0x1e
      00519E 84               [48] 3123 	div	ab
      00519F E5 F0            [12] 3124 	mov	a,b
      0051A1 24 05            [12] 3125 	add	a,#0x05
      0051A3 FF               [12] 3126 	mov	r7,a
      0051A4 90 02 DA         [24] 3127 	mov	dptr,#(_randomSet + 0x0001)
      0051A7 F0               [24] 3128 	movx	@dptr,a
                           0000AA  3129 	C$pktframing.c$114$1$373 ==.
                                   3130 ;	pktframing.c:114: while ((randomSet[1] == randomSet[2]) || (randomSet[0] == randomSet[2]))
      0051A8 80 D9            [24] 3131 	sjmp	00101$
      0051AA                       3132 00105$:
      0051AA 90 02 DA         [24] 3133 	mov	dptr,#(_randomSet + 0x0001)
      0051AD E0               [24] 3134 	movx	a,@dptr
      0051AE FF               [12] 3135 	mov	r7,a
      0051AF 90 02 DB         [24] 3136 	mov	dptr,#(_randomSet + 0x0002)
      0051B2 E0               [24] 3137 	movx	a,@dptr
      0051B3 FE               [12] 3138 	mov	r6,a
      0051B4 EF               [12] 3139 	mov	a,r7
      0051B5 B5 06 02         [24] 3140 	cjne	a,ar6,00139$
      0051B8 80 0E            [24] 3141 	sjmp	00106$
      0051BA                       3142 00139$:
      0051BA 90 02 D9         [24] 3143 	mov	dptr,#_randomSet
      0051BD E0               [24] 3144 	movx	a,@dptr
      0051BE FF               [12] 3145 	mov	r7,a
      0051BF 90 02 DB         [24] 3146 	mov	dptr,#(_randomSet + 0x0002)
      0051C2 E0               [24] 3147 	movx	a,@dptr
      0051C3 FE               [12] 3148 	mov	r6,a
      0051C4 EF               [12] 3149 	mov	a,r7
      0051C5 B5 06 19         [24] 3150 	cjne	a,ar6,00107$
      0051C8                       3151 00106$:
                           0000CA  3152 	C$pktframing.c$116$2$375 ==.
                                   3153 ;	pktframing.c:116: delay_ms(rngDELAY_MS);
      0051C8 90 00 0A         [24] 3154 	mov	dptr,#0x000a
      0051CB 12 44 91         [24] 3155 	lcall	_delay_ms
                           0000D0  3156 	C$pktframing.c$117$2$375 ==.
                                   3157 ;	pktframing.c:117: randomSet[2] = RNGBYTE % rangeRANDOM + slotBASE;
      0051CE 90 70 81         [24] 3158 	mov	dptr,#_RNGBYTE
      0051D1 E0               [24] 3159 	movx	a,@dptr
      0051D2 75 F0 1E         [24] 3160 	mov	b,#0x1e
      0051D5 84               [48] 3161 	div	ab
      0051D6 E5 F0            [12] 3162 	mov	a,b
      0051D8 24 05            [12] 3163 	add	a,#0x05
      0051DA FF               [12] 3164 	mov	r7,a
      0051DB 90 02 DB         [24] 3165 	mov	dptr,#(_randomSet + 0x0002)
      0051DE F0               [24] 3166 	movx	@dptr,a
      0051DF 80 C9            [24] 3167 	sjmp	00105$
      0051E1                       3168 00107$:
                           0000E3  3169 	C$pktframing.c$121$1$373 ==.
                                   3170 ;	pktframing.c:121: if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
      0051E1 90 02 D9         [24] 3171 	mov	dptr,#_randomSet
      0051E4 E0               [24] 3172 	movx	a,@dptr
      0051E5 FF               [12] 3173 	mov	r7,a
      0051E6 90 02 DA         [24] 3174 	mov	dptr,#(_randomSet + 0x0001)
      0051E9 E0               [24] 3175 	movx	a,@dptr
      0051EA FE               [12] 3176 	mov	r6,a
      0051EB C3               [12] 3177 	clr	c
      0051EC 9F               [12] 3178 	subb	a,r7
      0051ED 50 12            [24] 3179 	jnc	00109$
      0051EF 75 64 DA         [24] 3180 	mov	_swap_variables_PARM_2,#(_randomSet + 0x0001)
      0051F2 75 65 02         [24] 3181 	mov	(_swap_variables_PARM_2 + 1),#((_randomSet + 0x0001) >> 8)
      0051F5 75 66 00         [24] 3182 	mov	(_swap_variables_PARM_2 + 2),#0x00
      0051F8 90 02 D9         [24] 3183 	mov	dptr,#_randomSet
      0051FB 75 F0 00         [24] 3184 	mov	b,#0x00
      0051FE 12 50 FE         [24] 3185 	lcall	_swap_variables
      005201                       3186 00109$:
                           000103  3187 	C$pktframing.c$122$1$373 ==.
                                   3188 ;	pktframing.c:122: if (randomSet[1] > randomSet[2]) swap_variables(&randomSet[1], &randomSet[2]);
      005201 90 02 DA         [24] 3189 	mov	dptr,#(_randomSet + 0x0001)
      005204 E0               [24] 3190 	movx	a,@dptr
      005205 FF               [12] 3191 	mov	r7,a
      005206 90 02 DB         [24] 3192 	mov	dptr,#(_randomSet + 0x0002)
      005209 E0               [24] 3193 	movx	a,@dptr
      00520A FE               [12] 3194 	mov	r6,a
      00520B C3               [12] 3195 	clr	c
      00520C 9F               [12] 3196 	subb	a,r7
      00520D 50 12            [24] 3197 	jnc	00111$
      00520F 75 64 DB         [24] 3198 	mov	_swap_variables_PARM_2,#(_randomSet + 0x0002)
      005212 75 65 02         [24] 3199 	mov	(_swap_variables_PARM_2 + 1),#((_randomSet + 0x0002) >> 8)
      005215 75 66 00         [24] 3200 	mov	(_swap_variables_PARM_2 + 2),#0x00
      005218 90 02 DA         [24] 3201 	mov	dptr,#(_randomSet + 0x0001)
      00521B 75 F0 00         [24] 3202 	mov	b,#0x00
      00521E 12 50 FE         [24] 3203 	lcall	_swap_variables
      005221                       3204 00111$:
                           000123  3205 	C$pktframing.c$123$1$373 ==.
                                   3206 ;	pktframing.c:123: if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
      005221 90 02 D9         [24] 3207 	mov	dptr,#_randomSet
      005224 E0               [24] 3208 	movx	a,@dptr
      005225 FF               [12] 3209 	mov	r7,a
      005226 90 02 DA         [24] 3210 	mov	dptr,#(_randomSet + 0x0001)
      005229 E0               [24] 3211 	movx	a,@dptr
      00522A FE               [12] 3212 	mov	r6,a
      00522B C3               [12] 3213 	clr	c
      00522C 9F               [12] 3214 	subb	a,r7
      00522D 50 12            [24] 3215 	jnc	00113$
      00522F 75 64 DA         [24] 3216 	mov	_swap_variables_PARM_2,#(_randomSet + 0x0001)
      005232 75 65 02         [24] 3217 	mov	(_swap_variables_PARM_2 + 1),#((_randomSet + 0x0001) >> 8)
      005235 75 66 00         [24] 3218 	mov	(_swap_variables_PARM_2 + 2),#0x00
      005238 90 02 D9         [24] 3219 	mov	dptr,#_randomSet
      00523B 75 F0 00         [24] 3220 	mov	b,#0x00
      00523E 12 50 FE         [24] 3221 	lcall	_swap_variables
      005241                       3222 00113$:
                           000143  3223 	C$pktframing.c$125$1$373 ==.
                                   3224 ;	pktframing.c:125: dbglink_writestr("RNG done: ");
      005241 90 77 A1         [24] 3225 	mov	dptr,#___str_0
      005244 75 F0 80         [24] 3226 	mov	b,#0x80
      005247 12 69 E1         [24] 3227 	lcall	_dbglink_writestr
                           00014C  3228 	C$pktframing.c$126$1$373 ==.
                                   3229 ;	pktframing.c:126: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      00524A 12 73 E2         [24] 3230 	lcall	_wtimer0_curtime
      00524D AC 82            [24] 3231 	mov	r4,dpl
      00524F AD 83            [24] 3232 	mov	r5,dph
      005251 AE F0            [24] 3233 	mov	r6,b
      005253 FF               [12] 3234 	mov	r7,a
      005254 74 08            [12] 3235 	mov	a,#0x08
      005256 C0 E0            [24] 3236 	push	acc
      005258 C0 E0            [24] 3237 	push	acc
      00525A 8C 82            [24] 3238 	mov	dpl,r4
      00525C 8D 83            [24] 3239 	mov	dph,r5
      00525E 8E F0            [24] 3240 	mov	b,r6
      005260 EF               [12] 3241 	mov	a,r7
      005261 12 6C 06         [24] 3242 	lcall	_dbglink_writehex32
      005264 15 81            [12] 3243 	dec	sp
      005266 15 81            [12] 3244 	dec	sp
                           00016A  3245 	C$pktframing.c$127$1$373 ==.
                                   3246 ;	pktframing.c:127: dbglink_tx('\n');
      005268 75 82 0A         [24] 3247 	mov	dpl,#0x0a
      00526B 12 58 D1         [24] 3248 	lcall	_dbglink_tx
                           000170  3249 	C$pktframing.c$129$1$373 ==.
                           000170  3250 	XG$randomset_generate$0$0 ==.
      00526E 22               [24] 3251 	ret
                                   3252 ;------------------------------------------------------------
                                   3253 ;Allocation info for local variables in function 'packetFraming'
                                   3254 ;------------------------------------------------------------
                                   3255 ;sloc0                     Allocated with name '_packetFraming_sloc0_1_0'
                                   3256 ;sloc1                     Allocated with name '_packetFraming_sloc1_1_0'
                                   3257 ;sloc2                     Allocated with name '_packetFraming_sloc2_1_0'
                                   3258 ;sloc3                     Allocated with name '_packetFraming_sloc3_1_0'
                                   3259 ;sloc4                     Allocated with name '_packetFraming_sloc4_1_0'
                                   3260 ;sloc5                     Allocated with name '_packetFraming_sloc5_1_0'
                                   3261 ;sloc6                     Allocated with name '_packetFraming_sloc6_1_0'
                                   3262 ;i                         Allocated with name '_packetFraming_i_1_377'
                                   3263 ;j                         Allocated with name '_packetFraming_j_1_377'
                                   3264 ;n                         Allocated with name '_packetFraming_n_1_377'
                                   3265 ;goldseq                   Allocated with name '_packetFraming_goldseq_1_377'
                                   3266 ;StdPrem                   Allocated with name '_packetFraming_StdPrem_1_377'
                                   3267 ;sib1                      Allocated with name '_packetFraming_sib1_1_377'
                                   3268 ;sib2                      Allocated with name '_packetFraming_sib2_1_377'
                                   3269 ;ack                       Allocated with name '_packetFraming_ack_1_377'
                                   3270 ;CRC32                     Allocated with name '_packetFraming_CRC32_1_377'
                                   3271 ;------------------------------------------------------------
                           000171  3272 	G$packetFraming$0$0 ==.
                           000171  3273 	C$pktframing.c$136$1$373 ==.
                                   3274 ;	pktframing.c:136: void packetFraming(void)
                                   3275 ;	-----------------------------------------
                                   3276 ;	 function packetFraming
                                   3277 ;	-----------------------------------------
      00526F                       3278 _packetFraming:
                           000171  3279 	C$pktframing.c$141$1$373 ==.
                                   3280 ;	pktframing.c:141: uint8_t __xdata sib1 = 0, sib2 = 0;
      00526F 90 03 7D         [24] 3281 	mov	dptr,#_packetFraming_sib1_1_377
      005272 E4               [12] 3282 	clr	a
      005273 F0               [24] 3283 	movx	@dptr,a
      005274 90 03 7E         [24] 3284 	mov	dptr,#_packetFraming_sib2_1_377
      005277 F0               [24] 3285 	movx	@dptr,a
                           00017A  3286 	C$pktframing.c$146$1$377 ==.
                                   3287 ;	pktframing.c:146: goldseq = GOLDSEQUENCE_Obtain();
      005278 12 42 A2         [24] 3288 	lcall	_GOLDSEQUENCE_Obtain
      00527B AC 82            [24] 3289 	mov	r4,dpl
      00527D AD 83            [24] 3290 	mov	r5,dph
      00527F AE F0            [24] 3291 	mov	r6,b
      005281 FF               [12] 3292 	mov	r7,a
                           000184  3293 	C$pktframing.c$148$1$377 ==.
                                   3294 ;	pktframing.c:148: randomset_generate();
      005282 C0 07            [24] 3295 	push	ar7
      005284 C0 06            [24] 3296 	push	ar6
      005286 C0 05            [24] 3297 	push	ar5
      005288 C0 04            [24] 3298 	push	ar4
      00528A 12 51 46         [24] 3299 	lcall	_randomset_generate
      00528D D0 04            [24] 3300 	pop	ar4
      00528F D0 05            [24] 3301 	pop	ar5
      005291 D0 06            [24] 3302 	pop	ar6
      005293 D0 07            [24] 3303 	pop	ar7
                           000197  3304 	C$pktframing.c$150$3$379 ==.
                                   3305 ;	pktframing.c:150: for (n = 0; n < 3; n++)
      005295 8C 41            [24] 3306 	mov	_packetFraming_sloc0_1_0,r4
      005297 75 42 00         [24] 3307 	mov	(_packetFraming_sloc0_1_0 + 1),#0x00
      00529A 75 43 00         [24] 3308 	mov	(_packetFraming_sloc0_1_0 + 2),#0x00
      00529D 75 44 00         [24] 3309 	mov	(_packetFraming_sloc0_1_0 + 3),#0x00
      0052A0 8D 01            [24] 3310 	mov	ar1,r5
      0052A2 89 45            [24] 3311 	mov	_packetFraming_sloc1_1_0,r1
      0052A4 78 00            [12] 3312 	mov	r0,#0x00
      0052A6 79 00            [12] 3313 	mov	r1,#0x00
      0052A8 8E 02            [24] 3314 	mov	ar2,r6
      0052AA 7B 00            [12] 3315 	mov	r3,#0x00
      0052AC 8A 47            [24] 3316 	mov	_packetFraming_sloc3_1_0,r2
      0052AE 7C 00            [12] 3317 	mov	r4,#0x00
      0052B0 7D 00            [12] 3318 	mov	r5,#0x00
      0052B2 53 07 7F         [24] 3319 	anl	ar7,#0x7f
      0052B5 8F 46            [24] 3320 	mov	_packetFraming_sloc2_1_0,r7
      0052B7 7E 00            [12] 3321 	mov	r6,#0x00
      0052B9                       3322 00107$:
                           0001BB  3323 	C$pktframing.c$153$2$378 ==.
                                   3324 ;	pktframing.c:153: dbglink_writenum16(n, 1, WRNUM_PADZERO);
      0052B9 8E 04            [24] 3325 	mov	ar4,r6
      0052BB 7D 00            [12] 3326 	mov	r5,#0x00
      0052BD C0 06            [24] 3327 	push	ar6
      0052BF 74 08            [12] 3328 	mov	a,#0x08
      0052C1 C0 E0            [24] 3329 	push	acc
      0052C3 74 01            [12] 3330 	mov	a,#0x01
      0052C5 C0 E0            [24] 3331 	push	acc
      0052C7 8C 82            [24] 3332 	mov	dpl,r4
      0052C9 8D 83            [24] 3333 	mov	dph,r5
      0052CB 12 72 E6         [24] 3334 	lcall	_dbglink_writenum16
      0052CE 15 81            [12] 3335 	dec	sp
      0052D0 15 81            [12] 3336 	dec	sp
      0052D2 D0 06            [24] 3337 	pop	ar6
                           0001D6  3338 	C$pktframing.c$154$2$378 ==.
                                   3339 ;	pktframing.c:154: dbglink_tx(' ');
      0052D4 75 82 20         [24] 3340 	mov	dpl,#0x20
      0052D7 12 58 D1         [24] 3341 	lcall	_dbglink_tx
                           0001DC  3342 	C$pktframing.c$156$2$378 ==.
                                   3343 ;	pktframing.c:156: switch(n)
      0052DA BE 00 02         [24] 3344 	cjne	r6,#0x00,00124$
      0052DD 80 0A            [24] 3345 	sjmp	00101$
      0052DF                       3346 00124$:
      0052DF BE 01 02         [24] 3347 	cjne	r6,#0x01,00125$
      0052E2 80 18            [24] 3348 	sjmp	00102$
      0052E4                       3349 00125$:
                           0001E6  3350 	C$pktframing.c$158$3$379 ==.
                                   3351 ;	pktframing.c:158: case 0:
      0052E4 BE 02 39         [24] 3352 	cjne	r6,#0x02,00105$
      0052E7 80 26            [24] 3353 	sjmp	00103$
      0052E9                       3354 00101$:
                           0001EB  3355 	C$pktframing.c$159$3$379 ==.
                                   3356 ;	pktframing.c:159: sib1 = randomSet[1];
      0052E9 90 02 DA         [24] 3357 	mov	dptr,#(_randomSet + 0x0001)
      0052EC E0               [24] 3358 	movx	a,@dptr
      0052ED 90 03 7D         [24] 3359 	mov	dptr,#_packetFraming_sib1_1_377
      0052F0 F0               [24] 3360 	movx	@dptr,a
                           0001F3  3361 	C$pktframing.c$160$3$379 ==.
                                   3362 ;	pktframing.c:160: sib2 = randomSet[2];
      0052F1 90 02 DB         [24] 3363 	mov	dptr,#(_randomSet + 0x0002)
      0052F4 E0               [24] 3364 	movx	a,@dptr
      0052F5 FD               [12] 3365 	mov	r5,a
      0052F6 90 03 7E         [24] 3366 	mov	dptr,#_packetFraming_sib2_1_377
      0052F9 F0               [24] 3367 	movx	@dptr,a
                           0001FC  3368 	C$pktframing.c$161$3$379 ==.
                                   3369 ;	pktframing.c:161: break;
                           0001FC  3370 	C$pktframing.c$163$3$379 ==.
                                   3371 ;	pktframing.c:163: case 1:
      0052FA 80 24            [24] 3372 	sjmp	00105$
      0052FC                       3373 00102$:
                           0001FE  3374 	C$pktframing.c$164$3$379 ==.
                                   3375 ;	pktframing.c:164: sib1 = randomSet[0];
      0052FC 90 02 D9         [24] 3376 	mov	dptr,#_randomSet
      0052FF E0               [24] 3377 	movx	a,@dptr
      005300 90 03 7D         [24] 3378 	mov	dptr,#_packetFraming_sib1_1_377
      005303 F0               [24] 3379 	movx	@dptr,a
                           000206  3380 	C$pktframing.c$165$3$379 ==.
                                   3381 ;	pktframing.c:165: sib2 = randomSet[2];
      005304 90 02 DB         [24] 3382 	mov	dptr,#(_randomSet + 0x0002)
      005307 E0               [24] 3383 	movx	a,@dptr
      005308 FD               [12] 3384 	mov	r5,a
      005309 90 03 7E         [24] 3385 	mov	dptr,#_packetFraming_sib2_1_377
      00530C F0               [24] 3386 	movx	@dptr,a
                           00020F  3387 	C$pktframing.c$166$3$379 ==.
                                   3388 ;	pktframing.c:166: break;
                           00020F  3389 	C$pktframing.c$168$3$379 ==.
                                   3390 ;	pktframing.c:168: case 2:
      00530D 80 11            [24] 3391 	sjmp	00105$
      00530F                       3392 00103$:
                           000211  3393 	C$pktframing.c$169$3$379 ==.
                                   3394 ;	pktframing.c:169: sib1 = randomSet[0];
      00530F 90 02 D9         [24] 3395 	mov	dptr,#_randomSet
      005312 E0               [24] 3396 	movx	a,@dptr
      005313 90 03 7D         [24] 3397 	mov	dptr,#_packetFraming_sib1_1_377
      005316 F0               [24] 3398 	movx	@dptr,a
                           000219  3399 	C$pktframing.c$170$3$379 ==.
                                   3400 ;	pktframing.c:170: sib2 = randomSet[1];
      005317 90 02 DA         [24] 3401 	mov	dptr,#(_randomSet + 0x0001)
      00531A E0               [24] 3402 	movx	a,@dptr
      00531B FD               [12] 3403 	mov	r5,a
      00531C 90 03 7E         [24] 3404 	mov	dptr,#_packetFraming_sib2_1_377
      00531F F0               [24] 3405 	movx	@dptr,a
                           000222  3406 	C$pktframing.c$175$2$378 ==.
                                   3407 ;	pktframing.c:175: }
      005320                       3408 00105$:
                           000222  3409 	C$pktframing.c$177$2$378 ==.
                                   3410 ;	pktframing.c:177: dbglink_writehex16(sib1, 2, WRNUM_PADZERO);
      005320 90 03 7D         [24] 3411 	mov	dptr,#_packetFraming_sib1_1_377
      005323 E0               [24] 3412 	movx	a,@dptr
      005324 FD               [12] 3413 	mov	r5,a
      005325 FA               [12] 3414 	mov	r2,a
      005326 7C 00            [12] 3415 	mov	r4,#0x00
      005328 C0 06            [24] 3416 	push	ar6
      00532A C0 05            [24] 3417 	push	ar5
      00532C 74 08            [12] 3418 	mov	a,#0x08
      00532E C0 E0            [24] 3419 	push	acc
      005330 74 02            [12] 3420 	mov	a,#0x02
      005332 C0 E0            [24] 3421 	push	acc
      005334 8A 82            [24] 3422 	mov	dpl,r2
      005336 8C 83            [24] 3423 	mov	dph,r4
      005338 12 6E 24         [24] 3424 	lcall	_dbglink_writehex16
      00533B 15 81            [12] 3425 	dec	sp
      00533D 15 81            [12] 3426 	dec	sp
                           000241  3427 	C$pktframing.c$178$2$378 ==.
                                   3428 ;	pktframing.c:178: dbglink_writehex16(sib2, 2, WRNUM_PADZERO);
      00533F 90 03 7E         [24] 3429 	mov	dptr,#_packetFraming_sib2_1_377
      005342 E0               [24] 3430 	movx	a,@dptr
      005343 F5 48            [12] 3431 	mov	_packetFraming_sloc4_1_0,a
      005345 A9 48            [24] 3432 	mov	r1,_packetFraming_sloc4_1_0
      005347 7A 00            [12] 3433 	mov	r2,#0x00
      005349 74 08            [12] 3434 	mov	a,#0x08
      00534B C0 E0            [24] 3435 	push	acc
      00534D 74 02            [12] 3436 	mov	a,#0x02
      00534F C0 E0            [24] 3437 	push	acc
      005351 89 82            [24] 3438 	mov	dpl,r1
      005353 8A 83            [24] 3439 	mov	dph,r2
      005355 12 6E 24         [24] 3440 	lcall	_dbglink_writehex16
      005358 15 81            [12] 3441 	dec	sp
      00535A 15 81            [12] 3442 	dec	sp
      00535C D0 05            [24] 3443 	pop	ar5
      00535E D0 06            [24] 3444 	pop	ar6
                           000262  3445 	C$pktframing.c$179$2$378 ==.
                                   3446 ;	pktframing.c:179: dbglink_tx('\n');
      005360 75 82 0A         [24] 3447 	mov	dpl,#0x0a
      005363 12 58 D1         [24] 3448 	lcall	_dbglink_tx
                           000268  3449 	C$pktframing.c$182$2$378 ==.
                                   3450 ;	pktframing.c:182: *(demoPacketBuffer + (n*pktLENGTH)) = (uint8_t) (goldseq & 0x000000FF);
      005366 EE               [12] 3451 	mov	a,r6
      005367 75 F0 2D         [24] 3452 	mov	b,#0x2d
      00536A A4               [48] 3453 	mul	ab
      00536B F9               [12] 3454 	mov	r1,a
      00536C AA F0            [24] 3455 	mov	r2,b
      00536E 24 DE            [12] 3456 	add	a,#_demoPacketBuffer
      005370 F5 4B            [12] 3457 	mov	_packetFraming_sloc6_1_0,a
      005372 EA               [12] 3458 	mov	a,r2
      005373 34 02            [12] 3459 	addc	a,#(_demoPacketBuffer >> 8)
      005375 F5 4C            [12] 3460 	mov	(_packetFraming_sloc6_1_0 + 1),a
      005377 AB 41            [24] 3461 	mov	r3,_packetFraming_sloc0_1_0
      005379 85 4B 82         [24] 3462 	mov	dpl,_packetFraming_sloc6_1_0
      00537C 85 4C 83         [24] 3463 	mov	dph,(_packetFraming_sloc6_1_0 + 1)
      00537F EB               [12] 3464 	mov	a,r3
      005380 F0               [24] 3465 	movx	@dptr,a
                           000283  3466 	C$pktframing.c$187$2$378 ==.
                                   3467 ;	pktframing.c:187: *(demoPacketBuffer + (n*pktLENGTH + 1)) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
      005381 74 01            [12] 3468 	mov	a,#0x01
      005383 29               [12] 3469 	add	a,r1
      005384 FB               [12] 3470 	mov	r3,a
      005385 E4               [12] 3471 	clr	a
      005386 3A               [12] 3472 	addc	a,r2
      005387 FC               [12] 3473 	mov	r4,a
      005388 EB               [12] 3474 	mov	a,r3
      005389 24 DE            [12] 3475 	add	a,#_demoPacketBuffer
      00538B F5 82            [12] 3476 	mov	dpl,a
      00538D EC               [12] 3477 	mov	a,r4
      00538E 34 02            [12] 3478 	addc	a,#(_demoPacketBuffer >> 8)
      005390 F5 83            [12] 3479 	mov	dph,a
      005392 E5 45            [12] 3480 	mov	a,_packetFraming_sloc1_1_0
      005394 F0               [24] 3481 	movx	@dptr,a
                           000297  3482 	C$pktframing.c$188$2$378 ==.
                                   3483 ;	pktframing.c:188: *(demoPacketBuffer + (n*pktLENGTH + 2)) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
      005395 74 02            [12] 3484 	mov	a,#0x02
      005397 29               [12] 3485 	add	a,r1
      005398 FB               [12] 3486 	mov	r3,a
      005399 E4               [12] 3487 	clr	a
      00539A 3A               [12] 3488 	addc	a,r2
      00539B FC               [12] 3489 	mov	r4,a
      00539C EB               [12] 3490 	mov	a,r3
      00539D 24 DE            [12] 3491 	add	a,#_demoPacketBuffer
      00539F F5 82            [12] 3492 	mov	dpl,a
      0053A1 EC               [12] 3493 	mov	a,r4
      0053A2 34 02            [12] 3494 	addc	a,#(_demoPacketBuffer >> 8)
      0053A4 F5 83            [12] 3495 	mov	dph,a
      0053A6 E5 47            [12] 3496 	mov	a,_packetFraming_sloc3_1_0
      0053A8 F0               [24] 3497 	movx	@dptr,a
                           0002AB  3498 	C$pktframing.c$189$2$378 ==.
                                   3499 ;	pktframing.c:189: *(demoPacketBuffer + (n*pktLENGTH + 3)) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (sib1 & 0x01) /*<< 7*/);
      0053A9 74 03            [12] 3500 	mov	a,#0x03
      0053AB 29               [12] 3501 	add	a,r1
      0053AC FB               [12] 3502 	mov	r3,a
      0053AD E4               [12] 3503 	clr	a
      0053AE 3A               [12] 3504 	addc	a,r2
      0053AF FC               [12] 3505 	mov	r4,a
      0053B0 EB               [12] 3506 	mov	a,r3
      0053B1 24 DE            [12] 3507 	add	a,#_demoPacketBuffer
      0053B3 F5 82            [12] 3508 	mov	dpl,a
      0053B5 EC               [12] 3509 	mov	a,r4
      0053B6 34 02            [12] 3510 	addc	a,#(_demoPacketBuffer >> 8)
      0053B8 F5 83            [12] 3511 	mov	dph,a
      0053BA 74 01            [12] 3512 	mov	a,#0x01
      0053BC 5D               [12] 3513 	anl	a,r5
      0053BD AB 46            [24] 3514 	mov	r3,_packetFraming_sloc2_1_0
      0053BF 42 03            [12] 3515 	orl	ar3,a
      0053C1 EB               [12] 3516 	mov	a,r3
      0053C2 F0               [24] 3517 	movx	@dptr,a
                           0002C5  3518 	C$pktframing.c$190$2$378 ==.
                                   3519 ;	pktframing.c:190: *(demoPacketBuffer + (n*pktLENGTH + 4)) = (uint8_t) (sib1 & 0x3E)>>1 | ((sib2 & 0x07)<<5); // changed 0xFE to 0x3E
      0053C3 74 04            [12] 3520 	mov	a,#0x04
      0053C5 29               [12] 3521 	add	a,r1
      0053C6 FB               [12] 3522 	mov	r3,a
      0053C7 E4               [12] 3523 	clr	a
      0053C8 3A               [12] 3524 	addc	a,r2
      0053C9 FC               [12] 3525 	mov	r4,a
      0053CA EB               [12] 3526 	mov	a,r3
      0053CB 24 DE            [12] 3527 	add	a,#_demoPacketBuffer
      0053CD F5 82            [12] 3528 	mov	dpl,a
      0053CF EC               [12] 3529 	mov	a,r4
      0053D0 34 02            [12] 3530 	addc	a,#(_demoPacketBuffer >> 8)
      0053D2 F5 83            [12] 3531 	mov	dph,a
      0053D4 53 05 3E         [24] 3532 	anl	ar5,#0x3e
      0053D7 ED               [12] 3533 	mov	a,r5
      0053D8 C3               [12] 3534 	clr	c
      0053D9 13               [12] 3535 	rrc	a
      0053DA FD               [12] 3536 	mov	r5,a
      0053DB 74 07            [12] 3537 	mov	a,#0x07
      0053DD 55 48            [12] 3538 	anl	a,_packetFraming_sloc4_1_0
      0053DF C4               [12] 3539 	swap	a
      0053E0 23               [12] 3540 	rl	a
      0053E1 54 E0            [12] 3541 	anl	a,#0xe0
      0053E3 42 05            [12] 3542 	orl	ar5,a
      0053E5 ED               [12] 3543 	mov	a,r5
      0053E6 F0               [24] 3544 	movx	@dptr,a
                           0002E9  3545 	C$pktframing.c$191$2$378 ==.
                                   3546 ;	pktframing.c:191: *(demoPacketBuffer + (n*pktLENGTH + 5)) = (uint8_t) (sib2 & 0x38)>>3 | (trmID & 0x00001F)<<3;
      0053E7 74 05            [12] 3547 	mov	a,#0x05
      0053E9 29               [12] 3548 	add	a,r1
      0053EA FC               [12] 3549 	mov	r4,a
      0053EB E4               [12] 3550 	clr	a
      0053EC 3A               [12] 3551 	addc	a,r2
      0053ED FD               [12] 3552 	mov	r5,a
      0053EE EC               [12] 3553 	mov	a,r4
      0053EF 24 DE            [12] 3554 	add	a,#_demoPacketBuffer
      0053F1 F5 49            [12] 3555 	mov	_packetFraming_sloc5_1_0,a
      0053F3 ED               [12] 3556 	mov	a,r5
      0053F4 34 02            [12] 3557 	addc	a,#(_demoPacketBuffer >> 8)
      0053F6 F5 4A            [12] 3558 	mov	(_packetFraming_sloc5_1_0 + 1),a
      0053F8 74 38            [12] 3559 	mov	a,#0x38
      0053FA 55 48            [12] 3560 	anl	a,_packetFraming_sloc4_1_0
      0053FC C4               [12] 3561 	swap	a
      0053FD 23               [12] 3562 	rl	a
      0053FE 54 1F            [12] 3563 	anl	a,#0x1f
      005400 F5 48            [12] 3564 	mov	_packetFraming_sloc4_1_0,a
      005402 90 09 4F         [24] 3565 	mov	dptr,#_trmID
      005405 E0               [24] 3566 	movx	a,@dptr
      005406 FB               [12] 3567 	mov	r3,a
      005407 A3               [24] 3568 	inc	dptr
      005408 E0               [24] 3569 	movx	a,@dptr
      005409 A3               [24] 3570 	inc	dptr
      00540A E0               [24] 3571 	movx	a,@dptr
      00540B A3               [24] 3572 	inc	dptr
      00540C E0               [24] 3573 	movx	a,@dptr
      00540D 53 03 1F         [24] 3574 	anl	ar3,#0x1f
      005410 E4               [12] 3575 	clr	a
      005411 EB               [12] 3576 	mov	a,r3
      005412 C4               [12] 3577 	swap	a
      005413 03               [12] 3578 	rr	a
      005414 54 F8            [12] 3579 	anl	a,#0xf8
      005416 FB               [12] 3580 	mov	r3,a
      005417 E5 48            [12] 3581 	mov	a,_packetFraming_sloc4_1_0
      005419 42 03            [12] 3582 	orl	ar3,a
      00541B 85 49 82         [24] 3583 	mov	dpl,_packetFraming_sloc5_1_0
      00541E 85 4A 83         [24] 3584 	mov	dph,(_packetFraming_sloc5_1_0 + 1)
      005421 EB               [12] 3585 	mov	a,r3
      005422 F0               [24] 3586 	movx	@dptr,a
                           000325  3587 	C$pktframing.c$196$2$378 ==.
                                   3588 ;	pktframing.c:196: *(demoPacketBuffer + (n*pktLENGTH + 6)) = (uint8_t) ((trmID & 0x001FE0) >> 5);
      005423 74 06            [12] 3589 	mov	a,#0x06
      005425 29               [12] 3590 	add	a,r1
      005426 FD               [12] 3591 	mov	r5,a
      005427 E4               [12] 3592 	clr	a
      005428 3A               [12] 3593 	addc	a,r2
      005429 FF               [12] 3594 	mov	r7,a
      00542A ED               [12] 3595 	mov	a,r5
      00542B 24 DE            [12] 3596 	add	a,#_demoPacketBuffer
      00542D F5 49            [12] 3597 	mov	_packetFraming_sloc5_1_0,a
      00542F EF               [12] 3598 	mov	a,r7
      005430 34 02            [12] 3599 	addc	a,#(_demoPacketBuffer >> 8)
      005432 F5 4A            [12] 3600 	mov	(_packetFraming_sloc5_1_0 + 1),a
      005434 90 09 4F         [24] 3601 	mov	dptr,#_trmID
      005437 E0               [24] 3602 	movx	a,@dptr
      005438 F8               [12] 3603 	mov	r0,a
      005439 A3               [24] 3604 	inc	dptr
      00543A E0               [24] 3605 	movx	a,@dptr
      00543B FB               [12] 3606 	mov	r3,a
      00543C A3               [24] 3607 	inc	dptr
      00543D E0               [24] 3608 	movx	a,@dptr
      00543E A3               [24] 3609 	inc	dptr
      00543F E0               [24] 3610 	movx	a,@dptr
      005440 53 00 E0         [24] 3611 	anl	ar0,#0xe0
      005443 53 03 1F         [24] 3612 	anl	ar3,#0x1f
      005446 E4               [12] 3613 	clr	a
      005447 FC               [12] 3614 	mov	r4,a
      005448 FF               [12] 3615 	mov	r7,a
      005449 EB               [12] 3616 	mov	a,r3
      00544A C4               [12] 3617 	swap	a
      00544B 03               [12] 3618 	rr	a
      00544C C8               [12] 3619 	xch	a,r0
      00544D C4               [12] 3620 	swap	a
      00544E 03               [12] 3621 	rr	a
      00544F 54 07            [12] 3622 	anl	a,#0x07
      005451 68               [12] 3623 	xrl	a,r0
      005452 C8               [12] 3624 	xch	a,r0
      005453 54 07            [12] 3625 	anl	a,#0x07
      005455 C8               [12] 3626 	xch	a,r0
      005456 68               [12] 3627 	xrl	a,r0
      005457 C8               [12] 3628 	xch	a,r0
      005458 FB               [12] 3629 	mov	r3,a
      005459 EC               [12] 3630 	mov	a,r4
      00545A C4               [12] 3631 	swap	a
      00545B 03               [12] 3632 	rr	a
      00545C 54 F8            [12] 3633 	anl	a,#0xf8
      00545E 4B               [12] 3634 	orl	a,r3
      00545F EF               [12] 3635 	mov	a,r7
      005460 C4               [12] 3636 	swap	a
      005461 03               [12] 3637 	rr	a
      005462 CC               [12] 3638 	xch	a,r4
      005463 C4               [12] 3639 	swap	a
      005464 03               [12] 3640 	rr	a
      005465 54 07            [12] 3641 	anl	a,#0x07
      005467 6C               [12] 3642 	xrl	a,r4
      005468 CC               [12] 3643 	xch	a,r4
      005469 54 07            [12] 3644 	anl	a,#0x07
      00546B CC               [12] 3645 	xch	a,r4
      00546C 6C               [12] 3646 	xrl	a,r4
      00546D CC               [12] 3647 	xch	a,r4
      00546E 85 49 82         [24] 3648 	mov	dpl,_packetFraming_sloc5_1_0
      005471 85 4A 83         [24] 3649 	mov	dph,(_packetFraming_sloc5_1_0 + 1)
      005474 E8               [12] 3650 	mov	a,r0
      005475 F0               [24] 3651 	movx	@dptr,a
                           000378  3652 	C$pktframing.c$197$2$378 ==.
                                   3653 ;	pktframing.c:197: *(demoPacketBuffer + (n*pktLENGTH + 7)) = (uint8_t) ((trmID & 0x1FE000) >> 13);
      005476 74 07            [12] 3654 	mov	a,#0x07
      005478 29               [12] 3655 	add	a,r1
      005479 FD               [12] 3656 	mov	r5,a
      00547A E4               [12] 3657 	clr	a
      00547B 3A               [12] 3658 	addc	a,r2
      00547C FF               [12] 3659 	mov	r7,a
      00547D ED               [12] 3660 	mov	a,r5
      00547E 24 DE            [12] 3661 	add	a,#_demoPacketBuffer
      005480 F5 49            [12] 3662 	mov	_packetFraming_sloc5_1_0,a
      005482 EF               [12] 3663 	mov	a,r7
      005483 34 02            [12] 3664 	addc	a,#(_demoPacketBuffer >> 8)
      005485 F5 4A            [12] 3665 	mov	(_packetFraming_sloc5_1_0 + 1),a
      005487 90 09 4F         [24] 3666 	mov	dptr,#_trmID
      00548A E0               [24] 3667 	movx	a,@dptr
      00548B A3               [24] 3668 	inc	dptr
      00548C E0               [24] 3669 	movx	a,@dptr
      00548D FB               [12] 3670 	mov	r3,a
      00548E A3               [24] 3671 	inc	dptr
      00548F E0               [24] 3672 	movx	a,@dptr
      005490 FC               [12] 3673 	mov	r4,a
      005491 A3               [24] 3674 	inc	dptr
      005492 E0               [24] 3675 	movx	a,@dptr
      005493 53 03 E0         [24] 3676 	anl	ar3,#0xe0
      005496 53 04 1F         [24] 3677 	anl	ar4,#0x1f
      005499 7F 00            [12] 3678 	mov	r7,#0x00
      00549B 8B 00            [24] 3679 	mov	ar0,r3
      00549D EC               [12] 3680 	mov	a,r4
      00549E C4               [12] 3681 	swap	a
      00549F 03               [12] 3682 	rr	a
      0054A0 C8               [12] 3683 	xch	a,r0
      0054A1 C4               [12] 3684 	swap	a
      0054A2 03               [12] 3685 	rr	a
      0054A3 54 07            [12] 3686 	anl	a,#0x07
      0054A5 68               [12] 3687 	xrl	a,r0
      0054A6 C8               [12] 3688 	xch	a,r0
      0054A7 54 07            [12] 3689 	anl	a,#0x07
      0054A9 C8               [12] 3690 	xch	a,r0
      0054AA 68               [12] 3691 	xrl	a,r0
      0054AB C8               [12] 3692 	xch	a,r0
      0054AC FB               [12] 3693 	mov	r3,a
      0054AD EF               [12] 3694 	mov	a,r7
      0054AE C4               [12] 3695 	swap	a
      0054AF 03               [12] 3696 	rr	a
      0054B0 54 F8            [12] 3697 	anl	a,#0xf8
      0054B2 4B               [12] 3698 	orl	a,r3
      0054B3 EF               [12] 3699 	mov	a,r7
      0054B4 C4               [12] 3700 	swap	a
      0054B5 03               [12] 3701 	rr	a
      0054B6 54 07            [12] 3702 	anl	a,#0x07
      0054B8 85 49 82         [24] 3703 	mov	dpl,_packetFraming_sloc5_1_0
      0054BB 85 4A 83         [24] 3704 	mov	dph,(_packetFraming_sloc5_1_0 + 1)
      0054BE E8               [12] 3705 	mov	a,r0
      0054BF F0               [24] 3706 	movx	@dptr,a
                           0003C2  3707 	C$pktframing.c$198$2$378 ==.
                                   3708 ;	pktframing.c:198: *(demoPacketBuffer + (n*pktLENGTH + 8)) = (uint8_t) ((trmID & 0xE00000) >> 21) | (StdPrem & 0x0001) << 4 | (ack << 5 & 0x0001); // why <<5 why 0x0001?
      0054C0 74 08            [12] 3709 	mov	a,#0x08
      0054C2 29               [12] 3710 	add	a,r1
      0054C3 F9               [12] 3711 	mov	r1,a
      0054C4 E4               [12] 3712 	clr	a
      0054C5 3A               [12] 3713 	addc	a,r2
      0054C6 FA               [12] 3714 	mov	r2,a
      0054C7 E9               [12] 3715 	mov	a,r1
      0054C8 24 DE            [12] 3716 	add	a,#_demoPacketBuffer
      0054CA F9               [12] 3717 	mov	r1,a
      0054CB EA               [12] 3718 	mov	a,r2
      0054CC 34 02            [12] 3719 	addc	a,#(_demoPacketBuffer >> 8)
      0054CE FA               [12] 3720 	mov	r2,a
      0054CF 90 09 4F         [24] 3721 	mov	dptr,#_trmID
      0054D2 E0               [24] 3722 	movx	a,@dptr
      0054D3 A3               [24] 3723 	inc	dptr
      0054D4 E0               [24] 3724 	movx	a,@dptr
      0054D5 A3               [24] 3725 	inc	dptr
      0054D6 E0               [24] 3726 	movx	a,@dptr
      0054D7 FD               [12] 3727 	mov	r5,a
      0054D8 A3               [24] 3728 	inc	dptr
      0054D9 E0               [24] 3729 	movx	a,@dptr
      0054DA 53 05 E0         [24] 3730 	anl	ar5,#0xe0
      0054DD 7F 00            [12] 3731 	mov	r7,#0x00
      0054DF 8D 03            [24] 3732 	mov	ar3,r5
      0054E1 EF               [12] 3733 	mov	a,r7
      0054E2 C4               [12] 3734 	swap	a
      0054E3 03               [12] 3735 	rr	a
      0054E4 CB               [12] 3736 	xch	a,r3
      0054E5 C4               [12] 3737 	swap	a
      0054E6 03               [12] 3738 	rr	a
      0054E7 54 07            [12] 3739 	anl	a,#0x07
      0054E9 6B               [12] 3740 	xrl	a,r3
      0054EA CB               [12] 3741 	xch	a,r3
      0054EB 54 07            [12] 3742 	anl	a,#0x07
      0054ED CB               [12] 3743 	xch	a,r3
      0054EE 6B               [12] 3744 	xrl	a,r3
      0054EF CB               [12] 3745 	xch	a,r3
      0054F0 43 03 10         [24] 3746 	orl	ar3,#0x10
      0054F3 89 82            [24] 3747 	mov	dpl,r1
      0054F5 8A 83            [24] 3748 	mov	dph,r2
      0054F7 EB               [12] 3749 	mov	a,r3
      0054F8 F0               [24] 3750 	movx	@dptr,a
                           0003FB  3751 	C$pktframing.c$200$2$378 ==.
                                   3752 ;	pktframing.c:200: memcpy((demoPacketBuffer +  n*pktLENGTH + 9), &dummyHMAC, sizeof(uint32_t));
      0054F9 74 09            [12] 3753 	mov	a,#0x09
      0054FB 25 4B            [12] 3754 	add	a,_packetFraming_sloc6_1_0
      0054FD FD               [12] 3755 	mov	r5,a
      0054FE E4               [12] 3756 	clr	a
      0054FF 35 4C            [12] 3757 	addc	a,(_packetFraming_sloc6_1_0 + 1)
      005501 FF               [12] 3758 	mov	r7,a
      005502 7C 00            [12] 3759 	mov	r4,#0x00
      005504 75 64 53         [24] 3760 	mov	_memcpy_PARM_2,#_dummyHMAC
      005507 75 65 09         [24] 3761 	mov	(_memcpy_PARM_2 + 1),#(_dummyHMAC >> 8)
                                   3762 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      00550A 8C 66            [24] 3763 	mov	(_memcpy_PARM_2 + 2),r4
      00550C 75 67 04         [24] 3764 	mov	_memcpy_PARM_3,#0x04
                                   3765 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_3 + 1),#0x00
      00550F 8C 68            [24] 3766 	mov	(_memcpy_PARM_3 + 1),r4
      005511 8D 82            [24] 3767 	mov	dpl,r5
      005513 8F 83            [24] 3768 	mov	dph,r7
      005515 8C F0            [24] 3769 	mov	b,r4
      005517 C0 06            [24] 3770 	push	ar6
      005519 12 60 C7         [24] 3771 	lcall	_memcpy
      00551C D0 06            [24] 3772 	pop	ar6
                           000420  3773 	C$pktframing.c$201$2$378 ==.
                                   3774 ;	pktframing.c:201: memcpy((demoPacketBuffer +  n*pktLENGTH + 13), payload, sizeof(uint8_t)*payload_len);
      00551E 74 0D            [12] 3775 	mov	a,#0x0d
      005520 25 4B            [12] 3776 	add	a,_packetFraming_sloc6_1_0
      005522 FD               [12] 3777 	mov	r5,a
      005523 E4               [12] 3778 	clr	a
      005524 35 4C            [12] 3779 	addc	a,(_packetFraming_sloc6_1_0 + 1)
      005526 FF               [12] 3780 	mov	r7,a
      005527 8D 02            [24] 3781 	mov	ar2,r5
      005529 8F 03            [24] 3782 	mov	ar3,r7
      00552B 7C 00            [12] 3783 	mov	r4,#0x00
      00552D 85 3F 64         [24] 3784 	mov	_memcpy_PARM_2,_payload
      005530 85 40 65         [24] 3785 	mov	(_memcpy_PARM_2 + 1),(_payload + 1)
                                   3786 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      005533 8C 66            [24] 3787 	mov	(_memcpy_PARM_2 + 2),r4
      005535 90 02 D7         [24] 3788 	mov	dptr,#_payload_len
      005538 E0               [24] 3789 	movx	a,@dptr
      005539 F5 67            [12] 3790 	mov	_memcpy_PARM_3,a
      00553B A3               [24] 3791 	inc	dptr
      00553C E0               [24] 3792 	movx	a,@dptr
      00553D F5 68            [12] 3793 	mov	(_memcpy_PARM_3 + 1),a
      00553F 8A 82            [24] 3794 	mov	dpl,r2
      005541 8B 83            [24] 3795 	mov	dph,r3
      005543 8C F0            [24] 3796 	mov	b,r4
      005545 C0 07            [24] 3797 	push	ar7
      005547 C0 06            [24] 3798 	push	ar6
      005549 C0 05            [24] 3799 	push	ar5
      00554B 12 60 C7         [24] 3800 	lcall	_memcpy
                           000450  3801 	C$pktframing.c$203$2$378 ==.
                                   3802 ;	pktframing.c:203: CRC32 = crc_crc32_msb((demoPacketBuffer+ n*pktLENGTH), (13+payload_len), 0xFFFFFFFF);
      00554E 90 02 D7         [24] 3803 	mov	dptr,#_payload_len
      005551 E0               [24] 3804 	movx	a,@dptr
      005552 FB               [12] 3805 	mov	r3,a
      005553 A3               [24] 3806 	inc	dptr
      005554 E0               [24] 3807 	movx	a,@dptr
      005555 FC               [12] 3808 	mov	r4,a
      005556 74 0D            [12] 3809 	mov	a,#0x0d
      005558 2B               [12] 3810 	add	a,r3
      005559 FB               [12] 3811 	mov	r3,a
      00555A E4               [12] 3812 	clr	a
      00555B 3C               [12] 3813 	addc	a,r4
      00555C FC               [12] 3814 	mov	r4,a
      00555D A9 4B            [24] 3815 	mov	r1,_packetFraming_sloc6_1_0
      00555F A8 4C            [24] 3816 	mov	r0,(_packetFraming_sloc6_1_0 + 1)
      005561 7A 00            [12] 3817 	mov	r2,#0x00
      005563 74 FF            [12] 3818 	mov	a,#0xff
      005565 C0 E0            [24] 3819 	push	acc
      005567 C0 E0            [24] 3820 	push	acc
      005569 C0 E0            [24] 3821 	push	acc
      00556B C0 E0            [24] 3822 	push	acc
      00556D C0 03            [24] 3823 	push	ar3
      00556F C0 04            [24] 3824 	push	ar4
      005571 89 82            [24] 3825 	mov	dpl,r1
      005573 88 83            [24] 3826 	mov	dph,r0
      005575 8A F0            [24] 3827 	mov	b,r2
      005577 12 6F DC         [24] 3828 	lcall	_crc_crc32_msb
      00557A A9 82            [24] 3829 	mov	r1,dpl
      00557C AA 83            [24] 3830 	mov	r2,dph
      00557E AB F0            [24] 3831 	mov	r3,b
      005580 FC               [12] 3832 	mov	r4,a
      005581 E5 81            [12] 3833 	mov	a,sp
      005583 24 FA            [12] 3834 	add	a,#0xfa
      005585 F5 81            [12] 3835 	mov	sp,a
      005587 D0 05            [24] 3836 	pop	ar5
      005589 D0 06            [24] 3837 	pop	ar6
      00558B D0 07            [24] 3838 	pop	ar7
      00558D 90 03 7F         [24] 3839 	mov	dptr,#_packetFraming_CRC32_1_377
      005590 E9               [12] 3840 	mov	a,r1
      005591 F0               [24] 3841 	movx	@dptr,a
      005592 EA               [12] 3842 	mov	a,r2
      005593 A3               [24] 3843 	inc	dptr
      005594 F0               [24] 3844 	movx	@dptr,a
      005595 EB               [12] 3845 	mov	a,r3
      005596 A3               [24] 3846 	inc	dptr
      005597 F0               [24] 3847 	movx	@dptr,a
      005598 EC               [12] 3848 	mov	a,r4
      005599 A3               [24] 3849 	inc	dptr
      00559A F0               [24] 3850 	movx	@dptr,a
                           00049D  3851 	C$pktframing.c$204$2$378 ==.
                                   3852 ;	pktframing.c:204: memcpy((demoPacketBuffer+n*pktLENGTH+13+payload_len), &CRC32, sizeof(uint32_t));
      00559B 90 02 D7         [24] 3853 	mov	dptr,#_payload_len
      00559E E0               [24] 3854 	movx	a,@dptr
      00559F FB               [12] 3855 	mov	r3,a
      0055A0 A3               [24] 3856 	inc	dptr
      0055A1 E0               [24] 3857 	movx	a,@dptr
      0055A2 FC               [12] 3858 	mov	r4,a
      0055A3 EB               [12] 3859 	mov	a,r3
      0055A4 2D               [12] 3860 	add	a,r5
      0055A5 FD               [12] 3861 	mov	r5,a
      0055A6 EC               [12] 3862 	mov	a,r4
      0055A7 3F               [12] 3863 	addc	a,r7
      0055A8 FF               [12] 3864 	mov	r7,a
      0055A9 7C 00            [12] 3865 	mov	r4,#0x00
      0055AB 75 64 7F         [24] 3866 	mov	_memcpy_PARM_2,#_packetFraming_CRC32_1_377
      0055AE 75 65 03         [24] 3867 	mov	(_memcpy_PARM_2 + 1),#(_packetFraming_CRC32_1_377 >> 8)
                                   3868 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      0055B1 8C 66            [24] 3869 	mov	(_memcpy_PARM_2 + 2),r4
      0055B3 75 67 04         [24] 3870 	mov	_memcpy_PARM_3,#0x04
                                   3871 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_3 + 1),#0x00
      0055B6 8C 68            [24] 3872 	mov	(_memcpy_PARM_3 + 1),r4
      0055B8 8D 82            [24] 3873 	mov	dpl,r5
      0055BA 8F 83            [24] 3874 	mov	dph,r7
      0055BC 8C F0            [24] 3875 	mov	b,r4
      0055BE C0 06            [24] 3876 	push	ar6
      0055C0 12 60 C7         [24] 3877 	lcall	_memcpy
      0055C3 D0 06            [24] 3878 	pop	ar6
                           0004C7  3879 	C$pktframing.c$150$1$377 ==.
                                   3880 ;	pktframing.c:150: for (n = 0; n < 3; n++)
      0055C5 0E               [12] 3881 	inc	r6
      0055C6 BE 03 00         [24] 3882 	cjne	r6,#0x03,00127$
      0055C9                       3883 00127$:
      0055C9 50 03            [24] 3884 	jnc	00128$
      0055CB 02 52 B9         [24] 3885 	ljmp	00107$
      0055CE                       3886 00128$:
                           0004D0  3887 	C$pktframing.c$217$1$377 ==.
                           0004D0  3888 	XG$packetFraming$0$0 ==.
      0055CE 22               [24] 3889 	ret
                                   3890 	.area CSEG    (CODE)
                                   3891 	.area CONST   (CODE)
                           000000  3892 Fpktframing$__str_0$0$0 == .
      0077A1                       3893 ___str_0:
      0077A1 52 4E 47 20 64 6F 6E  3894 	.ascii "RNG done: "
             65 3A 20
      0077AB 00                    3895 	.db 0x00
                                   3896 	.area XINIT   (CODE)
                           000000  3897 Fpktframing$__xinit_trmID$0$0 == .
      007D96                       3898 __xinit__trmID:
      007D96 03 00 10 00           3899 	.byte #0x03,#0x00,#0x10,#0x00	; 1048579
                           000004  3900 Fpktframing$__xinit_dummyHMAC$0$0 == .
      007D9A                       3901 __xinit__dummyHMAC:
      007D9A C4 C3 C2 C1           3902 	.byte #0xc4,#0xc3,#0xc2,#0xc1	; 3250766788
                                   3903 	.area CABS    (ABS,CODE)
