                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module ublox_gnss
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_4
                                     12 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_3
                                     13 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_2
                                     14 	.globl _aligned_alloc_PARM_2
                                     15 	.globl _memcpy
                                     16 	.globl _free
                                     17 	.globl _malloc
                                     18 	.globl _uart1_tx
                                     19 	.globl _uart1_rx
                                     20 	.globl _uart1_init
                                     21 	.globl _uart1_rxcount
                                     22 	.globl _uart_timer1_baud
                                     23 	.globl _dbglink_writestr
                                     24 	.globl _delay
                                     25 	.globl _PORTC_7
                                     26 	.globl _PORTC_6
                                     27 	.globl _PORTC_5
                                     28 	.globl _PORTC_4
                                     29 	.globl _PORTC_3
                                     30 	.globl _PORTC_2
                                     31 	.globl _PORTC_1
                                     32 	.globl _PORTC_0
                                     33 	.globl _PORTB_7
                                     34 	.globl _PORTB_6
                                     35 	.globl _PORTB_5
                                     36 	.globl _PORTB_4
                                     37 	.globl _PORTB_3
                                     38 	.globl _PORTB_2
                                     39 	.globl _PORTB_1
                                     40 	.globl _PORTB_0
                                     41 	.globl _PORTA_7
                                     42 	.globl _PORTA_6
                                     43 	.globl _PORTA_5
                                     44 	.globl _PORTA_4
                                     45 	.globl _PORTA_3
                                     46 	.globl _PORTA_2
                                     47 	.globl _PORTA_1
                                     48 	.globl _PORTA_0
                                     49 	.globl _PINC_7
                                     50 	.globl _PINC_6
                                     51 	.globl _PINC_5
                                     52 	.globl _PINC_4
                                     53 	.globl _PINC_3
                                     54 	.globl _PINC_2
                                     55 	.globl _PINC_1
                                     56 	.globl _PINC_0
                                     57 	.globl _PINB_7
                                     58 	.globl _PINB_6
                                     59 	.globl _PINB_5
                                     60 	.globl _PINB_4
                                     61 	.globl _PINB_3
                                     62 	.globl _PINB_2
                                     63 	.globl _PINB_1
                                     64 	.globl _PINB_0
                                     65 	.globl _PINA_7
                                     66 	.globl _PINA_6
                                     67 	.globl _PINA_5
                                     68 	.globl _PINA_4
                                     69 	.globl _PINA_3
                                     70 	.globl _PINA_2
                                     71 	.globl _PINA_1
                                     72 	.globl _PINA_0
                                     73 	.globl _CY
                                     74 	.globl _AC
                                     75 	.globl _F0
                                     76 	.globl _RS1
                                     77 	.globl _RS0
                                     78 	.globl _OV
                                     79 	.globl _F1
                                     80 	.globl _P
                                     81 	.globl _IP_7
                                     82 	.globl _IP_6
                                     83 	.globl _IP_5
                                     84 	.globl _IP_4
                                     85 	.globl _IP_3
                                     86 	.globl _IP_2
                                     87 	.globl _IP_1
                                     88 	.globl _IP_0
                                     89 	.globl _EA
                                     90 	.globl _IE_7
                                     91 	.globl _IE_6
                                     92 	.globl _IE_5
                                     93 	.globl _IE_4
                                     94 	.globl _IE_3
                                     95 	.globl _IE_2
                                     96 	.globl _IE_1
                                     97 	.globl _IE_0
                                     98 	.globl _EIP_7
                                     99 	.globl _EIP_6
                                    100 	.globl _EIP_5
                                    101 	.globl _EIP_4
                                    102 	.globl _EIP_3
                                    103 	.globl _EIP_2
                                    104 	.globl _EIP_1
                                    105 	.globl _EIP_0
                                    106 	.globl _EIE_7
                                    107 	.globl _EIE_6
                                    108 	.globl _EIE_5
                                    109 	.globl _EIE_4
                                    110 	.globl _EIE_3
                                    111 	.globl _EIE_2
                                    112 	.globl _EIE_1
                                    113 	.globl _EIE_0
                                    114 	.globl _E2IP_7
                                    115 	.globl _E2IP_6
                                    116 	.globl _E2IP_5
                                    117 	.globl _E2IP_4
                                    118 	.globl _E2IP_3
                                    119 	.globl _E2IP_2
                                    120 	.globl _E2IP_1
                                    121 	.globl _E2IP_0
                                    122 	.globl _E2IE_7
                                    123 	.globl _E2IE_6
                                    124 	.globl _E2IE_5
                                    125 	.globl _E2IE_4
                                    126 	.globl _E2IE_3
                                    127 	.globl _E2IE_2
                                    128 	.globl _E2IE_1
                                    129 	.globl _E2IE_0
                                    130 	.globl _B_7
                                    131 	.globl _B_6
                                    132 	.globl _B_5
                                    133 	.globl _B_4
                                    134 	.globl _B_3
                                    135 	.globl _B_2
                                    136 	.globl _B_1
                                    137 	.globl _B_0
                                    138 	.globl _ACC_7
                                    139 	.globl _ACC_6
                                    140 	.globl _ACC_5
                                    141 	.globl _ACC_4
                                    142 	.globl _ACC_3
                                    143 	.globl _ACC_2
                                    144 	.globl _ACC_1
                                    145 	.globl _ACC_0
                                    146 	.globl _WTSTAT
                                    147 	.globl _WTIRQEN
                                    148 	.globl _WTEVTD
                                    149 	.globl _WTEVTD1
                                    150 	.globl _WTEVTD0
                                    151 	.globl _WTEVTC
                                    152 	.globl _WTEVTC1
                                    153 	.globl _WTEVTC0
                                    154 	.globl _WTEVTB
                                    155 	.globl _WTEVTB1
                                    156 	.globl _WTEVTB0
                                    157 	.globl _WTEVTA
                                    158 	.globl _WTEVTA1
                                    159 	.globl _WTEVTA0
                                    160 	.globl _WTCNTR1
                                    161 	.globl _WTCNTB
                                    162 	.globl _WTCNTB1
                                    163 	.globl _WTCNTB0
                                    164 	.globl _WTCNTA
                                    165 	.globl _WTCNTA1
                                    166 	.globl _WTCNTA0
                                    167 	.globl _WTCFGB
                                    168 	.globl _WTCFGA
                                    169 	.globl _WDTRESET
                                    170 	.globl _WDTCFG
                                    171 	.globl _U1STATUS
                                    172 	.globl _U1SHREG
                                    173 	.globl _U1MODE
                                    174 	.globl _U1CTRL
                                    175 	.globl _U0STATUS
                                    176 	.globl _U0SHREG
                                    177 	.globl _U0MODE
                                    178 	.globl _U0CTRL
                                    179 	.globl _T2STATUS
                                    180 	.globl _T2PERIOD
                                    181 	.globl _T2PERIOD1
                                    182 	.globl _T2PERIOD0
                                    183 	.globl _T2MODE
                                    184 	.globl _T2CNT
                                    185 	.globl _T2CNT1
                                    186 	.globl _T2CNT0
                                    187 	.globl _T2CLKSRC
                                    188 	.globl _T1STATUS
                                    189 	.globl _T1PERIOD
                                    190 	.globl _T1PERIOD1
                                    191 	.globl _T1PERIOD0
                                    192 	.globl _T1MODE
                                    193 	.globl _T1CNT
                                    194 	.globl _T1CNT1
                                    195 	.globl _T1CNT0
                                    196 	.globl _T1CLKSRC
                                    197 	.globl _T0STATUS
                                    198 	.globl _T0PERIOD
                                    199 	.globl _T0PERIOD1
                                    200 	.globl _T0PERIOD0
                                    201 	.globl _T0MODE
                                    202 	.globl _T0CNT
                                    203 	.globl _T0CNT1
                                    204 	.globl _T0CNT0
                                    205 	.globl _T0CLKSRC
                                    206 	.globl _SPSTATUS
                                    207 	.globl _SPSHREG
                                    208 	.globl _SPMODE
                                    209 	.globl _SPCLKSRC
                                    210 	.globl _RADIOSTAT
                                    211 	.globl _RADIOSTAT1
                                    212 	.globl _RADIOSTAT0
                                    213 	.globl _RADIODATA
                                    214 	.globl _RADIODATA3
                                    215 	.globl _RADIODATA2
                                    216 	.globl _RADIODATA1
                                    217 	.globl _RADIODATA0
                                    218 	.globl _RADIOADDR
                                    219 	.globl _RADIOADDR1
                                    220 	.globl _RADIOADDR0
                                    221 	.globl _RADIOACC
                                    222 	.globl _OC1STATUS
                                    223 	.globl _OC1PIN
                                    224 	.globl _OC1MODE
                                    225 	.globl _OC1COMP
                                    226 	.globl _OC1COMP1
                                    227 	.globl _OC1COMP0
                                    228 	.globl _OC0STATUS
                                    229 	.globl _OC0PIN
                                    230 	.globl _OC0MODE
                                    231 	.globl _OC0COMP
                                    232 	.globl _OC0COMP1
                                    233 	.globl _OC0COMP0
                                    234 	.globl _NVSTATUS
                                    235 	.globl _NVKEY
                                    236 	.globl _NVDATA
                                    237 	.globl _NVDATA1
                                    238 	.globl _NVDATA0
                                    239 	.globl _NVADDR
                                    240 	.globl _NVADDR1
                                    241 	.globl _NVADDR0
                                    242 	.globl _IC1STATUS
                                    243 	.globl _IC1MODE
                                    244 	.globl _IC1CAPT
                                    245 	.globl _IC1CAPT1
                                    246 	.globl _IC1CAPT0
                                    247 	.globl _IC0STATUS
                                    248 	.globl _IC0MODE
                                    249 	.globl _IC0CAPT
                                    250 	.globl _IC0CAPT1
                                    251 	.globl _IC0CAPT0
                                    252 	.globl _PORTR
                                    253 	.globl _PORTC
                                    254 	.globl _PORTB
                                    255 	.globl _PORTA
                                    256 	.globl _PINR
                                    257 	.globl _PINC
                                    258 	.globl _PINB
                                    259 	.globl _PINA
                                    260 	.globl _DIRR
                                    261 	.globl _DIRC
                                    262 	.globl _DIRB
                                    263 	.globl _DIRA
                                    264 	.globl _DBGLNKSTAT
                                    265 	.globl _DBGLNKBUF
                                    266 	.globl _CODECONFIG
                                    267 	.globl _CLKSTAT
                                    268 	.globl _CLKCON
                                    269 	.globl _ANALOGCOMP
                                    270 	.globl _ADCCONV
                                    271 	.globl _ADCCLKSRC
                                    272 	.globl _ADCCH3CONFIG
                                    273 	.globl _ADCCH2CONFIG
                                    274 	.globl _ADCCH1CONFIG
                                    275 	.globl _ADCCH0CONFIG
                                    276 	.globl __XPAGE
                                    277 	.globl _XPAGE
                                    278 	.globl _SP
                                    279 	.globl _PSW
                                    280 	.globl _PCON
                                    281 	.globl _IP
                                    282 	.globl _IE
                                    283 	.globl _EIP
                                    284 	.globl _EIE
                                    285 	.globl _E2IP
                                    286 	.globl _E2IE
                                    287 	.globl _DPS
                                    288 	.globl _DPTR1
                                    289 	.globl _DPTR0
                                    290 	.globl _DPL1
                                    291 	.globl _DPL
                                    292 	.globl _DPH1
                                    293 	.globl _DPH
                                    294 	.globl _B
                                    295 	.globl _ACC
                                    296 	.globl _XTALREADY
                                    297 	.globl _XTALOSC
                                    298 	.globl _XTALAMPL
                                    299 	.globl _SILICONREV
                                    300 	.globl _SCRATCH3
                                    301 	.globl _SCRATCH2
                                    302 	.globl _SCRATCH1
                                    303 	.globl _SCRATCH0
                                    304 	.globl _RADIOMUX
                                    305 	.globl _RADIOFSTATADDR
                                    306 	.globl _RADIOFSTATADDR1
                                    307 	.globl _RADIOFSTATADDR0
                                    308 	.globl _RADIOFDATAADDR
                                    309 	.globl _RADIOFDATAADDR1
                                    310 	.globl _RADIOFDATAADDR0
                                    311 	.globl _OSCRUN
                                    312 	.globl _OSCREADY
                                    313 	.globl _OSCFORCERUN
                                    314 	.globl _OSCCALIB
                                    315 	.globl _MISCCTRL
                                    316 	.globl _LPXOSCGM
                                    317 	.globl _LPOSCREF
                                    318 	.globl _LPOSCREF1
                                    319 	.globl _LPOSCREF0
                                    320 	.globl _LPOSCPER
                                    321 	.globl _LPOSCPER1
                                    322 	.globl _LPOSCPER0
                                    323 	.globl _LPOSCKFILT
                                    324 	.globl _LPOSCKFILT1
                                    325 	.globl _LPOSCKFILT0
                                    326 	.globl _LPOSCFREQ
                                    327 	.globl _LPOSCFREQ1
                                    328 	.globl _LPOSCFREQ0
                                    329 	.globl _LPOSCCONFIG
                                    330 	.globl _PINSEL
                                    331 	.globl _PINCHGC
                                    332 	.globl _PINCHGB
                                    333 	.globl _PINCHGA
                                    334 	.globl _PALTRADIO
                                    335 	.globl _PALTC
                                    336 	.globl _PALTB
                                    337 	.globl _PALTA
                                    338 	.globl _INTCHGC
                                    339 	.globl _INTCHGB
                                    340 	.globl _INTCHGA
                                    341 	.globl _EXTIRQ
                                    342 	.globl _GPIOENABLE
                                    343 	.globl _ANALOGA
                                    344 	.globl _FRCOSCREF
                                    345 	.globl _FRCOSCREF1
                                    346 	.globl _FRCOSCREF0
                                    347 	.globl _FRCOSCPER
                                    348 	.globl _FRCOSCPER1
                                    349 	.globl _FRCOSCPER0
                                    350 	.globl _FRCOSCKFILT
                                    351 	.globl _FRCOSCKFILT1
                                    352 	.globl _FRCOSCKFILT0
                                    353 	.globl _FRCOSCFREQ
                                    354 	.globl _FRCOSCFREQ1
                                    355 	.globl _FRCOSCFREQ0
                                    356 	.globl _FRCOSCCTRL
                                    357 	.globl _FRCOSCCONFIG
                                    358 	.globl _DMA1CONFIG
                                    359 	.globl _DMA1ADDR
                                    360 	.globl _DMA1ADDR1
                                    361 	.globl _DMA1ADDR0
                                    362 	.globl _DMA0CONFIG
                                    363 	.globl _DMA0ADDR
                                    364 	.globl _DMA0ADDR1
                                    365 	.globl _DMA0ADDR0
                                    366 	.globl _ADCTUNE2
                                    367 	.globl _ADCTUNE1
                                    368 	.globl _ADCTUNE0
                                    369 	.globl _ADCCH3VAL
                                    370 	.globl _ADCCH3VAL1
                                    371 	.globl _ADCCH3VAL0
                                    372 	.globl _ADCCH2VAL
                                    373 	.globl _ADCCH2VAL1
                                    374 	.globl _ADCCH2VAL0
                                    375 	.globl _ADCCH1VAL
                                    376 	.globl _ADCCH1VAL1
                                    377 	.globl _ADCCH1VAL0
                                    378 	.globl _ADCCH0VAL
                                    379 	.globl _ADCCH0VAL1
                                    380 	.globl _ADCCH0VAL0
                                    381 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_6
                                    382 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_5
                                    383 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_4
                                    384 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_3
                                    385 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_2
                                    386 	.globl _UBLOX_GPS_FletcherChecksum8
                                    387 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                    388 	.globl _UBLOX_GPS_PortInit
                                    389 ;--------------------------------------------------------
                                    390 ; special function registers
                                    391 ;--------------------------------------------------------
                                    392 	.area RSEG    (ABS,DATA)
      000000                        393 	.org 0x0000
                           0000E0   394 G$ACC$0$0 == 0x00e0
                           0000E0   395 _ACC	=	0x00e0
                           0000F0   396 G$B$0$0 == 0x00f0
                           0000F0   397 _B	=	0x00f0
                           000083   398 G$DPH$0$0 == 0x0083
                           000083   399 _DPH	=	0x0083
                           000085   400 G$DPH1$0$0 == 0x0085
                           000085   401 _DPH1	=	0x0085
                           000082   402 G$DPL$0$0 == 0x0082
                           000082   403 _DPL	=	0x0082
                           000084   404 G$DPL1$0$0 == 0x0084
                           000084   405 _DPL1	=	0x0084
                           008382   406 G$DPTR0$0$0 == 0x8382
                           008382   407 _DPTR0	=	0x8382
                           008584   408 G$DPTR1$0$0 == 0x8584
                           008584   409 _DPTR1	=	0x8584
                           000086   410 G$DPS$0$0 == 0x0086
                           000086   411 _DPS	=	0x0086
                           0000A0   412 G$E2IE$0$0 == 0x00a0
                           0000A0   413 _E2IE	=	0x00a0
                           0000C0   414 G$E2IP$0$0 == 0x00c0
                           0000C0   415 _E2IP	=	0x00c0
                           000098   416 G$EIE$0$0 == 0x0098
                           000098   417 _EIE	=	0x0098
                           0000B0   418 G$EIP$0$0 == 0x00b0
                           0000B0   419 _EIP	=	0x00b0
                           0000A8   420 G$IE$0$0 == 0x00a8
                           0000A8   421 _IE	=	0x00a8
                           0000B8   422 G$IP$0$0 == 0x00b8
                           0000B8   423 _IP	=	0x00b8
                           000087   424 G$PCON$0$0 == 0x0087
                           000087   425 _PCON	=	0x0087
                           0000D0   426 G$PSW$0$0 == 0x00d0
                           0000D0   427 _PSW	=	0x00d0
                           000081   428 G$SP$0$0 == 0x0081
                           000081   429 _SP	=	0x0081
                           0000D9   430 G$XPAGE$0$0 == 0x00d9
                           0000D9   431 _XPAGE	=	0x00d9
                           0000D9   432 G$_XPAGE$0$0 == 0x00d9
                           0000D9   433 __XPAGE	=	0x00d9
                           0000CA   434 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   435 _ADCCH0CONFIG	=	0x00ca
                           0000CB   436 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   437 _ADCCH1CONFIG	=	0x00cb
                           0000D2   438 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   439 _ADCCH2CONFIG	=	0x00d2
                           0000D3   440 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   441 _ADCCH3CONFIG	=	0x00d3
                           0000D1   442 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   443 _ADCCLKSRC	=	0x00d1
                           0000C9   444 G$ADCCONV$0$0 == 0x00c9
                           0000C9   445 _ADCCONV	=	0x00c9
                           0000E1   446 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   447 _ANALOGCOMP	=	0x00e1
                           0000C6   448 G$CLKCON$0$0 == 0x00c6
                           0000C6   449 _CLKCON	=	0x00c6
                           0000C7   450 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   451 _CLKSTAT	=	0x00c7
                           000097   452 G$CODECONFIG$0$0 == 0x0097
                           000097   453 _CODECONFIG	=	0x0097
                           0000E3   454 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   455 _DBGLNKBUF	=	0x00e3
                           0000E2   456 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   457 _DBGLNKSTAT	=	0x00e2
                           000089   458 G$DIRA$0$0 == 0x0089
                           000089   459 _DIRA	=	0x0089
                           00008A   460 G$DIRB$0$0 == 0x008a
                           00008A   461 _DIRB	=	0x008a
                           00008B   462 G$DIRC$0$0 == 0x008b
                           00008B   463 _DIRC	=	0x008b
                           00008E   464 G$DIRR$0$0 == 0x008e
                           00008E   465 _DIRR	=	0x008e
                           0000C8   466 G$PINA$0$0 == 0x00c8
                           0000C8   467 _PINA	=	0x00c8
                           0000E8   468 G$PINB$0$0 == 0x00e8
                           0000E8   469 _PINB	=	0x00e8
                           0000F8   470 G$PINC$0$0 == 0x00f8
                           0000F8   471 _PINC	=	0x00f8
                           00008D   472 G$PINR$0$0 == 0x008d
                           00008D   473 _PINR	=	0x008d
                           000080   474 G$PORTA$0$0 == 0x0080
                           000080   475 _PORTA	=	0x0080
                           000088   476 G$PORTB$0$0 == 0x0088
                           000088   477 _PORTB	=	0x0088
                           000090   478 G$PORTC$0$0 == 0x0090
                           000090   479 _PORTC	=	0x0090
                           00008C   480 G$PORTR$0$0 == 0x008c
                           00008C   481 _PORTR	=	0x008c
                           0000CE   482 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   483 _IC0CAPT0	=	0x00ce
                           0000CF   484 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   485 _IC0CAPT1	=	0x00cf
                           00CFCE   486 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   487 _IC0CAPT	=	0xcfce
                           0000CC   488 G$IC0MODE$0$0 == 0x00cc
                           0000CC   489 _IC0MODE	=	0x00cc
                           0000CD   490 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   491 _IC0STATUS	=	0x00cd
                           0000D6   492 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   493 _IC1CAPT0	=	0x00d6
                           0000D7   494 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   495 _IC1CAPT1	=	0x00d7
                           00D7D6   496 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   497 _IC1CAPT	=	0xd7d6
                           0000D4   498 G$IC1MODE$0$0 == 0x00d4
                           0000D4   499 _IC1MODE	=	0x00d4
                           0000D5   500 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   501 _IC1STATUS	=	0x00d5
                           000092   502 G$NVADDR0$0$0 == 0x0092
                           000092   503 _NVADDR0	=	0x0092
                           000093   504 G$NVADDR1$0$0 == 0x0093
                           000093   505 _NVADDR1	=	0x0093
                           009392   506 G$NVADDR$0$0 == 0x9392
                           009392   507 _NVADDR	=	0x9392
                           000094   508 G$NVDATA0$0$0 == 0x0094
                           000094   509 _NVDATA0	=	0x0094
                           000095   510 G$NVDATA1$0$0 == 0x0095
                           000095   511 _NVDATA1	=	0x0095
                           009594   512 G$NVDATA$0$0 == 0x9594
                           009594   513 _NVDATA	=	0x9594
                           000096   514 G$NVKEY$0$0 == 0x0096
                           000096   515 _NVKEY	=	0x0096
                           000091   516 G$NVSTATUS$0$0 == 0x0091
                           000091   517 _NVSTATUS	=	0x0091
                           0000BC   518 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   519 _OC0COMP0	=	0x00bc
                           0000BD   520 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   521 _OC0COMP1	=	0x00bd
                           00BDBC   522 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   523 _OC0COMP	=	0xbdbc
                           0000B9   524 G$OC0MODE$0$0 == 0x00b9
                           0000B9   525 _OC0MODE	=	0x00b9
                           0000BA   526 G$OC0PIN$0$0 == 0x00ba
                           0000BA   527 _OC0PIN	=	0x00ba
                           0000BB   528 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   529 _OC0STATUS	=	0x00bb
                           0000C4   530 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   531 _OC1COMP0	=	0x00c4
                           0000C5   532 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   533 _OC1COMP1	=	0x00c5
                           00C5C4   534 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   535 _OC1COMP	=	0xc5c4
                           0000C1   536 G$OC1MODE$0$0 == 0x00c1
                           0000C1   537 _OC1MODE	=	0x00c1
                           0000C2   538 G$OC1PIN$0$0 == 0x00c2
                           0000C2   539 _OC1PIN	=	0x00c2
                           0000C3   540 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   541 _OC1STATUS	=	0x00c3
                           0000B1   542 G$RADIOACC$0$0 == 0x00b1
                           0000B1   543 _RADIOACC	=	0x00b1
                           0000B3   544 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   545 _RADIOADDR0	=	0x00b3
                           0000B2   546 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   547 _RADIOADDR1	=	0x00b2
                           00B2B3   548 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   549 _RADIOADDR	=	0xb2b3
                           0000B7   550 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   551 _RADIODATA0	=	0x00b7
                           0000B6   552 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   553 _RADIODATA1	=	0x00b6
                           0000B5   554 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   555 _RADIODATA2	=	0x00b5
                           0000B4   556 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   557 _RADIODATA3	=	0x00b4
                           B4B5B6B7   558 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   559 _RADIODATA	=	0xb4b5b6b7
                           0000BE   560 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   561 _RADIOSTAT0	=	0x00be
                           0000BF   562 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   563 _RADIOSTAT1	=	0x00bf
                           00BFBE   564 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   565 _RADIOSTAT	=	0xbfbe
                           0000DF   566 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   567 _SPCLKSRC	=	0x00df
                           0000DC   568 G$SPMODE$0$0 == 0x00dc
                           0000DC   569 _SPMODE	=	0x00dc
                           0000DE   570 G$SPSHREG$0$0 == 0x00de
                           0000DE   571 _SPSHREG	=	0x00de
                           0000DD   572 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   573 _SPSTATUS	=	0x00dd
                           00009A   574 G$T0CLKSRC$0$0 == 0x009a
                           00009A   575 _T0CLKSRC	=	0x009a
                           00009C   576 G$T0CNT0$0$0 == 0x009c
                           00009C   577 _T0CNT0	=	0x009c
                           00009D   578 G$T0CNT1$0$0 == 0x009d
                           00009D   579 _T0CNT1	=	0x009d
                           009D9C   580 G$T0CNT$0$0 == 0x9d9c
                           009D9C   581 _T0CNT	=	0x9d9c
                           000099   582 G$T0MODE$0$0 == 0x0099
                           000099   583 _T0MODE	=	0x0099
                           00009E   584 G$T0PERIOD0$0$0 == 0x009e
                           00009E   585 _T0PERIOD0	=	0x009e
                           00009F   586 G$T0PERIOD1$0$0 == 0x009f
                           00009F   587 _T0PERIOD1	=	0x009f
                           009F9E   588 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   589 _T0PERIOD	=	0x9f9e
                           00009B   590 G$T0STATUS$0$0 == 0x009b
                           00009B   591 _T0STATUS	=	0x009b
                           0000A2   592 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   593 _T1CLKSRC	=	0x00a2
                           0000A4   594 G$T1CNT0$0$0 == 0x00a4
                           0000A4   595 _T1CNT0	=	0x00a4
                           0000A5   596 G$T1CNT1$0$0 == 0x00a5
                           0000A5   597 _T1CNT1	=	0x00a5
                           00A5A4   598 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   599 _T1CNT	=	0xa5a4
                           0000A1   600 G$T1MODE$0$0 == 0x00a1
                           0000A1   601 _T1MODE	=	0x00a1
                           0000A6   602 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   603 _T1PERIOD0	=	0x00a6
                           0000A7   604 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   605 _T1PERIOD1	=	0x00a7
                           00A7A6   606 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   607 _T1PERIOD	=	0xa7a6
                           0000A3   608 G$T1STATUS$0$0 == 0x00a3
                           0000A3   609 _T1STATUS	=	0x00a3
                           0000AA   610 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   611 _T2CLKSRC	=	0x00aa
                           0000AC   612 G$T2CNT0$0$0 == 0x00ac
                           0000AC   613 _T2CNT0	=	0x00ac
                           0000AD   614 G$T2CNT1$0$0 == 0x00ad
                           0000AD   615 _T2CNT1	=	0x00ad
                           00ADAC   616 G$T2CNT$0$0 == 0xadac
                           00ADAC   617 _T2CNT	=	0xadac
                           0000A9   618 G$T2MODE$0$0 == 0x00a9
                           0000A9   619 _T2MODE	=	0x00a9
                           0000AE   620 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   621 _T2PERIOD0	=	0x00ae
                           0000AF   622 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   623 _T2PERIOD1	=	0x00af
                           00AFAE   624 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   625 _T2PERIOD	=	0xafae
                           0000AB   626 G$T2STATUS$0$0 == 0x00ab
                           0000AB   627 _T2STATUS	=	0x00ab
                           0000E4   628 G$U0CTRL$0$0 == 0x00e4
                           0000E4   629 _U0CTRL	=	0x00e4
                           0000E7   630 G$U0MODE$0$0 == 0x00e7
                           0000E7   631 _U0MODE	=	0x00e7
                           0000E6   632 G$U0SHREG$0$0 == 0x00e6
                           0000E6   633 _U0SHREG	=	0x00e6
                           0000E5   634 G$U0STATUS$0$0 == 0x00e5
                           0000E5   635 _U0STATUS	=	0x00e5
                           0000EC   636 G$U1CTRL$0$0 == 0x00ec
                           0000EC   637 _U1CTRL	=	0x00ec
                           0000EF   638 G$U1MODE$0$0 == 0x00ef
                           0000EF   639 _U1MODE	=	0x00ef
                           0000EE   640 G$U1SHREG$0$0 == 0x00ee
                           0000EE   641 _U1SHREG	=	0x00ee
                           0000ED   642 G$U1STATUS$0$0 == 0x00ed
                           0000ED   643 _U1STATUS	=	0x00ed
                           0000DA   644 G$WDTCFG$0$0 == 0x00da
                           0000DA   645 _WDTCFG	=	0x00da
                           0000DB   646 G$WDTRESET$0$0 == 0x00db
                           0000DB   647 _WDTRESET	=	0x00db
                           0000F1   648 G$WTCFGA$0$0 == 0x00f1
                           0000F1   649 _WTCFGA	=	0x00f1
                           0000F9   650 G$WTCFGB$0$0 == 0x00f9
                           0000F9   651 _WTCFGB	=	0x00f9
                           0000F2   652 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   653 _WTCNTA0	=	0x00f2
                           0000F3   654 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   655 _WTCNTA1	=	0x00f3
                           00F3F2   656 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   657 _WTCNTA	=	0xf3f2
                           0000FA   658 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   659 _WTCNTB0	=	0x00fa
                           0000FB   660 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   661 _WTCNTB1	=	0x00fb
                           00FBFA   662 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   663 _WTCNTB	=	0xfbfa
                           0000EB   664 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   665 _WTCNTR1	=	0x00eb
                           0000F4   666 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   667 _WTEVTA0	=	0x00f4
                           0000F5   668 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   669 _WTEVTA1	=	0x00f5
                           00F5F4   670 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   671 _WTEVTA	=	0xf5f4
                           0000F6   672 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   673 _WTEVTB0	=	0x00f6
                           0000F7   674 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   675 _WTEVTB1	=	0x00f7
                           00F7F6   676 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   677 _WTEVTB	=	0xf7f6
                           0000FC   678 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   679 _WTEVTC0	=	0x00fc
                           0000FD   680 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   681 _WTEVTC1	=	0x00fd
                           00FDFC   682 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   683 _WTEVTC	=	0xfdfc
                           0000FE   684 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   685 _WTEVTD0	=	0x00fe
                           0000FF   686 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   687 _WTEVTD1	=	0x00ff
                           00FFFE   688 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   689 _WTEVTD	=	0xfffe
                           0000E9   690 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   691 _WTIRQEN	=	0x00e9
                           0000EA   692 G$WTSTAT$0$0 == 0x00ea
                           0000EA   693 _WTSTAT	=	0x00ea
                                    694 ;--------------------------------------------------------
                                    695 ; special function bits
                                    696 ;--------------------------------------------------------
                                    697 	.area RSEG    (ABS,DATA)
      000000                        698 	.org 0x0000
                           0000E0   699 G$ACC_0$0$0 == 0x00e0
                           0000E0   700 _ACC_0	=	0x00e0
                           0000E1   701 G$ACC_1$0$0 == 0x00e1
                           0000E1   702 _ACC_1	=	0x00e1
                           0000E2   703 G$ACC_2$0$0 == 0x00e2
                           0000E2   704 _ACC_2	=	0x00e2
                           0000E3   705 G$ACC_3$0$0 == 0x00e3
                           0000E3   706 _ACC_3	=	0x00e3
                           0000E4   707 G$ACC_4$0$0 == 0x00e4
                           0000E4   708 _ACC_4	=	0x00e4
                           0000E5   709 G$ACC_5$0$0 == 0x00e5
                           0000E5   710 _ACC_5	=	0x00e5
                           0000E6   711 G$ACC_6$0$0 == 0x00e6
                           0000E6   712 _ACC_6	=	0x00e6
                           0000E7   713 G$ACC_7$0$0 == 0x00e7
                           0000E7   714 _ACC_7	=	0x00e7
                           0000F0   715 G$B_0$0$0 == 0x00f0
                           0000F0   716 _B_0	=	0x00f0
                           0000F1   717 G$B_1$0$0 == 0x00f1
                           0000F1   718 _B_1	=	0x00f1
                           0000F2   719 G$B_2$0$0 == 0x00f2
                           0000F2   720 _B_2	=	0x00f2
                           0000F3   721 G$B_3$0$0 == 0x00f3
                           0000F3   722 _B_3	=	0x00f3
                           0000F4   723 G$B_4$0$0 == 0x00f4
                           0000F4   724 _B_4	=	0x00f4
                           0000F5   725 G$B_5$0$0 == 0x00f5
                           0000F5   726 _B_5	=	0x00f5
                           0000F6   727 G$B_6$0$0 == 0x00f6
                           0000F6   728 _B_6	=	0x00f6
                           0000F7   729 G$B_7$0$0 == 0x00f7
                           0000F7   730 _B_7	=	0x00f7
                           0000A0   731 G$E2IE_0$0$0 == 0x00a0
                           0000A0   732 _E2IE_0	=	0x00a0
                           0000A1   733 G$E2IE_1$0$0 == 0x00a1
                           0000A1   734 _E2IE_1	=	0x00a1
                           0000A2   735 G$E2IE_2$0$0 == 0x00a2
                           0000A2   736 _E2IE_2	=	0x00a2
                           0000A3   737 G$E2IE_3$0$0 == 0x00a3
                           0000A3   738 _E2IE_3	=	0x00a3
                           0000A4   739 G$E2IE_4$0$0 == 0x00a4
                           0000A4   740 _E2IE_4	=	0x00a4
                           0000A5   741 G$E2IE_5$0$0 == 0x00a5
                           0000A5   742 _E2IE_5	=	0x00a5
                           0000A6   743 G$E2IE_6$0$0 == 0x00a6
                           0000A6   744 _E2IE_6	=	0x00a6
                           0000A7   745 G$E2IE_7$0$0 == 0x00a7
                           0000A7   746 _E2IE_7	=	0x00a7
                           0000C0   747 G$E2IP_0$0$0 == 0x00c0
                           0000C0   748 _E2IP_0	=	0x00c0
                           0000C1   749 G$E2IP_1$0$0 == 0x00c1
                           0000C1   750 _E2IP_1	=	0x00c1
                           0000C2   751 G$E2IP_2$0$0 == 0x00c2
                           0000C2   752 _E2IP_2	=	0x00c2
                           0000C3   753 G$E2IP_3$0$0 == 0x00c3
                           0000C3   754 _E2IP_3	=	0x00c3
                           0000C4   755 G$E2IP_4$0$0 == 0x00c4
                           0000C4   756 _E2IP_4	=	0x00c4
                           0000C5   757 G$E2IP_5$0$0 == 0x00c5
                           0000C5   758 _E2IP_5	=	0x00c5
                           0000C6   759 G$E2IP_6$0$0 == 0x00c6
                           0000C6   760 _E2IP_6	=	0x00c6
                           0000C7   761 G$E2IP_7$0$0 == 0x00c7
                           0000C7   762 _E2IP_7	=	0x00c7
                           000098   763 G$EIE_0$0$0 == 0x0098
                           000098   764 _EIE_0	=	0x0098
                           000099   765 G$EIE_1$0$0 == 0x0099
                           000099   766 _EIE_1	=	0x0099
                           00009A   767 G$EIE_2$0$0 == 0x009a
                           00009A   768 _EIE_2	=	0x009a
                           00009B   769 G$EIE_3$0$0 == 0x009b
                           00009B   770 _EIE_3	=	0x009b
                           00009C   771 G$EIE_4$0$0 == 0x009c
                           00009C   772 _EIE_4	=	0x009c
                           00009D   773 G$EIE_5$0$0 == 0x009d
                           00009D   774 _EIE_5	=	0x009d
                           00009E   775 G$EIE_6$0$0 == 0x009e
                           00009E   776 _EIE_6	=	0x009e
                           00009F   777 G$EIE_7$0$0 == 0x009f
                           00009F   778 _EIE_7	=	0x009f
                           0000B0   779 G$EIP_0$0$0 == 0x00b0
                           0000B0   780 _EIP_0	=	0x00b0
                           0000B1   781 G$EIP_1$0$0 == 0x00b1
                           0000B1   782 _EIP_1	=	0x00b1
                           0000B2   783 G$EIP_2$0$0 == 0x00b2
                           0000B2   784 _EIP_2	=	0x00b2
                           0000B3   785 G$EIP_3$0$0 == 0x00b3
                           0000B3   786 _EIP_3	=	0x00b3
                           0000B4   787 G$EIP_4$0$0 == 0x00b4
                           0000B4   788 _EIP_4	=	0x00b4
                           0000B5   789 G$EIP_5$0$0 == 0x00b5
                           0000B5   790 _EIP_5	=	0x00b5
                           0000B6   791 G$EIP_6$0$0 == 0x00b6
                           0000B6   792 _EIP_6	=	0x00b6
                           0000B7   793 G$EIP_7$0$0 == 0x00b7
                           0000B7   794 _EIP_7	=	0x00b7
                           0000A8   795 G$IE_0$0$0 == 0x00a8
                           0000A8   796 _IE_0	=	0x00a8
                           0000A9   797 G$IE_1$0$0 == 0x00a9
                           0000A9   798 _IE_1	=	0x00a9
                           0000AA   799 G$IE_2$0$0 == 0x00aa
                           0000AA   800 _IE_2	=	0x00aa
                           0000AB   801 G$IE_3$0$0 == 0x00ab
                           0000AB   802 _IE_3	=	0x00ab
                           0000AC   803 G$IE_4$0$0 == 0x00ac
                           0000AC   804 _IE_4	=	0x00ac
                           0000AD   805 G$IE_5$0$0 == 0x00ad
                           0000AD   806 _IE_5	=	0x00ad
                           0000AE   807 G$IE_6$0$0 == 0x00ae
                           0000AE   808 _IE_6	=	0x00ae
                           0000AF   809 G$IE_7$0$0 == 0x00af
                           0000AF   810 _IE_7	=	0x00af
                           0000AF   811 G$EA$0$0 == 0x00af
                           0000AF   812 _EA	=	0x00af
                           0000B8   813 G$IP_0$0$0 == 0x00b8
                           0000B8   814 _IP_0	=	0x00b8
                           0000B9   815 G$IP_1$0$0 == 0x00b9
                           0000B9   816 _IP_1	=	0x00b9
                           0000BA   817 G$IP_2$0$0 == 0x00ba
                           0000BA   818 _IP_2	=	0x00ba
                           0000BB   819 G$IP_3$0$0 == 0x00bb
                           0000BB   820 _IP_3	=	0x00bb
                           0000BC   821 G$IP_4$0$0 == 0x00bc
                           0000BC   822 _IP_4	=	0x00bc
                           0000BD   823 G$IP_5$0$0 == 0x00bd
                           0000BD   824 _IP_5	=	0x00bd
                           0000BE   825 G$IP_6$0$0 == 0x00be
                           0000BE   826 _IP_6	=	0x00be
                           0000BF   827 G$IP_7$0$0 == 0x00bf
                           0000BF   828 _IP_7	=	0x00bf
                           0000D0   829 G$P$0$0 == 0x00d0
                           0000D0   830 _P	=	0x00d0
                           0000D1   831 G$F1$0$0 == 0x00d1
                           0000D1   832 _F1	=	0x00d1
                           0000D2   833 G$OV$0$0 == 0x00d2
                           0000D2   834 _OV	=	0x00d2
                           0000D3   835 G$RS0$0$0 == 0x00d3
                           0000D3   836 _RS0	=	0x00d3
                           0000D4   837 G$RS1$0$0 == 0x00d4
                           0000D4   838 _RS1	=	0x00d4
                           0000D5   839 G$F0$0$0 == 0x00d5
                           0000D5   840 _F0	=	0x00d5
                           0000D6   841 G$AC$0$0 == 0x00d6
                           0000D6   842 _AC	=	0x00d6
                           0000D7   843 G$CY$0$0 == 0x00d7
                           0000D7   844 _CY	=	0x00d7
                           0000C8   845 G$PINA_0$0$0 == 0x00c8
                           0000C8   846 _PINA_0	=	0x00c8
                           0000C9   847 G$PINA_1$0$0 == 0x00c9
                           0000C9   848 _PINA_1	=	0x00c9
                           0000CA   849 G$PINA_2$0$0 == 0x00ca
                           0000CA   850 _PINA_2	=	0x00ca
                           0000CB   851 G$PINA_3$0$0 == 0x00cb
                           0000CB   852 _PINA_3	=	0x00cb
                           0000CC   853 G$PINA_4$0$0 == 0x00cc
                           0000CC   854 _PINA_4	=	0x00cc
                           0000CD   855 G$PINA_5$0$0 == 0x00cd
                           0000CD   856 _PINA_5	=	0x00cd
                           0000CE   857 G$PINA_6$0$0 == 0x00ce
                           0000CE   858 _PINA_6	=	0x00ce
                           0000CF   859 G$PINA_7$0$0 == 0x00cf
                           0000CF   860 _PINA_7	=	0x00cf
                           0000E8   861 G$PINB_0$0$0 == 0x00e8
                           0000E8   862 _PINB_0	=	0x00e8
                           0000E9   863 G$PINB_1$0$0 == 0x00e9
                           0000E9   864 _PINB_1	=	0x00e9
                           0000EA   865 G$PINB_2$0$0 == 0x00ea
                           0000EA   866 _PINB_2	=	0x00ea
                           0000EB   867 G$PINB_3$0$0 == 0x00eb
                           0000EB   868 _PINB_3	=	0x00eb
                           0000EC   869 G$PINB_4$0$0 == 0x00ec
                           0000EC   870 _PINB_4	=	0x00ec
                           0000ED   871 G$PINB_5$0$0 == 0x00ed
                           0000ED   872 _PINB_5	=	0x00ed
                           0000EE   873 G$PINB_6$0$0 == 0x00ee
                           0000EE   874 _PINB_6	=	0x00ee
                           0000EF   875 G$PINB_7$0$0 == 0x00ef
                           0000EF   876 _PINB_7	=	0x00ef
                           0000F8   877 G$PINC_0$0$0 == 0x00f8
                           0000F8   878 _PINC_0	=	0x00f8
                           0000F9   879 G$PINC_1$0$0 == 0x00f9
                           0000F9   880 _PINC_1	=	0x00f9
                           0000FA   881 G$PINC_2$0$0 == 0x00fa
                           0000FA   882 _PINC_2	=	0x00fa
                           0000FB   883 G$PINC_3$0$0 == 0x00fb
                           0000FB   884 _PINC_3	=	0x00fb
                           0000FC   885 G$PINC_4$0$0 == 0x00fc
                           0000FC   886 _PINC_4	=	0x00fc
                           0000FD   887 G$PINC_5$0$0 == 0x00fd
                           0000FD   888 _PINC_5	=	0x00fd
                           0000FE   889 G$PINC_6$0$0 == 0x00fe
                           0000FE   890 _PINC_6	=	0x00fe
                           0000FF   891 G$PINC_7$0$0 == 0x00ff
                           0000FF   892 _PINC_7	=	0x00ff
                           000080   893 G$PORTA_0$0$0 == 0x0080
                           000080   894 _PORTA_0	=	0x0080
                           000081   895 G$PORTA_1$0$0 == 0x0081
                           000081   896 _PORTA_1	=	0x0081
                           000082   897 G$PORTA_2$0$0 == 0x0082
                           000082   898 _PORTA_2	=	0x0082
                           000083   899 G$PORTA_3$0$0 == 0x0083
                           000083   900 _PORTA_3	=	0x0083
                           000084   901 G$PORTA_4$0$0 == 0x0084
                           000084   902 _PORTA_4	=	0x0084
                           000085   903 G$PORTA_5$0$0 == 0x0085
                           000085   904 _PORTA_5	=	0x0085
                           000086   905 G$PORTA_6$0$0 == 0x0086
                           000086   906 _PORTA_6	=	0x0086
                           000087   907 G$PORTA_7$0$0 == 0x0087
                           000087   908 _PORTA_7	=	0x0087
                           000088   909 G$PORTB_0$0$0 == 0x0088
                           000088   910 _PORTB_0	=	0x0088
                           000089   911 G$PORTB_1$0$0 == 0x0089
                           000089   912 _PORTB_1	=	0x0089
                           00008A   913 G$PORTB_2$0$0 == 0x008a
                           00008A   914 _PORTB_2	=	0x008a
                           00008B   915 G$PORTB_3$0$0 == 0x008b
                           00008B   916 _PORTB_3	=	0x008b
                           00008C   917 G$PORTB_4$0$0 == 0x008c
                           00008C   918 _PORTB_4	=	0x008c
                           00008D   919 G$PORTB_5$0$0 == 0x008d
                           00008D   920 _PORTB_5	=	0x008d
                           00008E   921 G$PORTB_6$0$0 == 0x008e
                           00008E   922 _PORTB_6	=	0x008e
                           00008F   923 G$PORTB_7$0$0 == 0x008f
                           00008F   924 _PORTB_7	=	0x008f
                           000090   925 G$PORTC_0$0$0 == 0x0090
                           000090   926 _PORTC_0	=	0x0090
                           000091   927 G$PORTC_1$0$0 == 0x0091
                           000091   928 _PORTC_1	=	0x0091
                           000092   929 G$PORTC_2$0$0 == 0x0092
                           000092   930 _PORTC_2	=	0x0092
                           000093   931 G$PORTC_3$0$0 == 0x0093
                           000093   932 _PORTC_3	=	0x0093
                           000094   933 G$PORTC_4$0$0 == 0x0094
                           000094   934 _PORTC_4	=	0x0094
                           000095   935 G$PORTC_5$0$0 == 0x0095
                           000095   936 _PORTC_5	=	0x0095
                           000096   937 G$PORTC_6$0$0 == 0x0096
                           000096   938 _PORTC_6	=	0x0096
                           000097   939 G$PORTC_7$0$0 == 0x0097
                           000097   940 _PORTC_7	=	0x0097
                                    941 ;--------------------------------------------------------
                                    942 ; overlayable register banks
                                    943 ;--------------------------------------------------------
                                    944 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        945 	.ds 8
                                    946 ;--------------------------------------------------------
                                    947 ; internal ram data
                                    948 ;--------------------------------------------------------
                                    949 	.area DSEG    (DATA)
                           000000   950 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_id$1$184==.
      000026                        951 _UBLOX_GPS_SendCommand_WaitACK_PARM_2:
      000026                        952 	.ds 1
                           000001   953 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_length$1$184==.
      000027                        954 _UBLOX_GPS_SendCommand_WaitACK_PARM_3:
      000027                        955 	.ds 2
                           000003   956 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$payload$1$184==.
      000029                        957 _UBLOX_GPS_SendCommand_WaitACK_PARM_4:
      000029                        958 	.ds 3
                           000006   959 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$AnsOrAck$1$184==.
      00002C                        960 _UBLOX_GPS_SendCommand_WaitACK_PARM_5:
      00002C                        961 	.ds 1
                           000007   962 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$RxLength$1$184==.
      00002D                        963 _UBLOX_GPS_SendCommand_WaitACK_PARM_6:
      00002D                        964 	.ds 3
                           00000A   965 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_class$1$184==.
      000030                        966 _UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184:
      000030                        967 	.ds 1
                           00000B   968 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$k$1$185==.
      000031                        969 _UBLOX_GPS_SendCommand_WaitACK_k_1_185:
      000031                        970 	.ds 1
                           00000C   971 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$RetVal$1$185==.
      000032                        972 _UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185:
      000032                        973 	.ds 1
                           00000D   974 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$buffer_aux$1$185==.
      000033                        975 _UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185:
      000033                        976 	.ds 2
                           00000F   977 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$rx_buffer$1$185==.
      000035                        978 _UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185:
      000035                        979 	.ds 2
                           000011   980 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$CK_A$1$185==.
      000037                        981 _UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185:
      000037                        982 	.ds 1
                           000012   983 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$CK_B$1$185==.
      000038                        984 _UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185:
      000038                        985 	.ds 1
                           000013   986 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$sloc0$1$0==.
      000039                        987 _UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0:
      000039                        988 	.ds 2
                           000015   989 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$sloc1$1$0==.
      00003B                        990 _UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0:
      00003B                        991 	.ds 3
                                    992 ;--------------------------------------------------------
                                    993 ; overlayable items in internal ram 
                                    994 ;--------------------------------------------------------
                                    995 	.area	OSEG    (OVR,DATA)
                           000000   996 Lublox_gnss.aligned_alloc$size$1$150==.
      000064                        997 _aligned_alloc_PARM_2:
      000064                        998 	.ds 2
                           000002   999 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$CK_A$1$181==.
      000066                       1000 _UBLOX_GPS_FletcherChecksum8_PARM_2:
      000066                       1001 	.ds 3
                           000005  1002 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$CK_B$1$181==.
      000069                       1003 _UBLOX_GPS_FletcherChecksum8_PARM_3:
      000069                       1004 	.ds 3
                           000008  1005 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$length$1$181==.
      00006C                       1006 _UBLOX_GPS_FletcherChecksum8_PARM_4:
      00006C                       1007 	.ds 2
                           00000A  1008 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$buffer$1$181==.
      00006E                       1009 _UBLOX_GPS_FletcherChecksum8_buffer_1_181:
      00006E                       1010 	.ds 3
                           00000D  1011 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$sloc0$1$0==.
      000071                       1012 _UBLOX_GPS_FletcherChecksum8_sloc0_1_0:
      000071                       1013 	.ds 3
                                   1014 ;--------------------------------------------------------
                                   1015 ; indirectly addressable internal ram data
                                   1016 ;--------------------------------------------------------
                                   1017 	.area ISEG    (DATA)
                                   1018 ;--------------------------------------------------------
                                   1019 ; absolute internal ram data
                                   1020 ;--------------------------------------------------------
                                   1021 	.area IABS    (ABS,DATA)
                                   1022 	.area IABS    (ABS,DATA)
                                   1023 ;--------------------------------------------------------
                                   1024 ; bit data
                                   1025 ;--------------------------------------------------------
                                   1026 	.area BSEG    (BIT)
                                   1027 ;--------------------------------------------------------
                                   1028 ; paged external ram data
                                   1029 ;--------------------------------------------------------
                                   1030 	.area PSEG    (PAG,XDATA)
                                   1031 ;--------------------------------------------------------
                                   1032 ; external ram data
                                   1033 ;--------------------------------------------------------
                                   1034 	.area XSEG    (XDATA)
                           007020  1035 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1036 _ADCCH0VAL0	=	0x7020
                           007021  1037 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1038 _ADCCH0VAL1	=	0x7021
                           007020  1039 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1040 _ADCCH0VAL	=	0x7020
                           007022  1041 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1042 _ADCCH1VAL0	=	0x7022
                           007023  1043 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1044 _ADCCH1VAL1	=	0x7023
                           007022  1045 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1046 _ADCCH1VAL	=	0x7022
                           007024  1047 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1048 _ADCCH2VAL0	=	0x7024
                           007025  1049 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1050 _ADCCH2VAL1	=	0x7025
                           007024  1051 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1052 _ADCCH2VAL	=	0x7024
                           007026  1053 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1054 _ADCCH3VAL0	=	0x7026
                           007027  1055 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1056 _ADCCH3VAL1	=	0x7027
                           007026  1057 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1058 _ADCCH3VAL	=	0x7026
                           007028  1059 G$ADCTUNE0$0$0 == 0x7028
                           007028  1060 _ADCTUNE0	=	0x7028
                           007029  1061 G$ADCTUNE1$0$0 == 0x7029
                           007029  1062 _ADCTUNE1	=	0x7029
                           00702A  1063 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1064 _ADCTUNE2	=	0x702a
                           007010  1065 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1066 _DMA0ADDR0	=	0x7010
                           007011  1067 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1068 _DMA0ADDR1	=	0x7011
                           007010  1069 G$DMA0ADDR$0$0 == 0x7010
                           007010  1070 _DMA0ADDR	=	0x7010
                           007014  1071 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1072 _DMA0CONFIG	=	0x7014
                           007012  1073 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1074 _DMA1ADDR0	=	0x7012
                           007013  1075 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1076 _DMA1ADDR1	=	0x7013
                           007012  1077 G$DMA1ADDR$0$0 == 0x7012
                           007012  1078 _DMA1ADDR	=	0x7012
                           007015  1079 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1080 _DMA1CONFIG	=	0x7015
                           007070  1081 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1082 _FRCOSCCONFIG	=	0x7070
                           007071  1083 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1084 _FRCOSCCTRL	=	0x7071
                           007076  1085 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1086 _FRCOSCFREQ0	=	0x7076
                           007077  1087 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1088 _FRCOSCFREQ1	=	0x7077
                           007076  1089 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1090 _FRCOSCFREQ	=	0x7076
                           007072  1091 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1092 _FRCOSCKFILT0	=	0x7072
                           007073  1093 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1094 _FRCOSCKFILT1	=	0x7073
                           007072  1095 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1096 _FRCOSCKFILT	=	0x7072
                           007078  1097 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1098 _FRCOSCPER0	=	0x7078
                           007079  1099 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1100 _FRCOSCPER1	=	0x7079
                           007078  1101 G$FRCOSCPER$0$0 == 0x7078
                           007078  1102 _FRCOSCPER	=	0x7078
                           007074  1103 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1104 _FRCOSCREF0	=	0x7074
                           007075  1105 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1106 _FRCOSCREF1	=	0x7075
                           007074  1107 G$FRCOSCREF$0$0 == 0x7074
                           007074  1108 _FRCOSCREF	=	0x7074
                           007007  1109 G$ANALOGA$0$0 == 0x7007
                           007007  1110 _ANALOGA	=	0x7007
                           00700C  1111 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1112 _GPIOENABLE	=	0x700c
                           007003  1113 G$EXTIRQ$0$0 == 0x7003
                           007003  1114 _EXTIRQ	=	0x7003
                           007000  1115 G$INTCHGA$0$0 == 0x7000
                           007000  1116 _INTCHGA	=	0x7000
                           007001  1117 G$INTCHGB$0$0 == 0x7001
                           007001  1118 _INTCHGB	=	0x7001
                           007002  1119 G$INTCHGC$0$0 == 0x7002
                           007002  1120 _INTCHGC	=	0x7002
                           007008  1121 G$PALTA$0$0 == 0x7008
                           007008  1122 _PALTA	=	0x7008
                           007009  1123 G$PALTB$0$0 == 0x7009
                           007009  1124 _PALTB	=	0x7009
                           00700A  1125 G$PALTC$0$0 == 0x700a
                           00700A  1126 _PALTC	=	0x700a
                           007046  1127 G$PALTRADIO$0$0 == 0x7046
                           007046  1128 _PALTRADIO	=	0x7046
                           007004  1129 G$PINCHGA$0$0 == 0x7004
                           007004  1130 _PINCHGA	=	0x7004
                           007005  1131 G$PINCHGB$0$0 == 0x7005
                           007005  1132 _PINCHGB	=	0x7005
                           007006  1133 G$PINCHGC$0$0 == 0x7006
                           007006  1134 _PINCHGC	=	0x7006
                           00700B  1135 G$PINSEL$0$0 == 0x700b
                           00700B  1136 _PINSEL	=	0x700b
                           007060  1137 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1138 _LPOSCCONFIG	=	0x7060
                           007066  1139 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1140 _LPOSCFREQ0	=	0x7066
                           007067  1141 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1142 _LPOSCFREQ1	=	0x7067
                           007066  1143 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1144 _LPOSCFREQ	=	0x7066
                           007062  1145 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1146 _LPOSCKFILT0	=	0x7062
                           007063  1147 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1148 _LPOSCKFILT1	=	0x7063
                           007062  1149 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1150 _LPOSCKFILT	=	0x7062
                           007068  1151 G$LPOSCPER0$0$0 == 0x7068
                           007068  1152 _LPOSCPER0	=	0x7068
                           007069  1153 G$LPOSCPER1$0$0 == 0x7069
                           007069  1154 _LPOSCPER1	=	0x7069
                           007068  1155 G$LPOSCPER$0$0 == 0x7068
                           007068  1156 _LPOSCPER	=	0x7068
                           007064  1157 G$LPOSCREF0$0$0 == 0x7064
                           007064  1158 _LPOSCREF0	=	0x7064
                           007065  1159 G$LPOSCREF1$0$0 == 0x7065
                           007065  1160 _LPOSCREF1	=	0x7065
                           007064  1161 G$LPOSCREF$0$0 == 0x7064
                           007064  1162 _LPOSCREF	=	0x7064
                           007054  1163 G$LPXOSCGM$0$0 == 0x7054
                           007054  1164 _LPXOSCGM	=	0x7054
                           007F01  1165 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1166 _MISCCTRL	=	0x7f01
                           007053  1167 G$OSCCALIB$0$0 == 0x7053
                           007053  1168 _OSCCALIB	=	0x7053
                           007050  1169 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1170 _OSCFORCERUN	=	0x7050
                           007052  1171 G$OSCREADY$0$0 == 0x7052
                           007052  1172 _OSCREADY	=	0x7052
                           007051  1173 G$OSCRUN$0$0 == 0x7051
                           007051  1174 _OSCRUN	=	0x7051
                           007040  1175 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1176 _RADIOFDATAADDR0	=	0x7040
                           007041  1177 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1178 _RADIOFDATAADDR1	=	0x7041
                           007040  1179 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1180 _RADIOFDATAADDR	=	0x7040
                           007042  1181 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1182 _RADIOFSTATADDR0	=	0x7042
                           007043  1183 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1184 _RADIOFSTATADDR1	=	0x7043
                           007042  1185 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1186 _RADIOFSTATADDR	=	0x7042
                           007044  1187 G$RADIOMUX$0$0 == 0x7044
                           007044  1188 _RADIOMUX	=	0x7044
                           007084  1189 G$SCRATCH0$0$0 == 0x7084
                           007084  1190 _SCRATCH0	=	0x7084
                           007085  1191 G$SCRATCH1$0$0 == 0x7085
                           007085  1192 _SCRATCH1	=	0x7085
                           007086  1193 G$SCRATCH2$0$0 == 0x7086
                           007086  1194 _SCRATCH2	=	0x7086
                           007087  1195 G$SCRATCH3$0$0 == 0x7087
                           007087  1196 _SCRATCH3	=	0x7087
                           007F00  1197 G$SILICONREV$0$0 == 0x7f00
                           007F00  1198 _SILICONREV	=	0x7f00
                           007F19  1199 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1200 _XTALAMPL	=	0x7f19
                           007F18  1201 G$XTALOSC$0$0 == 0x7f18
                           007F18  1202 _XTALOSC	=	0x7f18
                           007F1A  1203 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1204 _XTALREADY	=	0x7f1a
                           000000  1205 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav1$1$196==.
      0002A0                       1206 _UBLOX_GPS_PortInit_UBLOX_setNav1_1_196:
      0002A0                       1207 	.ds 3
                           000003  1208 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav2$1$196==.
      0002A3                       1209 _UBLOX_GPS_PortInit_UBLOX_setNav2_1_196:
      0002A3                       1210 	.ds 8
                           00000B  1211 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav3$1$196==.
      0002AB                       1212 _UBLOX_GPS_PortInit_UBLOX_setNav3_1_196:
      0002AB                       1213 	.ds 8
                           000013  1214 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav5$1$196==.
      0002B3                       1215 _UBLOX_GPS_PortInit_UBLOX_setNav5_1_196:
      0002B3                       1216 	.ds 36
                                   1217 ;--------------------------------------------------------
                                   1218 ; absolute external ram data
                                   1219 ;--------------------------------------------------------
                                   1220 	.area XABS    (ABS,XDATA)
                                   1221 ;--------------------------------------------------------
                                   1222 ; external initialized ram data
                                   1223 ;--------------------------------------------------------
                                   1224 	.area XISEG   (XDATA)
                                   1225 	.area HOME    (CODE)
                                   1226 	.area GSINIT0 (CODE)
                                   1227 	.area GSINIT1 (CODE)
                                   1228 	.area GSINIT2 (CODE)
                                   1229 	.area GSINIT3 (CODE)
                                   1230 	.area GSINIT4 (CODE)
                                   1231 	.area GSINIT5 (CODE)
                                   1232 	.area GSINIT  (CODE)
                                   1233 	.area GSFINAL (CODE)
                                   1234 	.area CSEG    (CODE)
                                   1235 ;--------------------------------------------------------
                                   1236 ; global & static initialisations
                                   1237 ;--------------------------------------------------------
                                   1238 	.area HOME    (CODE)
                                   1239 	.area GSINIT  (CODE)
                                   1240 	.area GSFINAL (CODE)
                                   1241 	.area GSINIT  (CODE)
                                   1242 ;--------------------------------------------------------
                                   1243 ; Home
                                   1244 ;--------------------------------------------------------
                                   1245 	.area HOME    (CODE)
                                   1246 	.area HOME    (CODE)
                                   1247 ;--------------------------------------------------------
                                   1248 ; code
                                   1249 ;--------------------------------------------------------
                                   1250 	.area CSEG    (CODE)
                                   1251 ;------------------------------------------------------------
                                   1252 ;Allocation info for local variables in function 'UBLOX_GPS_FletcherChecksum8'
                                   1253 ;------------------------------------------------------------
                                   1254 ;CK_A                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_2'
                                   1255 ;CK_B                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_3'
                                   1256 ;length                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_4'
                                   1257 ;buffer                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_buffer_1_181'
                                   1258 ;i                         Allocated to registers r1 
                                   1259 ;sloc0                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc0_1_0'
                                   1260 ;------------------------------------------------------------
                           000000  1261 	G$UBLOX_GPS_FletcherChecksum8$0$0 ==.
                           000000  1262 	C$ublox_gnss.c$31$0$0 ==.
                                   1263 ;	..\COMMON\ublox_gnss.c:31: void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
                                   1264 ;	-----------------------------------------
                                   1265 ;	 function UBLOX_GPS_FletcherChecksum8
                                   1266 ;	-----------------------------------------
      0045F6                       1267 _UBLOX_GPS_FletcherChecksum8:
                           000007  1268 	ar7 = 0x07
                           000006  1269 	ar6 = 0x06
                           000005  1270 	ar5 = 0x05
                           000004  1271 	ar4 = 0x04
                           000003  1272 	ar3 = 0x03
                           000002  1273 	ar2 = 0x02
                           000001  1274 	ar1 = 0x01
                           000000  1275 	ar0 = 0x00
      0045F6 85 82 6E         [24] 1276 	mov	_UBLOX_GPS_FletcherChecksum8_buffer_1_181,dpl
      0045F9 85 83 6F         [24] 1277 	mov	(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 1),dph
      0045FC 85 F0 70         [24] 1278 	mov	(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 2),b
                           000009  1279 	C$ublox_gnss.c$34$1$182 ==.
                                   1280 ;	..\COMMON\ublox_gnss.c:34: *CK_A = 0;
      0045FF AA 66            [24] 1281 	mov	r2,_UBLOX_GPS_FletcherChecksum8_PARM_2
      004601 AB 67            [24] 1282 	mov	r3,(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1)
      004603 AC 68            [24] 1283 	mov	r4,(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2)
      004605 8A 82            [24] 1284 	mov	dpl,r2
      004607 8B 83            [24] 1285 	mov	dph,r3
      004609 8C F0            [24] 1286 	mov	b,r4
      00460B E4               [12] 1287 	clr	a
      00460C 12 64 AE         [24] 1288 	lcall	__gptrput
                           000019  1289 	C$ublox_gnss.c$35$1$182 ==.
                                   1290 ;	..\COMMON\ublox_gnss.c:35: *CK_B = 0;
      00460F 85 69 71         [24] 1291 	mov	_UBLOX_GPS_FletcherChecksum8_sloc0_1_0,_UBLOX_GPS_FletcherChecksum8_PARM_3
      004612 85 6A 72         [24] 1292 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1),(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1)
      004615 85 6B 73         [24] 1293 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2),(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2)
      004618 85 71 82         [24] 1294 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      00461B 85 72 83         [24] 1295 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      00461E 85 73 F0         [24] 1296 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      004621 12 64 AE         [24] 1297 	lcall	__gptrput
                           00002E  1298 	C$ublox_gnss.c$36$1$182 ==.
                                   1299 ;	..\COMMON\ublox_gnss.c:36: for (i = 0; i < length; i++)
      004624 79 00            [12] 1300 	mov	r1,#0x00
      004626                       1301 00103$:
      004626 89 00            [24] 1302 	mov	ar0,r1
      004628 7F 00            [12] 1303 	mov	r7,#0x00
      00462A C3               [12] 1304 	clr	c
      00462B E8               [12] 1305 	mov	a,r0
      00462C 95 6C            [12] 1306 	subb	a,_UBLOX_GPS_FletcherChecksum8_PARM_4
      00462E EF               [12] 1307 	mov	a,r7
      00462F 95 6D            [12] 1308 	subb	a,(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1)
      004631 50 47            [24] 1309 	jnc	00101$
                           00003D  1310 	C$ublox_gnss.c$38$2$183 ==.
                                   1311 ;	..\COMMON\ublox_gnss.c:38: *CK_A += buffer[i];
      004633 8A 82            [24] 1312 	mov	dpl,r2
      004635 8B 83            [24] 1313 	mov	dph,r3
      004637 8C F0            [24] 1314 	mov	b,r4
      004639 12 72 CA         [24] 1315 	lcall	__gptrget
      00463C FF               [12] 1316 	mov	r7,a
      00463D E9               [12] 1317 	mov	a,r1
      00463E 25 6E            [12] 1318 	add	a,_UBLOX_GPS_FletcherChecksum8_buffer_1_181
      004640 F8               [12] 1319 	mov	r0,a
      004641 E4               [12] 1320 	clr	a
      004642 35 6F            [12] 1321 	addc	a,(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 1)
      004644 FD               [12] 1322 	mov	r5,a
      004645 AE 70            [24] 1323 	mov	r6,(_UBLOX_GPS_FletcherChecksum8_buffer_1_181 + 2)
      004647 88 82            [24] 1324 	mov	dpl,r0
      004649 8D 83            [24] 1325 	mov	dph,r5
      00464B 8E F0            [24] 1326 	mov	b,r6
      00464D 12 72 CA         [24] 1327 	lcall	__gptrget
      004650 F8               [12] 1328 	mov	r0,a
      004651 2F               [12] 1329 	add	a,r7
      004652 FF               [12] 1330 	mov	r7,a
      004653 8A 82            [24] 1331 	mov	dpl,r2
      004655 8B 83            [24] 1332 	mov	dph,r3
      004657 8C F0            [24] 1333 	mov	b,r4
      004659 12 64 AE         [24] 1334 	lcall	__gptrput
                           000066  1335 	C$ublox_gnss.c$40$2$183 ==.
                                   1336 ;	..\COMMON\ublox_gnss.c:40: *CK_B += *CK_A;
      00465C 85 71 82         [24] 1337 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      00465F 85 72 83         [24] 1338 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      004662 85 73 F0         [24] 1339 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      004665 12 72 CA         [24] 1340 	lcall	__gptrget
      004668 FE               [12] 1341 	mov	r6,a
      004669 2F               [12] 1342 	add	a,r7
      00466A FF               [12] 1343 	mov	r7,a
      00466B 85 71 82         [24] 1344 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      00466E 85 72 83         [24] 1345 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      004671 85 73 F0         [24] 1346 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      004674 12 64 AE         [24] 1347 	lcall	__gptrput
                           000081  1348 	C$ublox_gnss.c$36$1$182 ==.
                                   1349 ;	..\COMMON\ublox_gnss.c:36: for (i = 0; i < length; i++)
      004677 09               [12] 1350 	inc	r1
      004678 80 AC            [24] 1351 	sjmp	00103$
      00467A                       1352 00101$:
                           000084  1353 	C$ublox_gnss.c$42$1$182 ==.
                                   1354 ;	..\COMMON\ublox_gnss.c:42: return;
                           000084  1355 	C$ublox_gnss.c$43$1$182 ==.
                           000084  1356 	XG$UBLOX_GPS_FletcherChecksum8$0$0 ==.
      00467A 22               [24] 1357 	ret
                                   1358 ;------------------------------------------------------------
                                   1359 ;Allocation info for local variables in function 'UBLOX_GPS_SendCommand_WaitACK'
                                   1360 ;------------------------------------------------------------
                                   1361 ;msg_id                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_2'
                                   1362 ;msg_length                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_3'
                                   1363 ;payload                   Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_4'
                                   1364 ;AnsOrAck                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_5'
                                   1365 ;RxLength                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_6'
                                   1366 ;msg_class                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184'
                                   1367 ;i                         Allocated to registers r1 
                                   1368 ;k                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_k_1_185'
                                   1369 ;RetVal                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185'
                                   1370 ;buffer_aux                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185'
                                   1371 ;rx_buffer                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185'
                                   1372 ;CK_A                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185'
                                   1373 ;CK_B                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185'
                                   1374 ;sloc0                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0'
                                   1375 ;sloc1                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0'
                                   1376 ;------------------------------------------------------------
                           000085  1377 	G$UBLOX_GPS_SendCommand_WaitACK$0$0 ==.
                           000085  1378 	C$ublox_gnss.c$56$1$182 ==.
                                   1379 ;	..\COMMON\ublox_gnss.c:56: uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
                                   1380 ;	-----------------------------------------
                                   1381 ;	 function UBLOX_GPS_SendCommand_WaitACK
                                   1382 ;	-----------------------------------------
      00467B                       1383 _UBLOX_GPS_SendCommand_WaitACK:
      00467B 85 82 30         [24] 1384 	mov	_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184,dpl
                           000088  1385 	C$ublox_gnss.c$59$1$182 ==.
                                   1386 ;	..\COMMON\ublox_gnss.c:59: uint8_t RetVal = 0;
      00467E 75 32 00         [24] 1387 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x00
                           00008B  1388 	C$ublox_gnss.c$63$1$185 ==.
                                   1389 ;	..\COMMON\ublox_gnss.c:63: buffer_aux =(uint8_t *) malloc((msg_length)+10);
      004681 74 0A            [12] 1390 	mov	a,#0x0a
      004683 25 27            [12] 1391 	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      004685 F5 82            [12] 1392 	mov	dpl,a
      004687 E4               [12] 1393 	clr	a
      004688 35 28            [12] 1394 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      00468A F5 83            [12] 1395 	mov	dph,a
      00468C 12 65 C3         [24] 1396 	lcall	_malloc
      00468F AC 82            [24] 1397 	mov	r4,dpl
      004691 AD 83            [24] 1398 	mov	r5,dph
      004693 8C 33            [24] 1399 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,r4
      004695 8D 34            [24] 1400 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),r5
                           0000A1  1401 	C$ublox_gnss.c$64$1$185 ==.
                                   1402 ;	..\COMMON\ublox_gnss.c:64: rx_buffer =(uint8_t *) malloc(40);
      004697 90 00 28         [24] 1403 	mov	dptr,#0x0028
      00469A 12 65 C3         [24] 1404 	lcall	_malloc
      00469D AA 82            [24] 1405 	mov	r2,dpl
      00469F AB 83            [24] 1406 	mov	r3,dph
      0046A1 8A 35            [24] 1407 	mov	_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185,r2
      0046A3 8B 36            [24] 1408 	mov	(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1),r3
                           0000AF  1409 	C$ublox_gnss.c$65$1$185 ==.
                                   1410 ;	..\COMMON\ublox_gnss.c:65: for(i = 0; i<40;i++)
      0046A5 79 00            [12] 1411 	mov	r1,#0x00
      0046A7                       1412 00125$:
                           0000B1  1413 	C$ublox_gnss.c$67$2$186 ==.
                                   1414 ;	..\COMMON\ublox_gnss.c:67: *(rx_buffer+i)=0;
      0046A7 E9               [12] 1415 	mov	a,r1
      0046A8 25 35            [12] 1416 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0046AA F5 82            [12] 1417 	mov	dpl,a
      0046AC E4               [12] 1418 	clr	a
      0046AD 35 36            [12] 1419 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0046AF F5 83            [12] 1420 	mov	dph,a
      0046B1 E4               [12] 1421 	clr	a
      0046B2 F0               [24] 1422 	movx	@dptr,a
                           0000BD  1423 	C$ublox_gnss.c$65$1$185 ==.
                                   1424 ;	..\COMMON\ublox_gnss.c:65: for(i = 0; i<40;i++)
      0046B3 09               [12] 1425 	inc	r1
      0046B4 B9 28 00         [24] 1426 	cjne	r1,#0x28,00186$
      0046B7                       1427 00186$:
      0046B7 40 EE            [24] 1428 	jc	00125$
                           0000C3  1429 	C$ublox_gnss.c$70$1$185 ==.
                                   1430 ;	..\COMMON\ublox_gnss.c:70: *buffer_aux = msg_class;
      0046B9 85 33 82         [24] 1431 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      0046BC 85 34 83         [24] 1432 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      0046BF E5 30            [12] 1433 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184
      0046C1 F0               [24] 1434 	movx	@dptr,a
      0046C2 A3               [24] 1435 	inc	dptr
                           0000CD  1436 	C$ublox_gnss.c$71$1$185 ==.
                                   1437 ;	..\COMMON\ublox_gnss.c:71: buffer_aux++;
                           0000CD  1438 	C$ublox_gnss.c$72$1$185 ==.
                                   1439 ;	..\COMMON\ublox_gnss.c:72: *buffer_aux = msg_id;
      0046C3 85 82 33         [24] 1440 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,dpl
      0046C6 85 83 34         [24] 1441 	mov  (_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),dph
      0046C9 E5 26            [12] 1442 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      0046CB F0               [24] 1443 	movx	@dptr,a
      0046CC A3               [24] 1444 	inc	dptr
      0046CD 85 82 33         [24] 1445 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,dpl
      0046D0 85 83 34         [24] 1446 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),dph
                           0000DD  1447 	C$ublox_gnss.c$73$1$185 ==.
                                   1448 ;	..\COMMON\ublox_gnss.c:73: buffer_aux++;
                           0000DD  1449 	C$ublox_gnss.c$74$1$185 ==.
                                   1450 ;	..\COMMON\ublox_gnss.c:74: *buffer_aux = msg_length;
      0046D3 A9 27            [24] 1451 	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0046D5 85 33 82         [24] 1452 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      0046D8 85 34 83         [24] 1453 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      0046DB E9               [12] 1454 	mov	a,r1
      0046DC F0               [24] 1455 	movx	@dptr,a
                           0000E7  1456 	C$ublox_gnss.c$75$1$185 ==.
                                   1457 ;	..\COMMON\ublox_gnss.c:75: buffer_aux +=2;
      0046DD 74 02            [12] 1458 	mov	a,#0x02
      0046DF 25 33            [12] 1459 	add	a,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      0046E1 F5 33            [12] 1460 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,a
      0046E3 E4               [12] 1461 	clr	a
      0046E4 35 34            [12] 1462 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      0046E6 F5 34            [12] 1463 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),a
                           0000F2  1464 	C$ublox_gnss.c$76$1$185 ==.
                                   1465 ;	..\COMMON\ublox_gnss.c:76: memcpy(buffer_aux,payload,msg_length);
      0046E8 A8 33            [24] 1466 	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      0046EA A9 34            [24] 1467 	mov	r1,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      0046EC 7E 00            [12] 1468 	mov	r6,#0x00
      0046EE 85 29 64         [24] 1469 	mov	_memcpy_PARM_2,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      0046F1 85 2A 65         [24] 1470 	mov	(_memcpy_PARM_2 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
      0046F4 85 2B 66         [24] 1471 	mov	(_memcpy_PARM_2 + 2),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
      0046F7 85 27 67         [24] 1472 	mov	_memcpy_PARM_3,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0046FA 85 28 68         [24] 1473 	mov	(_memcpy_PARM_3 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      0046FD 88 82            [24] 1474 	mov	dpl,r0
      0046FF 89 83            [24] 1475 	mov	dph,r1
      004701 8E F0            [24] 1476 	mov	b,r6
      004703 12 60 C7         [24] 1477 	lcall	_memcpy
                           000110  1478 	C$ublox_gnss.c$77$1$185 ==.
                                   1479 ;	..\COMMON\ublox_gnss.c:77: buffer_aux -=4;
      004706 E5 33            [12] 1480 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      004708 24 FC            [12] 1481 	add	a,#0xfc
      00470A F5 33            [12] 1482 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185,a
      00470C E5 34            [12] 1483 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      00470E 34 FF            [12] 1484 	addc	a,#0xff
      004710 F5 34            [12] 1485 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1),a
                           00011C  1486 	C$ublox_gnss.c$78$1$185 ==.
                                   1487 ;	..\COMMON\ublox_gnss.c:78: UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);
      004712 A8 33            [24] 1488 	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      004714 A9 34            [24] 1489 	mov	r1,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      004716 7E 00            [12] 1490 	mov	r6,#0x00
      004718 75 66 37         [24] 1491 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_2,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185
                                   1492 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),#0x00
      00471B 8E 67            [24] 1493 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),r6
      00471D 75 68 40         [24] 1494 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2),#0x40
      004720 75 69 38         [24] 1495 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_3,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185
                                   1496 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),#0x00
      004723 8E 6A            [24] 1497 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),r6
      004725 75 6B 40         [24] 1498 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2),#0x40
      004728 74 04            [12] 1499 	mov	a,#0x04
      00472A 25 27            [12] 1500 	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      00472C F5 6C            [12] 1501 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_4,a
      00472E E4               [12] 1502 	clr	a
      00472F 35 28            [12] 1503 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      004731 F5 6D            [12] 1504 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1),a
      004733 88 82            [24] 1505 	mov	dpl,r0
      004735 89 83            [24] 1506 	mov	dph,r1
      004737 8E F0            [24] 1507 	mov	b,r6
      004739 12 45 F6         [24] 1508 	lcall	_UBLOX_GPS_FletcherChecksum8
                           000146  1509 	C$ublox_gnss.c$80$1$185 ==.
                                   1510 ;	..\COMMON\ublox_gnss.c:80: uart1_tx(UBX_HEADER1_VAL);
      00473C 75 82 B5         [24] 1511 	mov	dpl,#0xb5
      00473F 12 67 45         [24] 1512 	lcall	_uart1_tx
                           00014C  1513 	C$ublox_gnss.c$81$1$185 ==.
                                   1514 ;	..\COMMON\ublox_gnss.c:81: uart1_tx(UBX_HEADER2_VAL);
      004742 75 82 62         [24] 1515 	mov	dpl,#0x62
      004745 12 67 45         [24] 1516 	lcall	_uart1_tx
                           000152  1517 	C$ublox_gnss.c$82$1$185 ==.
                                   1518 ;	..\COMMON\ublox_gnss.c:82: uart1_tx(msg_class);
      004748 85 30 82         [24] 1519 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184
      00474B 12 67 45         [24] 1520 	lcall	_uart1_tx
                           000158  1521 	C$ublox_gnss.c$83$1$185 ==.
                                   1522 ;	..\COMMON\ublox_gnss.c:83: uart1_tx(msg_id);
      00474E 85 26 82         [24] 1523 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      004751 12 67 45         [24] 1524 	lcall	_uart1_tx
                           00015E  1525 	C$ublox_gnss.c$84$1$185 ==.
                                   1526 ;	..\COMMON\ublox_gnss.c:84: uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
      004754 A9 27            [24] 1527 	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      004756 89 82            [24] 1528 	mov	dpl,r1
      004758 12 67 45         [24] 1529 	lcall	_uart1_tx
                           000165  1530 	C$ublox_gnss.c$85$1$185 ==.
                                   1531 ;	..\COMMON\ublox_gnss.c:85: uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB
      00475B 79 00            [12] 1532 	mov	r1,#0x00
      00475D AE 28            [24] 1533 	mov	r6,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      00475F 8E 82            [24] 1534 	mov	dpl,r6
      004761 12 67 45         [24] 1535 	lcall	_uart1_tx
                           00016E  1536 	C$ublox_gnss.c$88$1$185 ==.
                                   1537 ;	..\COMMON\ublox_gnss.c:88: for(i = 0;i<msg_length;i++)
      004764 7E 00            [12] 1538 	mov	r6,#0x00
      004766                       1539 00128$:
      004766 8E 00            [24] 1540 	mov	ar0,r6
      004768 79 00            [12] 1541 	mov	r1,#0x00
      00476A C3               [12] 1542 	clr	c
      00476B E8               [12] 1543 	mov	a,r0
      00476C 95 27            [12] 1544 	subb	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      00476E E9               [12] 1545 	mov	a,r1
      00476F 95 28            [12] 1546 	subb	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      004771 50 1C            [24] 1547 	jnc	00102$
                           00017D  1548 	C$ublox_gnss.c$90$2$187 ==.
                                   1549 ;	..\COMMON\ublox_gnss.c:90: uart1_tx(*(payload+i));
      004773 EE               [12] 1550 	mov	a,r6
      004774 25 29            [12] 1551 	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      004776 F8               [12] 1552 	mov	r0,a
      004777 E4               [12] 1553 	clr	a
      004778 35 2A            [12] 1554 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
      00477A F9               [12] 1555 	mov	r1,a
      00477B AF 2B            [24] 1556 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
      00477D 88 82            [24] 1557 	mov	dpl,r0
      00477F 89 83            [24] 1558 	mov	dph,r1
      004781 8F F0            [24] 1559 	mov	b,r7
      004783 12 72 CA         [24] 1560 	lcall	__gptrget
      004786 F8               [12] 1561 	mov	r0,a
      004787 F5 82            [12] 1562 	mov	dpl,a
      004789 12 67 45         [24] 1563 	lcall	_uart1_tx
                           000196  1564 	C$ublox_gnss.c$88$1$185 ==.
                                   1565 ;	..\COMMON\ublox_gnss.c:88: for(i = 0;i<msg_length;i++)
      00478C 0E               [12] 1566 	inc	r6
      00478D 80 D7            [24] 1567 	sjmp	00128$
      00478F                       1568 00102$:
                           000199  1569 	C$ublox_gnss.c$93$1$185 ==.
                                   1570 ;	..\COMMON\ublox_gnss.c:93: uart1_tx(CK_A);
      00478F 85 37 82         [24] 1571 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185
      004792 12 67 45         [24] 1572 	lcall	_uart1_tx
                           00019F  1573 	C$ublox_gnss.c$94$1$185 ==.
                                   1574 ;	..\COMMON\ublox_gnss.c:94: uart1_tx(CK_B);
      004795 85 38 82         [24] 1575 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185
      004798 12 67 45         [24] 1576 	lcall	_uart1_tx
                           0001A5  1577 	C$ublox_gnss.c$96$1$185 ==.
                                   1578 ;	..\COMMON\ublox_gnss.c:96: dbglink_writestr("command sent\n");
      00479B 90 76 EE         [24] 1579 	mov	dptr,#___str_0
      00479E 75 F0 80         [24] 1580 	mov	b,#0x80
      0047A1 12 69 E1         [24] 1581 	lcall	_dbglink_writestr
                           0001AE  1582 	C$ublox_gnss.c$99$1$185 ==.
                                   1583 ;	..\COMMON\ublox_gnss.c:99: do{
      0047A4                       1584 00103$:
                           0001AE  1585 	C$ublox_gnss.c$100$2$188 ==.
                                   1586 ;	..\COMMON\ublox_gnss.c:100: delay(2500);
      0047A4 90 09 C4         [24] 1587 	mov	dptr,#0x09c4
      0047A7 12 6B F2         [24] 1588 	lcall	_delay
                           0001B4  1589 	C$ublox_gnss.c$105$1$185 ==.
                                   1590 ;	..\COMMON\ublox_gnss.c:105: }while(!uart1_rxcount());
      0047AA 12 7E E8         [24] 1591 	lcall	_uart1_rxcount
      0047AD E5 82            [12] 1592 	mov	a,dpl
      0047AF 60 F3            [24] 1593 	jz	00103$
                           0001BB  1594 	C$ublox_gnss.c$106$1$185 ==.
                                   1595 ;	..\COMMON\ublox_gnss.c:106: delay(25000);
      0047B1 90 61 A8         [24] 1596 	mov	dptr,#0x61a8
      0047B4 12 6B F2         [24] 1597 	lcall	_delay
                           0001C1  1598 	C$ublox_gnss.c$109$1$185 ==.
                                   1599 ;	..\COMMON\ublox_gnss.c:109: do
      0047B7 75 31 00         [24] 1600 	mov	_UBLOX_GPS_SendCommand_WaitACK_k_1_185,#0x00
      0047BA                       1601 00106$:
                           0001C4  1602 	C$ublox_gnss.c$112$2$189 ==.
                                   1603 ;	..\COMMON\ublox_gnss.c:112: *(rx_buffer+k)=uart1_rx();
      0047BA E5 31            [12] 1604 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_k_1_185
      0047BC 25 35            [12] 1605 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0047BE F9               [12] 1606 	mov	r1,a
      0047BF E4               [12] 1607 	clr	a
      0047C0 35 36            [12] 1608 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0047C2 FE               [12] 1609 	mov	r6,a
      0047C3 12 64 FD         [24] 1610 	lcall	_uart1_rx
      0047C6 A8 82            [24] 1611 	mov	r0,dpl
      0047C8 89 82            [24] 1612 	mov	dpl,r1
      0047CA 8E 83            [24] 1613 	mov	dph,r6
      0047CC E8               [12] 1614 	mov	a,r0
      0047CD F0               [24] 1615 	movx	@dptr,a
                           0001D8  1616 	C$ublox_gnss.c$113$2$189 ==.
                                   1617 ;	..\COMMON\ublox_gnss.c:113: k++;
      0047CE 05 31            [12] 1618 	inc	_UBLOX_GPS_SendCommand_WaitACK_k_1_185
                           0001DA  1619 	C$ublox_gnss.c$115$2$189 ==.
                                   1620 ;	..\COMMON\ublox_gnss.c:115: delay(2500);
      0047D0 90 09 C4         [24] 1621 	mov	dptr,#0x09c4
      0047D3 12 6B F2         [24] 1622 	lcall	_delay
                           0001E0  1623 	C$ublox_gnss.c$116$1$185 ==.
                                   1624 ;	..\COMMON\ublox_gnss.c:116: }while(uart1_rxcount());
      0047D6 12 7E E8         [24] 1625 	lcall	_uart1_rxcount
      0047D9 E5 82            [12] 1626 	mov	a,dpl
      0047DB 70 DD            [24] 1627 	jnz	00106$
                           0001E7  1628 	C$ublox_gnss.c$120$1$185 ==.
                                   1629 ;	..\COMMON\ublox_gnss.c:120: dbglink_writestr("msg rcv\n");
      0047DD 90 76 FC         [24] 1630 	mov	dptr,#___str_1
      0047E0 75 F0 80         [24] 1631 	mov	b,#0x80
      0047E3 12 69 E1         [24] 1632 	lcall	_dbglink_writestr
                           0001F0  1633 	C$ublox_gnss.c$124$1$185 ==.
                                   1634 ;	..\COMMON\ublox_gnss.c:124: if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
      0047E6 85 35 82         [24] 1635 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0047E9 85 36 83         [24] 1636 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0047EC E0               [24] 1637 	movx	a,@dptr
      0047ED FE               [12] 1638 	mov	r6,a
      0047EE BE B5 02         [24] 1639 	cjne	r6,#0xb5,00191$
      0047F1 80 03            [24] 1640 	sjmp	00192$
      0047F3                       1641 00191$:
      0047F3 02 49 49         [24] 1642 	ljmp	00123$
      0047F6                       1643 00192$:
      0047F6 85 35 82         [24] 1644 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0047F9 85 36 83         [24] 1645 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0047FC A3               [24] 1646 	inc	dptr
      0047FD E0               [24] 1647 	movx	a,@dptr
      0047FE FE               [12] 1648 	mov	r6,a
      0047FF BE 62 02         [24] 1649 	cjne	r6,#0x62,00193$
      004802 80 03            [24] 1650 	sjmp	00194$
      004804                       1651 00193$:
      004804 02 49 49         [24] 1652 	ljmp	00123$
      004807                       1653 00194$:
                           000211  1654 	C$ublox_gnss.c$126$2$190 ==.
                                   1655 ;	..\COMMON\ublox_gnss.c:126: CK_A = 0;
                           000211  1656 	C$ublox_gnss.c$127$2$190 ==.
                                   1657 ;	..\COMMON\ublox_gnss.c:127: CK_B = 0;
                           000211  1658 	C$ublox_gnss.c$128$2$190 ==.
                                   1659 ;	..\COMMON\ublox_gnss.c:128: UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
      004807 E4               [12] 1660 	clr	a
      004808 F5 37            [12] 1661 	mov	_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185,a
      00480A F5 38            [12] 1662 	mov	_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185,a
      00480C 74 02            [12] 1663 	mov	a,#0x02
      00480E 25 35            [12] 1664 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      004810 F9               [12] 1665 	mov	r1,a
      004811 E4               [12] 1666 	clr	a
      004812 35 36            [12] 1667 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      004814 FE               [12] 1668 	mov	r6,a
      004815 89 00            [24] 1669 	mov	ar0,r1
      004817 8E 05            [24] 1670 	mov	ar5,r6
      004819 7F 00            [12] 1671 	mov	r7,#0x00
      00481B 75 66 37         [24] 1672 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_2,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185
                                   1673 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),#0x00
      00481E 8F 67            [24] 1674 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),r7
      004820 75 68 40         [24] 1675 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2),#0x40
      004823 75 69 38         [24] 1676 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_3,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185
                                   1677 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),#0x00
      004826 8F 6A            [24] 1678 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),r7
      004828 75 6B 40         [24] 1679 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2),#0x40
      00482B 85 31 39         [24] 1680 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,_UBLOX_GPS_SendCommand_WaitACK_k_1_185
                                   1681 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),#0x00
      00482E 8F 3A            [24] 1682 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),r7
      004830 E5 39            [12] 1683 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      004832 24 FC            [12] 1684 	add	a,#0xfc
      004834 F5 6C            [12] 1685 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_4,a
      004836 E5 3A            [12] 1686 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      004838 34 FF            [12] 1687 	addc	a,#0xff
      00483A F5 6D            [12] 1688 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1),a
      00483C 88 82            [24] 1689 	mov	dpl,r0
      00483E 8D 83            [24] 1690 	mov	dph,r5
      004840 8F F0            [24] 1691 	mov	b,r7
      004842 C0 06            [24] 1692 	push	ar6
      004844 C0 01            [24] 1693 	push	ar1
      004846 12 45 F6         [24] 1694 	lcall	_UBLOX_GPS_FletcherChecksum8
      004849 D0 01            [24] 1695 	pop	ar1
      00484B D0 06            [24] 1696 	pop	ar6
                           000257  1697 	C$ublox_gnss.c$129$2$190 ==.
                                   1698 ;	..\COMMON\ublox_gnss.c:129: if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
      00484D E5 39            [12] 1699 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      00484F 24 FE            [12] 1700 	add	a,#0xfe
      004851 FD               [12] 1701 	mov	r5,a
      004852 E5 3A            [12] 1702 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      004854 34 FF            [12] 1703 	addc	a,#0xff
      004856 FF               [12] 1704 	mov	r7,a
      004857 ED               [12] 1705 	mov	a,r5
      004858 25 35            [12] 1706 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      00485A F5 82            [12] 1707 	mov	dpl,a
      00485C EF               [12] 1708 	mov	a,r7
      00485D 35 36            [12] 1709 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      00485F F5 83            [12] 1710 	mov	dph,a
      004861 E0               [24] 1711 	movx	a,@dptr
      004862 FF               [12] 1712 	mov	r7,a
      004863 B5 37 02         [24] 1713 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_185,00195$
      004866 80 03            [24] 1714 	sjmp	00196$
      004868                       1715 00195$:
      004868 02 49 49         [24] 1716 	ljmp	00123$
      00486B                       1717 00196$:
      00486B E5 39            [12] 1718 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      00486D 24 FF            [12] 1719 	add	a,#0xff
      00486F FD               [12] 1720 	mov	r5,a
      004870 E5 3A            [12] 1721 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      004872 34 FF            [12] 1722 	addc	a,#0xff
      004874 FF               [12] 1723 	mov	r7,a
      004875 ED               [12] 1724 	mov	a,r5
      004876 25 35            [12] 1725 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      004878 F5 82            [12] 1726 	mov	dpl,a
      00487A EF               [12] 1727 	mov	a,r7
      00487B 35 36            [12] 1728 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      00487D F5 83            [12] 1729 	mov	dph,a
      00487F E0               [24] 1730 	movx	a,@dptr
      004880 FF               [12] 1731 	mov	r7,a
      004881 B5 38 02         [24] 1732 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_185,00197$
      004884 80 03            [24] 1733 	sjmp	00198$
      004886                       1734 00197$:
      004886 02 49 49         [24] 1735 	ljmp	00123$
      004889                       1736 00198$:
                           000293  1737 	C$ublox_gnss.c$132$3$191 ==.
                                   1738 ;	..\COMMON\ublox_gnss.c:132: if(!AnsOrAck)
      004889 E5 2C            [12] 1739 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      00488B 60 03            [24] 1740 	jz	00199$
      00488D 02 49 2A         [24] 1741 	ljmp	00117$
      004890                       1742 00199$:
                           00029A  1743 	C$ublox_gnss.c$135$4$192 ==.
                                   1744 ;	..\COMMON\ublox_gnss.c:135: if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
      004890 89 82            [24] 1745 	mov	dpl,r1
      004892 8E 83            [24] 1746 	mov	dph,r6
      004894 E0               [24] 1747 	movx	a,@dptr
      004895 FF               [12] 1748 	mov	r7,a
      004896 B5 30 02         [24] 1749 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_184,00200$
      004899 80 03            [24] 1750 	sjmp	00201$
      00489B                       1751 00200$:
      00489B 02 49 49         [24] 1752 	ljmp	00123$
      00489E                       1753 00201$:
      00489E 85 35 82         [24] 1754 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0048A1 85 36 83         [24] 1755 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0048A4 A3               [24] 1756 	inc	dptr
      0048A5 A3               [24] 1757 	inc	dptr
      0048A6 A3               [24] 1758 	inc	dptr
      0048A7 E0               [24] 1759 	movx	a,@dptr
      0048A8 FF               [12] 1760 	mov	r7,a
      0048A9 B5 26 02         [24] 1761 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_2,00202$
      0048AC 80 03            [24] 1762 	sjmp	00203$
      0048AE                       1763 00202$:
      0048AE 02 49 49         [24] 1764 	ljmp	00123$
      0048B1                       1765 00203$:
                           0002BB  1766 	C$ublox_gnss.c$137$5$193 ==.
                                   1767 ;	..\COMMON\ublox_gnss.c:137: *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
      0048B1 AC 2D            [24] 1768 	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      0048B3 AD 2E            [24] 1769 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1)
      0048B5 AF 2F            [24] 1770 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2)
      0048B7 85 35 82         [24] 1771 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0048BA 85 36 83         [24] 1772 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0048BD A3               [24] 1773 	inc	dptr
      0048BE A3               [24] 1774 	inc	dptr
      0048BF A3               [24] 1775 	inc	dptr
      0048C0 A3               [24] 1776 	inc	dptr
      0048C1 A3               [24] 1777 	inc	dptr
      0048C2 E0               [24] 1778 	movx	a,@dptr
      0048C3 F8               [12] 1779 	mov	r0,a
      0048C4 7B 00            [12] 1780 	mov	r3,#0x00
      0048C6 88 3A            [24] 1781 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),r0
                                   1782 ;	1-genFromRTrack replaced	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,#0x00
      0048C8 8B 39            [24] 1783 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,r3
      0048CA 85 35 82         [24] 1784 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0048CD 85 36 83         [24] 1785 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      0048D0 A3               [24] 1786 	inc	dptr
      0048D1 A3               [24] 1787 	inc	dptr
      0048D2 A3               [24] 1788 	inc	dptr
      0048D3 A3               [24] 1789 	inc	dptr
      0048D4 E0               [24] 1790 	movx	a,@dptr
      0048D5 FA               [12] 1791 	mov	r2,a
      0048D6 7B 00            [12] 1792 	mov	r3,#0x00
      0048D8 E5 39            [12] 1793 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      0048DA 42 02            [12] 1794 	orl	ar2,a
      0048DC E5 3A            [12] 1795 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      0048DE 42 03            [12] 1796 	orl	ar3,a
      0048E0 8C 82            [24] 1797 	mov	dpl,r4
      0048E2 8D 83            [24] 1798 	mov	dph,r5
      0048E4 8F F0            [24] 1799 	mov	b,r7
      0048E6 EA               [12] 1800 	mov	a,r2
      0048E7 12 64 AE         [24] 1801 	lcall	__gptrput
      0048EA A3               [24] 1802 	inc	dptr
      0048EB EB               [12] 1803 	mov	a,r3
      0048EC 12 64 AE         [24] 1804 	lcall	__gptrput
                           0002F9  1805 	C$ublox_gnss.c$138$5$193 ==.
                                   1806 ;	..\COMMON\ublox_gnss.c:138: memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
      0048EF 85 29 3B         [24] 1807 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      0048F2 85 2A 3C         [24] 1808 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
      0048F5 85 2B 3D         [24] 1809 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
      0048F8 74 06            [12] 1810 	mov	a,#0x06
      0048FA 25 35            [12] 1811 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      0048FC FA               [12] 1812 	mov	r2,a
      0048FD E4               [12] 1813 	clr	a
      0048FE 35 36            [12] 1814 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      004900 FB               [12] 1815 	mov	r3,a
      004901 8A 64            [24] 1816 	mov	_memcpy_PARM_2,r2
      004903 8B 65            [24] 1817 	mov	(_memcpy_PARM_2 + 1),r3
      004905 75 66 00         [24] 1818 	mov	(_memcpy_PARM_2 + 2),#0x00
      004908 8C 82            [24] 1819 	mov	dpl,r4
      00490A 8D 83            [24] 1820 	mov	dph,r5
      00490C 8F F0            [24] 1821 	mov	b,r7
      00490E 12 72 CA         [24] 1822 	lcall	__gptrget
      004911 F5 67            [12] 1823 	mov	_memcpy_PARM_3,a
      004913 A3               [24] 1824 	inc	dptr
      004914 12 72 CA         [24] 1825 	lcall	__gptrget
      004917 F5 68            [12] 1826 	mov	(_memcpy_PARM_3 + 1),a
      004919 85 3B 82         [24] 1827 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      00491C 85 3C 83         [24] 1828 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      00491F 85 3D F0         [24] 1829 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      004922 12 60 C7         [24] 1830 	lcall	_memcpy
                           00032F  1831 	C$ublox_gnss.c$139$5$193 ==.
                                   1832 ;	..\COMMON\ublox_gnss.c:139: RetVal = OK;
      004925 75 32 01         [24] 1833 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x01
      004928 80 1F            [24] 1834 	sjmp	00123$
      00492A                       1835 00117$:
                           000334  1836 	C$ublox_gnss.c$145$4$194 ==.
                                   1837 ;	..\COMMON\ublox_gnss.c:145: if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
      00492A 89 82            [24] 1838 	mov	dpl,r1
      00492C 8E 83            [24] 1839 	mov	dph,r6
      00492E E0               [24] 1840 	movx	a,@dptr
      00492F F9               [12] 1841 	mov	r1,a
      004930 B9 05 13         [24] 1842 	cjne	r1,#0x05,00113$
      004933 85 35 82         [24] 1843 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      004936 85 36 83         [24] 1844 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      004939 A3               [24] 1845 	inc	dptr
      00493A A3               [24] 1846 	inc	dptr
      00493B A3               [24] 1847 	inc	dptr
      00493C E0               [24] 1848 	movx	a,@dptr
      00493D FF               [12] 1849 	mov	r7,a
      00493E BF 01 05         [24] 1850 	cjne	r7,#0x01,00113$
      004941 75 32 01         [24] 1851 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x01
      004944 80 03            [24] 1852 	sjmp	00123$
      004946                       1853 00113$:
                           000350  1854 	C$ublox_gnss.c$146$4$194 ==.
                                   1855 ;	..\COMMON\ublox_gnss.c:146: else RetVal = NAK;
      004946 75 32 00         [24] 1856 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185,#0x00
      004949                       1857 00123$:
                           000353  1858 	C$ublox_gnss.c$150$1$185 ==.
                                   1859 ;	..\COMMON\ublox_gnss.c:150: free(buffer_aux);
      004949 AC 33            [24] 1860 	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185
      00494B AD 34            [24] 1861 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_185 + 1)
      00494D 7F 00            [12] 1862 	mov	r7,#0x00
      00494F 8C 82            [24] 1863 	mov	dpl,r4
      004951 8D 83            [24] 1864 	mov	dph,r5
      004953 8F F0            [24] 1865 	mov	b,r7
      004955 12 56 69         [24] 1866 	lcall	_free
                           000362  1867 	C$ublox_gnss.c$151$1$185 ==.
                                   1868 ;	..\COMMON\ublox_gnss.c:151: free(rx_buffer);
      004958 AA 35            [24] 1869 	mov	r2,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185
      00495A AB 36            [24] 1870 	mov	r3,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_185 + 1)
      00495C 7F 00            [12] 1871 	mov	r7,#0x00
      00495E 8A 82            [24] 1872 	mov	dpl,r2
      004960 8B 83            [24] 1873 	mov	dph,r3
      004962 8F F0            [24] 1874 	mov	b,r7
      004964 12 56 69         [24] 1875 	lcall	_free
                           000371  1876 	C$ublox_gnss.c$152$1$185 ==.
                                   1877 ;	..\COMMON\ublox_gnss.c:152: return RetVal;
      004967 85 32 82         [24] 1878 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_185
                           000374  1879 	C$ublox_gnss.c$153$1$185 ==.
                           000374  1880 	XG$UBLOX_GPS_SendCommand_WaitACK$0$0 ==.
      00496A 22               [24] 1881 	ret
                                   1882 ;------------------------------------------------------------
                                   1883 ;Allocation info for local variables in function 'UBLOX_GPS_PortInit'
                                   1884 ;------------------------------------------------------------
                                   1885 ;UBLOX_setNav1             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196'
                                   1886 ;UBLOX_setNav2             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196'
                                   1887 ;UBLOX_setNav3             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196'
                                   1888 ;UBLOX_setNav5             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196'
                                   1889 ;------------------------------------------------------------
                           000375  1890 	G$UBLOX_GPS_PortInit$0$0 ==.
                           000375  1891 	C$ublox_gnss.c$160$1$185 ==.
                                   1892 ;	..\COMMON\ublox_gnss.c:160: void UBLOX_GPS_PortInit(void)
                                   1893 ;	-----------------------------------------
                                   1894 ;	 function UBLOX_GPS_PortInit
                                   1895 ;	-----------------------------------------
      00496B                       1896 _UBLOX_GPS_PortInit:
                           000375  1897 	C$ublox_gnss.c$162$1$185 ==.
                                   1898 ;	..\COMMON\ublox_gnss.c:162: __xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
      00496B 90 02 A0         [24] 1899 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
      00496E 74 F0            [12] 1900 	mov	a,#0xf0
      004970 F0               [24] 1901 	movx	@dptr,a
      004971 90 02 A1         [24] 1902 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
      004974 74 04            [12] 1903 	mov	a,#0x04
      004976 F0               [24] 1904 	movx	@dptr,a
      004977 90 02 A2         [24] 1905 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0002)
      00497A E4               [12] 1906 	clr	a
      00497B F0               [24] 1907 	movx	@dptr,a
                           000386  1908 	C$ublox_gnss.c$163$1$185 ==.
                                   1909 ;	..\COMMON\ublox_gnss.c:163: __xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
      00497C 90 02 A3         [24] 1910 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196
      00497F 74 F0            [12] 1911 	mov	a,#0xf0
      004981 F0               [24] 1912 	movx	@dptr,a
      004982 90 02 A4         [24] 1913 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0001)
      004985 74 01            [12] 1914 	mov	a,#0x01
      004987 F0               [24] 1915 	movx	@dptr,a
      004988 90 02 A5         [24] 1916 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0002)
      00498B E4               [12] 1917 	clr	a
      00498C F0               [24] 1918 	movx	@dptr,a
      00498D 90 02 A6         [24] 1919 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0003)
      004990 F0               [24] 1920 	movx	@dptr,a
      004991 90 02 A7         [24] 1921 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0004)
      004994 F0               [24] 1922 	movx	@dptr,a
      004995 90 02 A8         [24] 1923 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0005)
      004998 F0               [24] 1924 	movx	@dptr,a
      004999 90 02 A9         [24] 1925 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0006)
      00499C F0               [24] 1926 	movx	@dptr,a
      00499D 90 02 AA         [24] 1927 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 + 0x0007)
      0049A0 04               [12] 1928 	inc	a
      0049A1 F0               [24] 1929 	movx	@dptr,a
                           0003AC  1930 	C$ublox_gnss.c$164$1$185 ==.
                                   1931 ;	..\COMMON\ublox_gnss.c:164: __xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
      0049A2 90 02 AB         [24] 1932 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196
      0049A5 74 F0            [12] 1933 	mov	a,#0xf0
      0049A7 F0               [24] 1934 	movx	@dptr,a
      0049A8 90 02 AC         [24] 1935 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0001)
      0049AB 74 04            [12] 1936 	mov	a,#0x04
      0049AD F0               [24] 1937 	movx	@dptr,a
      0049AE 90 02 AD         [24] 1938 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0002)
      0049B1 E4               [12] 1939 	clr	a
      0049B2 F0               [24] 1940 	movx	@dptr,a
      0049B3 90 02 AE         [24] 1941 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0003)
      0049B6 F0               [24] 1942 	movx	@dptr,a
      0049B7 90 02 AF         [24] 1943 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0004)
      0049BA F0               [24] 1944 	movx	@dptr,a
      0049BB 90 02 B0         [24] 1945 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0005)
      0049BE F0               [24] 1946 	movx	@dptr,a
      0049BF 90 02 B1         [24] 1947 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0006)
      0049C2 F0               [24] 1948 	movx	@dptr,a
      0049C3 90 02 B2         [24] 1949 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 + 0x0007)
      0049C6 04               [12] 1950 	inc	a
      0049C7 F0               [24] 1951 	movx	@dptr,a
                           0003D2  1952 	C$ublox_gnss.c$165$1$185 ==.
                                   1953 ;	..\COMMON\ublox_gnss.c:165: __xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
      0049C8 90 02 B3         [24] 1954 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196
      0049CB 74 FF            [12] 1955 	mov	a,#0xff
      0049CD F0               [24] 1956 	movx	@dptr,a
      0049CE 90 02 B4         [24] 1957 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0001)
      0049D1 F0               [24] 1958 	movx	@dptr,a
      0049D2 90 02 B5         [24] 1959 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0002)
      0049D5 74 06            [12] 1960 	mov	a,#0x06
      0049D7 F0               [24] 1961 	movx	@dptr,a
      0049D8 90 02 B6         [24] 1962 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0003)
      0049DB 03               [12] 1963 	rr	a
      0049DC F0               [24] 1964 	movx	@dptr,a
      0049DD 90 02 B7         [24] 1965 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0004)
      0049E0 E4               [12] 1966 	clr	a
      0049E1 F0               [24] 1967 	movx	@dptr,a
      0049E2 90 02 B8         [24] 1968 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0005)
      0049E5 F0               [24] 1969 	movx	@dptr,a
      0049E6 90 02 B9         [24] 1970 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0006)
      0049E9 F0               [24] 1971 	movx	@dptr,a
      0049EA 90 02 BA         [24] 1972 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0007)
      0049ED F0               [24] 1973 	movx	@dptr,a
      0049EE 90 02 BB         [24] 1974 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0008)
      0049F1 74 10            [12] 1975 	mov	a,#0x10
      0049F3 F0               [24] 1976 	movx	@dptr,a
      0049F4 90 02 BC         [24] 1977 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0009)
      0049F7 74 27            [12] 1978 	mov	a,#0x27
      0049F9 F0               [24] 1979 	movx	@dptr,a
      0049FA 90 02 BD         [24] 1980 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000a)
      0049FD E4               [12] 1981 	clr	a
      0049FE F0               [24] 1982 	movx	@dptr,a
      0049FF 90 02 BE         [24] 1983 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000b)
      004A02 F0               [24] 1984 	movx	@dptr,a
      004A03 90 02 BF         [24] 1985 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000c)
      004A06 74 05            [12] 1986 	mov	a,#0x05
      004A08 F0               [24] 1987 	movx	@dptr,a
      004A09 90 02 C0         [24] 1988 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000d)
      004A0C E4               [12] 1989 	clr	a
      004A0D F0               [24] 1990 	movx	@dptr,a
      004A0E 90 02 C1         [24] 1991 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000e)
      004A11 74 FA            [12] 1992 	mov	a,#0xfa
      004A13 F0               [24] 1993 	movx	@dptr,a
      004A14 90 02 C2         [24] 1994 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x000f)
      004A17 E4               [12] 1995 	clr	a
      004A18 F0               [24] 1996 	movx	@dptr,a
      004A19 90 02 C3         [24] 1997 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0010)
      004A1C 74 FA            [12] 1998 	mov	a,#0xfa
      004A1E F0               [24] 1999 	movx	@dptr,a
      004A1F 90 02 C4         [24] 2000 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0011)
      004A22 E4               [12] 2001 	clr	a
      004A23 F0               [24] 2002 	movx	@dptr,a
      004A24 90 02 C5         [24] 2003 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0012)
      004A27 74 64            [12] 2004 	mov	a,#0x64
      004A29 F0               [24] 2005 	movx	@dptr,a
      004A2A 90 02 C6         [24] 2006 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0013)
      004A2D E4               [12] 2007 	clr	a
      004A2E F0               [24] 2008 	movx	@dptr,a
      004A2F 90 02 C7         [24] 2009 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0014)
      004A32 74 2C            [12] 2010 	mov	a,#0x2c
      004A34 F0               [24] 2011 	movx	@dptr,a
      004A35 90 02 C8         [24] 2012 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0015)
      004A38 74 01            [12] 2013 	mov	a,#0x01
      004A3A F0               [24] 2014 	movx	@dptr,a
      004A3B 90 02 C9         [24] 2015 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0016)
      004A3E E4               [12] 2016 	clr	a
      004A3F F0               [24] 2017 	movx	@dptr,a
      004A40 90 02 CA         [24] 2018 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0017)
      004A43 F0               [24] 2019 	movx	@dptr,a
      004A44 90 02 CB         [24] 2020 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0018)
      004A47 F0               [24] 2021 	movx	@dptr,a
      004A48 90 02 CC         [24] 2022 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0019)
      004A4B F0               [24] 2023 	movx	@dptr,a
      004A4C 90 02 CD         [24] 2024 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001a)
      004A4F F0               [24] 2025 	movx	@dptr,a
      004A50 90 02 CE         [24] 2026 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001b)
      004A53 F0               [24] 2027 	movx	@dptr,a
      004A54 90 02 CF         [24] 2028 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001c)
      004A57 F0               [24] 2029 	movx	@dptr,a
      004A58 90 02 D0         [24] 2030 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001d)
      004A5B F0               [24] 2031 	movx	@dptr,a
      004A5C 90 02 D1         [24] 2032 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001e)
      004A5F F0               [24] 2033 	movx	@dptr,a
      004A60 90 02 D2         [24] 2034 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x001f)
      004A63 F0               [24] 2035 	movx	@dptr,a
      004A64 90 02 D3         [24] 2036 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0020)
      004A67 F0               [24] 2037 	movx	@dptr,a
      004A68 90 02 D4         [24] 2038 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0021)
      004A6B F0               [24] 2039 	movx	@dptr,a
      004A6C 90 02 D5         [24] 2040 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0022)
      004A6F F0               [24] 2041 	movx	@dptr,a
      004A70 90 02 D6         [24] 2042 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 + 0x0023)
      004A73 F0               [24] 2043 	movx	@dptr,a
                           00047E  2044 	C$ublox_gnss.c$168$1$196 ==.
                                   2045 ;	..\COMMON\ublox_gnss.c:168: PORTA_1 = 1;
      004A74 D2 81            [12] 2046 	setb	_PORTA_1
                           000480  2047 	C$ublox_gnss.c$169$1$196 ==.
                                   2048 ;	..\COMMON\ublox_gnss.c:169: uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
      004A76 E4               [12] 2049 	clr	a
      004A77 C0 E0            [24] 2050 	push	acc
      004A79 74 2D            [12] 2051 	mov	a,#0x2d
      004A7B C0 E0            [24] 2052 	push	acc
      004A7D 74 31            [12] 2053 	mov	a,#0x31
      004A7F C0 E0            [24] 2054 	push	acc
      004A81 74 01            [12] 2055 	mov	a,#0x01
      004A83 C0 E0            [24] 2056 	push	acc
      004A85 03               [12] 2057 	rr	a
      004A86 C0 E0            [24] 2058 	push	acc
      004A88 74 25            [12] 2059 	mov	a,#0x25
      004A8A C0 E0            [24] 2060 	push	acc
      004A8C E4               [12] 2061 	clr	a
      004A8D C0 E0            [24] 2062 	push	acc
      004A8F C0 E0            [24] 2063 	push	acc
      004A91 75 82 00         [24] 2064 	mov	dpl,#0x00
      004A94 12 5C 20         [24] 2065 	lcall	_uart_timer1_baud
      004A97 E5 81            [12] 2066 	mov	a,sp
      004A99 24 F8            [12] 2067 	add	a,#0xf8
      004A9B F5 81            [12] 2068 	mov	sp,a
                           0004A7  2069 	C$ublox_gnss.c$170$1$196 ==.
                                   2070 ;	..\COMMON\ublox_gnss.c:170: uart1_init(1, 8, 1);
      004A9D 75 64 08         [24] 2071 	mov	_uart1_init_PARM_2,#0x08
      004AA0 75 65 01         [24] 2072 	mov	_uart1_init_PARM_3,#0x01
      004AA3 75 82 01         [24] 2073 	mov	dpl,#0x01
      004AA6 12 56 2D         [24] 2074 	lcall	_uart1_init
                           0004B3  2075 	C$ublox_gnss.c$175$1$196 ==.
                                   2076 ;	..\COMMON\ublox_gnss.c:175: dbglink_writestr(" GPS init start\n ");
      004AA9 90 77 05         [24] 2077 	mov	dptr,#___str_2
      004AAC 75 F0 80         [24] 2078 	mov	b,#0x80
      004AAF 12 69 E1         [24] 2079 	lcall	_dbglink_writestr
                           0004BC  2080 	C$ublox_gnss.c$177$1$196 ==.
                                   2081 ;	..\COMMON\ublox_gnss.c:177: UBLOX_setNav1[1]=0x02;
      004AB2 90 02 A1         [24] 2082 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
      004AB5 74 02            [12] 2083 	mov	a,#0x02
      004AB7 F0               [24] 2084 	movx	@dptr,a
                           0004C2  2085 	C$ublox_gnss.c$178$1$196 ==.
                                   2086 ;	..\COMMON\ublox_gnss.c:178: do{
      004AB8                       2087 00101$:
                           0004C2  2088 	C$ublox_gnss.c$179$2$197 ==.
                                   2089 ;	..\COMMON\ublox_gnss.c:179: delay(5000);
      004AB8 90 13 88         [24] 2090 	mov	dptr,#0x1388
      004ABB 12 6B F2         [24] 2091 	lcall	_delay
                           0004C8  2092 	C$ublox_gnss.c$180$1$196 ==.
                                   2093 ;	..\COMMON\ublox_gnss.c:180: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      004ABE 75 29 A0         [24] 2094 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
      004AC1 75 2A 02         [24] 2095 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
      004AC4 75 2B 00         [24] 2096 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004AC7 75 26 01         [24] 2097 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004ACA 75 27 03         [24] 2098 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      004ACD 75 28 00         [24] 2099 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004AD0 75 2C 01         [24] 2100 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004AD3 E4               [12] 2101 	clr	a
      004AD4 F5 2D            [12] 2102 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004AD6 F5 2E            [12] 2103 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2104 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004AD8 F5 2F            [12] 2105 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004ADA 75 82 06         [24] 2106 	mov	dpl,#0x06
      004ADD 12 46 7B         [24] 2107 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004AE0 E5 82            [12] 2108 	mov	a,dpl
      004AE2 60 D4            [24] 2109 	jz	00101$
                           0004EE  2110 	C$ublox_gnss.c$182$1$196 ==.
                                   2111 ;	..\COMMON\ublox_gnss.c:182: dbglink_writestr(" GPS init 1\n ");
      004AE4 90 77 17         [24] 2112 	mov	dptr,#___str_3
      004AE7 75 F0 80         [24] 2113 	mov	b,#0x80
      004AEA 12 69 E1         [24] 2114 	lcall	_dbglink_writestr
                           0004F7  2115 	C$ublox_gnss.c$185$1$196 ==.
                                   2116 ;	..\COMMON\ublox_gnss.c:185: UBLOX_setNav1[1]=0x01;
      004AED 90 02 A1         [24] 2117 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
      004AF0 74 01            [12] 2118 	mov	a,#0x01
      004AF2 F0               [24] 2119 	movx	@dptr,a
                           0004FD  2120 	C$ublox_gnss.c$186$1$196 ==.
                                   2121 ;	..\COMMON\ublox_gnss.c:186: do{
      004AF3                       2122 00104$:
                           0004FD  2123 	C$ublox_gnss.c$187$2$198 ==.
                                   2124 ;	..\COMMON\ublox_gnss.c:187: delay(5000);
      004AF3 90 13 88         [24] 2125 	mov	dptr,#0x1388
      004AF6 12 6B F2         [24] 2126 	lcall	_delay
                           000503  2127 	C$ublox_gnss.c$188$1$196 ==.
                                   2128 ;	..\COMMON\ublox_gnss.c:188: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      004AF9 75 29 A0         [24] 2129 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
      004AFC 75 2A 02         [24] 2130 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
      004AFF 75 2B 00         [24] 2131 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004B02 75 26 01         [24] 2132 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004B05 75 27 03         [24] 2133 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      004B08 75 28 00         [24] 2134 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004B0B 75 2C 01         [24] 2135 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004B0E E4               [12] 2136 	clr	a
      004B0F F5 2D            [12] 2137 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004B11 F5 2E            [12] 2138 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2139 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004B13 F5 2F            [12] 2140 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004B15 75 82 06         [24] 2141 	mov	dpl,#0x06
      004B18 12 46 7B         [24] 2142 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004B1B E5 82            [12] 2143 	mov	a,dpl
      004B1D 60 D4            [24] 2144 	jz	00104$
                           000529  2145 	C$ublox_gnss.c$191$1$196 ==.
                                   2146 ;	..\COMMON\ublox_gnss.c:191: dbglink_writestr(" GPS init 2\n ");
      004B1F 90 77 25         [24] 2147 	mov	dptr,#___str_4
      004B22 75 F0 80         [24] 2148 	mov	b,#0x80
      004B25 12 69 E1         [24] 2149 	lcall	_dbglink_writestr
                           000532  2150 	C$ublox_gnss.c$194$1$196 ==.
                                   2151 ;	..\COMMON\ublox_gnss.c:194: UBLOX_setNav1[1]=0x00;
      004B28 90 02 A1         [24] 2152 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
      004B2B E4               [12] 2153 	clr	a
      004B2C F0               [24] 2154 	movx	@dptr,a
                           000537  2155 	C$ublox_gnss.c$195$1$196 ==.
                                   2156 ;	..\COMMON\ublox_gnss.c:195: do{
      004B2D                       2157 00107$:
                           000537  2158 	C$ublox_gnss.c$196$2$199 ==.
                                   2159 ;	..\COMMON\ublox_gnss.c:196: delay(5000);
      004B2D 90 13 88         [24] 2160 	mov	dptr,#0x1388
      004B30 12 6B F2         [24] 2161 	lcall	_delay
                           00053D  2162 	C$ublox_gnss.c$197$1$196 ==.
                                   2163 ;	..\COMMON\ublox_gnss.c:197: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      004B33 75 29 A0         [24] 2164 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
      004B36 75 2A 02         [24] 2165 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
      004B39 75 2B 00         [24] 2166 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004B3C 75 26 01         [24] 2167 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004B3F 75 27 03         [24] 2168 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      004B42 75 28 00         [24] 2169 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004B45 75 2C 01         [24] 2170 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004B48 E4               [12] 2171 	clr	a
      004B49 F5 2D            [12] 2172 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004B4B F5 2E            [12] 2173 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2174 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004B4D F5 2F            [12] 2175 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004B4F 75 82 06         [24] 2176 	mov	dpl,#0x06
      004B52 12 46 7B         [24] 2177 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004B55 E5 82            [12] 2178 	mov	a,dpl
      004B57 60 D4            [24] 2179 	jz	00107$
                           000563  2180 	C$ublox_gnss.c$200$1$196 ==.
                                   2181 ;	..\COMMON\ublox_gnss.c:200: dbglink_writestr(" GPS init 3\n ");
      004B59 90 77 33         [24] 2182 	mov	dptr,#___str_5
      004B5C 75 F0 80         [24] 2183 	mov	b,#0x80
      004B5F 12 69 E1         [24] 2184 	lcall	_dbglink_writestr
                           00056C  2185 	C$ublox_gnss.c$203$1$196 ==.
                                   2186 ;	..\COMMON\ublox_gnss.c:203: UBLOX_setNav1[1]=0x03;
      004B62 90 02 A1         [24] 2187 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
      004B65 74 03            [12] 2188 	mov	a,#0x03
      004B67 F0               [24] 2189 	movx	@dptr,a
                           000572  2190 	C$ublox_gnss.c$204$1$196 ==.
                                   2191 ;	..\COMMON\ublox_gnss.c:204: do{
      004B68                       2192 00110$:
                           000572  2193 	C$ublox_gnss.c$205$2$200 ==.
                                   2194 ;	..\COMMON\ublox_gnss.c:205: delay(5000);
      004B68 90 13 88         [24] 2195 	mov	dptr,#0x1388
      004B6B 12 6B F2         [24] 2196 	lcall	_delay
                           000578  2197 	C$ublox_gnss.c$206$1$196 ==.
                                   2198 ;	..\COMMON\ublox_gnss.c:206: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      004B6E 75 29 A0         [24] 2199 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
      004B71 75 2A 02         [24] 2200 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
      004B74 75 2B 00         [24] 2201 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004B77 75 26 01         [24] 2202 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004B7A 75 27 03         [24] 2203 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      004B7D 75 28 00         [24] 2204 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004B80 75 2C 01         [24] 2205 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004B83 E4               [12] 2206 	clr	a
      004B84 F5 2D            [12] 2207 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004B86 F5 2E            [12] 2208 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2209 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004B88 F5 2F            [12] 2210 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004B8A 75 82 06         [24] 2211 	mov	dpl,#0x06
      004B8D 12 46 7B         [24] 2212 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004B90 E5 82            [12] 2213 	mov	a,dpl
      004B92 60 D4            [24] 2214 	jz	00110$
                           00059E  2215 	C$ublox_gnss.c$209$1$196 ==.
                                   2216 ;	..\COMMON\ublox_gnss.c:209: dbglink_writestr(" GPS init 4\n ");
      004B94 90 77 41         [24] 2217 	mov	dptr,#___str_6
      004B97 75 F0 80         [24] 2218 	mov	b,#0x80
      004B9A 12 69 E1         [24] 2219 	lcall	_dbglink_writestr
                           0005A7  2220 	C$ublox_gnss.c$212$1$196 ==.
                                   2221 ;	..\COMMON\ublox_gnss.c:212: UBLOX_setNav1[1]=0x05;
      004B9D 90 02 A1         [24] 2222 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 + 0x0001)
      004BA0 74 05            [12] 2223 	mov	a,#0x05
      004BA2 F0               [24] 2224 	movx	@dptr,a
                           0005AD  2225 	C$ublox_gnss.c$213$1$196 ==.
                                   2226 ;	..\COMMON\ublox_gnss.c:213: do{
      004BA3                       2227 00113$:
                           0005AD  2228 	C$ublox_gnss.c$214$2$201 ==.
                                   2229 ;	..\COMMON\ublox_gnss.c:214: delay(5000);
      004BA3 90 13 88         [24] 2230 	mov	dptr,#0x1388
      004BA6 12 6B F2         [24] 2231 	lcall	_delay
                           0005B3  2232 	C$ublox_gnss.c$215$1$196 ==.
                                   2233 ;	..\COMMON\ublox_gnss.c:215: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      004BA9 75 29 A0         [24] 2234 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196
      004BAC 75 2A 02         [24] 2235 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_196 >> 8)
      004BAF 75 2B 00         [24] 2236 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004BB2 75 26 01         [24] 2237 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004BB5 75 27 03         [24] 2238 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      004BB8 75 28 00         [24] 2239 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004BBB 75 2C 01         [24] 2240 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004BBE E4               [12] 2241 	clr	a
      004BBF F5 2D            [12] 2242 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004BC1 F5 2E            [12] 2243 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2244 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004BC3 F5 2F            [12] 2245 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004BC5 75 82 06         [24] 2246 	mov	dpl,#0x06
      004BC8 12 46 7B         [24] 2247 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004BCB E5 82            [12] 2248 	mov	a,dpl
      004BCD 60 D4            [24] 2249 	jz	00113$
                           0005D9  2250 	C$ublox_gnss.c$217$1$196 ==.
                                   2251 ;	..\COMMON\ublox_gnss.c:217: do{
      004BCF                       2252 00116$:
                           0005D9  2253 	C$ublox_gnss.c$218$2$202 ==.
                                   2254 ;	..\COMMON\ublox_gnss.c:218: delay(5000);
      004BCF 90 13 88         [24] 2255 	mov	dptr,#0x1388
      004BD2 12 6B F2         [24] 2256 	lcall	_delay
                           0005DF  2257 	C$ublox_gnss.c$219$1$196 ==.
                                   2258 ;	..\COMMON\ublox_gnss.c:219: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav2,ACK,NULL));
      004BD5 75 29 A3         [24] 2259 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196
      004BD8 75 2A 02         [24] 2260 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_196 >> 8)
      004BDB 75 2B 00         [24] 2261 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004BDE 75 26 01         [24] 2262 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004BE1 75 27 08         [24] 2263 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x08
      004BE4 75 28 00         [24] 2264 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004BE7 75 2C 01         [24] 2265 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004BEA E4               [12] 2266 	clr	a
      004BEB F5 2D            [12] 2267 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004BED F5 2E            [12] 2268 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2269 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004BEF F5 2F            [12] 2270 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004BF1 75 82 06         [24] 2271 	mov	dpl,#0x06
      004BF4 12 46 7B         [24] 2272 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004BF7 E5 82            [12] 2273 	mov	a,dpl
      004BF9 60 D4            [24] 2274 	jz	00116$
                           000605  2275 	C$ublox_gnss.c$221$1$196 ==.
                                   2276 ;	..\COMMON\ublox_gnss.c:221: do{
      004BFB                       2277 00119$:
                           000605  2278 	C$ublox_gnss.c$222$2$203 ==.
                                   2279 ;	..\COMMON\ublox_gnss.c:222: delay(5000);
      004BFB 90 13 88         [24] 2280 	mov	dptr,#0x1388
      004BFE 12 6B F2         [24] 2281 	lcall	_delay
                           00060B  2282 	C$ublox_gnss.c$223$1$196 ==.
                                   2283 ;	..\COMMON\ublox_gnss.c:223: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav3,ACK,NULL));
      004C01 75 29 AB         [24] 2284 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196
      004C04 75 2A 02         [24] 2285 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_196 >> 8)
      004C07 75 2B 00         [24] 2286 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004C0A 75 26 01         [24] 2287 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      004C0D 75 27 08         [24] 2288 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x08
      004C10 75 28 00         [24] 2289 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004C13 75 2C 01         [24] 2290 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004C16 E4               [12] 2291 	clr	a
      004C17 F5 2D            [12] 2292 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004C19 F5 2E            [12] 2293 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2294 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004C1B F5 2F            [12] 2295 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004C1D 75 82 06         [24] 2296 	mov	dpl,#0x06
      004C20 12 46 7B         [24] 2297 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004C23 E5 82            [12] 2298 	mov	a,dpl
      004C25 60 D4            [24] 2299 	jz	00119$
                           000631  2300 	C$ublox_gnss.c$225$1$196 ==.
                                   2301 ;	..\COMMON\ublox_gnss.c:225: do{
      004C27                       2302 00122$:
                           000631  2303 	C$ublox_gnss.c$226$2$204 ==.
                                   2304 ;	..\COMMON\ublox_gnss.c:226: delay(5000);
      004C27 90 13 88         [24] 2305 	mov	dptr,#0x1388
      004C2A 12 6B F2         [24] 2306 	lcall	_delay
                           000637  2307 	C$ublox_gnss.c$227$1$196 ==.
                                   2308 ;	..\COMMON\ublox_gnss.c:227: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x24,sizeof(UBLOX_setNav5),UBLOX_setNav5,ACK,NULL));
      004C2D 75 29 B3         [24] 2309 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196
      004C30 75 2A 02         [24] 2310 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_196 >> 8)
      004C33 75 2B 00         [24] 2311 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      004C36 75 26 24         [24] 2312 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x24
      004C39 75 27 24         [24] 2313 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x24
      004C3C 75 28 00         [24] 2314 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      004C3F 75 2C 01         [24] 2315 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      004C42 E4               [12] 2316 	clr	a
      004C43 F5 2D            [12] 2317 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      004C45 F5 2E            [12] 2318 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2319 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      004C47 F5 2F            [12] 2320 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      004C49 75 82 06         [24] 2321 	mov	dpl,#0x06
      004C4C 12 46 7B         [24] 2322 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      004C4F E5 82            [12] 2323 	mov	a,dpl
      004C51 60 D4            [24] 2324 	jz	00122$
                           00065D  2325 	C$ublox_gnss.c$229$1$196 ==.
                                   2326 ;	..\COMMON\ublox_gnss.c:229: dbglink_writestr("GPS init END\n");
      004C53 90 77 4F         [24] 2327 	mov	dptr,#___str_7
      004C56 75 F0 80         [24] 2328 	mov	b,#0x80
      004C59 12 69 E1         [24] 2329 	lcall	_dbglink_writestr
                           000666  2330 	C$ublox_gnss.c$231$1$196 ==.
                           000666  2331 	XG$UBLOX_GPS_PortInit$0$0 ==.
      004C5C 22               [24] 2332 	ret
                                   2333 	.area CSEG    (CODE)
                                   2334 	.area CONST   (CODE)
                           000000  2335 Fublox_gnss$__str_0$0$0 == .
      0076EE                       2336 ___str_0:
      0076EE 63 6F 6D 6D 61 6E 64  2337 	.ascii "command sent"
             20 73 65 6E 74
      0076FA 0A                    2338 	.db 0x0a
      0076FB 00                    2339 	.db 0x00
                           00000E  2340 Fublox_gnss$__str_1$0$0 == .
      0076FC                       2341 ___str_1:
      0076FC 6D 73 67 20 72 63 76  2342 	.ascii "msg rcv"
      007703 0A                    2343 	.db 0x0a
      007704 00                    2344 	.db 0x00
                           000017  2345 Fublox_gnss$__str_2$0$0 == .
      007705                       2346 ___str_2:
      007705 20 47 50 53 20 69 6E  2347 	.ascii " GPS init start"
             69 74 20 73 74 61 72
             74
      007714 0A                    2348 	.db 0x0a
      007715 20                    2349 	.ascii " "
      007716 00                    2350 	.db 0x00
                           000029  2351 Fublox_gnss$__str_3$0$0 == .
      007717                       2352 ___str_3:
      007717 20 47 50 53 20 69 6E  2353 	.ascii " GPS init 1"
             69 74 20 31
      007722 0A                    2354 	.db 0x0a
      007723 20                    2355 	.ascii " "
      007724 00                    2356 	.db 0x00
                           000037  2357 Fublox_gnss$__str_4$0$0 == .
      007725                       2358 ___str_4:
      007725 20 47 50 53 20 69 6E  2359 	.ascii " GPS init 2"
             69 74 20 32
      007730 0A                    2360 	.db 0x0a
      007731 20                    2361 	.ascii " "
      007732 00                    2362 	.db 0x00
                           000045  2363 Fublox_gnss$__str_5$0$0 == .
      007733                       2364 ___str_5:
      007733 20 47 50 53 20 69 6E  2365 	.ascii " GPS init 3"
             69 74 20 33
      00773E 0A                    2366 	.db 0x0a
      00773F 20                    2367 	.ascii " "
      007740 00                    2368 	.db 0x00
                           000053  2369 Fublox_gnss$__str_6$0$0 == .
      007741                       2370 ___str_6:
      007741 20 47 50 53 20 69 6E  2371 	.ascii " GPS init 4"
             69 74 20 34
      00774C 0A                    2372 	.db 0x0a
      00774D 20                    2373 	.ascii " "
      00774E 00                    2374 	.db 0x00
                           000061  2375 Fublox_gnss$__str_7$0$0 == .
      00774F                       2376 ___str_7:
      00774F 47 50 53 20 69 6E 69  2377 	.ascii "GPS init END"
             74 20 45 4E 44
      00775B 0A                    2378 	.db 0x0a
      00775C 00                    2379 	.db 0x00
                                   2380 	.area XINIT   (CODE)
                                   2381 	.area CABS    (ABS,CODE)
