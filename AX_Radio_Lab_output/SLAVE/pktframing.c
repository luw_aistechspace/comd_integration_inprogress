// libraries
// from main.c
#include "../AX_Radio_Lab_output/configslave.h"
#include <ax8052.h>
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include "libmfosc.h"
#ifdef USE_COM0
#include <libmfuart0.h>
#endif /* USE_COM0 */
#ifdef USE_LCD
#include <libaxlcd2.h>
#endif /* USE_LCD */
#ifdef USE_DBGLINK
#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#endif /* USE_DBGLINK */
#define ADJUST_FREQREG
#define FREQOFFS_K 4//2//3
#if defined(USE_LCD) || defined(USE_COM0)
#define USE_DISPLAY
#endif /* defined(USE_LCD) || defined(USE_COM0) */
#include "../COMMON/display_com0.h"
#include <libminikitleds.h>
/*#include <stdlib.h> */
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"
#include "../COMMON/easyax5043.h"

#include <string.h>
#include <ax8052crypto.h>
#include <ax8052cryptoregaddr.h>
#include <libmfcrypto.h>

#include "../COMMON/ublox_gnss.h"
// macros

#define nRANDOM 3
#define rangeRANDOM 30
#define slotBASE 5
#define slotWIDTH_MS 80
#define pktLENGTH 45//32 - (3-nRANDOM)
#define pktBufferLENGTH nRANDOM * pktLENGTH
#define rngDELAY_MS 10

#define USE_DBGLINK

const uint32_t __xdata trmID = 0x100003;
const uint32_t __xdata dummyHMAC = 0xC1C2C3C4;

// global variables
uint8_t __xdata randomSet[nRANDOM];
uint8_t __xdata siblingSlots[nRANDOM - 1];
uint8_t __xdata demoPacketBuffer[pktBufferLENGTH];

struct wtimer_desc __xdata msg1_tmr;
struct wtimer_desc __xdata msg2_tmr;
struct wtimer_desc __xdata msg3_tmr;

// external variables
extern uint16_t __xdata pkt_counter;
extern uint8_t __xdata *payload;
extern uint16_t __xdata payload_len;

// functions
void swap_variables(uint8_t *a, uint8_t *b);
void randomset_generate(void);
//void simple_framing(void);
void msg1_callback(struct wtimer_desc __xdata *desc);
void msg2_callback(struct wtimer_desc __xdata *desc);
void msg3_callback(struct wtimer_desc __xdata *desc);
void transmit_copies_wtimer(void);
//void simple_transmit(void);
void packetFraming(void);

// external functions
extern __xdata uint32_t GOLDSEQUENCE_Obtain(void);

// code

/**
* \brief compare and swap (small to big)
*/
void swap_variables(uint8_t *a, uint8_t *b)
{
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
}
/*
 * Generate a set of true random numbers
 */
void randomset_generate(void)
{
//     uint8_t i, j;
//     uint16_t r_tmp;

//     delay_ms(rngDELAY_MS);
    randomSet[0] = RNGBYTE % rangeRANDOM + slotBASE;
    delay_ms(rngDELAY_MS);
    randomSet[1] = RNGBYTE % rangeRANDOM + slotBASE;
    delay_ms(rngDELAY_MS);
    randomSet[2] = RNGBYTE % rangeRANDOM + slotBASE;

    while(randomSet[0] == randomSet[1])
    {
        delay_ms(rngDELAY_MS);
        randomSet[1] = RNGBYTE % rangeRANDOM + slotBASE;
    }
    while ((randomSet[1] == randomSet[2]) || (randomSet[0] == randomSet[2]))
    {
        delay_ms(rngDELAY_MS);
        randomSet[2] = RNGBYTE % rangeRANDOM + slotBASE;
    }

// compare and swap
    if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
    if (randomSet[1] > randomSet[2]) swap_variables(&randomSet[1], &randomSet[2]);
    if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
#ifdef USE_DBGLINK
            dbglink_writestr("RNG done: ");
            dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
            dbglink_tx('\n');
#endif // USE_DBGLINK
}
/****************************************/
/*
 *
 *
 *
 */
 void packetFraming(void)
 {
     uint8_t __xdata i = 0, j = 0, n = 0;
     uint32_t __xdata goldseq = 0;
     uint8_t __xdata StdPrem = 0x1;
     uint8_t __xdata sib1 = 0, sib2 = 0;
     uint8_t __xdata ack = 0;
     uint32_t __xdata CRC32;


     goldseq = GOLDSEQUENCE_Obtain();

     randomset_generate();

     for (n = 0; n < 3; n++)
     {
#ifdef USE_DBGLINK
         dbglink_writenum16(n, 1, WRNUM_PADZERO);
         dbglink_tx(' ');
#endif // USE_DBGLINK
         switch(n)
         {
         case 0:
            sib1 = randomSet[1];
            sib2 = randomSet[2];
            break;

         case 1:
            sib1 = randomSet[0];
            sib2 = randomSet[2];
            break;

         case 2:
            sib1 = randomSet[0];
            sib2 = randomSet[1];
            break;

         default:
            break;
         }
#ifdef USE_DBGLINK
        dbglink_writehex16(sib1, 2, WRNUM_PADZERO);
        dbglink_writehex16(sib2, 2, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK

    *(demoPacketBuffer + (n*pktLENGTH)) = (uint8_t) (goldseq & 0x000000FF);
/*#ifdef USE_DBGLINK
        dbglink_writehex16(demoPacketBuffer[n*pktLENGTH], 2, WRNUM_PADZERO);
       // dbglink_tx('\n');
#endif // USE_DBGLINK*/
    *(demoPacketBuffer + (n*pktLENGTH + 1)) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
    *(demoPacketBuffer + (n*pktLENGTH + 2)) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
    *(demoPacketBuffer + (n*pktLENGTH + 3)) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (sib1 & 0x01) /*<< 7*/);
    *(demoPacketBuffer + (n*pktLENGTH + 4)) = (uint8_t) (sib1 & 0x3E)>>1 | ((sib2 & 0x07)<<5); // changed 0xFE to 0x3E
    *(demoPacketBuffer + (n*pktLENGTH + 5)) = (uint8_t) (sib2 & 0x38)>>3 | (trmID & 0x00001F)<<3;
/*#ifdef USE_DBGLINK
        dbglink_writehex16(demoPacketBuffer[n*pktLENGTH+5], 2, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK */
    *(demoPacketBuffer + (n*pktLENGTH + 6)) = (uint8_t) ((trmID & 0x001FE0) >> 5);
    *(demoPacketBuffer + (n*pktLENGTH + 7)) = (uint8_t) ((trmID & 0x1FE000) >> 13);
    *(demoPacketBuffer + (n*pktLENGTH + 8)) = (uint8_t) ((trmID & 0xE00000) >> 21) | (StdPrem & 0x0001) << 4 | (ack << 5 & 0x0001); // why <<5 why 0x0001?

    memcpy((demoPacketBuffer +  n*pktLENGTH + 9), &dummyHMAC, sizeof(uint32_t));
    memcpy((demoPacketBuffer +  n*pktLENGTH + 13), payload, sizeof(uint8_t)*payload_len);

    CRC32 = crc_crc32_msb((demoPacketBuffer+ n*pktLENGTH), (13+payload_len), 0xFFFFFFFF);
    memcpy((demoPacketBuffer+n*pktLENGTH+13+payload_len), &CRC32, sizeof(uint32_t));

/*#ifdef USE_DBGLINK
    for (i = 0; i < pktLENGTH; i++)
    {
        dbglink_writehex16(demoPacketBuffer[i + n*pktLENGTH], 2, WRNUM_PADZERO);
        dbglink_tx(' ');
    }
    dbglink_tx('\n');
#endif // USE_DBGLINK */

    }

 }
