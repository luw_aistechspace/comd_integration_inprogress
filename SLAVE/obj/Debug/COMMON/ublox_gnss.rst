                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module ublox_gnss
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_4
                                     12 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_3
                                     13 	.globl _UBLOX_GPS_FletcherChecksum8_PARM_2
                                     14 	.globl _aligned_alloc_PARM_2
                                     15 	.globl _UBLOX_payload_POSLLH
                                     16 	.globl _delay_ms
                                     17 	.globl _memcpy
                                     18 	.globl _free
                                     19 	.globl _malloc
                                     20 	.globl _uart1_tx
                                     21 	.globl _uart1_rx
                                     22 	.globl _uart1_init
                                     23 	.globl _uart1_rxcount
                                     24 	.globl _uart_timer1_baud
                                     25 	.globl _delay
                                     26 	.globl _PORTC_7
                                     27 	.globl _PORTC_6
                                     28 	.globl _PORTC_5
                                     29 	.globl _PORTC_4
                                     30 	.globl _PORTC_3
                                     31 	.globl _PORTC_2
                                     32 	.globl _PORTC_1
                                     33 	.globl _PORTC_0
                                     34 	.globl _PORTB_7
                                     35 	.globl _PORTB_6
                                     36 	.globl _PORTB_5
                                     37 	.globl _PORTB_4
                                     38 	.globl _PORTB_3
                                     39 	.globl _PORTB_2
                                     40 	.globl _PORTB_1
                                     41 	.globl _PORTB_0
                                     42 	.globl _PORTA_7
                                     43 	.globl _PORTA_6
                                     44 	.globl _PORTA_5
                                     45 	.globl _PORTA_4
                                     46 	.globl _PORTA_3
                                     47 	.globl _PORTA_2
                                     48 	.globl _PORTA_1
                                     49 	.globl _PORTA_0
                                     50 	.globl _PINC_7
                                     51 	.globl _PINC_6
                                     52 	.globl _PINC_5
                                     53 	.globl _PINC_4
                                     54 	.globl _PINC_3
                                     55 	.globl _PINC_2
                                     56 	.globl _PINC_1
                                     57 	.globl _PINC_0
                                     58 	.globl _PINB_7
                                     59 	.globl _PINB_6
                                     60 	.globl _PINB_5
                                     61 	.globl _PINB_4
                                     62 	.globl _PINB_3
                                     63 	.globl _PINB_2
                                     64 	.globl _PINB_1
                                     65 	.globl _PINB_0
                                     66 	.globl _PINA_7
                                     67 	.globl _PINA_6
                                     68 	.globl _PINA_5
                                     69 	.globl _PINA_4
                                     70 	.globl _PINA_3
                                     71 	.globl _PINA_2
                                     72 	.globl _PINA_1
                                     73 	.globl _PINA_0
                                     74 	.globl _CY
                                     75 	.globl _AC
                                     76 	.globl _F0
                                     77 	.globl _RS1
                                     78 	.globl _RS0
                                     79 	.globl _OV
                                     80 	.globl _F1
                                     81 	.globl _P
                                     82 	.globl _IP_7
                                     83 	.globl _IP_6
                                     84 	.globl _IP_5
                                     85 	.globl _IP_4
                                     86 	.globl _IP_3
                                     87 	.globl _IP_2
                                     88 	.globl _IP_1
                                     89 	.globl _IP_0
                                     90 	.globl _EA
                                     91 	.globl _IE_7
                                     92 	.globl _IE_6
                                     93 	.globl _IE_5
                                     94 	.globl _IE_4
                                     95 	.globl _IE_3
                                     96 	.globl _IE_2
                                     97 	.globl _IE_1
                                     98 	.globl _IE_0
                                     99 	.globl _EIP_7
                                    100 	.globl _EIP_6
                                    101 	.globl _EIP_5
                                    102 	.globl _EIP_4
                                    103 	.globl _EIP_3
                                    104 	.globl _EIP_2
                                    105 	.globl _EIP_1
                                    106 	.globl _EIP_0
                                    107 	.globl _EIE_7
                                    108 	.globl _EIE_6
                                    109 	.globl _EIE_5
                                    110 	.globl _EIE_4
                                    111 	.globl _EIE_3
                                    112 	.globl _EIE_2
                                    113 	.globl _EIE_1
                                    114 	.globl _EIE_0
                                    115 	.globl _E2IP_7
                                    116 	.globl _E2IP_6
                                    117 	.globl _E2IP_5
                                    118 	.globl _E2IP_4
                                    119 	.globl _E2IP_3
                                    120 	.globl _E2IP_2
                                    121 	.globl _E2IP_1
                                    122 	.globl _E2IP_0
                                    123 	.globl _E2IE_7
                                    124 	.globl _E2IE_6
                                    125 	.globl _E2IE_5
                                    126 	.globl _E2IE_4
                                    127 	.globl _E2IE_3
                                    128 	.globl _E2IE_2
                                    129 	.globl _E2IE_1
                                    130 	.globl _E2IE_0
                                    131 	.globl _B_7
                                    132 	.globl _B_6
                                    133 	.globl _B_5
                                    134 	.globl _B_4
                                    135 	.globl _B_3
                                    136 	.globl _B_2
                                    137 	.globl _B_1
                                    138 	.globl _B_0
                                    139 	.globl _ACC_7
                                    140 	.globl _ACC_6
                                    141 	.globl _ACC_5
                                    142 	.globl _ACC_4
                                    143 	.globl _ACC_3
                                    144 	.globl _ACC_2
                                    145 	.globl _ACC_1
                                    146 	.globl _ACC_0
                                    147 	.globl _WTSTAT
                                    148 	.globl _WTIRQEN
                                    149 	.globl _WTEVTD
                                    150 	.globl _WTEVTD1
                                    151 	.globl _WTEVTD0
                                    152 	.globl _WTEVTC
                                    153 	.globl _WTEVTC1
                                    154 	.globl _WTEVTC0
                                    155 	.globl _WTEVTB
                                    156 	.globl _WTEVTB1
                                    157 	.globl _WTEVTB0
                                    158 	.globl _WTEVTA
                                    159 	.globl _WTEVTA1
                                    160 	.globl _WTEVTA0
                                    161 	.globl _WTCNTR1
                                    162 	.globl _WTCNTB
                                    163 	.globl _WTCNTB1
                                    164 	.globl _WTCNTB0
                                    165 	.globl _WTCNTA
                                    166 	.globl _WTCNTA1
                                    167 	.globl _WTCNTA0
                                    168 	.globl _WTCFGB
                                    169 	.globl _WTCFGA
                                    170 	.globl _WDTRESET
                                    171 	.globl _WDTCFG
                                    172 	.globl _U1STATUS
                                    173 	.globl _U1SHREG
                                    174 	.globl _U1MODE
                                    175 	.globl _U1CTRL
                                    176 	.globl _U0STATUS
                                    177 	.globl _U0SHREG
                                    178 	.globl _U0MODE
                                    179 	.globl _U0CTRL
                                    180 	.globl _T2STATUS
                                    181 	.globl _T2PERIOD
                                    182 	.globl _T2PERIOD1
                                    183 	.globl _T2PERIOD0
                                    184 	.globl _T2MODE
                                    185 	.globl _T2CNT
                                    186 	.globl _T2CNT1
                                    187 	.globl _T2CNT0
                                    188 	.globl _T2CLKSRC
                                    189 	.globl _T1STATUS
                                    190 	.globl _T1PERIOD
                                    191 	.globl _T1PERIOD1
                                    192 	.globl _T1PERIOD0
                                    193 	.globl _T1MODE
                                    194 	.globl _T1CNT
                                    195 	.globl _T1CNT1
                                    196 	.globl _T1CNT0
                                    197 	.globl _T1CLKSRC
                                    198 	.globl _T0STATUS
                                    199 	.globl _T0PERIOD
                                    200 	.globl _T0PERIOD1
                                    201 	.globl _T0PERIOD0
                                    202 	.globl _T0MODE
                                    203 	.globl _T0CNT
                                    204 	.globl _T0CNT1
                                    205 	.globl _T0CNT0
                                    206 	.globl _T0CLKSRC
                                    207 	.globl _SPSTATUS
                                    208 	.globl _SPSHREG
                                    209 	.globl _SPMODE
                                    210 	.globl _SPCLKSRC
                                    211 	.globl _RADIOSTAT
                                    212 	.globl _RADIOSTAT1
                                    213 	.globl _RADIOSTAT0
                                    214 	.globl _RADIODATA
                                    215 	.globl _RADIODATA3
                                    216 	.globl _RADIODATA2
                                    217 	.globl _RADIODATA1
                                    218 	.globl _RADIODATA0
                                    219 	.globl _RADIOADDR
                                    220 	.globl _RADIOADDR1
                                    221 	.globl _RADIOADDR0
                                    222 	.globl _RADIOACC
                                    223 	.globl _OC1STATUS
                                    224 	.globl _OC1PIN
                                    225 	.globl _OC1MODE
                                    226 	.globl _OC1COMP
                                    227 	.globl _OC1COMP1
                                    228 	.globl _OC1COMP0
                                    229 	.globl _OC0STATUS
                                    230 	.globl _OC0PIN
                                    231 	.globl _OC0MODE
                                    232 	.globl _OC0COMP
                                    233 	.globl _OC0COMP1
                                    234 	.globl _OC0COMP0
                                    235 	.globl _NVSTATUS
                                    236 	.globl _NVKEY
                                    237 	.globl _NVDATA
                                    238 	.globl _NVDATA1
                                    239 	.globl _NVDATA0
                                    240 	.globl _NVADDR
                                    241 	.globl _NVADDR1
                                    242 	.globl _NVADDR0
                                    243 	.globl _IC1STATUS
                                    244 	.globl _IC1MODE
                                    245 	.globl _IC1CAPT
                                    246 	.globl _IC1CAPT1
                                    247 	.globl _IC1CAPT0
                                    248 	.globl _IC0STATUS
                                    249 	.globl _IC0MODE
                                    250 	.globl _IC0CAPT
                                    251 	.globl _IC0CAPT1
                                    252 	.globl _IC0CAPT0
                                    253 	.globl _PORTR
                                    254 	.globl _PORTC
                                    255 	.globl _PORTB
                                    256 	.globl _PORTA
                                    257 	.globl _PINR
                                    258 	.globl _PINC
                                    259 	.globl _PINB
                                    260 	.globl _PINA
                                    261 	.globl _DIRR
                                    262 	.globl _DIRC
                                    263 	.globl _DIRB
                                    264 	.globl _DIRA
                                    265 	.globl _DBGLNKSTAT
                                    266 	.globl _DBGLNKBUF
                                    267 	.globl _CODECONFIG
                                    268 	.globl _CLKSTAT
                                    269 	.globl _CLKCON
                                    270 	.globl _ANALOGCOMP
                                    271 	.globl _ADCCONV
                                    272 	.globl _ADCCLKSRC
                                    273 	.globl _ADCCH3CONFIG
                                    274 	.globl _ADCCH2CONFIG
                                    275 	.globl _ADCCH1CONFIG
                                    276 	.globl _ADCCH0CONFIG
                                    277 	.globl __XPAGE
                                    278 	.globl _XPAGE
                                    279 	.globl _SP
                                    280 	.globl _PSW
                                    281 	.globl _PCON
                                    282 	.globl _IP
                                    283 	.globl _IE
                                    284 	.globl _EIP
                                    285 	.globl _EIE
                                    286 	.globl _E2IP
                                    287 	.globl _E2IE
                                    288 	.globl _DPS
                                    289 	.globl _DPTR1
                                    290 	.globl _DPTR0
                                    291 	.globl _DPL1
                                    292 	.globl _DPL
                                    293 	.globl _DPH1
                                    294 	.globl _DPH
                                    295 	.globl _B
                                    296 	.globl _ACC
                                    297 	.globl _XTALREADY
                                    298 	.globl _XTALOSC
                                    299 	.globl _XTALAMPL
                                    300 	.globl _SILICONREV
                                    301 	.globl _SCRATCH3
                                    302 	.globl _SCRATCH2
                                    303 	.globl _SCRATCH1
                                    304 	.globl _SCRATCH0
                                    305 	.globl _RADIOMUX
                                    306 	.globl _RADIOFSTATADDR
                                    307 	.globl _RADIOFSTATADDR1
                                    308 	.globl _RADIOFSTATADDR0
                                    309 	.globl _RADIOFDATAADDR
                                    310 	.globl _RADIOFDATAADDR1
                                    311 	.globl _RADIOFDATAADDR0
                                    312 	.globl _OSCRUN
                                    313 	.globl _OSCREADY
                                    314 	.globl _OSCFORCERUN
                                    315 	.globl _OSCCALIB
                                    316 	.globl _MISCCTRL
                                    317 	.globl _LPXOSCGM
                                    318 	.globl _LPOSCREF
                                    319 	.globl _LPOSCREF1
                                    320 	.globl _LPOSCREF0
                                    321 	.globl _LPOSCPER
                                    322 	.globl _LPOSCPER1
                                    323 	.globl _LPOSCPER0
                                    324 	.globl _LPOSCKFILT
                                    325 	.globl _LPOSCKFILT1
                                    326 	.globl _LPOSCKFILT0
                                    327 	.globl _LPOSCFREQ
                                    328 	.globl _LPOSCFREQ1
                                    329 	.globl _LPOSCFREQ0
                                    330 	.globl _LPOSCCONFIG
                                    331 	.globl _PINSEL
                                    332 	.globl _PINCHGC
                                    333 	.globl _PINCHGB
                                    334 	.globl _PINCHGA
                                    335 	.globl _PALTRADIO
                                    336 	.globl _PALTC
                                    337 	.globl _PALTB
                                    338 	.globl _PALTA
                                    339 	.globl _INTCHGC
                                    340 	.globl _INTCHGB
                                    341 	.globl _INTCHGA
                                    342 	.globl _EXTIRQ
                                    343 	.globl _GPIOENABLE
                                    344 	.globl _ANALOGA
                                    345 	.globl _FRCOSCREF
                                    346 	.globl _FRCOSCREF1
                                    347 	.globl _FRCOSCREF0
                                    348 	.globl _FRCOSCPER
                                    349 	.globl _FRCOSCPER1
                                    350 	.globl _FRCOSCPER0
                                    351 	.globl _FRCOSCKFILT
                                    352 	.globl _FRCOSCKFILT1
                                    353 	.globl _FRCOSCKFILT0
                                    354 	.globl _FRCOSCFREQ
                                    355 	.globl _FRCOSCFREQ1
                                    356 	.globl _FRCOSCFREQ0
                                    357 	.globl _FRCOSCCTRL
                                    358 	.globl _FRCOSCCONFIG
                                    359 	.globl _DMA1CONFIG
                                    360 	.globl _DMA1ADDR
                                    361 	.globl _DMA1ADDR1
                                    362 	.globl _DMA1ADDR0
                                    363 	.globl _DMA0CONFIG
                                    364 	.globl _DMA0ADDR
                                    365 	.globl _DMA0ADDR1
                                    366 	.globl _DMA0ADDR0
                                    367 	.globl _ADCTUNE2
                                    368 	.globl _ADCTUNE1
                                    369 	.globl _ADCTUNE0
                                    370 	.globl _ADCCH3VAL
                                    371 	.globl _ADCCH3VAL1
                                    372 	.globl _ADCCH3VAL0
                                    373 	.globl _ADCCH2VAL
                                    374 	.globl _ADCCH2VAL1
                                    375 	.globl _ADCCH2VAL0
                                    376 	.globl _ADCCH1VAL
                                    377 	.globl _ADCCH1VAL1
                                    378 	.globl _ADCCH1VAL0
                                    379 	.globl _ADCCH0VAL
                                    380 	.globl _ADCCH0VAL1
                                    381 	.globl _ADCCH0VAL0
                                    382 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_6
                                    383 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_5
                                    384 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_4
                                    385 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_3
                                    386 	.globl _UBLOX_GPS_SendCommand_WaitACK_PARM_2
                                    387 	.globl _UBLOX_GPS_FletcherChecksum8
                                    388 	.globl _UBLOX_GPS_SendCommand_WaitACK
                                    389 	.globl _UBLOX_GPS_PortInit
                                    390 ;--------------------------------------------------------
                                    391 ; special function registers
                                    392 ;--------------------------------------------------------
                                    393 	.area RSEG    (ABS,DATA)
      000000                        394 	.org 0x0000
                           0000E0   395 G$ACC$0$0 == 0x00e0
                           0000E0   396 _ACC	=	0x00e0
                           0000F0   397 G$B$0$0 == 0x00f0
                           0000F0   398 _B	=	0x00f0
                           000083   399 G$DPH$0$0 == 0x0083
                           000083   400 _DPH	=	0x0083
                           000085   401 G$DPH1$0$0 == 0x0085
                           000085   402 _DPH1	=	0x0085
                           000082   403 G$DPL$0$0 == 0x0082
                           000082   404 _DPL	=	0x0082
                           000084   405 G$DPL1$0$0 == 0x0084
                           000084   406 _DPL1	=	0x0084
                           008382   407 G$DPTR0$0$0 == 0x8382
                           008382   408 _DPTR0	=	0x8382
                           008584   409 G$DPTR1$0$0 == 0x8584
                           008584   410 _DPTR1	=	0x8584
                           000086   411 G$DPS$0$0 == 0x0086
                           000086   412 _DPS	=	0x0086
                           0000A0   413 G$E2IE$0$0 == 0x00a0
                           0000A0   414 _E2IE	=	0x00a0
                           0000C0   415 G$E2IP$0$0 == 0x00c0
                           0000C0   416 _E2IP	=	0x00c0
                           000098   417 G$EIE$0$0 == 0x0098
                           000098   418 _EIE	=	0x0098
                           0000B0   419 G$EIP$0$0 == 0x00b0
                           0000B0   420 _EIP	=	0x00b0
                           0000A8   421 G$IE$0$0 == 0x00a8
                           0000A8   422 _IE	=	0x00a8
                           0000B8   423 G$IP$0$0 == 0x00b8
                           0000B8   424 _IP	=	0x00b8
                           000087   425 G$PCON$0$0 == 0x0087
                           000087   426 _PCON	=	0x0087
                           0000D0   427 G$PSW$0$0 == 0x00d0
                           0000D0   428 _PSW	=	0x00d0
                           000081   429 G$SP$0$0 == 0x0081
                           000081   430 _SP	=	0x0081
                           0000D9   431 G$XPAGE$0$0 == 0x00d9
                           0000D9   432 _XPAGE	=	0x00d9
                           0000D9   433 G$_XPAGE$0$0 == 0x00d9
                           0000D9   434 __XPAGE	=	0x00d9
                           0000CA   435 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   436 _ADCCH0CONFIG	=	0x00ca
                           0000CB   437 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   438 _ADCCH1CONFIG	=	0x00cb
                           0000D2   439 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   440 _ADCCH2CONFIG	=	0x00d2
                           0000D3   441 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   442 _ADCCH3CONFIG	=	0x00d3
                           0000D1   443 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   444 _ADCCLKSRC	=	0x00d1
                           0000C9   445 G$ADCCONV$0$0 == 0x00c9
                           0000C9   446 _ADCCONV	=	0x00c9
                           0000E1   447 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   448 _ANALOGCOMP	=	0x00e1
                           0000C6   449 G$CLKCON$0$0 == 0x00c6
                           0000C6   450 _CLKCON	=	0x00c6
                           0000C7   451 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   452 _CLKSTAT	=	0x00c7
                           000097   453 G$CODECONFIG$0$0 == 0x0097
                           000097   454 _CODECONFIG	=	0x0097
                           0000E3   455 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   456 _DBGLNKBUF	=	0x00e3
                           0000E2   457 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   458 _DBGLNKSTAT	=	0x00e2
                           000089   459 G$DIRA$0$0 == 0x0089
                           000089   460 _DIRA	=	0x0089
                           00008A   461 G$DIRB$0$0 == 0x008a
                           00008A   462 _DIRB	=	0x008a
                           00008B   463 G$DIRC$0$0 == 0x008b
                           00008B   464 _DIRC	=	0x008b
                           00008E   465 G$DIRR$0$0 == 0x008e
                           00008E   466 _DIRR	=	0x008e
                           0000C8   467 G$PINA$0$0 == 0x00c8
                           0000C8   468 _PINA	=	0x00c8
                           0000E8   469 G$PINB$0$0 == 0x00e8
                           0000E8   470 _PINB	=	0x00e8
                           0000F8   471 G$PINC$0$0 == 0x00f8
                           0000F8   472 _PINC	=	0x00f8
                           00008D   473 G$PINR$0$0 == 0x008d
                           00008D   474 _PINR	=	0x008d
                           000080   475 G$PORTA$0$0 == 0x0080
                           000080   476 _PORTA	=	0x0080
                           000088   477 G$PORTB$0$0 == 0x0088
                           000088   478 _PORTB	=	0x0088
                           000090   479 G$PORTC$0$0 == 0x0090
                           000090   480 _PORTC	=	0x0090
                           00008C   481 G$PORTR$0$0 == 0x008c
                           00008C   482 _PORTR	=	0x008c
                           0000CE   483 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   484 _IC0CAPT0	=	0x00ce
                           0000CF   485 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   486 _IC0CAPT1	=	0x00cf
                           00CFCE   487 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   488 _IC0CAPT	=	0xcfce
                           0000CC   489 G$IC0MODE$0$0 == 0x00cc
                           0000CC   490 _IC0MODE	=	0x00cc
                           0000CD   491 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   492 _IC0STATUS	=	0x00cd
                           0000D6   493 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   494 _IC1CAPT0	=	0x00d6
                           0000D7   495 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   496 _IC1CAPT1	=	0x00d7
                           00D7D6   497 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   498 _IC1CAPT	=	0xd7d6
                           0000D4   499 G$IC1MODE$0$0 == 0x00d4
                           0000D4   500 _IC1MODE	=	0x00d4
                           0000D5   501 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   502 _IC1STATUS	=	0x00d5
                           000092   503 G$NVADDR0$0$0 == 0x0092
                           000092   504 _NVADDR0	=	0x0092
                           000093   505 G$NVADDR1$0$0 == 0x0093
                           000093   506 _NVADDR1	=	0x0093
                           009392   507 G$NVADDR$0$0 == 0x9392
                           009392   508 _NVADDR	=	0x9392
                           000094   509 G$NVDATA0$0$0 == 0x0094
                           000094   510 _NVDATA0	=	0x0094
                           000095   511 G$NVDATA1$0$0 == 0x0095
                           000095   512 _NVDATA1	=	0x0095
                           009594   513 G$NVDATA$0$0 == 0x9594
                           009594   514 _NVDATA	=	0x9594
                           000096   515 G$NVKEY$0$0 == 0x0096
                           000096   516 _NVKEY	=	0x0096
                           000091   517 G$NVSTATUS$0$0 == 0x0091
                           000091   518 _NVSTATUS	=	0x0091
                           0000BC   519 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   520 _OC0COMP0	=	0x00bc
                           0000BD   521 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   522 _OC0COMP1	=	0x00bd
                           00BDBC   523 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   524 _OC0COMP	=	0xbdbc
                           0000B9   525 G$OC0MODE$0$0 == 0x00b9
                           0000B9   526 _OC0MODE	=	0x00b9
                           0000BA   527 G$OC0PIN$0$0 == 0x00ba
                           0000BA   528 _OC0PIN	=	0x00ba
                           0000BB   529 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   530 _OC0STATUS	=	0x00bb
                           0000C4   531 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   532 _OC1COMP0	=	0x00c4
                           0000C5   533 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   534 _OC1COMP1	=	0x00c5
                           00C5C4   535 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   536 _OC1COMP	=	0xc5c4
                           0000C1   537 G$OC1MODE$0$0 == 0x00c1
                           0000C1   538 _OC1MODE	=	0x00c1
                           0000C2   539 G$OC1PIN$0$0 == 0x00c2
                           0000C2   540 _OC1PIN	=	0x00c2
                           0000C3   541 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   542 _OC1STATUS	=	0x00c3
                           0000B1   543 G$RADIOACC$0$0 == 0x00b1
                           0000B1   544 _RADIOACC	=	0x00b1
                           0000B3   545 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   546 _RADIOADDR0	=	0x00b3
                           0000B2   547 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   548 _RADIOADDR1	=	0x00b2
                           00B2B3   549 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   550 _RADIOADDR	=	0xb2b3
                           0000B7   551 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   552 _RADIODATA0	=	0x00b7
                           0000B6   553 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   554 _RADIODATA1	=	0x00b6
                           0000B5   555 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   556 _RADIODATA2	=	0x00b5
                           0000B4   557 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   558 _RADIODATA3	=	0x00b4
                           B4B5B6B7   559 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   560 _RADIODATA	=	0xb4b5b6b7
                           0000BE   561 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   562 _RADIOSTAT0	=	0x00be
                           0000BF   563 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   564 _RADIOSTAT1	=	0x00bf
                           00BFBE   565 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   566 _RADIOSTAT	=	0xbfbe
                           0000DF   567 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   568 _SPCLKSRC	=	0x00df
                           0000DC   569 G$SPMODE$0$0 == 0x00dc
                           0000DC   570 _SPMODE	=	0x00dc
                           0000DE   571 G$SPSHREG$0$0 == 0x00de
                           0000DE   572 _SPSHREG	=	0x00de
                           0000DD   573 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   574 _SPSTATUS	=	0x00dd
                           00009A   575 G$T0CLKSRC$0$0 == 0x009a
                           00009A   576 _T0CLKSRC	=	0x009a
                           00009C   577 G$T0CNT0$0$0 == 0x009c
                           00009C   578 _T0CNT0	=	0x009c
                           00009D   579 G$T0CNT1$0$0 == 0x009d
                           00009D   580 _T0CNT1	=	0x009d
                           009D9C   581 G$T0CNT$0$0 == 0x9d9c
                           009D9C   582 _T0CNT	=	0x9d9c
                           000099   583 G$T0MODE$0$0 == 0x0099
                           000099   584 _T0MODE	=	0x0099
                           00009E   585 G$T0PERIOD0$0$0 == 0x009e
                           00009E   586 _T0PERIOD0	=	0x009e
                           00009F   587 G$T0PERIOD1$0$0 == 0x009f
                           00009F   588 _T0PERIOD1	=	0x009f
                           009F9E   589 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   590 _T0PERIOD	=	0x9f9e
                           00009B   591 G$T0STATUS$0$0 == 0x009b
                           00009B   592 _T0STATUS	=	0x009b
                           0000A2   593 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   594 _T1CLKSRC	=	0x00a2
                           0000A4   595 G$T1CNT0$0$0 == 0x00a4
                           0000A4   596 _T1CNT0	=	0x00a4
                           0000A5   597 G$T1CNT1$0$0 == 0x00a5
                           0000A5   598 _T1CNT1	=	0x00a5
                           00A5A4   599 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   600 _T1CNT	=	0xa5a4
                           0000A1   601 G$T1MODE$0$0 == 0x00a1
                           0000A1   602 _T1MODE	=	0x00a1
                           0000A6   603 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   604 _T1PERIOD0	=	0x00a6
                           0000A7   605 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   606 _T1PERIOD1	=	0x00a7
                           00A7A6   607 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   608 _T1PERIOD	=	0xa7a6
                           0000A3   609 G$T1STATUS$0$0 == 0x00a3
                           0000A3   610 _T1STATUS	=	0x00a3
                           0000AA   611 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   612 _T2CLKSRC	=	0x00aa
                           0000AC   613 G$T2CNT0$0$0 == 0x00ac
                           0000AC   614 _T2CNT0	=	0x00ac
                           0000AD   615 G$T2CNT1$0$0 == 0x00ad
                           0000AD   616 _T2CNT1	=	0x00ad
                           00ADAC   617 G$T2CNT$0$0 == 0xadac
                           00ADAC   618 _T2CNT	=	0xadac
                           0000A9   619 G$T2MODE$0$0 == 0x00a9
                           0000A9   620 _T2MODE	=	0x00a9
                           0000AE   621 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   622 _T2PERIOD0	=	0x00ae
                           0000AF   623 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   624 _T2PERIOD1	=	0x00af
                           00AFAE   625 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   626 _T2PERIOD	=	0xafae
                           0000AB   627 G$T2STATUS$0$0 == 0x00ab
                           0000AB   628 _T2STATUS	=	0x00ab
                           0000E4   629 G$U0CTRL$0$0 == 0x00e4
                           0000E4   630 _U0CTRL	=	0x00e4
                           0000E7   631 G$U0MODE$0$0 == 0x00e7
                           0000E7   632 _U0MODE	=	0x00e7
                           0000E6   633 G$U0SHREG$0$0 == 0x00e6
                           0000E6   634 _U0SHREG	=	0x00e6
                           0000E5   635 G$U0STATUS$0$0 == 0x00e5
                           0000E5   636 _U0STATUS	=	0x00e5
                           0000EC   637 G$U1CTRL$0$0 == 0x00ec
                           0000EC   638 _U1CTRL	=	0x00ec
                           0000EF   639 G$U1MODE$0$0 == 0x00ef
                           0000EF   640 _U1MODE	=	0x00ef
                           0000EE   641 G$U1SHREG$0$0 == 0x00ee
                           0000EE   642 _U1SHREG	=	0x00ee
                           0000ED   643 G$U1STATUS$0$0 == 0x00ed
                           0000ED   644 _U1STATUS	=	0x00ed
                           0000DA   645 G$WDTCFG$0$0 == 0x00da
                           0000DA   646 _WDTCFG	=	0x00da
                           0000DB   647 G$WDTRESET$0$0 == 0x00db
                           0000DB   648 _WDTRESET	=	0x00db
                           0000F1   649 G$WTCFGA$0$0 == 0x00f1
                           0000F1   650 _WTCFGA	=	0x00f1
                           0000F9   651 G$WTCFGB$0$0 == 0x00f9
                           0000F9   652 _WTCFGB	=	0x00f9
                           0000F2   653 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   654 _WTCNTA0	=	0x00f2
                           0000F3   655 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   656 _WTCNTA1	=	0x00f3
                           00F3F2   657 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   658 _WTCNTA	=	0xf3f2
                           0000FA   659 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   660 _WTCNTB0	=	0x00fa
                           0000FB   661 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   662 _WTCNTB1	=	0x00fb
                           00FBFA   663 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   664 _WTCNTB	=	0xfbfa
                           0000EB   665 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   666 _WTCNTR1	=	0x00eb
                           0000F4   667 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   668 _WTEVTA0	=	0x00f4
                           0000F5   669 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   670 _WTEVTA1	=	0x00f5
                           00F5F4   671 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   672 _WTEVTA	=	0xf5f4
                           0000F6   673 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   674 _WTEVTB0	=	0x00f6
                           0000F7   675 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   676 _WTEVTB1	=	0x00f7
                           00F7F6   677 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   678 _WTEVTB	=	0xf7f6
                           0000FC   679 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   680 _WTEVTC0	=	0x00fc
                           0000FD   681 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   682 _WTEVTC1	=	0x00fd
                           00FDFC   683 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   684 _WTEVTC	=	0xfdfc
                           0000FE   685 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   686 _WTEVTD0	=	0x00fe
                           0000FF   687 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   688 _WTEVTD1	=	0x00ff
                           00FFFE   689 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   690 _WTEVTD	=	0xfffe
                           0000E9   691 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   692 _WTIRQEN	=	0x00e9
                           0000EA   693 G$WTSTAT$0$0 == 0x00ea
                           0000EA   694 _WTSTAT	=	0x00ea
                                    695 ;--------------------------------------------------------
                                    696 ; special function bits
                                    697 ;--------------------------------------------------------
                                    698 	.area RSEG    (ABS,DATA)
      000000                        699 	.org 0x0000
                           0000E0   700 G$ACC_0$0$0 == 0x00e0
                           0000E0   701 _ACC_0	=	0x00e0
                           0000E1   702 G$ACC_1$0$0 == 0x00e1
                           0000E1   703 _ACC_1	=	0x00e1
                           0000E2   704 G$ACC_2$0$0 == 0x00e2
                           0000E2   705 _ACC_2	=	0x00e2
                           0000E3   706 G$ACC_3$0$0 == 0x00e3
                           0000E3   707 _ACC_3	=	0x00e3
                           0000E4   708 G$ACC_4$0$0 == 0x00e4
                           0000E4   709 _ACC_4	=	0x00e4
                           0000E5   710 G$ACC_5$0$0 == 0x00e5
                           0000E5   711 _ACC_5	=	0x00e5
                           0000E6   712 G$ACC_6$0$0 == 0x00e6
                           0000E6   713 _ACC_6	=	0x00e6
                           0000E7   714 G$ACC_7$0$0 == 0x00e7
                           0000E7   715 _ACC_7	=	0x00e7
                           0000F0   716 G$B_0$0$0 == 0x00f0
                           0000F0   717 _B_0	=	0x00f0
                           0000F1   718 G$B_1$0$0 == 0x00f1
                           0000F1   719 _B_1	=	0x00f1
                           0000F2   720 G$B_2$0$0 == 0x00f2
                           0000F2   721 _B_2	=	0x00f2
                           0000F3   722 G$B_3$0$0 == 0x00f3
                           0000F3   723 _B_3	=	0x00f3
                           0000F4   724 G$B_4$0$0 == 0x00f4
                           0000F4   725 _B_4	=	0x00f4
                           0000F5   726 G$B_5$0$0 == 0x00f5
                           0000F5   727 _B_5	=	0x00f5
                           0000F6   728 G$B_6$0$0 == 0x00f6
                           0000F6   729 _B_6	=	0x00f6
                           0000F7   730 G$B_7$0$0 == 0x00f7
                           0000F7   731 _B_7	=	0x00f7
                           0000A0   732 G$E2IE_0$0$0 == 0x00a0
                           0000A0   733 _E2IE_0	=	0x00a0
                           0000A1   734 G$E2IE_1$0$0 == 0x00a1
                           0000A1   735 _E2IE_1	=	0x00a1
                           0000A2   736 G$E2IE_2$0$0 == 0x00a2
                           0000A2   737 _E2IE_2	=	0x00a2
                           0000A3   738 G$E2IE_3$0$0 == 0x00a3
                           0000A3   739 _E2IE_3	=	0x00a3
                           0000A4   740 G$E2IE_4$0$0 == 0x00a4
                           0000A4   741 _E2IE_4	=	0x00a4
                           0000A5   742 G$E2IE_5$0$0 == 0x00a5
                           0000A5   743 _E2IE_5	=	0x00a5
                           0000A6   744 G$E2IE_6$0$0 == 0x00a6
                           0000A6   745 _E2IE_6	=	0x00a6
                           0000A7   746 G$E2IE_7$0$0 == 0x00a7
                           0000A7   747 _E2IE_7	=	0x00a7
                           0000C0   748 G$E2IP_0$0$0 == 0x00c0
                           0000C0   749 _E2IP_0	=	0x00c0
                           0000C1   750 G$E2IP_1$0$0 == 0x00c1
                           0000C1   751 _E2IP_1	=	0x00c1
                           0000C2   752 G$E2IP_2$0$0 == 0x00c2
                           0000C2   753 _E2IP_2	=	0x00c2
                           0000C3   754 G$E2IP_3$0$0 == 0x00c3
                           0000C3   755 _E2IP_3	=	0x00c3
                           0000C4   756 G$E2IP_4$0$0 == 0x00c4
                           0000C4   757 _E2IP_4	=	0x00c4
                           0000C5   758 G$E2IP_5$0$0 == 0x00c5
                           0000C5   759 _E2IP_5	=	0x00c5
                           0000C6   760 G$E2IP_6$0$0 == 0x00c6
                           0000C6   761 _E2IP_6	=	0x00c6
                           0000C7   762 G$E2IP_7$0$0 == 0x00c7
                           0000C7   763 _E2IP_7	=	0x00c7
                           000098   764 G$EIE_0$0$0 == 0x0098
                           000098   765 _EIE_0	=	0x0098
                           000099   766 G$EIE_1$0$0 == 0x0099
                           000099   767 _EIE_1	=	0x0099
                           00009A   768 G$EIE_2$0$0 == 0x009a
                           00009A   769 _EIE_2	=	0x009a
                           00009B   770 G$EIE_3$0$0 == 0x009b
                           00009B   771 _EIE_3	=	0x009b
                           00009C   772 G$EIE_4$0$0 == 0x009c
                           00009C   773 _EIE_4	=	0x009c
                           00009D   774 G$EIE_5$0$0 == 0x009d
                           00009D   775 _EIE_5	=	0x009d
                           00009E   776 G$EIE_6$0$0 == 0x009e
                           00009E   777 _EIE_6	=	0x009e
                           00009F   778 G$EIE_7$0$0 == 0x009f
                           00009F   779 _EIE_7	=	0x009f
                           0000B0   780 G$EIP_0$0$0 == 0x00b0
                           0000B0   781 _EIP_0	=	0x00b0
                           0000B1   782 G$EIP_1$0$0 == 0x00b1
                           0000B1   783 _EIP_1	=	0x00b1
                           0000B2   784 G$EIP_2$0$0 == 0x00b2
                           0000B2   785 _EIP_2	=	0x00b2
                           0000B3   786 G$EIP_3$0$0 == 0x00b3
                           0000B3   787 _EIP_3	=	0x00b3
                           0000B4   788 G$EIP_4$0$0 == 0x00b4
                           0000B4   789 _EIP_4	=	0x00b4
                           0000B5   790 G$EIP_5$0$0 == 0x00b5
                           0000B5   791 _EIP_5	=	0x00b5
                           0000B6   792 G$EIP_6$0$0 == 0x00b6
                           0000B6   793 _EIP_6	=	0x00b6
                           0000B7   794 G$EIP_7$0$0 == 0x00b7
                           0000B7   795 _EIP_7	=	0x00b7
                           0000A8   796 G$IE_0$0$0 == 0x00a8
                           0000A8   797 _IE_0	=	0x00a8
                           0000A9   798 G$IE_1$0$0 == 0x00a9
                           0000A9   799 _IE_1	=	0x00a9
                           0000AA   800 G$IE_2$0$0 == 0x00aa
                           0000AA   801 _IE_2	=	0x00aa
                           0000AB   802 G$IE_3$0$0 == 0x00ab
                           0000AB   803 _IE_3	=	0x00ab
                           0000AC   804 G$IE_4$0$0 == 0x00ac
                           0000AC   805 _IE_4	=	0x00ac
                           0000AD   806 G$IE_5$0$0 == 0x00ad
                           0000AD   807 _IE_5	=	0x00ad
                           0000AE   808 G$IE_6$0$0 == 0x00ae
                           0000AE   809 _IE_6	=	0x00ae
                           0000AF   810 G$IE_7$0$0 == 0x00af
                           0000AF   811 _IE_7	=	0x00af
                           0000AF   812 G$EA$0$0 == 0x00af
                           0000AF   813 _EA	=	0x00af
                           0000B8   814 G$IP_0$0$0 == 0x00b8
                           0000B8   815 _IP_0	=	0x00b8
                           0000B9   816 G$IP_1$0$0 == 0x00b9
                           0000B9   817 _IP_1	=	0x00b9
                           0000BA   818 G$IP_2$0$0 == 0x00ba
                           0000BA   819 _IP_2	=	0x00ba
                           0000BB   820 G$IP_3$0$0 == 0x00bb
                           0000BB   821 _IP_3	=	0x00bb
                           0000BC   822 G$IP_4$0$0 == 0x00bc
                           0000BC   823 _IP_4	=	0x00bc
                           0000BD   824 G$IP_5$0$0 == 0x00bd
                           0000BD   825 _IP_5	=	0x00bd
                           0000BE   826 G$IP_6$0$0 == 0x00be
                           0000BE   827 _IP_6	=	0x00be
                           0000BF   828 G$IP_7$0$0 == 0x00bf
                           0000BF   829 _IP_7	=	0x00bf
                           0000D0   830 G$P$0$0 == 0x00d0
                           0000D0   831 _P	=	0x00d0
                           0000D1   832 G$F1$0$0 == 0x00d1
                           0000D1   833 _F1	=	0x00d1
                           0000D2   834 G$OV$0$0 == 0x00d2
                           0000D2   835 _OV	=	0x00d2
                           0000D3   836 G$RS0$0$0 == 0x00d3
                           0000D3   837 _RS0	=	0x00d3
                           0000D4   838 G$RS1$0$0 == 0x00d4
                           0000D4   839 _RS1	=	0x00d4
                           0000D5   840 G$F0$0$0 == 0x00d5
                           0000D5   841 _F0	=	0x00d5
                           0000D6   842 G$AC$0$0 == 0x00d6
                           0000D6   843 _AC	=	0x00d6
                           0000D7   844 G$CY$0$0 == 0x00d7
                           0000D7   845 _CY	=	0x00d7
                           0000C8   846 G$PINA_0$0$0 == 0x00c8
                           0000C8   847 _PINA_0	=	0x00c8
                           0000C9   848 G$PINA_1$0$0 == 0x00c9
                           0000C9   849 _PINA_1	=	0x00c9
                           0000CA   850 G$PINA_2$0$0 == 0x00ca
                           0000CA   851 _PINA_2	=	0x00ca
                           0000CB   852 G$PINA_3$0$0 == 0x00cb
                           0000CB   853 _PINA_3	=	0x00cb
                           0000CC   854 G$PINA_4$0$0 == 0x00cc
                           0000CC   855 _PINA_4	=	0x00cc
                           0000CD   856 G$PINA_5$0$0 == 0x00cd
                           0000CD   857 _PINA_5	=	0x00cd
                           0000CE   858 G$PINA_6$0$0 == 0x00ce
                           0000CE   859 _PINA_6	=	0x00ce
                           0000CF   860 G$PINA_7$0$0 == 0x00cf
                           0000CF   861 _PINA_7	=	0x00cf
                           0000E8   862 G$PINB_0$0$0 == 0x00e8
                           0000E8   863 _PINB_0	=	0x00e8
                           0000E9   864 G$PINB_1$0$0 == 0x00e9
                           0000E9   865 _PINB_1	=	0x00e9
                           0000EA   866 G$PINB_2$0$0 == 0x00ea
                           0000EA   867 _PINB_2	=	0x00ea
                           0000EB   868 G$PINB_3$0$0 == 0x00eb
                           0000EB   869 _PINB_3	=	0x00eb
                           0000EC   870 G$PINB_4$0$0 == 0x00ec
                           0000EC   871 _PINB_4	=	0x00ec
                           0000ED   872 G$PINB_5$0$0 == 0x00ed
                           0000ED   873 _PINB_5	=	0x00ed
                           0000EE   874 G$PINB_6$0$0 == 0x00ee
                           0000EE   875 _PINB_6	=	0x00ee
                           0000EF   876 G$PINB_7$0$0 == 0x00ef
                           0000EF   877 _PINB_7	=	0x00ef
                           0000F8   878 G$PINC_0$0$0 == 0x00f8
                           0000F8   879 _PINC_0	=	0x00f8
                           0000F9   880 G$PINC_1$0$0 == 0x00f9
                           0000F9   881 _PINC_1	=	0x00f9
                           0000FA   882 G$PINC_2$0$0 == 0x00fa
                           0000FA   883 _PINC_2	=	0x00fa
                           0000FB   884 G$PINC_3$0$0 == 0x00fb
                           0000FB   885 _PINC_3	=	0x00fb
                           0000FC   886 G$PINC_4$0$0 == 0x00fc
                           0000FC   887 _PINC_4	=	0x00fc
                           0000FD   888 G$PINC_5$0$0 == 0x00fd
                           0000FD   889 _PINC_5	=	0x00fd
                           0000FE   890 G$PINC_6$0$0 == 0x00fe
                           0000FE   891 _PINC_6	=	0x00fe
                           0000FF   892 G$PINC_7$0$0 == 0x00ff
                           0000FF   893 _PINC_7	=	0x00ff
                           000080   894 G$PORTA_0$0$0 == 0x0080
                           000080   895 _PORTA_0	=	0x0080
                           000081   896 G$PORTA_1$0$0 == 0x0081
                           000081   897 _PORTA_1	=	0x0081
                           000082   898 G$PORTA_2$0$0 == 0x0082
                           000082   899 _PORTA_2	=	0x0082
                           000083   900 G$PORTA_3$0$0 == 0x0083
                           000083   901 _PORTA_3	=	0x0083
                           000084   902 G$PORTA_4$0$0 == 0x0084
                           000084   903 _PORTA_4	=	0x0084
                           000085   904 G$PORTA_5$0$0 == 0x0085
                           000085   905 _PORTA_5	=	0x0085
                           000086   906 G$PORTA_6$0$0 == 0x0086
                           000086   907 _PORTA_6	=	0x0086
                           000087   908 G$PORTA_7$0$0 == 0x0087
                           000087   909 _PORTA_7	=	0x0087
                           000088   910 G$PORTB_0$0$0 == 0x0088
                           000088   911 _PORTB_0	=	0x0088
                           000089   912 G$PORTB_1$0$0 == 0x0089
                           000089   913 _PORTB_1	=	0x0089
                           00008A   914 G$PORTB_2$0$0 == 0x008a
                           00008A   915 _PORTB_2	=	0x008a
                           00008B   916 G$PORTB_3$0$0 == 0x008b
                           00008B   917 _PORTB_3	=	0x008b
                           00008C   918 G$PORTB_4$0$0 == 0x008c
                           00008C   919 _PORTB_4	=	0x008c
                           00008D   920 G$PORTB_5$0$0 == 0x008d
                           00008D   921 _PORTB_5	=	0x008d
                           00008E   922 G$PORTB_6$0$0 == 0x008e
                           00008E   923 _PORTB_6	=	0x008e
                           00008F   924 G$PORTB_7$0$0 == 0x008f
                           00008F   925 _PORTB_7	=	0x008f
                           000090   926 G$PORTC_0$0$0 == 0x0090
                           000090   927 _PORTC_0	=	0x0090
                           000091   928 G$PORTC_1$0$0 == 0x0091
                           000091   929 _PORTC_1	=	0x0091
                           000092   930 G$PORTC_2$0$0 == 0x0092
                           000092   931 _PORTC_2	=	0x0092
                           000093   932 G$PORTC_3$0$0 == 0x0093
                           000093   933 _PORTC_3	=	0x0093
                           000094   934 G$PORTC_4$0$0 == 0x0094
                           000094   935 _PORTC_4	=	0x0094
                           000095   936 G$PORTC_5$0$0 == 0x0095
                           000095   937 _PORTC_5	=	0x0095
                           000096   938 G$PORTC_6$0$0 == 0x0096
                           000096   939 _PORTC_6	=	0x0096
                           000097   940 G$PORTC_7$0$0 == 0x0097
                           000097   941 _PORTC_7	=	0x0097
                                    942 ;--------------------------------------------------------
                                    943 ; overlayable register banks
                                    944 ;--------------------------------------------------------
                                    945 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        946 	.ds 8
                                    947 ;--------------------------------------------------------
                                    948 ; internal ram data
                                    949 ;--------------------------------------------------------
                                    950 	.area DSEG    (DATA)
                           000000   951 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_id$1$243==.
      000038                        952 _UBLOX_GPS_SendCommand_WaitACK_PARM_2:
      000038                        953 	.ds 1
                           000001   954 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_length$1$243==.
      000039                        955 _UBLOX_GPS_SendCommand_WaitACK_PARM_3:
      000039                        956 	.ds 2
                           000003   957 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$payload$1$243==.
      00003B                        958 _UBLOX_GPS_SendCommand_WaitACK_PARM_4:
      00003B                        959 	.ds 3
                           000006   960 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$AnsOrAck$1$243==.
      00003E                        961 _UBLOX_GPS_SendCommand_WaitACK_PARM_5:
      00003E                        962 	.ds 1
                           000007   963 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$RxLength$1$243==.
      00003F                        964 _UBLOX_GPS_SendCommand_WaitACK_PARM_6:
      00003F                        965 	.ds 3
                           00000A   966 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$msg_class$1$243==.
      000042                        967 _UBLOX_GPS_SendCommand_WaitACK_msg_class_1_243:
      000042                        968 	.ds 1
                           00000B   969 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$k$1$244==.
      000043                        970 _UBLOX_GPS_SendCommand_WaitACK_k_1_244:
      000043                        971 	.ds 1
                           00000C   972 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$RetVal$1$244==.
      000044                        973 _UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244:
      000044                        974 	.ds 1
                           00000D   975 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$buffer_aux$1$244==.
      000045                        976 _UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244:
      000045                        977 	.ds 2
                           00000F   978 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$rx_buffer$1$244==.
      000047                        979 _UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244:
      000047                        980 	.ds 2
                           000011   981 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$CK_A$1$244==.
      000049                        982 _UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244:
      000049                        983 	.ds 1
                           000012   984 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$CK_B$1$244==.
      00004A                        985 _UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244:
      00004A                        986 	.ds 1
                           000013   987 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$sloc0$1$0==.
      00004B                        988 _UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0:
      00004B                        989 	.ds 2
                           000015   990 Lublox_gnss.UBLOX_GPS_SendCommand_WaitACK$sloc1$1$0==.
      00004D                        991 _UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0:
      00004D                        992 	.ds 3
                                    993 ;--------------------------------------------------------
                                    994 ; overlayable items in internal ram 
                                    995 ;--------------------------------------------------------
                                    996 	.area	OSEG    (OVR,DATA)
                           000000   997 Lublox_gnss.aligned_alloc$size$1$150==.
      00006E                        998 _aligned_alloc_PARM_2:
      00006E                        999 	.ds 2
                           000002  1000 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$CK_A$1$240==.
      000070                       1001 _UBLOX_GPS_FletcherChecksum8_PARM_2:
      000070                       1002 	.ds 3
                           000005  1003 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$CK_B$1$240==.
      000073                       1004 _UBLOX_GPS_FletcherChecksum8_PARM_3:
      000073                       1005 	.ds 3
                           000008  1006 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$length$1$240==.
      000076                       1007 _UBLOX_GPS_FletcherChecksum8_PARM_4:
      000076                       1008 	.ds 2
                           00000A  1009 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$buffer$1$240==.
      000078                       1010 _UBLOX_GPS_FletcherChecksum8_buffer_1_240:
      000078                       1011 	.ds 3
                           00000D  1012 Lublox_gnss.UBLOX_GPS_FletcherChecksum8$sloc0$1$0==.
      00007B                       1013 _UBLOX_GPS_FletcherChecksum8_sloc0_1_0:
      00007B                       1014 	.ds 3
                                   1015 ;--------------------------------------------------------
                                   1016 ; indirectly addressable internal ram data
                                   1017 ;--------------------------------------------------------
                                   1018 	.area ISEG    (DATA)
                                   1019 ;--------------------------------------------------------
                                   1020 ; absolute internal ram data
                                   1021 ;--------------------------------------------------------
                                   1022 	.area IABS    (ABS,DATA)
                                   1023 	.area IABS    (ABS,DATA)
                                   1024 ;--------------------------------------------------------
                                   1025 ; bit data
                                   1026 ;--------------------------------------------------------
                                   1027 	.area BSEG    (BIT)
                                   1028 ;--------------------------------------------------------
                                   1029 ; paged external ram data
                                   1030 ;--------------------------------------------------------
                                   1031 	.area PSEG    (PAG,XDATA)
                                   1032 ;--------------------------------------------------------
                                   1033 ; external ram data
                                   1034 ;--------------------------------------------------------
                                   1035 	.area XSEG    (XDATA)
                           007020  1036 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1037 _ADCCH0VAL0	=	0x7020
                           007021  1038 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1039 _ADCCH0VAL1	=	0x7021
                           007020  1040 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1041 _ADCCH0VAL	=	0x7020
                           007022  1042 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1043 _ADCCH1VAL0	=	0x7022
                           007023  1044 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1045 _ADCCH1VAL1	=	0x7023
                           007022  1046 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1047 _ADCCH1VAL	=	0x7022
                           007024  1048 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1049 _ADCCH2VAL0	=	0x7024
                           007025  1050 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1051 _ADCCH2VAL1	=	0x7025
                           007024  1052 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1053 _ADCCH2VAL	=	0x7024
                           007026  1054 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1055 _ADCCH3VAL0	=	0x7026
                           007027  1056 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1057 _ADCCH3VAL1	=	0x7027
                           007026  1058 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1059 _ADCCH3VAL	=	0x7026
                           007028  1060 G$ADCTUNE0$0$0 == 0x7028
                           007028  1061 _ADCTUNE0	=	0x7028
                           007029  1062 G$ADCTUNE1$0$0 == 0x7029
                           007029  1063 _ADCTUNE1	=	0x7029
                           00702A  1064 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1065 _ADCTUNE2	=	0x702a
                           007010  1066 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1067 _DMA0ADDR0	=	0x7010
                           007011  1068 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1069 _DMA0ADDR1	=	0x7011
                           007010  1070 G$DMA0ADDR$0$0 == 0x7010
                           007010  1071 _DMA0ADDR	=	0x7010
                           007014  1072 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1073 _DMA0CONFIG	=	0x7014
                           007012  1074 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1075 _DMA1ADDR0	=	0x7012
                           007013  1076 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1077 _DMA1ADDR1	=	0x7013
                           007012  1078 G$DMA1ADDR$0$0 == 0x7012
                           007012  1079 _DMA1ADDR	=	0x7012
                           007015  1080 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1081 _DMA1CONFIG	=	0x7015
                           007070  1082 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1083 _FRCOSCCONFIG	=	0x7070
                           007071  1084 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1085 _FRCOSCCTRL	=	0x7071
                           007076  1086 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1087 _FRCOSCFREQ0	=	0x7076
                           007077  1088 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1089 _FRCOSCFREQ1	=	0x7077
                           007076  1090 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1091 _FRCOSCFREQ	=	0x7076
                           007072  1092 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1093 _FRCOSCKFILT0	=	0x7072
                           007073  1094 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1095 _FRCOSCKFILT1	=	0x7073
                           007072  1096 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1097 _FRCOSCKFILT	=	0x7072
                           007078  1098 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1099 _FRCOSCPER0	=	0x7078
                           007079  1100 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1101 _FRCOSCPER1	=	0x7079
                           007078  1102 G$FRCOSCPER$0$0 == 0x7078
                           007078  1103 _FRCOSCPER	=	0x7078
                           007074  1104 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1105 _FRCOSCREF0	=	0x7074
                           007075  1106 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1107 _FRCOSCREF1	=	0x7075
                           007074  1108 G$FRCOSCREF$0$0 == 0x7074
                           007074  1109 _FRCOSCREF	=	0x7074
                           007007  1110 G$ANALOGA$0$0 == 0x7007
                           007007  1111 _ANALOGA	=	0x7007
                           00700C  1112 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1113 _GPIOENABLE	=	0x700c
                           007003  1114 G$EXTIRQ$0$0 == 0x7003
                           007003  1115 _EXTIRQ	=	0x7003
                           007000  1116 G$INTCHGA$0$0 == 0x7000
                           007000  1117 _INTCHGA	=	0x7000
                           007001  1118 G$INTCHGB$0$0 == 0x7001
                           007001  1119 _INTCHGB	=	0x7001
                           007002  1120 G$INTCHGC$0$0 == 0x7002
                           007002  1121 _INTCHGC	=	0x7002
                           007008  1122 G$PALTA$0$0 == 0x7008
                           007008  1123 _PALTA	=	0x7008
                           007009  1124 G$PALTB$0$0 == 0x7009
                           007009  1125 _PALTB	=	0x7009
                           00700A  1126 G$PALTC$0$0 == 0x700a
                           00700A  1127 _PALTC	=	0x700a
                           007046  1128 G$PALTRADIO$0$0 == 0x7046
                           007046  1129 _PALTRADIO	=	0x7046
                           007004  1130 G$PINCHGA$0$0 == 0x7004
                           007004  1131 _PINCHGA	=	0x7004
                           007005  1132 G$PINCHGB$0$0 == 0x7005
                           007005  1133 _PINCHGB	=	0x7005
                           007006  1134 G$PINCHGC$0$0 == 0x7006
                           007006  1135 _PINCHGC	=	0x7006
                           00700B  1136 G$PINSEL$0$0 == 0x700b
                           00700B  1137 _PINSEL	=	0x700b
                           007060  1138 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1139 _LPOSCCONFIG	=	0x7060
                           007066  1140 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1141 _LPOSCFREQ0	=	0x7066
                           007067  1142 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1143 _LPOSCFREQ1	=	0x7067
                           007066  1144 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1145 _LPOSCFREQ	=	0x7066
                           007062  1146 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1147 _LPOSCKFILT0	=	0x7062
                           007063  1148 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1149 _LPOSCKFILT1	=	0x7063
                           007062  1150 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1151 _LPOSCKFILT	=	0x7062
                           007068  1152 G$LPOSCPER0$0$0 == 0x7068
                           007068  1153 _LPOSCPER0	=	0x7068
                           007069  1154 G$LPOSCPER1$0$0 == 0x7069
                           007069  1155 _LPOSCPER1	=	0x7069
                           007068  1156 G$LPOSCPER$0$0 == 0x7068
                           007068  1157 _LPOSCPER	=	0x7068
                           007064  1158 G$LPOSCREF0$0$0 == 0x7064
                           007064  1159 _LPOSCREF0	=	0x7064
                           007065  1160 G$LPOSCREF1$0$0 == 0x7065
                           007065  1161 _LPOSCREF1	=	0x7065
                           007064  1162 G$LPOSCREF$0$0 == 0x7064
                           007064  1163 _LPOSCREF	=	0x7064
                           007054  1164 G$LPXOSCGM$0$0 == 0x7054
                           007054  1165 _LPXOSCGM	=	0x7054
                           007F01  1166 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1167 _MISCCTRL	=	0x7f01
                           007053  1168 G$OSCCALIB$0$0 == 0x7053
                           007053  1169 _OSCCALIB	=	0x7053
                           007050  1170 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1171 _OSCFORCERUN	=	0x7050
                           007052  1172 G$OSCREADY$0$0 == 0x7052
                           007052  1173 _OSCREADY	=	0x7052
                           007051  1174 G$OSCRUN$0$0 == 0x7051
                           007051  1175 _OSCRUN	=	0x7051
                           007040  1176 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1177 _RADIOFDATAADDR0	=	0x7040
                           007041  1178 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1179 _RADIOFDATAADDR1	=	0x7041
                           007040  1180 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1181 _RADIOFDATAADDR	=	0x7040
                           007042  1182 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1183 _RADIOFSTATADDR0	=	0x7042
                           007043  1184 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1185 _RADIOFSTATADDR1	=	0x7043
                           007042  1186 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1187 _RADIOFSTATADDR	=	0x7042
                           007044  1188 G$RADIOMUX$0$0 == 0x7044
                           007044  1189 _RADIOMUX	=	0x7044
                           007084  1190 G$SCRATCH0$0$0 == 0x7084
                           007084  1191 _SCRATCH0	=	0x7084
                           007085  1192 G$SCRATCH1$0$0 == 0x7085
                           007085  1193 _SCRATCH1	=	0x7085
                           007086  1194 G$SCRATCH2$0$0 == 0x7086
                           007086  1195 _SCRATCH2	=	0x7086
                           007087  1196 G$SCRATCH3$0$0 == 0x7087
                           007087  1197 _SCRATCH3	=	0x7087
                           007F00  1198 G$SILICONREV$0$0 == 0x7f00
                           007F00  1199 _SILICONREV	=	0x7f00
                           007F19  1200 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1201 _XTALAMPL	=	0x7f19
                           007F18  1202 G$XTALOSC$0$0 == 0x7f18
                           007F18  1203 _XTALOSC	=	0x7f18
                           007F1A  1204 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1205 _XTALREADY	=	0x7f1a
                           000000  1206 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav1$1$255==.
      00039F                       1207 _UBLOX_GPS_PortInit_UBLOX_setNav1_1_255:
      00039F                       1208 	.ds 3
                           000003  1209 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav2$1$255==.
      0003A2                       1210 _UBLOX_GPS_PortInit_UBLOX_setNav2_1_255:
      0003A2                       1211 	.ds 8
                           00000B  1212 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav3$1$255==.
      0003AA                       1213 _UBLOX_GPS_PortInit_UBLOX_setNav3_1_255:
      0003AA                       1214 	.ds 8
                           000013  1215 Lublox_gnss.UBLOX_GPS_PortInit$UBLOX_setNav5$1$255==.
      0003B2                       1216 _UBLOX_GPS_PortInit_UBLOX_setNav5_1_255:
      0003B2                       1217 	.ds 36
                                   1218 ;--------------------------------------------------------
                                   1219 ; absolute external ram data
                                   1220 ;--------------------------------------------------------
                                   1221 	.area XABS    (ABS,XDATA)
                                   1222 ;--------------------------------------------------------
                                   1223 ; external initialized ram data
                                   1224 ;--------------------------------------------------------
                                   1225 	.area XISEG   (XDATA)
                                   1226 	.area HOME    (CODE)
                                   1227 	.area GSINIT0 (CODE)
                                   1228 	.area GSINIT1 (CODE)
                                   1229 	.area GSINIT2 (CODE)
                                   1230 	.area GSINIT3 (CODE)
                                   1231 	.area GSINIT4 (CODE)
                                   1232 	.area GSINIT5 (CODE)
                                   1233 	.area GSINIT  (CODE)
                                   1234 	.area GSFINAL (CODE)
                                   1235 	.area CSEG    (CODE)
                                   1236 ;--------------------------------------------------------
                                   1237 ; global & static initialisations
                                   1238 ;--------------------------------------------------------
                                   1239 	.area HOME    (CODE)
                                   1240 	.area GSINIT  (CODE)
                                   1241 	.area GSFINAL (CODE)
                                   1242 	.area GSINIT  (CODE)
                                   1243 ;--------------------------------------------------------
                                   1244 ; Home
                                   1245 ;--------------------------------------------------------
                                   1246 	.area HOME    (CODE)
                                   1247 	.area HOME    (CODE)
                                   1248 ;--------------------------------------------------------
                                   1249 ; code
                                   1250 ;--------------------------------------------------------
                                   1251 	.area CSEG    (CODE)
                                   1252 ;------------------------------------------------------------
                                   1253 ;Allocation info for local variables in function 'UBLOX_GPS_FletcherChecksum8'
                                   1254 ;------------------------------------------------------------
                                   1255 ;CK_A                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_2'
                                   1256 ;CK_B                      Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_3'
                                   1257 ;length                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_PARM_4'
                                   1258 ;buffer                    Allocated with name '_UBLOX_GPS_FletcherChecksum8_buffer_1_240'
                                   1259 ;i                         Allocated to registers r1 
                                   1260 ;sloc0                     Allocated with name '_UBLOX_GPS_FletcherChecksum8_sloc0_1_0'
                                   1261 ;------------------------------------------------------------
                           000000  1262 	G$UBLOX_GPS_FletcherChecksum8$0$0 ==.
                           000000  1263 	C$ublox_gnss.c$32$0$0 ==.
                                   1264 ;	..\COMMON\ublox_gnss.c:32: void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
                                   1265 ;	-----------------------------------------
                                   1266 ;	 function UBLOX_GPS_FletcherChecksum8
                                   1267 ;	-----------------------------------------
      005157                       1268 _UBLOX_GPS_FletcherChecksum8:
                           000007  1269 	ar7 = 0x07
                           000006  1270 	ar6 = 0x06
                           000005  1271 	ar5 = 0x05
                           000004  1272 	ar4 = 0x04
                           000003  1273 	ar3 = 0x03
                           000002  1274 	ar2 = 0x02
                           000001  1275 	ar1 = 0x01
                           000000  1276 	ar0 = 0x00
      005157 85 82 78         [24] 1277 	mov	_UBLOX_GPS_FletcherChecksum8_buffer_1_240,dpl
      00515A 85 83 79         [24] 1278 	mov	(_UBLOX_GPS_FletcherChecksum8_buffer_1_240 + 1),dph
      00515D 85 F0 7A         [24] 1279 	mov	(_UBLOX_GPS_FletcherChecksum8_buffer_1_240 + 2),b
                           000009  1280 	C$ublox_gnss.c$35$1$241 ==.
                                   1281 ;	..\COMMON\ublox_gnss.c:35: *CK_A = 0;
      005160 AA 70            [24] 1282 	mov	r2,_UBLOX_GPS_FletcherChecksum8_PARM_2
      005162 AB 71            [24] 1283 	mov	r3,(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1)
      005164 AC 72            [24] 1284 	mov	r4,(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2)
      005166 8A 82            [24] 1285 	mov	dpl,r2
      005168 8B 83            [24] 1286 	mov	dph,r3
      00516A 8C F0            [24] 1287 	mov	b,r4
      00516C E4               [12] 1288 	clr	a
      00516D 12 6D D2         [24] 1289 	lcall	__gptrput
                           000019  1290 	C$ublox_gnss.c$36$1$241 ==.
                                   1291 ;	..\COMMON\ublox_gnss.c:36: *CK_B = 0;
      005170 85 73 7B         [24] 1292 	mov	_UBLOX_GPS_FletcherChecksum8_sloc0_1_0,_UBLOX_GPS_FletcherChecksum8_PARM_3
      005173 85 74 7C         [24] 1293 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1),(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1)
      005176 85 75 7D         [24] 1294 	mov	(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2),(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2)
      005179 85 7B 82         [24] 1295 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      00517C 85 7C 83         [24] 1296 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      00517F 85 7D F0         [24] 1297 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      005182 12 6D D2         [24] 1298 	lcall	__gptrput
                           00002E  1299 	C$ublox_gnss.c$37$1$241 ==.
                                   1300 ;	..\COMMON\ublox_gnss.c:37: for (i = 0; i < length; i++)
      005185 79 00            [12] 1301 	mov	r1,#0x00
      005187                       1302 00103$:
      005187 89 00            [24] 1303 	mov	ar0,r1
      005189 7F 00            [12] 1304 	mov	r7,#0x00
      00518B C3               [12] 1305 	clr	c
      00518C E8               [12] 1306 	mov	a,r0
      00518D 95 76            [12] 1307 	subb	a,_UBLOX_GPS_FletcherChecksum8_PARM_4
      00518F EF               [12] 1308 	mov	a,r7
      005190 95 77            [12] 1309 	subb	a,(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1)
      005192 50 47            [24] 1310 	jnc	00101$
                           00003D  1311 	C$ublox_gnss.c$39$2$242 ==.
                                   1312 ;	..\COMMON\ublox_gnss.c:39: *CK_A += buffer[i];
      005194 8A 82            [24] 1313 	mov	dpl,r2
      005196 8B 83            [24] 1314 	mov	dph,r3
      005198 8C F0            [24] 1315 	mov	b,r4
      00519A 12 7C 55         [24] 1316 	lcall	__gptrget
      00519D FF               [12] 1317 	mov	r7,a
      00519E E9               [12] 1318 	mov	a,r1
      00519F 25 78            [12] 1319 	add	a,_UBLOX_GPS_FletcherChecksum8_buffer_1_240
      0051A1 F8               [12] 1320 	mov	r0,a
      0051A2 E4               [12] 1321 	clr	a
      0051A3 35 79            [12] 1322 	addc	a,(_UBLOX_GPS_FletcherChecksum8_buffer_1_240 + 1)
      0051A5 FD               [12] 1323 	mov	r5,a
      0051A6 AE 7A            [24] 1324 	mov	r6,(_UBLOX_GPS_FletcherChecksum8_buffer_1_240 + 2)
      0051A8 88 82            [24] 1325 	mov	dpl,r0
      0051AA 8D 83            [24] 1326 	mov	dph,r5
      0051AC 8E F0            [24] 1327 	mov	b,r6
      0051AE 12 7C 55         [24] 1328 	lcall	__gptrget
      0051B1 F8               [12] 1329 	mov	r0,a
      0051B2 2F               [12] 1330 	add	a,r7
      0051B3 FF               [12] 1331 	mov	r7,a
      0051B4 8A 82            [24] 1332 	mov	dpl,r2
      0051B6 8B 83            [24] 1333 	mov	dph,r3
      0051B8 8C F0            [24] 1334 	mov	b,r4
      0051BA 12 6D D2         [24] 1335 	lcall	__gptrput
                           000066  1336 	C$ublox_gnss.c$41$2$242 ==.
                                   1337 ;	..\COMMON\ublox_gnss.c:41: *CK_B += *CK_A;
      0051BD 85 7B 82         [24] 1338 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      0051C0 85 7C 83         [24] 1339 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      0051C3 85 7D F0         [24] 1340 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      0051C6 12 7C 55         [24] 1341 	lcall	__gptrget
      0051C9 FE               [12] 1342 	mov	r6,a
      0051CA 2F               [12] 1343 	add	a,r7
      0051CB FF               [12] 1344 	mov	r7,a
      0051CC 85 7B 82         [24] 1345 	mov	dpl,_UBLOX_GPS_FletcherChecksum8_sloc0_1_0
      0051CF 85 7C 83         [24] 1346 	mov	dph,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 1)
      0051D2 85 7D F0         [24] 1347 	mov	b,(_UBLOX_GPS_FletcherChecksum8_sloc0_1_0 + 2)
      0051D5 12 6D D2         [24] 1348 	lcall	__gptrput
                           000081  1349 	C$ublox_gnss.c$37$1$241 ==.
                                   1350 ;	..\COMMON\ublox_gnss.c:37: for (i = 0; i < length; i++)
      0051D8 09               [12] 1351 	inc	r1
      0051D9 80 AC            [24] 1352 	sjmp	00103$
      0051DB                       1353 00101$:
                           000084  1354 	C$ublox_gnss.c$43$1$241 ==.
                                   1355 ;	..\COMMON\ublox_gnss.c:43: return;
                           000084  1356 	C$ublox_gnss.c$44$1$241 ==.
                           000084  1357 	XG$UBLOX_GPS_FletcherChecksum8$0$0 ==.
      0051DB 22               [24] 1358 	ret
                                   1359 ;------------------------------------------------------------
                                   1360 ;Allocation info for local variables in function 'UBLOX_GPS_SendCommand_WaitACK'
                                   1361 ;------------------------------------------------------------
                                   1362 ;msg_id                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_2'
                                   1363 ;msg_length                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_3'
                                   1364 ;payload                   Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_4'
                                   1365 ;AnsOrAck                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_5'
                                   1366 ;RxLength                  Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_PARM_6'
                                   1367 ;msg_class                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_243'
                                   1368 ;i                         Allocated to registers r1 
                                   1369 ;k                         Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_k_1_244'
                                   1370 ;RetVal                    Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244'
                                   1371 ;buffer_aux                Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244'
                                   1372 ;rx_buffer                 Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244'
                                   1373 ;CK_A                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244'
                                   1374 ;CK_B                      Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244'
                                   1375 ;sloc0                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0'
                                   1376 ;sloc1                     Allocated with name '_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0'
                                   1377 ;------------------------------------------------------------
                           000085  1378 	G$UBLOX_GPS_SendCommand_WaitACK$0$0 ==.
                           000085  1379 	C$ublox_gnss.c$57$1$241 ==.
                                   1380 ;	..\COMMON\ublox_gnss.c:57: uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
                                   1381 ;	-----------------------------------------
                                   1382 ;	 function UBLOX_GPS_SendCommand_WaitACK
                                   1383 ;	-----------------------------------------
      0051DC                       1384 _UBLOX_GPS_SendCommand_WaitACK:
      0051DC 85 82 42         [24] 1385 	mov	_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_243,dpl
                           000088  1386 	C$ublox_gnss.c$60$1$241 ==.
                                   1387 ;	..\COMMON\ublox_gnss.c:60: uint8_t RetVal = 0;
      0051DF 75 44 00         [24] 1388 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244,#0x00
                           00008B  1389 	C$ublox_gnss.c$64$1$244 ==.
                                   1390 ;	..\COMMON\ublox_gnss.c:64: buffer_aux =(uint8_t *) malloc((msg_length)+10);
      0051E2 74 0A            [12] 1391 	mov	a,#0x0a
      0051E4 25 39            [12] 1392 	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0051E6 F5 82            [12] 1393 	mov	dpl,a
      0051E8 E4               [12] 1394 	clr	a
      0051E9 35 3A            [12] 1395 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      0051EB F5 83            [12] 1396 	mov	dph,a
      0051ED 12 6E E7         [24] 1397 	lcall	_malloc
      0051F0 AC 82            [24] 1398 	mov	r4,dpl
      0051F2 AD 83            [24] 1399 	mov	r5,dph
      0051F4 8C 45            [24] 1400 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244,r4
      0051F6 8D 46            [24] 1401 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1),r5
                           0000A1  1402 	C$ublox_gnss.c$65$1$244 ==.
                                   1403 ;	..\COMMON\ublox_gnss.c:65: rx_buffer =(uint8_t *) malloc(40);
      0051F8 90 00 28         [24] 1404 	mov	dptr,#0x0028
      0051FB 12 6E E7         [24] 1405 	lcall	_malloc
      0051FE AA 82            [24] 1406 	mov	r2,dpl
      005200 AB 83            [24] 1407 	mov	r3,dph
      005202 8A 47            [24] 1408 	mov	_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244,r2
      005204 8B 48            [24] 1409 	mov	(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1),r3
                           0000AF  1410 	C$ublox_gnss.c$66$1$244 ==.
                                   1411 ;	..\COMMON\ublox_gnss.c:66: for(i = 0; i<40;i++)
      005206 79 00            [12] 1412 	mov	r1,#0x00
      005208                       1413 00125$:
                           0000B1  1414 	C$ublox_gnss.c$68$2$245 ==.
                                   1415 ;	..\COMMON\ublox_gnss.c:68: *(rx_buffer+i)=0;
      005208 E9               [12] 1416 	mov	a,r1
      005209 25 47            [12] 1417 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      00520B F5 82            [12] 1418 	mov	dpl,a
      00520D E4               [12] 1419 	clr	a
      00520E 35 48            [12] 1420 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      005210 F5 83            [12] 1421 	mov	dph,a
      005212 E4               [12] 1422 	clr	a
      005213 F0               [24] 1423 	movx	@dptr,a
                           0000BD  1424 	C$ublox_gnss.c$66$1$244 ==.
                                   1425 ;	..\COMMON\ublox_gnss.c:66: for(i = 0; i<40;i++)
      005214 09               [12] 1426 	inc	r1
      005215 B9 28 00         [24] 1427 	cjne	r1,#0x28,00186$
      005218                       1428 00186$:
      005218 40 EE            [24] 1429 	jc	00125$
                           0000C3  1430 	C$ublox_gnss.c$71$1$244 ==.
                                   1431 ;	..\COMMON\ublox_gnss.c:71: *buffer_aux = msg_class;
      00521A 85 45 82         [24] 1432 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      00521D 85 46 83         [24] 1433 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      005220 E5 42            [12] 1434 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_243
      005222 F0               [24] 1435 	movx	@dptr,a
      005223 A3               [24] 1436 	inc	dptr
                           0000CD  1437 	C$ublox_gnss.c$72$1$244 ==.
                                   1438 ;	..\COMMON\ublox_gnss.c:72: buffer_aux++;
                           0000CD  1439 	C$ublox_gnss.c$73$1$244 ==.
                                   1440 ;	..\COMMON\ublox_gnss.c:73: *buffer_aux = msg_id;
      005224 85 82 45         [24] 1441 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244,dpl
      005227 85 83 46         [24] 1442 	mov  (_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1),dph
      00522A E5 38            [12] 1443 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      00522C F0               [24] 1444 	movx	@dptr,a
      00522D A3               [24] 1445 	inc	dptr
      00522E 85 82 45         [24] 1446 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244,dpl
      005231 85 83 46         [24] 1447 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1),dph
                           0000DD  1448 	C$ublox_gnss.c$74$1$244 ==.
                                   1449 ;	..\COMMON\ublox_gnss.c:74: buffer_aux++;
                           0000DD  1450 	C$ublox_gnss.c$75$1$244 ==.
                                   1451 ;	..\COMMON\ublox_gnss.c:75: *buffer_aux = msg_length;
      005234 A9 39            [24] 1452 	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      005236 85 45 82         [24] 1453 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      005239 85 46 83         [24] 1454 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      00523C E9               [12] 1455 	mov	a,r1
      00523D F0               [24] 1456 	movx	@dptr,a
                           0000E7  1457 	C$ublox_gnss.c$76$1$244 ==.
                                   1458 ;	..\COMMON\ublox_gnss.c:76: buffer_aux +=2;
      00523E 74 02            [12] 1459 	mov	a,#0x02
      005240 25 45            [12] 1460 	add	a,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      005242 F5 45            [12] 1461 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244,a
      005244 E4               [12] 1462 	clr	a
      005245 35 46            [12] 1463 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      005247 F5 46            [12] 1464 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1),a
                           0000F2  1465 	C$ublox_gnss.c$77$1$244 ==.
                                   1466 ;	..\COMMON\ublox_gnss.c:77: memcpy(buffer_aux,payload,msg_length);
      005249 A8 45            [24] 1467 	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      00524B A9 46            [24] 1468 	mov	r1,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      00524D 7E 00            [12] 1469 	mov	r6,#0x00
      00524F 85 3B 6E         [24] 1470 	mov	_memcpy_PARM_2,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      005252 85 3C 6F         [24] 1471 	mov	(_memcpy_PARM_2 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
      005255 85 3D 70         [24] 1472 	mov	(_memcpy_PARM_2 + 2),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
      005258 85 39 71         [24] 1473 	mov	_memcpy_PARM_3,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      00525B 85 3A 72         [24] 1474 	mov	(_memcpy_PARM_3 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      00525E 88 82            [24] 1475 	mov	dpl,r0
      005260 89 83            [24] 1476 	mov	dph,r1
      005262 8E F0            [24] 1477 	mov	b,r6
      005264 12 69 78         [24] 1478 	lcall	_memcpy
                           000110  1479 	C$ublox_gnss.c$78$1$244 ==.
                                   1480 ;	..\COMMON\ublox_gnss.c:78: buffer_aux -=4;
      005267 E5 45            [12] 1481 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      005269 24 FC            [12] 1482 	add	a,#0xfc
      00526B F5 45            [12] 1483 	mov	_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244,a
      00526D E5 46            [12] 1484 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      00526F 34 FF            [12] 1485 	addc	a,#0xff
      005271 F5 46            [12] 1486 	mov	(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1),a
                           00011C  1487 	C$ublox_gnss.c$79$1$244 ==.
                                   1488 ;	..\COMMON\ublox_gnss.c:79: UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);
      005273 A8 45            [24] 1489 	mov	r0,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      005275 A9 46            [24] 1490 	mov	r1,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      005277 7E 00            [12] 1491 	mov	r6,#0x00
      005279 75 70 49         [24] 1492 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_2,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244
                                   1493 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),#0x00
      00527C 8E 71            [24] 1494 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),r6
      00527E 75 72 40         [24] 1495 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2),#0x40
      005281 75 73 4A         [24] 1496 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_3,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244
                                   1497 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),#0x00
      005284 8E 74            [24] 1498 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),r6
      005286 75 75 40         [24] 1499 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2),#0x40
      005289 74 04            [12] 1500 	mov	a,#0x04
      00528B 25 39            [12] 1501 	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      00528D F5 76            [12] 1502 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_4,a
      00528F E4               [12] 1503 	clr	a
      005290 35 3A            [12] 1504 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      005292 F5 77            [12] 1505 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1),a
      005294 88 82            [24] 1506 	mov	dpl,r0
      005296 89 83            [24] 1507 	mov	dph,r1
      005298 8E F0            [24] 1508 	mov	b,r6
      00529A 12 51 57         [24] 1509 	lcall	_UBLOX_GPS_FletcherChecksum8
                           000146  1510 	C$ublox_gnss.c$81$1$244 ==.
                                   1511 ;	..\COMMON\ublox_gnss.c:81: uart1_tx(UBX_HEADER1_VAL);
      00529D 75 82 B5         [24] 1512 	mov	dpl,#0xb5
      0052A0 12 70 69         [24] 1513 	lcall	_uart1_tx
                           00014C  1514 	C$ublox_gnss.c$82$1$244 ==.
                                   1515 ;	..\COMMON\ublox_gnss.c:82: uart1_tx(UBX_HEADER2_VAL);
      0052A3 75 82 62         [24] 1516 	mov	dpl,#0x62
      0052A6 12 70 69         [24] 1517 	lcall	_uart1_tx
                           000152  1518 	C$ublox_gnss.c$83$1$244 ==.
                                   1519 ;	..\COMMON\ublox_gnss.c:83: uart1_tx(msg_class);
      0052A9 85 42 82         [24] 1520 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_243
      0052AC 12 70 69         [24] 1521 	lcall	_uart1_tx
                           000158  1522 	C$ublox_gnss.c$84$1$244 ==.
                                   1523 ;	..\COMMON\ublox_gnss.c:84: uart1_tx(msg_id);
      0052AF 85 38 82         [24] 1524 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_PARM_2
      0052B2 12 70 69         [24] 1525 	lcall	_uart1_tx
                           00015E  1526 	C$ublox_gnss.c$85$1$244 ==.
                                   1527 ;	..\COMMON\ublox_gnss.c:85: uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
      0052B5 A9 39            [24] 1528 	mov	r1,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0052B7 89 82            [24] 1529 	mov	dpl,r1
      0052B9 12 70 69         [24] 1530 	lcall	_uart1_tx
                           000165  1531 	C$ublox_gnss.c$86$1$244 ==.
                                   1532 ;	..\COMMON\ublox_gnss.c:86: uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB
      0052BC 79 00            [12] 1533 	mov	r1,#0x00
      0052BE AE 3A            [24] 1534 	mov	r6,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      0052C0 8E 82            [24] 1535 	mov	dpl,r6
      0052C2 12 70 69         [24] 1536 	lcall	_uart1_tx
                           00016E  1537 	C$ublox_gnss.c$89$1$244 ==.
                                   1538 ;	..\COMMON\ublox_gnss.c:89: for(i = 0;i<msg_length;i++)
      0052C5 7E 00            [12] 1539 	mov	r6,#0x00
      0052C7                       1540 00128$:
      0052C7 8E 00            [24] 1541 	mov	ar0,r6
      0052C9 79 00            [12] 1542 	mov	r1,#0x00
      0052CB C3               [12] 1543 	clr	c
      0052CC E8               [12] 1544 	mov	a,r0
      0052CD 95 39            [12] 1545 	subb	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_3
      0052CF E9               [12] 1546 	mov	a,r1
      0052D0 95 3A            [12] 1547 	subb	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1)
      0052D2 50 1C            [24] 1548 	jnc	00102$
                           00017D  1549 	C$ublox_gnss.c$91$2$246 ==.
                                   1550 ;	..\COMMON\ublox_gnss.c:91: uart1_tx(*(payload+i));
      0052D4 EE               [12] 1551 	mov	a,r6
      0052D5 25 3B            [12] 1552 	add	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      0052D7 F8               [12] 1553 	mov	r0,a
      0052D8 E4               [12] 1554 	clr	a
      0052D9 35 3C            [12] 1555 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
      0052DB F9               [12] 1556 	mov	r1,a
      0052DC AF 3D            [24] 1557 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
      0052DE 88 82            [24] 1558 	mov	dpl,r0
      0052E0 89 83            [24] 1559 	mov	dph,r1
      0052E2 8F F0            [24] 1560 	mov	b,r7
      0052E4 12 7C 55         [24] 1561 	lcall	__gptrget
      0052E7 F8               [12] 1562 	mov	r0,a
      0052E8 F5 82            [12] 1563 	mov	dpl,a
      0052EA 12 70 69         [24] 1564 	lcall	_uart1_tx
                           000196  1565 	C$ublox_gnss.c$89$1$244 ==.
                                   1566 ;	..\COMMON\ublox_gnss.c:89: for(i = 0;i<msg_length;i++)
      0052ED 0E               [12] 1567 	inc	r6
      0052EE 80 D7            [24] 1568 	sjmp	00128$
      0052F0                       1569 00102$:
                           000199  1570 	C$ublox_gnss.c$94$1$244 ==.
                                   1571 ;	..\COMMON\ublox_gnss.c:94: uart1_tx(CK_A);
      0052F0 85 49 82         [24] 1572 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244
      0052F3 12 70 69         [24] 1573 	lcall	_uart1_tx
                           00019F  1574 	C$ublox_gnss.c$95$1$244 ==.
                                   1575 ;	..\COMMON\ublox_gnss.c:95: uart1_tx(CK_B);
      0052F6 85 4A 82         [24] 1576 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244
      0052F9 12 70 69         [24] 1577 	lcall	_uart1_tx
                           0001A5  1578 	C$ublox_gnss.c$100$1$244 ==.
                                   1579 ;	..\COMMON\ublox_gnss.c:100: do{
      0052FC                       1580 00103$:
                           0001A5  1581 	C$ublox_gnss.c$101$2$247 ==.
                                   1582 ;	..\COMMON\ublox_gnss.c:101: delay(2500);
      0052FC 90 09 C4         [24] 1583 	mov	dptr,#0x09c4
      0052FF 12 75 74         [24] 1584 	lcall	_delay
                           0001AB  1585 	C$ublox_gnss.c$106$1$244 ==.
                                   1586 ;	..\COMMON\ublox_gnss.c:106: }while(!uart1_rxcount());
      005302 12 89 5C         [24] 1587 	lcall	_uart1_rxcount
      005305 E5 82            [12] 1588 	mov	a,dpl
      005307 60 F3            [24] 1589 	jz	00103$
                           0001B2  1590 	C$ublox_gnss.c$107$1$244 ==.
                                   1591 ;	..\COMMON\ublox_gnss.c:107: delay(25000);
      005309 90 61 A8         [24] 1592 	mov	dptr,#0x61a8
      00530C 12 75 74         [24] 1593 	lcall	_delay
                           0001B8  1594 	C$ublox_gnss.c$110$1$244 ==.
                                   1595 ;	..\COMMON\ublox_gnss.c:110: do
      00530F 75 43 00         [24] 1596 	mov	_UBLOX_GPS_SendCommand_WaitACK_k_1_244,#0x00
      005312                       1597 00106$:
                           0001BB  1598 	C$ublox_gnss.c$113$2$248 ==.
                                   1599 ;	..\COMMON\ublox_gnss.c:113: *(rx_buffer+k)=uart1_rx();
      005312 E5 43            [12] 1600 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_k_1_244
      005314 25 47            [12] 1601 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      005316 F9               [12] 1602 	mov	r1,a
      005317 E4               [12] 1603 	clr	a
      005318 35 48            [12] 1604 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      00531A FE               [12] 1605 	mov	r6,a
      00531B 12 6E 21         [24] 1606 	lcall	_uart1_rx
      00531E A8 82            [24] 1607 	mov	r0,dpl
      005320 89 82            [24] 1608 	mov	dpl,r1
      005322 8E 83            [24] 1609 	mov	dph,r6
      005324 E8               [12] 1610 	mov	a,r0
      005325 F0               [24] 1611 	movx	@dptr,a
                           0001CF  1612 	C$ublox_gnss.c$114$2$248 ==.
                                   1613 ;	..\COMMON\ublox_gnss.c:114: k++;
      005326 05 43            [12] 1614 	inc	_UBLOX_GPS_SendCommand_WaitACK_k_1_244
                           0001D1  1615 	C$ublox_gnss.c$116$2$248 ==.
                                   1616 ;	..\COMMON\ublox_gnss.c:116: delay(2500);
      005328 90 09 C4         [24] 1617 	mov	dptr,#0x09c4
      00532B 12 75 74         [24] 1618 	lcall	_delay
                           0001D7  1619 	C$ublox_gnss.c$117$1$244 ==.
                                   1620 ;	..\COMMON\ublox_gnss.c:117: }while(uart1_rxcount());
      00532E 12 89 5C         [24] 1621 	lcall	_uart1_rxcount
      005331 E5 82            [12] 1622 	mov	a,dpl
      005333 70 DD            [24] 1623 	jnz	00106$
                           0001DE  1624 	C$ublox_gnss.c$125$1$244 ==.
                                   1625 ;	..\COMMON\ublox_gnss.c:125: if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
      005335 85 47 82         [24] 1626 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      005338 85 48 83         [24] 1627 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      00533B E0               [24] 1628 	movx	a,@dptr
      00533C FE               [12] 1629 	mov	r6,a
      00533D BE B5 02         [24] 1630 	cjne	r6,#0xb5,00191$
      005340 80 03            [24] 1631 	sjmp	00192$
      005342                       1632 00191$:
      005342 02 54 98         [24] 1633 	ljmp	00123$
      005345                       1634 00192$:
      005345 85 47 82         [24] 1635 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      005348 85 48 83         [24] 1636 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      00534B A3               [24] 1637 	inc	dptr
      00534C E0               [24] 1638 	movx	a,@dptr
      00534D FE               [12] 1639 	mov	r6,a
      00534E BE 62 02         [24] 1640 	cjne	r6,#0x62,00193$
      005351 80 03            [24] 1641 	sjmp	00194$
      005353                       1642 00193$:
      005353 02 54 98         [24] 1643 	ljmp	00123$
      005356                       1644 00194$:
                           0001FF  1645 	C$ublox_gnss.c$127$2$249 ==.
                                   1646 ;	..\COMMON\ublox_gnss.c:127: CK_A = 0;
                           0001FF  1647 	C$ublox_gnss.c$128$2$249 ==.
                                   1648 ;	..\COMMON\ublox_gnss.c:128: CK_B = 0;
                           0001FF  1649 	C$ublox_gnss.c$129$2$249 ==.
                                   1650 ;	..\COMMON\ublox_gnss.c:129: UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
      005356 E4               [12] 1651 	clr	a
      005357 F5 49            [12] 1652 	mov	_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244,a
      005359 F5 4A            [12] 1653 	mov	_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244,a
      00535B 74 02            [12] 1654 	mov	a,#0x02
      00535D 25 47            [12] 1655 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      00535F F9               [12] 1656 	mov	r1,a
      005360 E4               [12] 1657 	clr	a
      005361 35 48            [12] 1658 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      005363 FE               [12] 1659 	mov	r6,a
      005364 89 00            [24] 1660 	mov	ar0,r1
      005366 8E 05            [24] 1661 	mov	ar5,r6
      005368 7F 00            [12] 1662 	mov	r7,#0x00
      00536A 75 70 49         [24] 1663 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_2,#_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244
                                   1664 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),#0x00
      00536D 8F 71            [24] 1665 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 1),r7
      00536F 75 72 40         [24] 1666 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_2 + 2),#0x40
      005372 75 73 4A         [24] 1667 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_3,#_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244
                                   1668 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),#0x00
      005375 8F 74            [24] 1669 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 1),r7
      005377 75 75 40         [24] 1670 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_3 + 2),#0x40
      00537A 85 43 4B         [24] 1671 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,_UBLOX_GPS_SendCommand_WaitACK_k_1_244
                                   1672 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),#0x00
      00537D 8F 4C            [24] 1673 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),r7
      00537F E5 4B            [12] 1674 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      005381 24 FC            [12] 1675 	add	a,#0xfc
      005383 F5 76            [12] 1676 	mov	_UBLOX_GPS_FletcherChecksum8_PARM_4,a
      005385 E5 4C            [12] 1677 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      005387 34 FF            [12] 1678 	addc	a,#0xff
      005389 F5 77            [12] 1679 	mov	(_UBLOX_GPS_FletcherChecksum8_PARM_4 + 1),a
      00538B 88 82            [24] 1680 	mov	dpl,r0
      00538D 8D 83            [24] 1681 	mov	dph,r5
      00538F 8F F0            [24] 1682 	mov	b,r7
      005391 C0 06            [24] 1683 	push	ar6
      005393 C0 01            [24] 1684 	push	ar1
      005395 12 51 57         [24] 1685 	lcall	_UBLOX_GPS_FletcherChecksum8
      005398 D0 01            [24] 1686 	pop	ar1
      00539A D0 06            [24] 1687 	pop	ar6
                           000245  1688 	C$ublox_gnss.c$130$2$249 ==.
                                   1689 ;	..\COMMON\ublox_gnss.c:130: if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
      00539C E5 4B            [12] 1690 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      00539E 24 FE            [12] 1691 	add	a,#0xfe
      0053A0 FD               [12] 1692 	mov	r5,a
      0053A1 E5 4C            [12] 1693 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      0053A3 34 FF            [12] 1694 	addc	a,#0xff
      0053A5 FF               [12] 1695 	mov	r7,a
      0053A6 ED               [12] 1696 	mov	a,r5
      0053A7 25 47            [12] 1697 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      0053A9 F5 82            [12] 1698 	mov	dpl,a
      0053AB EF               [12] 1699 	mov	a,r7
      0053AC 35 48            [12] 1700 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      0053AE F5 83            [12] 1701 	mov	dph,a
      0053B0 E0               [24] 1702 	movx	a,@dptr
      0053B1 FF               [12] 1703 	mov	r7,a
      0053B2 B5 49 02         [24] 1704 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_CK_A_1_244,00195$
      0053B5 80 03            [24] 1705 	sjmp	00196$
      0053B7                       1706 00195$:
      0053B7 02 54 98         [24] 1707 	ljmp	00123$
      0053BA                       1708 00196$:
      0053BA E5 4B            [12] 1709 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      0053BC 24 FF            [12] 1710 	add	a,#0xff
      0053BE FD               [12] 1711 	mov	r5,a
      0053BF E5 4C            [12] 1712 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      0053C1 34 FF            [12] 1713 	addc	a,#0xff
      0053C3 FF               [12] 1714 	mov	r7,a
      0053C4 ED               [12] 1715 	mov	a,r5
      0053C5 25 47            [12] 1716 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      0053C7 F5 82            [12] 1717 	mov	dpl,a
      0053C9 EF               [12] 1718 	mov	a,r7
      0053CA 35 48            [12] 1719 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      0053CC F5 83            [12] 1720 	mov	dph,a
      0053CE E0               [24] 1721 	movx	a,@dptr
      0053CF FF               [12] 1722 	mov	r7,a
      0053D0 B5 4A 02         [24] 1723 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_CK_B_1_244,00197$
      0053D3 80 03            [24] 1724 	sjmp	00198$
      0053D5                       1725 00197$:
      0053D5 02 54 98         [24] 1726 	ljmp	00123$
      0053D8                       1727 00198$:
                           000281  1728 	C$ublox_gnss.c$133$3$250 ==.
                                   1729 ;	..\COMMON\ublox_gnss.c:133: if(!AnsOrAck)
      0053D8 E5 3E            [12] 1730 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_5
      0053DA 60 03            [24] 1731 	jz	00199$
      0053DC 02 54 79         [24] 1732 	ljmp	00117$
      0053DF                       1733 00199$:
                           000288  1734 	C$ublox_gnss.c$136$4$251 ==.
                                   1735 ;	..\COMMON\ublox_gnss.c:136: if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
      0053DF 89 82            [24] 1736 	mov	dpl,r1
      0053E1 8E 83            [24] 1737 	mov	dph,r6
      0053E3 E0               [24] 1738 	movx	a,@dptr
      0053E4 FF               [12] 1739 	mov	r7,a
      0053E5 B5 42 02         [24] 1740 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_msg_class_1_243,00200$
      0053E8 80 03            [24] 1741 	sjmp	00201$
      0053EA                       1742 00200$:
      0053EA 02 54 98         [24] 1743 	ljmp	00123$
      0053ED                       1744 00201$:
      0053ED 85 47 82         [24] 1745 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      0053F0 85 48 83         [24] 1746 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      0053F3 A3               [24] 1747 	inc	dptr
      0053F4 A3               [24] 1748 	inc	dptr
      0053F5 A3               [24] 1749 	inc	dptr
      0053F6 E0               [24] 1750 	movx	a,@dptr
      0053F7 FF               [12] 1751 	mov	r7,a
      0053F8 B5 38 02         [24] 1752 	cjne	a,_UBLOX_GPS_SendCommand_WaitACK_PARM_2,00202$
      0053FB 80 03            [24] 1753 	sjmp	00203$
      0053FD                       1754 00202$:
      0053FD 02 54 98         [24] 1755 	ljmp	00123$
      005400                       1756 00203$:
                           0002A9  1757 	C$ublox_gnss.c$138$5$252 ==.
                                   1758 ;	..\COMMON\ublox_gnss.c:138: *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
      005400 AC 3F            [24] 1759 	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_PARM_6
      005402 AD 40            [24] 1760 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1)
      005404 AF 41            [24] 1761 	mov	r7,(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2)
      005406 85 47 82         [24] 1762 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      005409 85 48 83         [24] 1763 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      00540C A3               [24] 1764 	inc	dptr
      00540D A3               [24] 1765 	inc	dptr
      00540E A3               [24] 1766 	inc	dptr
      00540F A3               [24] 1767 	inc	dptr
      005410 A3               [24] 1768 	inc	dptr
      005411 E0               [24] 1769 	movx	a,@dptr
      005412 F8               [12] 1770 	mov	r0,a
      005413 7B 00            [12] 1771 	mov	r3,#0x00
      005415 88 4C            [24] 1772 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1),r0
                                   1773 ;	1-genFromRTrack replaced	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,#0x00
      005417 8B 4B            [24] 1774 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0,r3
      005419 85 47 82         [24] 1775 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      00541C 85 48 83         [24] 1776 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      00541F A3               [24] 1777 	inc	dptr
      005420 A3               [24] 1778 	inc	dptr
      005421 A3               [24] 1779 	inc	dptr
      005422 A3               [24] 1780 	inc	dptr
      005423 E0               [24] 1781 	movx	a,@dptr
      005424 FA               [12] 1782 	mov	r2,a
      005425 7B 00            [12] 1783 	mov	r3,#0x00
      005427 E5 4B            [12] 1784 	mov	a,_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0
      005429 42 02            [12] 1785 	orl	ar2,a
      00542B E5 4C            [12] 1786 	mov	a,(_UBLOX_GPS_SendCommand_WaitACK_sloc0_1_0 + 1)
      00542D 42 03            [12] 1787 	orl	ar3,a
      00542F 8C 82            [24] 1788 	mov	dpl,r4
      005431 8D 83            [24] 1789 	mov	dph,r5
      005433 8F F0            [24] 1790 	mov	b,r7
      005435 EA               [12] 1791 	mov	a,r2
      005436 12 6D D2         [24] 1792 	lcall	__gptrput
      005439 A3               [24] 1793 	inc	dptr
      00543A EB               [12] 1794 	mov	a,r3
      00543B 12 6D D2         [24] 1795 	lcall	__gptrput
                           0002E7  1796 	C$ublox_gnss.c$139$5$252 ==.
                                   1797 ;	..\COMMON\ublox_gnss.c:139: memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
      00543E 85 3B 4D         [24] 1798 	mov	_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0,_UBLOX_GPS_SendCommand_WaitACK_PARM_4
      005441 85 3C 4E         [24] 1799 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1)
      005444 85 3D 4F         [24] 1800 	mov	(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2),(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2)
      005447 74 06            [12] 1801 	mov	a,#0x06
      005449 25 47            [12] 1802 	add	a,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      00544B FA               [12] 1803 	mov	r2,a
      00544C E4               [12] 1804 	clr	a
      00544D 35 48            [12] 1805 	addc	a,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      00544F FB               [12] 1806 	mov	r3,a
      005450 8A 6E            [24] 1807 	mov	_memcpy_PARM_2,r2
      005452 8B 6F            [24] 1808 	mov	(_memcpy_PARM_2 + 1),r3
      005454 75 70 00         [24] 1809 	mov	(_memcpy_PARM_2 + 2),#0x00
      005457 8C 82            [24] 1810 	mov	dpl,r4
      005459 8D 83            [24] 1811 	mov	dph,r5
      00545B 8F F0            [24] 1812 	mov	b,r7
      00545D 12 7C 55         [24] 1813 	lcall	__gptrget
      005460 F5 71            [12] 1814 	mov	_memcpy_PARM_3,a
      005462 A3               [24] 1815 	inc	dptr
      005463 12 7C 55         [24] 1816 	lcall	__gptrget
      005466 F5 72            [12] 1817 	mov	(_memcpy_PARM_3 + 1),a
      005468 85 4D 82         [24] 1818 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0
      00546B 85 4E 83         [24] 1819 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 1)
      00546E 85 4F F0         [24] 1820 	mov	b,(_UBLOX_GPS_SendCommand_WaitACK_sloc1_1_0 + 2)
      005471 12 69 78         [24] 1821 	lcall	_memcpy
                           00031D  1822 	C$ublox_gnss.c$140$5$252 ==.
                                   1823 ;	..\COMMON\ublox_gnss.c:140: RetVal = OK;
      005474 75 44 01         [24] 1824 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244,#0x01
      005477 80 1F            [24] 1825 	sjmp	00123$
      005479                       1826 00117$:
                           000322  1827 	C$ublox_gnss.c$146$4$253 ==.
                                   1828 ;	..\COMMON\ublox_gnss.c:146: if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
      005479 89 82            [24] 1829 	mov	dpl,r1
      00547B 8E 83            [24] 1830 	mov	dph,r6
      00547D E0               [24] 1831 	movx	a,@dptr
      00547E F9               [12] 1832 	mov	r1,a
      00547F B9 05 13         [24] 1833 	cjne	r1,#0x05,00113$
      005482 85 47 82         [24] 1834 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      005485 85 48 83         [24] 1835 	mov	dph,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      005488 A3               [24] 1836 	inc	dptr
      005489 A3               [24] 1837 	inc	dptr
      00548A A3               [24] 1838 	inc	dptr
      00548B E0               [24] 1839 	movx	a,@dptr
      00548C FF               [12] 1840 	mov	r7,a
      00548D BF 01 05         [24] 1841 	cjne	r7,#0x01,00113$
      005490 75 44 01         [24] 1842 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244,#0x01
      005493 80 03            [24] 1843 	sjmp	00123$
      005495                       1844 00113$:
                           00033E  1845 	C$ublox_gnss.c$147$4$253 ==.
                                   1846 ;	..\COMMON\ublox_gnss.c:147: else RetVal = NAK;
      005495 75 44 00         [24] 1847 	mov	_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244,#0x00
      005498                       1848 00123$:
                           000341  1849 	C$ublox_gnss.c$151$1$244 ==.
                                   1850 ;	..\COMMON\ublox_gnss.c:151: free(buffer_aux);
      005498 AC 45            [24] 1851 	mov	r4,_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244
      00549A AD 46            [24] 1852 	mov	r5,(_UBLOX_GPS_SendCommand_WaitACK_buffer_aux_1_244 + 1)
      00549C 7F 00            [12] 1853 	mov	r7,#0x00
      00549E 8C 82            [24] 1854 	mov	dpl,r4
      0054A0 8D 83            [24] 1855 	mov	dph,r5
      0054A2 8F F0            [24] 1856 	mov	b,r7
      0054A4 12 5E 16         [24] 1857 	lcall	_free
                           000350  1858 	C$ublox_gnss.c$152$1$244 ==.
                                   1859 ;	..\COMMON\ublox_gnss.c:152: free(rx_buffer);
      0054A7 AA 47            [24] 1860 	mov	r2,_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244
      0054A9 AB 48            [24] 1861 	mov	r3,(_UBLOX_GPS_SendCommand_WaitACK_rx_buffer_1_244 + 1)
      0054AB 7F 00            [12] 1862 	mov	r7,#0x00
      0054AD 8A 82            [24] 1863 	mov	dpl,r2
      0054AF 8B 83            [24] 1864 	mov	dph,r3
      0054B1 8F F0            [24] 1865 	mov	b,r7
      0054B3 12 5E 16         [24] 1866 	lcall	_free
                           00035F  1867 	C$ublox_gnss.c$153$1$244 ==.
                                   1868 ;	..\COMMON\ublox_gnss.c:153: return RetVal;
      0054B6 85 44 82         [24] 1869 	mov	dpl,_UBLOX_GPS_SendCommand_WaitACK_RetVal_1_244
                           000362  1870 	C$ublox_gnss.c$154$1$244 ==.
                           000362  1871 	XG$UBLOX_GPS_SendCommand_WaitACK$0$0 ==.
      0054B9 22               [24] 1872 	ret
                                   1873 ;------------------------------------------------------------
                                   1874 ;Allocation info for local variables in function 'UBLOX_GPS_PortInit'
                                   1875 ;------------------------------------------------------------
                                   1876 ;UBLOX_setNav1             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255'
                                   1877 ;UBLOX_setNav2             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255'
                                   1878 ;UBLOX_setNav3             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255'
                                   1879 ;UBLOX_setNav5             Allocated with name '_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255'
                                   1880 ;------------------------------------------------------------
                           000363  1881 	G$UBLOX_GPS_PortInit$0$0 ==.
                           000363  1882 	C$ublox_gnss.c$161$1$244 ==.
                                   1883 ;	..\COMMON\ublox_gnss.c:161: void UBLOX_GPS_PortInit(void)
                                   1884 ;	-----------------------------------------
                                   1885 ;	 function UBLOX_GPS_PortInit
                                   1886 ;	-----------------------------------------
      0054BA                       1887 _UBLOX_GPS_PortInit:
                           000363  1888 	C$ublox_gnss.c$163$1$244 ==.
                                   1889 ;	..\COMMON\ublox_gnss.c:163: __xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
      0054BA 90 03 9F         [24] 1890 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255
      0054BD 74 F0            [12] 1891 	mov	a,#0xf0
      0054BF F0               [24] 1892 	movx	@dptr,a
      0054C0 90 03 A0         [24] 1893 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0001)
      0054C3 74 04            [12] 1894 	mov	a,#0x04
      0054C5 F0               [24] 1895 	movx	@dptr,a
      0054C6 90 03 A1         [24] 1896 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0002)
      0054C9 E4               [12] 1897 	clr	a
      0054CA F0               [24] 1898 	movx	@dptr,a
                           000374  1899 	C$ublox_gnss.c$164$1$244 ==.
                                   1900 ;	..\COMMON\ublox_gnss.c:164: __xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
      0054CB 90 03 A2         [24] 1901 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255
      0054CE 74 F0            [12] 1902 	mov	a,#0xf0
      0054D0 F0               [24] 1903 	movx	@dptr,a
      0054D1 90 03 A3         [24] 1904 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0001)
      0054D4 74 01            [12] 1905 	mov	a,#0x01
      0054D6 F0               [24] 1906 	movx	@dptr,a
      0054D7 90 03 A4         [24] 1907 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0002)
      0054DA E4               [12] 1908 	clr	a
      0054DB F0               [24] 1909 	movx	@dptr,a
      0054DC 90 03 A5         [24] 1910 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0003)
      0054DF F0               [24] 1911 	movx	@dptr,a
      0054E0 90 03 A6         [24] 1912 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0004)
      0054E3 F0               [24] 1913 	movx	@dptr,a
      0054E4 90 03 A7         [24] 1914 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0005)
      0054E7 F0               [24] 1915 	movx	@dptr,a
      0054E8 90 03 A8         [24] 1916 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0006)
      0054EB F0               [24] 1917 	movx	@dptr,a
      0054EC 90 03 A9         [24] 1918 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 + 0x0007)
      0054EF 04               [12] 1919 	inc	a
      0054F0 F0               [24] 1920 	movx	@dptr,a
                           00039A  1921 	C$ublox_gnss.c$165$1$244 ==.
                                   1922 ;	..\COMMON\ublox_gnss.c:165: __xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
      0054F1 90 03 AA         [24] 1923 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255
      0054F4 74 F0            [12] 1924 	mov	a,#0xf0
      0054F6 F0               [24] 1925 	movx	@dptr,a
      0054F7 90 03 AB         [24] 1926 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0001)
      0054FA 74 04            [12] 1927 	mov	a,#0x04
      0054FC F0               [24] 1928 	movx	@dptr,a
      0054FD 90 03 AC         [24] 1929 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0002)
      005500 E4               [12] 1930 	clr	a
      005501 F0               [24] 1931 	movx	@dptr,a
      005502 90 03 AD         [24] 1932 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0003)
      005505 F0               [24] 1933 	movx	@dptr,a
      005506 90 03 AE         [24] 1934 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0004)
      005509 F0               [24] 1935 	movx	@dptr,a
      00550A 90 03 AF         [24] 1936 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0005)
      00550D F0               [24] 1937 	movx	@dptr,a
      00550E 90 03 B0         [24] 1938 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0006)
      005511 F0               [24] 1939 	movx	@dptr,a
      005512 90 03 B1         [24] 1940 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 + 0x0007)
      005515 04               [12] 1941 	inc	a
      005516 F0               [24] 1942 	movx	@dptr,a
                           0003C0  1943 	C$ublox_gnss.c$166$1$244 ==.
                                   1944 ;	..\COMMON\ublox_gnss.c:166: __xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
      005517 90 03 B2         [24] 1945 	mov	dptr,#_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255
      00551A 74 FF            [12] 1946 	mov	a,#0xff
      00551C F0               [24] 1947 	movx	@dptr,a
      00551D 90 03 B3         [24] 1948 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0001)
      005520 F0               [24] 1949 	movx	@dptr,a
      005521 90 03 B4         [24] 1950 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0002)
      005524 74 06            [12] 1951 	mov	a,#0x06
      005526 F0               [24] 1952 	movx	@dptr,a
      005527 90 03 B5         [24] 1953 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0003)
      00552A 03               [12] 1954 	rr	a
      00552B F0               [24] 1955 	movx	@dptr,a
      00552C 90 03 B6         [24] 1956 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0004)
      00552F E4               [12] 1957 	clr	a
      005530 F0               [24] 1958 	movx	@dptr,a
      005531 90 03 B7         [24] 1959 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0005)
      005534 F0               [24] 1960 	movx	@dptr,a
      005535 90 03 B8         [24] 1961 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0006)
      005538 F0               [24] 1962 	movx	@dptr,a
      005539 90 03 B9         [24] 1963 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0007)
      00553C F0               [24] 1964 	movx	@dptr,a
      00553D 90 03 BA         [24] 1965 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0008)
      005540 74 10            [12] 1966 	mov	a,#0x10
      005542 F0               [24] 1967 	movx	@dptr,a
      005543 90 03 BB         [24] 1968 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0009)
      005546 74 27            [12] 1969 	mov	a,#0x27
      005548 F0               [24] 1970 	movx	@dptr,a
      005549 90 03 BC         [24] 1971 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x000a)
      00554C E4               [12] 1972 	clr	a
      00554D F0               [24] 1973 	movx	@dptr,a
      00554E 90 03 BD         [24] 1974 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x000b)
      005551 F0               [24] 1975 	movx	@dptr,a
      005552 90 03 BE         [24] 1976 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x000c)
      005555 74 05            [12] 1977 	mov	a,#0x05
      005557 F0               [24] 1978 	movx	@dptr,a
      005558 90 03 BF         [24] 1979 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x000d)
      00555B E4               [12] 1980 	clr	a
      00555C F0               [24] 1981 	movx	@dptr,a
      00555D 90 03 C0         [24] 1982 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x000e)
      005560 74 FA            [12] 1983 	mov	a,#0xfa
      005562 F0               [24] 1984 	movx	@dptr,a
      005563 90 03 C1         [24] 1985 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x000f)
      005566 E4               [12] 1986 	clr	a
      005567 F0               [24] 1987 	movx	@dptr,a
      005568 90 03 C2         [24] 1988 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0010)
      00556B 74 FA            [12] 1989 	mov	a,#0xfa
      00556D F0               [24] 1990 	movx	@dptr,a
      00556E 90 03 C3         [24] 1991 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0011)
      005571 E4               [12] 1992 	clr	a
      005572 F0               [24] 1993 	movx	@dptr,a
      005573 90 03 C4         [24] 1994 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0012)
      005576 74 64            [12] 1995 	mov	a,#0x64
      005578 F0               [24] 1996 	movx	@dptr,a
      005579 90 03 C5         [24] 1997 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0013)
      00557C E4               [12] 1998 	clr	a
      00557D F0               [24] 1999 	movx	@dptr,a
      00557E 90 03 C6         [24] 2000 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0014)
      005581 74 2C            [12] 2001 	mov	a,#0x2c
      005583 F0               [24] 2002 	movx	@dptr,a
      005584 90 03 C7         [24] 2003 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0015)
      005587 74 01            [12] 2004 	mov	a,#0x01
      005589 F0               [24] 2005 	movx	@dptr,a
      00558A 90 03 C8         [24] 2006 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0016)
      00558D E4               [12] 2007 	clr	a
      00558E F0               [24] 2008 	movx	@dptr,a
      00558F 90 03 C9         [24] 2009 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0017)
      005592 F0               [24] 2010 	movx	@dptr,a
      005593 90 03 CA         [24] 2011 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0018)
      005596 F0               [24] 2012 	movx	@dptr,a
      005597 90 03 CB         [24] 2013 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0019)
      00559A F0               [24] 2014 	movx	@dptr,a
      00559B 90 03 CC         [24] 2015 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x001a)
      00559E F0               [24] 2016 	movx	@dptr,a
      00559F 90 03 CD         [24] 2017 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x001b)
      0055A2 F0               [24] 2018 	movx	@dptr,a
      0055A3 90 03 CE         [24] 2019 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x001c)
      0055A6 F0               [24] 2020 	movx	@dptr,a
      0055A7 90 03 CF         [24] 2021 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x001d)
      0055AA F0               [24] 2022 	movx	@dptr,a
      0055AB 90 03 D0         [24] 2023 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x001e)
      0055AE F0               [24] 2024 	movx	@dptr,a
      0055AF 90 03 D1         [24] 2025 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x001f)
      0055B2 F0               [24] 2026 	movx	@dptr,a
      0055B3 90 03 D2         [24] 2027 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0020)
      0055B6 F0               [24] 2028 	movx	@dptr,a
      0055B7 90 03 D3         [24] 2029 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0021)
      0055BA F0               [24] 2030 	movx	@dptr,a
      0055BB 90 03 D4         [24] 2031 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0022)
      0055BE F0               [24] 2032 	movx	@dptr,a
      0055BF 90 03 D5         [24] 2033 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 + 0x0023)
      0055C2 F0               [24] 2034 	movx	@dptr,a
                           00046C  2035 	C$ublox_gnss.c$169$1$255 ==.
                                   2036 ;	..\COMMON\ublox_gnss.c:169: PORTA_1 = 1;
      0055C3 D2 81            [12] 2037 	setb	_PORTA_1
                           00046E  2038 	C$ublox_gnss.c$170$1$255 ==.
                                   2039 ;	..\COMMON\ublox_gnss.c:170: uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
      0055C5 E4               [12] 2040 	clr	a
      0055C6 C0 E0            [24] 2041 	push	acc
      0055C8 74 2D            [12] 2042 	mov	a,#0x2d
      0055CA C0 E0            [24] 2043 	push	acc
      0055CC 74 31            [12] 2044 	mov	a,#0x31
      0055CE C0 E0            [24] 2045 	push	acc
      0055D0 74 01            [12] 2046 	mov	a,#0x01
      0055D2 C0 E0            [24] 2047 	push	acc
      0055D4 03               [12] 2048 	rr	a
      0055D5 C0 E0            [24] 2049 	push	acc
      0055D7 74 25            [12] 2050 	mov	a,#0x25
      0055D9 C0 E0            [24] 2051 	push	acc
      0055DB E4               [12] 2052 	clr	a
      0055DC C0 E0            [24] 2053 	push	acc
      0055DE C0 E0            [24] 2054 	push	acc
      0055E0 75 82 00         [24] 2055 	mov	dpl,#0x00
      0055E3 12 64 9E         [24] 2056 	lcall	_uart_timer1_baud
      0055E6 E5 81            [12] 2057 	mov	a,sp
      0055E8 24 F8            [12] 2058 	add	a,#0xf8
      0055EA F5 81            [12] 2059 	mov	sp,a
                           000495  2060 	C$ublox_gnss.c$171$1$255 ==.
                                   2061 ;	..\COMMON\ublox_gnss.c:171: uart1_init(1, 8, 1);
      0055EC 75 6E 08         [24] 2062 	mov	_uart1_init_PARM_2,#0x08
      0055EF 75 6F 01         [24] 2063 	mov	_uart1_init_PARM_3,#0x01
      0055F2 75 82 01         [24] 2064 	mov	dpl,#0x01
      0055F5 12 5D DA         [24] 2065 	lcall	_uart1_init
                           0004A1  2066 	C$ublox_gnss.c$178$1$255 ==.
                                   2067 ;	..\COMMON\ublox_gnss.c:178: UBLOX_setNav1[1]=0x02;
      0055F8 90 03 A0         [24] 2068 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0001)
      0055FB 74 02            [12] 2069 	mov	a,#0x02
      0055FD F0               [24] 2070 	movx	@dptr,a
                           0004A7  2071 	C$ublox_gnss.c$179$1$255 ==.
                                   2072 ;	..\COMMON\ublox_gnss.c:179: do{
      0055FE                       2073 00101$:
                           0004A7  2074 	C$ublox_gnss.c$180$2$256 ==.
                                   2075 ;	..\COMMON\ublox_gnss.c:180: delay(5000);
      0055FE 90 13 88         [24] 2076 	mov	dptr,#0x1388
      005601 12 75 74         [24] 2077 	lcall	_delay
                           0004AD  2078 	C$ublox_gnss.c$181$1$255 ==.
                                   2079 ;	..\COMMON\ublox_gnss.c:181: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      005604 75 3B 9F         [24] 2080 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255
      005607 75 3C 03         [24] 2081 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 >> 8)
      00560A 75 3D 00         [24] 2082 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      00560D 75 38 01         [24] 2083 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      005610 75 39 03         [24] 2084 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      005613 75 3A 00         [24] 2085 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      005616 75 3E 01         [24] 2086 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      005619 E4               [12] 2087 	clr	a
      00561A F5 3F            [12] 2088 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      00561C F5 40            [12] 2089 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2090 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      00561E F5 41            [12] 2091 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      005620 75 82 06         [24] 2092 	mov	dpl,#0x06
      005623 12 51 DC         [24] 2093 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      005626 E5 82            [12] 2094 	mov	a,dpl
      005628 60 D4            [24] 2095 	jz	00101$
                           0004D3  2096 	C$ublox_gnss.c$186$1$255 ==.
                                   2097 ;	..\COMMON\ublox_gnss.c:186: UBLOX_setNav1[1]=0x01;
      00562A 90 03 A0         [24] 2098 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0001)
      00562D 74 01            [12] 2099 	mov	a,#0x01
      00562F F0               [24] 2100 	movx	@dptr,a
                           0004D9  2101 	C$ublox_gnss.c$187$1$255 ==.
                                   2102 ;	..\COMMON\ublox_gnss.c:187: do{
      005630                       2103 00104$:
                           0004D9  2104 	C$ublox_gnss.c$188$2$257 ==.
                                   2105 ;	..\COMMON\ublox_gnss.c:188: delay(5000);
      005630 90 13 88         [24] 2106 	mov	dptr,#0x1388
      005633 12 75 74         [24] 2107 	lcall	_delay
                           0004DF  2108 	C$ublox_gnss.c$189$1$255 ==.
                                   2109 ;	..\COMMON\ublox_gnss.c:189: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      005636 75 3B 9F         [24] 2110 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255
      005639 75 3C 03         [24] 2111 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 >> 8)
      00563C 75 3D 00         [24] 2112 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      00563F 75 38 01         [24] 2113 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      005642 75 39 03         [24] 2114 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      005645 75 3A 00         [24] 2115 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      005648 75 3E 01         [24] 2116 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      00564B E4               [12] 2117 	clr	a
      00564C F5 3F            [12] 2118 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      00564E F5 40            [12] 2119 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2120 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      005650 F5 41            [12] 2121 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      005652 75 82 06         [24] 2122 	mov	dpl,#0x06
      005655 12 51 DC         [24] 2123 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      005658 E5 82            [12] 2124 	mov	a,dpl
      00565A 60 D4            [24] 2125 	jz	00104$
                           000505  2126 	C$ublox_gnss.c$195$1$255 ==.
                                   2127 ;	..\COMMON\ublox_gnss.c:195: UBLOX_setNav1[1]=0x00;
      00565C 90 03 A0         [24] 2128 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0001)
      00565F E4               [12] 2129 	clr	a
      005660 F0               [24] 2130 	movx	@dptr,a
                           00050A  2131 	C$ublox_gnss.c$196$1$255 ==.
                                   2132 ;	..\COMMON\ublox_gnss.c:196: do{
      005661                       2133 00107$:
                           00050A  2134 	C$ublox_gnss.c$197$2$258 ==.
                                   2135 ;	..\COMMON\ublox_gnss.c:197: delay(5000);
      005661 90 13 88         [24] 2136 	mov	dptr,#0x1388
      005664 12 75 74         [24] 2137 	lcall	_delay
                           000510  2138 	C$ublox_gnss.c$198$1$255 ==.
                                   2139 ;	..\COMMON\ublox_gnss.c:198: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      005667 75 3B 9F         [24] 2140 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255
      00566A 75 3C 03         [24] 2141 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 >> 8)
      00566D 75 3D 00         [24] 2142 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      005670 75 38 01         [24] 2143 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      005673 75 39 03         [24] 2144 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      005676 75 3A 00         [24] 2145 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      005679 75 3E 01         [24] 2146 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      00567C E4               [12] 2147 	clr	a
      00567D F5 3F            [12] 2148 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      00567F F5 40            [12] 2149 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2150 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      005681 F5 41            [12] 2151 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      005683 75 82 06         [24] 2152 	mov	dpl,#0x06
      005686 12 51 DC         [24] 2153 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      005689 E5 82            [12] 2154 	mov	a,dpl
      00568B 60 D4            [24] 2155 	jz	00107$
                           000536  2156 	C$ublox_gnss.c$204$1$255 ==.
                                   2157 ;	..\COMMON\ublox_gnss.c:204: UBLOX_setNav1[1]=0x03;
      00568D 90 03 A0         [24] 2158 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0001)
      005690 74 03            [12] 2159 	mov	a,#0x03
      005692 F0               [24] 2160 	movx	@dptr,a
                           00053C  2161 	C$ublox_gnss.c$205$1$255 ==.
                                   2162 ;	..\COMMON\ublox_gnss.c:205: do{
      005693                       2163 00110$:
                           00053C  2164 	C$ublox_gnss.c$206$2$259 ==.
                                   2165 ;	..\COMMON\ublox_gnss.c:206: delay(5000);
      005693 90 13 88         [24] 2166 	mov	dptr,#0x1388
      005696 12 75 74         [24] 2167 	lcall	_delay
                           000542  2168 	C$ublox_gnss.c$207$1$255 ==.
                                   2169 ;	..\COMMON\ublox_gnss.c:207: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      005699 75 3B 9F         [24] 2170 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255
      00569C 75 3C 03         [24] 2171 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 >> 8)
      00569F 75 3D 00         [24] 2172 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      0056A2 75 38 01         [24] 2173 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      0056A5 75 39 03         [24] 2174 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      0056A8 75 3A 00         [24] 2175 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      0056AB 75 3E 01         [24] 2176 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      0056AE E4               [12] 2177 	clr	a
      0056AF F5 3F            [12] 2178 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      0056B1 F5 40            [12] 2179 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2180 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      0056B3 F5 41            [12] 2181 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      0056B5 75 82 06         [24] 2182 	mov	dpl,#0x06
      0056B8 12 51 DC         [24] 2183 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      0056BB E5 82            [12] 2184 	mov	a,dpl
      0056BD 60 D4            [24] 2185 	jz	00110$
                           000568  2186 	C$ublox_gnss.c$213$1$255 ==.
                                   2187 ;	..\COMMON\ublox_gnss.c:213: UBLOX_setNav1[1]=0x05;
      0056BF 90 03 A0         [24] 2188 	mov	dptr,#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 + 0x0001)
      0056C2 74 05            [12] 2189 	mov	a,#0x05
      0056C4 F0               [24] 2190 	movx	@dptr,a
                           00056E  2191 	C$ublox_gnss.c$214$1$255 ==.
                                   2192 ;	..\COMMON\ublox_gnss.c:214: do{
      0056C5                       2193 00113$:
                           00056E  2194 	C$ublox_gnss.c$215$2$260 ==.
                                   2195 ;	..\COMMON\ublox_gnss.c:215: delay(5000);
      0056C5 90 13 88         [24] 2196 	mov	dptr,#0x1388
      0056C8 12 75 74         [24] 2197 	lcall	_delay
                           000574  2198 	C$ublox_gnss.c$216$1$255 ==.
                                   2199 ;	..\COMMON\ublox_gnss.c:216: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
      0056CB 75 3B 9F         [24] 2200 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255
      0056CE 75 3C 03         [24] 2201 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav1_1_255 >> 8)
      0056D1 75 3D 00         [24] 2202 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      0056D4 75 38 01         [24] 2203 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      0056D7 75 39 03         [24] 2204 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x03
      0056DA 75 3A 00         [24] 2205 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      0056DD 75 3E 01         [24] 2206 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      0056E0 E4               [12] 2207 	clr	a
      0056E1 F5 3F            [12] 2208 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      0056E3 F5 40            [12] 2209 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2210 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      0056E5 F5 41            [12] 2211 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      0056E7 75 82 06         [24] 2212 	mov	dpl,#0x06
      0056EA 12 51 DC         [24] 2213 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      0056ED E5 82            [12] 2214 	mov	a,dpl
      0056EF 60 D4            [24] 2215 	jz	00113$
                           00059A  2216 	C$ublox_gnss.c$218$1$255 ==.
                                   2217 ;	..\COMMON\ublox_gnss.c:218: do{
      0056F1                       2218 00116$:
                           00059A  2219 	C$ublox_gnss.c$219$2$261 ==.
                                   2220 ;	..\COMMON\ublox_gnss.c:219: delay(5000);
      0056F1 90 13 88         [24] 2221 	mov	dptr,#0x1388
      0056F4 12 75 74         [24] 2222 	lcall	_delay
                           0005A0  2223 	C$ublox_gnss.c$220$1$255 ==.
                                   2224 ;	..\COMMON\ublox_gnss.c:220: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav2,ACK,NULL));
      0056F7 75 3B A2         [24] 2225 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255
      0056FA 75 3C 03         [24] 2226 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav2_1_255 >> 8)
      0056FD 75 3D 00         [24] 2227 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      005700 75 38 01         [24] 2228 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      005703 75 39 08         [24] 2229 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x08
      005706 75 3A 00         [24] 2230 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      005709 75 3E 01         [24] 2231 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      00570C E4               [12] 2232 	clr	a
      00570D F5 3F            [12] 2233 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      00570F F5 40            [12] 2234 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2235 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      005711 F5 41            [12] 2236 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      005713 75 82 06         [24] 2237 	mov	dpl,#0x06
      005716 12 51 DC         [24] 2238 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      005719 E5 82            [12] 2239 	mov	a,dpl
      00571B 60 D4            [24] 2240 	jz	00116$
                           0005C6  2241 	C$ublox_gnss.c$222$1$255 ==.
                                   2242 ;	..\COMMON\ublox_gnss.c:222: do{
      00571D                       2243 00119$:
                           0005C6  2244 	C$ublox_gnss.c$223$2$262 ==.
                                   2245 ;	..\COMMON\ublox_gnss.c:223: delay(5000);
      00571D 90 13 88         [24] 2246 	mov	dptr,#0x1388
      005720 12 75 74         [24] 2247 	lcall	_delay
                           0005CC  2248 	C$ublox_gnss.c$224$1$255 ==.
                                   2249 ;	..\COMMON\ublox_gnss.c:224: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav3,ACK,NULL));
      005723 75 3B AA         [24] 2250 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255
      005726 75 3C 03         [24] 2251 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav3_1_255 >> 8)
      005729 75 3D 00         [24] 2252 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      00572C 75 38 01         [24] 2253 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x01
      00572F 75 39 08         [24] 2254 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x08
      005732 75 3A 00         [24] 2255 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      005735 75 3E 01         [24] 2256 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      005738 E4               [12] 2257 	clr	a
      005739 F5 3F            [12] 2258 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      00573B F5 40            [12] 2259 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2260 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      00573D F5 41            [12] 2261 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      00573F 75 82 06         [24] 2262 	mov	dpl,#0x06
      005742 12 51 DC         [24] 2263 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      005745 E5 82            [12] 2264 	mov	a,dpl
      005747 60 D4            [24] 2265 	jz	00119$
                           0005F2  2266 	C$ublox_gnss.c$226$1$255 ==.
                                   2267 ;	..\COMMON\ublox_gnss.c:226: do{
      005749                       2268 00122$:
                           0005F2  2269 	C$ublox_gnss.c$227$2$263 ==.
                                   2270 ;	..\COMMON\ublox_gnss.c:227: delay(5000);
      005749 90 13 88         [24] 2271 	mov	dptr,#0x1388
      00574C 12 75 74         [24] 2272 	lcall	_delay
                           0005F8  2273 	C$ublox_gnss.c$228$1$255 ==.
                                   2274 ;	..\COMMON\ublox_gnss.c:228: }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x24,sizeof(UBLOX_setNav5),UBLOX_setNav5,ACK,NULL));
      00574F 75 3B B2         [24] 2275 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255
      005752 75 3C 03         [24] 2276 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_UBLOX_GPS_PortInit_UBLOX_setNav5_1_255 >> 8)
      005755 75 3D 00         [24] 2277 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      005758 75 38 24         [24] 2278 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x24
      00575B 75 39 24         [24] 2279 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,#0x24
      00575E 75 3A 00         [24] 2280 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),#0x00
      005761 75 3E 01         [24] 2281 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x01
      005764 E4               [12] 2282 	clr	a
      005765 F5 3F            [12] 2283 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,a
      005767 F5 40            [12] 2284 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),a
                                   2285 ;	1-genFromRTrack replaced	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      005769 F5 41            [12] 2286 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),a
      00576B 75 82 06         [24] 2287 	mov	dpl,#0x06
      00576E 12 51 DC         [24] 2288 	lcall	_UBLOX_GPS_SendCommand_WaitACK
      005771 E5 82            [12] 2289 	mov	a,dpl
      005773 60 D4            [24] 2290 	jz	00122$
                           00061E  2291 	C$ublox_gnss.c$232$1$255 ==.
                           00061E  2292 	XG$UBLOX_GPS_PortInit$0$0 ==.
      005775 22               [24] 2293 	ret
                                   2294 ;------------------------------------------------------------
                                   2295 ;Allocation info for local variables in function 'UBLOX_payload_POSLLH'
                                   2296 ;------------------------------------------------------------
                                   2297 ;i                         Allocated to registers 
                                   2298 ;------------------------------------------------------------
                           00061F  2299 	G$UBLOX_payload_POSLLH$0$0 ==.
                           00061F  2300 	C$ublox_gnss.c$237$1$255 ==.
                                   2301 ;	..\COMMON\ublox_gnss.c:237: void UBLOX_payload_POSLLH(void)
                                   2302 ;	-----------------------------------------
                                   2303 ;	 function UBLOX_payload_POSLLH
                                   2304 ;	-----------------------------------------
      005776                       2305 _UBLOX_payload_POSLLH:
                           00061F  2306 	C$ublox_gnss.c$244$1$265 ==.
                                   2307 ;	..\COMMON\ublox_gnss.c:244: payload_len = 0;
      005776 90 03 F2         [24] 2308 	mov	dptr,#_payload_len
      005779 E4               [12] 2309 	clr	a
      00577A F0               [24] 2310 	movx	@dptr,a
      00577B A3               [24] 2311 	inc	dptr
      00577C F0               [24] 2312 	movx	@dptr,a
                           000626  2313 	C$ublox_gnss.c$245$2$266 ==.
                                   2314 ;	..\COMMON\ublox_gnss.c:245: do
      00577D                       2315 00101$:
                           000626  2316 	C$ublox_gnss.c$247$2$266 ==.
                                   2317 ;	..\COMMON\ublox_gnss.c:247: UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,payload,ANS,&payload_len);
      00577D 75 3B D6         [24] 2318 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_4,#_payload
      005780 75 3C 03         [24] 2319 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 1),#(_payload >> 8)
      005783 75 3D 00         [24] 2320 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_4 + 2),#0x00
      005786 75 3F F2         [24] 2321 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_6,#_payload_len
      005789 75 40 03         [24] 2322 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 1),#(_payload_len >> 8)
      00578C 75 41 00         [24] 2323 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_6 + 2),#0x00
      00578F 75 38 02         [24] 2324 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_2,#0x02
      005792 E4               [12] 2325 	clr	a
      005793 F5 39            [12] 2326 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_3,a
      005795 F5 3A            [12] 2327 	mov	(_UBLOX_GPS_SendCommand_WaitACK_PARM_3 + 1),a
                                   2328 ;	1-genFromRTrack replaced	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,#0x00
      005797 F5 3E            [12] 2329 	mov	_UBLOX_GPS_SendCommand_WaitACK_PARM_5,a
      005799 75 82 01         [24] 2330 	mov	dpl,#0x01
      00579C 12 51 DC         [24] 2331 	lcall	_UBLOX_GPS_SendCommand_WaitACK
                           000648  2332 	C$ublox_gnss.c$248$2$266 ==.
                                   2333 ;	..\COMMON\ublox_gnss.c:248: delay_ms(50);
      00579F 90 00 32         [24] 2334 	mov	dptr,#0x0032
      0057A2 12 44 39         [24] 2335 	lcall	_delay_ms
                           00064E  2336 	C$ublox_gnss.c$256$1$265 ==.
                                   2337 ;	..\COMMON\ublox_gnss.c:256: } while((!payload_len)/*|| (!payload[2])*/); // added payload[2] on 2021-08-20
      0057A5 90 03 F2         [24] 2338 	mov	dptr,#_payload_len
      0057A8 E0               [24] 2339 	movx	a,@dptr
      0057A9 F5 F0            [12] 2340 	mov	b,a
      0057AB A3               [24] 2341 	inc	dptr
      0057AC E0               [24] 2342 	movx	a,@dptr
      0057AD 45 F0            [12] 2343 	orl	a,b
      0057AF 60 CC            [24] 2344 	jz	00101$
                           00065A  2345 	C$ublox_gnss.c$270$1$265 ==.
                           00065A  2346 	XG$UBLOX_payload_POSLLH$0$0 ==.
      0057B1 22               [24] 2347 	ret
                                   2348 	.area CSEG    (CODE)
                                   2349 	.area CONST   (CODE)
                                   2350 	.area XINIT   (CODE)
                                   2351 	.area CABS    (ABS,CODE)
