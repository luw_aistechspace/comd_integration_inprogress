                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl __sdcc_external_startup
                                     13 	.globl _UBLOX_payload_POSLLH
                                     14 	.globl _transmit_prem_wtimer
                                     15 	.globl _packetPremFraming
                                     16 	.globl _transmit_copies_wtimer
                                     17 	.globl _packetStdFraming
                                     18 	.globl _UBLOX_GPS_PortInit
                                     19 	.globl _GOLDSEQUENCE_Init
                                     20 	.globl _dbglink_display_radio_error
                                     21 	.globl _com0_display_radio_error
                                     22 	.globl _dbglink_received_packet
                                     23 	.globl _com0_writestr
                                     24 	.globl _com0_setpos
                                     25 	.globl _com0_init
                                     26 	.globl _uart0_init
                                     27 	.globl _uart0_txidle
                                     28 	.globl _uart_timer1_baud
                                     29 	.globl _dbglink_writehex32
                                     30 	.globl _dbglink_writehex16
                                     31 	.globl _dbglink_writenum16
                                     32 	.globl _dbglink_writestr
                                     33 	.globl _dbglink_tx
                                     34 	.globl _dbglink_init
                                     35 	.globl _dbglink_txidle
                                     36 	.globl _wtimer0_curtime
                                     37 	.globl _wtimer_runcallbacks
                                     38 	.globl _wtimer_idle
                                     39 	.globl _wtimer_init
                                     40 	.globl _wtimer1_setconfig
                                     41 	.globl _wtimer0_setconfig
                                     42 	.globl _flash_apply_calibration
                                     43 	.globl _axradio_commsleepexit
                                     44 	.globl _axradio_setup_pincfg2
                                     45 	.globl _axradio_setup_pincfg1
                                     46 	.globl _axradio_conv_freq_tohz
                                     47 	.globl _axradio_get_freqoffset
                                     48 	.globl _axradio_set_freqoffset
                                     49 	.globl _axradio_set_default_remote_address
                                     50 	.globl _axradio_set_local_address
                                     51 	.globl _axradio_get_pllvcoi
                                     52 	.globl _axradio_get_pllrange
                                     53 	.globl _axradio_set_mode
                                     54 	.globl _axradio_cansleep
                                     55 	.globl _axradio_init
                                     56 	.globl _PORTC_7
                                     57 	.globl _PORTC_6
                                     58 	.globl _PORTC_5
                                     59 	.globl _PORTC_4
                                     60 	.globl _PORTC_3
                                     61 	.globl _PORTC_2
                                     62 	.globl _PORTC_1
                                     63 	.globl _PORTC_0
                                     64 	.globl _PORTB_7
                                     65 	.globl _PORTB_6
                                     66 	.globl _PORTB_5
                                     67 	.globl _PORTB_4
                                     68 	.globl _PORTB_3
                                     69 	.globl _PORTB_2
                                     70 	.globl _PORTB_1
                                     71 	.globl _PORTB_0
                                     72 	.globl _PORTA_7
                                     73 	.globl _PORTA_6
                                     74 	.globl _PORTA_5
                                     75 	.globl _PORTA_4
                                     76 	.globl _PORTA_3
                                     77 	.globl _PORTA_2
                                     78 	.globl _PORTA_1
                                     79 	.globl _PORTA_0
                                     80 	.globl _PINC_7
                                     81 	.globl _PINC_6
                                     82 	.globl _PINC_5
                                     83 	.globl _PINC_4
                                     84 	.globl _PINC_3
                                     85 	.globl _PINC_2
                                     86 	.globl _PINC_1
                                     87 	.globl _PINC_0
                                     88 	.globl _PINB_7
                                     89 	.globl _PINB_6
                                     90 	.globl _PINB_5
                                     91 	.globl _PINB_4
                                     92 	.globl _PINB_3
                                     93 	.globl _PINB_2
                                     94 	.globl _PINB_1
                                     95 	.globl _PINB_0
                                     96 	.globl _PINA_7
                                     97 	.globl _PINA_6
                                     98 	.globl _PINA_5
                                     99 	.globl _PINA_4
                                    100 	.globl _PINA_3
                                    101 	.globl _PINA_2
                                    102 	.globl _PINA_1
                                    103 	.globl _PINA_0
                                    104 	.globl _CY
                                    105 	.globl _AC
                                    106 	.globl _F0
                                    107 	.globl _RS1
                                    108 	.globl _RS0
                                    109 	.globl _OV
                                    110 	.globl _F1
                                    111 	.globl _P
                                    112 	.globl _IP_7
                                    113 	.globl _IP_6
                                    114 	.globl _IP_5
                                    115 	.globl _IP_4
                                    116 	.globl _IP_3
                                    117 	.globl _IP_2
                                    118 	.globl _IP_1
                                    119 	.globl _IP_0
                                    120 	.globl _EA
                                    121 	.globl _IE_7
                                    122 	.globl _IE_6
                                    123 	.globl _IE_5
                                    124 	.globl _IE_4
                                    125 	.globl _IE_3
                                    126 	.globl _IE_2
                                    127 	.globl _IE_1
                                    128 	.globl _IE_0
                                    129 	.globl _EIP_7
                                    130 	.globl _EIP_6
                                    131 	.globl _EIP_5
                                    132 	.globl _EIP_4
                                    133 	.globl _EIP_3
                                    134 	.globl _EIP_2
                                    135 	.globl _EIP_1
                                    136 	.globl _EIP_0
                                    137 	.globl _EIE_7
                                    138 	.globl _EIE_6
                                    139 	.globl _EIE_5
                                    140 	.globl _EIE_4
                                    141 	.globl _EIE_3
                                    142 	.globl _EIE_2
                                    143 	.globl _EIE_1
                                    144 	.globl _EIE_0
                                    145 	.globl _E2IP_7
                                    146 	.globl _E2IP_6
                                    147 	.globl _E2IP_5
                                    148 	.globl _E2IP_4
                                    149 	.globl _E2IP_3
                                    150 	.globl _E2IP_2
                                    151 	.globl _E2IP_1
                                    152 	.globl _E2IP_0
                                    153 	.globl _E2IE_7
                                    154 	.globl _E2IE_6
                                    155 	.globl _E2IE_5
                                    156 	.globl _E2IE_4
                                    157 	.globl _E2IE_3
                                    158 	.globl _E2IE_2
                                    159 	.globl _E2IE_1
                                    160 	.globl _E2IE_0
                                    161 	.globl _B_7
                                    162 	.globl _B_6
                                    163 	.globl _B_5
                                    164 	.globl _B_4
                                    165 	.globl _B_3
                                    166 	.globl _B_2
                                    167 	.globl _B_1
                                    168 	.globl _B_0
                                    169 	.globl _ACC_7
                                    170 	.globl _ACC_6
                                    171 	.globl _ACC_5
                                    172 	.globl _ACC_4
                                    173 	.globl _ACC_3
                                    174 	.globl _ACC_2
                                    175 	.globl _ACC_1
                                    176 	.globl _ACC_0
                                    177 	.globl _WTSTAT
                                    178 	.globl _WTIRQEN
                                    179 	.globl _WTEVTD
                                    180 	.globl _WTEVTD1
                                    181 	.globl _WTEVTD0
                                    182 	.globl _WTEVTC
                                    183 	.globl _WTEVTC1
                                    184 	.globl _WTEVTC0
                                    185 	.globl _WTEVTB
                                    186 	.globl _WTEVTB1
                                    187 	.globl _WTEVTB0
                                    188 	.globl _WTEVTA
                                    189 	.globl _WTEVTA1
                                    190 	.globl _WTEVTA0
                                    191 	.globl _WTCNTR1
                                    192 	.globl _WTCNTB
                                    193 	.globl _WTCNTB1
                                    194 	.globl _WTCNTB0
                                    195 	.globl _WTCNTA
                                    196 	.globl _WTCNTA1
                                    197 	.globl _WTCNTA0
                                    198 	.globl _WTCFGB
                                    199 	.globl _WTCFGA
                                    200 	.globl _WDTRESET
                                    201 	.globl _WDTCFG
                                    202 	.globl _U1STATUS
                                    203 	.globl _U1SHREG
                                    204 	.globl _U1MODE
                                    205 	.globl _U1CTRL
                                    206 	.globl _U0STATUS
                                    207 	.globl _U0SHREG
                                    208 	.globl _U0MODE
                                    209 	.globl _U0CTRL
                                    210 	.globl _T2STATUS
                                    211 	.globl _T2PERIOD
                                    212 	.globl _T2PERIOD1
                                    213 	.globl _T2PERIOD0
                                    214 	.globl _T2MODE
                                    215 	.globl _T2CNT
                                    216 	.globl _T2CNT1
                                    217 	.globl _T2CNT0
                                    218 	.globl _T2CLKSRC
                                    219 	.globl _T1STATUS
                                    220 	.globl _T1PERIOD
                                    221 	.globl _T1PERIOD1
                                    222 	.globl _T1PERIOD0
                                    223 	.globl _T1MODE
                                    224 	.globl _T1CNT
                                    225 	.globl _T1CNT1
                                    226 	.globl _T1CNT0
                                    227 	.globl _T1CLKSRC
                                    228 	.globl _T0STATUS
                                    229 	.globl _T0PERIOD
                                    230 	.globl _T0PERIOD1
                                    231 	.globl _T0PERIOD0
                                    232 	.globl _T0MODE
                                    233 	.globl _T0CNT
                                    234 	.globl _T0CNT1
                                    235 	.globl _T0CNT0
                                    236 	.globl _T0CLKSRC
                                    237 	.globl _SPSTATUS
                                    238 	.globl _SPSHREG
                                    239 	.globl _SPMODE
                                    240 	.globl _SPCLKSRC
                                    241 	.globl _RADIOSTAT
                                    242 	.globl _RADIOSTAT1
                                    243 	.globl _RADIOSTAT0
                                    244 	.globl _RADIODATA
                                    245 	.globl _RADIODATA3
                                    246 	.globl _RADIODATA2
                                    247 	.globl _RADIODATA1
                                    248 	.globl _RADIODATA0
                                    249 	.globl _RADIOADDR
                                    250 	.globl _RADIOADDR1
                                    251 	.globl _RADIOADDR0
                                    252 	.globl _RADIOACC
                                    253 	.globl _OC1STATUS
                                    254 	.globl _OC1PIN
                                    255 	.globl _OC1MODE
                                    256 	.globl _OC1COMP
                                    257 	.globl _OC1COMP1
                                    258 	.globl _OC1COMP0
                                    259 	.globl _OC0STATUS
                                    260 	.globl _OC0PIN
                                    261 	.globl _OC0MODE
                                    262 	.globl _OC0COMP
                                    263 	.globl _OC0COMP1
                                    264 	.globl _OC0COMP0
                                    265 	.globl _NVSTATUS
                                    266 	.globl _NVKEY
                                    267 	.globl _NVDATA
                                    268 	.globl _NVDATA1
                                    269 	.globl _NVDATA0
                                    270 	.globl _NVADDR
                                    271 	.globl _NVADDR1
                                    272 	.globl _NVADDR0
                                    273 	.globl _IC1STATUS
                                    274 	.globl _IC1MODE
                                    275 	.globl _IC1CAPT
                                    276 	.globl _IC1CAPT1
                                    277 	.globl _IC1CAPT0
                                    278 	.globl _IC0STATUS
                                    279 	.globl _IC0MODE
                                    280 	.globl _IC0CAPT
                                    281 	.globl _IC0CAPT1
                                    282 	.globl _IC0CAPT0
                                    283 	.globl _PORTR
                                    284 	.globl _PORTC
                                    285 	.globl _PORTB
                                    286 	.globl _PORTA
                                    287 	.globl _PINR
                                    288 	.globl _PINC
                                    289 	.globl _PINB
                                    290 	.globl _PINA
                                    291 	.globl _DIRR
                                    292 	.globl _DIRC
                                    293 	.globl _DIRB
                                    294 	.globl _DIRA
                                    295 	.globl _DBGLNKSTAT
                                    296 	.globl _DBGLNKBUF
                                    297 	.globl _CODECONFIG
                                    298 	.globl _CLKSTAT
                                    299 	.globl _CLKCON
                                    300 	.globl _ANALOGCOMP
                                    301 	.globl _ADCCONV
                                    302 	.globl _ADCCLKSRC
                                    303 	.globl _ADCCH3CONFIG
                                    304 	.globl _ADCCH2CONFIG
                                    305 	.globl _ADCCH1CONFIG
                                    306 	.globl _ADCCH0CONFIG
                                    307 	.globl __XPAGE
                                    308 	.globl _XPAGE
                                    309 	.globl _SP
                                    310 	.globl _PSW
                                    311 	.globl _PCON
                                    312 	.globl _IP
                                    313 	.globl _IE
                                    314 	.globl _EIP
                                    315 	.globl _EIE
                                    316 	.globl _E2IP
                                    317 	.globl _E2IE
                                    318 	.globl _DPS
                                    319 	.globl _DPTR1
                                    320 	.globl _DPTR0
                                    321 	.globl _DPL1
                                    322 	.globl _DPL
                                    323 	.globl _DPH1
                                    324 	.globl _DPH
                                    325 	.globl _B
                                    326 	.globl _ACC
                                    327 	.globl _wt0_tx3
                                    328 	.globl _wt0_beacon
                                    329 	.globl _premSlot_counter
                                    330 	.globl _txdone
                                    331 	.globl _ack_flg
                                    332 	.globl _pkt_counter
                                    333 	.globl _pkts_missing
                                    334 	.globl _pkts_received
                                    335 	.globl _StdPrem
                                    336 	.globl _trmID
                                    337 	.globl _payload_len
                                    338 	.globl _payload
                                    339 	.globl _AX5043_TIMEGAIN3NB
                                    340 	.globl _AX5043_TIMEGAIN2NB
                                    341 	.globl _AX5043_TIMEGAIN1NB
                                    342 	.globl _AX5043_TIMEGAIN0NB
                                    343 	.globl _AX5043_RXPARAMSETSNB
                                    344 	.globl _AX5043_RXPARAMCURSETNB
                                    345 	.globl _AX5043_PKTMAXLENNB
                                    346 	.globl _AX5043_PKTLENOFFSETNB
                                    347 	.globl _AX5043_PKTLENCFGNB
                                    348 	.globl _AX5043_PKTADDRMASK3NB
                                    349 	.globl _AX5043_PKTADDRMASK2NB
                                    350 	.globl _AX5043_PKTADDRMASK1NB
                                    351 	.globl _AX5043_PKTADDRMASK0NB
                                    352 	.globl _AX5043_PKTADDRCFGNB
                                    353 	.globl _AX5043_PKTADDR3NB
                                    354 	.globl _AX5043_PKTADDR2NB
                                    355 	.globl _AX5043_PKTADDR1NB
                                    356 	.globl _AX5043_PKTADDR0NB
                                    357 	.globl _AX5043_PHASEGAIN3NB
                                    358 	.globl _AX5043_PHASEGAIN2NB
                                    359 	.globl _AX5043_PHASEGAIN1NB
                                    360 	.globl _AX5043_PHASEGAIN0NB
                                    361 	.globl _AX5043_FREQUENCYLEAKNB
                                    362 	.globl _AX5043_FREQUENCYGAIND3NB
                                    363 	.globl _AX5043_FREQUENCYGAIND2NB
                                    364 	.globl _AX5043_FREQUENCYGAIND1NB
                                    365 	.globl _AX5043_FREQUENCYGAIND0NB
                                    366 	.globl _AX5043_FREQUENCYGAINC3NB
                                    367 	.globl _AX5043_FREQUENCYGAINC2NB
                                    368 	.globl _AX5043_FREQUENCYGAINC1NB
                                    369 	.globl _AX5043_FREQUENCYGAINC0NB
                                    370 	.globl _AX5043_FREQUENCYGAINB3NB
                                    371 	.globl _AX5043_FREQUENCYGAINB2NB
                                    372 	.globl _AX5043_FREQUENCYGAINB1NB
                                    373 	.globl _AX5043_FREQUENCYGAINB0NB
                                    374 	.globl _AX5043_FREQUENCYGAINA3NB
                                    375 	.globl _AX5043_FREQUENCYGAINA2NB
                                    376 	.globl _AX5043_FREQUENCYGAINA1NB
                                    377 	.globl _AX5043_FREQUENCYGAINA0NB
                                    378 	.globl _AX5043_FREQDEV13NB
                                    379 	.globl _AX5043_FREQDEV12NB
                                    380 	.globl _AX5043_FREQDEV11NB
                                    381 	.globl _AX5043_FREQDEV10NB
                                    382 	.globl _AX5043_FREQDEV03NB
                                    383 	.globl _AX5043_FREQDEV02NB
                                    384 	.globl _AX5043_FREQDEV01NB
                                    385 	.globl _AX5043_FREQDEV00NB
                                    386 	.globl _AX5043_FOURFSK3NB
                                    387 	.globl _AX5043_FOURFSK2NB
                                    388 	.globl _AX5043_FOURFSK1NB
                                    389 	.globl _AX5043_FOURFSK0NB
                                    390 	.globl _AX5043_DRGAIN3NB
                                    391 	.globl _AX5043_DRGAIN2NB
                                    392 	.globl _AX5043_DRGAIN1NB
                                    393 	.globl _AX5043_DRGAIN0NB
                                    394 	.globl _AX5043_BBOFFSRES3NB
                                    395 	.globl _AX5043_BBOFFSRES2NB
                                    396 	.globl _AX5043_BBOFFSRES1NB
                                    397 	.globl _AX5043_BBOFFSRES0NB
                                    398 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    399 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    400 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    401 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    402 	.globl _AX5043_AGCTARGET3NB
                                    403 	.globl _AX5043_AGCTARGET2NB
                                    404 	.globl _AX5043_AGCTARGET1NB
                                    405 	.globl _AX5043_AGCTARGET0NB
                                    406 	.globl _AX5043_AGCMINMAX3NB
                                    407 	.globl _AX5043_AGCMINMAX2NB
                                    408 	.globl _AX5043_AGCMINMAX1NB
                                    409 	.globl _AX5043_AGCMINMAX0NB
                                    410 	.globl _AX5043_AGCGAIN3NB
                                    411 	.globl _AX5043_AGCGAIN2NB
                                    412 	.globl _AX5043_AGCGAIN1NB
                                    413 	.globl _AX5043_AGCGAIN0NB
                                    414 	.globl _AX5043_AGCAHYST3NB
                                    415 	.globl _AX5043_AGCAHYST2NB
                                    416 	.globl _AX5043_AGCAHYST1NB
                                    417 	.globl _AX5043_AGCAHYST0NB
                                    418 	.globl _AX5043_0xF44NB
                                    419 	.globl _AX5043_0xF35NB
                                    420 	.globl _AX5043_0xF34NB
                                    421 	.globl _AX5043_0xF33NB
                                    422 	.globl _AX5043_0xF32NB
                                    423 	.globl _AX5043_0xF31NB
                                    424 	.globl _AX5043_0xF30NB
                                    425 	.globl _AX5043_0xF26NB
                                    426 	.globl _AX5043_0xF23NB
                                    427 	.globl _AX5043_0xF22NB
                                    428 	.globl _AX5043_0xF21NB
                                    429 	.globl _AX5043_0xF1CNB
                                    430 	.globl _AX5043_0xF18NB
                                    431 	.globl _AX5043_0xF0CNB
                                    432 	.globl _AX5043_0xF00NB
                                    433 	.globl _AX5043_XTALSTATUSNB
                                    434 	.globl _AX5043_XTALOSCNB
                                    435 	.globl _AX5043_XTALCAPNB
                                    436 	.globl _AX5043_XTALAMPLNB
                                    437 	.globl _AX5043_WAKEUPXOEARLYNB
                                    438 	.globl _AX5043_WAKEUPTIMER1NB
                                    439 	.globl _AX5043_WAKEUPTIMER0NB
                                    440 	.globl _AX5043_WAKEUPFREQ1NB
                                    441 	.globl _AX5043_WAKEUPFREQ0NB
                                    442 	.globl _AX5043_WAKEUP1NB
                                    443 	.globl _AX5043_WAKEUP0NB
                                    444 	.globl _AX5043_TXRATE2NB
                                    445 	.globl _AX5043_TXRATE1NB
                                    446 	.globl _AX5043_TXRATE0NB
                                    447 	.globl _AX5043_TXPWRCOEFFE1NB
                                    448 	.globl _AX5043_TXPWRCOEFFE0NB
                                    449 	.globl _AX5043_TXPWRCOEFFD1NB
                                    450 	.globl _AX5043_TXPWRCOEFFD0NB
                                    451 	.globl _AX5043_TXPWRCOEFFC1NB
                                    452 	.globl _AX5043_TXPWRCOEFFC0NB
                                    453 	.globl _AX5043_TXPWRCOEFFB1NB
                                    454 	.globl _AX5043_TXPWRCOEFFB0NB
                                    455 	.globl _AX5043_TXPWRCOEFFA1NB
                                    456 	.globl _AX5043_TXPWRCOEFFA0NB
                                    457 	.globl _AX5043_TRKRFFREQ2NB
                                    458 	.globl _AX5043_TRKRFFREQ1NB
                                    459 	.globl _AX5043_TRKRFFREQ0NB
                                    460 	.globl _AX5043_TRKPHASE1NB
                                    461 	.globl _AX5043_TRKPHASE0NB
                                    462 	.globl _AX5043_TRKFSKDEMOD1NB
                                    463 	.globl _AX5043_TRKFSKDEMOD0NB
                                    464 	.globl _AX5043_TRKFREQ1NB
                                    465 	.globl _AX5043_TRKFREQ0NB
                                    466 	.globl _AX5043_TRKDATARATE2NB
                                    467 	.globl _AX5043_TRKDATARATE1NB
                                    468 	.globl _AX5043_TRKDATARATE0NB
                                    469 	.globl _AX5043_TRKAMPLITUDE1NB
                                    470 	.globl _AX5043_TRKAMPLITUDE0NB
                                    471 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    472 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    473 	.globl _AX5043_TMGTXSETTLENB
                                    474 	.globl _AX5043_TMGTXBOOSTNB
                                    475 	.globl _AX5043_TMGRXSETTLENB
                                    476 	.globl _AX5043_TMGRXRSSINB
                                    477 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    478 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    479 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    480 	.globl _AX5043_TMGRXOFFSACQNB
                                    481 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    482 	.globl _AX5043_TMGRXBOOSTNB
                                    483 	.globl _AX5043_TMGRXAGCNB
                                    484 	.globl _AX5043_TIMER2NB
                                    485 	.globl _AX5043_TIMER1NB
                                    486 	.globl _AX5043_TIMER0NB
                                    487 	.globl _AX5043_SILICONREVISIONNB
                                    488 	.globl _AX5043_SCRATCHNB
                                    489 	.globl _AX5043_RXDATARATE2NB
                                    490 	.globl _AX5043_RXDATARATE1NB
                                    491 	.globl _AX5043_RXDATARATE0NB
                                    492 	.globl _AX5043_RSSIREFERENCENB
                                    493 	.globl _AX5043_RSSIABSTHRNB
                                    494 	.globl _AX5043_RSSINB
                                    495 	.globl _AX5043_REFNB
                                    496 	.globl _AX5043_RADIOSTATENB
                                    497 	.globl _AX5043_RADIOEVENTREQ1NB
                                    498 	.globl _AX5043_RADIOEVENTREQ0NB
                                    499 	.globl _AX5043_RADIOEVENTMASK1NB
                                    500 	.globl _AX5043_RADIOEVENTMASK0NB
                                    501 	.globl _AX5043_PWRMODENB
                                    502 	.globl _AX5043_PWRAMPNB
                                    503 	.globl _AX5043_POWSTICKYSTATNB
                                    504 	.globl _AX5043_POWSTATNB
                                    505 	.globl _AX5043_POWIRQMASKNB
                                    506 	.globl _AX5043_POWCTRL1NB
                                    507 	.globl _AX5043_PLLVCOIRNB
                                    508 	.globl _AX5043_PLLVCOINB
                                    509 	.globl _AX5043_PLLVCODIVNB
                                    510 	.globl _AX5043_PLLRNGCLKNB
                                    511 	.globl _AX5043_PLLRANGINGBNB
                                    512 	.globl _AX5043_PLLRANGINGANB
                                    513 	.globl _AX5043_PLLLOOPBOOSTNB
                                    514 	.globl _AX5043_PLLLOOPNB
                                    515 	.globl _AX5043_PLLLOCKDETNB
                                    516 	.globl _AX5043_PLLCPIBOOSTNB
                                    517 	.globl _AX5043_PLLCPINB
                                    518 	.globl _AX5043_PKTSTOREFLAGSNB
                                    519 	.globl _AX5043_PKTMISCFLAGSNB
                                    520 	.globl _AX5043_PKTCHUNKSIZENB
                                    521 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    522 	.globl _AX5043_PINSTATENB
                                    523 	.globl _AX5043_PINFUNCSYSCLKNB
                                    524 	.globl _AX5043_PINFUNCPWRAMPNB
                                    525 	.globl _AX5043_PINFUNCIRQNB
                                    526 	.globl _AX5043_PINFUNCDCLKNB
                                    527 	.globl _AX5043_PINFUNCDATANB
                                    528 	.globl _AX5043_PINFUNCANTSELNB
                                    529 	.globl _AX5043_MODULATIONNB
                                    530 	.globl _AX5043_MODCFGPNB
                                    531 	.globl _AX5043_MODCFGFNB
                                    532 	.globl _AX5043_MODCFGANB
                                    533 	.globl _AX5043_MAXRFOFFSET2NB
                                    534 	.globl _AX5043_MAXRFOFFSET1NB
                                    535 	.globl _AX5043_MAXRFOFFSET0NB
                                    536 	.globl _AX5043_MAXDROFFSET2NB
                                    537 	.globl _AX5043_MAXDROFFSET1NB
                                    538 	.globl _AX5043_MAXDROFFSET0NB
                                    539 	.globl _AX5043_MATCH1PAT1NB
                                    540 	.globl _AX5043_MATCH1PAT0NB
                                    541 	.globl _AX5043_MATCH1MINNB
                                    542 	.globl _AX5043_MATCH1MAXNB
                                    543 	.globl _AX5043_MATCH1LENNB
                                    544 	.globl _AX5043_MATCH0PAT3NB
                                    545 	.globl _AX5043_MATCH0PAT2NB
                                    546 	.globl _AX5043_MATCH0PAT1NB
                                    547 	.globl _AX5043_MATCH0PAT0NB
                                    548 	.globl _AX5043_MATCH0MINNB
                                    549 	.globl _AX5043_MATCH0MAXNB
                                    550 	.globl _AX5043_MATCH0LENNB
                                    551 	.globl _AX5043_LPOSCSTATUSNB
                                    552 	.globl _AX5043_LPOSCREF1NB
                                    553 	.globl _AX5043_LPOSCREF0NB
                                    554 	.globl _AX5043_LPOSCPER1NB
                                    555 	.globl _AX5043_LPOSCPER0NB
                                    556 	.globl _AX5043_LPOSCKFILT1NB
                                    557 	.globl _AX5043_LPOSCKFILT0NB
                                    558 	.globl _AX5043_LPOSCFREQ1NB
                                    559 	.globl _AX5043_LPOSCFREQ0NB
                                    560 	.globl _AX5043_LPOSCCONFIGNB
                                    561 	.globl _AX5043_IRQREQUEST1NB
                                    562 	.globl _AX5043_IRQREQUEST0NB
                                    563 	.globl _AX5043_IRQMASK1NB
                                    564 	.globl _AX5043_IRQMASK0NB
                                    565 	.globl _AX5043_IRQINVERSION1NB
                                    566 	.globl _AX5043_IRQINVERSION0NB
                                    567 	.globl _AX5043_IFFREQ1NB
                                    568 	.globl _AX5043_IFFREQ0NB
                                    569 	.globl _AX5043_GPADCPERIODNB
                                    570 	.globl _AX5043_GPADCCTRLNB
                                    571 	.globl _AX5043_GPADC13VALUE1NB
                                    572 	.globl _AX5043_GPADC13VALUE0NB
                                    573 	.globl _AX5043_FSKDMIN1NB
                                    574 	.globl _AX5043_FSKDMIN0NB
                                    575 	.globl _AX5043_FSKDMAX1NB
                                    576 	.globl _AX5043_FSKDMAX0NB
                                    577 	.globl _AX5043_FSKDEV2NB
                                    578 	.globl _AX5043_FSKDEV1NB
                                    579 	.globl _AX5043_FSKDEV0NB
                                    580 	.globl _AX5043_FREQB3NB
                                    581 	.globl _AX5043_FREQB2NB
                                    582 	.globl _AX5043_FREQB1NB
                                    583 	.globl _AX5043_FREQB0NB
                                    584 	.globl _AX5043_FREQA3NB
                                    585 	.globl _AX5043_FREQA2NB
                                    586 	.globl _AX5043_FREQA1NB
                                    587 	.globl _AX5043_FREQA0NB
                                    588 	.globl _AX5043_FRAMINGNB
                                    589 	.globl _AX5043_FIFOTHRESH1NB
                                    590 	.globl _AX5043_FIFOTHRESH0NB
                                    591 	.globl _AX5043_FIFOSTATNB
                                    592 	.globl _AX5043_FIFOFREE1NB
                                    593 	.globl _AX5043_FIFOFREE0NB
                                    594 	.globl _AX5043_FIFODATANB
                                    595 	.globl _AX5043_FIFOCOUNT1NB
                                    596 	.globl _AX5043_FIFOCOUNT0NB
                                    597 	.globl _AX5043_FECSYNCNB
                                    598 	.globl _AX5043_FECSTATUSNB
                                    599 	.globl _AX5043_FECNB
                                    600 	.globl _AX5043_ENCODINGNB
                                    601 	.globl _AX5043_DIVERSITYNB
                                    602 	.globl _AX5043_DECIMATIONNB
                                    603 	.globl _AX5043_DACVALUE1NB
                                    604 	.globl _AX5043_DACVALUE0NB
                                    605 	.globl _AX5043_DACCONFIGNB
                                    606 	.globl _AX5043_CRCINIT3NB
                                    607 	.globl _AX5043_CRCINIT2NB
                                    608 	.globl _AX5043_CRCINIT1NB
                                    609 	.globl _AX5043_CRCINIT0NB
                                    610 	.globl _AX5043_BGNDRSSITHRNB
                                    611 	.globl _AX5043_BGNDRSSIGAINNB
                                    612 	.globl _AX5043_BGNDRSSINB
                                    613 	.globl _AX5043_BBTUNENB
                                    614 	.globl _AX5043_BBOFFSCAPNB
                                    615 	.globl _AX5043_AMPLFILTERNB
                                    616 	.globl _AX5043_AGCCOUNTERNB
                                    617 	.globl _AX5043_AFSKSPACE1NB
                                    618 	.globl _AX5043_AFSKSPACE0NB
                                    619 	.globl _AX5043_AFSKMARK1NB
                                    620 	.globl _AX5043_AFSKMARK0NB
                                    621 	.globl _AX5043_AFSKCTRLNB
                                    622 	.globl _AX5043_TIMEGAIN3
                                    623 	.globl _AX5043_TIMEGAIN2
                                    624 	.globl _AX5043_TIMEGAIN1
                                    625 	.globl _AX5043_TIMEGAIN0
                                    626 	.globl _AX5043_RXPARAMSETS
                                    627 	.globl _AX5043_RXPARAMCURSET
                                    628 	.globl _AX5043_PKTMAXLEN
                                    629 	.globl _AX5043_PKTLENOFFSET
                                    630 	.globl _AX5043_PKTLENCFG
                                    631 	.globl _AX5043_PKTADDRMASK3
                                    632 	.globl _AX5043_PKTADDRMASK2
                                    633 	.globl _AX5043_PKTADDRMASK1
                                    634 	.globl _AX5043_PKTADDRMASK0
                                    635 	.globl _AX5043_PKTADDRCFG
                                    636 	.globl _AX5043_PKTADDR3
                                    637 	.globl _AX5043_PKTADDR2
                                    638 	.globl _AX5043_PKTADDR1
                                    639 	.globl _AX5043_PKTADDR0
                                    640 	.globl _AX5043_PHASEGAIN3
                                    641 	.globl _AX5043_PHASEGAIN2
                                    642 	.globl _AX5043_PHASEGAIN1
                                    643 	.globl _AX5043_PHASEGAIN0
                                    644 	.globl _AX5043_FREQUENCYLEAK
                                    645 	.globl _AX5043_FREQUENCYGAIND3
                                    646 	.globl _AX5043_FREQUENCYGAIND2
                                    647 	.globl _AX5043_FREQUENCYGAIND1
                                    648 	.globl _AX5043_FREQUENCYGAIND0
                                    649 	.globl _AX5043_FREQUENCYGAINC3
                                    650 	.globl _AX5043_FREQUENCYGAINC2
                                    651 	.globl _AX5043_FREQUENCYGAINC1
                                    652 	.globl _AX5043_FREQUENCYGAINC0
                                    653 	.globl _AX5043_FREQUENCYGAINB3
                                    654 	.globl _AX5043_FREQUENCYGAINB2
                                    655 	.globl _AX5043_FREQUENCYGAINB1
                                    656 	.globl _AX5043_FREQUENCYGAINB0
                                    657 	.globl _AX5043_FREQUENCYGAINA3
                                    658 	.globl _AX5043_FREQUENCYGAINA2
                                    659 	.globl _AX5043_FREQUENCYGAINA1
                                    660 	.globl _AX5043_FREQUENCYGAINA0
                                    661 	.globl _AX5043_FREQDEV13
                                    662 	.globl _AX5043_FREQDEV12
                                    663 	.globl _AX5043_FREQDEV11
                                    664 	.globl _AX5043_FREQDEV10
                                    665 	.globl _AX5043_FREQDEV03
                                    666 	.globl _AX5043_FREQDEV02
                                    667 	.globl _AX5043_FREQDEV01
                                    668 	.globl _AX5043_FREQDEV00
                                    669 	.globl _AX5043_FOURFSK3
                                    670 	.globl _AX5043_FOURFSK2
                                    671 	.globl _AX5043_FOURFSK1
                                    672 	.globl _AX5043_FOURFSK0
                                    673 	.globl _AX5043_DRGAIN3
                                    674 	.globl _AX5043_DRGAIN2
                                    675 	.globl _AX5043_DRGAIN1
                                    676 	.globl _AX5043_DRGAIN0
                                    677 	.globl _AX5043_BBOFFSRES3
                                    678 	.globl _AX5043_BBOFFSRES2
                                    679 	.globl _AX5043_BBOFFSRES1
                                    680 	.globl _AX5043_BBOFFSRES0
                                    681 	.globl _AX5043_AMPLITUDEGAIN3
                                    682 	.globl _AX5043_AMPLITUDEGAIN2
                                    683 	.globl _AX5043_AMPLITUDEGAIN1
                                    684 	.globl _AX5043_AMPLITUDEGAIN0
                                    685 	.globl _AX5043_AGCTARGET3
                                    686 	.globl _AX5043_AGCTARGET2
                                    687 	.globl _AX5043_AGCTARGET1
                                    688 	.globl _AX5043_AGCTARGET0
                                    689 	.globl _AX5043_AGCMINMAX3
                                    690 	.globl _AX5043_AGCMINMAX2
                                    691 	.globl _AX5043_AGCMINMAX1
                                    692 	.globl _AX5043_AGCMINMAX0
                                    693 	.globl _AX5043_AGCGAIN3
                                    694 	.globl _AX5043_AGCGAIN2
                                    695 	.globl _AX5043_AGCGAIN1
                                    696 	.globl _AX5043_AGCGAIN0
                                    697 	.globl _AX5043_AGCAHYST3
                                    698 	.globl _AX5043_AGCAHYST2
                                    699 	.globl _AX5043_AGCAHYST1
                                    700 	.globl _AX5043_AGCAHYST0
                                    701 	.globl _AX5043_0xF44
                                    702 	.globl _AX5043_0xF35
                                    703 	.globl _AX5043_0xF34
                                    704 	.globl _AX5043_0xF33
                                    705 	.globl _AX5043_0xF32
                                    706 	.globl _AX5043_0xF31
                                    707 	.globl _AX5043_0xF30
                                    708 	.globl _AX5043_0xF26
                                    709 	.globl _AX5043_0xF23
                                    710 	.globl _AX5043_0xF22
                                    711 	.globl _AX5043_0xF21
                                    712 	.globl _AX5043_0xF1C
                                    713 	.globl _AX5043_0xF18
                                    714 	.globl _AX5043_0xF0C
                                    715 	.globl _AX5043_0xF00
                                    716 	.globl _AX5043_XTALSTATUS
                                    717 	.globl _AX5043_XTALOSC
                                    718 	.globl _AX5043_XTALCAP
                                    719 	.globl _AX5043_XTALAMPL
                                    720 	.globl _AX5043_WAKEUPXOEARLY
                                    721 	.globl _AX5043_WAKEUPTIMER1
                                    722 	.globl _AX5043_WAKEUPTIMER0
                                    723 	.globl _AX5043_WAKEUPFREQ1
                                    724 	.globl _AX5043_WAKEUPFREQ0
                                    725 	.globl _AX5043_WAKEUP1
                                    726 	.globl _AX5043_WAKEUP0
                                    727 	.globl _AX5043_TXRATE2
                                    728 	.globl _AX5043_TXRATE1
                                    729 	.globl _AX5043_TXRATE0
                                    730 	.globl _AX5043_TXPWRCOEFFE1
                                    731 	.globl _AX5043_TXPWRCOEFFE0
                                    732 	.globl _AX5043_TXPWRCOEFFD1
                                    733 	.globl _AX5043_TXPWRCOEFFD0
                                    734 	.globl _AX5043_TXPWRCOEFFC1
                                    735 	.globl _AX5043_TXPWRCOEFFC0
                                    736 	.globl _AX5043_TXPWRCOEFFB1
                                    737 	.globl _AX5043_TXPWRCOEFFB0
                                    738 	.globl _AX5043_TXPWRCOEFFA1
                                    739 	.globl _AX5043_TXPWRCOEFFA0
                                    740 	.globl _AX5043_TRKRFFREQ2
                                    741 	.globl _AX5043_TRKRFFREQ1
                                    742 	.globl _AX5043_TRKRFFREQ0
                                    743 	.globl _AX5043_TRKPHASE1
                                    744 	.globl _AX5043_TRKPHASE0
                                    745 	.globl _AX5043_TRKFSKDEMOD1
                                    746 	.globl _AX5043_TRKFSKDEMOD0
                                    747 	.globl _AX5043_TRKFREQ1
                                    748 	.globl _AX5043_TRKFREQ0
                                    749 	.globl _AX5043_TRKDATARATE2
                                    750 	.globl _AX5043_TRKDATARATE1
                                    751 	.globl _AX5043_TRKDATARATE0
                                    752 	.globl _AX5043_TRKAMPLITUDE1
                                    753 	.globl _AX5043_TRKAMPLITUDE0
                                    754 	.globl _AX5043_TRKAFSKDEMOD1
                                    755 	.globl _AX5043_TRKAFSKDEMOD0
                                    756 	.globl _AX5043_TMGTXSETTLE
                                    757 	.globl _AX5043_TMGTXBOOST
                                    758 	.globl _AX5043_TMGRXSETTLE
                                    759 	.globl _AX5043_TMGRXRSSI
                                    760 	.globl _AX5043_TMGRXPREAMBLE3
                                    761 	.globl _AX5043_TMGRXPREAMBLE2
                                    762 	.globl _AX5043_TMGRXPREAMBLE1
                                    763 	.globl _AX5043_TMGRXOFFSACQ
                                    764 	.globl _AX5043_TMGRXCOARSEAGC
                                    765 	.globl _AX5043_TMGRXBOOST
                                    766 	.globl _AX5043_TMGRXAGC
                                    767 	.globl _AX5043_TIMER2
                                    768 	.globl _AX5043_TIMER1
                                    769 	.globl _AX5043_TIMER0
                                    770 	.globl _AX5043_SILICONREVISION
                                    771 	.globl _AX5043_SCRATCH
                                    772 	.globl _AX5043_RXDATARATE2
                                    773 	.globl _AX5043_RXDATARATE1
                                    774 	.globl _AX5043_RXDATARATE0
                                    775 	.globl _AX5043_RSSIREFERENCE
                                    776 	.globl _AX5043_RSSIABSTHR
                                    777 	.globl _AX5043_RSSI
                                    778 	.globl _AX5043_REF
                                    779 	.globl _AX5043_RADIOSTATE
                                    780 	.globl _AX5043_RADIOEVENTREQ1
                                    781 	.globl _AX5043_RADIOEVENTREQ0
                                    782 	.globl _AX5043_RADIOEVENTMASK1
                                    783 	.globl _AX5043_RADIOEVENTMASK0
                                    784 	.globl _AX5043_PWRMODE
                                    785 	.globl _AX5043_PWRAMP
                                    786 	.globl _AX5043_POWSTICKYSTAT
                                    787 	.globl _AX5043_POWSTAT
                                    788 	.globl _AX5043_POWIRQMASK
                                    789 	.globl _AX5043_POWCTRL1
                                    790 	.globl _AX5043_PLLVCOIR
                                    791 	.globl _AX5043_PLLVCOI
                                    792 	.globl _AX5043_PLLVCODIV
                                    793 	.globl _AX5043_PLLRNGCLK
                                    794 	.globl _AX5043_PLLRANGINGB
                                    795 	.globl _AX5043_PLLRANGINGA
                                    796 	.globl _AX5043_PLLLOOPBOOST
                                    797 	.globl _AX5043_PLLLOOP
                                    798 	.globl _AX5043_PLLLOCKDET
                                    799 	.globl _AX5043_PLLCPIBOOST
                                    800 	.globl _AX5043_PLLCPI
                                    801 	.globl _AX5043_PKTSTOREFLAGS
                                    802 	.globl _AX5043_PKTMISCFLAGS
                                    803 	.globl _AX5043_PKTCHUNKSIZE
                                    804 	.globl _AX5043_PKTACCEPTFLAGS
                                    805 	.globl _AX5043_PINSTATE
                                    806 	.globl _AX5043_PINFUNCSYSCLK
                                    807 	.globl _AX5043_PINFUNCPWRAMP
                                    808 	.globl _AX5043_PINFUNCIRQ
                                    809 	.globl _AX5043_PINFUNCDCLK
                                    810 	.globl _AX5043_PINFUNCDATA
                                    811 	.globl _AX5043_PINFUNCANTSEL
                                    812 	.globl _AX5043_MODULATION
                                    813 	.globl _AX5043_MODCFGP
                                    814 	.globl _AX5043_MODCFGF
                                    815 	.globl _AX5043_MODCFGA
                                    816 	.globl _AX5043_MAXRFOFFSET2
                                    817 	.globl _AX5043_MAXRFOFFSET1
                                    818 	.globl _AX5043_MAXRFOFFSET0
                                    819 	.globl _AX5043_MAXDROFFSET2
                                    820 	.globl _AX5043_MAXDROFFSET1
                                    821 	.globl _AX5043_MAXDROFFSET0
                                    822 	.globl _AX5043_MATCH1PAT1
                                    823 	.globl _AX5043_MATCH1PAT0
                                    824 	.globl _AX5043_MATCH1MIN
                                    825 	.globl _AX5043_MATCH1MAX
                                    826 	.globl _AX5043_MATCH1LEN
                                    827 	.globl _AX5043_MATCH0PAT3
                                    828 	.globl _AX5043_MATCH0PAT2
                                    829 	.globl _AX5043_MATCH0PAT1
                                    830 	.globl _AX5043_MATCH0PAT0
                                    831 	.globl _AX5043_MATCH0MIN
                                    832 	.globl _AX5043_MATCH0MAX
                                    833 	.globl _AX5043_MATCH0LEN
                                    834 	.globl _AX5043_LPOSCSTATUS
                                    835 	.globl _AX5043_LPOSCREF1
                                    836 	.globl _AX5043_LPOSCREF0
                                    837 	.globl _AX5043_LPOSCPER1
                                    838 	.globl _AX5043_LPOSCPER0
                                    839 	.globl _AX5043_LPOSCKFILT1
                                    840 	.globl _AX5043_LPOSCKFILT0
                                    841 	.globl _AX5043_LPOSCFREQ1
                                    842 	.globl _AX5043_LPOSCFREQ0
                                    843 	.globl _AX5043_LPOSCCONFIG
                                    844 	.globl _AX5043_IRQREQUEST1
                                    845 	.globl _AX5043_IRQREQUEST0
                                    846 	.globl _AX5043_IRQMASK1
                                    847 	.globl _AX5043_IRQMASK0
                                    848 	.globl _AX5043_IRQINVERSION1
                                    849 	.globl _AX5043_IRQINVERSION0
                                    850 	.globl _AX5043_IFFREQ1
                                    851 	.globl _AX5043_IFFREQ0
                                    852 	.globl _AX5043_GPADCPERIOD
                                    853 	.globl _AX5043_GPADCCTRL
                                    854 	.globl _AX5043_GPADC13VALUE1
                                    855 	.globl _AX5043_GPADC13VALUE0
                                    856 	.globl _AX5043_FSKDMIN1
                                    857 	.globl _AX5043_FSKDMIN0
                                    858 	.globl _AX5043_FSKDMAX1
                                    859 	.globl _AX5043_FSKDMAX0
                                    860 	.globl _AX5043_FSKDEV2
                                    861 	.globl _AX5043_FSKDEV1
                                    862 	.globl _AX5043_FSKDEV0
                                    863 	.globl _AX5043_FREQB3
                                    864 	.globl _AX5043_FREQB2
                                    865 	.globl _AX5043_FREQB1
                                    866 	.globl _AX5043_FREQB0
                                    867 	.globl _AX5043_FREQA3
                                    868 	.globl _AX5043_FREQA2
                                    869 	.globl _AX5043_FREQA1
                                    870 	.globl _AX5043_FREQA0
                                    871 	.globl _AX5043_FRAMING
                                    872 	.globl _AX5043_FIFOTHRESH1
                                    873 	.globl _AX5043_FIFOTHRESH0
                                    874 	.globl _AX5043_FIFOSTAT
                                    875 	.globl _AX5043_FIFOFREE1
                                    876 	.globl _AX5043_FIFOFREE0
                                    877 	.globl _AX5043_FIFODATA
                                    878 	.globl _AX5043_FIFOCOUNT1
                                    879 	.globl _AX5043_FIFOCOUNT0
                                    880 	.globl _AX5043_FECSYNC
                                    881 	.globl _AX5043_FECSTATUS
                                    882 	.globl _AX5043_FEC
                                    883 	.globl _AX5043_ENCODING
                                    884 	.globl _AX5043_DIVERSITY
                                    885 	.globl _AX5043_DECIMATION
                                    886 	.globl _AX5043_DACVALUE1
                                    887 	.globl _AX5043_DACVALUE0
                                    888 	.globl _AX5043_DACCONFIG
                                    889 	.globl _AX5043_CRCINIT3
                                    890 	.globl _AX5043_CRCINIT2
                                    891 	.globl _AX5043_CRCINIT1
                                    892 	.globl _AX5043_CRCINIT0
                                    893 	.globl _AX5043_BGNDRSSITHR
                                    894 	.globl _AX5043_BGNDRSSIGAIN
                                    895 	.globl _AX5043_BGNDRSSI
                                    896 	.globl _AX5043_BBTUNE
                                    897 	.globl _AX5043_BBOFFSCAP
                                    898 	.globl _AX5043_AMPLFILTER
                                    899 	.globl _AX5043_AGCCOUNTER
                                    900 	.globl _AX5043_AFSKSPACE1
                                    901 	.globl _AX5043_AFSKSPACE0
                                    902 	.globl _AX5043_AFSKMARK1
                                    903 	.globl _AX5043_AFSKMARK0
                                    904 	.globl _AX5043_AFSKCTRL
                                    905 	.globl _RNGMODE
                                    906 	.globl _RNGCLKSRC1
                                    907 	.globl _RNGCLKSRC0
                                    908 	.globl _RNGBYTE
                                    909 	.globl _AESOUTADDR
                                    910 	.globl _AESOUTADDR1
                                    911 	.globl _AESOUTADDR0
                                    912 	.globl _AESMODE
                                    913 	.globl _AESKEYADDR
                                    914 	.globl _AESKEYADDR1
                                    915 	.globl _AESKEYADDR0
                                    916 	.globl _AESINADDR
                                    917 	.globl _AESINADDR1
                                    918 	.globl _AESINADDR0
                                    919 	.globl _AESCURBLOCK
                                    920 	.globl _AESCONFIG
                                    921 	.globl _XTALREADY
                                    922 	.globl _XTALOSC
                                    923 	.globl _XTALAMPL
                                    924 	.globl _SILICONREV
                                    925 	.globl _SCRATCH3
                                    926 	.globl _SCRATCH2
                                    927 	.globl _SCRATCH1
                                    928 	.globl _SCRATCH0
                                    929 	.globl _RADIOMUX
                                    930 	.globl _RADIOFSTATADDR
                                    931 	.globl _RADIOFSTATADDR1
                                    932 	.globl _RADIOFSTATADDR0
                                    933 	.globl _RADIOFDATAADDR
                                    934 	.globl _RADIOFDATAADDR1
                                    935 	.globl _RADIOFDATAADDR0
                                    936 	.globl _OSCRUN
                                    937 	.globl _OSCREADY
                                    938 	.globl _OSCFORCERUN
                                    939 	.globl _OSCCALIB
                                    940 	.globl _MISCCTRL
                                    941 	.globl _LPXOSCGM
                                    942 	.globl _LPOSCREF
                                    943 	.globl _LPOSCREF1
                                    944 	.globl _LPOSCREF0
                                    945 	.globl _LPOSCPER
                                    946 	.globl _LPOSCPER1
                                    947 	.globl _LPOSCPER0
                                    948 	.globl _LPOSCKFILT
                                    949 	.globl _LPOSCKFILT1
                                    950 	.globl _LPOSCKFILT0
                                    951 	.globl _LPOSCFREQ
                                    952 	.globl _LPOSCFREQ1
                                    953 	.globl _LPOSCFREQ0
                                    954 	.globl _LPOSCCONFIG
                                    955 	.globl _PINSEL
                                    956 	.globl _PINCHGC
                                    957 	.globl _PINCHGB
                                    958 	.globl _PINCHGA
                                    959 	.globl _PALTRADIO
                                    960 	.globl _PALTC
                                    961 	.globl _PALTB
                                    962 	.globl _PALTA
                                    963 	.globl _INTCHGC
                                    964 	.globl _INTCHGB
                                    965 	.globl _INTCHGA
                                    966 	.globl _EXTIRQ
                                    967 	.globl _GPIOENABLE
                                    968 	.globl _ANALOGA
                                    969 	.globl _FRCOSCREF
                                    970 	.globl _FRCOSCREF1
                                    971 	.globl _FRCOSCREF0
                                    972 	.globl _FRCOSCPER
                                    973 	.globl _FRCOSCPER1
                                    974 	.globl _FRCOSCPER0
                                    975 	.globl _FRCOSCKFILT
                                    976 	.globl _FRCOSCKFILT1
                                    977 	.globl _FRCOSCKFILT0
                                    978 	.globl _FRCOSCFREQ
                                    979 	.globl _FRCOSCFREQ1
                                    980 	.globl _FRCOSCFREQ0
                                    981 	.globl _FRCOSCCTRL
                                    982 	.globl _FRCOSCCONFIG
                                    983 	.globl _DMA1CONFIG
                                    984 	.globl _DMA1ADDR
                                    985 	.globl _DMA1ADDR1
                                    986 	.globl _DMA1ADDR0
                                    987 	.globl _DMA0CONFIG
                                    988 	.globl _DMA0ADDR
                                    989 	.globl _DMA0ADDR1
                                    990 	.globl _DMA0ADDR0
                                    991 	.globl _ADCTUNE2
                                    992 	.globl _ADCTUNE1
                                    993 	.globl _ADCTUNE0
                                    994 	.globl _ADCCH3VAL
                                    995 	.globl _ADCCH3VAL1
                                    996 	.globl _ADCCH3VAL0
                                    997 	.globl _ADCCH2VAL
                                    998 	.globl _ADCCH2VAL1
                                    999 	.globl _ADCCH2VAL0
                                   1000 	.globl _ADCCH1VAL
                                   1001 	.globl _ADCCH1VAL1
                                   1002 	.globl _ADCCH1VAL0
                                   1003 	.globl _ADCCH0VAL
                                   1004 	.globl _ADCCH0VAL1
                                   1005 	.globl _ADCCH0VAL0
                                   1006 	.globl _coldstart
                                   1007 	.globl _axradio_statuschange
                                   1008 	.globl _enable_radio_interrupt_in_mcu_pin
                                   1009 	.globl _disable_radio_interrupt_in_mcu_pin
                                   1010 ;--------------------------------------------------------
                                   1011 ; special function registers
                                   1012 ;--------------------------------------------------------
                                   1013 	.area RSEG    (ABS,DATA)
      000000                       1014 	.org 0x0000
                           0000E0  1015 G$ACC$0$0 == 0x00e0
                           0000E0  1016 _ACC	=	0x00e0
                           0000F0  1017 G$B$0$0 == 0x00f0
                           0000F0  1018 _B	=	0x00f0
                           000083  1019 G$DPH$0$0 == 0x0083
                           000083  1020 _DPH	=	0x0083
                           000085  1021 G$DPH1$0$0 == 0x0085
                           000085  1022 _DPH1	=	0x0085
                           000082  1023 G$DPL$0$0 == 0x0082
                           000082  1024 _DPL	=	0x0082
                           000084  1025 G$DPL1$0$0 == 0x0084
                           000084  1026 _DPL1	=	0x0084
                           008382  1027 G$DPTR0$0$0 == 0x8382
                           008382  1028 _DPTR0	=	0x8382
                           008584  1029 G$DPTR1$0$0 == 0x8584
                           008584  1030 _DPTR1	=	0x8584
                           000086  1031 G$DPS$0$0 == 0x0086
                           000086  1032 _DPS	=	0x0086
                           0000A0  1033 G$E2IE$0$0 == 0x00a0
                           0000A0  1034 _E2IE	=	0x00a0
                           0000C0  1035 G$E2IP$0$0 == 0x00c0
                           0000C0  1036 _E2IP	=	0x00c0
                           000098  1037 G$EIE$0$0 == 0x0098
                           000098  1038 _EIE	=	0x0098
                           0000B0  1039 G$EIP$0$0 == 0x00b0
                           0000B0  1040 _EIP	=	0x00b0
                           0000A8  1041 G$IE$0$0 == 0x00a8
                           0000A8  1042 _IE	=	0x00a8
                           0000B8  1043 G$IP$0$0 == 0x00b8
                           0000B8  1044 _IP	=	0x00b8
                           000087  1045 G$PCON$0$0 == 0x0087
                           000087  1046 _PCON	=	0x0087
                           0000D0  1047 G$PSW$0$0 == 0x00d0
                           0000D0  1048 _PSW	=	0x00d0
                           000081  1049 G$SP$0$0 == 0x0081
                           000081  1050 _SP	=	0x0081
                           0000D9  1051 G$XPAGE$0$0 == 0x00d9
                           0000D9  1052 _XPAGE	=	0x00d9
                           0000D9  1053 G$_XPAGE$0$0 == 0x00d9
                           0000D9  1054 __XPAGE	=	0x00d9
                           0000CA  1055 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA  1056 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1057 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB  1058 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1059 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2  1060 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1061 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3  1062 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1063 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1  1064 _ADCCLKSRC	=	0x00d1
                           0000C9  1065 G$ADCCONV$0$0 == 0x00c9
                           0000C9  1066 _ADCCONV	=	0x00c9
                           0000E1  1067 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1  1068 _ANALOGCOMP	=	0x00e1
                           0000C6  1069 G$CLKCON$0$0 == 0x00c6
                           0000C6  1070 _CLKCON	=	0x00c6
                           0000C7  1071 G$CLKSTAT$0$0 == 0x00c7
                           0000C7  1072 _CLKSTAT	=	0x00c7
                           000097  1073 G$CODECONFIG$0$0 == 0x0097
                           000097  1074 _CODECONFIG	=	0x0097
                           0000E3  1075 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3  1076 _DBGLNKBUF	=	0x00e3
                           0000E2  1077 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2  1078 _DBGLNKSTAT	=	0x00e2
                           000089  1079 G$DIRA$0$0 == 0x0089
                           000089  1080 _DIRA	=	0x0089
                           00008A  1081 G$DIRB$0$0 == 0x008a
                           00008A  1082 _DIRB	=	0x008a
                           00008B  1083 G$DIRC$0$0 == 0x008b
                           00008B  1084 _DIRC	=	0x008b
                           00008E  1085 G$DIRR$0$0 == 0x008e
                           00008E  1086 _DIRR	=	0x008e
                           0000C8  1087 G$PINA$0$0 == 0x00c8
                           0000C8  1088 _PINA	=	0x00c8
                           0000E8  1089 G$PINB$0$0 == 0x00e8
                           0000E8  1090 _PINB	=	0x00e8
                           0000F8  1091 G$PINC$0$0 == 0x00f8
                           0000F8  1092 _PINC	=	0x00f8
                           00008D  1093 G$PINR$0$0 == 0x008d
                           00008D  1094 _PINR	=	0x008d
                           000080  1095 G$PORTA$0$0 == 0x0080
                           000080  1096 _PORTA	=	0x0080
                           000088  1097 G$PORTB$0$0 == 0x0088
                           000088  1098 _PORTB	=	0x0088
                           000090  1099 G$PORTC$0$0 == 0x0090
                           000090  1100 _PORTC	=	0x0090
                           00008C  1101 G$PORTR$0$0 == 0x008c
                           00008C  1102 _PORTR	=	0x008c
                           0000CE  1103 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE  1104 _IC0CAPT0	=	0x00ce
                           0000CF  1105 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF  1106 _IC0CAPT1	=	0x00cf
                           00CFCE  1107 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE  1108 _IC0CAPT	=	0xcfce
                           0000CC  1109 G$IC0MODE$0$0 == 0x00cc
                           0000CC  1110 _IC0MODE	=	0x00cc
                           0000CD  1111 G$IC0STATUS$0$0 == 0x00cd
                           0000CD  1112 _IC0STATUS	=	0x00cd
                           0000D6  1113 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6  1114 _IC1CAPT0	=	0x00d6
                           0000D7  1115 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7  1116 _IC1CAPT1	=	0x00d7
                           00D7D6  1117 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6  1118 _IC1CAPT	=	0xd7d6
                           0000D4  1119 G$IC1MODE$0$0 == 0x00d4
                           0000D4  1120 _IC1MODE	=	0x00d4
                           0000D5  1121 G$IC1STATUS$0$0 == 0x00d5
                           0000D5  1122 _IC1STATUS	=	0x00d5
                           000092  1123 G$NVADDR0$0$0 == 0x0092
                           000092  1124 _NVADDR0	=	0x0092
                           000093  1125 G$NVADDR1$0$0 == 0x0093
                           000093  1126 _NVADDR1	=	0x0093
                           009392  1127 G$NVADDR$0$0 == 0x9392
                           009392  1128 _NVADDR	=	0x9392
                           000094  1129 G$NVDATA0$0$0 == 0x0094
                           000094  1130 _NVDATA0	=	0x0094
                           000095  1131 G$NVDATA1$0$0 == 0x0095
                           000095  1132 _NVDATA1	=	0x0095
                           009594  1133 G$NVDATA$0$0 == 0x9594
                           009594  1134 _NVDATA	=	0x9594
                           000096  1135 G$NVKEY$0$0 == 0x0096
                           000096  1136 _NVKEY	=	0x0096
                           000091  1137 G$NVSTATUS$0$0 == 0x0091
                           000091  1138 _NVSTATUS	=	0x0091
                           0000BC  1139 G$OC0COMP0$0$0 == 0x00bc
                           0000BC  1140 _OC0COMP0	=	0x00bc
                           0000BD  1141 G$OC0COMP1$0$0 == 0x00bd
                           0000BD  1142 _OC0COMP1	=	0x00bd
                           00BDBC  1143 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC  1144 _OC0COMP	=	0xbdbc
                           0000B9  1145 G$OC0MODE$0$0 == 0x00b9
                           0000B9  1146 _OC0MODE	=	0x00b9
                           0000BA  1147 G$OC0PIN$0$0 == 0x00ba
                           0000BA  1148 _OC0PIN	=	0x00ba
                           0000BB  1149 G$OC0STATUS$0$0 == 0x00bb
                           0000BB  1150 _OC0STATUS	=	0x00bb
                           0000C4  1151 G$OC1COMP0$0$0 == 0x00c4
                           0000C4  1152 _OC1COMP0	=	0x00c4
                           0000C5  1153 G$OC1COMP1$0$0 == 0x00c5
                           0000C5  1154 _OC1COMP1	=	0x00c5
                           00C5C4  1155 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4  1156 _OC1COMP	=	0xc5c4
                           0000C1  1157 G$OC1MODE$0$0 == 0x00c1
                           0000C1  1158 _OC1MODE	=	0x00c1
                           0000C2  1159 G$OC1PIN$0$0 == 0x00c2
                           0000C2  1160 _OC1PIN	=	0x00c2
                           0000C3  1161 G$OC1STATUS$0$0 == 0x00c3
                           0000C3  1162 _OC1STATUS	=	0x00c3
                           0000B1  1163 G$RADIOACC$0$0 == 0x00b1
                           0000B1  1164 _RADIOACC	=	0x00b1
                           0000B3  1165 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3  1166 _RADIOADDR0	=	0x00b3
                           0000B2  1167 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2  1168 _RADIOADDR1	=	0x00b2
                           00B2B3  1169 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3  1170 _RADIOADDR	=	0xb2b3
                           0000B7  1171 G$RADIODATA0$0$0 == 0x00b7
                           0000B7  1172 _RADIODATA0	=	0x00b7
                           0000B6  1173 G$RADIODATA1$0$0 == 0x00b6
                           0000B6  1174 _RADIODATA1	=	0x00b6
                           0000B5  1175 G$RADIODATA2$0$0 == 0x00b5
                           0000B5  1176 _RADIODATA2	=	0x00b5
                           0000B4  1177 G$RADIODATA3$0$0 == 0x00b4
                           0000B4  1178 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1179 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7  1180 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1181 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE  1182 _RADIOSTAT0	=	0x00be
                           0000BF  1183 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF  1184 _RADIOSTAT1	=	0x00bf
                           00BFBE  1185 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE  1186 _RADIOSTAT	=	0xbfbe
                           0000DF  1187 G$SPCLKSRC$0$0 == 0x00df
                           0000DF  1188 _SPCLKSRC	=	0x00df
                           0000DC  1189 G$SPMODE$0$0 == 0x00dc
                           0000DC  1190 _SPMODE	=	0x00dc
                           0000DE  1191 G$SPSHREG$0$0 == 0x00de
                           0000DE  1192 _SPSHREG	=	0x00de
                           0000DD  1193 G$SPSTATUS$0$0 == 0x00dd
                           0000DD  1194 _SPSTATUS	=	0x00dd
                           00009A  1195 G$T0CLKSRC$0$0 == 0x009a
                           00009A  1196 _T0CLKSRC	=	0x009a
                           00009C  1197 G$T0CNT0$0$0 == 0x009c
                           00009C  1198 _T0CNT0	=	0x009c
                           00009D  1199 G$T0CNT1$0$0 == 0x009d
                           00009D  1200 _T0CNT1	=	0x009d
                           009D9C  1201 G$T0CNT$0$0 == 0x9d9c
                           009D9C  1202 _T0CNT	=	0x9d9c
                           000099  1203 G$T0MODE$0$0 == 0x0099
                           000099  1204 _T0MODE	=	0x0099
                           00009E  1205 G$T0PERIOD0$0$0 == 0x009e
                           00009E  1206 _T0PERIOD0	=	0x009e
                           00009F  1207 G$T0PERIOD1$0$0 == 0x009f
                           00009F  1208 _T0PERIOD1	=	0x009f
                           009F9E  1209 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E  1210 _T0PERIOD	=	0x9f9e
                           00009B  1211 G$T0STATUS$0$0 == 0x009b
                           00009B  1212 _T0STATUS	=	0x009b
                           0000A2  1213 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2  1214 _T1CLKSRC	=	0x00a2
                           0000A4  1215 G$T1CNT0$0$0 == 0x00a4
                           0000A4  1216 _T1CNT0	=	0x00a4
                           0000A5  1217 G$T1CNT1$0$0 == 0x00a5
                           0000A5  1218 _T1CNT1	=	0x00a5
                           00A5A4  1219 G$T1CNT$0$0 == 0xa5a4
                           00A5A4  1220 _T1CNT	=	0xa5a4
                           0000A1  1221 G$T1MODE$0$0 == 0x00a1
                           0000A1  1222 _T1MODE	=	0x00a1
                           0000A6  1223 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6  1224 _T1PERIOD0	=	0x00a6
                           0000A7  1225 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7  1226 _T1PERIOD1	=	0x00a7
                           00A7A6  1227 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6  1228 _T1PERIOD	=	0xa7a6
                           0000A3  1229 G$T1STATUS$0$0 == 0x00a3
                           0000A3  1230 _T1STATUS	=	0x00a3
                           0000AA  1231 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA  1232 _T2CLKSRC	=	0x00aa
                           0000AC  1233 G$T2CNT0$0$0 == 0x00ac
                           0000AC  1234 _T2CNT0	=	0x00ac
                           0000AD  1235 G$T2CNT1$0$0 == 0x00ad
                           0000AD  1236 _T2CNT1	=	0x00ad
                           00ADAC  1237 G$T2CNT$0$0 == 0xadac
                           00ADAC  1238 _T2CNT	=	0xadac
                           0000A9  1239 G$T2MODE$0$0 == 0x00a9
                           0000A9  1240 _T2MODE	=	0x00a9
                           0000AE  1241 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE  1242 _T2PERIOD0	=	0x00ae
                           0000AF  1243 G$T2PERIOD1$0$0 == 0x00af
                           0000AF  1244 _T2PERIOD1	=	0x00af
                           00AFAE  1245 G$T2PERIOD$0$0 == 0xafae
                           00AFAE  1246 _T2PERIOD	=	0xafae
                           0000AB  1247 G$T2STATUS$0$0 == 0x00ab
                           0000AB  1248 _T2STATUS	=	0x00ab
                           0000E4  1249 G$U0CTRL$0$0 == 0x00e4
                           0000E4  1250 _U0CTRL	=	0x00e4
                           0000E7  1251 G$U0MODE$0$0 == 0x00e7
                           0000E7  1252 _U0MODE	=	0x00e7
                           0000E6  1253 G$U0SHREG$0$0 == 0x00e6
                           0000E6  1254 _U0SHREG	=	0x00e6
                           0000E5  1255 G$U0STATUS$0$0 == 0x00e5
                           0000E5  1256 _U0STATUS	=	0x00e5
                           0000EC  1257 G$U1CTRL$0$0 == 0x00ec
                           0000EC  1258 _U1CTRL	=	0x00ec
                           0000EF  1259 G$U1MODE$0$0 == 0x00ef
                           0000EF  1260 _U1MODE	=	0x00ef
                           0000EE  1261 G$U1SHREG$0$0 == 0x00ee
                           0000EE  1262 _U1SHREG	=	0x00ee
                           0000ED  1263 G$U1STATUS$0$0 == 0x00ed
                           0000ED  1264 _U1STATUS	=	0x00ed
                           0000DA  1265 G$WDTCFG$0$0 == 0x00da
                           0000DA  1266 _WDTCFG	=	0x00da
                           0000DB  1267 G$WDTRESET$0$0 == 0x00db
                           0000DB  1268 _WDTRESET	=	0x00db
                           0000F1  1269 G$WTCFGA$0$0 == 0x00f1
                           0000F1  1270 _WTCFGA	=	0x00f1
                           0000F9  1271 G$WTCFGB$0$0 == 0x00f9
                           0000F9  1272 _WTCFGB	=	0x00f9
                           0000F2  1273 G$WTCNTA0$0$0 == 0x00f2
                           0000F2  1274 _WTCNTA0	=	0x00f2
                           0000F3  1275 G$WTCNTA1$0$0 == 0x00f3
                           0000F3  1276 _WTCNTA1	=	0x00f3
                           00F3F2  1277 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2  1278 _WTCNTA	=	0xf3f2
                           0000FA  1279 G$WTCNTB0$0$0 == 0x00fa
                           0000FA  1280 _WTCNTB0	=	0x00fa
                           0000FB  1281 G$WTCNTB1$0$0 == 0x00fb
                           0000FB  1282 _WTCNTB1	=	0x00fb
                           00FBFA  1283 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA  1284 _WTCNTB	=	0xfbfa
                           0000EB  1285 G$WTCNTR1$0$0 == 0x00eb
                           0000EB  1286 _WTCNTR1	=	0x00eb
                           0000F4  1287 G$WTEVTA0$0$0 == 0x00f4
                           0000F4  1288 _WTEVTA0	=	0x00f4
                           0000F5  1289 G$WTEVTA1$0$0 == 0x00f5
                           0000F5  1290 _WTEVTA1	=	0x00f5
                           00F5F4  1291 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4  1292 _WTEVTA	=	0xf5f4
                           0000F6  1293 G$WTEVTB0$0$0 == 0x00f6
                           0000F6  1294 _WTEVTB0	=	0x00f6
                           0000F7  1295 G$WTEVTB1$0$0 == 0x00f7
                           0000F7  1296 _WTEVTB1	=	0x00f7
                           00F7F6  1297 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6  1298 _WTEVTB	=	0xf7f6
                           0000FC  1299 G$WTEVTC0$0$0 == 0x00fc
                           0000FC  1300 _WTEVTC0	=	0x00fc
                           0000FD  1301 G$WTEVTC1$0$0 == 0x00fd
                           0000FD  1302 _WTEVTC1	=	0x00fd
                           00FDFC  1303 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC  1304 _WTEVTC	=	0xfdfc
                           0000FE  1305 G$WTEVTD0$0$0 == 0x00fe
                           0000FE  1306 _WTEVTD0	=	0x00fe
                           0000FF  1307 G$WTEVTD1$0$0 == 0x00ff
                           0000FF  1308 _WTEVTD1	=	0x00ff
                           00FFFE  1309 G$WTEVTD$0$0 == 0xfffe
                           00FFFE  1310 _WTEVTD	=	0xfffe
                           0000E9  1311 G$WTIRQEN$0$0 == 0x00e9
                           0000E9  1312 _WTIRQEN	=	0x00e9
                           0000EA  1313 G$WTSTAT$0$0 == 0x00ea
                           0000EA  1314 _WTSTAT	=	0x00ea
                                   1315 ;--------------------------------------------------------
                                   1316 ; special function bits
                                   1317 ;--------------------------------------------------------
                                   1318 	.area RSEG    (ABS,DATA)
      000000                       1319 	.org 0x0000
                           0000E0  1320 G$ACC_0$0$0 == 0x00e0
                           0000E0  1321 _ACC_0	=	0x00e0
                           0000E1  1322 G$ACC_1$0$0 == 0x00e1
                           0000E1  1323 _ACC_1	=	0x00e1
                           0000E2  1324 G$ACC_2$0$0 == 0x00e2
                           0000E2  1325 _ACC_2	=	0x00e2
                           0000E3  1326 G$ACC_3$0$0 == 0x00e3
                           0000E3  1327 _ACC_3	=	0x00e3
                           0000E4  1328 G$ACC_4$0$0 == 0x00e4
                           0000E4  1329 _ACC_4	=	0x00e4
                           0000E5  1330 G$ACC_5$0$0 == 0x00e5
                           0000E5  1331 _ACC_5	=	0x00e5
                           0000E6  1332 G$ACC_6$0$0 == 0x00e6
                           0000E6  1333 _ACC_6	=	0x00e6
                           0000E7  1334 G$ACC_7$0$0 == 0x00e7
                           0000E7  1335 _ACC_7	=	0x00e7
                           0000F0  1336 G$B_0$0$0 == 0x00f0
                           0000F0  1337 _B_0	=	0x00f0
                           0000F1  1338 G$B_1$0$0 == 0x00f1
                           0000F1  1339 _B_1	=	0x00f1
                           0000F2  1340 G$B_2$0$0 == 0x00f2
                           0000F2  1341 _B_2	=	0x00f2
                           0000F3  1342 G$B_3$0$0 == 0x00f3
                           0000F3  1343 _B_3	=	0x00f3
                           0000F4  1344 G$B_4$0$0 == 0x00f4
                           0000F4  1345 _B_4	=	0x00f4
                           0000F5  1346 G$B_5$0$0 == 0x00f5
                           0000F5  1347 _B_5	=	0x00f5
                           0000F6  1348 G$B_6$0$0 == 0x00f6
                           0000F6  1349 _B_6	=	0x00f6
                           0000F7  1350 G$B_7$0$0 == 0x00f7
                           0000F7  1351 _B_7	=	0x00f7
                           0000A0  1352 G$E2IE_0$0$0 == 0x00a0
                           0000A0  1353 _E2IE_0	=	0x00a0
                           0000A1  1354 G$E2IE_1$0$0 == 0x00a1
                           0000A1  1355 _E2IE_1	=	0x00a1
                           0000A2  1356 G$E2IE_2$0$0 == 0x00a2
                           0000A2  1357 _E2IE_2	=	0x00a2
                           0000A3  1358 G$E2IE_3$0$0 == 0x00a3
                           0000A3  1359 _E2IE_3	=	0x00a3
                           0000A4  1360 G$E2IE_4$0$0 == 0x00a4
                           0000A4  1361 _E2IE_4	=	0x00a4
                           0000A5  1362 G$E2IE_5$0$0 == 0x00a5
                           0000A5  1363 _E2IE_5	=	0x00a5
                           0000A6  1364 G$E2IE_6$0$0 == 0x00a6
                           0000A6  1365 _E2IE_6	=	0x00a6
                           0000A7  1366 G$E2IE_7$0$0 == 0x00a7
                           0000A7  1367 _E2IE_7	=	0x00a7
                           0000C0  1368 G$E2IP_0$0$0 == 0x00c0
                           0000C0  1369 _E2IP_0	=	0x00c0
                           0000C1  1370 G$E2IP_1$0$0 == 0x00c1
                           0000C1  1371 _E2IP_1	=	0x00c1
                           0000C2  1372 G$E2IP_2$0$0 == 0x00c2
                           0000C2  1373 _E2IP_2	=	0x00c2
                           0000C3  1374 G$E2IP_3$0$0 == 0x00c3
                           0000C3  1375 _E2IP_3	=	0x00c3
                           0000C4  1376 G$E2IP_4$0$0 == 0x00c4
                           0000C4  1377 _E2IP_4	=	0x00c4
                           0000C5  1378 G$E2IP_5$0$0 == 0x00c5
                           0000C5  1379 _E2IP_5	=	0x00c5
                           0000C6  1380 G$E2IP_6$0$0 == 0x00c6
                           0000C6  1381 _E2IP_6	=	0x00c6
                           0000C7  1382 G$E2IP_7$0$0 == 0x00c7
                           0000C7  1383 _E2IP_7	=	0x00c7
                           000098  1384 G$EIE_0$0$0 == 0x0098
                           000098  1385 _EIE_0	=	0x0098
                           000099  1386 G$EIE_1$0$0 == 0x0099
                           000099  1387 _EIE_1	=	0x0099
                           00009A  1388 G$EIE_2$0$0 == 0x009a
                           00009A  1389 _EIE_2	=	0x009a
                           00009B  1390 G$EIE_3$0$0 == 0x009b
                           00009B  1391 _EIE_3	=	0x009b
                           00009C  1392 G$EIE_4$0$0 == 0x009c
                           00009C  1393 _EIE_4	=	0x009c
                           00009D  1394 G$EIE_5$0$0 == 0x009d
                           00009D  1395 _EIE_5	=	0x009d
                           00009E  1396 G$EIE_6$0$0 == 0x009e
                           00009E  1397 _EIE_6	=	0x009e
                           00009F  1398 G$EIE_7$0$0 == 0x009f
                           00009F  1399 _EIE_7	=	0x009f
                           0000B0  1400 G$EIP_0$0$0 == 0x00b0
                           0000B0  1401 _EIP_0	=	0x00b0
                           0000B1  1402 G$EIP_1$0$0 == 0x00b1
                           0000B1  1403 _EIP_1	=	0x00b1
                           0000B2  1404 G$EIP_2$0$0 == 0x00b2
                           0000B2  1405 _EIP_2	=	0x00b2
                           0000B3  1406 G$EIP_3$0$0 == 0x00b3
                           0000B3  1407 _EIP_3	=	0x00b3
                           0000B4  1408 G$EIP_4$0$0 == 0x00b4
                           0000B4  1409 _EIP_4	=	0x00b4
                           0000B5  1410 G$EIP_5$0$0 == 0x00b5
                           0000B5  1411 _EIP_5	=	0x00b5
                           0000B6  1412 G$EIP_6$0$0 == 0x00b6
                           0000B6  1413 _EIP_6	=	0x00b6
                           0000B7  1414 G$EIP_7$0$0 == 0x00b7
                           0000B7  1415 _EIP_7	=	0x00b7
                           0000A8  1416 G$IE_0$0$0 == 0x00a8
                           0000A8  1417 _IE_0	=	0x00a8
                           0000A9  1418 G$IE_1$0$0 == 0x00a9
                           0000A9  1419 _IE_1	=	0x00a9
                           0000AA  1420 G$IE_2$0$0 == 0x00aa
                           0000AA  1421 _IE_2	=	0x00aa
                           0000AB  1422 G$IE_3$0$0 == 0x00ab
                           0000AB  1423 _IE_3	=	0x00ab
                           0000AC  1424 G$IE_4$0$0 == 0x00ac
                           0000AC  1425 _IE_4	=	0x00ac
                           0000AD  1426 G$IE_5$0$0 == 0x00ad
                           0000AD  1427 _IE_5	=	0x00ad
                           0000AE  1428 G$IE_6$0$0 == 0x00ae
                           0000AE  1429 _IE_6	=	0x00ae
                           0000AF  1430 G$IE_7$0$0 == 0x00af
                           0000AF  1431 _IE_7	=	0x00af
                           0000AF  1432 G$EA$0$0 == 0x00af
                           0000AF  1433 _EA	=	0x00af
                           0000B8  1434 G$IP_0$0$0 == 0x00b8
                           0000B8  1435 _IP_0	=	0x00b8
                           0000B9  1436 G$IP_1$0$0 == 0x00b9
                           0000B9  1437 _IP_1	=	0x00b9
                           0000BA  1438 G$IP_2$0$0 == 0x00ba
                           0000BA  1439 _IP_2	=	0x00ba
                           0000BB  1440 G$IP_3$0$0 == 0x00bb
                           0000BB  1441 _IP_3	=	0x00bb
                           0000BC  1442 G$IP_4$0$0 == 0x00bc
                           0000BC  1443 _IP_4	=	0x00bc
                           0000BD  1444 G$IP_5$0$0 == 0x00bd
                           0000BD  1445 _IP_5	=	0x00bd
                           0000BE  1446 G$IP_6$0$0 == 0x00be
                           0000BE  1447 _IP_6	=	0x00be
                           0000BF  1448 G$IP_7$0$0 == 0x00bf
                           0000BF  1449 _IP_7	=	0x00bf
                           0000D0  1450 G$P$0$0 == 0x00d0
                           0000D0  1451 _P	=	0x00d0
                           0000D1  1452 G$F1$0$0 == 0x00d1
                           0000D1  1453 _F1	=	0x00d1
                           0000D2  1454 G$OV$0$0 == 0x00d2
                           0000D2  1455 _OV	=	0x00d2
                           0000D3  1456 G$RS0$0$0 == 0x00d3
                           0000D3  1457 _RS0	=	0x00d3
                           0000D4  1458 G$RS1$0$0 == 0x00d4
                           0000D4  1459 _RS1	=	0x00d4
                           0000D5  1460 G$F0$0$0 == 0x00d5
                           0000D5  1461 _F0	=	0x00d5
                           0000D6  1462 G$AC$0$0 == 0x00d6
                           0000D6  1463 _AC	=	0x00d6
                           0000D7  1464 G$CY$0$0 == 0x00d7
                           0000D7  1465 _CY	=	0x00d7
                           0000C8  1466 G$PINA_0$0$0 == 0x00c8
                           0000C8  1467 _PINA_0	=	0x00c8
                           0000C9  1468 G$PINA_1$0$0 == 0x00c9
                           0000C9  1469 _PINA_1	=	0x00c9
                           0000CA  1470 G$PINA_2$0$0 == 0x00ca
                           0000CA  1471 _PINA_2	=	0x00ca
                           0000CB  1472 G$PINA_3$0$0 == 0x00cb
                           0000CB  1473 _PINA_3	=	0x00cb
                           0000CC  1474 G$PINA_4$0$0 == 0x00cc
                           0000CC  1475 _PINA_4	=	0x00cc
                           0000CD  1476 G$PINA_5$0$0 == 0x00cd
                           0000CD  1477 _PINA_5	=	0x00cd
                           0000CE  1478 G$PINA_6$0$0 == 0x00ce
                           0000CE  1479 _PINA_6	=	0x00ce
                           0000CF  1480 G$PINA_7$0$0 == 0x00cf
                           0000CF  1481 _PINA_7	=	0x00cf
                           0000E8  1482 G$PINB_0$0$0 == 0x00e8
                           0000E8  1483 _PINB_0	=	0x00e8
                           0000E9  1484 G$PINB_1$0$0 == 0x00e9
                           0000E9  1485 _PINB_1	=	0x00e9
                           0000EA  1486 G$PINB_2$0$0 == 0x00ea
                           0000EA  1487 _PINB_2	=	0x00ea
                           0000EB  1488 G$PINB_3$0$0 == 0x00eb
                           0000EB  1489 _PINB_3	=	0x00eb
                           0000EC  1490 G$PINB_4$0$0 == 0x00ec
                           0000EC  1491 _PINB_4	=	0x00ec
                           0000ED  1492 G$PINB_5$0$0 == 0x00ed
                           0000ED  1493 _PINB_5	=	0x00ed
                           0000EE  1494 G$PINB_6$0$0 == 0x00ee
                           0000EE  1495 _PINB_6	=	0x00ee
                           0000EF  1496 G$PINB_7$0$0 == 0x00ef
                           0000EF  1497 _PINB_7	=	0x00ef
                           0000F8  1498 G$PINC_0$0$0 == 0x00f8
                           0000F8  1499 _PINC_0	=	0x00f8
                           0000F9  1500 G$PINC_1$0$0 == 0x00f9
                           0000F9  1501 _PINC_1	=	0x00f9
                           0000FA  1502 G$PINC_2$0$0 == 0x00fa
                           0000FA  1503 _PINC_2	=	0x00fa
                           0000FB  1504 G$PINC_3$0$0 == 0x00fb
                           0000FB  1505 _PINC_3	=	0x00fb
                           0000FC  1506 G$PINC_4$0$0 == 0x00fc
                           0000FC  1507 _PINC_4	=	0x00fc
                           0000FD  1508 G$PINC_5$0$0 == 0x00fd
                           0000FD  1509 _PINC_5	=	0x00fd
                           0000FE  1510 G$PINC_6$0$0 == 0x00fe
                           0000FE  1511 _PINC_6	=	0x00fe
                           0000FF  1512 G$PINC_7$0$0 == 0x00ff
                           0000FF  1513 _PINC_7	=	0x00ff
                           000080  1514 G$PORTA_0$0$0 == 0x0080
                           000080  1515 _PORTA_0	=	0x0080
                           000081  1516 G$PORTA_1$0$0 == 0x0081
                           000081  1517 _PORTA_1	=	0x0081
                           000082  1518 G$PORTA_2$0$0 == 0x0082
                           000082  1519 _PORTA_2	=	0x0082
                           000083  1520 G$PORTA_3$0$0 == 0x0083
                           000083  1521 _PORTA_3	=	0x0083
                           000084  1522 G$PORTA_4$0$0 == 0x0084
                           000084  1523 _PORTA_4	=	0x0084
                           000085  1524 G$PORTA_5$0$0 == 0x0085
                           000085  1525 _PORTA_5	=	0x0085
                           000086  1526 G$PORTA_6$0$0 == 0x0086
                           000086  1527 _PORTA_6	=	0x0086
                           000087  1528 G$PORTA_7$0$0 == 0x0087
                           000087  1529 _PORTA_7	=	0x0087
                           000088  1530 G$PORTB_0$0$0 == 0x0088
                           000088  1531 _PORTB_0	=	0x0088
                           000089  1532 G$PORTB_1$0$0 == 0x0089
                           000089  1533 _PORTB_1	=	0x0089
                           00008A  1534 G$PORTB_2$0$0 == 0x008a
                           00008A  1535 _PORTB_2	=	0x008a
                           00008B  1536 G$PORTB_3$0$0 == 0x008b
                           00008B  1537 _PORTB_3	=	0x008b
                           00008C  1538 G$PORTB_4$0$0 == 0x008c
                           00008C  1539 _PORTB_4	=	0x008c
                           00008D  1540 G$PORTB_5$0$0 == 0x008d
                           00008D  1541 _PORTB_5	=	0x008d
                           00008E  1542 G$PORTB_6$0$0 == 0x008e
                           00008E  1543 _PORTB_6	=	0x008e
                           00008F  1544 G$PORTB_7$0$0 == 0x008f
                           00008F  1545 _PORTB_7	=	0x008f
                           000090  1546 G$PORTC_0$0$0 == 0x0090
                           000090  1547 _PORTC_0	=	0x0090
                           000091  1548 G$PORTC_1$0$0 == 0x0091
                           000091  1549 _PORTC_1	=	0x0091
                           000092  1550 G$PORTC_2$0$0 == 0x0092
                           000092  1551 _PORTC_2	=	0x0092
                           000093  1552 G$PORTC_3$0$0 == 0x0093
                           000093  1553 _PORTC_3	=	0x0093
                           000094  1554 G$PORTC_4$0$0 == 0x0094
                           000094  1555 _PORTC_4	=	0x0094
                           000095  1556 G$PORTC_5$0$0 == 0x0095
                           000095  1557 _PORTC_5	=	0x0095
                           000096  1558 G$PORTC_6$0$0 == 0x0096
                           000096  1559 _PORTC_6	=	0x0096
                           000097  1560 G$PORTC_7$0$0 == 0x0097
                           000097  1561 _PORTC_7	=	0x0097
                                   1562 ;--------------------------------------------------------
                                   1563 ; overlayable register banks
                                   1564 ;--------------------------------------------------------
                                   1565 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1566 	.ds 8
                                   1567 ;--------------------------------------------------------
                                   1568 ; internal ram data
                                   1569 ;--------------------------------------------------------
                                   1570 	.area DSEG    (DATA)
                           000000  1571 G$coldstart$0$0==.
      000050                       1572 _coldstart::
      000050                       1573 	.ds 1
                           000001  1574 Lmain.axradio_statuschange$st$1$370==.
      000051                       1575 _axradio_statuschange_st_1_370:
      000051                       1576 	.ds 2
                           000003  1577 Lmain.axradio_statuschange$sloc0$1$0==.
      000053                       1578 _axradio_statuschange_sloc0_1_0:
      000053                       1579 	.ds 4
                                   1580 ;--------------------------------------------------------
                                   1581 ; overlayable items in internal ram 
                                   1582 ;--------------------------------------------------------
                                   1583 ;--------------------------------------------------------
                                   1584 ; Stack segment in internal ram 
                                   1585 ;--------------------------------------------------------
                                   1586 	.area	SSEG
      00007E                       1587 __start__stack:
      00007E                       1588 	.ds	1
                                   1589 
                                   1590 ;--------------------------------------------------------
                                   1591 ; indirectly addressable internal ram data
                                   1592 ;--------------------------------------------------------
                                   1593 	.area ISEG    (DATA)
                                   1594 ;--------------------------------------------------------
                                   1595 ; absolute internal ram data
                                   1596 ;--------------------------------------------------------
                                   1597 	.area IABS    (ABS,DATA)
                                   1598 	.area IABS    (ABS,DATA)
                                   1599 ;--------------------------------------------------------
                                   1600 ; bit data
                                   1601 ;--------------------------------------------------------
                                   1602 	.area BSEG    (BIT)
                           000000  1603 Lmain._sdcc_external_startup$sloc0$1$0==.
      000001                       1604 __sdcc_external_startup_sloc0_1_0:
      000001                       1605 	.ds 1
                                   1606 ;--------------------------------------------------------
                                   1607 ; paged external ram data
                                   1608 ;--------------------------------------------------------
                                   1609 	.area PSEG    (PAG,XDATA)
                                   1610 ;--------------------------------------------------------
                                   1611 ; external ram data
                                   1612 ;--------------------------------------------------------
                                   1613 	.area XSEG    (XDATA)
                           007020  1614 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1615 _ADCCH0VAL0	=	0x7020
                           007021  1616 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1617 _ADCCH0VAL1	=	0x7021
                           007020  1618 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1619 _ADCCH0VAL	=	0x7020
                           007022  1620 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1621 _ADCCH1VAL0	=	0x7022
                           007023  1622 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1623 _ADCCH1VAL1	=	0x7023
                           007022  1624 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1625 _ADCCH1VAL	=	0x7022
                           007024  1626 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1627 _ADCCH2VAL0	=	0x7024
                           007025  1628 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1629 _ADCCH2VAL1	=	0x7025
                           007024  1630 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1631 _ADCCH2VAL	=	0x7024
                           007026  1632 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1633 _ADCCH3VAL0	=	0x7026
                           007027  1634 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1635 _ADCCH3VAL1	=	0x7027
                           007026  1636 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1637 _ADCCH3VAL	=	0x7026
                           007028  1638 G$ADCTUNE0$0$0 == 0x7028
                           007028  1639 _ADCTUNE0	=	0x7028
                           007029  1640 G$ADCTUNE1$0$0 == 0x7029
                           007029  1641 _ADCTUNE1	=	0x7029
                           00702A  1642 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1643 _ADCTUNE2	=	0x702a
                           007010  1644 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1645 _DMA0ADDR0	=	0x7010
                           007011  1646 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1647 _DMA0ADDR1	=	0x7011
                           007010  1648 G$DMA0ADDR$0$0 == 0x7010
                           007010  1649 _DMA0ADDR	=	0x7010
                           007014  1650 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1651 _DMA0CONFIG	=	0x7014
                           007012  1652 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1653 _DMA1ADDR0	=	0x7012
                           007013  1654 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1655 _DMA1ADDR1	=	0x7013
                           007012  1656 G$DMA1ADDR$0$0 == 0x7012
                           007012  1657 _DMA1ADDR	=	0x7012
                           007015  1658 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1659 _DMA1CONFIG	=	0x7015
                           007070  1660 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1661 _FRCOSCCONFIG	=	0x7070
                           007071  1662 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1663 _FRCOSCCTRL	=	0x7071
                           007076  1664 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1665 _FRCOSCFREQ0	=	0x7076
                           007077  1666 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1667 _FRCOSCFREQ1	=	0x7077
                           007076  1668 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1669 _FRCOSCFREQ	=	0x7076
                           007072  1670 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1671 _FRCOSCKFILT0	=	0x7072
                           007073  1672 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1673 _FRCOSCKFILT1	=	0x7073
                           007072  1674 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1675 _FRCOSCKFILT	=	0x7072
                           007078  1676 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1677 _FRCOSCPER0	=	0x7078
                           007079  1678 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1679 _FRCOSCPER1	=	0x7079
                           007078  1680 G$FRCOSCPER$0$0 == 0x7078
                           007078  1681 _FRCOSCPER	=	0x7078
                           007074  1682 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1683 _FRCOSCREF0	=	0x7074
                           007075  1684 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1685 _FRCOSCREF1	=	0x7075
                           007074  1686 G$FRCOSCREF$0$0 == 0x7074
                           007074  1687 _FRCOSCREF	=	0x7074
                           007007  1688 G$ANALOGA$0$0 == 0x7007
                           007007  1689 _ANALOGA	=	0x7007
                           00700C  1690 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1691 _GPIOENABLE	=	0x700c
                           007003  1692 G$EXTIRQ$0$0 == 0x7003
                           007003  1693 _EXTIRQ	=	0x7003
                           007000  1694 G$INTCHGA$0$0 == 0x7000
                           007000  1695 _INTCHGA	=	0x7000
                           007001  1696 G$INTCHGB$0$0 == 0x7001
                           007001  1697 _INTCHGB	=	0x7001
                           007002  1698 G$INTCHGC$0$0 == 0x7002
                           007002  1699 _INTCHGC	=	0x7002
                           007008  1700 G$PALTA$0$0 == 0x7008
                           007008  1701 _PALTA	=	0x7008
                           007009  1702 G$PALTB$0$0 == 0x7009
                           007009  1703 _PALTB	=	0x7009
                           00700A  1704 G$PALTC$0$0 == 0x700a
                           00700A  1705 _PALTC	=	0x700a
                           007046  1706 G$PALTRADIO$0$0 == 0x7046
                           007046  1707 _PALTRADIO	=	0x7046
                           007004  1708 G$PINCHGA$0$0 == 0x7004
                           007004  1709 _PINCHGA	=	0x7004
                           007005  1710 G$PINCHGB$0$0 == 0x7005
                           007005  1711 _PINCHGB	=	0x7005
                           007006  1712 G$PINCHGC$0$0 == 0x7006
                           007006  1713 _PINCHGC	=	0x7006
                           00700B  1714 G$PINSEL$0$0 == 0x700b
                           00700B  1715 _PINSEL	=	0x700b
                           007060  1716 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1717 _LPOSCCONFIG	=	0x7060
                           007066  1718 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1719 _LPOSCFREQ0	=	0x7066
                           007067  1720 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1721 _LPOSCFREQ1	=	0x7067
                           007066  1722 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1723 _LPOSCFREQ	=	0x7066
                           007062  1724 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1725 _LPOSCKFILT0	=	0x7062
                           007063  1726 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1727 _LPOSCKFILT1	=	0x7063
                           007062  1728 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1729 _LPOSCKFILT	=	0x7062
                           007068  1730 G$LPOSCPER0$0$0 == 0x7068
                           007068  1731 _LPOSCPER0	=	0x7068
                           007069  1732 G$LPOSCPER1$0$0 == 0x7069
                           007069  1733 _LPOSCPER1	=	0x7069
                           007068  1734 G$LPOSCPER$0$0 == 0x7068
                           007068  1735 _LPOSCPER	=	0x7068
                           007064  1736 G$LPOSCREF0$0$0 == 0x7064
                           007064  1737 _LPOSCREF0	=	0x7064
                           007065  1738 G$LPOSCREF1$0$0 == 0x7065
                           007065  1739 _LPOSCREF1	=	0x7065
                           007064  1740 G$LPOSCREF$0$0 == 0x7064
                           007064  1741 _LPOSCREF	=	0x7064
                           007054  1742 G$LPXOSCGM$0$0 == 0x7054
                           007054  1743 _LPXOSCGM	=	0x7054
                           007F01  1744 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1745 _MISCCTRL	=	0x7f01
                           007053  1746 G$OSCCALIB$0$0 == 0x7053
                           007053  1747 _OSCCALIB	=	0x7053
                           007050  1748 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1749 _OSCFORCERUN	=	0x7050
                           007052  1750 G$OSCREADY$0$0 == 0x7052
                           007052  1751 _OSCREADY	=	0x7052
                           007051  1752 G$OSCRUN$0$0 == 0x7051
                           007051  1753 _OSCRUN	=	0x7051
                           007040  1754 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1755 _RADIOFDATAADDR0	=	0x7040
                           007041  1756 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1757 _RADIOFDATAADDR1	=	0x7041
                           007040  1758 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1759 _RADIOFDATAADDR	=	0x7040
                           007042  1760 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1761 _RADIOFSTATADDR0	=	0x7042
                           007043  1762 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1763 _RADIOFSTATADDR1	=	0x7043
                           007042  1764 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1765 _RADIOFSTATADDR	=	0x7042
                           007044  1766 G$RADIOMUX$0$0 == 0x7044
                           007044  1767 _RADIOMUX	=	0x7044
                           007084  1768 G$SCRATCH0$0$0 == 0x7084
                           007084  1769 _SCRATCH0	=	0x7084
                           007085  1770 G$SCRATCH1$0$0 == 0x7085
                           007085  1771 _SCRATCH1	=	0x7085
                           007086  1772 G$SCRATCH2$0$0 == 0x7086
                           007086  1773 _SCRATCH2	=	0x7086
                           007087  1774 G$SCRATCH3$0$0 == 0x7087
                           007087  1775 _SCRATCH3	=	0x7087
                           007F00  1776 G$SILICONREV$0$0 == 0x7f00
                           007F00  1777 _SILICONREV	=	0x7f00
                           007F19  1778 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1779 _XTALAMPL	=	0x7f19
                           007F18  1780 G$XTALOSC$0$0 == 0x7f18
                           007F18  1781 _XTALOSC	=	0x7f18
                           007F1A  1782 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1783 _XTALREADY	=	0x7f1a
                           00FC06  1784 Fmain$flash_deviceid$0$0 == 0xfc06
                           00FC06  1785 _flash_deviceid	=	0xfc06
                           00FC00  1786 Fmain$flash_calsector$0$0 == 0xfc00
                           00FC00  1787 _flash_calsector	=	0xfc00
                           007091  1788 G$AESCONFIG$0$0 == 0x7091
                           007091  1789 _AESCONFIG	=	0x7091
                           007098  1790 G$AESCURBLOCK$0$0 == 0x7098
                           007098  1791 _AESCURBLOCK	=	0x7098
                           007094  1792 G$AESINADDR0$0$0 == 0x7094
                           007094  1793 _AESINADDR0	=	0x7094
                           007095  1794 G$AESINADDR1$0$0 == 0x7095
                           007095  1795 _AESINADDR1	=	0x7095
                           007094  1796 G$AESINADDR$0$0 == 0x7094
                           007094  1797 _AESINADDR	=	0x7094
                           007092  1798 G$AESKEYADDR0$0$0 == 0x7092
                           007092  1799 _AESKEYADDR0	=	0x7092
                           007093  1800 G$AESKEYADDR1$0$0 == 0x7093
                           007093  1801 _AESKEYADDR1	=	0x7093
                           007092  1802 G$AESKEYADDR$0$0 == 0x7092
                           007092  1803 _AESKEYADDR	=	0x7092
                           007090  1804 G$AESMODE$0$0 == 0x7090
                           007090  1805 _AESMODE	=	0x7090
                           007096  1806 G$AESOUTADDR0$0$0 == 0x7096
                           007096  1807 _AESOUTADDR0	=	0x7096
                           007097  1808 G$AESOUTADDR1$0$0 == 0x7097
                           007097  1809 _AESOUTADDR1	=	0x7097
                           007096  1810 G$AESOUTADDR$0$0 == 0x7096
                           007096  1811 _AESOUTADDR	=	0x7096
                           007081  1812 G$RNGBYTE$0$0 == 0x7081
                           007081  1813 _RNGBYTE	=	0x7081
                           007082  1814 G$RNGCLKSRC0$0$0 == 0x7082
                           007082  1815 _RNGCLKSRC0	=	0x7082
                           007083  1816 G$RNGCLKSRC1$0$0 == 0x7083
                           007083  1817 _RNGCLKSRC1	=	0x7083
                           007080  1818 G$RNGMODE$0$0 == 0x7080
                           007080  1819 _RNGMODE	=	0x7080
                           004114  1820 G$AX5043_AFSKCTRL$0$0 == 0x4114
                           004114  1821 _AX5043_AFSKCTRL	=	0x4114
                           004113  1822 G$AX5043_AFSKMARK0$0$0 == 0x4113
                           004113  1823 _AX5043_AFSKMARK0	=	0x4113
                           004112  1824 G$AX5043_AFSKMARK1$0$0 == 0x4112
                           004112  1825 _AX5043_AFSKMARK1	=	0x4112
                           004111  1826 G$AX5043_AFSKSPACE0$0$0 == 0x4111
                           004111  1827 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1828 G$AX5043_AFSKSPACE1$0$0 == 0x4110
                           004110  1829 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1830 G$AX5043_AGCCOUNTER$0$0 == 0x4043
                           004043  1831 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1832 G$AX5043_AMPLFILTER$0$0 == 0x4115
                           004115  1833 _AX5043_AMPLFILTER	=	0x4115
                           004189  1834 G$AX5043_BBOFFSCAP$0$0 == 0x4189
                           004189  1835 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1836 G$AX5043_BBTUNE$0$0 == 0x4188
                           004188  1837 _AX5043_BBTUNE	=	0x4188
                           004041  1838 G$AX5043_BGNDRSSI$0$0 == 0x4041
                           004041  1839 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1840 G$AX5043_BGNDRSSIGAIN$0$0 == 0x422e
                           00422E  1841 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1842 G$AX5043_BGNDRSSITHR$0$0 == 0x422f
                           00422F  1843 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1844 G$AX5043_CRCINIT0$0$0 == 0x4017
                           004017  1845 _AX5043_CRCINIT0	=	0x4017
                           004016  1846 G$AX5043_CRCINIT1$0$0 == 0x4016
                           004016  1847 _AX5043_CRCINIT1	=	0x4016
                           004015  1848 G$AX5043_CRCINIT2$0$0 == 0x4015
                           004015  1849 _AX5043_CRCINIT2	=	0x4015
                           004014  1850 G$AX5043_CRCINIT3$0$0 == 0x4014
                           004014  1851 _AX5043_CRCINIT3	=	0x4014
                           004332  1852 G$AX5043_DACCONFIG$0$0 == 0x4332
                           004332  1853 _AX5043_DACCONFIG	=	0x4332
                           004331  1854 G$AX5043_DACVALUE0$0$0 == 0x4331
                           004331  1855 _AX5043_DACVALUE0	=	0x4331
                           004330  1856 G$AX5043_DACVALUE1$0$0 == 0x4330
                           004330  1857 _AX5043_DACVALUE1	=	0x4330
                           004102  1858 G$AX5043_DECIMATION$0$0 == 0x4102
                           004102  1859 _AX5043_DECIMATION	=	0x4102
                           004042  1860 G$AX5043_DIVERSITY$0$0 == 0x4042
                           004042  1861 _AX5043_DIVERSITY	=	0x4042
                           004011  1862 G$AX5043_ENCODING$0$0 == 0x4011
                           004011  1863 _AX5043_ENCODING	=	0x4011
                           004018  1864 G$AX5043_FEC$0$0 == 0x4018
                           004018  1865 _AX5043_FEC	=	0x4018
                           00401A  1866 G$AX5043_FECSTATUS$0$0 == 0x401a
                           00401A  1867 _AX5043_FECSTATUS	=	0x401a
                           004019  1868 G$AX5043_FECSYNC$0$0 == 0x4019
                           004019  1869 _AX5043_FECSYNC	=	0x4019
                           00402B  1870 G$AX5043_FIFOCOUNT0$0$0 == 0x402b
                           00402B  1871 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1872 G$AX5043_FIFOCOUNT1$0$0 == 0x402a
                           00402A  1873 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1874 G$AX5043_FIFODATA$0$0 == 0x4029
                           004029  1875 _AX5043_FIFODATA	=	0x4029
                           00402D  1876 G$AX5043_FIFOFREE0$0$0 == 0x402d
                           00402D  1877 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1878 G$AX5043_FIFOFREE1$0$0 == 0x402c
                           00402C  1879 _AX5043_FIFOFREE1	=	0x402c
                           004028  1880 G$AX5043_FIFOSTAT$0$0 == 0x4028
                           004028  1881 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1882 G$AX5043_FIFOTHRESH0$0$0 == 0x402f
                           00402F  1883 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1884 G$AX5043_FIFOTHRESH1$0$0 == 0x402e
                           00402E  1885 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1886 G$AX5043_FRAMING$0$0 == 0x4012
                           004012  1887 _AX5043_FRAMING	=	0x4012
                           004037  1888 G$AX5043_FREQA0$0$0 == 0x4037
                           004037  1889 _AX5043_FREQA0	=	0x4037
                           004036  1890 G$AX5043_FREQA1$0$0 == 0x4036
                           004036  1891 _AX5043_FREQA1	=	0x4036
                           004035  1892 G$AX5043_FREQA2$0$0 == 0x4035
                           004035  1893 _AX5043_FREQA2	=	0x4035
                           004034  1894 G$AX5043_FREQA3$0$0 == 0x4034
                           004034  1895 _AX5043_FREQA3	=	0x4034
                           00403F  1896 G$AX5043_FREQB0$0$0 == 0x403f
                           00403F  1897 _AX5043_FREQB0	=	0x403f
                           00403E  1898 G$AX5043_FREQB1$0$0 == 0x403e
                           00403E  1899 _AX5043_FREQB1	=	0x403e
                           00403D  1900 G$AX5043_FREQB2$0$0 == 0x403d
                           00403D  1901 _AX5043_FREQB2	=	0x403d
                           00403C  1902 G$AX5043_FREQB3$0$0 == 0x403c
                           00403C  1903 _AX5043_FREQB3	=	0x403c
                           004163  1904 G$AX5043_FSKDEV0$0$0 == 0x4163
                           004163  1905 _AX5043_FSKDEV0	=	0x4163
                           004162  1906 G$AX5043_FSKDEV1$0$0 == 0x4162
                           004162  1907 _AX5043_FSKDEV1	=	0x4162
                           004161  1908 G$AX5043_FSKDEV2$0$0 == 0x4161
                           004161  1909 _AX5043_FSKDEV2	=	0x4161
                           00410D  1910 G$AX5043_FSKDMAX0$0$0 == 0x410d
                           00410D  1911 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1912 G$AX5043_FSKDMAX1$0$0 == 0x410c
                           00410C  1913 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1914 G$AX5043_FSKDMIN0$0$0 == 0x410f
                           00410F  1915 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1916 G$AX5043_FSKDMIN1$0$0 == 0x410e
                           00410E  1917 _AX5043_FSKDMIN1	=	0x410e
                           004309  1918 G$AX5043_GPADC13VALUE0$0$0 == 0x4309
                           004309  1919 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1920 G$AX5043_GPADC13VALUE1$0$0 == 0x4308
                           004308  1921 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1922 G$AX5043_GPADCCTRL$0$0 == 0x4300
                           004300  1923 _AX5043_GPADCCTRL	=	0x4300
                           004301  1924 G$AX5043_GPADCPERIOD$0$0 == 0x4301
                           004301  1925 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1926 G$AX5043_IFFREQ0$0$0 == 0x4101
                           004101  1927 _AX5043_IFFREQ0	=	0x4101
                           004100  1928 G$AX5043_IFFREQ1$0$0 == 0x4100
                           004100  1929 _AX5043_IFFREQ1	=	0x4100
                           00400B  1930 G$AX5043_IRQINVERSION0$0$0 == 0x400b
                           00400B  1931 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1932 G$AX5043_IRQINVERSION1$0$0 == 0x400a
                           00400A  1933 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1934 G$AX5043_IRQMASK0$0$0 == 0x4007
                           004007  1935 _AX5043_IRQMASK0	=	0x4007
                           004006  1936 G$AX5043_IRQMASK1$0$0 == 0x4006
                           004006  1937 _AX5043_IRQMASK1	=	0x4006
                           00400D  1938 G$AX5043_IRQREQUEST0$0$0 == 0x400d
                           00400D  1939 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1940 G$AX5043_IRQREQUEST1$0$0 == 0x400c
                           00400C  1941 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1942 G$AX5043_LPOSCCONFIG$0$0 == 0x4310
                           004310  1943 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1944 G$AX5043_LPOSCFREQ0$0$0 == 0x4317
                           004317  1945 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1946 G$AX5043_LPOSCFREQ1$0$0 == 0x4316
                           004316  1947 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1948 G$AX5043_LPOSCKFILT0$0$0 == 0x4313
                           004313  1949 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1950 G$AX5043_LPOSCKFILT1$0$0 == 0x4312
                           004312  1951 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1952 G$AX5043_LPOSCPER0$0$0 == 0x4319
                           004319  1953 _AX5043_LPOSCPER0	=	0x4319
                           004318  1954 G$AX5043_LPOSCPER1$0$0 == 0x4318
                           004318  1955 _AX5043_LPOSCPER1	=	0x4318
                           004315  1956 G$AX5043_LPOSCREF0$0$0 == 0x4315
                           004315  1957 _AX5043_LPOSCREF0	=	0x4315
                           004314  1958 G$AX5043_LPOSCREF1$0$0 == 0x4314
                           004314  1959 _AX5043_LPOSCREF1	=	0x4314
                           004311  1960 G$AX5043_LPOSCSTATUS$0$0 == 0x4311
                           004311  1961 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1962 G$AX5043_MATCH0LEN$0$0 == 0x4214
                           004214  1963 _AX5043_MATCH0LEN	=	0x4214
                           004216  1964 G$AX5043_MATCH0MAX$0$0 == 0x4216
                           004216  1965 _AX5043_MATCH0MAX	=	0x4216
                           004215  1966 G$AX5043_MATCH0MIN$0$0 == 0x4215
                           004215  1967 _AX5043_MATCH0MIN	=	0x4215
                           004213  1968 G$AX5043_MATCH0PAT0$0$0 == 0x4213
                           004213  1969 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1970 G$AX5043_MATCH0PAT1$0$0 == 0x4212
                           004212  1971 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1972 G$AX5043_MATCH0PAT2$0$0 == 0x4211
                           004211  1973 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1974 G$AX5043_MATCH0PAT3$0$0 == 0x4210
                           004210  1975 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1976 G$AX5043_MATCH1LEN$0$0 == 0x421c
                           00421C  1977 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1978 G$AX5043_MATCH1MAX$0$0 == 0x421e
                           00421E  1979 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1980 G$AX5043_MATCH1MIN$0$0 == 0x421d
                           00421D  1981 _AX5043_MATCH1MIN	=	0x421d
                           004219  1982 G$AX5043_MATCH1PAT0$0$0 == 0x4219
                           004219  1983 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1984 G$AX5043_MATCH1PAT1$0$0 == 0x4218
                           004218  1985 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1986 G$AX5043_MAXDROFFSET0$0$0 == 0x4108
                           004108  1987 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1988 G$AX5043_MAXDROFFSET1$0$0 == 0x4107
                           004107  1989 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1990 G$AX5043_MAXDROFFSET2$0$0 == 0x4106
                           004106  1991 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1992 G$AX5043_MAXRFOFFSET0$0$0 == 0x410b
                           00410B  1993 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1994 G$AX5043_MAXRFOFFSET1$0$0 == 0x410a
                           00410A  1995 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1996 G$AX5043_MAXRFOFFSET2$0$0 == 0x4109
                           004109  1997 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1998 G$AX5043_MODCFGA$0$0 == 0x4164
                           004164  1999 _AX5043_MODCFGA	=	0x4164
                           004160  2000 G$AX5043_MODCFGF$0$0 == 0x4160
                           004160  2001 _AX5043_MODCFGF	=	0x4160
                           004F5F  2002 G$AX5043_MODCFGP$0$0 == 0x4f5f
                           004F5F  2003 _AX5043_MODCFGP	=	0x4f5f
                           004010  2004 G$AX5043_MODULATION$0$0 == 0x4010
                           004010  2005 _AX5043_MODULATION	=	0x4010
                           004025  2006 G$AX5043_PINFUNCANTSEL$0$0 == 0x4025
                           004025  2007 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  2008 G$AX5043_PINFUNCDATA$0$0 == 0x4023
                           004023  2009 _AX5043_PINFUNCDATA	=	0x4023
                           004022  2010 G$AX5043_PINFUNCDCLK$0$0 == 0x4022
                           004022  2011 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  2012 G$AX5043_PINFUNCIRQ$0$0 == 0x4024
                           004024  2013 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  2014 G$AX5043_PINFUNCPWRAMP$0$0 == 0x4026
                           004026  2015 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  2016 G$AX5043_PINFUNCSYSCLK$0$0 == 0x4021
                           004021  2017 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  2018 G$AX5043_PINSTATE$0$0 == 0x4020
                           004020  2019 _AX5043_PINSTATE	=	0x4020
                           004233  2020 G$AX5043_PKTACCEPTFLAGS$0$0 == 0x4233
                           004233  2021 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  2022 G$AX5043_PKTCHUNKSIZE$0$0 == 0x4230
                           004230  2023 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  2024 G$AX5043_PKTMISCFLAGS$0$0 == 0x4231
                           004231  2025 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  2026 G$AX5043_PKTSTOREFLAGS$0$0 == 0x4232
                           004232  2027 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  2028 G$AX5043_PLLCPI$0$0 == 0x4031
                           004031  2029 _AX5043_PLLCPI	=	0x4031
                           004039  2030 G$AX5043_PLLCPIBOOST$0$0 == 0x4039
                           004039  2031 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  2032 G$AX5043_PLLLOCKDET$0$0 == 0x4182
                           004182  2033 _AX5043_PLLLOCKDET	=	0x4182
                           004030  2034 G$AX5043_PLLLOOP$0$0 == 0x4030
                           004030  2035 _AX5043_PLLLOOP	=	0x4030
                           004038  2036 G$AX5043_PLLLOOPBOOST$0$0 == 0x4038
                           004038  2037 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  2038 G$AX5043_PLLRANGINGA$0$0 == 0x4033
                           004033  2039 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  2040 G$AX5043_PLLRANGINGB$0$0 == 0x403b
                           00403B  2041 _AX5043_PLLRANGINGB	=	0x403b
                           004183  2042 G$AX5043_PLLRNGCLK$0$0 == 0x4183
                           004183  2043 _AX5043_PLLRNGCLK	=	0x4183
                           004032  2044 G$AX5043_PLLVCODIV$0$0 == 0x4032
                           004032  2045 _AX5043_PLLVCODIV	=	0x4032
                           004180  2046 G$AX5043_PLLVCOI$0$0 == 0x4180
                           004180  2047 _AX5043_PLLVCOI	=	0x4180
                           004181  2048 G$AX5043_PLLVCOIR$0$0 == 0x4181
                           004181  2049 _AX5043_PLLVCOIR	=	0x4181
                           004F08  2050 G$AX5043_POWCTRL1$0$0 == 0x4f08
                           004F08  2051 _AX5043_POWCTRL1	=	0x4f08
                           004005  2052 G$AX5043_POWIRQMASK$0$0 == 0x4005
                           004005  2053 _AX5043_POWIRQMASK	=	0x4005
                           004003  2054 G$AX5043_POWSTAT$0$0 == 0x4003
                           004003  2055 _AX5043_POWSTAT	=	0x4003
                           004004  2056 G$AX5043_POWSTICKYSTAT$0$0 == 0x4004
                           004004  2057 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  2058 G$AX5043_PWRAMP$0$0 == 0x4027
                           004027  2059 _AX5043_PWRAMP	=	0x4027
                           004002  2060 G$AX5043_PWRMODE$0$0 == 0x4002
                           004002  2061 _AX5043_PWRMODE	=	0x4002
                           004009  2062 G$AX5043_RADIOEVENTMASK0$0$0 == 0x4009
                           004009  2063 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  2064 G$AX5043_RADIOEVENTMASK1$0$0 == 0x4008
                           004008  2065 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  2066 G$AX5043_RADIOEVENTREQ0$0$0 == 0x400f
                           00400F  2067 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  2068 G$AX5043_RADIOEVENTREQ1$0$0 == 0x400e
                           00400E  2069 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  2070 G$AX5043_RADIOSTATE$0$0 == 0x401c
                           00401C  2071 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  2072 G$AX5043_REF$0$0 == 0x4f0d
                           004F0D  2073 _AX5043_REF	=	0x4f0d
                           004040  2074 G$AX5043_RSSI$0$0 == 0x4040
                           004040  2075 _AX5043_RSSI	=	0x4040
                           00422D  2076 G$AX5043_RSSIABSTHR$0$0 == 0x422d
                           00422D  2077 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  2078 G$AX5043_RSSIREFERENCE$0$0 == 0x422c
                           00422C  2079 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  2080 G$AX5043_RXDATARATE0$0$0 == 0x4105
                           004105  2081 _AX5043_RXDATARATE0	=	0x4105
                           004104  2082 G$AX5043_RXDATARATE1$0$0 == 0x4104
                           004104  2083 _AX5043_RXDATARATE1	=	0x4104
                           004103  2084 G$AX5043_RXDATARATE2$0$0 == 0x4103
                           004103  2085 _AX5043_RXDATARATE2	=	0x4103
                           004001  2086 G$AX5043_SCRATCH$0$0 == 0x4001
                           004001  2087 _AX5043_SCRATCH	=	0x4001
                           004000  2088 G$AX5043_SILICONREVISION$0$0 == 0x4000
                           004000  2089 _AX5043_SILICONREVISION	=	0x4000
                           00405B  2090 G$AX5043_TIMER0$0$0 == 0x405b
                           00405B  2091 _AX5043_TIMER0	=	0x405b
                           00405A  2092 G$AX5043_TIMER1$0$0 == 0x405a
                           00405A  2093 _AX5043_TIMER1	=	0x405a
                           004059  2094 G$AX5043_TIMER2$0$0 == 0x4059
                           004059  2095 _AX5043_TIMER2	=	0x4059
                           004227  2096 G$AX5043_TMGRXAGC$0$0 == 0x4227
                           004227  2097 _AX5043_TMGRXAGC	=	0x4227
                           004223  2098 G$AX5043_TMGRXBOOST$0$0 == 0x4223
                           004223  2099 _AX5043_TMGRXBOOST	=	0x4223
                           004226  2100 G$AX5043_TMGRXCOARSEAGC$0$0 == 0x4226
                           004226  2101 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  2102 G$AX5043_TMGRXOFFSACQ$0$0 == 0x4225
                           004225  2103 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  2104 G$AX5043_TMGRXPREAMBLE1$0$0 == 0x4229
                           004229  2105 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  2106 G$AX5043_TMGRXPREAMBLE2$0$0 == 0x422a
                           00422A  2107 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  2108 G$AX5043_TMGRXPREAMBLE3$0$0 == 0x422b
                           00422B  2109 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  2110 G$AX5043_TMGRXRSSI$0$0 == 0x4228
                           004228  2111 _AX5043_TMGRXRSSI	=	0x4228
                           004224  2112 G$AX5043_TMGRXSETTLE$0$0 == 0x4224
                           004224  2113 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  2114 G$AX5043_TMGTXBOOST$0$0 == 0x4220
                           004220  2115 _AX5043_TMGTXBOOST	=	0x4220
                           004221  2116 G$AX5043_TMGTXSETTLE$0$0 == 0x4221
                           004221  2117 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  2118 G$AX5043_TRKAFSKDEMOD0$0$0 == 0x4055
                           004055  2119 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  2120 G$AX5043_TRKAFSKDEMOD1$0$0 == 0x4054
                           004054  2121 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  2122 G$AX5043_TRKAMPLITUDE0$0$0 == 0x4049
                           004049  2123 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  2124 G$AX5043_TRKAMPLITUDE1$0$0 == 0x4048
                           004048  2125 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  2126 G$AX5043_TRKDATARATE0$0$0 == 0x4047
                           004047  2127 _AX5043_TRKDATARATE0	=	0x4047
                           004046  2128 G$AX5043_TRKDATARATE1$0$0 == 0x4046
                           004046  2129 _AX5043_TRKDATARATE1	=	0x4046
                           004045  2130 G$AX5043_TRKDATARATE2$0$0 == 0x4045
                           004045  2131 _AX5043_TRKDATARATE2	=	0x4045
                           004051  2132 G$AX5043_TRKFREQ0$0$0 == 0x4051
                           004051  2133 _AX5043_TRKFREQ0	=	0x4051
                           004050  2134 G$AX5043_TRKFREQ1$0$0 == 0x4050
                           004050  2135 _AX5043_TRKFREQ1	=	0x4050
                           004053  2136 G$AX5043_TRKFSKDEMOD0$0$0 == 0x4053
                           004053  2137 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  2138 G$AX5043_TRKFSKDEMOD1$0$0 == 0x4052
                           004052  2139 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  2140 G$AX5043_TRKPHASE0$0$0 == 0x404b
                           00404B  2141 _AX5043_TRKPHASE0	=	0x404b
                           00404A  2142 G$AX5043_TRKPHASE1$0$0 == 0x404a
                           00404A  2143 _AX5043_TRKPHASE1	=	0x404a
                           00404F  2144 G$AX5043_TRKRFFREQ0$0$0 == 0x404f
                           00404F  2145 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  2146 G$AX5043_TRKRFFREQ1$0$0 == 0x404e
                           00404E  2147 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  2148 G$AX5043_TRKRFFREQ2$0$0 == 0x404d
                           00404D  2149 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  2150 G$AX5043_TXPWRCOEFFA0$0$0 == 0x4169
                           004169  2151 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  2152 G$AX5043_TXPWRCOEFFA1$0$0 == 0x4168
                           004168  2153 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  2154 G$AX5043_TXPWRCOEFFB0$0$0 == 0x416b
                           00416B  2155 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  2156 G$AX5043_TXPWRCOEFFB1$0$0 == 0x416a
                           00416A  2157 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  2158 G$AX5043_TXPWRCOEFFC0$0$0 == 0x416d
                           00416D  2159 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  2160 G$AX5043_TXPWRCOEFFC1$0$0 == 0x416c
                           00416C  2161 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  2162 G$AX5043_TXPWRCOEFFD0$0$0 == 0x416f
                           00416F  2163 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  2164 G$AX5043_TXPWRCOEFFD1$0$0 == 0x416e
                           00416E  2165 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  2166 G$AX5043_TXPWRCOEFFE0$0$0 == 0x4171
                           004171  2167 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  2168 G$AX5043_TXPWRCOEFFE1$0$0 == 0x4170
                           004170  2169 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  2170 G$AX5043_TXRATE0$0$0 == 0x4167
                           004167  2171 _AX5043_TXRATE0	=	0x4167
                           004166  2172 G$AX5043_TXRATE1$0$0 == 0x4166
                           004166  2173 _AX5043_TXRATE1	=	0x4166
                           004165  2174 G$AX5043_TXRATE2$0$0 == 0x4165
                           004165  2175 _AX5043_TXRATE2	=	0x4165
                           00406B  2176 G$AX5043_WAKEUP0$0$0 == 0x406b
                           00406B  2177 _AX5043_WAKEUP0	=	0x406b
                           00406A  2178 G$AX5043_WAKEUP1$0$0 == 0x406a
                           00406A  2179 _AX5043_WAKEUP1	=	0x406a
                           00406D  2180 G$AX5043_WAKEUPFREQ0$0$0 == 0x406d
                           00406D  2181 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  2182 G$AX5043_WAKEUPFREQ1$0$0 == 0x406c
                           00406C  2183 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  2184 G$AX5043_WAKEUPTIMER0$0$0 == 0x4069
                           004069  2185 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  2186 G$AX5043_WAKEUPTIMER1$0$0 == 0x4068
                           004068  2187 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  2188 G$AX5043_WAKEUPXOEARLY$0$0 == 0x406e
                           00406E  2189 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  2190 G$AX5043_XTALAMPL$0$0 == 0x4f11
                           004F11  2191 _AX5043_XTALAMPL	=	0x4f11
                           004184  2192 G$AX5043_XTALCAP$0$0 == 0x4184
                           004184  2193 _AX5043_XTALCAP	=	0x4184
                           004F10  2194 G$AX5043_XTALOSC$0$0 == 0x4f10
                           004F10  2195 _AX5043_XTALOSC	=	0x4f10
                           00401D  2196 G$AX5043_XTALSTATUS$0$0 == 0x401d
                           00401D  2197 _AX5043_XTALSTATUS	=	0x401d
                           004F00  2198 G$AX5043_0xF00$0$0 == 0x4f00
                           004F00  2199 _AX5043_0xF00	=	0x4f00
                           004F0C  2200 G$AX5043_0xF0C$0$0 == 0x4f0c
                           004F0C  2201 _AX5043_0xF0C	=	0x4f0c
                           004F18  2202 G$AX5043_0xF18$0$0 == 0x4f18
                           004F18  2203 _AX5043_0xF18	=	0x4f18
                           004F1C  2204 G$AX5043_0xF1C$0$0 == 0x4f1c
                           004F1C  2205 _AX5043_0xF1C	=	0x4f1c
                           004F21  2206 G$AX5043_0xF21$0$0 == 0x4f21
                           004F21  2207 _AX5043_0xF21	=	0x4f21
                           004F22  2208 G$AX5043_0xF22$0$0 == 0x4f22
                           004F22  2209 _AX5043_0xF22	=	0x4f22
                           004F23  2210 G$AX5043_0xF23$0$0 == 0x4f23
                           004F23  2211 _AX5043_0xF23	=	0x4f23
                           004F26  2212 G$AX5043_0xF26$0$0 == 0x4f26
                           004F26  2213 _AX5043_0xF26	=	0x4f26
                           004F30  2214 G$AX5043_0xF30$0$0 == 0x4f30
                           004F30  2215 _AX5043_0xF30	=	0x4f30
                           004F31  2216 G$AX5043_0xF31$0$0 == 0x4f31
                           004F31  2217 _AX5043_0xF31	=	0x4f31
                           004F32  2218 G$AX5043_0xF32$0$0 == 0x4f32
                           004F32  2219 _AX5043_0xF32	=	0x4f32
                           004F33  2220 G$AX5043_0xF33$0$0 == 0x4f33
                           004F33  2221 _AX5043_0xF33	=	0x4f33
                           004F34  2222 G$AX5043_0xF34$0$0 == 0x4f34
                           004F34  2223 _AX5043_0xF34	=	0x4f34
                           004F35  2224 G$AX5043_0xF35$0$0 == 0x4f35
                           004F35  2225 _AX5043_0xF35	=	0x4f35
                           004F44  2226 G$AX5043_0xF44$0$0 == 0x4f44
                           004F44  2227 _AX5043_0xF44	=	0x4f44
                           004122  2228 G$AX5043_AGCAHYST0$0$0 == 0x4122
                           004122  2229 _AX5043_AGCAHYST0	=	0x4122
                           004132  2230 G$AX5043_AGCAHYST1$0$0 == 0x4132
                           004132  2231 _AX5043_AGCAHYST1	=	0x4132
                           004142  2232 G$AX5043_AGCAHYST2$0$0 == 0x4142
                           004142  2233 _AX5043_AGCAHYST2	=	0x4142
                           004152  2234 G$AX5043_AGCAHYST3$0$0 == 0x4152
                           004152  2235 _AX5043_AGCAHYST3	=	0x4152
                           004120  2236 G$AX5043_AGCGAIN0$0$0 == 0x4120
                           004120  2237 _AX5043_AGCGAIN0	=	0x4120
                           004130  2238 G$AX5043_AGCGAIN1$0$0 == 0x4130
                           004130  2239 _AX5043_AGCGAIN1	=	0x4130
                           004140  2240 G$AX5043_AGCGAIN2$0$0 == 0x4140
                           004140  2241 _AX5043_AGCGAIN2	=	0x4140
                           004150  2242 G$AX5043_AGCGAIN3$0$0 == 0x4150
                           004150  2243 _AX5043_AGCGAIN3	=	0x4150
                           004123  2244 G$AX5043_AGCMINMAX0$0$0 == 0x4123
                           004123  2245 _AX5043_AGCMINMAX0	=	0x4123
                           004133  2246 G$AX5043_AGCMINMAX1$0$0 == 0x4133
                           004133  2247 _AX5043_AGCMINMAX1	=	0x4133
                           004143  2248 G$AX5043_AGCMINMAX2$0$0 == 0x4143
                           004143  2249 _AX5043_AGCMINMAX2	=	0x4143
                           004153  2250 G$AX5043_AGCMINMAX3$0$0 == 0x4153
                           004153  2251 _AX5043_AGCMINMAX3	=	0x4153
                           004121  2252 G$AX5043_AGCTARGET0$0$0 == 0x4121
                           004121  2253 _AX5043_AGCTARGET0	=	0x4121
                           004131  2254 G$AX5043_AGCTARGET1$0$0 == 0x4131
                           004131  2255 _AX5043_AGCTARGET1	=	0x4131
                           004141  2256 G$AX5043_AGCTARGET2$0$0 == 0x4141
                           004141  2257 _AX5043_AGCTARGET2	=	0x4141
                           004151  2258 G$AX5043_AGCTARGET3$0$0 == 0x4151
                           004151  2259 _AX5043_AGCTARGET3	=	0x4151
                           00412B  2260 G$AX5043_AMPLITUDEGAIN0$0$0 == 0x412b
                           00412B  2261 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  2262 G$AX5043_AMPLITUDEGAIN1$0$0 == 0x413b
                           00413B  2263 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  2264 G$AX5043_AMPLITUDEGAIN2$0$0 == 0x414b
                           00414B  2265 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  2266 G$AX5043_AMPLITUDEGAIN3$0$0 == 0x415b
                           00415B  2267 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  2268 G$AX5043_BBOFFSRES0$0$0 == 0x412f
                           00412F  2269 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  2270 G$AX5043_BBOFFSRES1$0$0 == 0x413f
                           00413F  2271 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  2272 G$AX5043_BBOFFSRES2$0$0 == 0x414f
                           00414F  2273 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  2274 G$AX5043_BBOFFSRES3$0$0 == 0x415f
                           00415F  2275 _AX5043_BBOFFSRES3	=	0x415f
                           004125  2276 G$AX5043_DRGAIN0$0$0 == 0x4125
                           004125  2277 _AX5043_DRGAIN0	=	0x4125
                           004135  2278 G$AX5043_DRGAIN1$0$0 == 0x4135
                           004135  2279 _AX5043_DRGAIN1	=	0x4135
                           004145  2280 G$AX5043_DRGAIN2$0$0 == 0x4145
                           004145  2281 _AX5043_DRGAIN2	=	0x4145
                           004155  2282 G$AX5043_DRGAIN3$0$0 == 0x4155
                           004155  2283 _AX5043_DRGAIN3	=	0x4155
                           00412E  2284 G$AX5043_FOURFSK0$0$0 == 0x412e
                           00412E  2285 _AX5043_FOURFSK0	=	0x412e
                           00413E  2286 G$AX5043_FOURFSK1$0$0 == 0x413e
                           00413E  2287 _AX5043_FOURFSK1	=	0x413e
                           00414E  2288 G$AX5043_FOURFSK2$0$0 == 0x414e
                           00414E  2289 _AX5043_FOURFSK2	=	0x414e
                           00415E  2290 G$AX5043_FOURFSK3$0$0 == 0x415e
                           00415E  2291 _AX5043_FOURFSK3	=	0x415e
                           00412D  2292 G$AX5043_FREQDEV00$0$0 == 0x412d
                           00412D  2293 _AX5043_FREQDEV00	=	0x412d
                           00413D  2294 G$AX5043_FREQDEV01$0$0 == 0x413d
                           00413D  2295 _AX5043_FREQDEV01	=	0x413d
                           00414D  2296 G$AX5043_FREQDEV02$0$0 == 0x414d
                           00414D  2297 _AX5043_FREQDEV02	=	0x414d
                           00415D  2298 G$AX5043_FREQDEV03$0$0 == 0x415d
                           00415D  2299 _AX5043_FREQDEV03	=	0x415d
                           00412C  2300 G$AX5043_FREQDEV10$0$0 == 0x412c
                           00412C  2301 _AX5043_FREQDEV10	=	0x412c
                           00413C  2302 G$AX5043_FREQDEV11$0$0 == 0x413c
                           00413C  2303 _AX5043_FREQDEV11	=	0x413c
                           00414C  2304 G$AX5043_FREQDEV12$0$0 == 0x414c
                           00414C  2305 _AX5043_FREQDEV12	=	0x414c
                           00415C  2306 G$AX5043_FREQDEV13$0$0 == 0x415c
                           00415C  2307 _AX5043_FREQDEV13	=	0x415c
                           004127  2308 G$AX5043_FREQUENCYGAINA0$0$0 == 0x4127
                           004127  2309 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  2310 G$AX5043_FREQUENCYGAINA1$0$0 == 0x4137
                           004137  2311 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  2312 G$AX5043_FREQUENCYGAINA2$0$0 == 0x4147
                           004147  2313 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  2314 G$AX5043_FREQUENCYGAINA3$0$0 == 0x4157
                           004157  2315 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  2316 G$AX5043_FREQUENCYGAINB0$0$0 == 0x4128
                           004128  2317 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  2318 G$AX5043_FREQUENCYGAINB1$0$0 == 0x4138
                           004138  2319 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  2320 G$AX5043_FREQUENCYGAINB2$0$0 == 0x4148
                           004148  2321 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  2322 G$AX5043_FREQUENCYGAINB3$0$0 == 0x4158
                           004158  2323 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  2324 G$AX5043_FREQUENCYGAINC0$0$0 == 0x4129
                           004129  2325 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  2326 G$AX5043_FREQUENCYGAINC1$0$0 == 0x4139
                           004139  2327 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  2328 G$AX5043_FREQUENCYGAINC2$0$0 == 0x4149
                           004149  2329 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  2330 G$AX5043_FREQUENCYGAINC3$0$0 == 0x4159
                           004159  2331 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  2332 G$AX5043_FREQUENCYGAIND0$0$0 == 0x412a
                           00412A  2333 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  2334 G$AX5043_FREQUENCYGAIND1$0$0 == 0x413a
                           00413A  2335 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  2336 G$AX5043_FREQUENCYGAIND2$0$0 == 0x414a
                           00414A  2337 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  2338 G$AX5043_FREQUENCYGAIND3$0$0 == 0x415a
                           00415A  2339 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  2340 G$AX5043_FREQUENCYLEAK$0$0 == 0x4116
                           004116  2341 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  2342 G$AX5043_PHASEGAIN0$0$0 == 0x4126
                           004126  2343 _AX5043_PHASEGAIN0	=	0x4126
                           004136  2344 G$AX5043_PHASEGAIN1$0$0 == 0x4136
                           004136  2345 _AX5043_PHASEGAIN1	=	0x4136
                           004146  2346 G$AX5043_PHASEGAIN2$0$0 == 0x4146
                           004146  2347 _AX5043_PHASEGAIN2	=	0x4146
                           004156  2348 G$AX5043_PHASEGAIN3$0$0 == 0x4156
                           004156  2349 _AX5043_PHASEGAIN3	=	0x4156
                           004207  2350 G$AX5043_PKTADDR0$0$0 == 0x4207
                           004207  2351 _AX5043_PKTADDR0	=	0x4207
                           004206  2352 G$AX5043_PKTADDR1$0$0 == 0x4206
                           004206  2353 _AX5043_PKTADDR1	=	0x4206
                           004205  2354 G$AX5043_PKTADDR2$0$0 == 0x4205
                           004205  2355 _AX5043_PKTADDR2	=	0x4205
                           004204  2356 G$AX5043_PKTADDR3$0$0 == 0x4204
                           004204  2357 _AX5043_PKTADDR3	=	0x4204
                           004200  2358 G$AX5043_PKTADDRCFG$0$0 == 0x4200
                           004200  2359 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  2360 G$AX5043_PKTADDRMASK0$0$0 == 0x420b
                           00420B  2361 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  2362 G$AX5043_PKTADDRMASK1$0$0 == 0x420a
                           00420A  2363 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  2364 G$AX5043_PKTADDRMASK2$0$0 == 0x4209
                           004209  2365 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  2366 G$AX5043_PKTADDRMASK3$0$0 == 0x4208
                           004208  2367 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  2368 G$AX5043_PKTLENCFG$0$0 == 0x4201
                           004201  2369 _AX5043_PKTLENCFG	=	0x4201
                           004202  2370 G$AX5043_PKTLENOFFSET$0$0 == 0x4202
                           004202  2371 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  2372 G$AX5043_PKTMAXLEN$0$0 == 0x4203
                           004203  2373 _AX5043_PKTMAXLEN	=	0x4203
                           004118  2374 G$AX5043_RXPARAMCURSET$0$0 == 0x4118
                           004118  2375 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  2376 G$AX5043_RXPARAMSETS$0$0 == 0x4117
                           004117  2377 _AX5043_RXPARAMSETS	=	0x4117
                           004124  2378 G$AX5043_TIMEGAIN0$0$0 == 0x4124
                           004124  2379 _AX5043_TIMEGAIN0	=	0x4124
                           004134  2380 G$AX5043_TIMEGAIN1$0$0 == 0x4134
                           004134  2381 _AX5043_TIMEGAIN1	=	0x4134
                           004144  2382 G$AX5043_TIMEGAIN2$0$0 == 0x4144
                           004144  2383 _AX5043_TIMEGAIN2	=	0x4144
                           004154  2384 G$AX5043_TIMEGAIN3$0$0 == 0x4154
                           004154  2385 _AX5043_TIMEGAIN3	=	0x4154
                           005114  2386 G$AX5043_AFSKCTRLNB$0$0 == 0x5114
                           005114  2387 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  2388 G$AX5043_AFSKMARK0NB$0$0 == 0x5113
                           005113  2389 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  2390 G$AX5043_AFSKMARK1NB$0$0 == 0x5112
                           005112  2391 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  2392 G$AX5043_AFSKSPACE0NB$0$0 == 0x5111
                           005111  2393 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  2394 G$AX5043_AFSKSPACE1NB$0$0 == 0x5110
                           005110  2395 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  2396 G$AX5043_AGCCOUNTERNB$0$0 == 0x5043
                           005043  2397 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  2398 G$AX5043_AMPLFILTERNB$0$0 == 0x5115
                           005115  2399 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  2400 G$AX5043_BBOFFSCAPNB$0$0 == 0x5189
                           005189  2401 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  2402 G$AX5043_BBTUNENB$0$0 == 0x5188
                           005188  2403 _AX5043_BBTUNENB	=	0x5188
                           005041  2404 G$AX5043_BGNDRSSINB$0$0 == 0x5041
                           005041  2405 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  2406 G$AX5043_BGNDRSSIGAINNB$0$0 == 0x522e
                           00522E  2407 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  2408 G$AX5043_BGNDRSSITHRNB$0$0 == 0x522f
                           00522F  2409 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2410 G$AX5043_CRCINIT0NB$0$0 == 0x5017
                           005017  2411 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2412 G$AX5043_CRCINIT1NB$0$0 == 0x5016
                           005016  2413 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2414 G$AX5043_CRCINIT2NB$0$0 == 0x5015
                           005015  2415 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2416 G$AX5043_CRCINIT3NB$0$0 == 0x5014
                           005014  2417 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2418 G$AX5043_DACCONFIGNB$0$0 == 0x5332
                           005332  2419 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2420 G$AX5043_DACVALUE0NB$0$0 == 0x5331
                           005331  2421 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2422 G$AX5043_DACVALUE1NB$0$0 == 0x5330
                           005330  2423 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2424 G$AX5043_DECIMATIONNB$0$0 == 0x5102
                           005102  2425 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2426 G$AX5043_DIVERSITYNB$0$0 == 0x5042
                           005042  2427 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2428 G$AX5043_ENCODINGNB$0$0 == 0x5011
                           005011  2429 _AX5043_ENCODINGNB	=	0x5011
                           005018  2430 G$AX5043_FECNB$0$0 == 0x5018
                           005018  2431 _AX5043_FECNB	=	0x5018
                           00501A  2432 G$AX5043_FECSTATUSNB$0$0 == 0x501a
                           00501A  2433 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2434 G$AX5043_FECSYNCNB$0$0 == 0x5019
                           005019  2435 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2436 G$AX5043_FIFOCOUNT0NB$0$0 == 0x502b
                           00502B  2437 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2438 G$AX5043_FIFOCOUNT1NB$0$0 == 0x502a
                           00502A  2439 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2440 G$AX5043_FIFODATANB$0$0 == 0x5029
                           005029  2441 _AX5043_FIFODATANB	=	0x5029
                           00502D  2442 G$AX5043_FIFOFREE0NB$0$0 == 0x502d
                           00502D  2443 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2444 G$AX5043_FIFOFREE1NB$0$0 == 0x502c
                           00502C  2445 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2446 G$AX5043_FIFOSTATNB$0$0 == 0x5028
                           005028  2447 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2448 G$AX5043_FIFOTHRESH0NB$0$0 == 0x502f
                           00502F  2449 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2450 G$AX5043_FIFOTHRESH1NB$0$0 == 0x502e
                           00502E  2451 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2452 G$AX5043_FRAMINGNB$0$0 == 0x5012
                           005012  2453 _AX5043_FRAMINGNB	=	0x5012
                           005037  2454 G$AX5043_FREQA0NB$0$0 == 0x5037
                           005037  2455 _AX5043_FREQA0NB	=	0x5037
                           005036  2456 G$AX5043_FREQA1NB$0$0 == 0x5036
                           005036  2457 _AX5043_FREQA1NB	=	0x5036
                           005035  2458 G$AX5043_FREQA2NB$0$0 == 0x5035
                           005035  2459 _AX5043_FREQA2NB	=	0x5035
                           005034  2460 G$AX5043_FREQA3NB$0$0 == 0x5034
                           005034  2461 _AX5043_FREQA3NB	=	0x5034
                           00503F  2462 G$AX5043_FREQB0NB$0$0 == 0x503f
                           00503F  2463 _AX5043_FREQB0NB	=	0x503f
                           00503E  2464 G$AX5043_FREQB1NB$0$0 == 0x503e
                           00503E  2465 _AX5043_FREQB1NB	=	0x503e
                           00503D  2466 G$AX5043_FREQB2NB$0$0 == 0x503d
                           00503D  2467 _AX5043_FREQB2NB	=	0x503d
                           00503C  2468 G$AX5043_FREQB3NB$0$0 == 0x503c
                           00503C  2469 _AX5043_FREQB3NB	=	0x503c
                           005163  2470 G$AX5043_FSKDEV0NB$0$0 == 0x5163
                           005163  2471 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2472 G$AX5043_FSKDEV1NB$0$0 == 0x5162
                           005162  2473 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2474 G$AX5043_FSKDEV2NB$0$0 == 0x5161
                           005161  2475 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2476 G$AX5043_FSKDMAX0NB$0$0 == 0x510d
                           00510D  2477 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2478 G$AX5043_FSKDMAX1NB$0$0 == 0x510c
                           00510C  2479 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2480 G$AX5043_FSKDMIN0NB$0$0 == 0x510f
                           00510F  2481 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2482 G$AX5043_FSKDMIN1NB$0$0 == 0x510e
                           00510E  2483 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2484 G$AX5043_GPADC13VALUE0NB$0$0 == 0x5309
                           005309  2485 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2486 G$AX5043_GPADC13VALUE1NB$0$0 == 0x5308
                           005308  2487 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2488 G$AX5043_GPADCCTRLNB$0$0 == 0x5300
                           005300  2489 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2490 G$AX5043_GPADCPERIODNB$0$0 == 0x5301
                           005301  2491 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2492 G$AX5043_IFFREQ0NB$0$0 == 0x5101
                           005101  2493 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2494 G$AX5043_IFFREQ1NB$0$0 == 0x5100
                           005100  2495 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2496 G$AX5043_IRQINVERSION0NB$0$0 == 0x500b
                           00500B  2497 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2498 G$AX5043_IRQINVERSION1NB$0$0 == 0x500a
                           00500A  2499 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2500 G$AX5043_IRQMASK0NB$0$0 == 0x5007
                           005007  2501 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2502 G$AX5043_IRQMASK1NB$0$0 == 0x5006
                           005006  2503 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2504 G$AX5043_IRQREQUEST0NB$0$0 == 0x500d
                           00500D  2505 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2506 G$AX5043_IRQREQUEST1NB$0$0 == 0x500c
                           00500C  2507 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2508 G$AX5043_LPOSCCONFIGNB$0$0 == 0x5310
                           005310  2509 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2510 G$AX5043_LPOSCFREQ0NB$0$0 == 0x5317
                           005317  2511 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2512 G$AX5043_LPOSCFREQ1NB$0$0 == 0x5316
                           005316  2513 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2514 G$AX5043_LPOSCKFILT0NB$0$0 == 0x5313
                           005313  2515 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2516 G$AX5043_LPOSCKFILT1NB$0$0 == 0x5312
                           005312  2517 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2518 G$AX5043_LPOSCPER0NB$0$0 == 0x5319
                           005319  2519 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2520 G$AX5043_LPOSCPER1NB$0$0 == 0x5318
                           005318  2521 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2522 G$AX5043_LPOSCREF0NB$0$0 == 0x5315
                           005315  2523 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2524 G$AX5043_LPOSCREF1NB$0$0 == 0x5314
                           005314  2525 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2526 G$AX5043_LPOSCSTATUSNB$0$0 == 0x5311
                           005311  2527 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2528 G$AX5043_MATCH0LENNB$0$0 == 0x5214
                           005214  2529 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2530 G$AX5043_MATCH0MAXNB$0$0 == 0x5216
                           005216  2531 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2532 G$AX5043_MATCH0MINNB$0$0 == 0x5215
                           005215  2533 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2534 G$AX5043_MATCH0PAT0NB$0$0 == 0x5213
                           005213  2535 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2536 G$AX5043_MATCH0PAT1NB$0$0 == 0x5212
                           005212  2537 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2538 G$AX5043_MATCH0PAT2NB$0$0 == 0x5211
                           005211  2539 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2540 G$AX5043_MATCH0PAT3NB$0$0 == 0x5210
                           005210  2541 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2542 G$AX5043_MATCH1LENNB$0$0 == 0x521c
                           00521C  2543 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2544 G$AX5043_MATCH1MAXNB$0$0 == 0x521e
                           00521E  2545 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2546 G$AX5043_MATCH1MINNB$0$0 == 0x521d
                           00521D  2547 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2548 G$AX5043_MATCH1PAT0NB$0$0 == 0x5219
                           005219  2549 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2550 G$AX5043_MATCH1PAT1NB$0$0 == 0x5218
                           005218  2551 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2552 G$AX5043_MAXDROFFSET0NB$0$0 == 0x5108
                           005108  2553 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2554 G$AX5043_MAXDROFFSET1NB$0$0 == 0x5107
                           005107  2555 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2556 G$AX5043_MAXDROFFSET2NB$0$0 == 0x5106
                           005106  2557 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2558 G$AX5043_MAXRFOFFSET0NB$0$0 == 0x510b
                           00510B  2559 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2560 G$AX5043_MAXRFOFFSET1NB$0$0 == 0x510a
                           00510A  2561 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2562 G$AX5043_MAXRFOFFSET2NB$0$0 == 0x5109
                           005109  2563 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2564 G$AX5043_MODCFGANB$0$0 == 0x5164
                           005164  2565 _AX5043_MODCFGANB	=	0x5164
                           005160  2566 G$AX5043_MODCFGFNB$0$0 == 0x5160
                           005160  2567 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2568 G$AX5043_MODCFGPNB$0$0 == 0x5f5f
                           005F5F  2569 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2570 G$AX5043_MODULATIONNB$0$0 == 0x5010
                           005010  2571 _AX5043_MODULATIONNB	=	0x5010
                           005025  2572 G$AX5043_PINFUNCANTSELNB$0$0 == 0x5025
                           005025  2573 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2574 G$AX5043_PINFUNCDATANB$0$0 == 0x5023
                           005023  2575 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2576 G$AX5043_PINFUNCDCLKNB$0$0 == 0x5022
                           005022  2577 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2578 G$AX5043_PINFUNCIRQNB$0$0 == 0x5024
                           005024  2579 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2580 G$AX5043_PINFUNCPWRAMPNB$0$0 == 0x5026
                           005026  2581 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2582 G$AX5043_PINFUNCSYSCLKNB$0$0 == 0x5021
                           005021  2583 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2584 G$AX5043_PINSTATENB$0$0 == 0x5020
                           005020  2585 _AX5043_PINSTATENB	=	0x5020
                           005233  2586 G$AX5043_PKTACCEPTFLAGSNB$0$0 == 0x5233
                           005233  2587 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2588 G$AX5043_PKTCHUNKSIZENB$0$0 == 0x5230
                           005230  2589 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2590 G$AX5043_PKTMISCFLAGSNB$0$0 == 0x5231
                           005231  2591 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2592 G$AX5043_PKTSTOREFLAGSNB$0$0 == 0x5232
                           005232  2593 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2594 G$AX5043_PLLCPINB$0$0 == 0x5031
                           005031  2595 _AX5043_PLLCPINB	=	0x5031
                           005039  2596 G$AX5043_PLLCPIBOOSTNB$0$0 == 0x5039
                           005039  2597 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2598 G$AX5043_PLLLOCKDETNB$0$0 == 0x5182
                           005182  2599 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2600 G$AX5043_PLLLOOPNB$0$0 == 0x5030
                           005030  2601 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2602 G$AX5043_PLLLOOPBOOSTNB$0$0 == 0x5038
                           005038  2603 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2604 G$AX5043_PLLRANGINGANB$0$0 == 0x5033
                           005033  2605 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2606 G$AX5043_PLLRANGINGBNB$0$0 == 0x503b
                           00503B  2607 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2608 G$AX5043_PLLRNGCLKNB$0$0 == 0x5183
                           005183  2609 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2610 G$AX5043_PLLVCODIVNB$0$0 == 0x5032
                           005032  2611 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2612 G$AX5043_PLLVCOINB$0$0 == 0x5180
                           005180  2613 _AX5043_PLLVCOINB	=	0x5180
                           005181  2614 G$AX5043_PLLVCOIRNB$0$0 == 0x5181
                           005181  2615 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2616 G$AX5043_POWCTRL1NB$0$0 == 0x5f08
                           005F08  2617 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2618 G$AX5043_POWIRQMASKNB$0$0 == 0x5005
                           005005  2619 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2620 G$AX5043_POWSTATNB$0$0 == 0x5003
                           005003  2621 _AX5043_POWSTATNB	=	0x5003
                           005004  2622 G$AX5043_POWSTICKYSTATNB$0$0 == 0x5004
                           005004  2623 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2624 G$AX5043_PWRAMPNB$0$0 == 0x5027
                           005027  2625 _AX5043_PWRAMPNB	=	0x5027
                           005002  2626 G$AX5043_PWRMODENB$0$0 == 0x5002
                           005002  2627 _AX5043_PWRMODENB	=	0x5002
                           005009  2628 G$AX5043_RADIOEVENTMASK0NB$0$0 == 0x5009
                           005009  2629 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2630 G$AX5043_RADIOEVENTMASK1NB$0$0 == 0x5008
                           005008  2631 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2632 G$AX5043_RADIOEVENTREQ0NB$0$0 == 0x500f
                           00500F  2633 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2634 G$AX5043_RADIOEVENTREQ1NB$0$0 == 0x500e
                           00500E  2635 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2636 G$AX5043_RADIOSTATENB$0$0 == 0x501c
                           00501C  2637 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2638 G$AX5043_REFNB$0$0 == 0x5f0d
                           005F0D  2639 _AX5043_REFNB	=	0x5f0d
                           005040  2640 G$AX5043_RSSINB$0$0 == 0x5040
                           005040  2641 _AX5043_RSSINB	=	0x5040
                           00522D  2642 G$AX5043_RSSIABSTHRNB$0$0 == 0x522d
                           00522D  2643 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2644 G$AX5043_RSSIREFERENCENB$0$0 == 0x522c
                           00522C  2645 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2646 G$AX5043_RXDATARATE0NB$0$0 == 0x5105
                           005105  2647 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2648 G$AX5043_RXDATARATE1NB$0$0 == 0x5104
                           005104  2649 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2650 G$AX5043_RXDATARATE2NB$0$0 == 0x5103
                           005103  2651 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2652 G$AX5043_SCRATCHNB$0$0 == 0x5001
                           005001  2653 _AX5043_SCRATCHNB	=	0x5001
                           005000  2654 G$AX5043_SILICONREVISIONNB$0$0 == 0x5000
                           005000  2655 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2656 G$AX5043_TIMER0NB$0$0 == 0x505b
                           00505B  2657 _AX5043_TIMER0NB	=	0x505b
                           00505A  2658 G$AX5043_TIMER1NB$0$0 == 0x505a
                           00505A  2659 _AX5043_TIMER1NB	=	0x505a
                           005059  2660 G$AX5043_TIMER2NB$0$0 == 0x5059
                           005059  2661 _AX5043_TIMER2NB	=	0x5059
                           005227  2662 G$AX5043_TMGRXAGCNB$0$0 == 0x5227
                           005227  2663 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2664 G$AX5043_TMGRXBOOSTNB$0$0 == 0x5223
                           005223  2665 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2666 G$AX5043_TMGRXCOARSEAGCNB$0$0 == 0x5226
                           005226  2667 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2668 G$AX5043_TMGRXOFFSACQNB$0$0 == 0x5225
                           005225  2669 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2670 G$AX5043_TMGRXPREAMBLE1NB$0$0 == 0x5229
                           005229  2671 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2672 G$AX5043_TMGRXPREAMBLE2NB$0$0 == 0x522a
                           00522A  2673 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2674 G$AX5043_TMGRXPREAMBLE3NB$0$0 == 0x522b
                           00522B  2675 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2676 G$AX5043_TMGRXRSSINB$0$0 == 0x5228
                           005228  2677 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2678 G$AX5043_TMGRXSETTLENB$0$0 == 0x5224
                           005224  2679 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2680 G$AX5043_TMGTXBOOSTNB$0$0 == 0x5220
                           005220  2681 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2682 G$AX5043_TMGTXSETTLENB$0$0 == 0x5221
                           005221  2683 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2684 G$AX5043_TRKAFSKDEMOD0NB$0$0 == 0x5055
                           005055  2685 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2686 G$AX5043_TRKAFSKDEMOD1NB$0$0 == 0x5054
                           005054  2687 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2688 G$AX5043_TRKAMPLITUDE0NB$0$0 == 0x5049
                           005049  2689 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2690 G$AX5043_TRKAMPLITUDE1NB$0$0 == 0x5048
                           005048  2691 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2692 G$AX5043_TRKDATARATE0NB$0$0 == 0x5047
                           005047  2693 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2694 G$AX5043_TRKDATARATE1NB$0$0 == 0x5046
                           005046  2695 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2696 G$AX5043_TRKDATARATE2NB$0$0 == 0x5045
                           005045  2697 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2698 G$AX5043_TRKFREQ0NB$0$0 == 0x5051
                           005051  2699 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2700 G$AX5043_TRKFREQ1NB$0$0 == 0x5050
                           005050  2701 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2702 G$AX5043_TRKFSKDEMOD0NB$0$0 == 0x5053
                           005053  2703 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2704 G$AX5043_TRKFSKDEMOD1NB$0$0 == 0x5052
                           005052  2705 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2706 G$AX5043_TRKPHASE0NB$0$0 == 0x504b
                           00504B  2707 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2708 G$AX5043_TRKPHASE1NB$0$0 == 0x504a
                           00504A  2709 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2710 G$AX5043_TRKRFFREQ0NB$0$0 == 0x504f
                           00504F  2711 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2712 G$AX5043_TRKRFFREQ1NB$0$0 == 0x504e
                           00504E  2713 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2714 G$AX5043_TRKRFFREQ2NB$0$0 == 0x504d
                           00504D  2715 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2716 G$AX5043_TXPWRCOEFFA0NB$0$0 == 0x5169
                           005169  2717 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2718 G$AX5043_TXPWRCOEFFA1NB$0$0 == 0x5168
                           005168  2719 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2720 G$AX5043_TXPWRCOEFFB0NB$0$0 == 0x516b
                           00516B  2721 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2722 G$AX5043_TXPWRCOEFFB1NB$0$0 == 0x516a
                           00516A  2723 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2724 G$AX5043_TXPWRCOEFFC0NB$0$0 == 0x516d
                           00516D  2725 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2726 G$AX5043_TXPWRCOEFFC1NB$0$0 == 0x516c
                           00516C  2727 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2728 G$AX5043_TXPWRCOEFFD0NB$0$0 == 0x516f
                           00516F  2729 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2730 G$AX5043_TXPWRCOEFFD1NB$0$0 == 0x516e
                           00516E  2731 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2732 G$AX5043_TXPWRCOEFFE0NB$0$0 == 0x5171
                           005171  2733 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2734 G$AX5043_TXPWRCOEFFE1NB$0$0 == 0x5170
                           005170  2735 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2736 G$AX5043_TXRATE0NB$0$0 == 0x5167
                           005167  2737 _AX5043_TXRATE0NB	=	0x5167
                           005166  2738 G$AX5043_TXRATE1NB$0$0 == 0x5166
                           005166  2739 _AX5043_TXRATE1NB	=	0x5166
                           005165  2740 G$AX5043_TXRATE2NB$0$0 == 0x5165
                           005165  2741 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2742 G$AX5043_WAKEUP0NB$0$0 == 0x506b
                           00506B  2743 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2744 G$AX5043_WAKEUP1NB$0$0 == 0x506a
                           00506A  2745 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2746 G$AX5043_WAKEUPFREQ0NB$0$0 == 0x506d
                           00506D  2747 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2748 G$AX5043_WAKEUPFREQ1NB$0$0 == 0x506c
                           00506C  2749 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2750 G$AX5043_WAKEUPTIMER0NB$0$0 == 0x5069
                           005069  2751 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2752 G$AX5043_WAKEUPTIMER1NB$0$0 == 0x5068
                           005068  2753 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2754 G$AX5043_WAKEUPXOEARLYNB$0$0 == 0x506e
                           00506E  2755 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2756 G$AX5043_XTALAMPLNB$0$0 == 0x5f11
                           005F11  2757 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2758 G$AX5043_XTALCAPNB$0$0 == 0x5184
                           005184  2759 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2760 G$AX5043_XTALOSCNB$0$0 == 0x5f10
                           005F10  2761 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2762 G$AX5043_XTALSTATUSNB$0$0 == 0x501d
                           00501D  2763 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2764 G$AX5043_0xF00NB$0$0 == 0x5f00
                           005F00  2765 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2766 G$AX5043_0xF0CNB$0$0 == 0x5f0c
                           005F0C  2767 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2768 G$AX5043_0xF18NB$0$0 == 0x5f18
                           005F18  2769 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2770 G$AX5043_0xF1CNB$0$0 == 0x5f1c
                           005F1C  2771 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2772 G$AX5043_0xF21NB$0$0 == 0x5f21
                           005F21  2773 _AX5043_0xF21NB	=	0x5f21
                           005F22  2774 G$AX5043_0xF22NB$0$0 == 0x5f22
                           005F22  2775 _AX5043_0xF22NB	=	0x5f22
                           005F23  2776 G$AX5043_0xF23NB$0$0 == 0x5f23
                           005F23  2777 _AX5043_0xF23NB	=	0x5f23
                           005F26  2778 G$AX5043_0xF26NB$0$0 == 0x5f26
                           005F26  2779 _AX5043_0xF26NB	=	0x5f26
                           005F30  2780 G$AX5043_0xF30NB$0$0 == 0x5f30
                           005F30  2781 _AX5043_0xF30NB	=	0x5f30
                           005F31  2782 G$AX5043_0xF31NB$0$0 == 0x5f31
                           005F31  2783 _AX5043_0xF31NB	=	0x5f31
                           005F32  2784 G$AX5043_0xF32NB$0$0 == 0x5f32
                           005F32  2785 _AX5043_0xF32NB	=	0x5f32
                           005F33  2786 G$AX5043_0xF33NB$0$0 == 0x5f33
                           005F33  2787 _AX5043_0xF33NB	=	0x5f33
                           005F34  2788 G$AX5043_0xF34NB$0$0 == 0x5f34
                           005F34  2789 _AX5043_0xF34NB	=	0x5f34
                           005F35  2790 G$AX5043_0xF35NB$0$0 == 0x5f35
                           005F35  2791 _AX5043_0xF35NB	=	0x5f35
                           005F44  2792 G$AX5043_0xF44NB$0$0 == 0x5f44
                           005F44  2793 _AX5043_0xF44NB	=	0x5f44
                           005122  2794 G$AX5043_AGCAHYST0NB$0$0 == 0x5122
                           005122  2795 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2796 G$AX5043_AGCAHYST1NB$0$0 == 0x5132
                           005132  2797 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2798 G$AX5043_AGCAHYST2NB$0$0 == 0x5142
                           005142  2799 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2800 G$AX5043_AGCAHYST3NB$0$0 == 0x5152
                           005152  2801 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2802 G$AX5043_AGCGAIN0NB$0$0 == 0x5120
                           005120  2803 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2804 G$AX5043_AGCGAIN1NB$0$0 == 0x5130
                           005130  2805 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2806 G$AX5043_AGCGAIN2NB$0$0 == 0x5140
                           005140  2807 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2808 G$AX5043_AGCGAIN3NB$0$0 == 0x5150
                           005150  2809 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2810 G$AX5043_AGCMINMAX0NB$0$0 == 0x5123
                           005123  2811 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2812 G$AX5043_AGCMINMAX1NB$0$0 == 0x5133
                           005133  2813 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2814 G$AX5043_AGCMINMAX2NB$0$0 == 0x5143
                           005143  2815 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2816 G$AX5043_AGCMINMAX3NB$0$0 == 0x5153
                           005153  2817 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2818 G$AX5043_AGCTARGET0NB$0$0 == 0x5121
                           005121  2819 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2820 G$AX5043_AGCTARGET1NB$0$0 == 0x5131
                           005131  2821 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2822 G$AX5043_AGCTARGET2NB$0$0 == 0x5141
                           005141  2823 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2824 G$AX5043_AGCTARGET3NB$0$0 == 0x5151
                           005151  2825 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2826 G$AX5043_AMPLITUDEGAIN0NB$0$0 == 0x512b
                           00512B  2827 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2828 G$AX5043_AMPLITUDEGAIN1NB$0$0 == 0x513b
                           00513B  2829 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2830 G$AX5043_AMPLITUDEGAIN2NB$0$0 == 0x514b
                           00514B  2831 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2832 G$AX5043_AMPLITUDEGAIN3NB$0$0 == 0x515b
                           00515B  2833 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2834 G$AX5043_BBOFFSRES0NB$0$0 == 0x512f
                           00512F  2835 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2836 G$AX5043_BBOFFSRES1NB$0$0 == 0x513f
                           00513F  2837 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2838 G$AX5043_BBOFFSRES2NB$0$0 == 0x514f
                           00514F  2839 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2840 G$AX5043_BBOFFSRES3NB$0$0 == 0x515f
                           00515F  2841 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2842 G$AX5043_DRGAIN0NB$0$0 == 0x5125
                           005125  2843 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2844 G$AX5043_DRGAIN1NB$0$0 == 0x5135
                           005135  2845 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2846 G$AX5043_DRGAIN2NB$0$0 == 0x5145
                           005145  2847 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2848 G$AX5043_DRGAIN3NB$0$0 == 0x5155
                           005155  2849 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2850 G$AX5043_FOURFSK0NB$0$0 == 0x512e
                           00512E  2851 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2852 G$AX5043_FOURFSK1NB$0$0 == 0x513e
                           00513E  2853 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2854 G$AX5043_FOURFSK2NB$0$0 == 0x514e
                           00514E  2855 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2856 G$AX5043_FOURFSK3NB$0$0 == 0x515e
                           00515E  2857 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2858 G$AX5043_FREQDEV00NB$0$0 == 0x512d
                           00512D  2859 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2860 G$AX5043_FREQDEV01NB$0$0 == 0x513d
                           00513D  2861 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2862 G$AX5043_FREQDEV02NB$0$0 == 0x514d
                           00514D  2863 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2864 G$AX5043_FREQDEV03NB$0$0 == 0x515d
                           00515D  2865 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2866 G$AX5043_FREQDEV10NB$0$0 == 0x512c
                           00512C  2867 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2868 G$AX5043_FREQDEV11NB$0$0 == 0x513c
                           00513C  2869 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2870 G$AX5043_FREQDEV12NB$0$0 == 0x514c
                           00514C  2871 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2872 G$AX5043_FREQDEV13NB$0$0 == 0x515c
                           00515C  2873 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2874 G$AX5043_FREQUENCYGAINA0NB$0$0 == 0x5127
                           005127  2875 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2876 G$AX5043_FREQUENCYGAINA1NB$0$0 == 0x5137
                           005137  2877 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2878 G$AX5043_FREQUENCYGAINA2NB$0$0 == 0x5147
                           005147  2879 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2880 G$AX5043_FREQUENCYGAINA3NB$0$0 == 0x5157
                           005157  2881 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2882 G$AX5043_FREQUENCYGAINB0NB$0$0 == 0x5128
                           005128  2883 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2884 G$AX5043_FREQUENCYGAINB1NB$0$0 == 0x5138
                           005138  2885 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2886 G$AX5043_FREQUENCYGAINB2NB$0$0 == 0x5148
                           005148  2887 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2888 G$AX5043_FREQUENCYGAINB3NB$0$0 == 0x5158
                           005158  2889 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2890 G$AX5043_FREQUENCYGAINC0NB$0$0 == 0x5129
                           005129  2891 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2892 G$AX5043_FREQUENCYGAINC1NB$0$0 == 0x5139
                           005139  2893 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2894 G$AX5043_FREQUENCYGAINC2NB$0$0 == 0x5149
                           005149  2895 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2896 G$AX5043_FREQUENCYGAINC3NB$0$0 == 0x5159
                           005159  2897 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2898 G$AX5043_FREQUENCYGAIND0NB$0$0 == 0x512a
                           00512A  2899 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2900 G$AX5043_FREQUENCYGAIND1NB$0$0 == 0x513a
                           00513A  2901 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2902 G$AX5043_FREQUENCYGAIND2NB$0$0 == 0x514a
                           00514A  2903 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2904 G$AX5043_FREQUENCYGAIND3NB$0$0 == 0x515a
                           00515A  2905 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2906 G$AX5043_FREQUENCYLEAKNB$0$0 == 0x5116
                           005116  2907 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2908 G$AX5043_PHASEGAIN0NB$0$0 == 0x5126
                           005126  2909 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2910 G$AX5043_PHASEGAIN1NB$0$0 == 0x5136
                           005136  2911 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2912 G$AX5043_PHASEGAIN2NB$0$0 == 0x5146
                           005146  2913 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2914 G$AX5043_PHASEGAIN3NB$0$0 == 0x5156
                           005156  2915 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2916 G$AX5043_PKTADDR0NB$0$0 == 0x5207
                           005207  2917 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2918 G$AX5043_PKTADDR1NB$0$0 == 0x5206
                           005206  2919 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2920 G$AX5043_PKTADDR2NB$0$0 == 0x5205
                           005205  2921 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2922 G$AX5043_PKTADDR3NB$0$0 == 0x5204
                           005204  2923 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2924 G$AX5043_PKTADDRCFGNB$0$0 == 0x5200
                           005200  2925 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2926 G$AX5043_PKTADDRMASK0NB$0$0 == 0x520b
                           00520B  2927 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2928 G$AX5043_PKTADDRMASK1NB$0$0 == 0x520a
                           00520A  2929 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2930 G$AX5043_PKTADDRMASK2NB$0$0 == 0x5209
                           005209  2931 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2932 G$AX5043_PKTADDRMASK3NB$0$0 == 0x5208
                           005208  2933 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2934 G$AX5043_PKTLENCFGNB$0$0 == 0x5201
                           005201  2935 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2936 G$AX5043_PKTLENOFFSETNB$0$0 == 0x5202
                           005202  2937 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2938 G$AX5043_PKTMAXLENNB$0$0 == 0x5203
                           005203  2939 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2940 G$AX5043_RXPARAMCURSETNB$0$0 == 0x5118
                           005118  2941 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2942 G$AX5043_RXPARAMSETSNB$0$0 == 0x5117
                           005117  2943 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2944 G$AX5043_TIMEGAIN0NB$0$0 == 0x5124
                           005124  2945 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2946 G$AX5043_TIMEGAIN1NB$0$0 == 0x5134
                           005134  2947 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2948 G$AX5043_TIMEGAIN2NB$0$0 == 0x5144
                           005144  2949 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2950 G$AX5043_TIMEGAIN3NB$0$0 == 0x5154
                           005154  2951 _AX5043_TIMEGAIN3NB	=	0x5154
                           000000  2952 G$payload$0$0==.
      0003D6                       2953 _payload::
      0003D6                       2954 	.ds 28
                           00001C  2955 G$payload_len$0$0==.
      0003F2                       2956 _payload_len::
      0003F2                       2957 	.ds 2
                                   2958 ;--------------------------------------------------------
                                   2959 ; absolute external ram data
                                   2960 ;--------------------------------------------------------
                                   2961 	.area XABS    (ABS,XDATA)
                                   2962 ;--------------------------------------------------------
                                   2963 ; external initialized ram data
                                   2964 ;--------------------------------------------------------
                                   2965 	.area XISEG   (XDATA)
                           000000  2966 G$trmID$0$0==.
      0009C1                       2967 _trmID::
      0009C1                       2968 	.ds 4
                           000004  2969 G$StdPrem$0$0==.
      0009C5                       2970 _StdPrem::
      0009C5                       2971 	.ds 1
                           000005  2972 G$pkts_received$0$0==.
      0009C6                       2973 _pkts_received::
      0009C6                       2974 	.ds 2
                           000007  2975 G$pkts_missing$0$0==.
      0009C8                       2976 _pkts_missing::
      0009C8                       2977 	.ds 2
                           000009  2978 G$pkt_counter$0$0==.
      0009CA                       2979 _pkt_counter::
      0009CA                       2980 	.ds 2
                           00000B  2981 G$ack_flg$0$0==.
      0009CC                       2982 _ack_flg::
      0009CC                       2983 	.ds 1
                           00000C  2984 G$txdone$0$0==.
      0009CD                       2985 _txdone::
      0009CD                       2986 	.ds 1
                           00000D  2987 G$premSlot_counter$0$0==.
      0009CE                       2988 _premSlot_counter::
      0009CE                       2989 	.ds 1
                           00000E  2990 G$wt0_beacon$0$0==.
      0009CF                       2991 _wt0_beacon::
      0009CF                       2992 	.ds 4
                           000012  2993 G$wt0_tx3$0$0==.
      0009D3                       2994 _wt0_tx3::
      0009D3                       2995 	.ds 4
                                   2996 	.area HOME    (CODE)
                                   2997 	.area GSINIT0 (CODE)
                                   2998 	.area GSINIT1 (CODE)
                                   2999 	.area GSINIT2 (CODE)
                                   3000 	.area GSINIT3 (CODE)
                                   3001 	.area GSINIT4 (CODE)
                                   3002 	.area GSINIT5 (CODE)
                                   3003 	.area GSINIT  (CODE)
                                   3004 	.area GSFINAL (CODE)
                                   3005 	.area CSEG    (CODE)
                                   3006 ;--------------------------------------------------------
                                   3007 ; interrupt vector 
                                   3008 ;--------------------------------------------------------
                                   3009 	.area HOME    (CODE)
      000000                       3010 __interrupt_vect:
      000000 02 03 11         [24] 3011 	ljmp	__sdcc_gsinit_startup
      000003 32               [24] 3012 	reti
      000004                       3013 	.ds	7
      00000B 02 00 E8         [24] 3014 	ljmp	_wtimer_irq
      00000E                       3015 	.ds	5
      000013 32               [24] 3016 	reti
      000014                       3017 	.ds	7
      00001B 32               [24] 3018 	reti
      00001C                       3019 	.ds	7
      000023 02 16 CD         [24] 3020 	ljmp	_axradio_isr
      000026                       3021 	.ds	5
      00002B 32               [24] 3022 	reti
      00002C                       3023 	.ds	7
      000033 02 57 B2         [24] 3024 	ljmp	_pwrmgmt_irq
      000036                       3025 	.ds	5
      00003B 32               [24] 3026 	reti
      00003C                       3027 	.ds	7
      000043 32               [24] 3028 	reti
      000044                       3029 	.ds	7
      00004B 32               [24] 3030 	reti
      00004C                       3031 	.ds	7
      000053 32               [24] 3032 	reti
      000054                       3033 	.ds	7
      00005B 02 02 DA         [24] 3034 	ljmp	_uart0_irq
      00005E                       3035 	.ds	5
      000063 02 00 B1         [24] 3036 	ljmp	_uart1_irq
      000066                       3037 	.ds	5
      00006B 32               [24] 3038 	reti
      00006C                       3039 	.ds	7
      000073 32               [24] 3040 	reti
      000074                       3041 	.ds	7
      00007B 32               [24] 3042 	reti
      00007C                       3043 	.ds	7
      000083 32               [24] 3044 	reti
      000084                       3045 	.ds	7
      00008B 32               [24] 3046 	reti
      00008C                       3047 	.ds	7
      000093 32               [24] 3048 	reti
      000094                       3049 	.ds	7
      00009B 32               [24] 3050 	reti
      00009C                       3051 	.ds	7
      0000A3 32               [24] 3052 	reti
      0000A4                       3053 	.ds	7
      0000AB 02 02 A3         [24] 3054 	ljmp	_dbglink_irq
                                   3055 ;--------------------------------------------------------
                                   3056 ; global & static initialisations
                                   3057 ;--------------------------------------------------------
                                   3058 	.area HOME    (CODE)
                                   3059 	.area GSINIT  (CODE)
                                   3060 	.area GSFINAL (CODE)
                                   3061 	.area GSINIT  (CODE)
                                   3062 	.globl __sdcc_gsinit_startup
                                   3063 	.globl __sdcc_program_startup
                                   3064 	.globl __start__stack
                                   3065 	.globl __mcs51_genXINIT
                                   3066 	.globl __mcs51_genXRAMCLEAR
                                   3067 	.globl __mcs51_genRAMCLEAR
                           000000  3068 	C$main.c$76$1$392 ==.
                                   3069 ;	main.c:76: uint8_t __data coldstart = 1; /* caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */
      000398 75 50 01         [24] 3070 	mov	_coldstart,#0x01
                                   3071 	.area GSFINAL (CODE)
      00039B 02 00 AE         [24] 3072 	ljmp	__sdcc_program_startup
                                   3073 ;--------------------------------------------------------
                                   3074 ; Home
                                   3075 ;--------------------------------------------------------
                                   3076 	.area HOME    (CODE)
                                   3077 	.area HOME    (CODE)
      0000AE                       3078 __sdcc_program_startup:
      0000AE 02 5B 54         [24] 3079 	ljmp	_main
                                   3080 ;	return from main will return to caller
                                   3081 ;--------------------------------------------------------
                                   3082 ; code
                                   3083 ;--------------------------------------------------------
                                   3084 	.area CSEG    (CODE)
                                   3085 ;------------------------------------------------------------
                                   3086 ;Allocation info for local variables in function 'pwrmgmt_irq'
                                   3087 ;------------------------------------------------------------
                                   3088 ;pc                        Allocated to registers r7 
                                   3089 ;------------------------------------------------------------
                           000000  3090 	Fmain$pwrmgmt_irq$0$0 ==.
                           000000  3091 	C$main.c$101$0$0 ==.
                                   3092 ;	main.c:101: static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
                                   3093 ;	-----------------------------------------
                                   3094 ;	 function pwrmgmt_irq
                                   3095 ;	-----------------------------------------
      0057B2                       3096 _pwrmgmt_irq:
                           000007  3097 	ar7 = 0x07
                           000006  3098 	ar6 = 0x06
                           000005  3099 	ar5 = 0x05
                           000004  3100 	ar4 = 0x04
                           000003  3101 	ar3 = 0x03
                           000002  3102 	ar2 = 0x02
                           000001  3103 	ar1 = 0x01
                           000000  3104 	ar0 = 0x00
      0057B2 C0 E0            [24] 3105 	push	acc
      0057B4 C0 82            [24] 3106 	push	dpl
      0057B6 C0 83            [24] 3107 	push	dph
      0057B8 C0 07            [24] 3108 	push	ar7
      0057BA C0 D0            [24] 3109 	push	psw
      0057BC 75 D0 00         [24] 3110 	mov	psw,#0x00
                           00000D  3111 	C$main.c$103$1$0 ==.
                                   3112 ;	main.c:103: uint8_t pc = PCON;
                           00000D  3113 	C$main.c$105$1$369 ==.
                                   3114 ;	main.c:105: if (!(pc & 0x80))
      0057BF E5 87            [12] 3115 	mov	a,_PCON
      0057C1 FF               [12] 3116 	mov	r7,a
      0057C2 20 E7 02         [24] 3117 	jb	acc.7,00102$
                           000013  3118 	C$main.c$106$1$369 ==.
                                   3119 ;	main.c:106: return;
      0057C5 80 10            [24] 3120 	sjmp	00106$
      0057C7                       3121 00102$:
                           000015  3122 	C$main.c$108$1$369 ==.
                                   3123 ;	main.c:108: GPIOENABLE = 0;
      0057C7 90 70 0C         [24] 3124 	mov	dptr,#_GPIOENABLE
      0057CA E4               [12] 3125 	clr	a
      0057CB F0               [24] 3126 	movx	@dptr,a
                           00001A  3127 	C$main.c$109$1$369 ==.
                                   3128 ;	main.c:109: IE = EIE = E2IE = 0;
                                   3129 ;	1-genFromRTrack replaced	mov	_E2IE,#0x00
      0057CC F5 A0            [12] 3130 	mov	_E2IE,a
                                   3131 ;	1-genFromRTrack replaced	mov	_EIE,#0x00
      0057CE F5 98            [12] 3132 	mov	_EIE,a
                                   3133 ;	1-genFromRTrack replaced	mov	_IE,#0x00
      0057D0 F5 A8            [12] 3134 	mov	_IE,a
      0057D2                       3135 00104$:
                           000020  3136 	C$main.c$112$1$369 ==.
                                   3137 ;	main.c:112: PCON |= 0x01;
      0057D2 43 87 01         [24] 3138 	orl	_PCON,#0x01
      0057D5 80 FB            [24] 3139 	sjmp	00104$
      0057D7                       3140 00106$:
      0057D7 D0 D0            [24] 3141 	pop	psw
      0057D9 D0 07            [24] 3142 	pop	ar7
      0057DB D0 83            [24] 3143 	pop	dph
      0057DD D0 82            [24] 3144 	pop	dpl
      0057DF D0 E0            [24] 3145 	pop	acc
                           00002F  3146 	C$main.c$113$1$369 ==.
                           00002F  3147 	XFmain$pwrmgmt_irq$0$0 ==.
      0057E1 32               [24] 3148 	reti
                                   3149 ;	eliminated unneeded push/pop b
                                   3150 ;------------------------------------------------------------
                                   3151 ;Allocation info for local variables in function 'axradio_statuschange'
                                   3152 ;------------------------------------------------------------
                                   3153 ;st                        Allocated with name '_axradio_statuschange_st_1_370'
                                   3154 ;pktdata                   Allocated to registers r4 r5 
                                   3155 ;foffs                     Allocated to registers r4 r5 r6 r7 
                                   3156 ;sloc0                     Allocated with name '_axradio_statuschange_sloc0_1_0'
                                   3157 ;pktlen                    Allocated with name '_axradio_statuschange_pktlen_3_373'
                                   3158 ;------------------------------------------------------------
                           000030  3159 	G$axradio_statuschange$0$0 ==.
                           000030  3160 	C$main.c$115$1$369 ==.
                                   3161 ;	main.c:115: void axradio_statuschange(struct axradio_status __xdata *st)
                                   3162 ;	-----------------------------------------
                                   3163 ;	 function axradio_statuschange
                                   3164 ;	-----------------------------------------
      0057E2                       3165 _axradio_statuschange:
                           000030  3166 	C$main.c$129$1$371 ==.
                                   3167 ;	main.c:129: switch (st->status)
      0057E2 85 82 51         [24] 3168 	mov	_axradio_statuschange_st_1_370,dpl
      0057E5 85 83 52         [24] 3169 	mov  (_axradio_statuschange_st_1_370 + 1),dph
      0057E8 E0               [24] 3170 	movx	a,@dptr
      0057E9 FD               [12] 3171 	mov	r5,a
      0057EA 60 03            [24] 3172 	jz	00157$
      0057EC 02 5A C6         [24] 3173 	ljmp	00126$
      0057EF                       3174 00157$:
                           00003D  3175 	C$main.c$134$3$373 ==.
                                   3176 ;	main.c:134: uint8_t __xdata pktlen = st->u.rx.pktlen;
      0057EF 74 06            [12] 3177 	mov	a,#0x06
      0057F1 25 51            [12] 3178 	add	a,_axradio_statuschange_st_1_370
      0057F3 FC               [12] 3179 	mov	r4,a
      0057F4 E4               [12] 3180 	clr	a
      0057F5 35 52            [12] 3181 	addc	a,(_axradio_statuschange_st_1_370 + 1)
      0057F7 FD               [12] 3182 	mov	r5,a
      0057F8 74 18            [12] 3183 	mov	a,#0x18
      0057FA 2C               [12] 3184 	add	a,r4
      0057FB F5 82            [12] 3185 	mov	dpl,a
      0057FD E4               [12] 3186 	clr	a
      0057FE 3D               [12] 3187 	addc	a,r5
      0057FF F5 83            [12] 3188 	mov	dph,a
      005801 E0               [24] 3189 	movx	a,@dptr
      005802 FA               [12] 3190 	mov	r2,a
      005803 A3               [24] 3191 	inc	dptr
      005804 E0               [24] 3192 	movx	a,@dptr
                           000053  3193 	C$main.c$135$3$373 ==.
                                   3194 ;	main.c:135: const uint8_t __xdata *pktdata = st->u.rx.mac.raw;
      005805 74 14            [12] 3195 	mov	a,#0x14
      005807 2C               [12] 3196 	add	a,r4
      005808 F5 82            [12] 3197 	mov	dpl,a
      00580A E4               [12] 3198 	clr	a
      00580B 3D               [12] 3199 	addc	a,r5
      00580C F5 83            [12] 3200 	mov	dph,a
      00580E E0               [24] 3201 	movx	a,@dptr
      00580F FC               [12] 3202 	mov	r4,a
      005810 A3               [24] 3203 	inc	dptr
      005811 E0               [24] 3204 	movx	a,@dptr
      005812 FD               [12] 3205 	mov	r5,a
                           000061  3206 	C$main.c$138$3$373 ==.
                                   3207 ;	main.c:138: if (st->error == AXRADIO_ERR_NOERROR)
      005813 74 01            [12] 3208 	mov	a,#0x01
      005815 25 51            [12] 3209 	add	a,_axradio_statuschange_st_1_370
      005817 F9               [12] 3210 	mov	r1,a
      005818 E4               [12] 3211 	clr	a
      005819 35 52            [12] 3212 	addc	a,(_axradio_statuschange_st_1_370 + 1)
      00581B FB               [12] 3213 	mov	r3,a
      00581C 89 82            [24] 3214 	mov	dpl,r1
      00581E 8B 83            [24] 3215 	mov	dph,r3
      005820 E0               [24] 3216 	movx	a,@dptr
      005821 60 03            [24] 3217 	jz	00158$
      005823 02 5A 09         [24] 3218 	ljmp	00118$
      005826                       3219 00158$:
                           000074  3220 	C$main.c$140$4$374 ==.
                                   3221 ;	main.c:140: if (pktlen == 8) // check beacon frame
      005826 BA 08 02         [24] 3222 	cjne	r2,#0x08,00159$
      005829 80 03            [24] 3223 	sjmp	00160$
      00582B                       3224 00159$:
      00582B 02 58 B4         [24] 3225 	ljmp	00115$
      00582E                       3226 00160$:
                           00007C  3227 	C$main.c$142$5$375 ==.
                                   3228 ;	main.c:142: wt0_beacon = wtimer0_curtime();
      00582E 12 7D AD         [24] 3229 	lcall	_wtimer0_curtime
      005831 A8 82            [24] 3230 	mov	r0,dpl
      005833 A9 83            [24] 3231 	mov	r1,dph
      005835 AB F0            [24] 3232 	mov	r3,b
      005837 FF               [12] 3233 	mov	r7,a
      005838 90 09 CF         [24] 3234 	mov	dptr,#_wt0_beacon
      00583B E8               [12] 3235 	mov	a,r0
      00583C F0               [24] 3236 	movx	@dptr,a
      00583D E9               [12] 3237 	mov	a,r1
      00583E A3               [24] 3238 	inc	dptr
      00583F F0               [24] 3239 	movx	@dptr,a
      005840 EB               [12] 3240 	mov	a,r3
      005841 A3               [24] 3241 	inc	dptr
      005842 F0               [24] 3242 	movx	@dptr,a
      005843 EF               [12] 3243 	mov	a,r7
      005844 A3               [24] 3244 	inc	dptr
      005845 F0               [24] 3245 	movx	@dptr,a
                           000094  3246 	C$main.c$144$5$375 ==.
                                   3247 ;	main.c:144: dbglink_writestr("5_RX_WT0:");
      005846 90 81 83         [24] 3248 	mov	dptr,#___str_0
      005849 75 F0 80         [24] 3249 	mov	b,#0x80
      00584C 12 73 05         [24] 3250 	lcall	_dbglink_writestr
                           00009D  3251 	C$main.c$145$5$375 ==.
                                   3252 ;	main.c:145: dbglink_writehex32(/*wtimer0_curtime()*/wt0_beacon, 8, WRNUM_PADZERO);
      00584F 90 09 CF         [24] 3253 	mov	dptr,#_wt0_beacon
      005852 E0               [24] 3254 	movx	a,@dptr
      005853 F9               [12] 3255 	mov	r1,a
      005854 A3               [24] 3256 	inc	dptr
      005855 E0               [24] 3257 	movx	a,@dptr
      005856 FB               [12] 3258 	mov	r3,a
      005857 A3               [24] 3259 	inc	dptr
      005858 E0               [24] 3260 	movx	a,@dptr
      005859 FE               [12] 3261 	mov	r6,a
      00585A A3               [24] 3262 	inc	dptr
      00585B E0               [24] 3263 	movx	a,@dptr
      00585C FF               [12] 3264 	mov	r7,a
      00585D 74 08            [12] 3265 	mov	a,#0x08
      00585F C0 E0            [24] 3266 	push	acc
      005861 C0 E0            [24] 3267 	push	acc
      005863 89 82            [24] 3268 	mov	dpl,r1
      005865 8B 83            [24] 3269 	mov	dph,r3
      005867 8E F0            [24] 3270 	mov	b,r6
      005869 EF               [12] 3271 	mov	a,r7
      00586A 12 75 88         [24] 3272 	lcall	_dbglink_writehex32
      00586D 15 81            [12] 3273 	dec	sp
      00586F 15 81            [12] 3274 	dec	sp
                           0000BF  3275 	C$main.c$146$5$375 ==.
                                   3276 ;	main.c:146: dbglink_tx('\n');
      005871 75 82 0A         [24] 3277 	mov	dpl,#0x0a
      005874 12 61 21         [24] 3278 	lcall	_dbglink_tx
                           0000C5  3279 	C$main.c$148$5$375 ==.
                                   3280 ;	main.c:148: ++pkts_received;
      005877 90 09 C6         [24] 3281 	mov	dptr,#_pkts_received
      00587A E0               [24] 3282 	movx	a,@dptr
      00587B 24 01            [12] 3283 	add	a,#0x01
      00587D F0               [24] 3284 	movx	@dptr,a
      00587E A3               [24] 3285 	inc	dptr
      00587F E0               [24] 3286 	movx	a,@dptr
      005880 34 00            [12] 3287 	addc	a,#0x00
      005882 F0               [24] 3288 	movx	@dptr,a
                           0000D1  3289 	C$main.c$155$5$375 ==.
                                   3290 ;	main.c:155: dbglink_received_packet(st);
      005883 85 51 82         [24] 3291 	mov	dpl,_axradio_statuschange_st_1_370
      005886 85 52 83         [24] 3292 	mov	dph,(_axradio_statuschange_st_1_370 + 1)
      005889 12 0B A7         [24] 3293 	lcall	_dbglink_received_packet
                           0000DA  3294 	C$main.c$158$5$375 ==.
                                   3295 ;	main.c:158: if(StdPrem == 0x1) // In the case of standard user type
      00588C 90 09 C5         [24] 3296 	mov	dptr,#_StdPrem
      00588F E0               [24] 3297 	movx	a,@dptr
      005890 FF               [12] 3298 	mov	r7,a
      005891 BF 01 06         [24] 3299 	cjne	r7,#0x01,00108$
                           0000E2  3300 	C$main.c$169$6$376 ==.
                                   3301 ;	main.c:169: transmit_copies_wtimer();
      005894 12 4E 48         [24] 3302 	lcall	_transmit_copies_wtimer
      005897 02 5A 09         [24] 3303 	ljmp	00118$
      00589A                       3304 00108$:
                           0000E8  3305 	C$main.c$172$5$375 ==.
                                   3306 ;	main.c:172: else if (StdPrem == 0x0) // In the case of premium user type
      00589A EF               [12] 3307 	mov	a,r7
      00589B 70 06            [24] 3308 	jnz	00105$
                           0000EB  3309 	C$main.c$183$6$377 ==.
                                   3310 ;	main.c:183: transmit_prem_wtimer();
      00589D 12 50 3F         [24] 3311 	lcall	_transmit_prem_wtimer
      0058A0 02 5A 09         [24] 3312 	ljmp	00118$
      0058A3                       3313 00105$:
                           0000F1  3314 	C$main.c$187$5$375 ==.
                                   3315 ;	main.c:187: else if (StdPrem == 0x2) // In the case of timing test
      0058A3 BF 02 02         [24] 3316 	cjne	r7,#0x02,00164$
      0058A6 80 03            [24] 3317 	sjmp	00165$
      0058A8                       3318 00164$:
      0058A8 02 5A 09         [24] 3319 	ljmp	00118$
      0058AB                       3320 00165$:
                           0000F9  3321 	C$main.c$189$6$378 ==.
                                   3322 ;	main.c:189: transmit_prem_wtimer();
      0058AB 12 50 3F         [24] 3323 	lcall	_transmit_prem_wtimer
                           0000FC  3324 	C$main.c$190$6$378 ==.
                                   3325 ;	main.c:190: transmit_copies_wtimer();
      0058AE 12 4E 48         [24] 3326 	lcall	_transmit_copies_wtimer
      0058B1 02 5A 09         [24] 3327 	ljmp	00118$
      0058B4                       3328 00115$:
                           000102  3329 	C$main.c$194$4$374 ==.
                                   3330 ;	main.c:194: else if ((pktlen == 4) && (pktdata[1] >> ((uint8_t)(trmID & 0x00000F)-1)))
      0058B4 BA 04 02         [24] 3331 	cjne	r2,#0x04,00166$
      0058B7 80 03            [24] 3332 	sjmp	00167$
      0058B9                       3333 00166$:
      0058B9 02 59 B2         [24] 3334 	ljmp	00111$
      0058BC                       3335 00167$:
      0058BC 8C 82            [24] 3336 	mov	dpl,r4
      0058BE 8D 83            [24] 3337 	mov	dph,r5
      0058C0 A3               [24] 3338 	inc	dptr
      0058C1 E0               [24] 3339 	movx	a,@dptr
      0058C2 FF               [12] 3340 	mov	r7,a
      0058C3 90 09 C1         [24] 3341 	mov	dptr,#_trmID
      0058C6 E0               [24] 3342 	movx	a,@dptr
      0058C7 F9               [12] 3343 	mov	r1,a
      0058C8 A3               [24] 3344 	inc	dptr
      0058C9 E0               [24] 3345 	movx	a,@dptr
      0058CA A3               [24] 3346 	inc	dptr
      0058CB E0               [24] 3347 	movx	a,@dptr
      0058CC A3               [24] 3348 	inc	dptr
      0058CD E0               [24] 3349 	movx	a,@dptr
      0058CE 53 01 0F         [24] 3350 	anl	ar1,#0x0f
      0058D1 7A 00            [12] 3351 	mov	r2,#0x00
      0058D3 7B 00            [12] 3352 	mov	r3,#0x00
      0058D5 7E 00            [12] 3353 	mov	r6,#0x00
      0058D7 19               [12] 3354 	dec	r1
      0058D8 B9 FF 01         [24] 3355 	cjne	r1,#0xff,00168$
      0058DB 1E               [12] 3356 	dec	r6
      0058DC                       3357 00168$:
      0058DC 89 F0            [24] 3358 	mov	b,r1
      0058DE 05 F0            [12] 3359 	inc	b
      0058E0 EF               [12] 3360 	mov	a,r7
      0058E1 80 02            [24] 3361 	sjmp	00170$
      0058E3                       3362 00169$:
      0058E3 C3               [12] 3363 	clr	c
      0058E4 13               [12] 3364 	rrc	a
      0058E5                       3365 00170$:
      0058E5 D5 F0 FB         [24] 3366 	djnz	b,00169$
      0058E8 70 03            [24] 3367 	jnz	00171$
      0058EA 02 59 B2         [24] 3368 	ljmp	00111$
      0058ED                       3369 00171$:
                           00013B  3370 	C$main.c$196$5$379 ==.
                                   3371 ;	main.c:196: ack_flg = 1;
      0058ED 90 09 CC         [24] 3372 	mov	dptr,#_ack_flg
      0058F0 74 01            [12] 3373 	mov	a,#0x01
      0058F2 F0               [24] 3374 	movx	@dptr,a
                           000141  3375 	C$main.c$197$5$379 ==.
                                   3376 ;	main.c:197: pkt_counter += 1;
      0058F3 90 09 CA         [24] 3377 	mov	dptr,#_pkt_counter
      0058F6 E0               [24] 3378 	movx	a,@dptr
      0058F7 FE               [12] 3379 	mov	r6,a
      0058F8 A3               [24] 3380 	inc	dptr
      0058F9 E0               [24] 3381 	movx	a,@dptr
      0058FA FF               [12] 3382 	mov	r7,a
      0058FB 90 09 CA         [24] 3383 	mov	dptr,#_pkt_counter
      0058FE 74 01            [12] 3384 	mov	a,#0x01
      005900 2E               [12] 3385 	add	a,r6
      005901 F0               [24] 3386 	movx	@dptr,a
      005902 E4               [12] 3387 	clr	a
      005903 3F               [12] 3388 	addc	a,r7
      005904 A3               [24] 3389 	inc	dptr
      005905 F0               [24] 3390 	movx	@dptr,a
                           000154  3391 	C$main.c$199$5$379 ==.
                                   3392 ;	main.c:199: dbglink_writestr("90_ACK_WT0:");
      005906 90 81 8D         [24] 3393 	mov	dptr,#___str_1
      005909 75 F0 80         [24] 3394 	mov	b,#0x80
      00590C 12 73 05         [24] 3395 	lcall	_dbglink_writestr
                           00015D  3396 	C$main.c$200$5$379 ==.
                                   3397 ;	main.c:200: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      00590F 12 7D AD         [24] 3398 	lcall	_wtimer0_curtime
      005912 AA 82            [24] 3399 	mov	r2,dpl
      005914 AB 83            [24] 3400 	mov	r3,dph
      005916 AE F0            [24] 3401 	mov	r6,b
      005918 FF               [12] 3402 	mov	r7,a
      005919 74 08            [12] 3403 	mov	a,#0x08
      00591B C0 E0            [24] 3404 	push	acc
      00591D C0 E0            [24] 3405 	push	acc
      00591F 8A 82            [24] 3406 	mov	dpl,r2
      005921 8B 83            [24] 3407 	mov	dph,r3
      005923 8E F0            [24] 3408 	mov	b,r6
      005925 EF               [12] 3409 	mov	a,r7
      005926 12 75 88         [24] 3410 	lcall	_dbglink_writehex32
      005929 15 81            [12] 3411 	dec	sp
      00592B 15 81            [12] 3412 	dec	sp
                           00017B  3413 	C$main.c$201$5$379 ==.
                                   3414 ;	main.c:201: dbglink_tx('\n');
      00592D 75 82 0A         [24] 3415 	mov	dpl,#0x0a
      005930 12 61 21         [24] 3416 	lcall	_dbglink_tx
                           000181  3417 	C$main.c$202$5$379 ==.
                                   3418 ;	main.c:202: dbglink_writestr("91_ACK_delta:");
      005933 90 81 99         [24] 3419 	mov	dptr,#___str_2
      005936 75 F0 80         [24] 3420 	mov	b,#0x80
      005939 12 73 05         [24] 3421 	lcall	_dbglink_writestr
                           00018A  3422 	C$main.c$203$5$379 ==.
                                   3423 ;	main.c:203: dbglink_writehex32(wtimer0_curtime()-wt0_tx3, 4, WRNUM_PADZERO);
      00593C 12 7D AD         [24] 3424 	lcall	_wtimer0_curtime
      00593F 85 82 53         [24] 3425 	mov	_axradio_statuschange_sloc0_1_0,dpl
      005942 85 83 54         [24] 3426 	mov	(_axradio_statuschange_sloc0_1_0 + 1),dph
      005945 85 F0 55         [24] 3427 	mov	(_axradio_statuschange_sloc0_1_0 + 2),b
      005948 F5 56            [12] 3428 	mov	(_axradio_statuschange_sloc0_1_0 + 3),a
      00594A 90 09 D3         [24] 3429 	mov	dptr,#_wt0_tx3
      00594D E0               [24] 3430 	movx	a,@dptr
      00594E F8               [12] 3431 	mov	r0,a
      00594F A3               [24] 3432 	inc	dptr
      005950 E0               [24] 3433 	movx	a,@dptr
      005951 F9               [12] 3434 	mov	r1,a
      005952 A3               [24] 3435 	inc	dptr
      005953 E0               [24] 3436 	movx	a,@dptr
      005954 FE               [12] 3437 	mov	r6,a
      005955 A3               [24] 3438 	inc	dptr
      005956 E0               [24] 3439 	movx	a,@dptr
      005957 FF               [12] 3440 	mov	r7,a
      005958 E5 53            [12] 3441 	mov	a,_axradio_statuschange_sloc0_1_0
      00595A C3               [12] 3442 	clr	c
      00595B 98               [12] 3443 	subb	a,r0
      00595C F8               [12] 3444 	mov	r0,a
      00595D E5 54            [12] 3445 	mov	a,(_axradio_statuschange_sloc0_1_0 + 1)
      00595F 99               [12] 3446 	subb	a,r1
      005960 F9               [12] 3447 	mov	r1,a
      005961 E5 55            [12] 3448 	mov	a,(_axradio_statuschange_sloc0_1_0 + 2)
      005963 9E               [12] 3449 	subb	a,r6
      005964 FE               [12] 3450 	mov	r6,a
      005965 E5 56            [12] 3451 	mov	a,(_axradio_statuschange_sloc0_1_0 + 3)
      005967 9F               [12] 3452 	subb	a,r7
      005968 FF               [12] 3453 	mov	r7,a
      005969 74 08            [12] 3454 	mov	a,#0x08
      00596B C0 E0            [24] 3455 	push	acc
      00596D 03               [12] 3456 	rr	a
      00596E C0 E0            [24] 3457 	push	acc
      005970 88 82            [24] 3458 	mov	dpl,r0
      005972 89 83            [24] 3459 	mov	dph,r1
      005974 8E F0            [24] 3460 	mov	b,r6
      005976 EF               [12] 3461 	mov	a,r7
      005977 12 75 88         [24] 3462 	lcall	_dbglink_writehex32
      00597A 15 81            [12] 3463 	dec	sp
      00597C 15 81            [12] 3464 	dec	sp
                           0001CC  3465 	C$main.c$204$5$379 ==.
                                   3466 ;	main.c:204: dbglink_tx('\n');
      00597E 75 82 0A         [24] 3467 	mov	dpl,#0x0a
      005981 12 61 21         [24] 3468 	lcall	_dbglink_tx
                           0001D2  3469 	C$main.c$205$5$379 ==.
                                   3470 ;	main.c:205: dbglink_writehex16(pkt_counter, 4, WRNUM_PADZERO);
      005984 90 09 CA         [24] 3471 	mov	dptr,#_pkt_counter
      005987 E0               [24] 3472 	movx	a,@dptr
      005988 FE               [12] 3473 	mov	r6,a
      005989 A3               [24] 3474 	inc	dptr
      00598A E0               [24] 3475 	movx	a,@dptr
      00598B FF               [12] 3476 	mov	r7,a
      00598C 74 08            [12] 3477 	mov	a,#0x08
      00598E C0 E0            [24] 3478 	push	acc
      005990 03               [12] 3479 	rr	a
      005991 C0 E0            [24] 3480 	push	acc
      005993 8E 82            [24] 3481 	mov	dpl,r6
      005995 8F 83            [24] 3482 	mov	dph,r7
      005997 12 77 A6         [24] 3483 	lcall	_dbglink_writehex16
      00599A 15 81            [12] 3484 	dec	sp
      00599C 15 81            [12] 3485 	dec	sp
                           0001EC  3486 	C$main.c$206$5$379 ==.
                                   3487 ;	main.c:206: dbglink_writestr(" successful pkts.\n\n");
      00599E 90 81 A7         [24] 3488 	mov	dptr,#___str_3
      0059A1 75 F0 80         [24] 3489 	mov	b,#0x80
      0059A4 12 73 05         [24] 3490 	lcall	_dbglink_writestr
                           0001F5  3491 	C$main.c$207$5$379 ==.
                                   3492 ;	main.c:207: dbglink_received_packet(st);
      0059A7 85 51 82         [24] 3493 	mov	dpl,_axradio_statuschange_st_1_370
      0059AA 85 52 83         [24] 3494 	mov	dph,(_axradio_statuschange_st_1_370 + 1)
      0059AD 12 0B A7         [24] 3495 	lcall	_dbglink_received_packet
      0059B0 80 57            [24] 3496 	sjmp	00118$
      0059B2                       3497 00111$:
                           000200  3498 	C$main.c$234$5$380 ==.
                                   3499 ;	main.c:234: dbglink_received_packet(st);
      0059B2 85 51 82         [24] 3500 	mov	dpl,_axradio_statuschange_st_1_370
      0059B5 85 52 83         [24] 3501 	mov	dph,(_axradio_statuschange_st_1_370 + 1)
      0059B8 C0 05            [24] 3502 	push	ar5
      0059BA C0 04            [24] 3503 	push	ar4
      0059BC 12 0B A7         [24] 3504 	lcall	_dbglink_received_packet
      0059BF D0 04            [24] 3505 	pop	ar4
      0059C1 D0 05            [24] 3506 	pop	ar5
                           000211  3507 	C$main.c$235$5$380 ==.
                                   3508 ;	main.c:235: dbglink_writenum16((pktdata[1]>>(uint8_t)(trmID & 0x00000F)),1,WRNUM_PADZERO);
      0059C3 8C 82            [24] 3509 	mov	dpl,r4
      0059C5 8D 83            [24] 3510 	mov	dph,r5
      0059C7 A3               [24] 3511 	inc	dptr
      0059C8 E0               [24] 3512 	movx	a,@dptr
      0059C9 FF               [12] 3513 	mov	r7,a
      0059CA 90 09 C1         [24] 3514 	mov	dptr,#_trmID
      0059CD E0               [24] 3515 	movx	a,@dptr
      0059CE FB               [12] 3516 	mov	r3,a
      0059CF A3               [24] 3517 	inc	dptr
      0059D0 E0               [24] 3518 	movx	a,@dptr
      0059D1 A3               [24] 3519 	inc	dptr
      0059D2 E0               [24] 3520 	movx	a,@dptr
      0059D3 A3               [24] 3521 	inc	dptr
      0059D4 E0               [24] 3522 	movx	a,@dptr
      0059D5 53 03 0F         [24] 3523 	anl	ar3,#0x0f
      0059D8 7C 00            [12] 3524 	mov	r4,#0x00
      0059DA 7D 00            [12] 3525 	mov	r5,#0x00
      0059DC 7E 00            [12] 3526 	mov	r6,#0x00
      0059DE 8B F0            [24] 3527 	mov	b,r3
      0059E0 05 F0            [12] 3528 	inc	b
      0059E2 EF               [12] 3529 	mov	a,r7
      0059E3 80 02            [24] 3530 	sjmp	00173$
      0059E5                       3531 00172$:
      0059E5 C3               [12] 3532 	clr	c
      0059E6 13               [12] 3533 	rrc	a
      0059E7                       3534 00173$:
      0059E7 D5 F0 FB         [24] 3535 	djnz	b,00172$
      0059EA FF               [12] 3536 	mov	r7,a
      0059EB 7E 00            [12] 3537 	mov	r6,#0x00
      0059ED 74 08            [12] 3538 	mov	a,#0x08
      0059EF C0 E0            [24] 3539 	push	acc
      0059F1 74 01            [12] 3540 	mov	a,#0x01
      0059F3 C0 E0            [24] 3541 	push	acc
      0059F5 8F 82            [24] 3542 	mov	dpl,r7
      0059F7 8E 83            [24] 3543 	mov	dph,r6
      0059F9 12 7C 71         [24] 3544 	lcall	_dbglink_writenum16
      0059FC 15 81            [12] 3545 	dec	sp
      0059FE 15 81            [12] 3546 	dec	sp
                           00024E  3547 	C$main.c$236$5$380 ==.
                                   3548 ;	main.c:236: dbglink_writestr("INVALID\n");
      005A00 90 81 BB         [24] 3549 	mov	dptr,#___str_4
      005A03 75 F0 80         [24] 3550 	mov	b,#0x80
      005A06 12 73 05         [24] 3551 	lcall	_dbglink_writestr
      005A09                       3552 00118$:
                           000257  3553 	C$main.c$245$4$381 ==.
                                   3554 ;	main.c:245: int32_t foffs = axradio_get_freqoffset();
      005A09 12 3A 45         [24] 3555 	lcall	_axradio_get_freqoffset
      005A0C AC 82            [24] 3556 	mov	r4,dpl
      005A0E AD 83            [24] 3557 	mov	r5,dph
      005A10 AE F0            [24] 3558 	mov	r6,b
      005A12 FF               [12] 3559 	mov	r7,a
                           000261  3560 	C$main.c$254$4$381 ==.
                                   3561 ;	main.c:254: foffs -= (st->u.rx.phy.offset)>>(FREQOFFS_K); /*adjust RX frequency by low-pass filtered frequency offset */
      005A13 74 06            [12] 3562 	mov	a,#0x06
      005A15 25 51            [12] 3563 	add	a,_axradio_statuschange_st_1_370
      005A17 FA               [12] 3564 	mov	r2,a
      005A18 E4               [12] 3565 	clr	a
      005A19 35 52            [12] 3566 	addc	a,(_axradio_statuschange_st_1_370 + 1)
      005A1B FB               [12] 3567 	mov	r3,a
      005A1C 8A 82            [24] 3568 	mov	dpl,r2
      005A1E 8B 83            [24] 3569 	mov	dph,r3
      005A20 A3               [24] 3570 	inc	dptr
      005A21 A3               [24] 3571 	inc	dptr
      005A22 E0               [24] 3572 	movx	a,@dptr
      005A23 F8               [12] 3573 	mov	r0,a
      005A24 A3               [24] 3574 	inc	dptr
      005A25 E0               [24] 3575 	movx	a,@dptr
      005A26 F9               [12] 3576 	mov	r1,a
      005A27 A3               [24] 3577 	inc	dptr
      005A28 E0               [24] 3578 	movx	a,@dptr
      005A29 FA               [12] 3579 	mov	r2,a
      005A2A A3               [24] 3580 	inc	dptr
      005A2B E0               [24] 3581 	movx	a,@dptr
      005A2C FB               [12] 3582 	mov	r3,a
      005A2D E9               [12] 3583 	mov	a,r1
      005A2E C4               [12] 3584 	swap	a
      005A2F 23               [12] 3585 	rl	a
      005A30 C8               [12] 3586 	xch	a,r0
      005A31 C4               [12] 3587 	swap	a
      005A32 23               [12] 3588 	rl	a
      005A33 54 1F            [12] 3589 	anl	a,#0x1f
      005A35 68               [12] 3590 	xrl	a,r0
      005A36 C8               [12] 3591 	xch	a,r0
      005A37 54 1F            [12] 3592 	anl	a,#0x1f
      005A39 C8               [12] 3593 	xch	a,r0
      005A3A 68               [12] 3594 	xrl	a,r0
      005A3B C8               [12] 3595 	xch	a,r0
      005A3C F9               [12] 3596 	mov	r1,a
      005A3D EA               [12] 3597 	mov	a,r2
      005A3E C4               [12] 3598 	swap	a
      005A3F 23               [12] 3599 	rl	a
      005A40 54 E0            [12] 3600 	anl	a,#0xe0
      005A42 49               [12] 3601 	orl	a,r1
      005A43 F9               [12] 3602 	mov	r1,a
      005A44 EB               [12] 3603 	mov	a,r3
      005A45 C4               [12] 3604 	swap	a
      005A46 23               [12] 3605 	rl	a
      005A47 CA               [12] 3606 	xch	a,r2
      005A48 C4               [12] 3607 	swap	a
      005A49 23               [12] 3608 	rl	a
      005A4A 54 1F            [12] 3609 	anl	a,#0x1f
      005A4C 6A               [12] 3610 	xrl	a,r2
      005A4D CA               [12] 3611 	xch	a,r2
      005A4E 54 1F            [12] 3612 	anl	a,#0x1f
      005A50 CA               [12] 3613 	xch	a,r2
      005A51 6A               [12] 3614 	xrl	a,r2
      005A52 CA               [12] 3615 	xch	a,r2
      005A53 30 E4 02         [24] 3616 	jnb	acc.4,00174$
      005A56 44 E0            [12] 3617 	orl	a,#0xe0
      005A58                       3618 00174$:
      005A58 FB               [12] 3619 	mov	r3,a
      005A59 EC               [12] 3620 	mov	a,r4
      005A5A C3               [12] 3621 	clr	c
      005A5B 98               [12] 3622 	subb	a,r0
      005A5C FC               [12] 3623 	mov	r4,a
      005A5D ED               [12] 3624 	mov	a,r5
      005A5E 99               [12] 3625 	subb	a,r1
      005A5F FD               [12] 3626 	mov	r5,a
      005A60 EE               [12] 3627 	mov	a,r6
      005A61 9A               [12] 3628 	subb	a,r2
      005A62 FE               [12] 3629 	mov	r6,a
      005A63 EF               [12] 3630 	mov	a,r7
      005A64 9B               [12] 3631 	subb	a,r3
                           0002B3  3632 	C$main.c$266$4$381 ==.
                                   3633 ;	main.c:266: if ((axradio_set_freqoffset(foffs) != AXRADIO_ERR_NOERROR) || (axradio_conv_freq_tohz(axradio_get_freqoffset()) < -1000))
      005A65 8C 82            [24] 3634 	mov	dpl,r4
      005A67 8D 83            [24] 3635 	mov	dph,r5
      005A69 8E F0            [24] 3636 	mov	b,r6
      005A6B 12 3A 29         [24] 3637 	lcall	_axradio_set_freqoffset
      005A6E E5 82            [12] 3638 	mov	a,dpl
      005A70 70 1E            [24] 3639 	jnz	00119$
      005A72 12 3A 45         [24] 3640 	lcall	_axradio_get_freqoffset
      005A75 12 07 99         [24] 3641 	lcall	_axradio_conv_freq_tohz
      005A78 AC 82            [24] 3642 	mov	r4,dpl
      005A7A AD 83            [24] 3643 	mov	r5,dph
      005A7C AE F0            [24] 3644 	mov	r6,b
      005A7E FF               [12] 3645 	mov	r7,a
      005A7F C3               [12] 3646 	clr	c
      005A80 EC               [12] 3647 	mov	a,r4
      005A81 94 18            [12] 3648 	subb	a,#0x18
      005A83 ED               [12] 3649 	mov	a,r5
      005A84 94 FC            [12] 3650 	subb	a,#0xfc
      005A86 EE               [12] 3651 	mov	a,r6
      005A87 94 FF            [12] 3652 	subb	a,#0xff
      005A89 EF               [12] 3653 	mov	a,r7
      005A8A 64 80            [12] 3654 	xrl	a,#0x80
      005A8C 94 7F            [12] 3655 	subb	a,#0x7f
      005A8E 50 36            [24] 3656 	jnc	00126$
      005A90                       3657 00119$:
                           0002DE  3658 	C$main.c$272$5$382 ==.
                                   3659 ;	main.c:272: axradio_set_freqoffset(0);
      005A90 90 00 00         [24] 3660 	mov	dptr,#(0x00&0x00ff)
      005A93 E4               [12] 3661 	clr	a
      005A94 F5 F0            [12] 3662 	mov	b,a
      005A96 12 3A 29         [24] 3663 	lcall	_axradio_set_freqoffset
                           0002E7  3664 	C$main.c$274$5$382 ==.
                                   3665 ;	main.c:274: dbglink_writestr("0_ADJUST:");
      005A99 90 81 C4         [24] 3666 	mov	dptr,#___str_5
      005A9C 75 F0 80         [24] 3667 	mov	b,#0x80
      005A9F 12 73 05         [24] 3668 	lcall	_dbglink_writestr
                           0002F0  3669 	C$main.c$275$5$382 ==.
                                   3670 ;	main.c:275: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      005AA2 12 7D AD         [24] 3671 	lcall	_wtimer0_curtime
      005AA5 AC 82            [24] 3672 	mov	r4,dpl
      005AA7 AD 83            [24] 3673 	mov	r5,dph
      005AA9 AE F0            [24] 3674 	mov	r6,b
      005AAB FF               [12] 3675 	mov	r7,a
      005AAC 74 08            [12] 3676 	mov	a,#0x08
      005AAE C0 E0            [24] 3677 	push	acc
      005AB0 C0 E0            [24] 3678 	push	acc
      005AB2 8C 82            [24] 3679 	mov	dpl,r4
      005AB4 8D 83            [24] 3680 	mov	dph,r5
      005AB6 8E F0            [24] 3681 	mov	b,r6
      005AB8 EF               [12] 3682 	mov	a,r7
      005AB9 12 75 88         [24] 3683 	lcall	_dbglink_writehex32
      005ABC 15 81            [12] 3684 	dec	sp
      005ABE 15 81            [12] 3685 	dec	sp
                           00030E  3686 	C$main.c$276$5$382 ==.
                                   3687 ;	main.c:276: dbglink_tx('\n');
      005AC0 75 82 0A         [24] 3688 	mov	dpl,#0x0a
      005AC3 12 61 21         [24] 3689 	lcall	_dbglink_tx
                           000314  3690 	C$main.c$295$1$371 ==.
                                   3691 ;	main.c:295: }
      005AC6                       3692 00126$:
                           000314  3693 	C$main.c$296$1$371 ==.
                           000314  3694 	XG$axradio_statuschange$0$0 ==.
      005AC6 22               [24] 3695 	ret
                                   3696 ;------------------------------------------------------------
                                   3697 ;Allocation info for local variables in function 'enable_radio_interrupt_in_mcu_pin'
                                   3698 ;------------------------------------------------------------
                           000315  3699 	G$enable_radio_interrupt_in_mcu_pin$0$0 ==.
                           000315  3700 	C$main.c$298$1$371 ==.
                                   3701 ;	main.c:298: void enable_radio_interrupt_in_mcu_pin(void)
                                   3702 ;	-----------------------------------------
                                   3703 ;	 function enable_radio_interrupt_in_mcu_pin
                                   3704 ;	-----------------------------------------
      005AC7                       3705 _enable_radio_interrupt_in_mcu_pin:
                           000315  3706 	C$main.c$300$1$384 ==.
                                   3707 ;	main.c:300: IE_4 = 1;
      005AC7 D2 AC            [12] 3708 	setb	_IE_4
                           000317  3709 	C$main.c$301$1$384 ==.
                           000317  3710 	XG$enable_radio_interrupt_in_mcu_pin$0$0 ==.
      005AC9 22               [24] 3711 	ret
                                   3712 ;------------------------------------------------------------
                                   3713 ;Allocation info for local variables in function 'disable_radio_interrupt_in_mcu_pin'
                                   3714 ;------------------------------------------------------------
                           000318  3715 	G$disable_radio_interrupt_in_mcu_pin$0$0 ==.
                           000318  3716 	C$main.c$303$1$384 ==.
                                   3717 ;	main.c:303: void disable_radio_interrupt_in_mcu_pin(void)
                                   3718 ;	-----------------------------------------
                                   3719 ;	 function disable_radio_interrupt_in_mcu_pin
                                   3720 ;	-----------------------------------------
      005ACA                       3721 _disable_radio_interrupt_in_mcu_pin:
                           000318  3722 	C$main.c$305$1$386 ==.
                                   3723 ;	main.c:305: IE_4 = 0;
      005ACA C2 AC            [12] 3724 	clr	_IE_4
                           00031A  3725 	C$main.c$306$1$386 ==.
                           00031A  3726 	XG$disable_radio_interrupt_in_mcu_pin$0$0 ==.
      005ACC 22               [24] 3727 	ret
                                   3728 ;------------------------------------------------------------
                                   3729 ;Allocation info for local variables in function '_sdcc_external_startup'
                                   3730 ;------------------------------------------------------------
                                   3731 ;c                         Allocated to registers 
                                   3732 ;p                         Allocated to registers 
                                   3733 ;c                         Allocated to registers 
                                   3734 ;p                         Allocated to registers 
                                   3735 ;------------------------------------------------------------
                           00031B  3736 	G$_sdcc_external_startup$0$0 ==.
                           00031B  3737 	C$main.c$309$1$386 ==.
                                   3738 ;	main.c:309: uint8_t _sdcc_external_startup(void)
                                   3739 ;	-----------------------------------------
                                   3740 ;	 function _sdcc_external_startup
                                   3741 ;	-----------------------------------------
      005ACD                       3742 __sdcc_external_startup:
                           00031B  3743 	C$main.c$311$1$388 ==.
                                   3744 ;	main.c:311: LPXOSCGM = 0x8A;
      005ACD 90 70 54         [24] 3745 	mov	dptr,#_LPXOSCGM
      005AD0 74 8A            [12] 3746 	mov	a,#0x8a
      005AD2 F0               [24] 3747 	movx	@dptr,a
                           000321  3748 	C$main.c$312$2$389 ==.
                                   3749 ;	main.c:312: wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
      005AD3 75 82 09         [24] 3750 	mov	dpl,#0x09
      005AD6 12 66 24         [24] 3751 	lcall	_wtimer0_setconfig
                           000327  3752 	C$main.c$313$2$390 ==.
                                   3753 ;	main.c:313: wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
      005AD9 75 82 38         [24] 3754 	mov	dpl,#0x38
      005ADC 12 66 71         [24] 3755 	lcall	_wtimer1_setconfig
                           00032D  3756 	C$main.c$316$1$388 ==.
                                   3757 ;	main.c:316: coldstart = !(PCON & 0x40);
      005ADF E5 87            [12] 3758 	mov	a,_PCON
      005AE1 A2 E6            [12] 3759 	mov	c,acc[6]
      005AE3 B3               [12] 3760 	cpl	c
      005AE4 92 01            [24] 3761 	mov	__sdcc_external_startup_sloc0_1_0,c
      005AE6 E4               [12] 3762 	clr	a
      005AE7 33               [12] 3763 	rlc	a
      005AE8 F5 50            [12] 3764 	mov	_coldstart,a
                           000338  3765 	C$main.c$318$1$388 ==.
                                   3766 ;	main.c:318: ANALOGA = 0x18; /* PA[3,4] LPXOSC, other PA are used as digital pins */
      005AEA 90 70 07         [24] 3767 	mov	dptr,#_ANALOGA
      005AED 74 18            [12] 3768 	mov	a,#0x18
      005AEF F0               [24] 3769 	movx	@dptr,a
                           00033E  3770 	C$main.c$319$1$388 ==.
                                   3771 ;	main.c:319: PORTA = 0x3F;
      005AF0 75 80 3F         [24] 3772 	mov	_PORTA,#0x3f
                           000341  3773 	C$main.c$320$1$388 ==.
                                   3774 ;	main.c:320: PORTB = 0x37;
      005AF3 75 88 37         [24] 3775 	mov	_PORTB,#0x37
                           000344  3776 	C$main.c$321$1$388 ==.
                                   3777 ;	main.c:321: PORTC = 0x00;
      005AF6 75 90 00         [24] 3778 	mov	_PORTC,#0x00
                           000347  3779 	C$main.c$322$1$388 ==.
                                   3780 ;	main.c:322: PORTR = 0x0B;
      005AF9 75 8C 0B         [24] 3781 	mov	_PORTR,#0x0b
                           00034A  3782 	C$main.c$324$1$388 ==.
                                   3783 ;	main.c:324: DIRA = 0x3F;
      005AFC 75 89 3F         [24] 3784 	mov	_DIRA,#0x3f
                           00034D  3785 	C$main.c$325$1$388 ==.
                                   3786 ;	main.c:325: DIRB = 0x0D;
      005AFF 75 8A 0D         [24] 3787 	mov	_DIRB,#0x0d
                           000350  3788 	C$main.c$326$1$388 ==.
                                   3789 ;	main.c:326: DIRC = 0x10;
      005B02 75 8B 10         [24] 3790 	mov	_DIRC,#0x10
                           000353  3791 	C$main.c$327$1$388 ==.
                                   3792 ;	main.c:327: DIRR = 0x15;
      005B05 75 8E 15         [24] 3793 	mov	_DIRR,#0x15
                           000356  3794 	C$main.c$329$1$388 ==.
                                   3795 ;	main.c:329: PALTA = 0x00;
      005B08 90 70 08         [24] 3796 	mov	dptr,#_PALTA
      005B0B E4               [12] 3797 	clr	a
      005B0C F0               [24] 3798 	movx	@dptr,a
                           00035B  3799 	C$main.c$330$1$388 ==.
                                   3800 ;	main.c:330: PALTB = 0x11;
      005B0D 90 70 09         [24] 3801 	mov	dptr,#_PALTB
      005B10 74 11            [12] 3802 	mov	a,#0x11
      005B12 F0               [24] 3803 	movx	@dptr,a
                           000361  3804 	C$main.c$331$1$388 ==.
                                   3805 ;	main.c:331: PALTC = 0x0F;
      005B13 90 70 0A         [24] 3806 	mov	dptr,#_PALTC
      005B16 74 0F            [12] 3807 	mov	a,#0x0f
      005B18 F0               [24] 3808 	movx	@dptr,a
                           000367  3809 	C$main.c$333$1$388 ==.
                                   3810 ;	main.c:333: PINSEL |= 0xC0;
      005B19 90 70 0B         [24] 3811 	mov	dptr,#_PINSEL
      005B1C E0               [24] 3812 	movx	a,@dptr
      005B1D FF               [12] 3813 	mov	r7,a
      005B1E 74 C0            [12] 3814 	mov	a,#0xc0
      005B20 4F               [12] 3815 	orl	a,r7
      005B21 F0               [24] 3816 	movx	@dptr,a
                           000370  3817 	C$main.c$335$1$388 ==.
                                   3818 ;	main.c:335: axradio_setup_pincfg1();
      005B22 12 06 DB         [24] 3819 	lcall	_axradio_setup_pincfg1
                           000373  3820 	C$main.c$336$1$388 ==.
                                   3821 ;	main.c:336: DPS = 0;
      005B25 75 86 00         [24] 3822 	mov	_DPS,#0x00
                           000376  3823 	C$main.c$337$1$388 ==.
                                   3824 ;	main.c:337: IE = 0x40;
      005B28 75 A8 40         [24] 3825 	mov	_IE,#0x40
                           000379  3826 	C$main.c$338$1$388 ==.
                                   3827 ;	main.c:338: EIE = 0x00;
      005B2B 75 98 00         [24] 3828 	mov	_EIE,#0x00
                           00037C  3829 	C$main.c$339$1$388 ==.
                                   3830 ;	main.c:339: E2IE = 0x00;
      005B2E 75 A0 00         [24] 3831 	mov	_E2IE,#0x00
                           00037F  3832 	C$main.c$342$1$388 ==.
                                   3833 ;	main.c:342: GPIOENABLE = 1; /* unfreeze GPIO */
      005B31 90 70 0C         [24] 3834 	mov	dptr,#_GPIOENABLE
      005B34 74 01            [12] 3835 	mov	a,#0x01
      005B36 F0               [24] 3836 	movx	@dptr,a
                           000385  3837 	C$main.c$345$1$388 ==.
                                   3838 ;	main.c:345: RNGMODE = 0x0F;
      005B37 90 70 80         [24] 3839 	mov	dptr,#_RNGMODE
      005B3A 74 0F            [12] 3840 	mov	a,#0x0f
      005B3C F0               [24] 3841 	movx	@dptr,a
                           00038B  3842 	C$main.c$346$1$388 ==.
                                   3843 ;	main.c:346: RNGCLKSRC0 = 0x09;
      005B3D 90 70 82         [24] 3844 	mov	dptr,#_RNGCLKSRC0
      005B40 74 09            [12] 3845 	mov	a,#0x09
      005B42 F0               [24] 3846 	movx	@dptr,a
                           000391  3847 	C$main.c$347$1$388 ==.
                                   3848 ;	main.c:347: RNGCLKSRC1 = 0x00;
      005B43 90 70 83         [24] 3849 	mov	dptr,#_RNGCLKSRC1
      005B46 E4               [12] 3850 	clr	a
      005B47 F0               [24] 3851 	movx	@dptr,a
                           000396  3852 	C$main.c$351$1$388 ==.
                                   3853 ;	main.c:351: return !coldstart; /* coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization */
      005B48 E5 50            [12] 3854 	mov	a,_coldstart
      005B4A B4 01 00         [24] 3855 	cjne	a,#0x01,00109$
      005B4D                       3856 00109$:
      005B4D 92 01            [24] 3857 	mov  __sdcc_external_startup_sloc0_1_0,c
      005B4F E4               [12] 3858 	clr	a
      005B50 33               [12] 3859 	rlc	a
      005B51 F5 82            [12] 3860 	mov	dpl,a
                           0003A1  3861 	C$main.c$352$1$388 ==.
                           0003A1  3862 	XG$_sdcc_external_startup$0$0 ==.
      005B53 22               [24] 3863 	ret
                                   3864 ;------------------------------------------------------------
                                   3865 ;Allocation info for local variables in function 'main'
                                   3866 ;------------------------------------------------------------
                                   3867 ;i                         Allocated to registers r7 
                                   3868 ;x                         Allocated to registers r6 
                                   3869 ;flg                       Allocated to registers r6 
                                   3870 ;flg                       Allocated to registers r7 
                                   3871 ;------------------------------------------------------------
                           0003A2  3872 	G$main$0$0 ==.
                           0003A2  3873 	C$main.c$354$1$388 ==.
                                   3874 ;	main.c:354: int main(void)
                                   3875 ;	-----------------------------------------
                                   3876 ;	 function main
                                   3877 ;	-----------------------------------------
      005B54                       3878 _main:
                           0003A2  3879 	C$main.c$360$1$392 ==.
                                   3880 ;	main.c:360: __endasm;
                           000000  3881 	G$_start__stack$0$0	= __start__stack
                                   3882 	.globl	G$_start__stack$0$0
                           0003A2  3883 	C$main.c$362$1$392 ==.
                                   3884 ;	main.c:362: dbglink_init();
      005B54 12 6B B5         [24] 3885 	lcall	_dbglink_init
                           0003A5  3886 	C$libmftypes.h$368$4$408 ==.
                                   3887 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
      005B57 D2 AF            [12] 3888 	setb	_EA
                           0003A7  3889 	C$main.c$365$1$392 ==.
                                   3890 ;	main.c:365: flash_apply_calibration();
      005B59 12 71 49         [24] 3891 	lcall	_flash_apply_calibration
                           0003AA  3892 	C$main.c$366$1$392 ==.
                                   3893 ;	main.c:366: CLKCON = 0x00;
      005B5C 75 C6 00         [24] 3894 	mov	_CLKCON,#0x00
                           0003AD  3895 	C$main.c$367$1$392 ==.
                                   3896 ;	main.c:367: wtimer_init();
      005B5F 12 67 20         [24] 3897 	lcall	_wtimer_init
                           0003B0  3898 	C$main.c$368$1$392 ==.
                                   3899 ;	main.c:368: GOLDSEQUENCE_Init();
      005B62 12 3F 7D         [24] 3900 	lcall	_GOLDSEQUENCE_Init
                           0003B3  3901 	C$main.c$370$1$392 ==.
                                   3902 ;	main.c:370: if (coldstart)
      005B65 E5 50            [12] 3903 	mov	a,_coldstart
      005B67 70 03            [24] 3904 	jnz	00214$
      005B69 02 5C 38         [24] 3905 	ljmp	00118$
      005B6C                       3906 00214$:
                           0003BA  3907 	C$main.c$372$2$393 ==.
                                   3908 ;	main.c:372: display_init();
      005B6C 12 0E 11         [24] 3909 	lcall	_com0_init
                           0003BD  3910 	C$main.c$373$2$393 ==.
                                   3911 ;	main.c:373: display_setpos(0);
      005B6F 75 82 00         [24] 3912 	mov	dpl,#0x00
      005B72 12 0E B9         [24] 3913 	lcall	_com0_setpos
                           0003C3  3914 	C$main.c$375$2$393 ==.
                                   3915 ;	main.c:375: i = axradio_init();
      005B75 12 2F A3         [24] 3916 	lcall	_axradio_init
                           0003C6  3917 	C$main.c$377$2$393 ==.
                                   3918 ;	main.c:377: if (i != AXRADIO_ERR_NOERROR)
      005B78 E5 82            [12] 3919 	mov	a,dpl
      005B7A FF               [12] 3920 	mov	r7,a
      005B7B 60 25            [24] 3921 	jz	00106$
                           0003CB  3922 	C$main.c$379$3$394 ==.
                                   3923 ;	main.c:379: if (i == AXRADIO_ERR_NOCHIP)
      005B7D BF 05 02         [24] 3924 	cjne	r7,#0x05,00216$
      005B80 80 03            [24] 3925 	sjmp	00217$
      005B82                       3926 00216$:
      005B82 02 5D 46         [24] 3927 	ljmp	00135$
      005B85                       3928 00217$:
                           0003D3  3929 	C$main.c$381$4$395 ==.
                                   3930 ;	main.c:381: display_writestr(radio_not_found_lcd_display);
      005B85 90 09 9A         [24] 3931 	mov	dptr,#_radio_not_found_lcd_display
      005B88 75 F0 00         [24] 3932 	mov	b,#0x00
      005B8B 12 0E D5         [24] 3933 	lcall	_com0_writestr
                           0003DC  3934 	C$main.c$384$4$395 ==.
                                   3935 ;	main.c:384: if(DBGLNKSTAT & 0x10)
      005B8E E5 E2            [12] 3936 	mov	a,_DBGLNKSTAT
      005B90 20 E4 03         [24] 3937 	jb	acc.4,00218$
      005B93 02 5D 54         [24] 3938 	ljmp	00147$
      005B96                       3939 00218$:
                           0003E4  3940 	C$main.c$385$4$395 ==.
                                   3941 ;	main.c:385: dbglink_writestr(radio_not_found_lcd_display);
      005B96 90 09 9A         [24] 3942 	mov	dptr,#_radio_not_found_lcd_display
      005B99 75 F0 00         [24] 3943 	mov	b,#0x00
      005B9C 12 73 05         [24] 3944 	lcall	_dbglink_writestr
                           0003ED  3945 	C$main.c$388$4$395 ==.
                                   3946 ;	main.c:388: goto terminate_error;
      005B9F 02 5D 54         [24] 3947 	ljmp	00147$
                           0003F0  3948 	C$main.c$391$2$393 ==.
                                   3949 ;	main.c:391: goto terminate_radio_error;
      005BA2                       3950 00106$:
                           0003F0  3951 	C$main.c$394$2$393 ==.
                                   3952 ;	main.c:394: display_writestr(radio_lcd_display);
      005BA2 90 09 8C         [24] 3953 	mov	dptr,#_radio_lcd_display
      005BA5 75 F0 00         [24] 3954 	mov	b,#0x00
      005BA8 12 0E D5         [24] 3955 	lcall	_com0_writestr
                           0003F9  3956 	C$main.c$397$2$393 ==.
                                   3957 ;	main.c:397: if (DBGLNKSTAT & 0x10)
      005BAB E5 E2            [12] 3958 	mov	a,_DBGLNKSTAT
      005BAD 30 E4 09         [24] 3959 	jnb	acc.4,00108$
                           0003FE  3960 	C$main.c$398$2$393 ==.
                                   3961 ;	main.c:398: dbglink_writestr(radio_lcd_display);
      005BB0 90 09 8C         [24] 3962 	mov	dptr,#_radio_lcd_display
      005BB3 75 F0 00         [24] 3963 	mov	b,#0x00
      005BB6 12 73 05         [24] 3964 	lcall	_dbglink_writestr
      005BB9                       3965 00108$:
                           000407  3966 	C$main.c$401$2$393 ==.
                                   3967 ;	main.c:401: axradio_set_local_address(&localaddr);
      005BB9 90 7F 74         [24] 3968 	mov	dptr,#_localaddr
      005BBC 75 F0 80         [24] 3969 	mov	b,#0x80
      005BBF 12 3A 59         [24] 3970 	lcall	_axradio_set_local_address
                           000410  3971 	C$main.c$402$2$393 ==.
                                   3972 ;	main.c:402: axradio_set_default_remote_address(&remoteaddr);
      005BC2 90 7F 6F         [24] 3973 	mov	dptr,#_remoteaddr
      005BC5 75 F0 80         [24] 3974 	mov	b,#0x80
      005BC8 12 3A 97         [24] 3975 	lcall	_axradio_set_default_remote_address
                           000419  3976 	C$main.c$406$2$393 ==.
                                   3977 ;	main.c:406: if (DBGLNKSTAT & 0x10)
      005BCB E5 E2            [12] 3978 	mov	a,_DBGLNKSTAT
      005BCD 30 E4 4C         [24] 3979 	jnb	acc.4,00112$
                           00041E  3980 	C$main.c$408$3$396 ==.
                                   3981 ;	main.c:408: dbglink_writestr("RNG = ");
      005BD0 90 81 CE         [24] 3982 	mov	dptr,#___str_6
      005BD3 75 F0 80         [24] 3983 	mov	b,#0x80
      005BD6 12 73 05         [24] 3984 	lcall	_dbglink_writestr
                           000427  3985 	C$main.c$409$3$396 ==.
                                   3986 ;	main.c:409: dbglink_writenum16(axradio_get_pllrange(), 2, 0);
      005BD9 12 38 EB         [24] 3987 	lcall	_axradio_get_pllrange
      005BDC E4               [12] 3988 	clr	a
      005BDD C0 E0            [24] 3989 	push	acc
      005BDF 74 02            [12] 3990 	mov	a,#0x02
      005BE1 C0 E0            [24] 3991 	push	acc
      005BE3 12 7C 71         [24] 3992 	lcall	_dbglink_writenum16
      005BE6 15 81            [12] 3993 	dec	sp
      005BE8 15 81            [12] 3994 	dec	sp
                           000438  3995 	C$main.c$411$4$397 ==.
                                   3996 ;	main.c:411: uint8_t x = axradio_get_pllvcoi();
      005BEA 12 39 0A         [24] 3997 	lcall	_axradio_get_pllvcoi
                           00043B  3998 	C$main.c$413$4$397 ==.
                                   3999 ;	main.c:413: if (x & 0x80)
      005BED E5 82            [12] 4000 	mov	a,dpl
      005BEF FE               [12] 4001 	mov	r6,a
      005BF0 30 E7 20         [24] 4002 	jnb	acc.7,00110$
                           000441  4003 	C$main.c$415$5$398 ==.
                                   4004 ;	main.c:415: dbglink_writestr("\nVCOI = ");
      005BF3 90 81 D5         [24] 4005 	mov	dptr,#___str_7
      005BF6 75 F0 80         [24] 4006 	mov	b,#0x80
      005BF9 C0 06            [24] 4007 	push	ar6
      005BFB 12 73 05         [24] 4008 	lcall	_dbglink_writestr
      005BFE D0 06            [24] 4009 	pop	ar6
                           00044E  4010 	C$main.c$416$5$398 ==.
                                   4011 ;	main.c:416: dbglink_writehex16(x, 2, 0);
      005C00 E4               [12] 4012 	clr	a
      005C01 FD               [12] 4013 	mov	r5,a
      005C02 C0 E0            [24] 4014 	push	acc
      005C04 74 02            [12] 4015 	mov	a,#0x02
      005C06 C0 E0            [24] 4016 	push	acc
      005C08 8E 82            [24] 4017 	mov	dpl,r6
      005C0A 8D 83            [24] 4018 	mov	dph,r5
      005C0C 12 77 A6         [24] 4019 	lcall	_dbglink_writehex16
      005C0F 15 81            [12] 4020 	dec	sp
      005C11 15 81            [12] 4021 	dec	sp
      005C13                       4022 00110$:
                           000461  4023 	C$main.c$419$3$396 ==.
                                   4024 ;	main.c:419: dbglink_writestr("\nSLAVE\n");
      005C13 90 81 DE         [24] 4025 	mov	dptr,#___str_8
      005C16 75 F0 80         [24] 4026 	mov	b,#0x80
      005C19 12 73 05         [24] 4027 	lcall	_dbglink_writestr
      005C1C                       4028 00112$:
                           00046A  4029 	C$main.c$426$2$393 ==.
                                   4030 ;	main.c:426: if (DBGLNKSTAT & 0x10)
      005C1C E5 E2            [12] 4031 	mov	a,_DBGLNKSTAT
      005C1E 30 E4 09         [24] 4032 	jnb	acc.4,00114$
                           00046F  4033 	C$main.c$427$2$393 ==.
                                   4034 ;	main.c:427: dbglink_writestr("SLAVE  RX CONT\n");
      005C21 90 81 E6         [24] 4035 	mov	dptr,#___str_9
      005C24 75 F0 80         [24] 4036 	mov	b,#0x80
      005C27 12 73 05         [24] 4037 	lcall	_dbglink_writestr
      005C2A                       4038 00114$:
                           000478  4039 	C$main.c$430$2$393 ==.
                                   4040 ;	main.c:430: i = axradio_set_mode(RADIO_MODE);
      005C2A 75 82 20         [24] 4041 	mov	dpl,#0x20
      005C2D 12 33 A7         [24] 4042 	lcall	_axradio_set_mode
                           00047E  4043 	C$main.c$432$2$393 ==.
                                   4044 ;	main.c:432: if (i != AXRADIO_ERR_NOERROR)
      005C30 E5 82            [12] 4045 	mov	a,dpl
      005C32 FF               [12] 4046 	mov	r7,a
      005C33 60 08            [24] 4047 	jz	00119$
                           000483  4048 	C$main.c$433$2$393 ==.
                                   4049 ;	main.c:433: goto terminate_radio_error;
      005C35 02 5D 46         [24] 4050 	ljmp	00135$
      005C38                       4051 00118$:
                           000486  4052 	C$main.c$442$2$399 ==.
                                   4053 ;	main.c:442: axradio_commsleepexit();
      005C38 12 3F 5A         [24] 4054 	lcall	_axradio_commsleepexit
                           000489  4055 	C$main.c$443$2$399 ==.
                                   4056 ;	main.c:443: IE_4 = 1; /* enable radio interrupt */
      005C3B D2 AC            [12] 4057 	setb	_IE_4
      005C3D                       4058 00119$:
                           00048B  4059 	C$main.c$446$1$392 ==.
                                   4060 ;	main.c:446: axradio_setup_pincfg2();
      005C3D 12 06 E2         [24] 4061 	lcall	_axradio_setup_pincfg2
                           00048E  4062 	C$main.c$449$1$392 ==.
                                   4063 ;	main.c:449: PALTB |= 0x11;
      005C40 90 70 09         [24] 4064 	mov	dptr,#_PALTB
      005C43 E0               [24] 4065 	movx	a,@dptr
      005C44 FE               [12] 4066 	mov	r6,a
      005C45 74 11            [12] 4067 	mov	a,#0x11
      005C47 4E               [12] 4068 	orl	a,r6
      005C48 F0               [24] 4069 	movx	@dptr,a
                           000497  4070 	C$main.c$450$1$392 ==.
                                   4071 ;	main.c:450: DIRB |= (1<<0) | (1<<4);
      005C49 43 8A 11         [24] 4072 	orl	_DIRB,#0x11
                           00049A  4073 	C$main.c$451$1$392 ==.
                                   4074 ;	main.c:451: DIRB &= (uint8_t)~(1<<5);
      005C4C 53 8A DF         [24] 4075 	anl	_DIRB,#0xdf
                           00049D  4076 	C$main.c$452$1$392 ==.
                                   4077 ;	main.c:452: DIRB &= (uint8_t)~(1<<1);
      005C4F 53 8A FD         [24] 4078 	anl	_DIRB,#0xfd
                           0004A0  4079 	C$main.c$453$1$392 ==.
                                   4080 ;	main.c:453: PINSEL &= (uint8_t)~((1<<0) | (1<<1));
      005C52 90 70 0B         [24] 4081 	mov	dptr,#_PINSEL
      005C55 E0               [24] 4082 	movx	a,@dptr
      005C56 FE               [12] 4083 	mov	r6,a
      005C57 74 FC            [12] 4084 	mov	a,#0xfc
      005C59 5E               [12] 4085 	anl	a,r6
      005C5A F0               [24] 4086 	movx	@dptr,a
                           0004A9  4087 	C$main.c$454$1$392 ==.
                                   4088 ;	main.c:454: uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
      005C5B E4               [12] 4089 	clr	a
      005C5C C0 E0            [24] 4090 	push	acc
      005C5E 74 2D            [12] 4091 	mov	a,#0x2d
      005C60 C0 E0            [24] 4092 	push	acc
      005C62 74 31            [12] 4093 	mov	a,#0x31
      005C64 C0 E0            [24] 4094 	push	acc
      005C66 74 01            [12] 4095 	mov	a,#0x01
      005C68 C0 E0            [24] 4096 	push	acc
      005C6A 03               [12] 4097 	rr	a
      005C6B C0 E0            [24] 4098 	push	acc
      005C6D 74 25            [12] 4099 	mov	a,#0x25
      005C6F C0 E0            [24] 4100 	push	acc
      005C71 E4               [12] 4101 	clr	a
      005C72 C0 E0            [24] 4102 	push	acc
      005C74 C0 E0            [24] 4103 	push	acc
      005C76 75 82 00         [24] 4104 	mov	dpl,#0x00
      005C79 12 64 9E         [24] 4105 	lcall	_uart_timer1_baud
      005C7C E5 81            [12] 4106 	mov	a,sp
      005C7E 24 F8            [12] 4107 	add	a,#0xf8
      005C80 F5 81            [12] 4108 	mov	sp,a
                           0004D0  4109 	C$main.c$455$1$392 ==.
                                   4110 ;	main.c:455: uart0_init(1,8,1);
      005C82 75 6E 08         [24] 4111 	mov	_uart0_init_PARM_2,#0x08
      005C85 75 6F 01         [24] 4112 	mov	_uart0_init_PARM_3,#0x01
      005C88 75 82 01         [24] 4113 	mov	dpl,#0x01
      005C8B 12 71 0D         [24] 4114 	lcall	_uart0_init
                           0004DC  4115 	C$main.c$458$1$392 ==.
                                   4116 ;	main.c:458: UBLOX_GPS_PortInit();
      005C8E 12 54 BA         [24] 4117 	lcall	_UBLOX_GPS_PortInit
      005C91                       4118 00145$:
                           0004DF  4119 	C$main.c$479$2$400 ==.
                                   4120 ;	main.c:479: if (ack_flg /*&& (pkts_received >0)*/)
      005C91 90 09 CC         [24] 4121 	mov	dptr,#_ack_flg
      005C94 E0               [24] 4122 	movx	a,@dptr
      005C95 70 03            [24] 4123 	jnz	00224$
      005C97 02 5D 1A         [24] 4124 	ljmp	00129$
      005C9A                       4125 00224$:
                           0004E8  4126 	C$main.c$481$3$401 ==.
                                   4127 ;	main.c:481: UBLOX_payload_POSLLH();
      005C9A 12 57 76         [24] 4128 	lcall	_UBLOX_payload_POSLLH
                           0004EB  4129 	C$main.c$483$3$401 ==.
                                   4130 ;	main.c:483: dbglink_writestr("1_GNSS_WT0:");
      005C9D 90 81 F6         [24] 4131 	mov	dptr,#___str_10
      005CA0 75 F0 80         [24] 4132 	mov	b,#0x80
      005CA3 12 73 05         [24] 4133 	lcall	_dbglink_writestr
                           0004F4  4134 	C$main.c$484$3$401 ==.
                                   4135 ;	main.c:484: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      005CA6 12 7D AD         [24] 4136 	lcall	_wtimer0_curtime
      005CA9 AB 82            [24] 4137 	mov	r3,dpl
      005CAB AC 83            [24] 4138 	mov	r4,dph
      005CAD AD F0            [24] 4139 	mov	r5,b
      005CAF FE               [12] 4140 	mov	r6,a
      005CB0 74 08            [12] 4141 	mov	a,#0x08
      005CB2 C0 E0            [24] 4142 	push	acc
      005CB4 C0 E0            [24] 4143 	push	acc
      005CB6 8B 82            [24] 4144 	mov	dpl,r3
      005CB8 8C 83            [24] 4145 	mov	dph,r4
      005CBA 8D F0            [24] 4146 	mov	b,r5
      005CBC EE               [12] 4147 	mov	a,r6
      005CBD 12 75 88         [24] 4148 	lcall	_dbglink_writehex32
      005CC0 15 81            [12] 4149 	dec	sp
      005CC2 15 81            [12] 4150 	dec	sp
                           000512  4151 	C$main.c$490$3$401 ==.
                                   4152 ;	main.c:490: dbglink_tx('\n');
      005CC4 75 82 0A         [24] 4153 	mov	dpl,#0x0a
      005CC7 12 61 21         [24] 4154 	lcall	_dbglink_tx
                           000518  4155 	C$main.c$492$3$401 ==.
                                   4156 ;	main.c:492: if (StdPrem == 0x1)
      005CCA 90 09 C5         [24] 4157 	mov	dptr,#_StdPrem
      005CCD E0               [24] 4158 	movx	a,@dptr
      005CCE FE               [12] 4159 	mov	r6,a
      005CCF BE 01 05         [24] 4160 	cjne	r6,#0x01,00126$
                           000520  4161 	C$main.c$493$3$401 ==.
                                   4162 ;	main.c:493: packetStdFraming();
      005CD2 12 49 0D         [24] 4163 	lcall	_packetStdFraming
      005CD5 80 11            [24] 4164 	sjmp	00127$
      005CD7                       4165 00126$:
                           000525  4166 	C$main.c$494$3$401 ==.
                                   4167 ;	main.c:494: else if (StdPrem == 0x0)
      005CD7 EE               [12] 4168 	mov	a,r6
      005CD8 70 05            [24] 4169 	jnz	00123$
                           000528  4170 	C$main.c$495$3$401 ==.
                                   4171 ;	main.c:495: packetPremFraming();
      005CDA 12 47 23         [24] 4172 	lcall	_packetPremFraming
      005CDD 80 09            [24] 4173 	sjmp	00127$
      005CDF                       4174 00123$:
                           00052D  4175 	C$main.c$497$3$401 ==.
                                   4176 ;	main.c:497: else if (StdPrem == 0x2)
      005CDF BE 02 06         [24] 4177 	cjne	r6,#0x02,00127$
                           000530  4178 	C$main.c$499$4$402 ==.
                                   4179 ;	main.c:499: packetPremFraming();
      005CE2 12 47 23         [24] 4180 	lcall	_packetPremFraming
                           000533  4181 	C$main.c$500$4$402 ==.
                                   4182 ;	main.c:500: packetStdFraming();
      005CE5 12 49 0D         [24] 4183 	lcall	_packetStdFraming
      005CE8                       4184 00127$:
                           000536  4185 	C$main.c$503$3$401 ==.
                                   4186 ;	main.c:503: dbglink_writestr("4_Framing_WT0:");
      005CE8 90 82 02         [24] 4187 	mov	dptr,#___str_11
      005CEB 75 F0 80         [24] 4188 	mov	b,#0x80
      005CEE 12 73 05         [24] 4189 	lcall	_dbglink_writestr
                           00053F  4190 	C$main.c$504$3$401 ==.
                                   4191 ;	main.c:504: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      005CF1 12 7D AD         [24] 4192 	lcall	_wtimer0_curtime
      005CF4 AB 82            [24] 4193 	mov	r3,dpl
      005CF6 AC 83            [24] 4194 	mov	r4,dph
      005CF8 AD F0            [24] 4195 	mov	r5,b
      005CFA FE               [12] 4196 	mov	r6,a
      005CFB 74 08            [12] 4197 	mov	a,#0x08
      005CFD C0 E0            [24] 4198 	push	acc
      005CFF C0 E0            [24] 4199 	push	acc
      005D01 8B 82            [24] 4200 	mov	dpl,r3
      005D03 8C 83            [24] 4201 	mov	dph,r4
      005D05 8D F0            [24] 4202 	mov	b,r5
      005D07 EE               [12] 4203 	mov	a,r6
      005D08 12 75 88         [24] 4204 	lcall	_dbglink_writehex32
      005D0B 15 81            [12] 4205 	dec	sp
      005D0D 15 81            [12] 4206 	dec	sp
                           00055D  4207 	C$main.c$505$3$401 ==.
                                   4208 ;	main.c:505: dbglink_tx('\n');
      005D0F 75 82 0A         [24] 4209 	mov	dpl,#0x0a
      005D12 12 61 21         [24] 4210 	lcall	_dbglink_tx
                           000563  4211 	C$main.c$507$3$401 ==.
                                   4212 ;	main.c:507: ack_flg = 0;
      005D15 90 09 CC         [24] 4213 	mov	dptr,#_ack_flg
      005D18 E4               [12] 4214 	clr	a
      005D19 F0               [24] 4215 	movx	@dptr,a
      005D1A                       4216 00129$:
                           000568  4217 	C$main.c$510$2$400 ==.
                                   4218 ;	main.c:510: wtimer_runcallbacks();
      005D1A 12 68 D8         [24] 4219 	lcall	_wtimer_runcallbacks
                           00056B  4220 	C$libmftypes.h$373$5$411 ==.
                                   4221 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:373: EA = 0;
      005D1D C2 AF            [12] 4222 	clr	_EA
                           00056D  4223 	C$main.c$513$3$400 ==.
                                   4224 ;	main.c:513: uint8_t flg = WTFLAG_CANSTANDBY;
      005D1F 7E 02            [12] 4225 	mov	r6,#0x02
                           00056F  4226 	C$main.c$516$3$403 ==.
                                   4227 ;	main.c:516: if (axradio_cansleep()
      005D21 C0 06            [24] 4228 	push	ar6
      005D23 12 33 95         [24] 4229 	lcall	_axradio_cansleep
      005D26 E5 82            [12] 4230 	mov	a,dpl
      005D28 D0 06            [24] 4231 	pop	ar6
      005D2A 60 10            [24] 4232 	jz	00131$
                           00057A  4233 	C$main.c$518$3$403 ==.
                                   4234 ;	main.c:518: && dbglink_txidle()
      005D2C 12 6B 97         [24] 4235 	lcall	_dbglink_txidle
      005D2F E5 82            [12] 4236 	mov	a,dpl
      005D31 60 09            [24] 4237 	jz	00131$
                           000581  4238 	C$main.c$520$3$403 ==.
                                   4239 ;	main.c:520: && display_txidle())
      005D33 12 70 DB         [24] 4240 	lcall	_uart0_txidle
      005D36 E5 82            [12] 4241 	mov	a,dpl
      005D38 60 02            [24] 4242 	jz	00131$
                           000588  4243 	C$main.c$521$3$403 ==.
                                   4244 ;	main.c:521: flg |= WTFLAG_CANSLEEP;
      005D3A 7E 03            [12] 4245 	mov	r6,#0x03
      005D3C                       4246 00131$:
                           00058A  4247 	C$main.c$523$3$403 ==.
                                   4248 ;	main.c:523: wtimer_idle(flg);
      005D3C 8E 82            [24] 4249 	mov	dpl,r6
      005D3E 12 68 54         [24] 4250 	lcall	_wtimer_idle
                           00058F  4251 	C$libmftypes.h$368$5$414 ==.
                                   4252 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
      005D41 D2 AF            [12] 4253 	setb	_EA
                           000591  4254 	C$main.c$525$4$413 ==.
                                   4255 ;	main.c:525: __enable_irq();
      005D43 02 5C 91         [24] 4256 	ljmp	00145$
                           000594  4257 	C$main.c$528$1$392 ==.
                                   4258 ;	main.c:528: terminate_radio_error:
      005D46                       4259 00135$:
                           000594  4260 	C$main.c$529$1$392 ==.
                                   4261 ;	main.c:529: display_radio_error(i);
      005D46 8F 82            [24] 4262 	mov	dpl,r7
      005D48 C0 07            [24] 4263 	push	ar7
      005D4A 12 43 92         [24] 4264 	lcall	_com0_display_radio_error
      005D4D D0 07            [24] 4265 	pop	ar7
                           00059D  4266 	C$main.c$531$1$392 ==.
                                   4267 ;	main.c:531: dbglink_display_radio_error(i);
      005D4F 8F 82            [24] 4268 	mov	dpl,r7
      005D51 12 43 E1         [24] 4269 	lcall	_dbglink_display_radio_error
                           0005A2  4270 	C$main.c$533$1$392 ==.
                                   4271 ;	main.c:533: terminate_error:
      005D54                       4272 00147$:
                           0005A2  4273 	C$main.c$537$2$404 ==.
                                   4274 ;	main.c:537: wtimer_runcallbacks();
      005D54 12 68 D8         [24] 4275 	lcall	_wtimer_runcallbacks
                           0005A5  4276 	C$main.c$539$3$404 ==.
                                   4277 ;	main.c:539: uint8_t flg = WTFLAG_CANSTANDBY;
      005D57 7F 02            [12] 4278 	mov	r7,#0x02
                           0005A7  4279 	C$main.c$542$3$405 ==.
                                   4280 ;	main.c:542: if (axradio_cansleep()
      005D59 C0 07            [24] 4281 	push	ar7
      005D5B 12 33 95         [24] 4282 	lcall	_axradio_cansleep
      005D5E E5 82            [12] 4283 	mov	a,dpl
      005D60 D0 07            [24] 4284 	pop	ar7
      005D62 60 10            [24] 4285 	jz	00138$
                           0005B2  4286 	C$main.c$544$3$405 ==.
                                   4287 ;	main.c:544: && dbglink_txidle()
      005D64 12 6B 97         [24] 4288 	lcall	_dbglink_txidle
      005D67 E5 82            [12] 4289 	mov	a,dpl
      005D69 60 09            [24] 4290 	jz	00138$
                           0005B9  4291 	C$main.c$546$3$405 ==.
                                   4292 ;	main.c:546: && display_txidle())
      005D6B 12 70 DB         [24] 4293 	lcall	_uart0_txidle
      005D6E E5 82            [12] 4294 	mov	a,dpl
      005D70 60 02            [24] 4295 	jz	00138$
                           0005C0  4296 	C$main.c$547$3$405 ==.
                                   4297 ;	main.c:547: flg |= WTFLAG_CANSLEEP;
      005D72 7F 03            [12] 4298 	mov	r7,#0x03
      005D74                       4299 00138$:
                           0005C2  4300 	C$main.c$549$3$405 ==.
                                   4301 ;	main.c:549: wtimer_idle(flg);
      005D74 8F 82            [24] 4302 	mov	dpl,r7
      005D76 12 68 54         [24] 4303 	lcall	_wtimer_idle
      005D79 80 D9            [24] 4304 	sjmp	00147$
                           0005C9  4305 	C$main.c$552$1$392 ==.
                           0005C9  4306 	XG$main$0$0 ==.
      005D7B 22               [24] 4307 	ret
                                   4308 	.area CSEG    (CODE)
                                   4309 	.area CONST   (CODE)
                           000000  4310 Fmain$__str_0$0$0 == .
      008183                       4311 ___str_0:
      008183 35 5F 52 58 5F 57 54  4312 	.ascii "5_RX_WT0:"
             30 3A
      00818C 00                    4313 	.db 0x00
                           00000A  4314 Fmain$__str_1$0$0 == .
      00818D                       4315 ___str_1:
      00818D 39 30 5F 41 43 4B 5F  4316 	.ascii "90_ACK_WT0:"
             57 54 30 3A
      008198 00                    4317 	.db 0x00
                           000016  4318 Fmain$__str_2$0$0 == .
      008199                       4319 ___str_2:
      008199 39 31 5F 41 43 4B 5F  4320 	.ascii "91_ACK_delta:"
             64 65 6C 74 61 3A
      0081A6 00                    4321 	.db 0x00
                           000024  4322 Fmain$__str_3$0$0 == .
      0081A7                       4323 ___str_3:
      0081A7 20 73 75 63 63 65 73  4324 	.ascii " successful pkts."
             73 66 75 6C 20 70 6B
             74 73 2E
      0081B8 0A                    4325 	.db 0x0a
      0081B9 0A                    4326 	.db 0x0a
      0081BA 00                    4327 	.db 0x00
                           000038  4328 Fmain$__str_4$0$0 == .
      0081BB                       4329 ___str_4:
      0081BB 49 4E 56 41 4C 49 44  4330 	.ascii "INVALID"
      0081C2 0A                    4331 	.db 0x0a
      0081C3 00                    4332 	.db 0x00
                           000041  4333 Fmain$__str_5$0$0 == .
      0081C4                       4334 ___str_5:
      0081C4 30 5F 41 44 4A 55 53  4335 	.ascii "0_ADJUST:"
             54 3A
      0081CD 00                    4336 	.db 0x00
                           00004B  4337 Fmain$__str_6$0$0 == .
      0081CE                       4338 ___str_6:
      0081CE 52 4E 47 20 3D 20     4339 	.ascii "RNG = "
      0081D4 00                    4340 	.db 0x00
                           000052  4341 Fmain$__str_7$0$0 == .
      0081D5                       4342 ___str_7:
      0081D5 0A                    4343 	.db 0x0a
      0081D6 56 43 4F 49 20 3D 20  4344 	.ascii "VCOI = "
      0081DD 00                    4345 	.db 0x00
                           00005B  4346 Fmain$__str_8$0$0 == .
      0081DE                       4347 ___str_8:
      0081DE 0A                    4348 	.db 0x0a
      0081DF 53 4C 41 56 45        4349 	.ascii "SLAVE"
      0081E4 0A                    4350 	.db 0x0a
      0081E5 00                    4351 	.db 0x00
                           000063  4352 Fmain$__str_9$0$0 == .
      0081E6                       4353 ___str_9:
      0081E6 53 4C 41 56 45 20 20  4354 	.ascii "SLAVE  RX CONT"
             52 58 20 43 4F 4E 54
      0081F4 0A                    4355 	.db 0x0a
      0081F5 00                    4356 	.db 0x00
                           000073  4357 Fmain$__str_10$0$0 == .
      0081F6                       4358 ___str_10:
      0081F6 31 5F 47 4E 53 53 5F  4359 	.ascii "1_GNSS_WT0:"
             57 54 30 3A
      008201 00                    4360 	.db 0x00
                           00007F  4361 Fmain$__str_11$0$0 == .
      008202                       4362 ___str_11:
      008202 34 5F 46 72 61 6D 69  4363 	.ascii "4_Framing_WT0:"
             6E 67 5F 57 54 30 3A
      008210 00                    4364 	.db 0x00
                                   4365 	.area XINIT   (CODE)
                           000000  4366 Fmain$__xinit_trmID$0$0 == .
      0087FC                       4367 __xinit__trmID:
      0087FC 01 00 10 00           4368 	.byte #0x01,#0x00,#0x10,#0x00	; 1048577
                           000004  4369 Fmain$__xinit_StdPrem$0$0 == .
      008800                       4370 __xinit__StdPrem:
      008800 01                    4371 	.db #0x01	; 1
                           000005  4372 Fmain$__xinit_pkts_received$0$0 == .
      008801                       4373 __xinit__pkts_received:
      008801 00 00                 4374 	.byte #0x00,#0x00	; 0
                           000007  4375 Fmain$__xinit_pkts_missing$0$0 == .
      008803                       4376 __xinit__pkts_missing:
      008803 00 00                 4377 	.byte #0x00,#0x00	; 0
                           000009  4378 Fmain$__xinit_pkt_counter$0$0 == .
      008805                       4379 __xinit__pkt_counter:
      008805 00 00                 4380 	.byte #0x00,#0x00	; 0
                           00000B  4381 Fmain$__xinit_ack_flg$0$0 == .
      008807                       4382 __xinit__ack_flg:
      008807 01                    4383 	.db #0x01	; 1
                           00000C  4384 Fmain$__xinit_txdone$0$0 == .
      008808                       4385 __xinit__txdone:
      008808 00                    4386 	.db #0x00	; 0
                           00000D  4387 Fmain$__xinit_premSlot_counter$0$0 == .
      008809                       4388 __xinit__premSlot_counter:
      008809 00                    4389 	.db #0x00	; 0
                           00000E  4390 Fmain$__xinit_wt0_beacon$0$0 == .
      00880A                       4391 __xinit__wt0_beacon:
      00880A 00 00 00 00           4392 	.byte #0x00,#0x00,#0x00,#0x00	; 0
                           000012  4393 Fmain$__xinit_wt0_tx3$0$0 == .
      00880E                       4394 __xinit__wt0_tx3:
      00880E 00 00 00 00           4395 	.byte #0x00,#0x00,#0x00,#0x00	; 0
                                   4396 	.area CABS    (ABS,CODE)
