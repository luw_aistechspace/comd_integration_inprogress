/**
*******************************************************************************************************
* @file SLAVE\main.c
* @brief Code skeleton for SLAVE module, illustrating reception of packets.
*        The packet format is determined by AX-RadioLAB_output\config.c, produced by the AX-RadioLab GUI
* @internal
* @author   Thomas Sailer, Janani Chellappan, Srinivasan Tamilarasan, Pradeep Kumar GR
* $Rev: $
* $Date: $
********************************************************************************************************
* Copyright 2016 Semiconductor Components Industries LLC (d/b/a �ON Semiconductor�).
* All rights reserved.  This software and/or documentation is licensed by ON Semiconductor
* under limited terms and conditions.  The terms and conditions pertaining to the software
* and/or documentation are available at http://www.onsemi.com/site/pdf/ONSEMI_T&C.pdf
* (�ON Semiconductor Standard Terms and Conditions of Sale, Section 8 Software�) and
* if applicable the software license agreement.  Do not use this software and/or
* documentation unless you have carefully read and you agree to the limited terms and
* conditions.  By using this software and/or documentation, you agree to the limited
* terms and conditions.
*
* THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
* ON SEMICONDUCTOR SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL,
* INCIDENTAL, OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
* @endinternal
*
* @ingroup TEMPLATE_FIRMWARE_504x
*
* @details
*/
// libraries
#include "../AX_Radio_Lab_output/configslave.h"
#include <ax8052.h>
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include "libmfosc.h"
#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#if defined(USE_LCD) || defined(USE_COM0)
#define USE_DISPLAY
#endif /* defined(USE_LCD) || defined(USE_COM0) */
#include "../COMMON/display_com0.h"
#include <libminikitleds.h>
/*#include <stdlib.h> */
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"
/* Verify the function display_writenum16 in libaxdvk2.a */
#include <string.h>
#include <ax8052crypto.h>
#include <ax8052cryptoregaddr.h>
#include <libmfcrypto.h>
#include "../COMMON/easyax5043.h"
#include "../COMMON/ublox_gnss.h"
#include "../COMMON/pktframing.h"

// macros
#define ADJUST_FREQREG
#define FREQOFFS_K 3

// function declarations
extern void GOLDSEQUENCE_Init(void);
extern void UBLOX_GPS_PortInit();
extern void packetStdFraming(void);
extern void transmit_copies_wtimer(void);
extern void packetPremFraming(void);
extern void transmit_prem_wtimer(void);
extern void UBLOX_payload_POSLLH(void);
// variables
extern uint8_t _start__stack[];

uint8_t __data coldstart = 1; /* caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */
uint16_t __xdata pkts_received = 0, pkts_missing = 0;
uint16_t __xdata pkt_counter = 0;
uint8_t __xdata ack_flg = 1; // initial value set to 1
uint8_t __xdata txdone = 0;
uint8_t __xdata payload[28];
uint16_t __xdata payload_len;
uint8_t __xdata premSlot_counter = 0;
uint32_t __xdata wt0_beacon = 0;
uint32_t __xdata wt0_tx3 = 0;
//uint8_t __xdata testpkt[10] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99};

// code
/* Debug Link Configuration */
#if defined __ARMEB_
#if defined USE_DBGLINK
void debuglink_init_axm0(void)
{
    dbglink_timer0_baud(0, 9600, AXM0XX_HFCLK_CLOCK_FREQ);  /* HS OSC, Baud rate, Clock */
    dbglink_init(0, 8, 1);                              	/* TImer No., Word length, Stop bit */
}
#endif /* USE_DBGLINK */
#endif /* defined */

/* If IAR will this condition is valid */
static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
{
    uint8_t pc = PCON;

    if (!(pc & 0x80))
        return;

    GPIOENABLE = 0;
    IE = EIE = E2IE = 0;

    for (;;)
        PCON |= 0x01;
}

void axradio_statuschange(struct axradio_status __xdata *st)
{
    /*    static uint16_t rssi_ratelimit; */
/*#if defined(USE_DBGLINK) && defined(DEBUGMSG)
    if (DBGLNKSTAT & 0x10 && st->status != AXRADIO_STAT_CHANNELSTATE)
    {
        dbglink_writestr("ST: 0x");
        dbglink_writehex16(st->status, 2, WRNUM_PADZERO);
        dbglink_writestr(" ERR: 0x");
        dbglink_writehex16(st->error, 2, WRNUM_PADZERO);
        dbglink_tx('\n');
    }

#endif*/
    switch (st->status)
    {
    case AXRADIO_STAT_RECEIVE:

        {
            uint8_t __xdata pktlen = st->u.rx.pktlen;
            const uint8_t __xdata *pktdata = st->u.rx.mac.raw;


            if (st->error == AXRADIO_ERR_NOERROR)
            {
                if (pktlen == 8) // check beacon frame
                {
                        wt0_beacon = wtimer0_curtime();
#ifdef USE_DBGLINK
                        dbglink_writestr("5_RX_WT0:");
                        dbglink_writehex32(/*wtimer0_curtime()*/wt0_beacon, 8, WRNUM_PADZERO);
                        dbglink_tx('\n');
#endif // USE_DBGLINK
                        ++pkts_received;
//                        if (ack_flg) // prepare next packet only if ack pkt is received for the last transmission
//                        {
// added on 2021-08-19 to correct pkt counting.
#ifdef USE_DBGLINK

        //if (st->error == AXRADIO_ERR_NOERROR)
            dbglink_received_packet(st);
#endif
//////////////////////////
                        if(StdPrem == 0x1) // In the case of standard user type
                        {
                           /* if (ack_flg)
                            {
                                packetStdFraming();
#ifdef USE_DBGLINK
                                dbglink_writestr("5_Framing_WT0:");
                                dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
                                dbglink_tx('\n');
#endif // USE_DBGLINK
                            }*/
                            transmit_copies_wtimer();

                        }
                        else if (StdPrem == 0x0) // In the case of premium user type
                        {
                            /*if (ack_flg)
                            {
                                packetPremFraming();
#ifdef USE_DBGLINK
                                dbglink_writestr("5_Framing_WT0:");
                                dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
                                dbglink_tx('\n');
#endif // USE_DBGLINK
                            }*/
                            transmit_prem_wtimer();

                        }
// added on 2021-08-30
                        else if (StdPrem == 0x2) // In the case of timing test
                        {
                            transmit_prem_wtimer();
                            transmit_copies_wtimer();
                        }
/////////////////////////
                }
                else if ((pktlen == 4) && (pktdata[1] >> ((uint8_t)(trmID & 0x00000F)-1)))
                {
                    ack_flg = 1;
                    pkt_counter += 1;
#ifdef USE_DBGLINK
                        dbglink_writestr("90_ACK_WT0:");
                        dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
                        dbglink_tx('\n');
                        dbglink_writestr("91_ACK_delta:");
                        dbglink_writehex32(wtimer0_curtime()-wt0_tx3, 4, WRNUM_PADZERO);
                        dbglink_tx('\n');
                        dbglink_writehex16(pkt_counter, 4, WRNUM_PADZERO);
                        dbglink_writestr(" successful pkts.\n\n");
                        dbglink_received_packet(st);

#endif // USE_DBGLINK
      //              UBLOX_payload_POSLLH();
// added on 2021-08-06 to test GNSS/UART1 reliability
/*    PORTA_1 = 1;
    do
    {
        UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,payload,ANS,&payload_len);
        delay_ms(50);
    } while(!payload_len);
    PORTA_1 = 0;
#ifdef USE_DBGLINK
    dbglink_writestr("4_GNSS_WT0:");
    dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);*/
/*    for (i = 0; i < payload_len; i++)
    {
        dbglink_writehex16(payload[i], 2, WRNUM_PADZERO);
        dbglink_tx(' ');
    }*/
/*    dbglink_tx('\n');
#endif // USE_DBGLINK*/
/////////////////////////////////GNSS/UART1 reliability
                }
                else
                {
#ifdef USE_DBGLINK
                    dbglink_received_packet(st);
                    dbglink_writenum16((pktdata[1]>>(uint8_t)(trmID & 0x00000F)),1,WRNUM_PADZERO);
                    dbglink_writestr("INVALID\n");
#endif // USE_DBGLINK
                   // ack_flg = 0;
                    // what else?
                }
            }

        /* Frequency Offset Correction */
        {
            int32_t foffs = axradio_get_freqoffset();
// added on 2021-08-13
/*#ifdef USE_DBGLINK
            dbglink_writestr("FOFFS=");
            dbglink_writenum32(foffs, 2, WRNUM_SIGNED);
            dbglink_tx('\n');
#endif // USE_DBGLINK*/
//////////////////////////
#if defined(ADJUST_FREQREG)
            foffs -= (st->u.rx.phy.offset)>>(FREQOFFS_K); /*adjust RX frequency by low-pass filtered frequency offset */
#endif
// added on 2021-08-13
/*#ifdef USE_DBGLINK
            dbglink_writestr("FOFFS_new=");
            dbglink_writenum32(foffs, 2, WRNUM_SIGNED);
            dbglink_tx('\n');
#endif // USE_DBGLINK*/
//////////////////////////

            /* reset if deviation too large */
// added on 2021-08-06 from codeintegration
            if ((axradio_set_freqoffset(foffs) != AXRADIO_ERR_NOERROR) || (axradio_conv_freq_tohz(axradio_get_freqoffset()) < -1000))
///////////////////////////////////////////////////
//            if (axradio_set_freqoffset(foffs) != AXRADIO_ERR_NOERROR)
// added on 2021-08-06
            {
///////////////////////////
                axradio_set_freqoffset(0);
#ifdef USE_DBGLINK
                dbglink_writestr("0_ADJUST:");
                dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
                dbglink_tx('\n');
#endif // USE_DBGLINK
// added on 2021-08-06
            }
///////////////////////////
        }
        }
        break;

    case AXRADIO_STAT_TRANSMITSTART:
      //  led3_on();
        break;

    case AXRADIO_STAT_TRANSMITEND:
       // led3_off();
        break;

    default:
        break;
    }
}

void enable_radio_interrupt_in_mcu_pin(void)
{
	IE_4 = 1;
}

void disable_radio_interrupt_in_mcu_pin(void)
{
	IE_4 = 0;
}


uint8_t _sdcc_external_startup(void)
{
    LPXOSCGM = 0x8A;
    wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
    wtimer1_setclksrc(CLKSRC_FRCOSC, 7);

    /* caution: coldstart has to be initialized with 1 in it's definition! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */
    coldstart = !(PCON & 0x40);

    ANALOGA = 0x18; /* PA[3,4] LPXOSC, other PA are used as digital pins */
    PORTA = 0x3F;
    PORTB = 0x37;
    PORTC = 0x00;
    PORTR = 0x0B;

    DIRA = 0x3F;
    DIRB = 0x0D;
    DIRC = 0x10;
    DIRR = 0x15;

    PALTA = 0x00;
    PALTB = 0x11;
    PALTC = 0x0F;

    PINSEL |= 0xC0;

    axradio_setup_pincfg1();
    DPS = 0;
    IE = 0x40;
    EIE = 0x00;
    E2IE = 0x00;

    //display_portinit();
    GPIOENABLE = 1; /* unfreeze GPIO */
// added on 2021-08-05
    //random number generator
    RNGMODE = 0x0F;
    RNGCLKSRC0 = 0x09;
    RNGCLKSRC1 = 0x00;
    //delay_ms(500);
    //GOLDSEQUENCE_Init();
//////////////////////////
    return !coldstart; /* coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization */
}

int main(void)
{
    uint8_t i;
    __asm
    G$_start__stack$0$0 = __start__stack
                          .globl G$_start__stack$0$0
                          __endasm;
#ifdef USE_DBGLINK
    dbglink_init();
#endif
    __enable_irq();
    flash_apply_calibration();
    CLKCON = 0x00;
    wtimer_init();
    GOLDSEQUENCE_Init();

    if (coldstart)
    {
        display_init();
        display_setpos(0);

        i = axradio_init();

        if (i != AXRADIO_ERR_NOERROR)
        {
            if (i == AXRADIO_ERR_NOCHIP)
            {
                display_writestr(radio_not_found_lcd_display);
#ifdef USE_DBGLINK

                if(DBGLNKSTAT & 0x10)
                    dbglink_writestr(radio_not_found_lcd_display);

#endif /* USE_DBGLINK */
                goto terminate_error;
            }

            goto terminate_radio_error;
        }

        display_writestr(radio_lcd_display);
#ifdef USE_DBGLINK

        if (DBGLNKSTAT & 0x10)
            dbglink_writestr(radio_lcd_display);

#endif /* USE_DBGLINK */
        axradio_set_local_address(&localaddr);
        axradio_set_default_remote_address(&remoteaddr);

#ifdef USE_DBGLINK

        if (DBGLNKSTAT & 0x10)
        {
            dbglink_writestr("RNG = ");
            dbglink_writenum16(axradio_get_pllrange(), 2, 0);
            {
                uint8_t x = axradio_get_pllvcoi();

                if (x & 0x80)
                {
                    dbglink_writestr("\nVCOI = ");
                    dbglink_writehex16(x, 2, 0);
                }
            }
            dbglink_writestr("\nSLAVE\n");
        }

#endif /* USE_DBGLINK */

#ifdef USE_DBGLINK

        if (DBGLNKSTAT & 0x10)
            dbglink_writestr("SLAVE  RX CONT\n");

#endif /* USE_DBGLINK */
        i = axradio_set_mode(RADIO_MODE);

        if (i != AXRADIO_ERR_NOERROR)
            goto terminate_radio_error;

#ifdef DBGPKT
        axradio_dbgpkt_enableIRQ();
#endif
    }
    else
    {
        /*  Warm Start */
        axradio_commsleepexit();
        IE_4 = 1; /* enable radio interrupt */
    }

    axradio_setup_pincfg2();
// added on 2021-07-28
// uart0 initialization
    PALTB |= 0x11;
    DIRB |= (1<<0) | (1<<4);
    DIRB &= (uint8_t)~(1<<5);
    DIRB &= (uint8_t)~(1<<1);
    PINSEL &= (uint8_t)~((1<<0) | (1<<1));
    uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
    uart0_init(1,8,1);

// GNSS initialization
    UBLOX_GPS_PortInit();
/*    do
    {
        UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,payload,ANS,&payload_len);
        delay_ms(50);
    } while(!payload_len);
#ifdef USE_DBGLINK
    dbglink_writenum16(payload_len, 2, WRNUM_PADZERO);
    for (i = 0; i < payload_len; i++)
    {
        dbglink_writehex16(payload[i], 2, WRNUM_PADZERO);
        dbglink_tx(' ');
    }
    dbglink_tx('\n');
#endif // */
//    PORTA_1 = 0;
//    delay_ms(200);
///////////////////////////
    for(;;)
    {
// added on 2021-08-20
        if (ack_flg /*&& (pkts_received >0)*/)
        {
            UBLOX_payload_POSLLH();
#ifdef USE_DBGLINK
                dbglink_writestr("1_GNSS_WT0:");
                dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
/*              for (i = 0; i < payload_len; i++)
                {
                    dbglink_writehex16(payload[i], 2, WRNUM_PADZERO);
                    dbglink_tx(' ');
                }*/
                dbglink_tx('\n');
#endif
            if (StdPrem == 0x1)
                packetStdFraming();
            else if (StdPrem == 0x0)
                packetPremFraming();
// added on 2021-08-30
            else if (StdPrem == 0x2)
            {
                packetPremFraming();
                packetStdFraming();
            }
#ifdef USE_DBGLINK
                                dbglink_writestr("4_Framing_WT0:");
                                dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
                                dbglink_tx('\n');
#endif // USE_DBGLINK
            ack_flg = 0;
        }
//////////////////////////
        wtimer_runcallbacks();
        __disable_irq();
        {
            uint8_t flg = WTFLAG_CANSTANDBY;
#ifdef MCU_SLEEP

            if (axradio_cansleep()
#ifdef USE_DBGLINK
                    && dbglink_txidle()
#endif /* USE_DBGLINK */
                    && display_txidle())
                flg |= WTFLAG_CANSLEEP;
#endif /* MCU_SLEEP */
            wtimer_idle(flg);
        }
        __enable_irq();
    }

terminate_radio_error:
    display_radio_error(i);
#ifdef USE_DBGLINK
    dbglink_display_radio_error(i);
#endif /* USE_DBGLINK */
terminate_error:

    for (;;)
    {
        wtimer_runcallbacks();
        {
            uint8_t flg = WTFLAG_CANSTANDBY;
#ifdef MCU_SLEEP

            if (axradio_cansleep()
#ifdef USE_DBGLINK
                    && dbglink_txidle()
#endif /* USE_DBGLINK */
                    && display_txidle())
                flg |= WTFLAG_CANSLEEP;
#endif /* MCU_SLEEP */
            wtimer_idle(flg);
        }
    }
}

