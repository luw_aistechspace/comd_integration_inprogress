                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module misc
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _com0_writestr
                                     12 	.globl _com0_setpos
                                     13 	.globl _dbglink_writestr
                                     14 	.globl _dbglink_tx
                                     15 	.globl _wtimer_remove
                                     16 	.globl _wtimer1_addrelative
                                     17 	.globl _wtimer_runcallbacks
                                     18 	.globl _wtimer_idle
                                     19 	.globl _PORTC_7
                                     20 	.globl _PORTC_6
                                     21 	.globl _PORTC_5
                                     22 	.globl _PORTC_4
                                     23 	.globl _PORTC_3
                                     24 	.globl _PORTC_2
                                     25 	.globl _PORTC_1
                                     26 	.globl _PORTC_0
                                     27 	.globl _PORTB_7
                                     28 	.globl _PORTB_6
                                     29 	.globl _PORTB_5
                                     30 	.globl _PORTB_4
                                     31 	.globl _PORTB_3
                                     32 	.globl _PORTB_2
                                     33 	.globl _PORTB_1
                                     34 	.globl _PORTB_0
                                     35 	.globl _PORTA_7
                                     36 	.globl _PORTA_6
                                     37 	.globl _PORTA_5
                                     38 	.globl _PORTA_4
                                     39 	.globl _PORTA_3
                                     40 	.globl _PORTA_2
                                     41 	.globl _PORTA_1
                                     42 	.globl _PORTA_0
                                     43 	.globl _PINC_7
                                     44 	.globl _PINC_6
                                     45 	.globl _PINC_5
                                     46 	.globl _PINC_4
                                     47 	.globl _PINC_3
                                     48 	.globl _PINC_2
                                     49 	.globl _PINC_1
                                     50 	.globl _PINC_0
                                     51 	.globl _PINB_7
                                     52 	.globl _PINB_6
                                     53 	.globl _PINB_5
                                     54 	.globl _PINB_4
                                     55 	.globl _PINB_3
                                     56 	.globl _PINB_2
                                     57 	.globl _PINB_1
                                     58 	.globl _PINB_0
                                     59 	.globl _PINA_7
                                     60 	.globl _PINA_6
                                     61 	.globl _PINA_5
                                     62 	.globl _PINA_4
                                     63 	.globl _PINA_3
                                     64 	.globl _PINA_2
                                     65 	.globl _PINA_1
                                     66 	.globl _PINA_0
                                     67 	.globl _CY
                                     68 	.globl _AC
                                     69 	.globl _F0
                                     70 	.globl _RS1
                                     71 	.globl _RS0
                                     72 	.globl _OV
                                     73 	.globl _F1
                                     74 	.globl _P
                                     75 	.globl _IP_7
                                     76 	.globl _IP_6
                                     77 	.globl _IP_5
                                     78 	.globl _IP_4
                                     79 	.globl _IP_3
                                     80 	.globl _IP_2
                                     81 	.globl _IP_1
                                     82 	.globl _IP_0
                                     83 	.globl _EA
                                     84 	.globl _IE_7
                                     85 	.globl _IE_6
                                     86 	.globl _IE_5
                                     87 	.globl _IE_4
                                     88 	.globl _IE_3
                                     89 	.globl _IE_2
                                     90 	.globl _IE_1
                                     91 	.globl _IE_0
                                     92 	.globl _EIP_7
                                     93 	.globl _EIP_6
                                     94 	.globl _EIP_5
                                     95 	.globl _EIP_4
                                     96 	.globl _EIP_3
                                     97 	.globl _EIP_2
                                     98 	.globl _EIP_1
                                     99 	.globl _EIP_0
                                    100 	.globl _EIE_7
                                    101 	.globl _EIE_6
                                    102 	.globl _EIE_5
                                    103 	.globl _EIE_4
                                    104 	.globl _EIE_3
                                    105 	.globl _EIE_2
                                    106 	.globl _EIE_1
                                    107 	.globl _EIE_0
                                    108 	.globl _E2IP_7
                                    109 	.globl _E2IP_6
                                    110 	.globl _E2IP_5
                                    111 	.globl _E2IP_4
                                    112 	.globl _E2IP_3
                                    113 	.globl _E2IP_2
                                    114 	.globl _E2IP_1
                                    115 	.globl _E2IP_0
                                    116 	.globl _E2IE_7
                                    117 	.globl _E2IE_6
                                    118 	.globl _E2IE_5
                                    119 	.globl _E2IE_4
                                    120 	.globl _E2IE_3
                                    121 	.globl _E2IE_2
                                    122 	.globl _E2IE_1
                                    123 	.globl _E2IE_0
                                    124 	.globl _B_7
                                    125 	.globl _B_6
                                    126 	.globl _B_5
                                    127 	.globl _B_4
                                    128 	.globl _B_3
                                    129 	.globl _B_2
                                    130 	.globl _B_1
                                    131 	.globl _B_0
                                    132 	.globl _ACC_7
                                    133 	.globl _ACC_6
                                    134 	.globl _ACC_5
                                    135 	.globl _ACC_4
                                    136 	.globl _ACC_3
                                    137 	.globl _ACC_2
                                    138 	.globl _ACC_1
                                    139 	.globl _ACC_0
                                    140 	.globl _WTSTAT
                                    141 	.globl _WTIRQEN
                                    142 	.globl _WTEVTD
                                    143 	.globl _WTEVTD1
                                    144 	.globl _WTEVTD0
                                    145 	.globl _WTEVTC
                                    146 	.globl _WTEVTC1
                                    147 	.globl _WTEVTC0
                                    148 	.globl _WTEVTB
                                    149 	.globl _WTEVTB1
                                    150 	.globl _WTEVTB0
                                    151 	.globl _WTEVTA
                                    152 	.globl _WTEVTA1
                                    153 	.globl _WTEVTA0
                                    154 	.globl _WTCNTR1
                                    155 	.globl _WTCNTB
                                    156 	.globl _WTCNTB1
                                    157 	.globl _WTCNTB0
                                    158 	.globl _WTCNTA
                                    159 	.globl _WTCNTA1
                                    160 	.globl _WTCNTA0
                                    161 	.globl _WTCFGB
                                    162 	.globl _WTCFGA
                                    163 	.globl _WDTRESET
                                    164 	.globl _WDTCFG
                                    165 	.globl _U1STATUS
                                    166 	.globl _U1SHREG
                                    167 	.globl _U1MODE
                                    168 	.globl _U1CTRL
                                    169 	.globl _U0STATUS
                                    170 	.globl _U0SHREG
                                    171 	.globl _U0MODE
                                    172 	.globl _U0CTRL
                                    173 	.globl _T2STATUS
                                    174 	.globl _T2PERIOD
                                    175 	.globl _T2PERIOD1
                                    176 	.globl _T2PERIOD0
                                    177 	.globl _T2MODE
                                    178 	.globl _T2CNT
                                    179 	.globl _T2CNT1
                                    180 	.globl _T2CNT0
                                    181 	.globl _T2CLKSRC
                                    182 	.globl _T1STATUS
                                    183 	.globl _T1PERIOD
                                    184 	.globl _T1PERIOD1
                                    185 	.globl _T1PERIOD0
                                    186 	.globl _T1MODE
                                    187 	.globl _T1CNT
                                    188 	.globl _T1CNT1
                                    189 	.globl _T1CNT0
                                    190 	.globl _T1CLKSRC
                                    191 	.globl _T0STATUS
                                    192 	.globl _T0PERIOD
                                    193 	.globl _T0PERIOD1
                                    194 	.globl _T0PERIOD0
                                    195 	.globl _T0MODE
                                    196 	.globl _T0CNT
                                    197 	.globl _T0CNT1
                                    198 	.globl _T0CNT0
                                    199 	.globl _T0CLKSRC
                                    200 	.globl _SPSTATUS
                                    201 	.globl _SPSHREG
                                    202 	.globl _SPMODE
                                    203 	.globl _SPCLKSRC
                                    204 	.globl _RADIOSTAT
                                    205 	.globl _RADIOSTAT1
                                    206 	.globl _RADIOSTAT0
                                    207 	.globl _RADIODATA
                                    208 	.globl _RADIODATA3
                                    209 	.globl _RADIODATA2
                                    210 	.globl _RADIODATA1
                                    211 	.globl _RADIODATA0
                                    212 	.globl _RADIOADDR
                                    213 	.globl _RADIOADDR1
                                    214 	.globl _RADIOADDR0
                                    215 	.globl _RADIOACC
                                    216 	.globl _OC1STATUS
                                    217 	.globl _OC1PIN
                                    218 	.globl _OC1MODE
                                    219 	.globl _OC1COMP
                                    220 	.globl _OC1COMP1
                                    221 	.globl _OC1COMP0
                                    222 	.globl _OC0STATUS
                                    223 	.globl _OC0PIN
                                    224 	.globl _OC0MODE
                                    225 	.globl _OC0COMP
                                    226 	.globl _OC0COMP1
                                    227 	.globl _OC0COMP0
                                    228 	.globl _NVSTATUS
                                    229 	.globl _NVKEY
                                    230 	.globl _NVDATA
                                    231 	.globl _NVDATA1
                                    232 	.globl _NVDATA0
                                    233 	.globl _NVADDR
                                    234 	.globl _NVADDR1
                                    235 	.globl _NVADDR0
                                    236 	.globl _IC1STATUS
                                    237 	.globl _IC1MODE
                                    238 	.globl _IC1CAPT
                                    239 	.globl _IC1CAPT1
                                    240 	.globl _IC1CAPT0
                                    241 	.globl _IC0STATUS
                                    242 	.globl _IC0MODE
                                    243 	.globl _IC0CAPT
                                    244 	.globl _IC0CAPT1
                                    245 	.globl _IC0CAPT0
                                    246 	.globl _PORTR
                                    247 	.globl _PORTC
                                    248 	.globl _PORTB
                                    249 	.globl _PORTA
                                    250 	.globl _PINR
                                    251 	.globl _PINC
                                    252 	.globl _PINB
                                    253 	.globl _PINA
                                    254 	.globl _DIRR
                                    255 	.globl _DIRC
                                    256 	.globl _DIRB
                                    257 	.globl _DIRA
                                    258 	.globl _DBGLNKSTAT
                                    259 	.globl _DBGLNKBUF
                                    260 	.globl _CODECONFIG
                                    261 	.globl _CLKSTAT
                                    262 	.globl _CLKCON
                                    263 	.globl _ANALOGCOMP
                                    264 	.globl _ADCCONV
                                    265 	.globl _ADCCLKSRC
                                    266 	.globl _ADCCH3CONFIG
                                    267 	.globl _ADCCH2CONFIG
                                    268 	.globl _ADCCH1CONFIG
                                    269 	.globl _ADCCH0CONFIG
                                    270 	.globl __XPAGE
                                    271 	.globl _XPAGE
                                    272 	.globl _SP
                                    273 	.globl _PSW
                                    274 	.globl _PCON
                                    275 	.globl _IP
                                    276 	.globl _IE
                                    277 	.globl _EIP
                                    278 	.globl _EIE
                                    279 	.globl _E2IP
                                    280 	.globl _E2IE
                                    281 	.globl _DPS
                                    282 	.globl _DPTR1
                                    283 	.globl _DPTR0
                                    284 	.globl _DPL1
                                    285 	.globl _DPL
                                    286 	.globl _DPH1
                                    287 	.globl _DPH
                                    288 	.globl _B
                                    289 	.globl _ACC
                                    290 	.globl _XTALREADY
                                    291 	.globl _XTALOSC
                                    292 	.globl _XTALAMPL
                                    293 	.globl _SILICONREV
                                    294 	.globl _SCRATCH3
                                    295 	.globl _SCRATCH2
                                    296 	.globl _SCRATCH1
                                    297 	.globl _SCRATCH0
                                    298 	.globl _RADIOMUX
                                    299 	.globl _RADIOFSTATADDR
                                    300 	.globl _RADIOFSTATADDR1
                                    301 	.globl _RADIOFSTATADDR0
                                    302 	.globl _RADIOFDATAADDR
                                    303 	.globl _RADIOFDATAADDR1
                                    304 	.globl _RADIOFDATAADDR0
                                    305 	.globl _OSCRUN
                                    306 	.globl _OSCREADY
                                    307 	.globl _OSCFORCERUN
                                    308 	.globl _OSCCALIB
                                    309 	.globl _MISCCTRL
                                    310 	.globl _LPXOSCGM
                                    311 	.globl _LPOSCREF
                                    312 	.globl _LPOSCREF1
                                    313 	.globl _LPOSCREF0
                                    314 	.globl _LPOSCPER
                                    315 	.globl _LPOSCPER1
                                    316 	.globl _LPOSCPER0
                                    317 	.globl _LPOSCKFILT
                                    318 	.globl _LPOSCKFILT1
                                    319 	.globl _LPOSCKFILT0
                                    320 	.globl _LPOSCFREQ
                                    321 	.globl _LPOSCFREQ1
                                    322 	.globl _LPOSCFREQ0
                                    323 	.globl _LPOSCCONFIG
                                    324 	.globl _PINSEL
                                    325 	.globl _PINCHGC
                                    326 	.globl _PINCHGB
                                    327 	.globl _PINCHGA
                                    328 	.globl _PALTRADIO
                                    329 	.globl _PALTC
                                    330 	.globl _PALTB
                                    331 	.globl _PALTA
                                    332 	.globl _INTCHGC
                                    333 	.globl _INTCHGB
                                    334 	.globl _INTCHGA
                                    335 	.globl _EXTIRQ
                                    336 	.globl _GPIOENABLE
                                    337 	.globl _ANALOGA
                                    338 	.globl _FRCOSCREF
                                    339 	.globl _FRCOSCREF1
                                    340 	.globl _FRCOSCREF0
                                    341 	.globl _FRCOSCPER
                                    342 	.globl _FRCOSCPER1
                                    343 	.globl _FRCOSCPER0
                                    344 	.globl _FRCOSCKFILT
                                    345 	.globl _FRCOSCKFILT1
                                    346 	.globl _FRCOSCKFILT0
                                    347 	.globl _FRCOSCFREQ
                                    348 	.globl _FRCOSCFREQ1
                                    349 	.globl _FRCOSCFREQ0
                                    350 	.globl _FRCOSCCTRL
                                    351 	.globl _FRCOSCCONFIG
                                    352 	.globl _DMA1CONFIG
                                    353 	.globl _DMA1ADDR
                                    354 	.globl _DMA1ADDR1
                                    355 	.globl _DMA1ADDR0
                                    356 	.globl _DMA0CONFIG
                                    357 	.globl _DMA0ADDR
                                    358 	.globl _DMA0ADDR1
                                    359 	.globl _DMA0ADDR0
                                    360 	.globl _ADCTUNE2
                                    361 	.globl _ADCTUNE1
                                    362 	.globl _ADCTUNE0
                                    363 	.globl _ADCCH3VAL
                                    364 	.globl _ADCCH3VAL1
                                    365 	.globl _ADCCH3VAL0
                                    366 	.globl _ADCCH2VAL
                                    367 	.globl _ADCCH2VAL1
                                    368 	.globl _ADCCH2VAL0
                                    369 	.globl _ADCCH1VAL
                                    370 	.globl _ADCCH1VAL1
                                    371 	.globl _ADCCH1VAL0
                                    372 	.globl _ADCCH0VAL
                                    373 	.globl _ADCCH0VAL1
                                    374 	.globl _ADCCH0VAL0
                                    375 	.globl _lcd2_display_radio_error
                                    376 	.globl _com0_display_radio_error
                                    377 	.globl _dbglink_display_radio_error
                                    378 	.globl _delay_ms
                                    379 ;--------------------------------------------------------
                                    380 ; special function registers
                                    381 ;--------------------------------------------------------
                                    382 	.area RSEG    (ABS,DATA)
      000000                        383 	.org 0x0000
                           0000E0   384 G$ACC$0$0 == 0x00e0
                           0000E0   385 _ACC	=	0x00e0
                           0000F0   386 G$B$0$0 == 0x00f0
                           0000F0   387 _B	=	0x00f0
                           000083   388 G$DPH$0$0 == 0x0083
                           000083   389 _DPH	=	0x0083
                           000085   390 G$DPH1$0$0 == 0x0085
                           000085   391 _DPH1	=	0x0085
                           000082   392 G$DPL$0$0 == 0x0082
                           000082   393 _DPL	=	0x0082
                           000084   394 G$DPL1$0$0 == 0x0084
                           000084   395 _DPL1	=	0x0084
                           008382   396 G$DPTR0$0$0 == 0x8382
                           008382   397 _DPTR0	=	0x8382
                           008584   398 G$DPTR1$0$0 == 0x8584
                           008584   399 _DPTR1	=	0x8584
                           000086   400 G$DPS$0$0 == 0x0086
                           000086   401 _DPS	=	0x0086
                           0000A0   402 G$E2IE$0$0 == 0x00a0
                           0000A0   403 _E2IE	=	0x00a0
                           0000C0   404 G$E2IP$0$0 == 0x00c0
                           0000C0   405 _E2IP	=	0x00c0
                           000098   406 G$EIE$0$0 == 0x0098
                           000098   407 _EIE	=	0x0098
                           0000B0   408 G$EIP$0$0 == 0x00b0
                           0000B0   409 _EIP	=	0x00b0
                           0000A8   410 G$IE$0$0 == 0x00a8
                           0000A8   411 _IE	=	0x00a8
                           0000B8   412 G$IP$0$0 == 0x00b8
                           0000B8   413 _IP	=	0x00b8
                           000087   414 G$PCON$0$0 == 0x0087
                           000087   415 _PCON	=	0x0087
                           0000D0   416 G$PSW$0$0 == 0x00d0
                           0000D0   417 _PSW	=	0x00d0
                           000081   418 G$SP$0$0 == 0x0081
                           000081   419 _SP	=	0x0081
                           0000D9   420 G$XPAGE$0$0 == 0x00d9
                           0000D9   421 _XPAGE	=	0x00d9
                           0000D9   422 G$_XPAGE$0$0 == 0x00d9
                           0000D9   423 __XPAGE	=	0x00d9
                           0000CA   424 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   425 _ADCCH0CONFIG	=	0x00ca
                           0000CB   426 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   427 _ADCCH1CONFIG	=	0x00cb
                           0000D2   428 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   429 _ADCCH2CONFIG	=	0x00d2
                           0000D3   430 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   431 _ADCCH3CONFIG	=	0x00d3
                           0000D1   432 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   433 _ADCCLKSRC	=	0x00d1
                           0000C9   434 G$ADCCONV$0$0 == 0x00c9
                           0000C9   435 _ADCCONV	=	0x00c9
                           0000E1   436 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   437 _ANALOGCOMP	=	0x00e1
                           0000C6   438 G$CLKCON$0$0 == 0x00c6
                           0000C6   439 _CLKCON	=	0x00c6
                           0000C7   440 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   441 _CLKSTAT	=	0x00c7
                           000097   442 G$CODECONFIG$0$0 == 0x0097
                           000097   443 _CODECONFIG	=	0x0097
                           0000E3   444 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   445 _DBGLNKBUF	=	0x00e3
                           0000E2   446 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   447 _DBGLNKSTAT	=	0x00e2
                           000089   448 G$DIRA$0$0 == 0x0089
                           000089   449 _DIRA	=	0x0089
                           00008A   450 G$DIRB$0$0 == 0x008a
                           00008A   451 _DIRB	=	0x008a
                           00008B   452 G$DIRC$0$0 == 0x008b
                           00008B   453 _DIRC	=	0x008b
                           00008E   454 G$DIRR$0$0 == 0x008e
                           00008E   455 _DIRR	=	0x008e
                           0000C8   456 G$PINA$0$0 == 0x00c8
                           0000C8   457 _PINA	=	0x00c8
                           0000E8   458 G$PINB$0$0 == 0x00e8
                           0000E8   459 _PINB	=	0x00e8
                           0000F8   460 G$PINC$0$0 == 0x00f8
                           0000F8   461 _PINC	=	0x00f8
                           00008D   462 G$PINR$0$0 == 0x008d
                           00008D   463 _PINR	=	0x008d
                           000080   464 G$PORTA$0$0 == 0x0080
                           000080   465 _PORTA	=	0x0080
                           000088   466 G$PORTB$0$0 == 0x0088
                           000088   467 _PORTB	=	0x0088
                           000090   468 G$PORTC$0$0 == 0x0090
                           000090   469 _PORTC	=	0x0090
                           00008C   470 G$PORTR$0$0 == 0x008c
                           00008C   471 _PORTR	=	0x008c
                           0000CE   472 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   473 _IC0CAPT0	=	0x00ce
                           0000CF   474 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   475 _IC0CAPT1	=	0x00cf
                           00CFCE   476 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   477 _IC0CAPT	=	0xcfce
                           0000CC   478 G$IC0MODE$0$0 == 0x00cc
                           0000CC   479 _IC0MODE	=	0x00cc
                           0000CD   480 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   481 _IC0STATUS	=	0x00cd
                           0000D6   482 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   483 _IC1CAPT0	=	0x00d6
                           0000D7   484 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   485 _IC1CAPT1	=	0x00d7
                           00D7D6   486 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   487 _IC1CAPT	=	0xd7d6
                           0000D4   488 G$IC1MODE$0$0 == 0x00d4
                           0000D4   489 _IC1MODE	=	0x00d4
                           0000D5   490 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   491 _IC1STATUS	=	0x00d5
                           000092   492 G$NVADDR0$0$0 == 0x0092
                           000092   493 _NVADDR0	=	0x0092
                           000093   494 G$NVADDR1$0$0 == 0x0093
                           000093   495 _NVADDR1	=	0x0093
                           009392   496 G$NVADDR$0$0 == 0x9392
                           009392   497 _NVADDR	=	0x9392
                           000094   498 G$NVDATA0$0$0 == 0x0094
                           000094   499 _NVDATA0	=	0x0094
                           000095   500 G$NVDATA1$0$0 == 0x0095
                           000095   501 _NVDATA1	=	0x0095
                           009594   502 G$NVDATA$0$0 == 0x9594
                           009594   503 _NVDATA	=	0x9594
                           000096   504 G$NVKEY$0$0 == 0x0096
                           000096   505 _NVKEY	=	0x0096
                           000091   506 G$NVSTATUS$0$0 == 0x0091
                           000091   507 _NVSTATUS	=	0x0091
                           0000BC   508 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   509 _OC0COMP0	=	0x00bc
                           0000BD   510 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   511 _OC0COMP1	=	0x00bd
                           00BDBC   512 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   513 _OC0COMP	=	0xbdbc
                           0000B9   514 G$OC0MODE$0$0 == 0x00b9
                           0000B9   515 _OC0MODE	=	0x00b9
                           0000BA   516 G$OC0PIN$0$0 == 0x00ba
                           0000BA   517 _OC0PIN	=	0x00ba
                           0000BB   518 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   519 _OC0STATUS	=	0x00bb
                           0000C4   520 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   521 _OC1COMP0	=	0x00c4
                           0000C5   522 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   523 _OC1COMP1	=	0x00c5
                           00C5C4   524 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   525 _OC1COMP	=	0xc5c4
                           0000C1   526 G$OC1MODE$0$0 == 0x00c1
                           0000C1   527 _OC1MODE	=	0x00c1
                           0000C2   528 G$OC1PIN$0$0 == 0x00c2
                           0000C2   529 _OC1PIN	=	0x00c2
                           0000C3   530 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   531 _OC1STATUS	=	0x00c3
                           0000B1   532 G$RADIOACC$0$0 == 0x00b1
                           0000B1   533 _RADIOACC	=	0x00b1
                           0000B3   534 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   535 _RADIOADDR0	=	0x00b3
                           0000B2   536 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   537 _RADIOADDR1	=	0x00b2
                           00B2B3   538 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   539 _RADIOADDR	=	0xb2b3
                           0000B7   540 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   541 _RADIODATA0	=	0x00b7
                           0000B6   542 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   543 _RADIODATA1	=	0x00b6
                           0000B5   544 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   545 _RADIODATA2	=	0x00b5
                           0000B4   546 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   547 _RADIODATA3	=	0x00b4
                           B4B5B6B7   548 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   549 _RADIODATA	=	0xb4b5b6b7
                           0000BE   550 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   551 _RADIOSTAT0	=	0x00be
                           0000BF   552 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   553 _RADIOSTAT1	=	0x00bf
                           00BFBE   554 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   555 _RADIOSTAT	=	0xbfbe
                           0000DF   556 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   557 _SPCLKSRC	=	0x00df
                           0000DC   558 G$SPMODE$0$0 == 0x00dc
                           0000DC   559 _SPMODE	=	0x00dc
                           0000DE   560 G$SPSHREG$0$0 == 0x00de
                           0000DE   561 _SPSHREG	=	0x00de
                           0000DD   562 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   563 _SPSTATUS	=	0x00dd
                           00009A   564 G$T0CLKSRC$0$0 == 0x009a
                           00009A   565 _T0CLKSRC	=	0x009a
                           00009C   566 G$T0CNT0$0$0 == 0x009c
                           00009C   567 _T0CNT0	=	0x009c
                           00009D   568 G$T0CNT1$0$0 == 0x009d
                           00009D   569 _T0CNT1	=	0x009d
                           009D9C   570 G$T0CNT$0$0 == 0x9d9c
                           009D9C   571 _T0CNT	=	0x9d9c
                           000099   572 G$T0MODE$0$0 == 0x0099
                           000099   573 _T0MODE	=	0x0099
                           00009E   574 G$T0PERIOD0$0$0 == 0x009e
                           00009E   575 _T0PERIOD0	=	0x009e
                           00009F   576 G$T0PERIOD1$0$0 == 0x009f
                           00009F   577 _T0PERIOD1	=	0x009f
                           009F9E   578 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   579 _T0PERIOD	=	0x9f9e
                           00009B   580 G$T0STATUS$0$0 == 0x009b
                           00009B   581 _T0STATUS	=	0x009b
                           0000A2   582 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   583 _T1CLKSRC	=	0x00a2
                           0000A4   584 G$T1CNT0$0$0 == 0x00a4
                           0000A4   585 _T1CNT0	=	0x00a4
                           0000A5   586 G$T1CNT1$0$0 == 0x00a5
                           0000A5   587 _T1CNT1	=	0x00a5
                           00A5A4   588 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   589 _T1CNT	=	0xa5a4
                           0000A1   590 G$T1MODE$0$0 == 0x00a1
                           0000A1   591 _T1MODE	=	0x00a1
                           0000A6   592 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   593 _T1PERIOD0	=	0x00a6
                           0000A7   594 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   595 _T1PERIOD1	=	0x00a7
                           00A7A6   596 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   597 _T1PERIOD	=	0xa7a6
                           0000A3   598 G$T1STATUS$0$0 == 0x00a3
                           0000A3   599 _T1STATUS	=	0x00a3
                           0000AA   600 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   601 _T2CLKSRC	=	0x00aa
                           0000AC   602 G$T2CNT0$0$0 == 0x00ac
                           0000AC   603 _T2CNT0	=	0x00ac
                           0000AD   604 G$T2CNT1$0$0 == 0x00ad
                           0000AD   605 _T2CNT1	=	0x00ad
                           00ADAC   606 G$T2CNT$0$0 == 0xadac
                           00ADAC   607 _T2CNT	=	0xadac
                           0000A9   608 G$T2MODE$0$0 == 0x00a9
                           0000A9   609 _T2MODE	=	0x00a9
                           0000AE   610 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   611 _T2PERIOD0	=	0x00ae
                           0000AF   612 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   613 _T2PERIOD1	=	0x00af
                           00AFAE   614 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   615 _T2PERIOD	=	0xafae
                           0000AB   616 G$T2STATUS$0$0 == 0x00ab
                           0000AB   617 _T2STATUS	=	0x00ab
                           0000E4   618 G$U0CTRL$0$0 == 0x00e4
                           0000E4   619 _U0CTRL	=	0x00e4
                           0000E7   620 G$U0MODE$0$0 == 0x00e7
                           0000E7   621 _U0MODE	=	0x00e7
                           0000E6   622 G$U0SHREG$0$0 == 0x00e6
                           0000E6   623 _U0SHREG	=	0x00e6
                           0000E5   624 G$U0STATUS$0$0 == 0x00e5
                           0000E5   625 _U0STATUS	=	0x00e5
                           0000EC   626 G$U1CTRL$0$0 == 0x00ec
                           0000EC   627 _U1CTRL	=	0x00ec
                           0000EF   628 G$U1MODE$0$0 == 0x00ef
                           0000EF   629 _U1MODE	=	0x00ef
                           0000EE   630 G$U1SHREG$0$0 == 0x00ee
                           0000EE   631 _U1SHREG	=	0x00ee
                           0000ED   632 G$U1STATUS$0$0 == 0x00ed
                           0000ED   633 _U1STATUS	=	0x00ed
                           0000DA   634 G$WDTCFG$0$0 == 0x00da
                           0000DA   635 _WDTCFG	=	0x00da
                           0000DB   636 G$WDTRESET$0$0 == 0x00db
                           0000DB   637 _WDTRESET	=	0x00db
                           0000F1   638 G$WTCFGA$0$0 == 0x00f1
                           0000F1   639 _WTCFGA	=	0x00f1
                           0000F9   640 G$WTCFGB$0$0 == 0x00f9
                           0000F9   641 _WTCFGB	=	0x00f9
                           0000F2   642 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   643 _WTCNTA0	=	0x00f2
                           0000F3   644 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   645 _WTCNTA1	=	0x00f3
                           00F3F2   646 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   647 _WTCNTA	=	0xf3f2
                           0000FA   648 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   649 _WTCNTB0	=	0x00fa
                           0000FB   650 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   651 _WTCNTB1	=	0x00fb
                           00FBFA   652 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   653 _WTCNTB	=	0xfbfa
                           0000EB   654 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   655 _WTCNTR1	=	0x00eb
                           0000F4   656 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   657 _WTEVTA0	=	0x00f4
                           0000F5   658 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   659 _WTEVTA1	=	0x00f5
                           00F5F4   660 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   661 _WTEVTA	=	0xf5f4
                           0000F6   662 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   663 _WTEVTB0	=	0x00f6
                           0000F7   664 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   665 _WTEVTB1	=	0x00f7
                           00F7F6   666 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   667 _WTEVTB	=	0xf7f6
                           0000FC   668 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   669 _WTEVTC0	=	0x00fc
                           0000FD   670 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   671 _WTEVTC1	=	0x00fd
                           00FDFC   672 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   673 _WTEVTC	=	0xfdfc
                           0000FE   674 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   675 _WTEVTD0	=	0x00fe
                           0000FF   676 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   677 _WTEVTD1	=	0x00ff
                           00FFFE   678 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   679 _WTEVTD	=	0xfffe
                           0000E9   680 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   681 _WTIRQEN	=	0x00e9
                           0000EA   682 G$WTSTAT$0$0 == 0x00ea
                           0000EA   683 _WTSTAT	=	0x00ea
                                    684 ;--------------------------------------------------------
                                    685 ; special function bits
                                    686 ;--------------------------------------------------------
                                    687 	.area RSEG    (ABS,DATA)
      000000                        688 	.org 0x0000
                           0000E0   689 G$ACC_0$0$0 == 0x00e0
                           0000E0   690 _ACC_0	=	0x00e0
                           0000E1   691 G$ACC_1$0$0 == 0x00e1
                           0000E1   692 _ACC_1	=	0x00e1
                           0000E2   693 G$ACC_2$0$0 == 0x00e2
                           0000E2   694 _ACC_2	=	0x00e2
                           0000E3   695 G$ACC_3$0$0 == 0x00e3
                           0000E3   696 _ACC_3	=	0x00e3
                           0000E4   697 G$ACC_4$0$0 == 0x00e4
                           0000E4   698 _ACC_4	=	0x00e4
                           0000E5   699 G$ACC_5$0$0 == 0x00e5
                           0000E5   700 _ACC_5	=	0x00e5
                           0000E6   701 G$ACC_6$0$0 == 0x00e6
                           0000E6   702 _ACC_6	=	0x00e6
                           0000E7   703 G$ACC_7$0$0 == 0x00e7
                           0000E7   704 _ACC_7	=	0x00e7
                           0000F0   705 G$B_0$0$0 == 0x00f0
                           0000F0   706 _B_0	=	0x00f0
                           0000F1   707 G$B_1$0$0 == 0x00f1
                           0000F1   708 _B_1	=	0x00f1
                           0000F2   709 G$B_2$0$0 == 0x00f2
                           0000F2   710 _B_2	=	0x00f2
                           0000F3   711 G$B_3$0$0 == 0x00f3
                           0000F3   712 _B_3	=	0x00f3
                           0000F4   713 G$B_4$0$0 == 0x00f4
                           0000F4   714 _B_4	=	0x00f4
                           0000F5   715 G$B_5$0$0 == 0x00f5
                           0000F5   716 _B_5	=	0x00f5
                           0000F6   717 G$B_6$0$0 == 0x00f6
                           0000F6   718 _B_6	=	0x00f6
                           0000F7   719 G$B_7$0$0 == 0x00f7
                           0000F7   720 _B_7	=	0x00f7
                           0000A0   721 G$E2IE_0$0$0 == 0x00a0
                           0000A0   722 _E2IE_0	=	0x00a0
                           0000A1   723 G$E2IE_1$0$0 == 0x00a1
                           0000A1   724 _E2IE_1	=	0x00a1
                           0000A2   725 G$E2IE_2$0$0 == 0x00a2
                           0000A2   726 _E2IE_2	=	0x00a2
                           0000A3   727 G$E2IE_3$0$0 == 0x00a3
                           0000A3   728 _E2IE_3	=	0x00a3
                           0000A4   729 G$E2IE_4$0$0 == 0x00a4
                           0000A4   730 _E2IE_4	=	0x00a4
                           0000A5   731 G$E2IE_5$0$0 == 0x00a5
                           0000A5   732 _E2IE_5	=	0x00a5
                           0000A6   733 G$E2IE_6$0$0 == 0x00a6
                           0000A6   734 _E2IE_6	=	0x00a6
                           0000A7   735 G$E2IE_7$0$0 == 0x00a7
                           0000A7   736 _E2IE_7	=	0x00a7
                           0000C0   737 G$E2IP_0$0$0 == 0x00c0
                           0000C0   738 _E2IP_0	=	0x00c0
                           0000C1   739 G$E2IP_1$0$0 == 0x00c1
                           0000C1   740 _E2IP_1	=	0x00c1
                           0000C2   741 G$E2IP_2$0$0 == 0x00c2
                           0000C2   742 _E2IP_2	=	0x00c2
                           0000C3   743 G$E2IP_3$0$0 == 0x00c3
                           0000C3   744 _E2IP_3	=	0x00c3
                           0000C4   745 G$E2IP_4$0$0 == 0x00c4
                           0000C4   746 _E2IP_4	=	0x00c4
                           0000C5   747 G$E2IP_5$0$0 == 0x00c5
                           0000C5   748 _E2IP_5	=	0x00c5
                           0000C6   749 G$E2IP_6$0$0 == 0x00c6
                           0000C6   750 _E2IP_6	=	0x00c6
                           0000C7   751 G$E2IP_7$0$0 == 0x00c7
                           0000C7   752 _E2IP_7	=	0x00c7
                           000098   753 G$EIE_0$0$0 == 0x0098
                           000098   754 _EIE_0	=	0x0098
                           000099   755 G$EIE_1$0$0 == 0x0099
                           000099   756 _EIE_1	=	0x0099
                           00009A   757 G$EIE_2$0$0 == 0x009a
                           00009A   758 _EIE_2	=	0x009a
                           00009B   759 G$EIE_3$0$0 == 0x009b
                           00009B   760 _EIE_3	=	0x009b
                           00009C   761 G$EIE_4$0$0 == 0x009c
                           00009C   762 _EIE_4	=	0x009c
                           00009D   763 G$EIE_5$0$0 == 0x009d
                           00009D   764 _EIE_5	=	0x009d
                           00009E   765 G$EIE_6$0$0 == 0x009e
                           00009E   766 _EIE_6	=	0x009e
                           00009F   767 G$EIE_7$0$0 == 0x009f
                           00009F   768 _EIE_7	=	0x009f
                           0000B0   769 G$EIP_0$0$0 == 0x00b0
                           0000B0   770 _EIP_0	=	0x00b0
                           0000B1   771 G$EIP_1$0$0 == 0x00b1
                           0000B1   772 _EIP_1	=	0x00b1
                           0000B2   773 G$EIP_2$0$0 == 0x00b2
                           0000B2   774 _EIP_2	=	0x00b2
                           0000B3   775 G$EIP_3$0$0 == 0x00b3
                           0000B3   776 _EIP_3	=	0x00b3
                           0000B4   777 G$EIP_4$0$0 == 0x00b4
                           0000B4   778 _EIP_4	=	0x00b4
                           0000B5   779 G$EIP_5$0$0 == 0x00b5
                           0000B5   780 _EIP_5	=	0x00b5
                           0000B6   781 G$EIP_6$0$0 == 0x00b6
                           0000B6   782 _EIP_6	=	0x00b6
                           0000B7   783 G$EIP_7$0$0 == 0x00b7
                           0000B7   784 _EIP_7	=	0x00b7
                           0000A8   785 G$IE_0$0$0 == 0x00a8
                           0000A8   786 _IE_0	=	0x00a8
                           0000A9   787 G$IE_1$0$0 == 0x00a9
                           0000A9   788 _IE_1	=	0x00a9
                           0000AA   789 G$IE_2$0$0 == 0x00aa
                           0000AA   790 _IE_2	=	0x00aa
                           0000AB   791 G$IE_3$0$0 == 0x00ab
                           0000AB   792 _IE_3	=	0x00ab
                           0000AC   793 G$IE_4$0$0 == 0x00ac
                           0000AC   794 _IE_4	=	0x00ac
                           0000AD   795 G$IE_5$0$0 == 0x00ad
                           0000AD   796 _IE_5	=	0x00ad
                           0000AE   797 G$IE_6$0$0 == 0x00ae
                           0000AE   798 _IE_6	=	0x00ae
                           0000AF   799 G$IE_7$0$0 == 0x00af
                           0000AF   800 _IE_7	=	0x00af
                           0000AF   801 G$EA$0$0 == 0x00af
                           0000AF   802 _EA	=	0x00af
                           0000B8   803 G$IP_0$0$0 == 0x00b8
                           0000B8   804 _IP_0	=	0x00b8
                           0000B9   805 G$IP_1$0$0 == 0x00b9
                           0000B9   806 _IP_1	=	0x00b9
                           0000BA   807 G$IP_2$0$0 == 0x00ba
                           0000BA   808 _IP_2	=	0x00ba
                           0000BB   809 G$IP_3$0$0 == 0x00bb
                           0000BB   810 _IP_3	=	0x00bb
                           0000BC   811 G$IP_4$0$0 == 0x00bc
                           0000BC   812 _IP_4	=	0x00bc
                           0000BD   813 G$IP_5$0$0 == 0x00bd
                           0000BD   814 _IP_5	=	0x00bd
                           0000BE   815 G$IP_6$0$0 == 0x00be
                           0000BE   816 _IP_6	=	0x00be
                           0000BF   817 G$IP_7$0$0 == 0x00bf
                           0000BF   818 _IP_7	=	0x00bf
                           0000D0   819 G$P$0$0 == 0x00d0
                           0000D0   820 _P	=	0x00d0
                           0000D1   821 G$F1$0$0 == 0x00d1
                           0000D1   822 _F1	=	0x00d1
                           0000D2   823 G$OV$0$0 == 0x00d2
                           0000D2   824 _OV	=	0x00d2
                           0000D3   825 G$RS0$0$0 == 0x00d3
                           0000D3   826 _RS0	=	0x00d3
                           0000D4   827 G$RS1$0$0 == 0x00d4
                           0000D4   828 _RS1	=	0x00d4
                           0000D5   829 G$F0$0$0 == 0x00d5
                           0000D5   830 _F0	=	0x00d5
                           0000D6   831 G$AC$0$0 == 0x00d6
                           0000D6   832 _AC	=	0x00d6
                           0000D7   833 G$CY$0$0 == 0x00d7
                           0000D7   834 _CY	=	0x00d7
                           0000C8   835 G$PINA_0$0$0 == 0x00c8
                           0000C8   836 _PINA_0	=	0x00c8
                           0000C9   837 G$PINA_1$0$0 == 0x00c9
                           0000C9   838 _PINA_1	=	0x00c9
                           0000CA   839 G$PINA_2$0$0 == 0x00ca
                           0000CA   840 _PINA_2	=	0x00ca
                           0000CB   841 G$PINA_3$0$0 == 0x00cb
                           0000CB   842 _PINA_3	=	0x00cb
                           0000CC   843 G$PINA_4$0$0 == 0x00cc
                           0000CC   844 _PINA_4	=	0x00cc
                           0000CD   845 G$PINA_5$0$0 == 0x00cd
                           0000CD   846 _PINA_5	=	0x00cd
                           0000CE   847 G$PINA_6$0$0 == 0x00ce
                           0000CE   848 _PINA_6	=	0x00ce
                           0000CF   849 G$PINA_7$0$0 == 0x00cf
                           0000CF   850 _PINA_7	=	0x00cf
                           0000E8   851 G$PINB_0$0$0 == 0x00e8
                           0000E8   852 _PINB_0	=	0x00e8
                           0000E9   853 G$PINB_1$0$0 == 0x00e9
                           0000E9   854 _PINB_1	=	0x00e9
                           0000EA   855 G$PINB_2$0$0 == 0x00ea
                           0000EA   856 _PINB_2	=	0x00ea
                           0000EB   857 G$PINB_3$0$0 == 0x00eb
                           0000EB   858 _PINB_3	=	0x00eb
                           0000EC   859 G$PINB_4$0$0 == 0x00ec
                           0000EC   860 _PINB_4	=	0x00ec
                           0000ED   861 G$PINB_5$0$0 == 0x00ed
                           0000ED   862 _PINB_5	=	0x00ed
                           0000EE   863 G$PINB_6$0$0 == 0x00ee
                           0000EE   864 _PINB_6	=	0x00ee
                           0000EF   865 G$PINB_7$0$0 == 0x00ef
                           0000EF   866 _PINB_7	=	0x00ef
                           0000F8   867 G$PINC_0$0$0 == 0x00f8
                           0000F8   868 _PINC_0	=	0x00f8
                           0000F9   869 G$PINC_1$0$0 == 0x00f9
                           0000F9   870 _PINC_1	=	0x00f9
                           0000FA   871 G$PINC_2$0$0 == 0x00fa
                           0000FA   872 _PINC_2	=	0x00fa
                           0000FB   873 G$PINC_3$0$0 == 0x00fb
                           0000FB   874 _PINC_3	=	0x00fb
                           0000FC   875 G$PINC_4$0$0 == 0x00fc
                           0000FC   876 _PINC_4	=	0x00fc
                           0000FD   877 G$PINC_5$0$0 == 0x00fd
                           0000FD   878 _PINC_5	=	0x00fd
                           0000FE   879 G$PINC_6$0$0 == 0x00fe
                           0000FE   880 _PINC_6	=	0x00fe
                           0000FF   881 G$PINC_7$0$0 == 0x00ff
                           0000FF   882 _PINC_7	=	0x00ff
                           000080   883 G$PORTA_0$0$0 == 0x0080
                           000080   884 _PORTA_0	=	0x0080
                           000081   885 G$PORTA_1$0$0 == 0x0081
                           000081   886 _PORTA_1	=	0x0081
                           000082   887 G$PORTA_2$0$0 == 0x0082
                           000082   888 _PORTA_2	=	0x0082
                           000083   889 G$PORTA_3$0$0 == 0x0083
                           000083   890 _PORTA_3	=	0x0083
                           000084   891 G$PORTA_4$0$0 == 0x0084
                           000084   892 _PORTA_4	=	0x0084
                           000085   893 G$PORTA_5$0$0 == 0x0085
                           000085   894 _PORTA_5	=	0x0085
                           000086   895 G$PORTA_6$0$0 == 0x0086
                           000086   896 _PORTA_6	=	0x0086
                           000087   897 G$PORTA_7$0$0 == 0x0087
                           000087   898 _PORTA_7	=	0x0087
                           000088   899 G$PORTB_0$0$0 == 0x0088
                           000088   900 _PORTB_0	=	0x0088
                           000089   901 G$PORTB_1$0$0 == 0x0089
                           000089   902 _PORTB_1	=	0x0089
                           00008A   903 G$PORTB_2$0$0 == 0x008a
                           00008A   904 _PORTB_2	=	0x008a
                           00008B   905 G$PORTB_3$0$0 == 0x008b
                           00008B   906 _PORTB_3	=	0x008b
                           00008C   907 G$PORTB_4$0$0 == 0x008c
                           00008C   908 _PORTB_4	=	0x008c
                           00008D   909 G$PORTB_5$0$0 == 0x008d
                           00008D   910 _PORTB_5	=	0x008d
                           00008E   911 G$PORTB_6$0$0 == 0x008e
                           00008E   912 _PORTB_6	=	0x008e
                           00008F   913 G$PORTB_7$0$0 == 0x008f
                           00008F   914 _PORTB_7	=	0x008f
                           000090   915 G$PORTC_0$0$0 == 0x0090
                           000090   916 _PORTC_0	=	0x0090
                           000091   917 G$PORTC_1$0$0 == 0x0091
                           000091   918 _PORTC_1	=	0x0091
                           000092   919 G$PORTC_2$0$0 == 0x0092
                           000092   920 _PORTC_2	=	0x0092
                           000093   921 G$PORTC_3$0$0 == 0x0093
                           000093   922 _PORTC_3	=	0x0093
                           000094   923 G$PORTC_4$0$0 == 0x0094
                           000094   924 _PORTC_4	=	0x0094
                           000095   925 G$PORTC_5$0$0 == 0x0095
                           000095   926 _PORTC_5	=	0x0095
                           000096   927 G$PORTC_6$0$0 == 0x0096
                           000096   928 _PORTC_6	=	0x0096
                           000097   929 G$PORTC_7$0$0 == 0x0097
                           000097   930 _PORTC_7	=	0x0097
                                    931 ;--------------------------------------------------------
                                    932 ; overlayable register banks
                                    933 ;--------------------------------------------------------
                                    934 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        935 	.ds 8
                                    936 ;--------------------------------------------------------
                                    937 ; internal ram data
                                    938 ;--------------------------------------------------------
                                    939 	.area DSEG    (DATA)
                                    940 ;--------------------------------------------------------
                                    941 ; overlayable items in internal ram 
                                    942 ;--------------------------------------------------------
                                    943 	.area	OSEG    (OVR,DATA)
                                    944 ;--------------------------------------------------------
                                    945 ; indirectly addressable internal ram data
                                    946 ;--------------------------------------------------------
                                    947 	.area ISEG    (DATA)
                                    948 ;--------------------------------------------------------
                                    949 ; absolute internal ram data
                                    950 ;--------------------------------------------------------
                                    951 	.area IABS    (ABS,DATA)
                                    952 	.area IABS    (ABS,DATA)
                                    953 ;--------------------------------------------------------
                                    954 ; bit data
                                    955 ;--------------------------------------------------------
                                    956 	.area BSEG    (BIT)
                                    957 ;--------------------------------------------------------
                                    958 ; paged external ram data
                                    959 ;--------------------------------------------------------
                                    960 	.area PSEG    (PAG,XDATA)
                                    961 ;--------------------------------------------------------
                                    962 ; external ram data
                                    963 ;--------------------------------------------------------
                                    964 	.area XSEG    (XDATA)
                           007020   965 G$ADCCH0VAL0$0$0 == 0x7020
                           007020   966 _ADCCH0VAL0	=	0x7020
                           007021   967 G$ADCCH0VAL1$0$0 == 0x7021
                           007021   968 _ADCCH0VAL1	=	0x7021
                           007020   969 G$ADCCH0VAL$0$0 == 0x7020
                           007020   970 _ADCCH0VAL	=	0x7020
                           007022   971 G$ADCCH1VAL0$0$0 == 0x7022
                           007022   972 _ADCCH1VAL0	=	0x7022
                           007023   973 G$ADCCH1VAL1$0$0 == 0x7023
                           007023   974 _ADCCH1VAL1	=	0x7023
                           007022   975 G$ADCCH1VAL$0$0 == 0x7022
                           007022   976 _ADCCH1VAL	=	0x7022
                           007024   977 G$ADCCH2VAL0$0$0 == 0x7024
                           007024   978 _ADCCH2VAL0	=	0x7024
                           007025   979 G$ADCCH2VAL1$0$0 == 0x7025
                           007025   980 _ADCCH2VAL1	=	0x7025
                           007024   981 G$ADCCH2VAL$0$0 == 0x7024
                           007024   982 _ADCCH2VAL	=	0x7024
                           007026   983 G$ADCCH3VAL0$0$0 == 0x7026
                           007026   984 _ADCCH3VAL0	=	0x7026
                           007027   985 G$ADCCH3VAL1$0$0 == 0x7027
                           007027   986 _ADCCH3VAL1	=	0x7027
                           007026   987 G$ADCCH3VAL$0$0 == 0x7026
                           007026   988 _ADCCH3VAL	=	0x7026
                           007028   989 G$ADCTUNE0$0$0 == 0x7028
                           007028   990 _ADCTUNE0	=	0x7028
                           007029   991 G$ADCTUNE1$0$0 == 0x7029
                           007029   992 _ADCTUNE1	=	0x7029
                           00702A   993 G$ADCTUNE2$0$0 == 0x702a
                           00702A   994 _ADCTUNE2	=	0x702a
                           007010   995 G$DMA0ADDR0$0$0 == 0x7010
                           007010   996 _DMA0ADDR0	=	0x7010
                           007011   997 G$DMA0ADDR1$0$0 == 0x7011
                           007011   998 _DMA0ADDR1	=	0x7011
                           007010   999 G$DMA0ADDR$0$0 == 0x7010
                           007010  1000 _DMA0ADDR	=	0x7010
                           007014  1001 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1002 _DMA0CONFIG	=	0x7014
                           007012  1003 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1004 _DMA1ADDR0	=	0x7012
                           007013  1005 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1006 _DMA1ADDR1	=	0x7013
                           007012  1007 G$DMA1ADDR$0$0 == 0x7012
                           007012  1008 _DMA1ADDR	=	0x7012
                           007015  1009 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1010 _DMA1CONFIG	=	0x7015
                           007070  1011 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1012 _FRCOSCCONFIG	=	0x7070
                           007071  1013 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1014 _FRCOSCCTRL	=	0x7071
                           007076  1015 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1016 _FRCOSCFREQ0	=	0x7076
                           007077  1017 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1018 _FRCOSCFREQ1	=	0x7077
                           007076  1019 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1020 _FRCOSCFREQ	=	0x7076
                           007072  1021 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1022 _FRCOSCKFILT0	=	0x7072
                           007073  1023 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1024 _FRCOSCKFILT1	=	0x7073
                           007072  1025 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1026 _FRCOSCKFILT	=	0x7072
                           007078  1027 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1028 _FRCOSCPER0	=	0x7078
                           007079  1029 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1030 _FRCOSCPER1	=	0x7079
                           007078  1031 G$FRCOSCPER$0$0 == 0x7078
                           007078  1032 _FRCOSCPER	=	0x7078
                           007074  1033 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1034 _FRCOSCREF0	=	0x7074
                           007075  1035 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1036 _FRCOSCREF1	=	0x7075
                           007074  1037 G$FRCOSCREF$0$0 == 0x7074
                           007074  1038 _FRCOSCREF	=	0x7074
                           007007  1039 G$ANALOGA$0$0 == 0x7007
                           007007  1040 _ANALOGA	=	0x7007
                           00700C  1041 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1042 _GPIOENABLE	=	0x700c
                           007003  1043 G$EXTIRQ$0$0 == 0x7003
                           007003  1044 _EXTIRQ	=	0x7003
                           007000  1045 G$INTCHGA$0$0 == 0x7000
                           007000  1046 _INTCHGA	=	0x7000
                           007001  1047 G$INTCHGB$0$0 == 0x7001
                           007001  1048 _INTCHGB	=	0x7001
                           007002  1049 G$INTCHGC$0$0 == 0x7002
                           007002  1050 _INTCHGC	=	0x7002
                           007008  1051 G$PALTA$0$0 == 0x7008
                           007008  1052 _PALTA	=	0x7008
                           007009  1053 G$PALTB$0$0 == 0x7009
                           007009  1054 _PALTB	=	0x7009
                           00700A  1055 G$PALTC$0$0 == 0x700a
                           00700A  1056 _PALTC	=	0x700a
                           007046  1057 G$PALTRADIO$0$0 == 0x7046
                           007046  1058 _PALTRADIO	=	0x7046
                           007004  1059 G$PINCHGA$0$0 == 0x7004
                           007004  1060 _PINCHGA	=	0x7004
                           007005  1061 G$PINCHGB$0$0 == 0x7005
                           007005  1062 _PINCHGB	=	0x7005
                           007006  1063 G$PINCHGC$0$0 == 0x7006
                           007006  1064 _PINCHGC	=	0x7006
                           00700B  1065 G$PINSEL$0$0 == 0x700b
                           00700B  1066 _PINSEL	=	0x700b
                           007060  1067 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1068 _LPOSCCONFIG	=	0x7060
                           007066  1069 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1070 _LPOSCFREQ0	=	0x7066
                           007067  1071 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1072 _LPOSCFREQ1	=	0x7067
                           007066  1073 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1074 _LPOSCFREQ	=	0x7066
                           007062  1075 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1076 _LPOSCKFILT0	=	0x7062
                           007063  1077 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1078 _LPOSCKFILT1	=	0x7063
                           007062  1079 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1080 _LPOSCKFILT	=	0x7062
                           007068  1081 G$LPOSCPER0$0$0 == 0x7068
                           007068  1082 _LPOSCPER0	=	0x7068
                           007069  1083 G$LPOSCPER1$0$0 == 0x7069
                           007069  1084 _LPOSCPER1	=	0x7069
                           007068  1085 G$LPOSCPER$0$0 == 0x7068
                           007068  1086 _LPOSCPER	=	0x7068
                           007064  1087 G$LPOSCREF0$0$0 == 0x7064
                           007064  1088 _LPOSCREF0	=	0x7064
                           007065  1089 G$LPOSCREF1$0$0 == 0x7065
                           007065  1090 _LPOSCREF1	=	0x7065
                           007064  1091 G$LPOSCREF$0$0 == 0x7064
                           007064  1092 _LPOSCREF	=	0x7064
                           007054  1093 G$LPXOSCGM$0$0 == 0x7054
                           007054  1094 _LPXOSCGM	=	0x7054
                           007F01  1095 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1096 _MISCCTRL	=	0x7f01
                           007053  1097 G$OSCCALIB$0$0 == 0x7053
                           007053  1098 _OSCCALIB	=	0x7053
                           007050  1099 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1100 _OSCFORCERUN	=	0x7050
                           007052  1101 G$OSCREADY$0$0 == 0x7052
                           007052  1102 _OSCREADY	=	0x7052
                           007051  1103 G$OSCRUN$0$0 == 0x7051
                           007051  1104 _OSCRUN	=	0x7051
                           007040  1105 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1106 _RADIOFDATAADDR0	=	0x7040
                           007041  1107 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1108 _RADIOFDATAADDR1	=	0x7041
                           007040  1109 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1110 _RADIOFDATAADDR	=	0x7040
                           007042  1111 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1112 _RADIOFSTATADDR0	=	0x7042
                           007043  1113 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1114 _RADIOFSTATADDR1	=	0x7043
                           007042  1115 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1116 _RADIOFSTATADDR	=	0x7042
                           007044  1117 G$RADIOMUX$0$0 == 0x7044
                           007044  1118 _RADIOMUX	=	0x7044
                           007084  1119 G$SCRATCH0$0$0 == 0x7084
                           007084  1120 _SCRATCH0	=	0x7084
                           007085  1121 G$SCRATCH1$0$0 == 0x7085
                           007085  1122 _SCRATCH1	=	0x7085
                           007086  1123 G$SCRATCH2$0$0 == 0x7086
                           007086  1124 _SCRATCH2	=	0x7086
                           007087  1125 G$SCRATCH3$0$0 == 0x7087
                           007087  1126 _SCRATCH3	=	0x7087
                           007F00  1127 G$SILICONREV$0$0 == 0x7f00
                           007F00  1128 _SILICONREV	=	0x7f00
                           007F19  1129 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1130 _XTALAMPL	=	0x7f19
                           007F18  1131 G$XTALOSC$0$0 == 0x7f18
                           007F18  1132 _XTALOSC	=	0x7f18
                           007F1A  1133 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1134 _XTALREADY	=	0x7f1a
                           00FC06  1135 Fmisc$flash_deviceid$0$0 == 0xfc06
                           00FC06  1136 _flash_deviceid	=	0xfc06
                           00FC00  1137 Fmisc$flash_calsector$0$0 == 0xfc00
                           00FC00  1138 _flash_calsector	=	0xfc00
                           000000  1139 Fmisc$delaymstimer$0$0==.
      000298                       1140 _delaymstimer:
      000298                       1141 	.ds 8
                                   1142 ;--------------------------------------------------------
                                   1143 ; absolute external ram data
                                   1144 ;--------------------------------------------------------
                                   1145 	.area XABS    (ABS,XDATA)
                                   1146 ;--------------------------------------------------------
                                   1147 ; external initialized ram data
                                   1148 ;--------------------------------------------------------
                                   1149 	.area XISEG   (XDATA)
                                   1150 	.area HOME    (CODE)
                                   1151 	.area GSINIT0 (CODE)
                                   1152 	.area GSINIT1 (CODE)
                                   1153 	.area GSINIT2 (CODE)
                                   1154 	.area GSINIT3 (CODE)
                                   1155 	.area GSINIT4 (CODE)
                                   1156 	.area GSINIT5 (CODE)
                                   1157 	.area GSINIT  (CODE)
                                   1158 	.area GSFINAL (CODE)
                                   1159 	.area CSEG    (CODE)
                                   1160 ;--------------------------------------------------------
                                   1161 ; global & static initialisations
                                   1162 ;--------------------------------------------------------
                                   1163 	.area HOME    (CODE)
                                   1164 	.area GSINIT  (CODE)
                                   1165 	.area GSFINAL (CODE)
                                   1166 	.area GSINIT  (CODE)
                                   1167 ;--------------------------------------------------------
                                   1168 ; Home
                                   1169 ;--------------------------------------------------------
                                   1170 	.area HOME    (CODE)
                                   1171 	.area HOME    (CODE)
                                   1172 ;--------------------------------------------------------
                                   1173 ; code
                                   1174 ;--------------------------------------------------------
                                   1175 	.area CSEG    (CODE)
                                   1176 ;------------------------------------------------------------
                                   1177 ;Allocation info for local variables in function 'lcd2_display_radio_error'
                                   1178 ;------------------------------------------------------------
                                   1179 ;err                       Allocated to registers r7 
                                   1180 ;p                         Allocated to registers r5 r6 
                                   1181 ;------------------------------------------------------------
                           000000  1182 	G$lcd2_display_radio_error$0$0 ==.
                           000000  1183 	C$misc.c$74$0$0 ==.
                                   1184 ;	..\COMMON\misc.c:74: void lcd2_display_radio_error(uint8_t err)
                                   1185 ;	-----------------------------------------
                                   1186 ;	 function lcd2_display_radio_error
                                   1187 ;	-----------------------------------------
      003F85                       1188 _lcd2_display_radio_error:
                           000007  1189 	ar7 = 0x07
                           000006  1190 	ar6 = 0x06
                           000005  1191 	ar5 = 0x05
                           000004  1192 	ar4 = 0x04
                           000003  1193 	ar3 = 0x03
                           000002  1194 	ar2 = 0x02
                           000001  1195 	ar1 = 0x01
                           000000  1196 	ar0 = 0x00
      003F85 AF 82            [24] 1197 	mov	r7,dpl
                           000002  1198 	C$misc.c$76$1$0 ==.
                                   1199 ;	..\COMMON\misc.c:76: const struct errtbl __code *p = errtbl;
      003F87 7D F7            [12] 1200 	mov	r5,#_errtbl
      003F89 7E 64            [12] 1201 	mov	r6,#(_errtbl >> 8)
                           000006  1202 	C$misc.c$77$1$337 ==.
                                   1203 ;	..\COMMON\misc.c:77: do {
      003F8B 8D 03            [24] 1204 	mov	ar3,r5
      003F8D 8E 04            [24] 1205 	mov	ar4,r6
      003F8F                       1206 00103$:
                           00000A  1207 	C$misc.c$78$2$338 ==.
                                   1208 ;	..\COMMON\misc.c:78: if (p->errcode == err) {
      003F8F 8B 82            [24] 1209 	mov	dpl,r3
      003F91 8C 83            [24] 1210 	mov	dph,r4
      003F93 E4               [12] 1211 	clr	a
      003F94 93               [24] 1212 	movc	a,@a+dptr
      003F95 FA               [12] 1213 	mov	r2,a
      003F96 B5 07 27         [24] 1214 	cjne	a,ar7,00102$
                           000014  1215 	C$misc.c$79$3$339 ==.
                                   1216 ;	..\COMMON\misc.c:79: display_setpos(0);
      003F99 75 82 00         [24] 1217 	mov	dpl,#0x00
      003F9C C0 06            [24] 1218 	push	ar6
      003F9E C0 05            [24] 1219 	push	ar5
      003FA0 12 0E C1         [24] 1220 	lcall	_com0_setpos
      003FA3 D0 05            [24] 1221 	pop	ar5
      003FA5 D0 06            [24] 1222 	pop	ar6
                           000022  1223 	C$misc.c$80$3$339 ==.
                                   1224 ;	..\COMMON\misc.c:80: display_writestr(p->msg);
      003FA7 8D 82            [24] 1225 	mov	dpl,r5
      003FA9 8E 83            [24] 1226 	mov	dph,r6
      003FAB A3               [24] 1227 	inc	dptr
      003FAC E4               [12] 1228 	clr	a
      003FAD 93               [24] 1229 	movc	a,@a+dptr
      003FAE F9               [12] 1230 	mov	r1,a
      003FAF A3               [24] 1231 	inc	dptr
      003FB0 E4               [12] 1232 	clr	a
      003FB1 93               [24] 1233 	movc	a,@a+dptr
      003FB2 F8               [12] 1234 	mov	r0,a
      003FB3 7A 80            [12] 1235 	mov	r2,#0x80
      003FB5 89 82            [24] 1236 	mov	dpl,r1
      003FB7 88 83            [24] 1237 	mov	dph,r0
      003FB9 8A F0            [24] 1238 	mov	b,r2
      003FBB 12 0E DD         [24] 1239 	lcall	_com0_writestr
                           000039  1240 	C$misc.c$81$3$339 ==.
                                   1241 ;	..\COMMON\misc.c:81: return;
      003FBE 80 13            [24] 1242 	sjmp	00106$
      003FC0                       1243 00102$:
                           00003B  1244 	C$misc.c$83$2$338 ==.
                                   1245 ;	..\COMMON\misc.c:83: ++p;
      003FC0 74 03            [12] 1246 	mov	a,#0x03
      003FC2 2B               [12] 1247 	add	a,r3
      003FC3 FB               [12] 1248 	mov	r3,a
      003FC4 E4               [12] 1249 	clr	a
      003FC5 3C               [12] 1250 	addc	a,r4
      003FC6 FC               [12] 1251 	mov	r4,a
      003FC7 8B 05            [24] 1252 	mov	ar5,r3
      003FC9 8C 06            [24] 1253 	mov	ar6,r4
                           000046  1254 	C$misc.c$84$1$337 ==.
                                   1255 ;	..\COMMON\misc.c:84: } while (p->errcode != AXRADIO_ERR_NOERROR);
      003FCB 8B 82            [24] 1256 	mov	dpl,r3
      003FCD 8C 83            [24] 1257 	mov	dph,r4
      003FCF E4               [12] 1258 	clr	a
      003FD0 93               [24] 1259 	movc	a,@a+dptr
      003FD1 70 BC            [24] 1260 	jnz	00103$
      003FD3                       1261 00106$:
                           00004E  1262 	C$misc.c$85$1$337 ==.
                           00004E  1263 	XG$lcd2_display_radio_error$0$0 ==.
      003FD3 22               [24] 1264 	ret
                                   1265 ;------------------------------------------------------------
                                   1266 ;Allocation info for local variables in function 'com0_display_radio_error'
                                   1267 ;------------------------------------------------------------
                                   1268 ;err                       Allocated to registers r7 
                                   1269 ;p                         Allocated to registers r5 r6 
                                   1270 ;------------------------------------------------------------
                           00004F  1271 	G$com0_display_radio_error$0$0 ==.
                           00004F  1272 	C$misc.c$88$1$337 ==.
                                   1273 ;	..\COMMON\misc.c:88: void com0_display_radio_error(uint8_t err)
                                   1274 ;	-----------------------------------------
                                   1275 ;	 function com0_display_radio_error
                                   1276 ;	-----------------------------------------
      003FD4                       1277 _com0_display_radio_error:
      003FD4 AF 82            [24] 1278 	mov	r7,dpl
                           000051  1279 	C$misc.c$90$1$337 ==.
                                   1280 ;	..\COMMON\misc.c:90: const struct errtbl __code *p = errtbl;
      003FD6 7D F7            [12] 1281 	mov	r5,#_errtbl
      003FD8 7E 64            [12] 1282 	mov	r6,#(_errtbl >> 8)
                           000055  1283 	C$misc.c$91$1$341 ==.
                                   1284 ;	..\COMMON\misc.c:91: do {
      003FDA 8D 03            [24] 1285 	mov	ar3,r5
      003FDC 8E 04            [24] 1286 	mov	ar4,r6
      003FDE                       1287 00103$:
                           000059  1288 	C$misc.c$92$2$342 ==.
                                   1289 ;	..\COMMON\misc.c:92: if (p->errcode == err) {
      003FDE 8B 82            [24] 1290 	mov	dpl,r3
      003FE0 8C 83            [24] 1291 	mov	dph,r4
      003FE2 E4               [12] 1292 	clr	a
      003FE3 93               [24] 1293 	movc	a,@a+dptr
      003FE4 FA               [12] 1294 	mov	r2,a
      003FE5 B5 07 27         [24] 1295 	cjne	a,ar7,00102$
                           000063  1296 	C$misc.c$93$3$343 ==.
                                   1297 ;	..\COMMON\misc.c:93: com0_setpos(0);
      003FE8 75 82 00         [24] 1298 	mov	dpl,#0x00
      003FEB C0 06            [24] 1299 	push	ar6
      003FED C0 05            [24] 1300 	push	ar5
      003FEF 12 0E C1         [24] 1301 	lcall	_com0_setpos
      003FF2 D0 05            [24] 1302 	pop	ar5
      003FF4 D0 06            [24] 1303 	pop	ar6
                           000071  1304 	C$misc.c$94$3$343 ==.
                                   1305 ;	..\COMMON\misc.c:94: com0_writestr(p->msg);
      003FF6 8D 82            [24] 1306 	mov	dpl,r5
      003FF8 8E 83            [24] 1307 	mov	dph,r6
      003FFA A3               [24] 1308 	inc	dptr
      003FFB E4               [12] 1309 	clr	a
      003FFC 93               [24] 1310 	movc	a,@a+dptr
      003FFD F9               [12] 1311 	mov	r1,a
      003FFE A3               [24] 1312 	inc	dptr
      003FFF E4               [12] 1313 	clr	a
      004000 93               [24] 1314 	movc	a,@a+dptr
      004001 F8               [12] 1315 	mov	r0,a
      004002 7A 80            [12] 1316 	mov	r2,#0x80
      004004 89 82            [24] 1317 	mov	dpl,r1
      004006 88 83            [24] 1318 	mov	dph,r0
      004008 8A F0            [24] 1319 	mov	b,r2
      00400A 12 0E DD         [24] 1320 	lcall	_com0_writestr
                           000088  1321 	C$misc.c$95$3$343 ==.
                                   1322 ;	..\COMMON\misc.c:95: return;
      00400D 80 13            [24] 1323 	sjmp	00106$
      00400F                       1324 00102$:
                           00008A  1325 	C$misc.c$97$2$342 ==.
                                   1326 ;	..\COMMON\misc.c:97: ++p;
      00400F 74 03            [12] 1327 	mov	a,#0x03
      004011 2B               [12] 1328 	add	a,r3
      004012 FB               [12] 1329 	mov	r3,a
      004013 E4               [12] 1330 	clr	a
      004014 3C               [12] 1331 	addc	a,r4
      004015 FC               [12] 1332 	mov	r4,a
      004016 8B 05            [24] 1333 	mov	ar5,r3
      004018 8C 06            [24] 1334 	mov	ar6,r4
                           000095  1335 	C$misc.c$98$1$341 ==.
                                   1336 ;	..\COMMON\misc.c:98: } while (p->errcode != AXRADIO_ERR_NOERROR);
      00401A 8B 82            [24] 1337 	mov	dpl,r3
      00401C 8C 83            [24] 1338 	mov	dph,r4
      00401E E4               [12] 1339 	clr	a
      00401F 93               [24] 1340 	movc	a,@a+dptr
      004020 70 BC            [24] 1341 	jnz	00103$
      004022                       1342 00106$:
                           00009D  1343 	C$misc.c$99$1$341 ==.
                           00009D  1344 	XG$com0_display_radio_error$0$0 ==.
      004022 22               [24] 1345 	ret
                                   1346 ;------------------------------------------------------------
                                   1347 ;Allocation info for local variables in function 'dbglink_display_radio_error'
                                   1348 ;------------------------------------------------------------
                                   1349 ;err                       Allocated to registers r7 
                                   1350 ;p                         Allocated to registers 
                                   1351 ;------------------------------------------------------------
                           00009E  1352 	G$dbglink_display_radio_error$0$0 ==.
                           00009E  1353 	C$misc.c$102$1$341 ==.
                                   1354 ;	..\COMMON\misc.c:102: void dbglink_display_radio_error(uint8_t err)
                                   1355 ;	-----------------------------------------
                                   1356 ;	 function dbglink_display_radio_error
                                   1357 ;	-----------------------------------------
      004023                       1358 _dbglink_display_radio_error:
      004023 AF 82            [24] 1359 	mov	r7,dpl
                           0000A0  1360 	C$misc.c$104$1$341 ==.
                                   1361 ;	..\COMMON\misc.c:104: const struct errtbl __code *p = errtbl;
                           0000A0  1362 	C$misc.c$105$1$345 ==.
                                   1363 ;	..\COMMON\misc.c:105: if (!(DBGLNKSTAT & 0x10))
      004025 E5 E2            [12] 1364 	mov	a,_DBGLNKSTAT
      004027 20 E4 02         [24] 1365 	jb	acc.4,00112$
                           0000A5  1366 	C$misc.c$106$1$345 ==.
                                   1367 ;	..\COMMON\misc.c:106: return;
                           0000A5  1368 	C$misc.c$107$1$345 ==.
                                   1369 ;	..\COMMON\misc.c:107: do {
      00402A 80 46            [24] 1370 	sjmp	00108$
      00402C                       1371 00112$:
      00402C 7D F7            [12] 1372 	mov	r5,#_errtbl
      00402E 7E 64            [12] 1373 	mov	r6,#(_errtbl >> 8)
      004030                       1374 00105$:
                           0000AB  1375 	C$misc.c$108$2$346 ==.
                                   1376 ;	..\COMMON\misc.c:108: if (p->errcode == err) {
      004030 8D 82            [24] 1377 	mov	dpl,r5
      004032 8E 83            [24] 1378 	mov	dph,r6
      004034 E4               [12] 1379 	clr	a
      004035 93               [24] 1380 	movc	a,@a+dptr
      004036 FC               [12] 1381 	mov	r4,a
      004037 B5 07 29         [24] 1382 	cjne	a,ar7,00104$
                           0000B5  1383 	C$misc.c$110$3$347 ==.
                                   1384 ;	..\COMMON\misc.c:110: dbglink_writestr(p->msg);
      00403A 8D 82            [24] 1385 	mov	dpl,r5
      00403C 8E 83            [24] 1386 	mov	dph,r6
      00403E A3               [24] 1387 	inc	dptr
      00403F E4               [12] 1388 	clr	a
      004040 93               [24] 1389 	movc	a,@a+dptr
      004041 FB               [12] 1390 	mov	r3,a
      004042 A3               [24] 1391 	inc	dptr
      004043 E4               [12] 1392 	clr	a
      004044 93               [24] 1393 	movc	a,@a+dptr
      004045 FC               [12] 1394 	mov	r4,a
      004046 7A 80            [12] 1395 	mov	r2,#0x80
      004048 8B 82            [24] 1396 	mov	dpl,r3
      00404A 8C 83            [24] 1397 	mov	dph,r4
      00404C 8A F0            [24] 1398 	mov	b,r2
      00404E C0 07            [24] 1399 	push	ar7
      004050 C0 06            [24] 1400 	push	ar6
      004052 C0 05            [24] 1401 	push	ar5
      004054 12 59 1E         [24] 1402 	lcall	_dbglink_writestr
      004057 D0 05            [24] 1403 	pop	ar5
      004059 D0 06            [24] 1404 	pop	ar6
      00405B D0 07            [24] 1405 	pop	ar7
                           0000D8  1406 	C$misc.c$111$3$347 ==.
                                   1407 ;	..\COMMON\misc.c:111: dbglink_tx('\n');
      00405D 75 82 0A         [24] 1408 	mov	dpl,#0x0a
      004060 12 49 80         [24] 1409 	lcall	_dbglink_tx
      004063                       1410 00104$:
                           0000DE  1411 	C$misc.c$114$2$346 ==.
                                   1412 ;	..\COMMON\misc.c:114: ++p;
      004063 74 03            [12] 1413 	mov	a,#0x03
      004065 2D               [12] 1414 	add	a,r5
      004066 FD               [12] 1415 	mov	r5,a
      004067 E4               [12] 1416 	clr	a
      004068 3E               [12] 1417 	addc	a,r6
      004069 FE               [12] 1418 	mov	r6,a
                           0000E5  1419 	C$misc.c$115$1$345 ==.
                                   1420 ;	..\COMMON\misc.c:115: } while (p->errcode != AXRADIO_ERR_NOERROR);
      00406A 8D 82            [24] 1421 	mov	dpl,r5
      00406C 8E 83            [24] 1422 	mov	dph,r6
      00406E E4               [12] 1423 	clr	a
      00406F 93               [24] 1424 	movc	a,@a+dptr
      004070 70 BE            [24] 1425 	jnz	00105$
      004072                       1426 00108$:
                           0000ED  1427 	C$misc.c$116$1$345 ==.
                           0000ED  1428 	XG$dbglink_display_radio_error$0$0 ==.
      004072 22               [24] 1429 	ret
                                   1430 ;------------------------------------------------------------
                                   1431 ;Allocation info for local variables in function 'delayms_callback'
                                   1432 ;------------------------------------------------------------
                                   1433 ;desc                      Allocated to registers 
                                   1434 ;------------------------------------------------------------
                           0000EE  1435 	Fmisc$delayms_callback$0$0 ==.
                           0000EE  1436 	C$misc.c$121$1$345 ==.
                                   1437 ;	..\COMMON\misc.c:121: static void delayms_callback(struct wtimer_desc __xdata *desc)
                                   1438 ;	-----------------------------------------
                                   1439 ;	 function delayms_callback
                                   1440 ;	-----------------------------------------
      004073                       1441 _delayms_callback:
                           0000EE  1442 	C$misc.c$124$1$349 ==.
                                   1443 ;	..\COMMON\misc.c:124: delaymstimer.handler = 0;
      004073 90 02 9A         [24] 1444 	mov	dptr,#(_delaymstimer + 0x0002)
      004076 E4               [12] 1445 	clr	a
      004077 F0               [24] 1446 	movx	@dptr,a
      004078 A3               [24] 1447 	inc	dptr
      004079 F0               [24] 1448 	movx	@dptr,a
                           0000F5  1449 	C$misc.c$125$1$349 ==.
                           0000F5  1450 	XFmisc$delayms_callback$0$0 ==.
      00407A 22               [24] 1451 	ret
                                   1452 ;------------------------------------------------------------
                                   1453 ;Allocation info for local variables in function 'delay_ms'
                                   1454 ;------------------------------------------------------------
                                   1455 ;ms                        Allocated to registers r6 r7 
                                   1456 ;x                         Allocated to stack - _bp +1
                                   1457 ;------------------------------------------------------------
                           0000F6  1458 	G$delay_ms$0$0 ==.
                           0000F6  1459 	C$misc.c$127$1$349 ==.
                                   1460 ;	..\COMMON\misc.c:127: __reentrantb void delay_ms(uint16_t ms) __reentrant
                                   1461 ;	-----------------------------------------
                                   1462 ;	 function delay_ms
                                   1463 ;	-----------------------------------------
      00407B                       1464 _delay_ms:
      00407B C0 2E            [24] 1465 	push	_bp
      00407D E5 81            [12] 1466 	mov	a,sp
      00407F F5 2E            [12] 1467 	mov	_bp,a
      004081 24 04            [12] 1468 	add	a,#0x04
      004083 F5 81            [12] 1469 	mov	sp,a
      004085 AE 82            [24] 1470 	mov	r6,dpl
      004087 AF 83            [24] 1471 	mov	r7,dph
                           000104  1472 	C$misc.c$131$1$351 ==.
                                   1473 ;	..\COMMON\misc.c:131: wtimer_remove(&delaymstimer);
      004089 90 02 98         [24] 1474 	mov	dptr,#_delaymstimer
      00408C C0 07            [24] 1475 	push	ar7
      00408E C0 06            [24] 1476 	push	ar6
      004090 12 5A 48         [24] 1477 	lcall	_wtimer_remove
      004093 D0 06            [24] 1478 	pop	ar6
      004095 D0 07            [24] 1479 	pop	ar7
                           000112  1480 	C$misc.c$132$1$351 ==.
                                   1481 ;	..\COMMON\misc.c:132: x = ms;
      004097 A8 2E            [24] 1482 	mov	r0,_bp
      004099 08               [12] 1483 	inc	r0
      00409A A6 06            [24] 1484 	mov	@r0,ar6
      00409C 08               [12] 1485 	inc	r0
      00409D A6 07            [24] 1486 	mov	@r0,ar7
      00409F 08               [12] 1487 	inc	r0
      0040A0 76 00            [12] 1488 	mov	@r0,#0x00
      0040A2 08               [12] 1489 	inc	r0
      0040A3 76 00            [12] 1490 	mov	@r0,#0x00
                           000120  1491 	C$misc.c$133$1$351 ==.
                                   1492 ;	..\COMMON\misc.c:133: delaymstimer.time = ms >> 1;
      0040A5 EF               [12] 1493 	mov	a,r7
      0040A6 C3               [12] 1494 	clr	c
      0040A7 13               [12] 1495 	rrc	a
      0040A8 CE               [12] 1496 	xch	a,r6
      0040A9 13               [12] 1497 	rrc	a
      0040AA CE               [12] 1498 	xch	a,r6
      0040AB FF               [12] 1499 	mov	r7,a
      0040AC 8E 04            [24] 1500 	mov	ar4,r6
      0040AE 8F 05            [24] 1501 	mov	ar5,r7
      0040B0 7E 00            [12] 1502 	mov	r6,#0x00
      0040B2 7F 00            [12] 1503 	mov	r7,#0x00
      0040B4 90 02 9C         [24] 1504 	mov	dptr,#(_delaymstimer + 0x0004)
      0040B7 EC               [12] 1505 	mov	a,r4
      0040B8 F0               [24] 1506 	movx	@dptr,a
      0040B9 ED               [12] 1507 	mov	a,r5
      0040BA A3               [24] 1508 	inc	dptr
      0040BB F0               [24] 1509 	movx	@dptr,a
      0040BC EE               [12] 1510 	mov	a,r6
      0040BD A3               [24] 1511 	inc	dptr
      0040BE F0               [24] 1512 	movx	@dptr,a
      0040BF EF               [12] 1513 	mov	a,r7
      0040C0 A3               [24] 1514 	inc	dptr
      0040C1 F0               [24] 1515 	movx	@dptr,a
                           00013D  1516 	C$misc.c$134$1$351 ==.
                                   1517 ;	..\COMMON\misc.c:134: x <<= 3;
      0040C2 A8 2E            [24] 1518 	mov	r0,_bp
      0040C4 08               [12] 1519 	inc	r0
      0040C5 08               [12] 1520 	inc	r0
      0040C6 08               [12] 1521 	inc	r0
      0040C7 08               [12] 1522 	inc	r0
      0040C8 E6               [12] 1523 	mov	a,@r0
      0040C9 18               [12] 1524 	dec	r0
      0040CA C4               [12] 1525 	swap	a
      0040CB 03               [12] 1526 	rr	a
      0040CC 54 F8            [12] 1527 	anl	a,#0xf8
      0040CE C6               [12] 1528 	xch	a,@r0
      0040CF C4               [12] 1529 	swap	a
      0040D0 03               [12] 1530 	rr	a
      0040D1 C6               [12] 1531 	xch	a,@r0
      0040D2 66               [12] 1532 	xrl	a,@r0
      0040D3 C6               [12] 1533 	xch	a,@r0
      0040D4 54 F8            [12] 1534 	anl	a,#0xf8
      0040D6 C6               [12] 1535 	xch	a,@r0
      0040D7 66               [12] 1536 	xrl	a,@r0
      0040D8 08               [12] 1537 	inc	r0
      0040D9 F6               [12] 1538 	mov	@r0,a
      0040DA 18               [12] 1539 	dec	r0
      0040DB 18               [12] 1540 	dec	r0
      0040DC E6               [12] 1541 	mov	a,@r0
      0040DD C4               [12] 1542 	swap	a
      0040DE 03               [12] 1543 	rr	a
      0040DF 54 07            [12] 1544 	anl	a,#0x07
      0040E1 08               [12] 1545 	inc	r0
      0040E2 46               [12] 1546 	orl	a,@r0
      0040E3 F6               [12] 1547 	mov	@r0,a
      0040E4 18               [12] 1548 	dec	r0
      0040E5 E6               [12] 1549 	mov	a,@r0
      0040E6 18               [12] 1550 	dec	r0
      0040E7 C4               [12] 1551 	swap	a
      0040E8 03               [12] 1552 	rr	a
      0040E9 54 F8            [12] 1553 	anl	a,#0xf8
      0040EB C6               [12] 1554 	xch	a,@r0
      0040EC C4               [12] 1555 	swap	a
      0040ED 03               [12] 1556 	rr	a
      0040EE C6               [12] 1557 	xch	a,@r0
      0040EF 66               [12] 1558 	xrl	a,@r0
      0040F0 C6               [12] 1559 	xch	a,@r0
      0040F1 54 F8            [12] 1560 	anl	a,#0xf8
      0040F3 C6               [12] 1561 	xch	a,@r0
      0040F4 66               [12] 1562 	xrl	a,@r0
      0040F5 08               [12] 1563 	inc	r0
      0040F6 F6               [12] 1564 	mov	@r0,a
                           000172  1565 	C$misc.c$135$1$351 ==.
                                   1566 ;	..\COMMON\misc.c:135: delaymstimer.time -= x;
      0040F7 A8 2E            [24] 1567 	mov	r0,_bp
      0040F9 08               [12] 1568 	inc	r0
      0040FA EC               [12] 1569 	mov	a,r4
      0040FB C3               [12] 1570 	clr	c
      0040FC 96               [12] 1571 	subb	a,@r0
      0040FD FC               [12] 1572 	mov	r4,a
      0040FE ED               [12] 1573 	mov	a,r5
      0040FF 08               [12] 1574 	inc	r0
      004100 96               [12] 1575 	subb	a,@r0
      004101 FD               [12] 1576 	mov	r5,a
      004102 EE               [12] 1577 	mov	a,r6
      004103 08               [12] 1578 	inc	r0
      004104 96               [12] 1579 	subb	a,@r0
      004105 FE               [12] 1580 	mov	r6,a
      004106 EF               [12] 1581 	mov	a,r7
      004107 08               [12] 1582 	inc	r0
      004108 96               [12] 1583 	subb	a,@r0
      004109 FF               [12] 1584 	mov	r7,a
      00410A 90 02 9C         [24] 1585 	mov	dptr,#(_delaymstimer + 0x0004)
      00410D EC               [12] 1586 	mov	a,r4
      00410E F0               [24] 1587 	movx	@dptr,a
      00410F ED               [12] 1588 	mov	a,r5
      004110 A3               [24] 1589 	inc	dptr
      004111 F0               [24] 1590 	movx	@dptr,a
      004112 EE               [12] 1591 	mov	a,r6
      004113 A3               [24] 1592 	inc	dptr
      004114 F0               [24] 1593 	movx	@dptr,a
      004115 EF               [12] 1594 	mov	a,r7
      004116 A3               [24] 1595 	inc	dptr
      004117 F0               [24] 1596 	movx	@dptr,a
                           000193  1597 	C$misc.c$136$1$351 ==.
                                   1598 ;	..\COMMON\misc.c:136: x <<= 3;
      004118 A8 2E            [24] 1599 	mov	r0,_bp
      00411A 08               [12] 1600 	inc	r0
      00411B 08               [12] 1601 	inc	r0
      00411C 08               [12] 1602 	inc	r0
      00411D 08               [12] 1603 	inc	r0
      00411E E6               [12] 1604 	mov	a,@r0
      00411F 18               [12] 1605 	dec	r0
      004120 C4               [12] 1606 	swap	a
      004121 03               [12] 1607 	rr	a
      004122 54 F8            [12] 1608 	anl	a,#0xf8
      004124 C6               [12] 1609 	xch	a,@r0
      004125 C4               [12] 1610 	swap	a
      004126 03               [12] 1611 	rr	a
      004127 C6               [12] 1612 	xch	a,@r0
      004128 66               [12] 1613 	xrl	a,@r0
      004129 C6               [12] 1614 	xch	a,@r0
      00412A 54 F8            [12] 1615 	anl	a,#0xf8
      00412C C6               [12] 1616 	xch	a,@r0
      00412D 66               [12] 1617 	xrl	a,@r0
      00412E 08               [12] 1618 	inc	r0
      00412F F6               [12] 1619 	mov	@r0,a
      004130 18               [12] 1620 	dec	r0
      004131 18               [12] 1621 	dec	r0
      004132 E6               [12] 1622 	mov	a,@r0
      004133 C4               [12] 1623 	swap	a
      004134 03               [12] 1624 	rr	a
      004135 54 07            [12] 1625 	anl	a,#0x07
      004137 08               [12] 1626 	inc	r0
      004138 46               [12] 1627 	orl	a,@r0
      004139 F6               [12] 1628 	mov	@r0,a
      00413A 18               [12] 1629 	dec	r0
      00413B E6               [12] 1630 	mov	a,@r0
      00413C 18               [12] 1631 	dec	r0
      00413D C4               [12] 1632 	swap	a
      00413E 03               [12] 1633 	rr	a
      00413F 54 F8            [12] 1634 	anl	a,#0xf8
      004141 C6               [12] 1635 	xch	a,@r0
      004142 C4               [12] 1636 	swap	a
      004143 03               [12] 1637 	rr	a
      004144 C6               [12] 1638 	xch	a,@r0
      004145 66               [12] 1639 	xrl	a,@r0
      004146 C6               [12] 1640 	xch	a,@r0
      004147 54 F8            [12] 1641 	anl	a,#0xf8
      004149 C6               [12] 1642 	xch	a,@r0
      00414A 66               [12] 1643 	xrl	a,@r0
      00414B 08               [12] 1644 	inc	r0
      00414C F6               [12] 1645 	mov	@r0,a
                           0001C8  1646 	C$misc.c$137$1$351 ==.
                                   1647 ;	..\COMMON\misc.c:137: delaymstimer.time += x;
      00414D A8 2E            [24] 1648 	mov	r0,_bp
      00414F 08               [12] 1649 	inc	r0
      004150 E6               [12] 1650 	mov	a,@r0
      004151 2C               [12] 1651 	add	a,r4
      004152 FC               [12] 1652 	mov	r4,a
      004153 08               [12] 1653 	inc	r0
      004154 E6               [12] 1654 	mov	a,@r0
      004155 3D               [12] 1655 	addc	a,r5
      004156 FD               [12] 1656 	mov	r5,a
      004157 08               [12] 1657 	inc	r0
      004158 E6               [12] 1658 	mov	a,@r0
      004159 3E               [12] 1659 	addc	a,r6
      00415A FE               [12] 1660 	mov	r6,a
      00415B 08               [12] 1661 	inc	r0
      00415C E6               [12] 1662 	mov	a,@r0
      00415D 3F               [12] 1663 	addc	a,r7
      00415E FF               [12] 1664 	mov	r7,a
      00415F 90 02 9C         [24] 1665 	mov	dptr,#(_delaymstimer + 0x0004)
      004162 EC               [12] 1666 	mov	a,r4
      004163 F0               [24] 1667 	movx	@dptr,a
      004164 ED               [12] 1668 	mov	a,r5
      004165 A3               [24] 1669 	inc	dptr
      004166 F0               [24] 1670 	movx	@dptr,a
      004167 EE               [12] 1671 	mov	a,r6
      004168 A3               [24] 1672 	inc	dptr
      004169 F0               [24] 1673 	movx	@dptr,a
      00416A EF               [12] 1674 	mov	a,r7
      00416B A3               [24] 1675 	inc	dptr
      00416C F0               [24] 1676 	movx	@dptr,a
                           0001E8  1677 	C$misc.c$138$1$351 ==.
                                   1678 ;	..\COMMON\misc.c:138: x <<= 2;
      00416D A8 2E            [24] 1679 	mov	r0,_bp
      00416F 08               [12] 1680 	inc	r0
      004170 E6               [12] 1681 	mov	a,@r0
      004171 25 E0            [12] 1682 	add	a,acc
      004173 F6               [12] 1683 	mov	@r0,a
      004174 08               [12] 1684 	inc	r0
      004175 E6               [12] 1685 	mov	a,@r0
      004176 33               [12] 1686 	rlc	a
      004177 F6               [12] 1687 	mov	@r0,a
      004178 08               [12] 1688 	inc	r0
      004179 E6               [12] 1689 	mov	a,@r0
      00417A 33               [12] 1690 	rlc	a
      00417B F6               [12] 1691 	mov	@r0,a
      00417C 08               [12] 1692 	inc	r0
      00417D E6               [12] 1693 	mov	a,@r0
      00417E 33               [12] 1694 	rlc	a
      00417F F6               [12] 1695 	mov	@r0,a
      004180 18               [12] 1696 	dec	r0
      004181 18               [12] 1697 	dec	r0
      004182 18               [12] 1698 	dec	r0
      004183 E6               [12] 1699 	mov	a,@r0
      004184 25 E0            [12] 1700 	add	a,acc
      004186 F6               [12] 1701 	mov	@r0,a
      004187 08               [12] 1702 	inc	r0
      004188 E6               [12] 1703 	mov	a,@r0
      004189 33               [12] 1704 	rlc	a
      00418A F6               [12] 1705 	mov	@r0,a
      00418B 08               [12] 1706 	inc	r0
      00418C E6               [12] 1707 	mov	a,@r0
      00418D 33               [12] 1708 	rlc	a
      00418E F6               [12] 1709 	mov	@r0,a
      00418F 08               [12] 1710 	inc	r0
      004190 E6               [12] 1711 	mov	a,@r0
      004191 33               [12] 1712 	rlc	a
      004192 F6               [12] 1713 	mov	@r0,a
                           00020E  1714 	C$misc.c$139$1$351 ==.
                                   1715 ;	..\COMMON\misc.c:139: delaymstimer.time += x;
      004193 A8 2E            [24] 1716 	mov	r0,_bp
      004195 08               [12] 1717 	inc	r0
      004196 E6               [12] 1718 	mov	a,@r0
      004197 2C               [12] 1719 	add	a,r4
      004198 FC               [12] 1720 	mov	r4,a
      004199 08               [12] 1721 	inc	r0
      00419A E6               [12] 1722 	mov	a,@r0
      00419B 3D               [12] 1723 	addc	a,r5
      00419C FD               [12] 1724 	mov	r5,a
      00419D 08               [12] 1725 	inc	r0
      00419E E6               [12] 1726 	mov	a,@r0
      00419F 3E               [12] 1727 	addc	a,r6
      0041A0 FE               [12] 1728 	mov	r6,a
      0041A1 08               [12] 1729 	inc	r0
      0041A2 E6               [12] 1730 	mov	a,@r0
      0041A3 3F               [12] 1731 	addc	a,r7
      0041A4 FF               [12] 1732 	mov	r7,a
      0041A5 90 02 9C         [24] 1733 	mov	dptr,#(_delaymstimer + 0x0004)
      0041A8 EC               [12] 1734 	mov	a,r4
      0041A9 F0               [24] 1735 	movx	@dptr,a
      0041AA ED               [12] 1736 	mov	a,r5
      0041AB A3               [24] 1737 	inc	dptr
      0041AC F0               [24] 1738 	movx	@dptr,a
      0041AD EE               [12] 1739 	mov	a,r6
      0041AE A3               [24] 1740 	inc	dptr
      0041AF F0               [24] 1741 	movx	@dptr,a
      0041B0 EF               [12] 1742 	mov	a,r7
      0041B1 A3               [24] 1743 	inc	dptr
      0041B2 F0               [24] 1744 	movx	@dptr,a
                           00022E  1745 	C$misc.c$140$1$351 ==.
                                   1746 ;	..\COMMON\misc.c:140: delaymstimer.handler = delayms_callback;
      0041B3 90 02 9A         [24] 1747 	mov	dptr,#(_delaymstimer + 0x0002)
      0041B6 74 73            [12] 1748 	mov	a,#_delayms_callback
      0041B8 F0               [24] 1749 	movx	@dptr,a
      0041B9 74 40            [12] 1750 	mov	a,#(_delayms_callback >> 8)
      0041BB A3               [24] 1751 	inc	dptr
      0041BC F0               [24] 1752 	movx	@dptr,a
                           000238  1753 	C$misc.c$141$1$351 ==.
                                   1754 ;	..\COMMON\misc.c:141: wtimer1_addrelative(&delaymstimer);
      0041BD 90 02 98         [24] 1755 	mov	dptr,#_delaymstimer
      0041C0 12 50 A6         [24] 1756 	lcall	_wtimer1_addrelative
                           00023E  1757 	C$misc.c$142$1$351 ==.
                                   1758 ;	..\COMMON\misc.c:142: wtimer_runcallbacks();
      0041C3 12 4F 50         [24] 1759 	lcall	_wtimer_runcallbacks
                           000241  1760 	C$misc.c$143$1$351 ==.
                                   1761 ;	..\COMMON\misc.c:143: do {
      0041C6                       1762 00101$:
                           000241  1763 	C$misc.c$144$2$352 ==.
                                   1764 ;	..\COMMON\misc.c:144: wtimer_idle(WTFLAG_CANSTANDBY);
      0041C6 75 82 02         [24] 1765 	mov	dpl,#0x02
      0041C9 12 4E CC         [24] 1766 	lcall	_wtimer_idle
                           000247  1767 	C$misc.c$145$2$352 ==.
                                   1768 ;	..\COMMON\misc.c:145: wtimer_runcallbacks();
      0041CC 12 4F 50         [24] 1769 	lcall	_wtimer_runcallbacks
                           00024A  1770 	C$misc.c$146$1$351 ==.
                                   1771 ;	..\COMMON\misc.c:146: } while (delaymstimer.handler);
      0041CF 90 02 9A         [24] 1772 	mov	dptr,#(_delaymstimer + 0x0002)
      0041D2 E0               [24] 1773 	movx	a,@dptr
      0041D3 FE               [12] 1774 	mov	r6,a
      0041D4 A3               [24] 1775 	inc	dptr
      0041D5 E0               [24] 1776 	movx	a,@dptr
      0041D6 FF               [12] 1777 	mov	r7,a
      0041D7 4E               [12] 1778 	orl	a,r6
      0041D8 70 EC            [24] 1779 	jnz	00101$
      0041DA 85 2E 81         [24] 1780 	mov	sp,_bp
      0041DD D0 2E            [24] 1781 	pop	_bp
                           00025A  1782 	C$misc.c$147$1$351 ==.
                           00025A  1783 	XG$delay_ms$0$0 ==.
      0041DF 22               [24] 1784 	ret
                                   1785 	.area CSEG    (CODE)
                                   1786 	.area CONST   (CODE)
                           000000  1787 Fmisc$errtbl$0$0 == .
      0064F7                       1788 _errtbl:
      0064F7 01                    1789 	.db #0x01	; 1
      0064F8 0F 65                 1790 	.byte __str_0, (__str_0 >> 8)
      0064FA 02                    1791 	.db #0x02	; 2
      0064FB 20 65                 1792 	.byte __str_1, (__str_1 >> 8)
      0064FD 03                    1793 	.db #0x03	; 3
      0064FE 28 65                 1794 	.byte __str_2, (__str_2 >> 8)
      006500 04                    1795 	.db #0x04	; 4
      006501 33 65                 1796 	.byte __str_3, (__str_3 >> 8)
      006503 05                    1797 	.db #0x05	; 5
      006504 3E 65                 1798 	.byte __str_4, (__str_4 >> 8)
      006506 06                    1799 	.db #0x06	; 6
      006507 4F 65                 1800 	.byte __str_5, (__str_5 >> 8)
      006509 07                    1801 	.db #0x07	; 7
      00650A 5A 65                 1802 	.byte __str_6, (__str_6 >> 8)
      00650C 00                    1803 	.db #0x00	; 0
      00650D 00 00                 1804 	.byte #0x00,#0x00
                           000018  1805 Fmisc$__str_0$0$0 == .
      00650F                       1806 __str_0:
      00650F 45 3A 20 6E 6F 74 20  1807 	.ascii "E: not supported"
             73 75 70 70 6F 72 74
             65 64
      00651F 00                    1808 	.db 0x00
                           000029  1809 Fmisc$__str_1$0$0 == .
      006520                       1810 __str_1:
      006520 45 3A 20 62 75 73 79  1811 	.ascii "E: busy"
      006527 00                    1812 	.db 0x00
                           000031  1813 Fmisc$__str_2$0$0 == .
      006528                       1814 __str_2:
      006528 45 3A 20 74 69 6D 65  1815 	.ascii "E: timeout"
             6F 75 74
      006532 00                    1816 	.db 0x00
                           00003C  1817 Fmisc$__str_3$0$0 == .
      006533                       1818 __str_3:
      006533 45 3A 20 69 6E 76 61  1819 	.ascii "E: invalid"
             6C 69 64
      00653D 00                    1820 	.db 0x00
                           000047  1821 Fmisc$__str_4$0$0 == .
      00653E                       1822 __str_4:
      00653E 45 3A 20 6E 6F 20 63  1823 	.ascii "E: no chip found"
             68 69 70 20 66 6F 75
             6E 64
      00654E 00                    1824 	.db 0x00
                           000058  1825 Fmisc$__str_5$0$0 == .
      00654F                       1826 __str_5:
      00654F 45 3A 20 72 61 6E 67  1827 	.ascii "E: ranging"
             69 6E 67
      006559 00                    1828 	.db 0x00
                           000063  1829 Fmisc$__str_6$0$0 == .
      00655A                       1830 __str_6:
      00655A 45 3A 20 6C 6F 63 6B  1831 	.ascii "E: lock lost"
             20 6C 6F 73 74
      006566 00                    1832 	.db 0x00
                                   1833 	.area XINIT   (CODE)
                                   1834 	.area CABS    (ABS,CODE)
