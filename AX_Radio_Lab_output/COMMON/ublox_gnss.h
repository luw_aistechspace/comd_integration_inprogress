#define USE_DBGLINK

#define OK 1
#define NOK 0
#define ANS 0
#define ACK 0x01
#define NAK 0x00
/* Message positions and header values */
#define UBX_HEADER1_VAL         0xB5
#define UBX_HEADER2_VAL         0x62
#define UBX_HEADER1_POS         0x00
#define UBX_HEADER2_POS         0x01
#define UBX_MESSAGE_CLASS       0x02
#define UBX_MESSAGE_ID          0x03
#define UBX_MESSAGE_LENGTH_LSB  0x04 //length of the payload
#define UBX_MESSAGE_LENGTH_MSB  0x05 //length of the payload
#define UBX_MESSAGE_PAYLOAD     0x06
#define UBX_HEADER_LENGHT       0x07

#define POSLLH    0x02 // geodesic position

/* Messages class ID enum */
enum eUBX_MESSAGE_CLASS {
    eNAV = 0x01,
    eRXM,
    eINF = 0x04,
    eACK,
    eCFG,
    eUPD = 0x09,
    eMON,
    eAID,
    eTIM = 0x0C,
    eESF = 0x10,
    eMGA = 0x13,
    eLOG = 0x21,
    eSEC = 0x27,
    eHNR
};

// functions in ublox_gnss.c
void UBLOX_GPS_FletcherChecksum8 ( uint8_t *buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length);
void UBLOX_GPS_PortInit(void);
uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength);
