// libraries
#include "../AX_Radio_Lab_output/configslave.h"
#include <ax8052.h>
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include "libmfosc.h"

#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#include "../COMMON/display_com0.h"
#include <libminikitleds.h>
/*#include <stdlib.h> */
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"
#include "../COMMON/easyax5043.h"

#include <string.h>
#include <ax8052crypto.h>
#include <ax8052cryptoregaddr.h>
#include <libmfcrypto.h>

#include "../COMMON/ublox_gnss.h"
#include "../COMMON/pktframing.h"

// macros
// see to pktframing.h

//#define USE_DBGLINK


// global variables
const uint32_t __xdata dummyHMAC = 0xC3C2C1C0;
uint8_t __xdata randomSet[nRANDOM];
uint8_t __xdata siblingSlots[nRANDOM - 1];
uint8_t __xdata demoPacketBuffer[pktBufferLENGTH];
uint8_t __xdata premPacketBuffer[pktLENGTH]; // added on 2021-08-17

struct wtimer_desc __xdata msg1_tmr;
struct wtimer_desc __xdata msg2_tmr;
struct wtimer_desc __xdata msg3_tmr;

// external variables
extern const uint32_t __xdata trmID;
extern uint16_t __xdata pkt_counter;
extern uint8_t __xdata payload[28];
extern uint16_t __xdata payload_len;
extern uint8_t __xdata ack_flg;
extern const uint8_t __xdata StdPrem;
// functions
void swap_variables(uint8_t *a, uint8_t *b);
void randomset_generate(void);
//void simple_framing(void);
void msg1_callback(struct wtimer_desc __xdata *desc);
void msg2_callback(struct wtimer_desc __xdata *desc);
void msg3_callback(struct wtimer_desc __xdata *desc);
void transmit_copies_wtimer(void);
//void simple_transmit(void);
void packetStdFraming(void);

// external functions
extern __xdata uint32_t GOLDSEQUENCE_Obtain(void);

// code

/**
* \brief compare and swap (small to big)
*/
void swap_variables(uint8_t *a, uint8_t *b)
{
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
}
/*
 * Generate a set of true random numbers and sort them in ascending order
 */
void randomset_generate(void)
{
//     delay_ms(rngDELAY_MS);
    randomSet[0] = RNGBYTE % rangeRANDOM + slotStdBASE;
    delay_ms(rngDELAY_MS);
    randomSet[1] = RNGBYTE % rangeRANDOM + slotStdBASE;
    delay_ms(rngDELAY_MS);
    randomSet[2] = RNGBYTE % rangeRANDOM + slotStdBASE;

    while(randomSet[0] == randomSet[1])
    {
        delay_ms(rngDELAY_MS);
        randomSet[1] = RNGBYTE % rangeRANDOM + slotStdBASE;
    }
    while ((randomSet[1] == randomSet[2]) || (randomSet[0] == randomSet[2]))
    {
        delay_ms(rngDELAY_MS);
        randomSet[2] = RNGBYTE % rangeRANDOM + slotStdBASE;
    }

// compare and swap
    if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
    if (randomSet[1] > randomSet[2]) swap_variables(&randomSet[1], &randomSet[2]);
    if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);

// added on 2021-08-30 for timing test
    if (StdPrem == 0x2)
    {
        randomSet[0] = 0 + slotStdBASE;
        randomSet[2] = 29 + slotStdBASE;
    }
///////////////////////////////////////////
#ifdef USE_DBGLINK
            dbglink_writestr("3_RNG_WT0: ");
            dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
            dbglink_tx('\n');
#endif // USE_DBGLINK
}
/****************************************/
// added on 2021-08-17
/*
 * Prem user packet framing 45 bytes
 * 4 bytes: 31 bits Gold sequence + 1 bit user type (Premium/Standard)
 * 5 bytes: 12 bits of sibling slots + 24 bits of terminal ID + 1 bit ack + 3 bits unused
 * 4 bytes of HMAC
 * 28 bytes of GNSS payload
 * 4 bytes of CRC32
 */
 void packetPremFraming(void)
 {
     uint8_t __xdata i = 0, j = 0, n = 0;
     uint32_t __xdata goldseq = 0;
     uint8_t __xdata sib0 = 63, sib1 = 63; // temporary set both to 0x3F
//     uint8_t __xdata ack = 0;
     uint32_t __xdata CRC32;


     goldseq = GOLDSEQUENCE_Obtain();
// added on 2021-08-16
#ifdef USE_DBGLINK
        dbglink_writestr("2_Gold_WT0:");
        dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK
//////////////////////////
     randomset_generate();

// added on 2021-08-06 to test GNSS/UART1 reliability
/*    PORTA_1 = 1;
    do
    {
        UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,payload,ANS,&payload_len);
        delay_ms(50);
    } while(!payload_len);
    PORTA_1 = 0;
#ifdef USE_DBGLINK
    dbglink_writestr("4_GNSS_WT0:");
    dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);*/
/*    for (i = 0; i < payload_len; i++)
    {
        dbglink_writehex16(payload[i], 2, WRNUM_PADZERO);
        dbglink_tx(' ');
    }*/
//    dbglink_tx('\n');
//#endif // USE_DBGLINK
/////////////////////////////////GNSS/UART1 reliability

//     for (n = 0; n < 3; n++)
//     {
/*#ifdef USE_DBGLINK
         dbglink_writenum16(n, 1, WRNUM_PADZERO);
         dbglink_tx(' ');
#endif // USE_DBGLINK*/
/*         switch(n)
         {
         case 0:
            sib0 = randomSet[1];
            sib1 = randomSet[2];
            break;

         case 1:
            sib0 = randomSet[0];
            sib1 = randomSet[2];
            break;

         case 2:
            sib0 = randomSet[0];
            sib1 = randomSet[1];
            break;

         default:
            break;
         }*/
/*#ifdef USE_DBGLINK
        dbglink_writehex16(sib0, 2, WRNUM_PADZERO);
        dbglink_writehex16(sib1, 2, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK*/

#if defined(FRAM1)
    *(demoPacketBuffer + (n*pktLENGTH)) = (uint8_t) (goldseq & 0x000000FF);
    *(demoPacketBuffer + (n*pktLENGTH + 1)) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
    *(demoPacketBuffer + (n*pktLENGTH + 2)) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
    *(premPacketBuffer + 3) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (sib0 & 0x01) /*<< 7*/);
    *(premPacketBuffer + 4) = (uint8_t) (sib0 & 0x3E)>>1 | ((sib1 & 0x07)<<5); // changed 0xFE to 0x3E
    *(premPacketBuffer + 5) = (uint8_t) (sib1 & 0x38)>>3 | (trmID & 0x00001F)<<3;
    *(premPacketBuffer + 6) = (uint8_t) ((trmID & 0x001FE0) >> 5);
    *(premPacketBuffer + 7) = (uint8_t) ((trmID & 0x1FE000) >> 13);
    *(premPacketBuffer + 8) = (uint8_t) ((trmID & 0xE00000) >> 21) | (StdPrem & 0x0001) << 4 | (ack_flg << 5 & 0x0001); // why <<5 why 0x0001?
#else
    *(premPacketBuffer) = (uint8_t) (goldseq & 0x000000FF);
    *(premPacketBuffer + 1) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
    *(premPacketBuffer + 2) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
    *(premPacketBuffer + 3) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (StdPrem&0x0001) <<7);
    *(premPacketBuffer + 4) = (uint8_t) ((sib0 & 0x3F) | ((sib1 & 0x03)<<6));
    *(premPacketBuffer + 5) = (uint8_t) (((sib1 & 0x3C) >> 2) | (trmID & 0x00000F)<<4);
    *(premPacketBuffer + 6) = (uint8_t) ((trmID & 0x000FF0) >> 4);
    *(premPacketBuffer + 7) = (uint8_t) ((trmID & 0x0FF000) >> 12);
    *(premPacketBuffer + 8) = (uint8_t) (((trmID & 0xF00000) >> 20) | (ack_flg << 4));
#endif // defined(FRAM1)

    memcpy((premPacketBuffer + 9), &dummyHMAC, sizeof(uint32_t));
    memcpy((premPacketBuffer + 13), payload, sizeof(uint8_t)*payload_len);

    CRC32 = crc_crc32_msb(premPacketBuffer, (13+payload_len), 0xFFFFFFFF);
    memcpy((premPacketBuffer+13+payload_len), &CRC32, sizeof(uint32_t));

 //   }

    ack_flg = 0; // clear the flag after a new series of packets have been prepared.

 }
/****************************************/
//////////////////////////
/*
 * Std user packet framing 45 bytes
 * 4 bytes: 31 bits Gold sequence + 1 bit user type (Premium/Standard)
 * 5 bytes: 12 bits of sibling slots + 24 bits of terminal ID + 1 bit ack + 3 bits unused
 * 4 bytes of HMAC
 * 28 bytes of GNSS payload
 * 4 bytes of CRC32
 */
 void packetStdFraming(void)
 {
     uint8_t __xdata i = 0, j = 0, n = 0;
     uint32_t __xdata goldseq = 0;
//     uint8_t __xdata StdPrem = 0x1;
     uint8_t __xdata sib0 = 0, sib1 = 0;
//     uint8_t __xdata ack = 0;
     uint32_t __xdata CRC32;


     goldseq = GOLDSEQUENCE_Obtain();
// added on 2021-08-16
#ifdef USE_DBGLINK
        dbglink_writestr("2_Gold_WT0:");
        dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK
//////////////////////////
     randomset_generate();

// added on 2021-08-06 to test GNSS/UART1 reliability
/*    PORTA_1 = 1;
    do
    {
        UBLOX_GPS_SendCommand_WaitACK(eNAV,POSLLH,0x00,payload,ANS,&payload_len);
        delay_ms(50);
    } while(!payload_len);
    PORTA_1 = 0;
#ifdef USE_DBGLINK
    dbglink_writestr("4_GNSS_WT0:");
    dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);*/
/*    for (i = 0; i < payload_len; i++)
    {
        dbglink_writehex16(payload[i], 2, WRNUM_PADZERO);
        dbglink_tx(' ');
    }*/
//    dbglink_tx('\n');
//#endif // USE_DBGLINK
/////////////////////////////////GNSS/UART1 reliability

     for (n = 0; n < 3; n++)
     {
/*#ifdef USE_DBGLINK
         dbglink_writenum16(n, 1, WRNUM_PADZERO);
         dbglink_tx(' ');
#endif // USE_DBGLINK*/
         switch(n)
         {
         case 0:
            sib0 = randomSet[1];
            sib1 = randomSet[2];
            break;

         case 1:
            sib0 = randomSet[0];
            sib1 = randomSet[2];
            break;

         case 2:
            sib0 = randomSet[0];
            sib1 = randomSet[1];
            break;

         default:
            break;
         }
/*#ifdef USE_DBGLINK
        dbglink_writehex16(sib0, 2, WRNUM_PADZERO);
        dbglink_writehex16(sib1, 2, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK*/

#if defined(FRAM1)
    *(demoPacketBuffer + (n*pktLENGTH)) = (uint8_t) (goldseq & 0x000000FF);
    *(demoPacketBuffer + (n*pktLENGTH + 1)) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
    *(demoPacketBuffer + (n*pktLENGTH + 2)) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
    *(demoPacketBuffer + (n*pktLENGTH + 3)) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (sib0 & 0x01) /*<< 7*/);
    *(demoPacketBuffer + (n*pktLENGTH + 4)) = (uint8_t) (sib0 & 0x3E)>>1 | ((sib1 & 0x07)<<5); // changed 0xFE to 0x3E
    *(demoPacketBuffer + (n*pktLENGTH + 5)) = (uint8_t) (sib1 & 0x38)>>3 | (trmID & 0x00001F)<<3;
    *(demoPacketBuffer + (n*pktLENGTH + 6)) = (uint8_t) ((trmID & 0x001FE0) >> 5);
    *(demoPacketBuffer + (n*pktLENGTH + 7)) = (uint8_t) ((trmID & 0x1FE000) >> 13);
    *(demoPacketBuffer + (n*pktLENGTH + 8)) = (uint8_t) ((trmID & 0xE00000) >> 21) | (StdPrem & 0x0001) << 4 | (ack_flg << 5 & 0x0001); // why <<5 why 0x0001?
#else
    *(demoPacketBuffer + (n*pktLENGTH)) = (uint8_t) (goldseq & 0x000000FF);
    *(demoPacketBuffer + (n*pktLENGTH + 1)) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
    *(demoPacketBuffer + (n*pktLENGTH + 2)) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
    *(demoPacketBuffer + (n*pktLENGTH + 3)) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (StdPrem&0x0001) <<7);
    *(demoPacketBuffer + (n*pktLENGTH + 4)) = (uint8_t) ((sib0 & 0x3F) | ((sib1 & 0x03)<<6));
    *(demoPacketBuffer + (n*pktLENGTH + 5)) = (uint8_t) (((sib1 & 0x3C) >> 2) | (trmID & 0x00000F)<<4);
    *(demoPacketBuffer + (n*pktLENGTH + 6)) = (uint8_t) ((trmID & 0x000FF0) >> 4);
    *(demoPacketBuffer + (n*pktLENGTH + 7)) = (uint8_t) ((trmID & 0x0FF000) >> 12);
    *(demoPacketBuffer + (n*pktLENGTH + 8)) = (uint8_t) (((trmID & 0xF00000) >> 20) | (ack_flg << 4));
#endif // defined(FRAM1)

    memcpy((demoPacketBuffer +  n*pktLENGTH + 9), &dummyHMAC, sizeof(uint32_t));
    memcpy((demoPacketBuffer +  n*pktLENGTH + 13), payload, sizeof(uint8_t)*payload_len);

    CRC32 = crc_crc32_msb((demoPacketBuffer+ n*pktLENGTH), (13+payload_len), 0xFFFFFFFF);
    memcpy((demoPacketBuffer+n*pktLENGTH+13+payload_len), &CRC32, sizeof(uint32_t));

    }

    ack_flg = 0; // clear the flag after a new series of packets have been prepared.

 }
