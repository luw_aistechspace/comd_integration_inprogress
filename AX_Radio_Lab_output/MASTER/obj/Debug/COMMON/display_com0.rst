                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module display_com0
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _com0_newline
                                     12 	.globl _com0_writestr2
                                     13 	.globl _uart0_writestr
                                     14 	.globl _uart0_tx
                                     15 	.globl _uart0_init
                                     16 	.globl _uart_timer0_baud
                                     17 	.globl _PORTC_7
                                     18 	.globl _PORTC_6
                                     19 	.globl _PORTC_5
                                     20 	.globl _PORTC_4
                                     21 	.globl _PORTC_3
                                     22 	.globl _PORTC_2
                                     23 	.globl _PORTC_1
                                     24 	.globl _PORTC_0
                                     25 	.globl _PORTB_7
                                     26 	.globl _PORTB_6
                                     27 	.globl _PORTB_5
                                     28 	.globl _PORTB_4
                                     29 	.globl _PORTB_3
                                     30 	.globl _PORTB_2
                                     31 	.globl _PORTB_1
                                     32 	.globl _PORTB_0
                                     33 	.globl _PORTA_7
                                     34 	.globl _PORTA_6
                                     35 	.globl _PORTA_5
                                     36 	.globl _PORTA_4
                                     37 	.globl _PORTA_3
                                     38 	.globl _PORTA_2
                                     39 	.globl _PORTA_1
                                     40 	.globl _PORTA_0
                                     41 	.globl _PINC_7
                                     42 	.globl _PINC_6
                                     43 	.globl _PINC_5
                                     44 	.globl _PINC_4
                                     45 	.globl _PINC_3
                                     46 	.globl _PINC_2
                                     47 	.globl _PINC_1
                                     48 	.globl _PINC_0
                                     49 	.globl _PINB_7
                                     50 	.globl _PINB_6
                                     51 	.globl _PINB_5
                                     52 	.globl _PINB_4
                                     53 	.globl _PINB_3
                                     54 	.globl _PINB_2
                                     55 	.globl _PINB_1
                                     56 	.globl _PINB_0
                                     57 	.globl _PINA_7
                                     58 	.globl _PINA_6
                                     59 	.globl _PINA_5
                                     60 	.globl _PINA_4
                                     61 	.globl _PINA_3
                                     62 	.globl _PINA_2
                                     63 	.globl _PINA_1
                                     64 	.globl _PINA_0
                                     65 	.globl _CY
                                     66 	.globl _AC
                                     67 	.globl _F0
                                     68 	.globl _RS1
                                     69 	.globl _RS0
                                     70 	.globl _OV
                                     71 	.globl _F1
                                     72 	.globl _P
                                     73 	.globl _IP_7
                                     74 	.globl _IP_6
                                     75 	.globl _IP_5
                                     76 	.globl _IP_4
                                     77 	.globl _IP_3
                                     78 	.globl _IP_2
                                     79 	.globl _IP_1
                                     80 	.globl _IP_0
                                     81 	.globl _EA
                                     82 	.globl _IE_7
                                     83 	.globl _IE_6
                                     84 	.globl _IE_5
                                     85 	.globl _IE_4
                                     86 	.globl _IE_3
                                     87 	.globl _IE_2
                                     88 	.globl _IE_1
                                     89 	.globl _IE_0
                                     90 	.globl _EIP_7
                                     91 	.globl _EIP_6
                                     92 	.globl _EIP_5
                                     93 	.globl _EIP_4
                                     94 	.globl _EIP_3
                                     95 	.globl _EIP_2
                                     96 	.globl _EIP_1
                                     97 	.globl _EIP_0
                                     98 	.globl _EIE_7
                                     99 	.globl _EIE_6
                                    100 	.globl _EIE_5
                                    101 	.globl _EIE_4
                                    102 	.globl _EIE_3
                                    103 	.globl _EIE_2
                                    104 	.globl _EIE_1
                                    105 	.globl _EIE_0
                                    106 	.globl _E2IP_7
                                    107 	.globl _E2IP_6
                                    108 	.globl _E2IP_5
                                    109 	.globl _E2IP_4
                                    110 	.globl _E2IP_3
                                    111 	.globl _E2IP_2
                                    112 	.globl _E2IP_1
                                    113 	.globl _E2IP_0
                                    114 	.globl _E2IE_7
                                    115 	.globl _E2IE_6
                                    116 	.globl _E2IE_5
                                    117 	.globl _E2IE_4
                                    118 	.globl _E2IE_3
                                    119 	.globl _E2IE_2
                                    120 	.globl _E2IE_1
                                    121 	.globl _E2IE_0
                                    122 	.globl _B_7
                                    123 	.globl _B_6
                                    124 	.globl _B_5
                                    125 	.globl _B_4
                                    126 	.globl _B_3
                                    127 	.globl _B_2
                                    128 	.globl _B_1
                                    129 	.globl _B_0
                                    130 	.globl _ACC_7
                                    131 	.globl _ACC_6
                                    132 	.globl _ACC_5
                                    133 	.globl _ACC_4
                                    134 	.globl _ACC_3
                                    135 	.globl _ACC_2
                                    136 	.globl _ACC_1
                                    137 	.globl _ACC_0
                                    138 	.globl _WTSTAT
                                    139 	.globl _WTIRQEN
                                    140 	.globl _WTEVTD
                                    141 	.globl _WTEVTD1
                                    142 	.globl _WTEVTD0
                                    143 	.globl _WTEVTC
                                    144 	.globl _WTEVTC1
                                    145 	.globl _WTEVTC0
                                    146 	.globl _WTEVTB
                                    147 	.globl _WTEVTB1
                                    148 	.globl _WTEVTB0
                                    149 	.globl _WTEVTA
                                    150 	.globl _WTEVTA1
                                    151 	.globl _WTEVTA0
                                    152 	.globl _WTCNTR1
                                    153 	.globl _WTCNTB
                                    154 	.globl _WTCNTB1
                                    155 	.globl _WTCNTB0
                                    156 	.globl _WTCNTA
                                    157 	.globl _WTCNTA1
                                    158 	.globl _WTCNTA0
                                    159 	.globl _WTCFGB
                                    160 	.globl _WTCFGA
                                    161 	.globl _WDTRESET
                                    162 	.globl _WDTCFG
                                    163 	.globl _U1STATUS
                                    164 	.globl _U1SHREG
                                    165 	.globl _U1MODE
                                    166 	.globl _U1CTRL
                                    167 	.globl _U0STATUS
                                    168 	.globl _U0SHREG
                                    169 	.globl _U0MODE
                                    170 	.globl _U0CTRL
                                    171 	.globl _T2STATUS
                                    172 	.globl _T2PERIOD
                                    173 	.globl _T2PERIOD1
                                    174 	.globl _T2PERIOD0
                                    175 	.globl _T2MODE
                                    176 	.globl _T2CNT
                                    177 	.globl _T2CNT1
                                    178 	.globl _T2CNT0
                                    179 	.globl _T2CLKSRC
                                    180 	.globl _T1STATUS
                                    181 	.globl _T1PERIOD
                                    182 	.globl _T1PERIOD1
                                    183 	.globl _T1PERIOD0
                                    184 	.globl _T1MODE
                                    185 	.globl _T1CNT
                                    186 	.globl _T1CNT1
                                    187 	.globl _T1CNT0
                                    188 	.globl _T1CLKSRC
                                    189 	.globl _T0STATUS
                                    190 	.globl _T0PERIOD
                                    191 	.globl _T0PERIOD1
                                    192 	.globl _T0PERIOD0
                                    193 	.globl _T0MODE
                                    194 	.globl _T0CNT
                                    195 	.globl _T0CNT1
                                    196 	.globl _T0CNT0
                                    197 	.globl _T0CLKSRC
                                    198 	.globl _SPSTATUS
                                    199 	.globl _SPSHREG
                                    200 	.globl _SPMODE
                                    201 	.globl _SPCLKSRC
                                    202 	.globl _RADIOSTAT
                                    203 	.globl _RADIOSTAT1
                                    204 	.globl _RADIOSTAT0
                                    205 	.globl _RADIODATA
                                    206 	.globl _RADIODATA3
                                    207 	.globl _RADIODATA2
                                    208 	.globl _RADIODATA1
                                    209 	.globl _RADIODATA0
                                    210 	.globl _RADIOADDR
                                    211 	.globl _RADIOADDR1
                                    212 	.globl _RADIOADDR0
                                    213 	.globl _RADIOACC
                                    214 	.globl _OC1STATUS
                                    215 	.globl _OC1PIN
                                    216 	.globl _OC1MODE
                                    217 	.globl _OC1COMP
                                    218 	.globl _OC1COMP1
                                    219 	.globl _OC1COMP0
                                    220 	.globl _OC0STATUS
                                    221 	.globl _OC0PIN
                                    222 	.globl _OC0MODE
                                    223 	.globl _OC0COMP
                                    224 	.globl _OC0COMP1
                                    225 	.globl _OC0COMP0
                                    226 	.globl _NVSTATUS
                                    227 	.globl _NVKEY
                                    228 	.globl _NVDATA
                                    229 	.globl _NVDATA1
                                    230 	.globl _NVDATA0
                                    231 	.globl _NVADDR
                                    232 	.globl _NVADDR1
                                    233 	.globl _NVADDR0
                                    234 	.globl _IC1STATUS
                                    235 	.globl _IC1MODE
                                    236 	.globl _IC1CAPT
                                    237 	.globl _IC1CAPT1
                                    238 	.globl _IC1CAPT0
                                    239 	.globl _IC0STATUS
                                    240 	.globl _IC0MODE
                                    241 	.globl _IC0CAPT
                                    242 	.globl _IC0CAPT1
                                    243 	.globl _IC0CAPT0
                                    244 	.globl _PORTR
                                    245 	.globl _PORTC
                                    246 	.globl _PORTB
                                    247 	.globl _PORTA
                                    248 	.globl _PINR
                                    249 	.globl _PINC
                                    250 	.globl _PINB
                                    251 	.globl _PINA
                                    252 	.globl _DIRR
                                    253 	.globl _DIRC
                                    254 	.globl _DIRB
                                    255 	.globl _DIRA
                                    256 	.globl _DBGLNKSTAT
                                    257 	.globl _DBGLNKBUF
                                    258 	.globl _CODECONFIG
                                    259 	.globl _CLKSTAT
                                    260 	.globl _CLKCON
                                    261 	.globl _ANALOGCOMP
                                    262 	.globl _ADCCONV
                                    263 	.globl _ADCCLKSRC
                                    264 	.globl _ADCCH3CONFIG
                                    265 	.globl _ADCCH2CONFIG
                                    266 	.globl _ADCCH1CONFIG
                                    267 	.globl _ADCCH0CONFIG
                                    268 	.globl __XPAGE
                                    269 	.globl _XPAGE
                                    270 	.globl _SP
                                    271 	.globl _PSW
                                    272 	.globl _PCON
                                    273 	.globl _IP
                                    274 	.globl _IE
                                    275 	.globl _EIP
                                    276 	.globl _EIE
                                    277 	.globl _E2IP
                                    278 	.globl _E2IE
                                    279 	.globl _DPS
                                    280 	.globl _DPTR1
                                    281 	.globl _DPTR0
                                    282 	.globl _DPL1
                                    283 	.globl _DPL
                                    284 	.globl _DPH1
                                    285 	.globl _DPH
                                    286 	.globl _B
                                    287 	.globl _ACC
                                    288 	.globl _column
                                    289 	.globl _row
                                    290 	.globl _XTALREADY
                                    291 	.globl _XTALOSC
                                    292 	.globl _XTALAMPL
                                    293 	.globl _SILICONREV
                                    294 	.globl _SCRATCH3
                                    295 	.globl _SCRATCH2
                                    296 	.globl _SCRATCH1
                                    297 	.globl _SCRATCH0
                                    298 	.globl _RADIOMUX
                                    299 	.globl _RADIOFSTATADDR
                                    300 	.globl _RADIOFSTATADDR1
                                    301 	.globl _RADIOFSTATADDR0
                                    302 	.globl _RADIOFDATAADDR
                                    303 	.globl _RADIOFDATAADDR1
                                    304 	.globl _RADIOFDATAADDR0
                                    305 	.globl _OSCRUN
                                    306 	.globl _OSCREADY
                                    307 	.globl _OSCFORCERUN
                                    308 	.globl _OSCCALIB
                                    309 	.globl _MISCCTRL
                                    310 	.globl _LPXOSCGM
                                    311 	.globl _LPOSCREF
                                    312 	.globl _LPOSCREF1
                                    313 	.globl _LPOSCREF0
                                    314 	.globl _LPOSCPER
                                    315 	.globl _LPOSCPER1
                                    316 	.globl _LPOSCPER0
                                    317 	.globl _LPOSCKFILT
                                    318 	.globl _LPOSCKFILT1
                                    319 	.globl _LPOSCKFILT0
                                    320 	.globl _LPOSCFREQ
                                    321 	.globl _LPOSCFREQ1
                                    322 	.globl _LPOSCFREQ0
                                    323 	.globl _LPOSCCONFIG
                                    324 	.globl _PINSEL
                                    325 	.globl _PINCHGC
                                    326 	.globl _PINCHGB
                                    327 	.globl _PINCHGA
                                    328 	.globl _PALTRADIO
                                    329 	.globl _PALTC
                                    330 	.globl _PALTB
                                    331 	.globl _PALTA
                                    332 	.globl _INTCHGC
                                    333 	.globl _INTCHGB
                                    334 	.globl _INTCHGA
                                    335 	.globl _EXTIRQ
                                    336 	.globl _GPIOENABLE
                                    337 	.globl _ANALOGA
                                    338 	.globl _FRCOSCREF
                                    339 	.globl _FRCOSCREF1
                                    340 	.globl _FRCOSCREF0
                                    341 	.globl _FRCOSCPER
                                    342 	.globl _FRCOSCPER1
                                    343 	.globl _FRCOSCPER0
                                    344 	.globl _FRCOSCKFILT
                                    345 	.globl _FRCOSCKFILT1
                                    346 	.globl _FRCOSCKFILT0
                                    347 	.globl _FRCOSCFREQ
                                    348 	.globl _FRCOSCFREQ1
                                    349 	.globl _FRCOSCFREQ0
                                    350 	.globl _FRCOSCCTRL
                                    351 	.globl _FRCOSCCONFIG
                                    352 	.globl _DMA1CONFIG
                                    353 	.globl _DMA1ADDR
                                    354 	.globl _DMA1ADDR1
                                    355 	.globl _DMA1ADDR0
                                    356 	.globl _DMA0CONFIG
                                    357 	.globl _DMA0ADDR
                                    358 	.globl _DMA0ADDR1
                                    359 	.globl _DMA0ADDR0
                                    360 	.globl _ADCTUNE2
                                    361 	.globl _ADCTUNE1
                                    362 	.globl _ADCTUNE0
                                    363 	.globl _ADCCH3VAL
                                    364 	.globl _ADCCH3VAL1
                                    365 	.globl _ADCCH3VAL0
                                    366 	.globl _ADCCH2VAL
                                    367 	.globl _ADCCH2VAL1
                                    368 	.globl _ADCCH2VAL0
                                    369 	.globl _ADCCH1VAL
                                    370 	.globl _ADCCH1VAL1
                                    371 	.globl _ADCCH1VAL0
                                    372 	.globl _ADCCH0VAL
                                    373 	.globl _ADCCH0VAL1
                                    374 	.globl _ADCCH0VAL0
                                    375 	.globl _com0_portinit
                                    376 	.globl _com0_init
                                    377 	.globl _com0_setpos
                                    378 	.globl _com0_writestr
                                    379 	.globl _com0_tx
                                    380 	.globl _com0_clear
                                    381 ;--------------------------------------------------------
                                    382 ; special function registers
                                    383 ;--------------------------------------------------------
                                    384 	.area RSEG    (ABS,DATA)
      000000                        385 	.org 0x0000
                           0000E0   386 G$ACC$0$0 == 0x00e0
                           0000E0   387 _ACC	=	0x00e0
                           0000F0   388 G$B$0$0 == 0x00f0
                           0000F0   389 _B	=	0x00f0
                           000083   390 G$DPH$0$0 == 0x0083
                           000083   391 _DPH	=	0x0083
                           000085   392 G$DPH1$0$0 == 0x0085
                           000085   393 _DPH1	=	0x0085
                           000082   394 G$DPL$0$0 == 0x0082
                           000082   395 _DPL	=	0x0082
                           000084   396 G$DPL1$0$0 == 0x0084
                           000084   397 _DPL1	=	0x0084
                           008382   398 G$DPTR0$0$0 == 0x8382
                           008382   399 _DPTR0	=	0x8382
                           008584   400 G$DPTR1$0$0 == 0x8584
                           008584   401 _DPTR1	=	0x8584
                           000086   402 G$DPS$0$0 == 0x0086
                           000086   403 _DPS	=	0x0086
                           0000A0   404 G$E2IE$0$0 == 0x00a0
                           0000A0   405 _E2IE	=	0x00a0
                           0000C0   406 G$E2IP$0$0 == 0x00c0
                           0000C0   407 _E2IP	=	0x00c0
                           000098   408 G$EIE$0$0 == 0x0098
                           000098   409 _EIE	=	0x0098
                           0000B0   410 G$EIP$0$0 == 0x00b0
                           0000B0   411 _EIP	=	0x00b0
                           0000A8   412 G$IE$0$0 == 0x00a8
                           0000A8   413 _IE	=	0x00a8
                           0000B8   414 G$IP$0$0 == 0x00b8
                           0000B8   415 _IP	=	0x00b8
                           000087   416 G$PCON$0$0 == 0x0087
                           000087   417 _PCON	=	0x0087
                           0000D0   418 G$PSW$0$0 == 0x00d0
                           0000D0   419 _PSW	=	0x00d0
                           000081   420 G$SP$0$0 == 0x0081
                           000081   421 _SP	=	0x0081
                           0000D9   422 G$XPAGE$0$0 == 0x00d9
                           0000D9   423 _XPAGE	=	0x00d9
                           0000D9   424 G$_XPAGE$0$0 == 0x00d9
                           0000D9   425 __XPAGE	=	0x00d9
                           0000CA   426 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   427 _ADCCH0CONFIG	=	0x00ca
                           0000CB   428 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   429 _ADCCH1CONFIG	=	0x00cb
                           0000D2   430 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   431 _ADCCH2CONFIG	=	0x00d2
                           0000D3   432 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   433 _ADCCH3CONFIG	=	0x00d3
                           0000D1   434 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   435 _ADCCLKSRC	=	0x00d1
                           0000C9   436 G$ADCCONV$0$0 == 0x00c9
                           0000C9   437 _ADCCONV	=	0x00c9
                           0000E1   438 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   439 _ANALOGCOMP	=	0x00e1
                           0000C6   440 G$CLKCON$0$0 == 0x00c6
                           0000C6   441 _CLKCON	=	0x00c6
                           0000C7   442 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   443 _CLKSTAT	=	0x00c7
                           000097   444 G$CODECONFIG$0$0 == 0x0097
                           000097   445 _CODECONFIG	=	0x0097
                           0000E3   446 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   447 _DBGLNKBUF	=	0x00e3
                           0000E2   448 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   449 _DBGLNKSTAT	=	0x00e2
                           000089   450 G$DIRA$0$0 == 0x0089
                           000089   451 _DIRA	=	0x0089
                           00008A   452 G$DIRB$0$0 == 0x008a
                           00008A   453 _DIRB	=	0x008a
                           00008B   454 G$DIRC$0$0 == 0x008b
                           00008B   455 _DIRC	=	0x008b
                           00008E   456 G$DIRR$0$0 == 0x008e
                           00008E   457 _DIRR	=	0x008e
                           0000C8   458 G$PINA$0$0 == 0x00c8
                           0000C8   459 _PINA	=	0x00c8
                           0000E8   460 G$PINB$0$0 == 0x00e8
                           0000E8   461 _PINB	=	0x00e8
                           0000F8   462 G$PINC$0$0 == 0x00f8
                           0000F8   463 _PINC	=	0x00f8
                           00008D   464 G$PINR$0$0 == 0x008d
                           00008D   465 _PINR	=	0x008d
                           000080   466 G$PORTA$0$0 == 0x0080
                           000080   467 _PORTA	=	0x0080
                           000088   468 G$PORTB$0$0 == 0x0088
                           000088   469 _PORTB	=	0x0088
                           000090   470 G$PORTC$0$0 == 0x0090
                           000090   471 _PORTC	=	0x0090
                           00008C   472 G$PORTR$0$0 == 0x008c
                           00008C   473 _PORTR	=	0x008c
                           0000CE   474 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   475 _IC0CAPT0	=	0x00ce
                           0000CF   476 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   477 _IC0CAPT1	=	0x00cf
                           00CFCE   478 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   479 _IC0CAPT	=	0xcfce
                           0000CC   480 G$IC0MODE$0$0 == 0x00cc
                           0000CC   481 _IC0MODE	=	0x00cc
                           0000CD   482 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   483 _IC0STATUS	=	0x00cd
                           0000D6   484 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   485 _IC1CAPT0	=	0x00d6
                           0000D7   486 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   487 _IC1CAPT1	=	0x00d7
                           00D7D6   488 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   489 _IC1CAPT	=	0xd7d6
                           0000D4   490 G$IC1MODE$0$0 == 0x00d4
                           0000D4   491 _IC1MODE	=	0x00d4
                           0000D5   492 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   493 _IC1STATUS	=	0x00d5
                           000092   494 G$NVADDR0$0$0 == 0x0092
                           000092   495 _NVADDR0	=	0x0092
                           000093   496 G$NVADDR1$0$0 == 0x0093
                           000093   497 _NVADDR1	=	0x0093
                           009392   498 G$NVADDR$0$0 == 0x9392
                           009392   499 _NVADDR	=	0x9392
                           000094   500 G$NVDATA0$0$0 == 0x0094
                           000094   501 _NVDATA0	=	0x0094
                           000095   502 G$NVDATA1$0$0 == 0x0095
                           000095   503 _NVDATA1	=	0x0095
                           009594   504 G$NVDATA$0$0 == 0x9594
                           009594   505 _NVDATA	=	0x9594
                           000096   506 G$NVKEY$0$0 == 0x0096
                           000096   507 _NVKEY	=	0x0096
                           000091   508 G$NVSTATUS$0$0 == 0x0091
                           000091   509 _NVSTATUS	=	0x0091
                           0000BC   510 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   511 _OC0COMP0	=	0x00bc
                           0000BD   512 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   513 _OC0COMP1	=	0x00bd
                           00BDBC   514 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   515 _OC0COMP	=	0xbdbc
                           0000B9   516 G$OC0MODE$0$0 == 0x00b9
                           0000B9   517 _OC0MODE	=	0x00b9
                           0000BA   518 G$OC0PIN$0$0 == 0x00ba
                           0000BA   519 _OC0PIN	=	0x00ba
                           0000BB   520 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   521 _OC0STATUS	=	0x00bb
                           0000C4   522 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   523 _OC1COMP0	=	0x00c4
                           0000C5   524 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   525 _OC1COMP1	=	0x00c5
                           00C5C4   526 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   527 _OC1COMP	=	0xc5c4
                           0000C1   528 G$OC1MODE$0$0 == 0x00c1
                           0000C1   529 _OC1MODE	=	0x00c1
                           0000C2   530 G$OC1PIN$0$0 == 0x00c2
                           0000C2   531 _OC1PIN	=	0x00c2
                           0000C3   532 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   533 _OC1STATUS	=	0x00c3
                           0000B1   534 G$RADIOACC$0$0 == 0x00b1
                           0000B1   535 _RADIOACC	=	0x00b1
                           0000B3   536 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   537 _RADIOADDR0	=	0x00b3
                           0000B2   538 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   539 _RADIOADDR1	=	0x00b2
                           00B2B3   540 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   541 _RADIOADDR	=	0xb2b3
                           0000B7   542 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   543 _RADIODATA0	=	0x00b7
                           0000B6   544 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   545 _RADIODATA1	=	0x00b6
                           0000B5   546 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   547 _RADIODATA2	=	0x00b5
                           0000B4   548 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   549 _RADIODATA3	=	0x00b4
                           B4B5B6B7   550 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   551 _RADIODATA	=	0xb4b5b6b7
                           0000BE   552 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   553 _RADIOSTAT0	=	0x00be
                           0000BF   554 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   555 _RADIOSTAT1	=	0x00bf
                           00BFBE   556 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   557 _RADIOSTAT	=	0xbfbe
                           0000DF   558 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   559 _SPCLKSRC	=	0x00df
                           0000DC   560 G$SPMODE$0$0 == 0x00dc
                           0000DC   561 _SPMODE	=	0x00dc
                           0000DE   562 G$SPSHREG$0$0 == 0x00de
                           0000DE   563 _SPSHREG	=	0x00de
                           0000DD   564 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   565 _SPSTATUS	=	0x00dd
                           00009A   566 G$T0CLKSRC$0$0 == 0x009a
                           00009A   567 _T0CLKSRC	=	0x009a
                           00009C   568 G$T0CNT0$0$0 == 0x009c
                           00009C   569 _T0CNT0	=	0x009c
                           00009D   570 G$T0CNT1$0$0 == 0x009d
                           00009D   571 _T0CNT1	=	0x009d
                           009D9C   572 G$T0CNT$0$0 == 0x9d9c
                           009D9C   573 _T0CNT	=	0x9d9c
                           000099   574 G$T0MODE$0$0 == 0x0099
                           000099   575 _T0MODE	=	0x0099
                           00009E   576 G$T0PERIOD0$0$0 == 0x009e
                           00009E   577 _T0PERIOD0	=	0x009e
                           00009F   578 G$T0PERIOD1$0$0 == 0x009f
                           00009F   579 _T0PERIOD1	=	0x009f
                           009F9E   580 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   581 _T0PERIOD	=	0x9f9e
                           00009B   582 G$T0STATUS$0$0 == 0x009b
                           00009B   583 _T0STATUS	=	0x009b
                           0000A2   584 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   585 _T1CLKSRC	=	0x00a2
                           0000A4   586 G$T1CNT0$0$0 == 0x00a4
                           0000A4   587 _T1CNT0	=	0x00a4
                           0000A5   588 G$T1CNT1$0$0 == 0x00a5
                           0000A5   589 _T1CNT1	=	0x00a5
                           00A5A4   590 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   591 _T1CNT	=	0xa5a4
                           0000A1   592 G$T1MODE$0$0 == 0x00a1
                           0000A1   593 _T1MODE	=	0x00a1
                           0000A6   594 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   595 _T1PERIOD0	=	0x00a6
                           0000A7   596 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   597 _T1PERIOD1	=	0x00a7
                           00A7A6   598 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   599 _T1PERIOD	=	0xa7a6
                           0000A3   600 G$T1STATUS$0$0 == 0x00a3
                           0000A3   601 _T1STATUS	=	0x00a3
                           0000AA   602 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   603 _T2CLKSRC	=	0x00aa
                           0000AC   604 G$T2CNT0$0$0 == 0x00ac
                           0000AC   605 _T2CNT0	=	0x00ac
                           0000AD   606 G$T2CNT1$0$0 == 0x00ad
                           0000AD   607 _T2CNT1	=	0x00ad
                           00ADAC   608 G$T2CNT$0$0 == 0xadac
                           00ADAC   609 _T2CNT	=	0xadac
                           0000A9   610 G$T2MODE$0$0 == 0x00a9
                           0000A9   611 _T2MODE	=	0x00a9
                           0000AE   612 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   613 _T2PERIOD0	=	0x00ae
                           0000AF   614 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   615 _T2PERIOD1	=	0x00af
                           00AFAE   616 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   617 _T2PERIOD	=	0xafae
                           0000AB   618 G$T2STATUS$0$0 == 0x00ab
                           0000AB   619 _T2STATUS	=	0x00ab
                           0000E4   620 G$U0CTRL$0$0 == 0x00e4
                           0000E4   621 _U0CTRL	=	0x00e4
                           0000E7   622 G$U0MODE$0$0 == 0x00e7
                           0000E7   623 _U0MODE	=	0x00e7
                           0000E6   624 G$U0SHREG$0$0 == 0x00e6
                           0000E6   625 _U0SHREG	=	0x00e6
                           0000E5   626 G$U0STATUS$0$0 == 0x00e5
                           0000E5   627 _U0STATUS	=	0x00e5
                           0000EC   628 G$U1CTRL$0$0 == 0x00ec
                           0000EC   629 _U1CTRL	=	0x00ec
                           0000EF   630 G$U1MODE$0$0 == 0x00ef
                           0000EF   631 _U1MODE	=	0x00ef
                           0000EE   632 G$U1SHREG$0$0 == 0x00ee
                           0000EE   633 _U1SHREG	=	0x00ee
                           0000ED   634 G$U1STATUS$0$0 == 0x00ed
                           0000ED   635 _U1STATUS	=	0x00ed
                           0000DA   636 G$WDTCFG$0$0 == 0x00da
                           0000DA   637 _WDTCFG	=	0x00da
                           0000DB   638 G$WDTRESET$0$0 == 0x00db
                           0000DB   639 _WDTRESET	=	0x00db
                           0000F1   640 G$WTCFGA$0$0 == 0x00f1
                           0000F1   641 _WTCFGA	=	0x00f1
                           0000F9   642 G$WTCFGB$0$0 == 0x00f9
                           0000F9   643 _WTCFGB	=	0x00f9
                           0000F2   644 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   645 _WTCNTA0	=	0x00f2
                           0000F3   646 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   647 _WTCNTA1	=	0x00f3
                           00F3F2   648 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   649 _WTCNTA	=	0xf3f2
                           0000FA   650 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   651 _WTCNTB0	=	0x00fa
                           0000FB   652 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   653 _WTCNTB1	=	0x00fb
                           00FBFA   654 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   655 _WTCNTB	=	0xfbfa
                           0000EB   656 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   657 _WTCNTR1	=	0x00eb
                           0000F4   658 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   659 _WTEVTA0	=	0x00f4
                           0000F5   660 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   661 _WTEVTA1	=	0x00f5
                           00F5F4   662 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   663 _WTEVTA	=	0xf5f4
                           0000F6   664 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   665 _WTEVTB0	=	0x00f6
                           0000F7   666 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   667 _WTEVTB1	=	0x00f7
                           00F7F6   668 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   669 _WTEVTB	=	0xf7f6
                           0000FC   670 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   671 _WTEVTC0	=	0x00fc
                           0000FD   672 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   673 _WTEVTC1	=	0x00fd
                           00FDFC   674 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   675 _WTEVTC	=	0xfdfc
                           0000FE   676 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   677 _WTEVTD0	=	0x00fe
                           0000FF   678 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   679 _WTEVTD1	=	0x00ff
                           00FFFE   680 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   681 _WTEVTD	=	0xfffe
                           0000E9   682 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   683 _WTIRQEN	=	0x00e9
                           0000EA   684 G$WTSTAT$0$0 == 0x00ea
                           0000EA   685 _WTSTAT	=	0x00ea
                                    686 ;--------------------------------------------------------
                                    687 ; special function bits
                                    688 ;--------------------------------------------------------
                                    689 	.area RSEG    (ABS,DATA)
      000000                        690 	.org 0x0000
                           0000E0   691 G$ACC_0$0$0 == 0x00e0
                           0000E0   692 _ACC_0	=	0x00e0
                           0000E1   693 G$ACC_1$0$0 == 0x00e1
                           0000E1   694 _ACC_1	=	0x00e1
                           0000E2   695 G$ACC_2$0$0 == 0x00e2
                           0000E2   696 _ACC_2	=	0x00e2
                           0000E3   697 G$ACC_3$0$0 == 0x00e3
                           0000E3   698 _ACC_3	=	0x00e3
                           0000E4   699 G$ACC_4$0$0 == 0x00e4
                           0000E4   700 _ACC_4	=	0x00e4
                           0000E5   701 G$ACC_5$0$0 == 0x00e5
                           0000E5   702 _ACC_5	=	0x00e5
                           0000E6   703 G$ACC_6$0$0 == 0x00e6
                           0000E6   704 _ACC_6	=	0x00e6
                           0000E7   705 G$ACC_7$0$0 == 0x00e7
                           0000E7   706 _ACC_7	=	0x00e7
                           0000F0   707 G$B_0$0$0 == 0x00f0
                           0000F0   708 _B_0	=	0x00f0
                           0000F1   709 G$B_1$0$0 == 0x00f1
                           0000F1   710 _B_1	=	0x00f1
                           0000F2   711 G$B_2$0$0 == 0x00f2
                           0000F2   712 _B_2	=	0x00f2
                           0000F3   713 G$B_3$0$0 == 0x00f3
                           0000F3   714 _B_3	=	0x00f3
                           0000F4   715 G$B_4$0$0 == 0x00f4
                           0000F4   716 _B_4	=	0x00f4
                           0000F5   717 G$B_5$0$0 == 0x00f5
                           0000F5   718 _B_5	=	0x00f5
                           0000F6   719 G$B_6$0$0 == 0x00f6
                           0000F6   720 _B_6	=	0x00f6
                           0000F7   721 G$B_7$0$0 == 0x00f7
                           0000F7   722 _B_7	=	0x00f7
                           0000A0   723 G$E2IE_0$0$0 == 0x00a0
                           0000A0   724 _E2IE_0	=	0x00a0
                           0000A1   725 G$E2IE_1$0$0 == 0x00a1
                           0000A1   726 _E2IE_1	=	0x00a1
                           0000A2   727 G$E2IE_2$0$0 == 0x00a2
                           0000A2   728 _E2IE_2	=	0x00a2
                           0000A3   729 G$E2IE_3$0$0 == 0x00a3
                           0000A3   730 _E2IE_3	=	0x00a3
                           0000A4   731 G$E2IE_4$0$0 == 0x00a4
                           0000A4   732 _E2IE_4	=	0x00a4
                           0000A5   733 G$E2IE_5$0$0 == 0x00a5
                           0000A5   734 _E2IE_5	=	0x00a5
                           0000A6   735 G$E2IE_6$0$0 == 0x00a6
                           0000A6   736 _E2IE_6	=	0x00a6
                           0000A7   737 G$E2IE_7$0$0 == 0x00a7
                           0000A7   738 _E2IE_7	=	0x00a7
                           0000C0   739 G$E2IP_0$0$0 == 0x00c0
                           0000C0   740 _E2IP_0	=	0x00c0
                           0000C1   741 G$E2IP_1$0$0 == 0x00c1
                           0000C1   742 _E2IP_1	=	0x00c1
                           0000C2   743 G$E2IP_2$0$0 == 0x00c2
                           0000C2   744 _E2IP_2	=	0x00c2
                           0000C3   745 G$E2IP_3$0$0 == 0x00c3
                           0000C3   746 _E2IP_3	=	0x00c3
                           0000C4   747 G$E2IP_4$0$0 == 0x00c4
                           0000C4   748 _E2IP_4	=	0x00c4
                           0000C5   749 G$E2IP_5$0$0 == 0x00c5
                           0000C5   750 _E2IP_5	=	0x00c5
                           0000C6   751 G$E2IP_6$0$0 == 0x00c6
                           0000C6   752 _E2IP_6	=	0x00c6
                           0000C7   753 G$E2IP_7$0$0 == 0x00c7
                           0000C7   754 _E2IP_7	=	0x00c7
                           000098   755 G$EIE_0$0$0 == 0x0098
                           000098   756 _EIE_0	=	0x0098
                           000099   757 G$EIE_1$0$0 == 0x0099
                           000099   758 _EIE_1	=	0x0099
                           00009A   759 G$EIE_2$0$0 == 0x009a
                           00009A   760 _EIE_2	=	0x009a
                           00009B   761 G$EIE_3$0$0 == 0x009b
                           00009B   762 _EIE_3	=	0x009b
                           00009C   763 G$EIE_4$0$0 == 0x009c
                           00009C   764 _EIE_4	=	0x009c
                           00009D   765 G$EIE_5$0$0 == 0x009d
                           00009D   766 _EIE_5	=	0x009d
                           00009E   767 G$EIE_6$0$0 == 0x009e
                           00009E   768 _EIE_6	=	0x009e
                           00009F   769 G$EIE_7$0$0 == 0x009f
                           00009F   770 _EIE_7	=	0x009f
                           0000B0   771 G$EIP_0$0$0 == 0x00b0
                           0000B0   772 _EIP_0	=	0x00b0
                           0000B1   773 G$EIP_1$0$0 == 0x00b1
                           0000B1   774 _EIP_1	=	0x00b1
                           0000B2   775 G$EIP_2$0$0 == 0x00b2
                           0000B2   776 _EIP_2	=	0x00b2
                           0000B3   777 G$EIP_3$0$0 == 0x00b3
                           0000B3   778 _EIP_3	=	0x00b3
                           0000B4   779 G$EIP_4$0$0 == 0x00b4
                           0000B4   780 _EIP_4	=	0x00b4
                           0000B5   781 G$EIP_5$0$0 == 0x00b5
                           0000B5   782 _EIP_5	=	0x00b5
                           0000B6   783 G$EIP_6$0$0 == 0x00b6
                           0000B6   784 _EIP_6	=	0x00b6
                           0000B7   785 G$EIP_7$0$0 == 0x00b7
                           0000B7   786 _EIP_7	=	0x00b7
                           0000A8   787 G$IE_0$0$0 == 0x00a8
                           0000A8   788 _IE_0	=	0x00a8
                           0000A9   789 G$IE_1$0$0 == 0x00a9
                           0000A9   790 _IE_1	=	0x00a9
                           0000AA   791 G$IE_2$0$0 == 0x00aa
                           0000AA   792 _IE_2	=	0x00aa
                           0000AB   793 G$IE_3$0$0 == 0x00ab
                           0000AB   794 _IE_3	=	0x00ab
                           0000AC   795 G$IE_4$0$0 == 0x00ac
                           0000AC   796 _IE_4	=	0x00ac
                           0000AD   797 G$IE_5$0$0 == 0x00ad
                           0000AD   798 _IE_5	=	0x00ad
                           0000AE   799 G$IE_6$0$0 == 0x00ae
                           0000AE   800 _IE_6	=	0x00ae
                           0000AF   801 G$IE_7$0$0 == 0x00af
                           0000AF   802 _IE_7	=	0x00af
                           0000AF   803 G$EA$0$0 == 0x00af
                           0000AF   804 _EA	=	0x00af
                           0000B8   805 G$IP_0$0$0 == 0x00b8
                           0000B8   806 _IP_0	=	0x00b8
                           0000B9   807 G$IP_1$0$0 == 0x00b9
                           0000B9   808 _IP_1	=	0x00b9
                           0000BA   809 G$IP_2$0$0 == 0x00ba
                           0000BA   810 _IP_2	=	0x00ba
                           0000BB   811 G$IP_3$0$0 == 0x00bb
                           0000BB   812 _IP_3	=	0x00bb
                           0000BC   813 G$IP_4$0$0 == 0x00bc
                           0000BC   814 _IP_4	=	0x00bc
                           0000BD   815 G$IP_5$0$0 == 0x00bd
                           0000BD   816 _IP_5	=	0x00bd
                           0000BE   817 G$IP_6$0$0 == 0x00be
                           0000BE   818 _IP_6	=	0x00be
                           0000BF   819 G$IP_7$0$0 == 0x00bf
                           0000BF   820 _IP_7	=	0x00bf
                           0000D0   821 G$P$0$0 == 0x00d0
                           0000D0   822 _P	=	0x00d0
                           0000D1   823 G$F1$0$0 == 0x00d1
                           0000D1   824 _F1	=	0x00d1
                           0000D2   825 G$OV$0$0 == 0x00d2
                           0000D2   826 _OV	=	0x00d2
                           0000D3   827 G$RS0$0$0 == 0x00d3
                           0000D3   828 _RS0	=	0x00d3
                           0000D4   829 G$RS1$0$0 == 0x00d4
                           0000D4   830 _RS1	=	0x00d4
                           0000D5   831 G$F0$0$0 == 0x00d5
                           0000D5   832 _F0	=	0x00d5
                           0000D6   833 G$AC$0$0 == 0x00d6
                           0000D6   834 _AC	=	0x00d6
                           0000D7   835 G$CY$0$0 == 0x00d7
                           0000D7   836 _CY	=	0x00d7
                           0000C8   837 G$PINA_0$0$0 == 0x00c8
                           0000C8   838 _PINA_0	=	0x00c8
                           0000C9   839 G$PINA_1$0$0 == 0x00c9
                           0000C9   840 _PINA_1	=	0x00c9
                           0000CA   841 G$PINA_2$0$0 == 0x00ca
                           0000CA   842 _PINA_2	=	0x00ca
                           0000CB   843 G$PINA_3$0$0 == 0x00cb
                           0000CB   844 _PINA_3	=	0x00cb
                           0000CC   845 G$PINA_4$0$0 == 0x00cc
                           0000CC   846 _PINA_4	=	0x00cc
                           0000CD   847 G$PINA_5$0$0 == 0x00cd
                           0000CD   848 _PINA_5	=	0x00cd
                           0000CE   849 G$PINA_6$0$0 == 0x00ce
                           0000CE   850 _PINA_6	=	0x00ce
                           0000CF   851 G$PINA_7$0$0 == 0x00cf
                           0000CF   852 _PINA_7	=	0x00cf
                           0000E8   853 G$PINB_0$0$0 == 0x00e8
                           0000E8   854 _PINB_0	=	0x00e8
                           0000E9   855 G$PINB_1$0$0 == 0x00e9
                           0000E9   856 _PINB_1	=	0x00e9
                           0000EA   857 G$PINB_2$0$0 == 0x00ea
                           0000EA   858 _PINB_2	=	0x00ea
                           0000EB   859 G$PINB_3$0$0 == 0x00eb
                           0000EB   860 _PINB_3	=	0x00eb
                           0000EC   861 G$PINB_4$0$0 == 0x00ec
                           0000EC   862 _PINB_4	=	0x00ec
                           0000ED   863 G$PINB_5$0$0 == 0x00ed
                           0000ED   864 _PINB_5	=	0x00ed
                           0000EE   865 G$PINB_6$0$0 == 0x00ee
                           0000EE   866 _PINB_6	=	0x00ee
                           0000EF   867 G$PINB_7$0$0 == 0x00ef
                           0000EF   868 _PINB_7	=	0x00ef
                           0000F8   869 G$PINC_0$0$0 == 0x00f8
                           0000F8   870 _PINC_0	=	0x00f8
                           0000F9   871 G$PINC_1$0$0 == 0x00f9
                           0000F9   872 _PINC_1	=	0x00f9
                           0000FA   873 G$PINC_2$0$0 == 0x00fa
                           0000FA   874 _PINC_2	=	0x00fa
                           0000FB   875 G$PINC_3$0$0 == 0x00fb
                           0000FB   876 _PINC_3	=	0x00fb
                           0000FC   877 G$PINC_4$0$0 == 0x00fc
                           0000FC   878 _PINC_4	=	0x00fc
                           0000FD   879 G$PINC_5$0$0 == 0x00fd
                           0000FD   880 _PINC_5	=	0x00fd
                           0000FE   881 G$PINC_6$0$0 == 0x00fe
                           0000FE   882 _PINC_6	=	0x00fe
                           0000FF   883 G$PINC_7$0$0 == 0x00ff
                           0000FF   884 _PINC_7	=	0x00ff
                           000080   885 G$PORTA_0$0$0 == 0x0080
                           000080   886 _PORTA_0	=	0x0080
                           000081   887 G$PORTA_1$0$0 == 0x0081
                           000081   888 _PORTA_1	=	0x0081
                           000082   889 G$PORTA_2$0$0 == 0x0082
                           000082   890 _PORTA_2	=	0x0082
                           000083   891 G$PORTA_3$0$0 == 0x0083
                           000083   892 _PORTA_3	=	0x0083
                           000084   893 G$PORTA_4$0$0 == 0x0084
                           000084   894 _PORTA_4	=	0x0084
                           000085   895 G$PORTA_5$0$0 == 0x0085
                           000085   896 _PORTA_5	=	0x0085
                           000086   897 G$PORTA_6$0$0 == 0x0086
                           000086   898 _PORTA_6	=	0x0086
                           000087   899 G$PORTA_7$0$0 == 0x0087
                           000087   900 _PORTA_7	=	0x0087
                           000088   901 G$PORTB_0$0$0 == 0x0088
                           000088   902 _PORTB_0	=	0x0088
                           000089   903 G$PORTB_1$0$0 == 0x0089
                           000089   904 _PORTB_1	=	0x0089
                           00008A   905 G$PORTB_2$0$0 == 0x008a
                           00008A   906 _PORTB_2	=	0x008a
                           00008B   907 G$PORTB_3$0$0 == 0x008b
                           00008B   908 _PORTB_3	=	0x008b
                           00008C   909 G$PORTB_4$0$0 == 0x008c
                           00008C   910 _PORTB_4	=	0x008c
                           00008D   911 G$PORTB_5$0$0 == 0x008d
                           00008D   912 _PORTB_5	=	0x008d
                           00008E   913 G$PORTB_6$0$0 == 0x008e
                           00008E   914 _PORTB_6	=	0x008e
                           00008F   915 G$PORTB_7$0$0 == 0x008f
                           00008F   916 _PORTB_7	=	0x008f
                           000090   917 G$PORTC_0$0$0 == 0x0090
                           000090   918 _PORTC_0	=	0x0090
                           000091   919 G$PORTC_1$0$0 == 0x0091
                           000091   920 _PORTC_1	=	0x0091
                           000092   921 G$PORTC_2$0$0 == 0x0092
                           000092   922 _PORTC_2	=	0x0092
                           000093   923 G$PORTC_3$0$0 == 0x0093
                           000093   924 _PORTC_3	=	0x0093
                           000094   925 G$PORTC_4$0$0 == 0x0094
                           000094   926 _PORTC_4	=	0x0094
                           000095   927 G$PORTC_5$0$0 == 0x0095
                           000095   928 _PORTC_5	=	0x0095
                           000096   929 G$PORTC_6$0$0 == 0x0096
                           000096   930 _PORTC_6	=	0x0096
                           000097   931 G$PORTC_7$0$0 == 0x0097
                           000097   932 _PORTC_7	=	0x0097
                                    933 ;--------------------------------------------------------
                                    934 ; overlayable register banks
                                    935 ;--------------------------------------------------------
                                    936 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        937 	.ds 8
                                    938 ;--------------------------------------------------------
                                    939 ; internal ram data
                                    940 ;--------------------------------------------------------
                                    941 	.area DSEG    (DATA)
                                    942 ;--------------------------------------------------------
                                    943 ; overlayable items in internal ram 
                                    944 ;--------------------------------------------------------
                                    945 ;--------------------------------------------------------
                                    946 ; indirectly addressable internal ram data
                                    947 ;--------------------------------------------------------
                                    948 	.area ISEG    (DATA)
                                    949 ;--------------------------------------------------------
                                    950 ; absolute internal ram data
                                    951 ;--------------------------------------------------------
                                    952 	.area IABS    (ABS,DATA)
                                    953 	.area IABS    (ABS,DATA)
                                    954 ;--------------------------------------------------------
                                    955 ; bit data
                                    956 ;--------------------------------------------------------
                                    957 	.area BSEG    (BIT)
                                    958 ;--------------------------------------------------------
                                    959 ; paged external ram data
                                    960 ;--------------------------------------------------------
                                    961 	.area PSEG    (PAG,XDATA)
                                    962 ;--------------------------------------------------------
                                    963 ; external ram data
                                    964 ;--------------------------------------------------------
                                    965 	.area XSEG    (XDATA)
                           007020   966 G$ADCCH0VAL0$0$0 == 0x7020
                           007020   967 _ADCCH0VAL0	=	0x7020
                           007021   968 G$ADCCH0VAL1$0$0 == 0x7021
                           007021   969 _ADCCH0VAL1	=	0x7021
                           007020   970 G$ADCCH0VAL$0$0 == 0x7020
                           007020   971 _ADCCH0VAL	=	0x7020
                           007022   972 G$ADCCH1VAL0$0$0 == 0x7022
                           007022   973 _ADCCH1VAL0	=	0x7022
                           007023   974 G$ADCCH1VAL1$0$0 == 0x7023
                           007023   975 _ADCCH1VAL1	=	0x7023
                           007022   976 G$ADCCH1VAL$0$0 == 0x7022
                           007022   977 _ADCCH1VAL	=	0x7022
                           007024   978 G$ADCCH2VAL0$0$0 == 0x7024
                           007024   979 _ADCCH2VAL0	=	0x7024
                           007025   980 G$ADCCH2VAL1$0$0 == 0x7025
                           007025   981 _ADCCH2VAL1	=	0x7025
                           007024   982 G$ADCCH2VAL$0$0 == 0x7024
                           007024   983 _ADCCH2VAL	=	0x7024
                           007026   984 G$ADCCH3VAL0$0$0 == 0x7026
                           007026   985 _ADCCH3VAL0	=	0x7026
                           007027   986 G$ADCCH3VAL1$0$0 == 0x7027
                           007027   987 _ADCCH3VAL1	=	0x7027
                           007026   988 G$ADCCH3VAL$0$0 == 0x7026
                           007026   989 _ADCCH3VAL	=	0x7026
                           007028   990 G$ADCTUNE0$0$0 == 0x7028
                           007028   991 _ADCTUNE0	=	0x7028
                           007029   992 G$ADCTUNE1$0$0 == 0x7029
                           007029   993 _ADCTUNE1	=	0x7029
                           00702A   994 G$ADCTUNE2$0$0 == 0x702a
                           00702A   995 _ADCTUNE2	=	0x702a
                           007010   996 G$DMA0ADDR0$0$0 == 0x7010
                           007010   997 _DMA0ADDR0	=	0x7010
                           007011   998 G$DMA0ADDR1$0$0 == 0x7011
                           007011   999 _DMA0ADDR1	=	0x7011
                           007010  1000 G$DMA0ADDR$0$0 == 0x7010
                           007010  1001 _DMA0ADDR	=	0x7010
                           007014  1002 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1003 _DMA0CONFIG	=	0x7014
                           007012  1004 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1005 _DMA1ADDR0	=	0x7012
                           007013  1006 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1007 _DMA1ADDR1	=	0x7013
                           007012  1008 G$DMA1ADDR$0$0 == 0x7012
                           007012  1009 _DMA1ADDR	=	0x7012
                           007015  1010 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1011 _DMA1CONFIG	=	0x7015
                           007070  1012 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1013 _FRCOSCCONFIG	=	0x7070
                           007071  1014 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1015 _FRCOSCCTRL	=	0x7071
                           007076  1016 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1017 _FRCOSCFREQ0	=	0x7076
                           007077  1018 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1019 _FRCOSCFREQ1	=	0x7077
                           007076  1020 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1021 _FRCOSCFREQ	=	0x7076
                           007072  1022 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1023 _FRCOSCKFILT0	=	0x7072
                           007073  1024 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1025 _FRCOSCKFILT1	=	0x7073
                           007072  1026 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1027 _FRCOSCKFILT	=	0x7072
                           007078  1028 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1029 _FRCOSCPER0	=	0x7078
                           007079  1030 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1031 _FRCOSCPER1	=	0x7079
                           007078  1032 G$FRCOSCPER$0$0 == 0x7078
                           007078  1033 _FRCOSCPER	=	0x7078
                           007074  1034 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1035 _FRCOSCREF0	=	0x7074
                           007075  1036 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1037 _FRCOSCREF1	=	0x7075
                           007074  1038 G$FRCOSCREF$0$0 == 0x7074
                           007074  1039 _FRCOSCREF	=	0x7074
                           007007  1040 G$ANALOGA$0$0 == 0x7007
                           007007  1041 _ANALOGA	=	0x7007
                           00700C  1042 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1043 _GPIOENABLE	=	0x700c
                           007003  1044 G$EXTIRQ$0$0 == 0x7003
                           007003  1045 _EXTIRQ	=	0x7003
                           007000  1046 G$INTCHGA$0$0 == 0x7000
                           007000  1047 _INTCHGA	=	0x7000
                           007001  1048 G$INTCHGB$0$0 == 0x7001
                           007001  1049 _INTCHGB	=	0x7001
                           007002  1050 G$INTCHGC$0$0 == 0x7002
                           007002  1051 _INTCHGC	=	0x7002
                           007008  1052 G$PALTA$0$0 == 0x7008
                           007008  1053 _PALTA	=	0x7008
                           007009  1054 G$PALTB$0$0 == 0x7009
                           007009  1055 _PALTB	=	0x7009
                           00700A  1056 G$PALTC$0$0 == 0x700a
                           00700A  1057 _PALTC	=	0x700a
                           007046  1058 G$PALTRADIO$0$0 == 0x7046
                           007046  1059 _PALTRADIO	=	0x7046
                           007004  1060 G$PINCHGA$0$0 == 0x7004
                           007004  1061 _PINCHGA	=	0x7004
                           007005  1062 G$PINCHGB$0$0 == 0x7005
                           007005  1063 _PINCHGB	=	0x7005
                           007006  1064 G$PINCHGC$0$0 == 0x7006
                           007006  1065 _PINCHGC	=	0x7006
                           00700B  1066 G$PINSEL$0$0 == 0x700b
                           00700B  1067 _PINSEL	=	0x700b
                           007060  1068 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1069 _LPOSCCONFIG	=	0x7060
                           007066  1070 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1071 _LPOSCFREQ0	=	0x7066
                           007067  1072 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1073 _LPOSCFREQ1	=	0x7067
                           007066  1074 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1075 _LPOSCFREQ	=	0x7066
                           007062  1076 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1077 _LPOSCKFILT0	=	0x7062
                           007063  1078 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1079 _LPOSCKFILT1	=	0x7063
                           007062  1080 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1081 _LPOSCKFILT	=	0x7062
                           007068  1082 G$LPOSCPER0$0$0 == 0x7068
                           007068  1083 _LPOSCPER0	=	0x7068
                           007069  1084 G$LPOSCPER1$0$0 == 0x7069
                           007069  1085 _LPOSCPER1	=	0x7069
                           007068  1086 G$LPOSCPER$0$0 == 0x7068
                           007068  1087 _LPOSCPER	=	0x7068
                           007064  1088 G$LPOSCREF0$0$0 == 0x7064
                           007064  1089 _LPOSCREF0	=	0x7064
                           007065  1090 G$LPOSCREF1$0$0 == 0x7065
                           007065  1091 _LPOSCREF1	=	0x7065
                           007064  1092 G$LPOSCREF$0$0 == 0x7064
                           007064  1093 _LPOSCREF	=	0x7064
                           007054  1094 G$LPXOSCGM$0$0 == 0x7054
                           007054  1095 _LPXOSCGM	=	0x7054
                           007F01  1096 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1097 _MISCCTRL	=	0x7f01
                           007053  1098 G$OSCCALIB$0$0 == 0x7053
                           007053  1099 _OSCCALIB	=	0x7053
                           007050  1100 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1101 _OSCFORCERUN	=	0x7050
                           007052  1102 G$OSCREADY$0$0 == 0x7052
                           007052  1103 _OSCREADY	=	0x7052
                           007051  1104 G$OSCRUN$0$0 == 0x7051
                           007051  1105 _OSCRUN	=	0x7051
                           007040  1106 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1107 _RADIOFDATAADDR0	=	0x7040
                           007041  1108 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1109 _RADIOFDATAADDR1	=	0x7041
                           007040  1110 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1111 _RADIOFDATAADDR	=	0x7040
                           007042  1112 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1113 _RADIOFSTATADDR0	=	0x7042
                           007043  1114 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1115 _RADIOFSTATADDR1	=	0x7043
                           007042  1116 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1117 _RADIOFSTATADDR	=	0x7042
                           007044  1118 G$RADIOMUX$0$0 == 0x7044
                           007044  1119 _RADIOMUX	=	0x7044
                           007084  1120 G$SCRATCH0$0$0 == 0x7084
                           007084  1121 _SCRATCH0	=	0x7084
                           007085  1122 G$SCRATCH1$0$0 == 0x7085
                           007085  1123 _SCRATCH1	=	0x7085
                           007086  1124 G$SCRATCH2$0$0 == 0x7086
                           007086  1125 _SCRATCH2	=	0x7086
                           007087  1126 G$SCRATCH3$0$0 == 0x7087
                           007087  1127 _SCRATCH3	=	0x7087
                           007F00  1128 G$SILICONREV$0$0 == 0x7f00
                           007F00  1129 _SILICONREV	=	0x7f00
                           007F19  1130 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1131 _XTALAMPL	=	0x7f19
                           007F18  1132 G$XTALOSC$0$0 == 0x7f18
                           007F18  1133 _XTALOSC	=	0x7f18
                           007F1A  1134 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1135 _XTALREADY	=	0x7f1a
                           00FC06  1136 Fdisplay_com0$flash_deviceid$0$0 == 0xfc06
                           00FC06  1137 _flash_deviceid	=	0xfc06
                           00FC00  1138 Fdisplay_com0$flash_calsector$0$0 == 0xfc00
                           00FC00  1139 _flash_calsector	=	0xfc00
                           000000  1140 G$row$0$0==.
      000004                       1141 _row::
      000004                       1142 	.ds 1
                           000001  1143 G$column$0$0==.
      000005                       1144 _column::
      000005                       1145 	.ds 1
                                   1146 ;--------------------------------------------------------
                                   1147 ; absolute external ram data
                                   1148 ;--------------------------------------------------------
                                   1149 	.area XABS    (ABS,XDATA)
                                   1150 ;--------------------------------------------------------
                                   1151 ; external initialized ram data
                                   1152 ;--------------------------------------------------------
                                   1153 	.area XISEG   (XDATA)
                                   1154 	.area HOME    (CODE)
                                   1155 	.area GSINIT0 (CODE)
                                   1156 	.area GSINIT1 (CODE)
                                   1157 	.area GSINIT2 (CODE)
                                   1158 	.area GSINIT3 (CODE)
                                   1159 	.area GSINIT4 (CODE)
                                   1160 	.area GSINIT5 (CODE)
                                   1161 	.area GSINIT  (CODE)
                                   1162 	.area GSFINAL (CODE)
                                   1163 	.area CSEG    (CODE)
                                   1164 ;--------------------------------------------------------
                                   1165 ; global & static initialisations
                                   1166 ;--------------------------------------------------------
                                   1167 	.area HOME    (CODE)
                                   1168 	.area GSINIT  (CODE)
                                   1169 	.area GSFINAL (CODE)
                                   1170 	.area GSINIT  (CODE)
                                   1171 ;--------------------------------------------------------
                                   1172 ; Home
                                   1173 ;--------------------------------------------------------
                                   1174 	.area HOME    (CODE)
                                   1175 	.area HOME    (CODE)
                                   1176 ;--------------------------------------------------------
                                   1177 ; code
                                   1178 ;--------------------------------------------------------
                                   1179 	.area CSEG    (CODE)
                                   1180 ;------------------------------------------------------------
                                   1181 ;Allocation info for local variables in function 'com0_portinit'
                                   1182 ;------------------------------------------------------------
                           000000  1183 	G$com0_portinit$0$0 ==.
                           000000  1184 	C$display_com0.c$72$0$0 ==.
                                   1185 ;	..\COMMON\display_com0.c:72: __reentrantb void com0_portinit(void) __reentrant
                                   1186 ;	-----------------------------------------
                                   1187 ;	 function com0_portinit
                                   1188 ;	-----------------------------------------
      000DCB                       1189 _com0_portinit:
                           000007  1190 	ar7 = 0x07
                           000006  1191 	ar6 = 0x06
                           000005  1192 	ar5 = 0x05
                           000004  1193 	ar4 = 0x04
                           000003  1194 	ar3 = 0x03
                           000002  1195 	ar2 = 0x02
                           000001  1196 	ar1 = 0x01
                           000000  1197 	ar0 = 0x00
                           000000  1198 	C$display_com0.c$88$1$282 ==.
                                   1199 ;	..\COMMON\display_com0.c:88: PALTB |= 0x11;
      000DCB 90 70 09         [24] 1200 	mov	dptr,#_PALTB
      000DCE E0               [24] 1201 	movx	a,@dptr
      000DCF FF               [12] 1202 	mov	r7,a
      000DD0 74 11            [12] 1203 	mov	a,#0x11
      000DD2 4F               [12] 1204 	orl	a,r7
      000DD3 F0               [24] 1205 	movx	@dptr,a
                           000009  1206 	C$display_com0.c$89$1$282 ==.
                                   1207 ;	..\COMMON\display_com0.c:89: DIRB |= (1<<0) | (1<<4);
      000DD4 43 8A 11         [24] 1208 	orl	_DIRB,#0x11
                           00000C  1209 	C$display_com0.c$90$1$282 ==.
                                   1210 ;	..\COMMON\display_com0.c:90: DIRB &= (uint8_t)~(1<<5);
      000DD7 53 8A DF         [24] 1211 	anl	_DIRB,#0xdf
                           00000F  1212 	C$display_com0.c$91$1$282 ==.
                                   1213 ;	..\COMMON\display_com0.c:91: PINSEL &= (uint8_t)~((1<<0) | (1<<1));
      000DDA 90 70 0B         [24] 1214 	mov	dptr,#_PINSEL
      000DDD E0               [24] 1215 	movx	a,@dptr
      000DDE FF               [12] 1216 	mov	r7,a
      000DDF 74 FC            [12] 1217 	mov	a,#0xfc
      000DE1 5F               [12] 1218 	anl	a,r7
      000DE2 F0               [24] 1219 	movx	@dptr,a
                           000018  1220 	C$display_com0.c$93$1$282 ==.
                                   1221 ;	..\COMMON\display_com0.c:93: uart_timer0_baud(CLKSRC_SYSCLK, 115200, 20000000UL);
      000DE3 E4               [12] 1222 	clr	a
      000DE4 C0 E0            [24] 1223 	push	acc
      000DE6 74 2D            [12] 1224 	mov	a,#0x2d
      000DE8 C0 E0            [24] 1225 	push	acc
      000DEA 74 31            [12] 1226 	mov	a,#0x31
      000DEC C0 E0            [24] 1227 	push	acc
      000DEE 74 01            [12] 1228 	mov	a,#0x01
      000DF0 C0 E0            [24] 1229 	push	acc
      000DF2 E4               [12] 1230 	clr	a
      000DF3 C0 E0            [24] 1231 	push	acc
      000DF5 74 C2            [12] 1232 	mov	a,#0xc2
      000DF7 C0 E0            [24] 1233 	push	acc
      000DF9 74 01            [12] 1234 	mov	a,#0x01
      000DFB C0 E0            [24] 1235 	push	acc
      000DFD E4               [12] 1236 	clr	a
      000DFE C0 E0            [24] 1237 	push	acc
      000E00 75 82 06         [24] 1238 	mov	dpl,#0x06
      000E03 12 4B 49         [24] 1239 	lcall	_uart_timer0_baud
      000E06 E5 81            [12] 1240 	mov	a,sp
      000E08 24 F8            [12] 1241 	add	a,#0xf8
      000E0A F5 81            [12] 1242 	mov	sp,a
                           000041  1243 	C$display_com0.c$94$1$282 ==.
                                   1244 ;	..\COMMON\display_com0.c:94: uart0_init(0, 8, 1);
      000E0C 75 33 08         [24] 1245 	mov	_uart0_init_PARM_2,#0x08
      000E0F 75 34 01         [24] 1246 	mov	_uart0_init_PARM_3,#0x01
      000E12 75 82 00         [24] 1247 	mov	dpl,#0x00
      000E15 12 54 CA         [24] 1248 	lcall	_uart0_init
                           00004D  1249 	C$display_com0.c$96$1$282 ==.
                           00004D  1250 	XG$com0_portinit$0$0 ==.
      000E18 22               [24] 1251 	ret
                                   1252 ;------------------------------------------------------------
                                   1253 ;Allocation info for local variables in function 'com0_init'
                                   1254 ;------------------------------------------------------------
                           00004E  1255 	G$com0_init$0$0 ==.
                           00004E  1256 	C$display_com0.c$98$1$282 ==.
                                   1257 ;	..\COMMON\display_com0.c:98: __reentrantb void com0_init(void) __reentrant
                                   1258 ;	-----------------------------------------
                                   1259 ;	 function com0_init
                                   1260 ;	-----------------------------------------
      000E19                       1261 _com0_init:
                           00004E  1262 	C$display_com0.c$115$1$284 ==.
                                   1263 ;	..\COMMON\display_com0.c:115: uart_timer0_baud(CLKSRC_SYSCLK, 115200, 20000000UL);
      000E19 E4               [12] 1264 	clr	a
      000E1A C0 E0            [24] 1265 	push	acc
      000E1C 74 2D            [12] 1266 	mov	a,#0x2d
      000E1E C0 E0            [24] 1267 	push	acc
      000E20 74 31            [12] 1268 	mov	a,#0x31
      000E22 C0 E0            [24] 1269 	push	acc
      000E24 74 01            [12] 1270 	mov	a,#0x01
      000E26 C0 E0            [24] 1271 	push	acc
      000E28 E4               [12] 1272 	clr	a
      000E29 C0 E0            [24] 1273 	push	acc
      000E2B 74 C2            [12] 1274 	mov	a,#0xc2
      000E2D C0 E0            [24] 1275 	push	acc
      000E2F 74 01            [12] 1276 	mov	a,#0x01
      000E31 C0 E0            [24] 1277 	push	acc
      000E33 E4               [12] 1278 	clr	a
      000E34 C0 E0            [24] 1279 	push	acc
      000E36 75 82 06         [24] 1280 	mov	dpl,#0x06
      000E39 12 4B 49         [24] 1281 	lcall	_uart_timer0_baud
      000E3C E5 81            [12] 1282 	mov	a,sp
      000E3E 24 F8            [12] 1283 	add	a,#0xf8
      000E40 F5 81            [12] 1284 	mov	sp,a
                           000077  1285 	C$display_com0.c$116$1$284 ==.
                                   1286 ;	..\COMMON\display_com0.c:116: uart0_init(0, 8, 1);
      000E42 75 33 08         [24] 1287 	mov	_uart0_init_PARM_2,#0x08
      000E45 75 34 01         [24] 1288 	mov	_uart0_init_PARM_3,#0x01
      000E48 75 82 00         [24] 1289 	mov	dpl,#0x00
      000E4B 12 54 CA         [24] 1290 	lcall	_uart0_init
                           000083  1291 	C$display_com0.c$118$1$284 ==.
                                   1292 ;	..\COMMON\display_com0.c:118: com0_writestr(lcd_border);
      000E4E 90 64 79         [24] 1293 	mov	dptr,#_lcd_border
      000E51 75 F0 80         [24] 1294 	mov	b,#0x80
      000E54 12 0E DD         [24] 1295 	lcall	_com0_writestr
                           00008C  1296 	C$display_com0.c$119$1$284 ==.
                                   1297 ;	..\COMMON\display_com0.c:119: com0_setpos(0);
      000E57 75 82 00         [24] 1298 	mov	dpl,#0x00
      000E5A 12 0E C1         [24] 1299 	lcall	_com0_setpos
                           000092  1300 	C$display_com0.c$120$1$284 ==.
                           000092  1301 	XG$com0_init$0$0 ==.
      000E5D 22               [24] 1302 	ret
                                   1303 ;------------------------------------------------------------
                                   1304 ;Allocation info for local variables in function 'com0_writestr2'
                                   1305 ;------------------------------------------------------------
                                   1306 ;msg                       Allocated to registers r5 r6 r7 
                                   1307 ;------------------------------------------------------------
                           000093  1308 	G$com0_writestr2$0$0 ==.
                           000093  1309 	C$display_com0.c$122$1$284 ==.
                                   1310 ;	..\COMMON\display_com0.c:122: __reentrantb void com0_writestr2(const char* msg)  __reentrant
                                   1311 ;	-----------------------------------------
                                   1312 ;	 function com0_writestr2
                                   1313 ;	-----------------------------------------
      000E5E                       1314 _com0_writestr2:
                           000093  1315 	C$display_com0.c$132$1$286 ==.
                                   1316 ;	..\COMMON\display_com0.c:132: uart0_writestr(msg);
      000E5E 12 5C 24         [24] 1317 	lcall	_uart0_writestr
                           000096  1318 	C$display_com0.c$134$1$286 ==.
                           000096  1319 	XG$com0_writestr2$0$0 ==.
      000E61 22               [24] 1320 	ret
                                   1321 ;------------------------------------------------------------
                                   1322 ;Allocation info for local variables in function 'com0_setrowcol'
                                   1323 ;------------------------------------------------------------
                           000097  1324 	Fdisplay_com0$com0_setrowcol$0$0 ==.
                           000097  1325 	C$display_com0.c$136$1$286 ==.
                                   1326 ;	..\COMMON\display_com0.c:136: static __reentrantb void com0_setrowcol(void) __reentrant
                                   1327 ;	-----------------------------------------
                                   1328 ;	 function com0_setrowcol
                                   1329 ;	-----------------------------------------
      000E62                       1330 _com0_setrowcol:
                           000097  1331 	C$display_com0.c$159$1$288 ==.
                                   1332 ;	..\COMMON\display_com0.c:159: uart0_tx(0x1b);
      000E62 75 82 1B         [24] 1333 	mov	dpl,#0x1b
      000E65 12 54 4F         [24] 1334 	lcall	_uart0_tx
                           00009D  1335 	C$display_com0.c$160$1$288 ==.
                                   1336 ;	..\COMMON\display_com0.c:160: uart0_tx('[');
      000E68 75 82 5B         [24] 1337 	mov	dpl,#0x5b
      000E6B 12 54 4F         [24] 1338 	lcall	_uart0_tx
                           0000A3  1339 	C$display_com0.c$161$1$288 ==.
                                   1340 ;	..\COMMON\display_com0.c:161: uart0_tx(row + '0');
      000E6E 90 00 04         [24] 1341 	mov	dptr,#_row
      000E71 E0               [24] 1342 	movx	a,@dptr
      000E72 24 30            [12] 1343 	add	a,#0x30
      000E74 F5 82            [12] 1344 	mov	dpl,a
      000E76 12 54 4F         [24] 1345 	lcall	_uart0_tx
                           0000AE  1346 	C$display_com0.c$162$1$288 ==.
                                   1347 ;	..\COMMON\display_com0.c:162: uart0_tx(';');
      000E79 75 82 3B         [24] 1348 	mov	dpl,#0x3b
      000E7C 12 54 4F         [24] 1349 	lcall	_uart0_tx
                           0000B4  1350 	C$display_com0.c$163$1$288 ==.
                                   1351 ;	..\COMMON\display_com0.c:163: uart0_tx(column / 10 + '0');
      000E7F 90 00 05         [24] 1352 	mov	dptr,#_column
      000E82 E0               [24] 1353 	movx	a,@dptr
      000E83 75 F0 0A         [24] 1354 	mov	b,#0x0a
      000E86 84               [48] 1355 	div	ab
      000E87 24 30            [12] 1356 	add	a,#0x30
      000E89 F5 82            [12] 1357 	mov	dpl,a
      000E8B 12 54 4F         [24] 1358 	lcall	_uart0_tx
                           0000C3  1359 	C$display_com0.c$164$1$288 ==.
                                   1360 ;	..\COMMON\display_com0.c:164: uart0_tx(column % 10 + '0');
      000E8E 90 00 05         [24] 1361 	mov	dptr,#_column
      000E91 E0               [24] 1362 	movx	a,@dptr
      000E92 75 F0 0A         [24] 1363 	mov	b,#0x0a
      000E95 84               [48] 1364 	div	ab
      000E96 AF F0            [24] 1365 	mov	r7,b
      000E98 74 30            [12] 1366 	mov	a,#0x30
      000E9A 2F               [12] 1367 	add	a,r7
      000E9B F5 82            [12] 1368 	mov	dpl,a
      000E9D 12 54 4F         [24] 1369 	lcall	_uart0_tx
                           0000D5  1370 	C$display_com0.c$165$1$288 ==.
                                   1371 ;	..\COMMON\display_com0.c:165: uart0_tx('f');
      000EA0 75 82 66         [24] 1372 	mov	dpl,#0x66
      000EA3 12 54 4F         [24] 1373 	lcall	_uart0_tx
                           0000DB  1374 	C$display_com0.c$168$1$288 ==.
                           0000DB  1375 	XFdisplay_com0$com0_setrowcol$0$0 ==.
      000EA6 22               [24] 1376 	ret
                                   1377 ;------------------------------------------------------------
                                   1378 ;Allocation info for local variables in function 'com0_newline'
                                   1379 ;------------------------------------------------------------
                           0000DC  1380 	G$com0_newline$0$0 ==.
                           0000DC  1381 	C$display_com0.c$170$1$288 ==.
                                   1382 ;	..\COMMON\display_com0.c:170: __reentrantb void com0_newline(void) __reentrant
                                   1383 ;	-----------------------------------------
                                   1384 ;	 function com0_newline
                                   1385 ;	-----------------------------------------
      000EA7                       1386 _com0_newline:
                           0000DC  1387 	C$display_com0.c$172$1$290 ==.
                                   1388 ;	..\COMMON\display_com0.c:172: if (row < 3)
      000EA7 90 00 04         [24] 1389 	mov	dptr,#_row
      000EAA E0               [24] 1390 	movx	a,@dptr
      000EAB FF               [12] 1391 	mov	r7,a
      000EAC BF 03 00         [24] 1392 	cjne	r7,#0x03,00108$
      000EAF                       1393 00108$:
      000EAF 50 06            [24] 1394 	jnc	00102$
                           0000E6  1395 	C$display_com0.c$173$1$290 ==.
                                   1396 ;	..\COMMON\display_com0.c:173: ++row;
      000EB1 90 00 04         [24] 1397 	mov	dptr,#_row
      000EB4 EF               [12] 1398 	mov	a,r7
      000EB5 04               [12] 1399 	inc	a
      000EB6 F0               [24] 1400 	movx	@dptr,a
      000EB7                       1401 00102$:
                           0000EC  1402 	C$display_com0.c$174$1$290 ==.
                                   1403 ;	..\COMMON\display_com0.c:174: column = 2;
      000EB7 90 00 05         [24] 1404 	mov	dptr,#_column
      000EBA 74 02            [12] 1405 	mov	a,#0x02
      000EBC F0               [24] 1406 	movx	@dptr,a
                           0000F2  1407 	C$display_com0.c$175$1$290 ==.
                                   1408 ;	..\COMMON\display_com0.c:175: com0_setrowcol();
      000EBD 12 0E 62         [24] 1409 	lcall	_com0_setrowcol
                           0000F5  1410 	C$display_com0.c$176$1$290 ==.
                           0000F5  1411 	XG$com0_newline$0$0 ==.
      000EC0 22               [24] 1412 	ret
                                   1413 ;------------------------------------------------------------
                                   1414 ;Allocation info for local variables in function 'com0_setpos'
                                   1415 ;------------------------------------------------------------
                                   1416 ;v                         Allocated to registers r7 
                                   1417 ;------------------------------------------------------------
                           0000F6  1418 	G$com0_setpos$0$0 ==.
                           0000F6  1419 	C$display_com0.c$178$1$290 ==.
                                   1420 ;	..\COMMON\display_com0.c:178: __reentrantb void com0_setpos(uint8_t v) __reentrant
                                   1421 ;	-----------------------------------------
                                   1422 ;	 function com0_setpos
                                   1423 ;	-----------------------------------------
      000EC1                       1424 _com0_setpos:
                           0000F6  1425 	C$display_com0.c$182$1$292 ==.
                                   1426 ;	..\COMMON\display_com0.c:182: row = (v >> 6) + 2;
      000EC1 E5 82            [12] 1427 	mov	a,dpl
      000EC3 FF               [12] 1428 	mov	r7,a
      000EC4 23               [12] 1429 	rl	a
      000EC5 23               [12] 1430 	rl	a
      000EC6 54 03            [12] 1431 	anl	a,#0x03
      000EC8 FE               [12] 1432 	mov	r6,a
      000EC9 90 00 04         [24] 1433 	mov	dptr,#_row
      000ECC 74 02            [12] 1434 	mov	a,#0x02
      000ECE 2E               [12] 1435 	add	a,r6
      000ECF F0               [24] 1436 	movx	@dptr,a
                           000105  1437 	C$display_com0.c$183$1$292 ==.
                                   1438 ;	..\COMMON\display_com0.c:183: column = (v & 0x3F) + 2;
      000ED0 74 3F            [12] 1439 	mov	a,#0x3f
      000ED2 5F               [12] 1440 	anl	a,r7
      000ED3 90 00 05         [24] 1441 	mov	dptr,#_column
      000ED6 24 02            [12] 1442 	add	a,#0x02
      000ED8 F0               [24] 1443 	movx	@dptr,a
                           00010E  1444 	C$display_com0.c$184$1$292 ==.
                                   1445 ;	..\COMMON\display_com0.c:184: com0_setrowcol();
      000ED9 12 0E 62         [24] 1446 	lcall	_com0_setrowcol
                           000111  1447 	C$display_com0.c$185$1$292 ==.
                           000111  1448 	XG$com0_setpos$0$0 ==.
      000EDC 22               [24] 1449 	ret
                                   1450 ;------------------------------------------------------------
                                   1451 ;Allocation info for local variables in function 'com0_writestr'
                                   1452 ;------------------------------------------------------------
                                   1453 ;msg                       Allocated to registers 
                                   1454 ;ch                        Allocated to registers r3 
                                   1455 ;------------------------------------------------------------
                           000112  1456 	G$com0_writestr$0$0 ==.
                           000112  1457 	C$display_com0.c$187$1$292 ==.
                                   1458 ;	..\COMMON\display_com0.c:187: __reentrantb void com0_writestr(const char __genericaddr *msg) __reentrant
                                   1459 ;	-----------------------------------------
                                   1460 ;	 function com0_writestr
                                   1461 ;	-----------------------------------------
      000EDD                       1462 _com0_writestr:
      000EDD AD 82            [24] 1463 	mov	r5,dpl
      000EDF AE 83            [24] 1464 	mov	r6,dph
      000EE1 AF F0            [24] 1465 	mov	r7,b
      000EE3                       1466 00104$:
                           000118  1467 	C$display_com0.c$190$2$295 ==.
                                   1468 ;	..\COMMON\display_com0.c:190: char ch = *msg;
      000EE3 8D 82            [24] 1469 	mov	dpl,r5
      000EE5 8E 83            [24] 1470 	mov	dph,r6
      000EE7 8F F0            [24] 1471 	mov	b,r7
      000EE9 12 61 58         [24] 1472 	lcall	__gptrget
                           000121  1473 	C$display_com0.c$191$2$295 ==.
                                   1474 ;	..\COMMON\display_com0.c:191: if (!ch)
      000EEC FC               [12] 1475 	mov	r4,a
      000EED FB               [12] 1476 	mov	r3,a
      000EEE 60 18            [24] 1477 	jz	00106$
                           000125  1478 	C$display_com0.c$193$2$295 ==.
                                   1479 ;	..\COMMON\display_com0.c:193: com0_tx(ch);
      000EF0 8B 82            [24] 1480 	mov	dpl,r3
      000EF2 C0 07            [24] 1481 	push	ar7
      000EF4 C0 06            [24] 1482 	push	ar6
      000EF6 C0 05            [24] 1483 	push	ar5
      000EF8 12 0F 09         [24] 1484 	lcall	_com0_tx
      000EFB D0 05            [24] 1485 	pop	ar5
      000EFD D0 06            [24] 1486 	pop	ar6
      000EFF D0 07            [24] 1487 	pop	ar7
                           000136  1488 	C$display_com0.c$194$2$295 ==.
                                   1489 ;	..\COMMON\display_com0.c:194: msg++;
      000F01 0D               [12] 1490 	inc	r5
      000F02 BD 00 DE         [24] 1491 	cjne	r5,#0x00,00104$
      000F05 0E               [12] 1492 	inc	r6
      000F06 80 DB            [24] 1493 	sjmp	00104$
      000F08                       1494 00106$:
                           00013D  1495 	C$display_com0.c$197$1$294 ==.
                           00013D  1496 	XG$com0_writestr$0$0 ==.
      000F08 22               [24] 1497 	ret
                                   1498 ;------------------------------------------------------------
                                   1499 ;Allocation info for local variables in function 'com0_tx'
                                   1500 ;------------------------------------------------------------
                                   1501 ;val                       Allocated to registers r7 
                                   1502 ;------------------------------------------------------------
                           00013E  1503 	G$com0_tx$0$0 ==.
                           00013E  1504 	C$display_com0.c$199$1$294 ==.
                                   1505 ;	..\COMMON\display_com0.c:199: __reentrantb void com0_tx(uint8_t val)  __reentrant
                                   1506 ;	-----------------------------------------
                                   1507 ;	 function com0_tx
                                   1508 ;	-----------------------------------------
      000F09                       1509 _com0_tx:
      000F09 AF 82            [24] 1510 	mov	r7,dpl
                           000140  1511 	C$display_com0.c$201$1$297 ==.
                                   1512 ;	..\COMMON\display_com0.c:201: if (val == '\n')
      000F0B BF 0A 05         [24] 1513 	cjne	r7,#0x0a,00102$
                           000143  1514 	C$display_com0.c$202$1$297 ==.
                                   1515 ;	..\COMMON\display_com0.c:202: com0_newline();
      000F0E 12 0E A7         [24] 1516 	lcall	_com0_newline
      000F11 80 05            [24] 1517 	sjmp	00104$
      000F13                       1518 00102$:
                           000148  1519 	C$display_com0.c$212$1$297 ==.
                                   1520 ;	..\COMMON\display_com0.c:212: uart0_tx(val);
      000F13 8F 82            [24] 1521 	mov	dpl,r7
      000F15 12 54 4F         [24] 1522 	lcall	_uart0_tx
      000F18                       1523 00104$:
                           00014D  1524 	C$display_com0.c$215$1$297 ==.
                           00014D  1525 	XG$com0_tx$0$0 ==.
      000F18 22               [24] 1526 	ret
                                   1527 ;------------------------------------------------------------
                                   1528 ;Allocation info for local variables in function 'com0_clear'
                                   1529 ;------------------------------------------------------------
                                   1530 ;len                       Allocated to stack - _bp -3
                                   1531 ;pos                       Allocated to registers r7 
                                   1532 ;------------------------------------------------------------
                           00014E  1533 	G$com0_clear$0$0 ==.
                           00014E  1534 	C$display_com0.c$217$1$297 ==.
                                   1535 ;	..\COMMON\display_com0.c:217: __reentrantb void com0_clear(uint8_t pos, uint8_t len) __reentrant
                                   1536 ;	-----------------------------------------
                                   1537 ;	 function com0_clear
                                   1538 ;	-----------------------------------------
      000F19                       1539 _com0_clear:
      000F19 C0 2E            [24] 1540 	push	_bp
      000F1B 85 81 2E         [24] 1541 	mov	_bp,sp
                           000153  1542 	C$display_com0.c$219$1$299 ==.
                                   1543 ;	..\COMMON\display_com0.c:219: com0_setpos(pos);
      000F1E 12 0E C1         [24] 1544 	lcall	_com0_setpos
                           000156  1545 	C$display_com0.c$220$1$299 ==.
                                   1546 ;	..\COMMON\display_com0.c:220: if (!len)
      000F21 E5 2E            [12] 1547 	mov	a,_bp
      000F23 24 FD            [12] 1548 	add	a,#0xfd
      000F25 F8               [12] 1549 	mov	r0,a
      000F26 E6               [12] 1550 	mov	a,@r0
      000F27 70 02            [24] 1551 	jnz	00109$
                           00015E  1552 	C$display_com0.c$221$1$299 ==.
                                   1553 ;	..\COMMON\display_com0.c:221: return;
                           00015E  1554 	C$display_com0.c$222$1$299 ==.
                                   1555 ;	..\COMMON\display_com0.c:222: do {
      000F29 80 13            [24] 1556 	sjmp	00106$
      000F2B                       1557 00109$:
      000F2B E5 2E            [12] 1558 	mov	a,_bp
      000F2D 24 FD            [12] 1559 	add	a,#0xfd
      000F2F F8               [12] 1560 	mov	r0,a
      000F30 86 07            [24] 1561 	mov	ar7,@r0
      000F32                       1562 00103$:
                           000167  1563 	C$display_com0.c$223$2$300 ==.
                                   1564 ;	..\COMMON\display_com0.c:223: com0_tx(' ');
      000F32 75 82 20         [24] 1565 	mov	dpl,#0x20
      000F35 C0 07            [24] 1566 	push	ar7
      000F37 12 0F 09         [24] 1567 	lcall	_com0_tx
      000F3A D0 07            [24] 1568 	pop	ar7
                           000171  1569 	C$display_com0.c$224$1$299 ==.
                                   1570 ;	..\COMMON\display_com0.c:224: } while (--len);
      000F3C DF F4            [24] 1571 	djnz	r7,00103$
      000F3E                       1572 00106$:
      000F3E D0 2E            [24] 1573 	pop	_bp
                           000175  1574 	C$display_com0.c$225$1$299 ==.
                           000175  1575 	XG$com0_clear$0$0 ==.
      000F40 22               [24] 1576 	ret
                                   1577 	.area CSEG    (CODE)
                                   1578 	.area CONST   (CODE)
                           000000  1579 Fdisplay_com0$lcd_border$0$0 == .
      006479                       1580 _lcd_border:
      006479 1B                    1581 	.db 0x1b
      00647A 5B 30 3B 38 3B 34 34  1582 	.ascii "[0;8;44;30m"
             3B 33 30 6D
      006485 1B                    1583 	.db 0x1b
      006486 5B 31 3B 31 66 2A 2A  1584 	.ascii "[1;1f******************"
             2A 2A 2A 2A 2A 2A 2A
             2A 2A 2A 2A 2A 2A 2A
             2A 2A
      00649D 1B                    1585 	.db 0x1b
      00649E 5B 32 3B 31 66 2A 20  1586 	.ascii "[2;1f*                *"
             20 20 20 20 20 20 20
             20 20 20 20 20 20 20
             20 2A
      0064B5 1B                    1587 	.db 0x1b
      0064B6 5B 33 3B 31 66 2A 20  1588 	.ascii "[3;1f*                *"
             20 20 20 20 20 20 20
             20 20 20 20 20 20 20
             20 2A
      0064CD 1B                    1589 	.db 0x1b
      0064CE 5B 34 3B 31 66 2A 2A  1590 	.ascii "[4;1f******************"
             2A 2A 2A 2A 2A 2A 2A
             2A 2A 2A 2A 2A 2A 2A
             2A 2A
      0064E5 1B                    1591 	.db 0x1b
      0064E6 5B 35 3B 31 66        1592 	.ascii "[5;1f"
      0064EB 1B                    1593 	.db 0x1b
      0064EC 5B 33 37 6D           1594 	.ascii "[37m"
      0064F0 1B                    1595 	.db 0x1b
      0064F1 5B 3F 32 35 6C        1596 	.ascii "[?25l"
      0064F6 00                    1597 	.db 0x00
                                   1598 	.area XINIT   (CODE)
                                   1599 	.area CABS    (ABS,CODE)
