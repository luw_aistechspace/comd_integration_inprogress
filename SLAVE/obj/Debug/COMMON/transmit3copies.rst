                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module transmit3copies
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _transmit_copies_wtimer
                                     12 	.globl _msg3_callback
                                     13 	.globl _msg2_callback
                                     14 	.globl _msg1_callback
                                     15 	.globl _dbglink_writehex32
                                     16 	.globl _dbglink_writestr
                                     17 	.globl _dbglink_tx
                                     18 	.globl _wtimer0_remove
                                     19 	.globl _wtimer0_addrelative
                                     20 	.globl _wtimer0_curtime
                                     21 	.globl _axradio_transmit
                                     22 	.globl _PORTC_7
                                     23 	.globl _PORTC_6
                                     24 	.globl _PORTC_5
                                     25 	.globl _PORTC_4
                                     26 	.globl _PORTC_3
                                     27 	.globl _PORTC_2
                                     28 	.globl _PORTC_1
                                     29 	.globl _PORTC_0
                                     30 	.globl _PORTB_7
                                     31 	.globl _PORTB_6
                                     32 	.globl _PORTB_5
                                     33 	.globl _PORTB_4
                                     34 	.globl _PORTB_3
                                     35 	.globl _PORTB_2
                                     36 	.globl _PORTB_1
                                     37 	.globl _PORTB_0
                                     38 	.globl _PORTA_7
                                     39 	.globl _PORTA_6
                                     40 	.globl _PORTA_5
                                     41 	.globl _PORTA_4
                                     42 	.globl _PORTA_3
                                     43 	.globl _PORTA_2
                                     44 	.globl _PORTA_1
                                     45 	.globl _PORTA_0
                                     46 	.globl _PINC_7
                                     47 	.globl _PINC_6
                                     48 	.globl _PINC_5
                                     49 	.globl _PINC_4
                                     50 	.globl _PINC_3
                                     51 	.globl _PINC_2
                                     52 	.globl _PINC_1
                                     53 	.globl _PINC_0
                                     54 	.globl _PINB_7
                                     55 	.globl _PINB_6
                                     56 	.globl _PINB_5
                                     57 	.globl _PINB_4
                                     58 	.globl _PINB_3
                                     59 	.globl _PINB_2
                                     60 	.globl _PINB_1
                                     61 	.globl _PINB_0
                                     62 	.globl _PINA_7
                                     63 	.globl _PINA_6
                                     64 	.globl _PINA_5
                                     65 	.globl _PINA_4
                                     66 	.globl _PINA_3
                                     67 	.globl _PINA_2
                                     68 	.globl _PINA_1
                                     69 	.globl _PINA_0
                                     70 	.globl _CY
                                     71 	.globl _AC
                                     72 	.globl _F0
                                     73 	.globl _RS1
                                     74 	.globl _RS0
                                     75 	.globl _OV
                                     76 	.globl _F1
                                     77 	.globl _P
                                     78 	.globl _IP_7
                                     79 	.globl _IP_6
                                     80 	.globl _IP_5
                                     81 	.globl _IP_4
                                     82 	.globl _IP_3
                                     83 	.globl _IP_2
                                     84 	.globl _IP_1
                                     85 	.globl _IP_0
                                     86 	.globl _EA
                                     87 	.globl _IE_7
                                     88 	.globl _IE_6
                                     89 	.globl _IE_5
                                     90 	.globl _IE_4
                                     91 	.globl _IE_3
                                     92 	.globl _IE_2
                                     93 	.globl _IE_1
                                     94 	.globl _IE_0
                                     95 	.globl _EIP_7
                                     96 	.globl _EIP_6
                                     97 	.globl _EIP_5
                                     98 	.globl _EIP_4
                                     99 	.globl _EIP_3
                                    100 	.globl _EIP_2
                                    101 	.globl _EIP_1
                                    102 	.globl _EIP_0
                                    103 	.globl _EIE_7
                                    104 	.globl _EIE_6
                                    105 	.globl _EIE_5
                                    106 	.globl _EIE_4
                                    107 	.globl _EIE_3
                                    108 	.globl _EIE_2
                                    109 	.globl _EIE_1
                                    110 	.globl _EIE_0
                                    111 	.globl _E2IP_7
                                    112 	.globl _E2IP_6
                                    113 	.globl _E2IP_5
                                    114 	.globl _E2IP_4
                                    115 	.globl _E2IP_3
                                    116 	.globl _E2IP_2
                                    117 	.globl _E2IP_1
                                    118 	.globl _E2IP_0
                                    119 	.globl _E2IE_7
                                    120 	.globl _E2IE_6
                                    121 	.globl _E2IE_5
                                    122 	.globl _E2IE_4
                                    123 	.globl _E2IE_3
                                    124 	.globl _E2IE_2
                                    125 	.globl _E2IE_1
                                    126 	.globl _E2IE_0
                                    127 	.globl _B_7
                                    128 	.globl _B_6
                                    129 	.globl _B_5
                                    130 	.globl _B_4
                                    131 	.globl _B_3
                                    132 	.globl _B_2
                                    133 	.globl _B_1
                                    134 	.globl _B_0
                                    135 	.globl _ACC_7
                                    136 	.globl _ACC_6
                                    137 	.globl _ACC_5
                                    138 	.globl _ACC_4
                                    139 	.globl _ACC_3
                                    140 	.globl _ACC_2
                                    141 	.globl _ACC_1
                                    142 	.globl _ACC_0
                                    143 	.globl _WTSTAT
                                    144 	.globl _WTIRQEN
                                    145 	.globl _WTEVTD
                                    146 	.globl _WTEVTD1
                                    147 	.globl _WTEVTD0
                                    148 	.globl _WTEVTC
                                    149 	.globl _WTEVTC1
                                    150 	.globl _WTEVTC0
                                    151 	.globl _WTEVTB
                                    152 	.globl _WTEVTB1
                                    153 	.globl _WTEVTB0
                                    154 	.globl _WTEVTA
                                    155 	.globl _WTEVTA1
                                    156 	.globl _WTEVTA0
                                    157 	.globl _WTCNTR1
                                    158 	.globl _WTCNTB
                                    159 	.globl _WTCNTB1
                                    160 	.globl _WTCNTB0
                                    161 	.globl _WTCNTA
                                    162 	.globl _WTCNTA1
                                    163 	.globl _WTCNTA0
                                    164 	.globl _WTCFGB
                                    165 	.globl _WTCFGA
                                    166 	.globl _WDTRESET
                                    167 	.globl _WDTCFG
                                    168 	.globl _U1STATUS
                                    169 	.globl _U1SHREG
                                    170 	.globl _U1MODE
                                    171 	.globl _U1CTRL
                                    172 	.globl _U0STATUS
                                    173 	.globl _U0SHREG
                                    174 	.globl _U0MODE
                                    175 	.globl _U0CTRL
                                    176 	.globl _T2STATUS
                                    177 	.globl _T2PERIOD
                                    178 	.globl _T2PERIOD1
                                    179 	.globl _T2PERIOD0
                                    180 	.globl _T2MODE
                                    181 	.globl _T2CNT
                                    182 	.globl _T2CNT1
                                    183 	.globl _T2CNT0
                                    184 	.globl _T2CLKSRC
                                    185 	.globl _T1STATUS
                                    186 	.globl _T1PERIOD
                                    187 	.globl _T1PERIOD1
                                    188 	.globl _T1PERIOD0
                                    189 	.globl _T1MODE
                                    190 	.globl _T1CNT
                                    191 	.globl _T1CNT1
                                    192 	.globl _T1CNT0
                                    193 	.globl _T1CLKSRC
                                    194 	.globl _T0STATUS
                                    195 	.globl _T0PERIOD
                                    196 	.globl _T0PERIOD1
                                    197 	.globl _T0PERIOD0
                                    198 	.globl _T0MODE
                                    199 	.globl _T0CNT
                                    200 	.globl _T0CNT1
                                    201 	.globl _T0CNT0
                                    202 	.globl _T0CLKSRC
                                    203 	.globl _SPSTATUS
                                    204 	.globl _SPSHREG
                                    205 	.globl _SPMODE
                                    206 	.globl _SPCLKSRC
                                    207 	.globl _RADIOSTAT
                                    208 	.globl _RADIOSTAT1
                                    209 	.globl _RADIOSTAT0
                                    210 	.globl _RADIODATA
                                    211 	.globl _RADIODATA3
                                    212 	.globl _RADIODATA2
                                    213 	.globl _RADIODATA1
                                    214 	.globl _RADIODATA0
                                    215 	.globl _RADIOADDR
                                    216 	.globl _RADIOADDR1
                                    217 	.globl _RADIOADDR0
                                    218 	.globl _RADIOACC
                                    219 	.globl _OC1STATUS
                                    220 	.globl _OC1PIN
                                    221 	.globl _OC1MODE
                                    222 	.globl _OC1COMP
                                    223 	.globl _OC1COMP1
                                    224 	.globl _OC1COMP0
                                    225 	.globl _OC0STATUS
                                    226 	.globl _OC0PIN
                                    227 	.globl _OC0MODE
                                    228 	.globl _OC0COMP
                                    229 	.globl _OC0COMP1
                                    230 	.globl _OC0COMP0
                                    231 	.globl _NVSTATUS
                                    232 	.globl _NVKEY
                                    233 	.globl _NVDATA
                                    234 	.globl _NVDATA1
                                    235 	.globl _NVDATA0
                                    236 	.globl _NVADDR
                                    237 	.globl _NVADDR1
                                    238 	.globl _NVADDR0
                                    239 	.globl _IC1STATUS
                                    240 	.globl _IC1MODE
                                    241 	.globl _IC1CAPT
                                    242 	.globl _IC1CAPT1
                                    243 	.globl _IC1CAPT0
                                    244 	.globl _IC0STATUS
                                    245 	.globl _IC0MODE
                                    246 	.globl _IC0CAPT
                                    247 	.globl _IC0CAPT1
                                    248 	.globl _IC0CAPT0
                                    249 	.globl _PORTR
                                    250 	.globl _PORTC
                                    251 	.globl _PORTB
                                    252 	.globl _PORTA
                                    253 	.globl _PINR
                                    254 	.globl _PINC
                                    255 	.globl _PINB
                                    256 	.globl _PINA
                                    257 	.globl _DIRR
                                    258 	.globl _DIRC
                                    259 	.globl _DIRB
                                    260 	.globl _DIRA
                                    261 	.globl _DBGLNKSTAT
                                    262 	.globl _DBGLNKBUF
                                    263 	.globl _CODECONFIG
                                    264 	.globl _CLKSTAT
                                    265 	.globl _CLKCON
                                    266 	.globl _ANALOGCOMP
                                    267 	.globl _ADCCONV
                                    268 	.globl _ADCCLKSRC
                                    269 	.globl _ADCCH3CONFIG
                                    270 	.globl _ADCCH2CONFIG
                                    271 	.globl _ADCCH1CONFIG
                                    272 	.globl _ADCCH0CONFIG
                                    273 	.globl __XPAGE
                                    274 	.globl _XPAGE
                                    275 	.globl _SP
                                    276 	.globl _PSW
                                    277 	.globl _PCON
                                    278 	.globl _IP
                                    279 	.globl _IE
                                    280 	.globl _EIP
                                    281 	.globl _EIE
                                    282 	.globl _E2IP
                                    283 	.globl _E2IE
                                    284 	.globl _DPS
                                    285 	.globl _DPTR1
                                    286 	.globl _DPTR0
                                    287 	.globl _DPL1
                                    288 	.globl _DPL
                                    289 	.globl _DPH1
                                    290 	.globl _DPH
                                    291 	.globl _B
                                    292 	.globl _ACC
                                    293 	.globl _StdPrem
                                    294 	.globl _trmID
                                    295 	.globl _msg3_tmr
                                    296 	.globl _msg2_tmr
                                    297 	.globl _msg1_tmr
                                    298 	.globl _AX5043_TIMEGAIN3NB
                                    299 	.globl _AX5043_TIMEGAIN2NB
                                    300 	.globl _AX5043_TIMEGAIN1NB
                                    301 	.globl _AX5043_TIMEGAIN0NB
                                    302 	.globl _AX5043_RXPARAMSETSNB
                                    303 	.globl _AX5043_RXPARAMCURSETNB
                                    304 	.globl _AX5043_PKTMAXLENNB
                                    305 	.globl _AX5043_PKTLENOFFSETNB
                                    306 	.globl _AX5043_PKTLENCFGNB
                                    307 	.globl _AX5043_PKTADDRMASK3NB
                                    308 	.globl _AX5043_PKTADDRMASK2NB
                                    309 	.globl _AX5043_PKTADDRMASK1NB
                                    310 	.globl _AX5043_PKTADDRMASK0NB
                                    311 	.globl _AX5043_PKTADDRCFGNB
                                    312 	.globl _AX5043_PKTADDR3NB
                                    313 	.globl _AX5043_PKTADDR2NB
                                    314 	.globl _AX5043_PKTADDR1NB
                                    315 	.globl _AX5043_PKTADDR0NB
                                    316 	.globl _AX5043_PHASEGAIN3NB
                                    317 	.globl _AX5043_PHASEGAIN2NB
                                    318 	.globl _AX5043_PHASEGAIN1NB
                                    319 	.globl _AX5043_PHASEGAIN0NB
                                    320 	.globl _AX5043_FREQUENCYLEAKNB
                                    321 	.globl _AX5043_FREQUENCYGAIND3NB
                                    322 	.globl _AX5043_FREQUENCYGAIND2NB
                                    323 	.globl _AX5043_FREQUENCYGAIND1NB
                                    324 	.globl _AX5043_FREQUENCYGAIND0NB
                                    325 	.globl _AX5043_FREQUENCYGAINC3NB
                                    326 	.globl _AX5043_FREQUENCYGAINC2NB
                                    327 	.globl _AX5043_FREQUENCYGAINC1NB
                                    328 	.globl _AX5043_FREQUENCYGAINC0NB
                                    329 	.globl _AX5043_FREQUENCYGAINB3NB
                                    330 	.globl _AX5043_FREQUENCYGAINB2NB
                                    331 	.globl _AX5043_FREQUENCYGAINB1NB
                                    332 	.globl _AX5043_FREQUENCYGAINB0NB
                                    333 	.globl _AX5043_FREQUENCYGAINA3NB
                                    334 	.globl _AX5043_FREQUENCYGAINA2NB
                                    335 	.globl _AX5043_FREQUENCYGAINA1NB
                                    336 	.globl _AX5043_FREQUENCYGAINA0NB
                                    337 	.globl _AX5043_FREQDEV13NB
                                    338 	.globl _AX5043_FREQDEV12NB
                                    339 	.globl _AX5043_FREQDEV11NB
                                    340 	.globl _AX5043_FREQDEV10NB
                                    341 	.globl _AX5043_FREQDEV03NB
                                    342 	.globl _AX5043_FREQDEV02NB
                                    343 	.globl _AX5043_FREQDEV01NB
                                    344 	.globl _AX5043_FREQDEV00NB
                                    345 	.globl _AX5043_FOURFSK3NB
                                    346 	.globl _AX5043_FOURFSK2NB
                                    347 	.globl _AX5043_FOURFSK1NB
                                    348 	.globl _AX5043_FOURFSK0NB
                                    349 	.globl _AX5043_DRGAIN3NB
                                    350 	.globl _AX5043_DRGAIN2NB
                                    351 	.globl _AX5043_DRGAIN1NB
                                    352 	.globl _AX5043_DRGAIN0NB
                                    353 	.globl _AX5043_BBOFFSRES3NB
                                    354 	.globl _AX5043_BBOFFSRES2NB
                                    355 	.globl _AX5043_BBOFFSRES1NB
                                    356 	.globl _AX5043_BBOFFSRES0NB
                                    357 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    358 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    359 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    360 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    361 	.globl _AX5043_AGCTARGET3NB
                                    362 	.globl _AX5043_AGCTARGET2NB
                                    363 	.globl _AX5043_AGCTARGET1NB
                                    364 	.globl _AX5043_AGCTARGET0NB
                                    365 	.globl _AX5043_AGCMINMAX3NB
                                    366 	.globl _AX5043_AGCMINMAX2NB
                                    367 	.globl _AX5043_AGCMINMAX1NB
                                    368 	.globl _AX5043_AGCMINMAX0NB
                                    369 	.globl _AX5043_AGCGAIN3NB
                                    370 	.globl _AX5043_AGCGAIN2NB
                                    371 	.globl _AX5043_AGCGAIN1NB
                                    372 	.globl _AX5043_AGCGAIN0NB
                                    373 	.globl _AX5043_AGCAHYST3NB
                                    374 	.globl _AX5043_AGCAHYST2NB
                                    375 	.globl _AX5043_AGCAHYST1NB
                                    376 	.globl _AX5043_AGCAHYST0NB
                                    377 	.globl _AX5043_0xF44NB
                                    378 	.globl _AX5043_0xF35NB
                                    379 	.globl _AX5043_0xF34NB
                                    380 	.globl _AX5043_0xF33NB
                                    381 	.globl _AX5043_0xF32NB
                                    382 	.globl _AX5043_0xF31NB
                                    383 	.globl _AX5043_0xF30NB
                                    384 	.globl _AX5043_0xF26NB
                                    385 	.globl _AX5043_0xF23NB
                                    386 	.globl _AX5043_0xF22NB
                                    387 	.globl _AX5043_0xF21NB
                                    388 	.globl _AX5043_0xF1CNB
                                    389 	.globl _AX5043_0xF18NB
                                    390 	.globl _AX5043_0xF0CNB
                                    391 	.globl _AX5043_0xF00NB
                                    392 	.globl _AX5043_XTALSTATUSNB
                                    393 	.globl _AX5043_XTALOSCNB
                                    394 	.globl _AX5043_XTALCAPNB
                                    395 	.globl _AX5043_XTALAMPLNB
                                    396 	.globl _AX5043_WAKEUPXOEARLYNB
                                    397 	.globl _AX5043_WAKEUPTIMER1NB
                                    398 	.globl _AX5043_WAKEUPTIMER0NB
                                    399 	.globl _AX5043_WAKEUPFREQ1NB
                                    400 	.globl _AX5043_WAKEUPFREQ0NB
                                    401 	.globl _AX5043_WAKEUP1NB
                                    402 	.globl _AX5043_WAKEUP0NB
                                    403 	.globl _AX5043_TXRATE2NB
                                    404 	.globl _AX5043_TXRATE1NB
                                    405 	.globl _AX5043_TXRATE0NB
                                    406 	.globl _AX5043_TXPWRCOEFFE1NB
                                    407 	.globl _AX5043_TXPWRCOEFFE0NB
                                    408 	.globl _AX5043_TXPWRCOEFFD1NB
                                    409 	.globl _AX5043_TXPWRCOEFFD0NB
                                    410 	.globl _AX5043_TXPWRCOEFFC1NB
                                    411 	.globl _AX5043_TXPWRCOEFFC0NB
                                    412 	.globl _AX5043_TXPWRCOEFFB1NB
                                    413 	.globl _AX5043_TXPWRCOEFFB0NB
                                    414 	.globl _AX5043_TXPWRCOEFFA1NB
                                    415 	.globl _AX5043_TXPWRCOEFFA0NB
                                    416 	.globl _AX5043_TRKRFFREQ2NB
                                    417 	.globl _AX5043_TRKRFFREQ1NB
                                    418 	.globl _AX5043_TRKRFFREQ0NB
                                    419 	.globl _AX5043_TRKPHASE1NB
                                    420 	.globl _AX5043_TRKPHASE0NB
                                    421 	.globl _AX5043_TRKFSKDEMOD1NB
                                    422 	.globl _AX5043_TRKFSKDEMOD0NB
                                    423 	.globl _AX5043_TRKFREQ1NB
                                    424 	.globl _AX5043_TRKFREQ0NB
                                    425 	.globl _AX5043_TRKDATARATE2NB
                                    426 	.globl _AX5043_TRKDATARATE1NB
                                    427 	.globl _AX5043_TRKDATARATE0NB
                                    428 	.globl _AX5043_TRKAMPLITUDE1NB
                                    429 	.globl _AX5043_TRKAMPLITUDE0NB
                                    430 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    431 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    432 	.globl _AX5043_TMGTXSETTLENB
                                    433 	.globl _AX5043_TMGTXBOOSTNB
                                    434 	.globl _AX5043_TMGRXSETTLENB
                                    435 	.globl _AX5043_TMGRXRSSINB
                                    436 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    437 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    438 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    439 	.globl _AX5043_TMGRXOFFSACQNB
                                    440 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    441 	.globl _AX5043_TMGRXBOOSTNB
                                    442 	.globl _AX5043_TMGRXAGCNB
                                    443 	.globl _AX5043_TIMER2NB
                                    444 	.globl _AX5043_TIMER1NB
                                    445 	.globl _AX5043_TIMER0NB
                                    446 	.globl _AX5043_SILICONREVISIONNB
                                    447 	.globl _AX5043_SCRATCHNB
                                    448 	.globl _AX5043_RXDATARATE2NB
                                    449 	.globl _AX5043_RXDATARATE1NB
                                    450 	.globl _AX5043_RXDATARATE0NB
                                    451 	.globl _AX5043_RSSIREFERENCENB
                                    452 	.globl _AX5043_RSSIABSTHRNB
                                    453 	.globl _AX5043_RSSINB
                                    454 	.globl _AX5043_REFNB
                                    455 	.globl _AX5043_RADIOSTATENB
                                    456 	.globl _AX5043_RADIOEVENTREQ1NB
                                    457 	.globl _AX5043_RADIOEVENTREQ0NB
                                    458 	.globl _AX5043_RADIOEVENTMASK1NB
                                    459 	.globl _AX5043_RADIOEVENTMASK0NB
                                    460 	.globl _AX5043_PWRMODENB
                                    461 	.globl _AX5043_PWRAMPNB
                                    462 	.globl _AX5043_POWSTICKYSTATNB
                                    463 	.globl _AX5043_POWSTATNB
                                    464 	.globl _AX5043_POWIRQMASKNB
                                    465 	.globl _AX5043_POWCTRL1NB
                                    466 	.globl _AX5043_PLLVCOIRNB
                                    467 	.globl _AX5043_PLLVCOINB
                                    468 	.globl _AX5043_PLLVCODIVNB
                                    469 	.globl _AX5043_PLLRNGCLKNB
                                    470 	.globl _AX5043_PLLRANGINGBNB
                                    471 	.globl _AX5043_PLLRANGINGANB
                                    472 	.globl _AX5043_PLLLOOPBOOSTNB
                                    473 	.globl _AX5043_PLLLOOPNB
                                    474 	.globl _AX5043_PLLLOCKDETNB
                                    475 	.globl _AX5043_PLLCPIBOOSTNB
                                    476 	.globl _AX5043_PLLCPINB
                                    477 	.globl _AX5043_PKTSTOREFLAGSNB
                                    478 	.globl _AX5043_PKTMISCFLAGSNB
                                    479 	.globl _AX5043_PKTCHUNKSIZENB
                                    480 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    481 	.globl _AX5043_PINSTATENB
                                    482 	.globl _AX5043_PINFUNCSYSCLKNB
                                    483 	.globl _AX5043_PINFUNCPWRAMPNB
                                    484 	.globl _AX5043_PINFUNCIRQNB
                                    485 	.globl _AX5043_PINFUNCDCLKNB
                                    486 	.globl _AX5043_PINFUNCDATANB
                                    487 	.globl _AX5043_PINFUNCANTSELNB
                                    488 	.globl _AX5043_MODULATIONNB
                                    489 	.globl _AX5043_MODCFGPNB
                                    490 	.globl _AX5043_MODCFGFNB
                                    491 	.globl _AX5043_MODCFGANB
                                    492 	.globl _AX5043_MAXRFOFFSET2NB
                                    493 	.globl _AX5043_MAXRFOFFSET1NB
                                    494 	.globl _AX5043_MAXRFOFFSET0NB
                                    495 	.globl _AX5043_MAXDROFFSET2NB
                                    496 	.globl _AX5043_MAXDROFFSET1NB
                                    497 	.globl _AX5043_MAXDROFFSET0NB
                                    498 	.globl _AX5043_MATCH1PAT1NB
                                    499 	.globl _AX5043_MATCH1PAT0NB
                                    500 	.globl _AX5043_MATCH1MINNB
                                    501 	.globl _AX5043_MATCH1MAXNB
                                    502 	.globl _AX5043_MATCH1LENNB
                                    503 	.globl _AX5043_MATCH0PAT3NB
                                    504 	.globl _AX5043_MATCH0PAT2NB
                                    505 	.globl _AX5043_MATCH0PAT1NB
                                    506 	.globl _AX5043_MATCH0PAT0NB
                                    507 	.globl _AX5043_MATCH0MINNB
                                    508 	.globl _AX5043_MATCH0MAXNB
                                    509 	.globl _AX5043_MATCH0LENNB
                                    510 	.globl _AX5043_LPOSCSTATUSNB
                                    511 	.globl _AX5043_LPOSCREF1NB
                                    512 	.globl _AX5043_LPOSCREF0NB
                                    513 	.globl _AX5043_LPOSCPER1NB
                                    514 	.globl _AX5043_LPOSCPER0NB
                                    515 	.globl _AX5043_LPOSCKFILT1NB
                                    516 	.globl _AX5043_LPOSCKFILT0NB
                                    517 	.globl _AX5043_LPOSCFREQ1NB
                                    518 	.globl _AX5043_LPOSCFREQ0NB
                                    519 	.globl _AX5043_LPOSCCONFIGNB
                                    520 	.globl _AX5043_IRQREQUEST1NB
                                    521 	.globl _AX5043_IRQREQUEST0NB
                                    522 	.globl _AX5043_IRQMASK1NB
                                    523 	.globl _AX5043_IRQMASK0NB
                                    524 	.globl _AX5043_IRQINVERSION1NB
                                    525 	.globl _AX5043_IRQINVERSION0NB
                                    526 	.globl _AX5043_IFFREQ1NB
                                    527 	.globl _AX5043_IFFREQ0NB
                                    528 	.globl _AX5043_GPADCPERIODNB
                                    529 	.globl _AX5043_GPADCCTRLNB
                                    530 	.globl _AX5043_GPADC13VALUE1NB
                                    531 	.globl _AX5043_GPADC13VALUE0NB
                                    532 	.globl _AX5043_FSKDMIN1NB
                                    533 	.globl _AX5043_FSKDMIN0NB
                                    534 	.globl _AX5043_FSKDMAX1NB
                                    535 	.globl _AX5043_FSKDMAX0NB
                                    536 	.globl _AX5043_FSKDEV2NB
                                    537 	.globl _AX5043_FSKDEV1NB
                                    538 	.globl _AX5043_FSKDEV0NB
                                    539 	.globl _AX5043_FREQB3NB
                                    540 	.globl _AX5043_FREQB2NB
                                    541 	.globl _AX5043_FREQB1NB
                                    542 	.globl _AX5043_FREQB0NB
                                    543 	.globl _AX5043_FREQA3NB
                                    544 	.globl _AX5043_FREQA2NB
                                    545 	.globl _AX5043_FREQA1NB
                                    546 	.globl _AX5043_FREQA0NB
                                    547 	.globl _AX5043_FRAMINGNB
                                    548 	.globl _AX5043_FIFOTHRESH1NB
                                    549 	.globl _AX5043_FIFOTHRESH0NB
                                    550 	.globl _AX5043_FIFOSTATNB
                                    551 	.globl _AX5043_FIFOFREE1NB
                                    552 	.globl _AX5043_FIFOFREE0NB
                                    553 	.globl _AX5043_FIFODATANB
                                    554 	.globl _AX5043_FIFOCOUNT1NB
                                    555 	.globl _AX5043_FIFOCOUNT0NB
                                    556 	.globl _AX5043_FECSYNCNB
                                    557 	.globl _AX5043_FECSTATUSNB
                                    558 	.globl _AX5043_FECNB
                                    559 	.globl _AX5043_ENCODINGNB
                                    560 	.globl _AX5043_DIVERSITYNB
                                    561 	.globl _AX5043_DECIMATIONNB
                                    562 	.globl _AX5043_DACVALUE1NB
                                    563 	.globl _AX5043_DACVALUE0NB
                                    564 	.globl _AX5043_DACCONFIGNB
                                    565 	.globl _AX5043_CRCINIT3NB
                                    566 	.globl _AX5043_CRCINIT2NB
                                    567 	.globl _AX5043_CRCINIT1NB
                                    568 	.globl _AX5043_CRCINIT0NB
                                    569 	.globl _AX5043_BGNDRSSITHRNB
                                    570 	.globl _AX5043_BGNDRSSIGAINNB
                                    571 	.globl _AX5043_BGNDRSSINB
                                    572 	.globl _AX5043_BBTUNENB
                                    573 	.globl _AX5043_BBOFFSCAPNB
                                    574 	.globl _AX5043_AMPLFILTERNB
                                    575 	.globl _AX5043_AGCCOUNTERNB
                                    576 	.globl _AX5043_AFSKSPACE1NB
                                    577 	.globl _AX5043_AFSKSPACE0NB
                                    578 	.globl _AX5043_AFSKMARK1NB
                                    579 	.globl _AX5043_AFSKMARK0NB
                                    580 	.globl _AX5043_AFSKCTRLNB
                                    581 	.globl _AX5043_TIMEGAIN3
                                    582 	.globl _AX5043_TIMEGAIN2
                                    583 	.globl _AX5043_TIMEGAIN1
                                    584 	.globl _AX5043_TIMEGAIN0
                                    585 	.globl _AX5043_RXPARAMSETS
                                    586 	.globl _AX5043_RXPARAMCURSET
                                    587 	.globl _AX5043_PKTMAXLEN
                                    588 	.globl _AX5043_PKTLENOFFSET
                                    589 	.globl _AX5043_PKTLENCFG
                                    590 	.globl _AX5043_PKTADDRMASK3
                                    591 	.globl _AX5043_PKTADDRMASK2
                                    592 	.globl _AX5043_PKTADDRMASK1
                                    593 	.globl _AX5043_PKTADDRMASK0
                                    594 	.globl _AX5043_PKTADDRCFG
                                    595 	.globl _AX5043_PKTADDR3
                                    596 	.globl _AX5043_PKTADDR2
                                    597 	.globl _AX5043_PKTADDR1
                                    598 	.globl _AX5043_PKTADDR0
                                    599 	.globl _AX5043_PHASEGAIN3
                                    600 	.globl _AX5043_PHASEGAIN2
                                    601 	.globl _AX5043_PHASEGAIN1
                                    602 	.globl _AX5043_PHASEGAIN0
                                    603 	.globl _AX5043_FREQUENCYLEAK
                                    604 	.globl _AX5043_FREQUENCYGAIND3
                                    605 	.globl _AX5043_FREQUENCYGAIND2
                                    606 	.globl _AX5043_FREQUENCYGAIND1
                                    607 	.globl _AX5043_FREQUENCYGAIND0
                                    608 	.globl _AX5043_FREQUENCYGAINC3
                                    609 	.globl _AX5043_FREQUENCYGAINC2
                                    610 	.globl _AX5043_FREQUENCYGAINC1
                                    611 	.globl _AX5043_FREQUENCYGAINC0
                                    612 	.globl _AX5043_FREQUENCYGAINB3
                                    613 	.globl _AX5043_FREQUENCYGAINB2
                                    614 	.globl _AX5043_FREQUENCYGAINB1
                                    615 	.globl _AX5043_FREQUENCYGAINB0
                                    616 	.globl _AX5043_FREQUENCYGAINA3
                                    617 	.globl _AX5043_FREQUENCYGAINA2
                                    618 	.globl _AX5043_FREQUENCYGAINA1
                                    619 	.globl _AX5043_FREQUENCYGAINA0
                                    620 	.globl _AX5043_FREQDEV13
                                    621 	.globl _AX5043_FREQDEV12
                                    622 	.globl _AX5043_FREQDEV11
                                    623 	.globl _AX5043_FREQDEV10
                                    624 	.globl _AX5043_FREQDEV03
                                    625 	.globl _AX5043_FREQDEV02
                                    626 	.globl _AX5043_FREQDEV01
                                    627 	.globl _AX5043_FREQDEV00
                                    628 	.globl _AX5043_FOURFSK3
                                    629 	.globl _AX5043_FOURFSK2
                                    630 	.globl _AX5043_FOURFSK1
                                    631 	.globl _AX5043_FOURFSK0
                                    632 	.globl _AX5043_DRGAIN3
                                    633 	.globl _AX5043_DRGAIN2
                                    634 	.globl _AX5043_DRGAIN1
                                    635 	.globl _AX5043_DRGAIN0
                                    636 	.globl _AX5043_BBOFFSRES3
                                    637 	.globl _AX5043_BBOFFSRES2
                                    638 	.globl _AX5043_BBOFFSRES1
                                    639 	.globl _AX5043_BBOFFSRES0
                                    640 	.globl _AX5043_AMPLITUDEGAIN3
                                    641 	.globl _AX5043_AMPLITUDEGAIN2
                                    642 	.globl _AX5043_AMPLITUDEGAIN1
                                    643 	.globl _AX5043_AMPLITUDEGAIN0
                                    644 	.globl _AX5043_AGCTARGET3
                                    645 	.globl _AX5043_AGCTARGET2
                                    646 	.globl _AX5043_AGCTARGET1
                                    647 	.globl _AX5043_AGCTARGET0
                                    648 	.globl _AX5043_AGCMINMAX3
                                    649 	.globl _AX5043_AGCMINMAX2
                                    650 	.globl _AX5043_AGCMINMAX1
                                    651 	.globl _AX5043_AGCMINMAX0
                                    652 	.globl _AX5043_AGCGAIN3
                                    653 	.globl _AX5043_AGCGAIN2
                                    654 	.globl _AX5043_AGCGAIN1
                                    655 	.globl _AX5043_AGCGAIN0
                                    656 	.globl _AX5043_AGCAHYST3
                                    657 	.globl _AX5043_AGCAHYST2
                                    658 	.globl _AX5043_AGCAHYST1
                                    659 	.globl _AX5043_AGCAHYST0
                                    660 	.globl _AX5043_0xF44
                                    661 	.globl _AX5043_0xF35
                                    662 	.globl _AX5043_0xF34
                                    663 	.globl _AX5043_0xF33
                                    664 	.globl _AX5043_0xF32
                                    665 	.globl _AX5043_0xF31
                                    666 	.globl _AX5043_0xF30
                                    667 	.globl _AX5043_0xF26
                                    668 	.globl _AX5043_0xF23
                                    669 	.globl _AX5043_0xF22
                                    670 	.globl _AX5043_0xF21
                                    671 	.globl _AX5043_0xF1C
                                    672 	.globl _AX5043_0xF18
                                    673 	.globl _AX5043_0xF0C
                                    674 	.globl _AX5043_0xF00
                                    675 	.globl _AX5043_XTALSTATUS
                                    676 	.globl _AX5043_XTALOSC
                                    677 	.globl _AX5043_XTALCAP
                                    678 	.globl _AX5043_XTALAMPL
                                    679 	.globl _AX5043_WAKEUPXOEARLY
                                    680 	.globl _AX5043_WAKEUPTIMER1
                                    681 	.globl _AX5043_WAKEUPTIMER0
                                    682 	.globl _AX5043_WAKEUPFREQ1
                                    683 	.globl _AX5043_WAKEUPFREQ0
                                    684 	.globl _AX5043_WAKEUP1
                                    685 	.globl _AX5043_WAKEUP0
                                    686 	.globl _AX5043_TXRATE2
                                    687 	.globl _AX5043_TXRATE1
                                    688 	.globl _AX5043_TXRATE0
                                    689 	.globl _AX5043_TXPWRCOEFFE1
                                    690 	.globl _AX5043_TXPWRCOEFFE0
                                    691 	.globl _AX5043_TXPWRCOEFFD1
                                    692 	.globl _AX5043_TXPWRCOEFFD0
                                    693 	.globl _AX5043_TXPWRCOEFFC1
                                    694 	.globl _AX5043_TXPWRCOEFFC0
                                    695 	.globl _AX5043_TXPWRCOEFFB1
                                    696 	.globl _AX5043_TXPWRCOEFFB0
                                    697 	.globl _AX5043_TXPWRCOEFFA1
                                    698 	.globl _AX5043_TXPWRCOEFFA0
                                    699 	.globl _AX5043_TRKRFFREQ2
                                    700 	.globl _AX5043_TRKRFFREQ1
                                    701 	.globl _AX5043_TRKRFFREQ0
                                    702 	.globl _AX5043_TRKPHASE1
                                    703 	.globl _AX5043_TRKPHASE0
                                    704 	.globl _AX5043_TRKFSKDEMOD1
                                    705 	.globl _AX5043_TRKFSKDEMOD0
                                    706 	.globl _AX5043_TRKFREQ1
                                    707 	.globl _AX5043_TRKFREQ0
                                    708 	.globl _AX5043_TRKDATARATE2
                                    709 	.globl _AX5043_TRKDATARATE1
                                    710 	.globl _AX5043_TRKDATARATE0
                                    711 	.globl _AX5043_TRKAMPLITUDE1
                                    712 	.globl _AX5043_TRKAMPLITUDE0
                                    713 	.globl _AX5043_TRKAFSKDEMOD1
                                    714 	.globl _AX5043_TRKAFSKDEMOD0
                                    715 	.globl _AX5043_TMGTXSETTLE
                                    716 	.globl _AX5043_TMGTXBOOST
                                    717 	.globl _AX5043_TMGRXSETTLE
                                    718 	.globl _AX5043_TMGRXRSSI
                                    719 	.globl _AX5043_TMGRXPREAMBLE3
                                    720 	.globl _AX5043_TMGRXPREAMBLE2
                                    721 	.globl _AX5043_TMGRXPREAMBLE1
                                    722 	.globl _AX5043_TMGRXOFFSACQ
                                    723 	.globl _AX5043_TMGRXCOARSEAGC
                                    724 	.globl _AX5043_TMGRXBOOST
                                    725 	.globl _AX5043_TMGRXAGC
                                    726 	.globl _AX5043_TIMER2
                                    727 	.globl _AX5043_TIMER1
                                    728 	.globl _AX5043_TIMER0
                                    729 	.globl _AX5043_SILICONREVISION
                                    730 	.globl _AX5043_SCRATCH
                                    731 	.globl _AX5043_RXDATARATE2
                                    732 	.globl _AX5043_RXDATARATE1
                                    733 	.globl _AX5043_RXDATARATE0
                                    734 	.globl _AX5043_RSSIREFERENCE
                                    735 	.globl _AX5043_RSSIABSTHR
                                    736 	.globl _AX5043_RSSI
                                    737 	.globl _AX5043_REF
                                    738 	.globl _AX5043_RADIOSTATE
                                    739 	.globl _AX5043_RADIOEVENTREQ1
                                    740 	.globl _AX5043_RADIOEVENTREQ0
                                    741 	.globl _AX5043_RADIOEVENTMASK1
                                    742 	.globl _AX5043_RADIOEVENTMASK0
                                    743 	.globl _AX5043_PWRMODE
                                    744 	.globl _AX5043_PWRAMP
                                    745 	.globl _AX5043_POWSTICKYSTAT
                                    746 	.globl _AX5043_POWSTAT
                                    747 	.globl _AX5043_POWIRQMASK
                                    748 	.globl _AX5043_POWCTRL1
                                    749 	.globl _AX5043_PLLVCOIR
                                    750 	.globl _AX5043_PLLVCOI
                                    751 	.globl _AX5043_PLLVCODIV
                                    752 	.globl _AX5043_PLLRNGCLK
                                    753 	.globl _AX5043_PLLRANGINGB
                                    754 	.globl _AX5043_PLLRANGINGA
                                    755 	.globl _AX5043_PLLLOOPBOOST
                                    756 	.globl _AX5043_PLLLOOP
                                    757 	.globl _AX5043_PLLLOCKDET
                                    758 	.globl _AX5043_PLLCPIBOOST
                                    759 	.globl _AX5043_PLLCPI
                                    760 	.globl _AX5043_PKTSTOREFLAGS
                                    761 	.globl _AX5043_PKTMISCFLAGS
                                    762 	.globl _AX5043_PKTCHUNKSIZE
                                    763 	.globl _AX5043_PKTACCEPTFLAGS
                                    764 	.globl _AX5043_PINSTATE
                                    765 	.globl _AX5043_PINFUNCSYSCLK
                                    766 	.globl _AX5043_PINFUNCPWRAMP
                                    767 	.globl _AX5043_PINFUNCIRQ
                                    768 	.globl _AX5043_PINFUNCDCLK
                                    769 	.globl _AX5043_PINFUNCDATA
                                    770 	.globl _AX5043_PINFUNCANTSEL
                                    771 	.globl _AX5043_MODULATION
                                    772 	.globl _AX5043_MODCFGP
                                    773 	.globl _AX5043_MODCFGF
                                    774 	.globl _AX5043_MODCFGA
                                    775 	.globl _AX5043_MAXRFOFFSET2
                                    776 	.globl _AX5043_MAXRFOFFSET1
                                    777 	.globl _AX5043_MAXRFOFFSET0
                                    778 	.globl _AX5043_MAXDROFFSET2
                                    779 	.globl _AX5043_MAXDROFFSET1
                                    780 	.globl _AX5043_MAXDROFFSET0
                                    781 	.globl _AX5043_MATCH1PAT1
                                    782 	.globl _AX5043_MATCH1PAT0
                                    783 	.globl _AX5043_MATCH1MIN
                                    784 	.globl _AX5043_MATCH1MAX
                                    785 	.globl _AX5043_MATCH1LEN
                                    786 	.globl _AX5043_MATCH0PAT3
                                    787 	.globl _AX5043_MATCH0PAT2
                                    788 	.globl _AX5043_MATCH0PAT1
                                    789 	.globl _AX5043_MATCH0PAT0
                                    790 	.globl _AX5043_MATCH0MIN
                                    791 	.globl _AX5043_MATCH0MAX
                                    792 	.globl _AX5043_MATCH0LEN
                                    793 	.globl _AX5043_LPOSCSTATUS
                                    794 	.globl _AX5043_LPOSCREF1
                                    795 	.globl _AX5043_LPOSCREF0
                                    796 	.globl _AX5043_LPOSCPER1
                                    797 	.globl _AX5043_LPOSCPER0
                                    798 	.globl _AX5043_LPOSCKFILT1
                                    799 	.globl _AX5043_LPOSCKFILT0
                                    800 	.globl _AX5043_LPOSCFREQ1
                                    801 	.globl _AX5043_LPOSCFREQ0
                                    802 	.globl _AX5043_LPOSCCONFIG
                                    803 	.globl _AX5043_IRQREQUEST1
                                    804 	.globl _AX5043_IRQREQUEST0
                                    805 	.globl _AX5043_IRQMASK1
                                    806 	.globl _AX5043_IRQMASK0
                                    807 	.globl _AX5043_IRQINVERSION1
                                    808 	.globl _AX5043_IRQINVERSION0
                                    809 	.globl _AX5043_IFFREQ1
                                    810 	.globl _AX5043_IFFREQ0
                                    811 	.globl _AX5043_GPADCPERIOD
                                    812 	.globl _AX5043_GPADCCTRL
                                    813 	.globl _AX5043_GPADC13VALUE1
                                    814 	.globl _AX5043_GPADC13VALUE0
                                    815 	.globl _AX5043_FSKDMIN1
                                    816 	.globl _AX5043_FSKDMIN0
                                    817 	.globl _AX5043_FSKDMAX1
                                    818 	.globl _AX5043_FSKDMAX0
                                    819 	.globl _AX5043_FSKDEV2
                                    820 	.globl _AX5043_FSKDEV1
                                    821 	.globl _AX5043_FSKDEV0
                                    822 	.globl _AX5043_FREQB3
                                    823 	.globl _AX5043_FREQB2
                                    824 	.globl _AX5043_FREQB1
                                    825 	.globl _AX5043_FREQB0
                                    826 	.globl _AX5043_FREQA3
                                    827 	.globl _AX5043_FREQA2
                                    828 	.globl _AX5043_FREQA1
                                    829 	.globl _AX5043_FREQA0
                                    830 	.globl _AX5043_FRAMING
                                    831 	.globl _AX5043_FIFOTHRESH1
                                    832 	.globl _AX5043_FIFOTHRESH0
                                    833 	.globl _AX5043_FIFOSTAT
                                    834 	.globl _AX5043_FIFOFREE1
                                    835 	.globl _AX5043_FIFOFREE0
                                    836 	.globl _AX5043_FIFODATA
                                    837 	.globl _AX5043_FIFOCOUNT1
                                    838 	.globl _AX5043_FIFOCOUNT0
                                    839 	.globl _AX5043_FECSYNC
                                    840 	.globl _AX5043_FECSTATUS
                                    841 	.globl _AX5043_FEC
                                    842 	.globl _AX5043_ENCODING
                                    843 	.globl _AX5043_DIVERSITY
                                    844 	.globl _AX5043_DECIMATION
                                    845 	.globl _AX5043_DACVALUE1
                                    846 	.globl _AX5043_DACVALUE0
                                    847 	.globl _AX5043_DACCONFIG
                                    848 	.globl _AX5043_CRCINIT3
                                    849 	.globl _AX5043_CRCINIT2
                                    850 	.globl _AX5043_CRCINIT1
                                    851 	.globl _AX5043_CRCINIT0
                                    852 	.globl _AX5043_BGNDRSSITHR
                                    853 	.globl _AX5043_BGNDRSSIGAIN
                                    854 	.globl _AX5043_BGNDRSSI
                                    855 	.globl _AX5043_BBTUNE
                                    856 	.globl _AX5043_BBOFFSCAP
                                    857 	.globl _AX5043_AMPLFILTER
                                    858 	.globl _AX5043_AGCCOUNTER
                                    859 	.globl _AX5043_AFSKSPACE1
                                    860 	.globl _AX5043_AFSKSPACE0
                                    861 	.globl _AX5043_AFSKMARK1
                                    862 	.globl _AX5043_AFSKMARK0
                                    863 	.globl _AX5043_AFSKCTRL
                                    864 	.globl _XTALREADY
                                    865 	.globl _XTALOSC
                                    866 	.globl _XTALAMPL
                                    867 	.globl _SILICONREV
                                    868 	.globl _SCRATCH3
                                    869 	.globl _SCRATCH2
                                    870 	.globl _SCRATCH1
                                    871 	.globl _SCRATCH0
                                    872 	.globl _RADIOMUX
                                    873 	.globl _RADIOFSTATADDR
                                    874 	.globl _RADIOFSTATADDR1
                                    875 	.globl _RADIOFSTATADDR0
                                    876 	.globl _RADIOFDATAADDR
                                    877 	.globl _RADIOFDATAADDR1
                                    878 	.globl _RADIOFDATAADDR0
                                    879 	.globl _OSCRUN
                                    880 	.globl _OSCREADY
                                    881 	.globl _OSCFORCERUN
                                    882 	.globl _OSCCALIB
                                    883 	.globl _MISCCTRL
                                    884 	.globl _LPXOSCGM
                                    885 	.globl _LPOSCREF
                                    886 	.globl _LPOSCREF1
                                    887 	.globl _LPOSCREF0
                                    888 	.globl _LPOSCPER
                                    889 	.globl _LPOSCPER1
                                    890 	.globl _LPOSCPER0
                                    891 	.globl _LPOSCKFILT
                                    892 	.globl _LPOSCKFILT1
                                    893 	.globl _LPOSCKFILT0
                                    894 	.globl _LPOSCFREQ
                                    895 	.globl _LPOSCFREQ1
                                    896 	.globl _LPOSCFREQ0
                                    897 	.globl _LPOSCCONFIG
                                    898 	.globl _PINSEL
                                    899 	.globl _PINCHGC
                                    900 	.globl _PINCHGB
                                    901 	.globl _PINCHGA
                                    902 	.globl _PALTRADIO
                                    903 	.globl _PALTC
                                    904 	.globl _PALTB
                                    905 	.globl _PALTA
                                    906 	.globl _INTCHGC
                                    907 	.globl _INTCHGB
                                    908 	.globl _INTCHGA
                                    909 	.globl _EXTIRQ
                                    910 	.globl _GPIOENABLE
                                    911 	.globl _ANALOGA
                                    912 	.globl _FRCOSCREF
                                    913 	.globl _FRCOSCREF1
                                    914 	.globl _FRCOSCREF0
                                    915 	.globl _FRCOSCPER
                                    916 	.globl _FRCOSCPER1
                                    917 	.globl _FRCOSCPER0
                                    918 	.globl _FRCOSCKFILT
                                    919 	.globl _FRCOSCKFILT1
                                    920 	.globl _FRCOSCKFILT0
                                    921 	.globl _FRCOSCFREQ
                                    922 	.globl _FRCOSCFREQ1
                                    923 	.globl _FRCOSCFREQ0
                                    924 	.globl _FRCOSCCTRL
                                    925 	.globl _FRCOSCCONFIG
                                    926 	.globl _DMA1CONFIG
                                    927 	.globl _DMA1ADDR
                                    928 	.globl _DMA1ADDR1
                                    929 	.globl _DMA1ADDR0
                                    930 	.globl _DMA0CONFIG
                                    931 	.globl _DMA0ADDR
                                    932 	.globl _DMA0ADDR1
                                    933 	.globl _DMA0ADDR0
                                    934 	.globl _ADCTUNE2
                                    935 	.globl _ADCTUNE1
                                    936 	.globl _ADCTUNE0
                                    937 	.globl _ADCCH3VAL
                                    938 	.globl _ADCCH3VAL1
                                    939 	.globl _ADCCH3VAL0
                                    940 	.globl _ADCCH2VAL
                                    941 	.globl _ADCCH2VAL1
                                    942 	.globl _ADCCH2VAL0
                                    943 	.globl _ADCCH1VAL
                                    944 	.globl _ADCCH1VAL1
                                    945 	.globl _ADCCH1VAL0
                                    946 	.globl _ADCCH0VAL
                                    947 	.globl _ADCCH0VAL1
                                    948 	.globl _ADCCH0VAL0
                                    949 ;--------------------------------------------------------
                                    950 ; special function registers
                                    951 ;--------------------------------------------------------
                                    952 	.area RSEG    (ABS,DATA)
      000000                        953 	.org 0x0000
                           0000E0   954 G$ACC$0$0 == 0x00e0
                           0000E0   955 _ACC	=	0x00e0
                           0000F0   956 G$B$0$0 == 0x00f0
                           0000F0   957 _B	=	0x00f0
                           000083   958 G$DPH$0$0 == 0x0083
                           000083   959 _DPH	=	0x0083
                           000085   960 G$DPH1$0$0 == 0x0085
                           000085   961 _DPH1	=	0x0085
                           000082   962 G$DPL$0$0 == 0x0082
                           000082   963 _DPL	=	0x0082
                           000084   964 G$DPL1$0$0 == 0x0084
                           000084   965 _DPL1	=	0x0084
                           008382   966 G$DPTR0$0$0 == 0x8382
                           008382   967 _DPTR0	=	0x8382
                           008584   968 G$DPTR1$0$0 == 0x8584
                           008584   969 _DPTR1	=	0x8584
                           000086   970 G$DPS$0$0 == 0x0086
                           000086   971 _DPS	=	0x0086
                           0000A0   972 G$E2IE$0$0 == 0x00a0
                           0000A0   973 _E2IE	=	0x00a0
                           0000C0   974 G$E2IP$0$0 == 0x00c0
                           0000C0   975 _E2IP	=	0x00c0
                           000098   976 G$EIE$0$0 == 0x0098
                           000098   977 _EIE	=	0x0098
                           0000B0   978 G$EIP$0$0 == 0x00b0
                           0000B0   979 _EIP	=	0x00b0
                           0000A8   980 G$IE$0$0 == 0x00a8
                           0000A8   981 _IE	=	0x00a8
                           0000B8   982 G$IP$0$0 == 0x00b8
                           0000B8   983 _IP	=	0x00b8
                           000087   984 G$PCON$0$0 == 0x0087
                           000087   985 _PCON	=	0x0087
                           0000D0   986 G$PSW$0$0 == 0x00d0
                           0000D0   987 _PSW	=	0x00d0
                           000081   988 G$SP$0$0 == 0x0081
                           000081   989 _SP	=	0x0081
                           0000D9   990 G$XPAGE$0$0 == 0x00d9
                           0000D9   991 _XPAGE	=	0x00d9
                           0000D9   992 G$_XPAGE$0$0 == 0x00d9
                           0000D9   993 __XPAGE	=	0x00d9
                           0000CA   994 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   995 _ADCCH0CONFIG	=	0x00ca
                           0000CB   996 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   997 _ADCCH1CONFIG	=	0x00cb
                           0000D2   998 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   999 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1000 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3  1001 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1002 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1  1003 _ADCCLKSRC	=	0x00d1
                           0000C9  1004 G$ADCCONV$0$0 == 0x00c9
                           0000C9  1005 _ADCCONV	=	0x00c9
                           0000E1  1006 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1  1007 _ANALOGCOMP	=	0x00e1
                           0000C6  1008 G$CLKCON$0$0 == 0x00c6
                           0000C6  1009 _CLKCON	=	0x00c6
                           0000C7  1010 G$CLKSTAT$0$0 == 0x00c7
                           0000C7  1011 _CLKSTAT	=	0x00c7
                           000097  1012 G$CODECONFIG$0$0 == 0x0097
                           000097  1013 _CODECONFIG	=	0x0097
                           0000E3  1014 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3  1015 _DBGLNKBUF	=	0x00e3
                           0000E2  1016 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2  1017 _DBGLNKSTAT	=	0x00e2
                           000089  1018 G$DIRA$0$0 == 0x0089
                           000089  1019 _DIRA	=	0x0089
                           00008A  1020 G$DIRB$0$0 == 0x008a
                           00008A  1021 _DIRB	=	0x008a
                           00008B  1022 G$DIRC$0$0 == 0x008b
                           00008B  1023 _DIRC	=	0x008b
                           00008E  1024 G$DIRR$0$0 == 0x008e
                           00008E  1025 _DIRR	=	0x008e
                           0000C8  1026 G$PINA$0$0 == 0x00c8
                           0000C8  1027 _PINA	=	0x00c8
                           0000E8  1028 G$PINB$0$0 == 0x00e8
                           0000E8  1029 _PINB	=	0x00e8
                           0000F8  1030 G$PINC$0$0 == 0x00f8
                           0000F8  1031 _PINC	=	0x00f8
                           00008D  1032 G$PINR$0$0 == 0x008d
                           00008D  1033 _PINR	=	0x008d
                           000080  1034 G$PORTA$0$0 == 0x0080
                           000080  1035 _PORTA	=	0x0080
                           000088  1036 G$PORTB$0$0 == 0x0088
                           000088  1037 _PORTB	=	0x0088
                           000090  1038 G$PORTC$0$0 == 0x0090
                           000090  1039 _PORTC	=	0x0090
                           00008C  1040 G$PORTR$0$0 == 0x008c
                           00008C  1041 _PORTR	=	0x008c
                           0000CE  1042 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE  1043 _IC0CAPT0	=	0x00ce
                           0000CF  1044 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF  1045 _IC0CAPT1	=	0x00cf
                           00CFCE  1046 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE  1047 _IC0CAPT	=	0xcfce
                           0000CC  1048 G$IC0MODE$0$0 == 0x00cc
                           0000CC  1049 _IC0MODE	=	0x00cc
                           0000CD  1050 G$IC0STATUS$0$0 == 0x00cd
                           0000CD  1051 _IC0STATUS	=	0x00cd
                           0000D6  1052 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6  1053 _IC1CAPT0	=	0x00d6
                           0000D7  1054 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7  1055 _IC1CAPT1	=	0x00d7
                           00D7D6  1056 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6  1057 _IC1CAPT	=	0xd7d6
                           0000D4  1058 G$IC1MODE$0$0 == 0x00d4
                           0000D4  1059 _IC1MODE	=	0x00d4
                           0000D5  1060 G$IC1STATUS$0$0 == 0x00d5
                           0000D5  1061 _IC1STATUS	=	0x00d5
                           000092  1062 G$NVADDR0$0$0 == 0x0092
                           000092  1063 _NVADDR0	=	0x0092
                           000093  1064 G$NVADDR1$0$0 == 0x0093
                           000093  1065 _NVADDR1	=	0x0093
                           009392  1066 G$NVADDR$0$0 == 0x9392
                           009392  1067 _NVADDR	=	0x9392
                           000094  1068 G$NVDATA0$0$0 == 0x0094
                           000094  1069 _NVDATA0	=	0x0094
                           000095  1070 G$NVDATA1$0$0 == 0x0095
                           000095  1071 _NVDATA1	=	0x0095
                           009594  1072 G$NVDATA$0$0 == 0x9594
                           009594  1073 _NVDATA	=	0x9594
                           000096  1074 G$NVKEY$0$0 == 0x0096
                           000096  1075 _NVKEY	=	0x0096
                           000091  1076 G$NVSTATUS$0$0 == 0x0091
                           000091  1077 _NVSTATUS	=	0x0091
                           0000BC  1078 G$OC0COMP0$0$0 == 0x00bc
                           0000BC  1079 _OC0COMP0	=	0x00bc
                           0000BD  1080 G$OC0COMP1$0$0 == 0x00bd
                           0000BD  1081 _OC0COMP1	=	0x00bd
                           00BDBC  1082 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC  1083 _OC0COMP	=	0xbdbc
                           0000B9  1084 G$OC0MODE$0$0 == 0x00b9
                           0000B9  1085 _OC0MODE	=	0x00b9
                           0000BA  1086 G$OC0PIN$0$0 == 0x00ba
                           0000BA  1087 _OC0PIN	=	0x00ba
                           0000BB  1088 G$OC0STATUS$0$0 == 0x00bb
                           0000BB  1089 _OC0STATUS	=	0x00bb
                           0000C4  1090 G$OC1COMP0$0$0 == 0x00c4
                           0000C4  1091 _OC1COMP0	=	0x00c4
                           0000C5  1092 G$OC1COMP1$0$0 == 0x00c5
                           0000C5  1093 _OC1COMP1	=	0x00c5
                           00C5C4  1094 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4  1095 _OC1COMP	=	0xc5c4
                           0000C1  1096 G$OC1MODE$0$0 == 0x00c1
                           0000C1  1097 _OC1MODE	=	0x00c1
                           0000C2  1098 G$OC1PIN$0$0 == 0x00c2
                           0000C2  1099 _OC1PIN	=	0x00c2
                           0000C3  1100 G$OC1STATUS$0$0 == 0x00c3
                           0000C3  1101 _OC1STATUS	=	0x00c3
                           0000B1  1102 G$RADIOACC$0$0 == 0x00b1
                           0000B1  1103 _RADIOACC	=	0x00b1
                           0000B3  1104 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3  1105 _RADIOADDR0	=	0x00b3
                           0000B2  1106 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2  1107 _RADIOADDR1	=	0x00b2
                           00B2B3  1108 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3  1109 _RADIOADDR	=	0xb2b3
                           0000B7  1110 G$RADIODATA0$0$0 == 0x00b7
                           0000B7  1111 _RADIODATA0	=	0x00b7
                           0000B6  1112 G$RADIODATA1$0$0 == 0x00b6
                           0000B6  1113 _RADIODATA1	=	0x00b6
                           0000B5  1114 G$RADIODATA2$0$0 == 0x00b5
                           0000B5  1115 _RADIODATA2	=	0x00b5
                           0000B4  1116 G$RADIODATA3$0$0 == 0x00b4
                           0000B4  1117 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1118 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7  1119 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1120 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE  1121 _RADIOSTAT0	=	0x00be
                           0000BF  1122 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF  1123 _RADIOSTAT1	=	0x00bf
                           00BFBE  1124 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE  1125 _RADIOSTAT	=	0xbfbe
                           0000DF  1126 G$SPCLKSRC$0$0 == 0x00df
                           0000DF  1127 _SPCLKSRC	=	0x00df
                           0000DC  1128 G$SPMODE$0$0 == 0x00dc
                           0000DC  1129 _SPMODE	=	0x00dc
                           0000DE  1130 G$SPSHREG$0$0 == 0x00de
                           0000DE  1131 _SPSHREG	=	0x00de
                           0000DD  1132 G$SPSTATUS$0$0 == 0x00dd
                           0000DD  1133 _SPSTATUS	=	0x00dd
                           00009A  1134 G$T0CLKSRC$0$0 == 0x009a
                           00009A  1135 _T0CLKSRC	=	0x009a
                           00009C  1136 G$T0CNT0$0$0 == 0x009c
                           00009C  1137 _T0CNT0	=	0x009c
                           00009D  1138 G$T0CNT1$0$0 == 0x009d
                           00009D  1139 _T0CNT1	=	0x009d
                           009D9C  1140 G$T0CNT$0$0 == 0x9d9c
                           009D9C  1141 _T0CNT	=	0x9d9c
                           000099  1142 G$T0MODE$0$0 == 0x0099
                           000099  1143 _T0MODE	=	0x0099
                           00009E  1144 G$T0PERIOD0$0$0 == 0x009e
                           00009E  1145 _T0PERIOD0	=	0x009e
                           00009F  1146 G$T0PERIOD1$0$0 == 0x009f
                           00009F  1147 _T0PERIOD1	=	0x009f
                           009F9E  1148 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E  1149 _T0PERIOD	=	0x9f9e
                           00009B  1150 G$T0STATUS$0$0 == 0x009b
                           00009B  1151 _T0STATUS	=	0x009b
                           0000A2  1152 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2  1153 _T1CLKSRC	=	0x00a2
                           0000A4  1154 G$T1CNT0$0$0 == 0x00a4
                           0000A4  1155 _T1CNT0	=	0x00a4
                           0000A5  1156 G$T1CNT1$0$0 == 0x00a5
                           0000A5  1157 _T1CNT1	=	0x00a5
                           00A5A4  1158 G$T1CNT$0$0 == 0xa5a4
                           00A5A4  1159 _T1CNT	=	0xa5a4
                           0000A1  1160 G$T1MODE$0$0 == 0x00a1
                           0000A1  1161 _T1MODE	=	0x00a1
                           0000A6  1162 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6  1163 _T1PERIOD0	=	0x00a6
                           0000A7  1164 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7  1165 _T1PERIOD1	=	0x00a7
                           00A7A6  1166 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6  1167 _T1PERIOD	=	0xa7a6
                           0000A3  1168 G$T1STATUS$0$0 == 0x00a3
                           0000A3  1169 _T1STATUS	=	0x00a3
                           0000AA  1170 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA  1171 _T2CLKSRC	=	0x00aa
                           0000AC  1172 G$T2CNT0$0$0 == 0x00ac
                           0000AC  1173 _T2CNT0	=	0x00ac
                           0000AD  1174 G$T2CNT1$0$0 == 0x00ad
                           0000AD  1175 _T2CNT1	=	0x00ad
                           00ADAC  1176 G$T2CNT$0$0 == 0xadac
                           00ADAC  1177 _T2CNT	=	0xadac
                           0000A9  1178 G$T2MODE$0$0 == 0x00a9
                           0000A9  1179 _T2MODE	=	0x00a9
                           0000AE  1180 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE  1181 _T2PERIOD0	=	0x00ae
                           0000AF  1182 G$T2PERIOD1$0$0 == 0x00af
                           0000AF  1183 _T2PERIOD1	=	0x00af
                           00AFAE  1184 G$T2PERIOD$0$0 == 0xafae
                           00AFAE  1185 _T2PERIOD	=	0xafae
                           0000AB  1186 G$T2STATUS$0$0 == 0x00ab
                           0000AB  1187 _T2STATUS	=	0x00ab
                           0000E4  1188 G$U0CTRL$0$0 == 0x00e4
                           0000E4  1189 _U0CTRL	=	0x00e4
                           0000E7  1190 G$U0MODE$0$0 == 0x00e7
                           0000E7  1191 _U0MODE	=	0x00e7
                           0000E6  1192 G$U0SHREG$0$0 == 0x00e6
                           0000E6  1193 _U0SHREG	=	0x00e6
                           0000E5  1194 G$U0STATUS$0$0 == 0x00e5
                           0000E5  1195 _U0STATUS	=	0x00e5
                           0000EC  1196 G$U1CTRL$0$0 == 0x00ec
                           0000EC  1197 _U1CTRL	=	0x00ec
                           0000EF  1198 G$U1MODE$0$0 == 0x00ef
                           0000EF  1199 _U1MODE	=	0x00ef
                           0000EE  1200 G$U1SHREG$0$0 == 0x00ee
                           0000EE  1201 _U1SHREG	=	0x00ee
                           0000ED  1202 G$U1STATUS$0$0 == 0x00ed
                           0000ED  1203 _U1STATUS	=	0x00ed
                           0000DA  1204 G$WDTCFG$0$0 == 0x00da
                           0000DA  1205 _WDTCFG	=	0x00da
                           0000DB  1206 G$WDTRESET$0$0 == 0x00db
                           0000DB  1207 _WDTRESET	=	0x00db
                           0000F1  1208 G$WTCFGA$0$0 == 0x00f1
                           0000F1  1209 _WTCFGA	=	0x00f1
                           0000F9  1210 G$WTCFGB$0$0 == 0x00f9
                           0000F9  1211 _WTCFGB	=	0x00f9
                           0000F2  1212 G$WTCNTA0$0$0 == 0x00f2
                           0000F2  1213 _WTCNTA0	=	0x00f2
                           0000F3  1214 G$WTCNTA1$0$0 == 0x00f3
                           0000F3  1215 _WTCNTA1	=	0x00f3
                           00F3F2  1216 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2  1217 _WTCNTA	=	0xf3f2
                           0000FA  1218 G$WTCNTB0$0$0 == 0x00fa
                           0000FA  1219 _WTCNTB0	=	0x00fa
                           0000FB  1220 G$WTCNTB1$0$0 == 0x00fb
                           0000FB  1221 _WTCNTB1	=	0x00fb
                           00FBFA  1222 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA  1223 _WTCNTB	=	0xfbfa
                           0000EB  1224 G$WTCNTR1$0$0 == 0x00eb
                           0000EB  1225 _WTCNTR1	=	0x00eb
                           0000F4  1226 G$WTEVTA0$0$0 == 0x00f4
                           0000F4  1227 _WTEVTA0	=	0x00f4
                           0000F5  1228 G$WTEVTA1$0$0 == 0x00f5
                           0000F5  1229 _WTEVTA1	=	0x00f5
                           00F5F4  1230 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4  1231 _WTEVTA	=	0xf5f4
                           0000F6  1232 G$WTEVTB0$0$0 == 0x00f6
                           0000F6  1233 _WTEVTB0	=	0x00f6
                           0000F7  1234 G$WTEVTB1$0$0 == 0x00f7
                           0000F7  1235 _WTEVTB1	=	0x00f7
                           00F7F6  1236 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6  1237 _WTEVTB	=	0xf7f6
                           0000FC  1238 G$WTEVTC0$0$0 == 0x00fc
                           0000FC  1239 _WTEVTC0	=	0x00fc
                           0000FD  1240 G$WTEVTC1$0$0 == 0x00fd
                           0000FD  1241 _WTEVTC1	=	0x00fd
                           00FDFC  1242 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC  1243 _WTEVTC	=	0xfdfc
                           0000FE  1244 G$WTEVTD0$0$0 == 0x00fe
                           0000FE  1245 _WTEVTD0	=	0x00fe
                           0000FF  1246 G$WTEVTD1$0$0 == 0x00ff
                           0000FF  1247 _WTEVTD1	=	0x00ff
                           00FFFE  1248 G$WTEVTD$0$0 == 0xfffe
                           00FFFE  1249 _WTEVTD	=	0xfffe
                           0000E9  1250 G$WTIRQEN$0$0 == 0x00e9
                           0000E9  1251 _WTIRQEN	=	0x00e9
                           0000EA  1252 G$WTSTAT$0$0 == 0x00ea
                           0000EA  1253 _WTSTAT	=	0x00ea
                                   1254 ;--------------------------------------------------------
                                   1255 ; special function bits
                                   1256 ;--------------------------------------------------------
                                   1257 	.area RSEG    (ABS,DATA)
      000000                       1258 	.org 0x0000
                           0000E0  1259 G$ACC_0$0$0 == 0x00e0
                           0000E0  1260 _ACC_0	=	0x00e0
                           0000E1  1261 G$ACC_1$0$0 == 0x00e1
                           0000E1  1262 _ACC_1	=	0x00e1
                           0000E2  1263 G$ACC_2$0$0 == 0x00e2
                           0000E2  1264 _ACC_2	=	0x00e2
                           0000E3  1265 G$ACC_3$0$0 == 0x00e3
                           0000E3  1266 _ACC_3	=	0x00e3
                           0000E4  1267 G$ACC_4$0$0 == 0x00e4
                           0000E4  1268 _ACC_4	=	0x00e4
                           0000E5  1269 G$ACC_5$0$0 == 0x00e5
                           0000E5  1270 _ACC_5	=	0x00e5
                           0000E6  1271 G$ACC_6$0$0 == 0x00e6
                           0000E6  1272 _ACC_6	=	0x00e6
                           0000E7  1273 G$ACC_7$0$0 == 0x00e7
                           0000E7  1274 _ACC_7	=	0x00e7
                           0000F0  1275 G$B_0$0$0 == 0x00f0
                           0000F0  1276 _B_0	=	0x00f0
                           0000F1  1277 G$B_1$0$0 == 0x00f1
                           0000F1  1278 _B_1	=	0x00f1
                           0000F2  1279 G$B_2$0$0 == 0x00f2
                           0000F2  1280 _B_2	=	0x00f2
                           0000F3  1281 G$B_3$0$0 == 0x00f3
                           0000F3  1282 _B_3	=	0x00f3
                           0000F4  1283 G$B_4$0$0 == 0x00f4
                           0000F4  1284 _B_4	=	0x00f4
                           0000F5  1285 G$B_5$0$0 == 0x00f5
                           0000F5  1286 _B_5	=	0x00f5
                           0000F6  1287 G$B_6$0$0 == 0x00f6
                           0000F6  1288 _B_6	=	0x00f6
                           0000F7  1289 G$B_7$0$0 == 0x00f7
                           0000F7  1290 _B_7	=	0x00f7
                           0000A0  1291 G$E2IE_0$0$0 == 0x00a0
                           0000A0  1292 _E2IE_0	=	0x00a0
                           0000A1  1293 G$E2IE_1$0$0 == 0x00a1
                           0000A1  1294 _E2IE_1	=	0x00a1
                           0000A2  1295 G$E2IE_2$0$0 == 0x00a2
                           0000A2  1296 _E2IE_2	=	0x00a2
                           0000A3  1297 G$E2IE_3$0$0 == 0x00a3
                           0000A3  1298 _E2IE_3	=	0x00a3
                           0000A4  1299 G$E2IE_4$0$0 == 0x00a4
                           0000A4  1300 _E2IE_4	=	0x00a4
                           0000A5  1301 G$E2IE_5$0$0 == 0x00a5
                           0000A5  1302 _E2IE_5	=	0x00a5
                           0000A6  1303 G$E2IE_6$0$0 == 0x00a6
                           0000A6  1304 _E2IE_6	=	0x00a6
                           0000A7  1305 G$E2IE_7$0$0 == 0x00a7
                           0000A7  1306 _E2IE_7	=	0x00a7
                           0000C0  1307 G$E2IP_0$0$0 == 0x00c0
                           0000C0  1308 _E2IP_0	=	0x00c0
                           0000C1  1309 G$E2IP_1$0$0 == 0x00c1
                           0000C1  1310 _E2IP_1	=	0x00c1
                           0000C2  1311 G$E2IP_2$0$0 == 0x00c2
                           0000C2  1312 _E2IP_2	=	0x00c2
                           0000C3  1313 G$E2IP_3$0$0 == 0x00c3
                           0000C3  1314 _E2IP_3	=	0x00c3
                           0000C4  1315 G$E2IP_4$0$0 == 0x00c4
                           0000C4  1316 _E2IP_4	=	0x00c4
                           0000C5  1317 G$E2IP_5$0$0 == 0x00c5
                           0000C5  1318 _E2IP_5	=	0x00c5
                           0000C6  1319 G$E2IP_6$0$0 == 0x00c6
                           0000C6  1320 _E2IP_6	=	0x00c6
                           0000C7  1321 G$E2IP_7$0$0 == 0x00c7
                           0000C7  1322 _E2IP_7	=	0x00c7
                           000098  1323 G$EIE_0$0$0 == 0x0098
                           000098  1324 _EIE_0	=	0x0098
                           000099  1325 G$EIE_1$0$0 == 0x0099
                           000099  1326 _EIE_1	=	0x0099
                           00009A  1327 G$EIE_2$0$0 == 0x009a
                           00009A  1328 _EIE_2	=	0x009a
                           00009B  1329 G$EIE_3$0$0 == 0x009b
                           00009B  1330 _EIE_3	=	0x009b
                           00009C  1331 G$EIE_4$0$0 == 0x009c
                           00009C  1332 _EIE_4	=	0x009c
                           00009D  1333 G$EIE_5$0$0 == 0x009d
                           00009D  1334 _EIE_5	=	0x009d
                           00009E  1335 G$EIE_6$0$0 == 0x009e
                           00009E  1336 _EIE_6	=	0x009e
                           00009F  1337 G$EIE_7$0$0 == 0x009f
                           00009F  1338 _EIE_7	=	0x009f
                           0000B0  1339 G$EIP_0$0$0 == 0x00b0
                           0000B0  1340 _EIP_0	=	0x00b0
                           0000B1  1341 G$EIP_1$0$0 == 0x00b1
                           0000B1  1342 _EIP_1	=	0x00b1
                           0000B2  1343 G$EIP_2$0$0 == 0x00b2
                           0000B2  1344 _EIP_2	=	0x00b2
                           0000B3  1345 G$EIP_3$0$0 == 0x00b3
                           0000B3  1346 _EIP_3	=	0x00b3
                           0000B4  1347 G$EIP_4$0$0 == 0x00b4
                           0000B4  1348 _EIP_4	=	0x00b4
                           0000B5  1349 G$EIP_5$0$0 == 0x00b5
                           0000B5  1350 _EIP_5	=	0x00b5
                           0000B6  1351 G$EIP_6$0$0 == 0x00b6
                           0000B6  1352 _EIP_6	=	0x00b6
                           0000B7  1353 G$EIP_7$0$0 == 0x00b7
                           0000B7  1354 _EIP_7	=	0x00b7
                           0000A8  1355 G$IE_0$0$0 == 0x00a8
                           0000A8  1356 _IE_0	=	0x00a8
                           0000A9  1357 G$IE_1$0$0 == 0x00a9
                           0000A9  1358 _IE_1	=	0x00a9
                           0000AA  1359 G$IE_2$0$0 == 0x00aa
                           0000AA  1360 _IE_2	=	0x00aa
                           0000AB  1361 G$IE_3$0$0 == 0x00ab
                           0000AB  1362 _IE_3	=	0x00ab
                           0000AC  1363 G$IE_4$0$0 == 0x00ac
                           0000AC  1364 _IE_4	=	0x00ac
                           0000AD  1365 G$IE_5$0$0 == 0x00ad
                           0000AD  1366 _IE_5	=	0x00ad
                           0000AE  1367 G$IE_6$0$0 == 0x00ae
                           0000AE  1368 _IE_6	=	0x00ae
                           0000AF  1369 G$IE_7$0$0 == 0x00af
                           0000AF  1370 _IE_7	=	0x00af
                           0000AF  1371 G$EA$0$0 == 0x00af
                           0000AF  1372 _EA	=	0x00af
                           0000B8  1373 G$IP_0$0$0 == 0x00b8
                           0000B8  1374 _IP_0	=	0x00b8
                           0000B9  1375 G$IP_1$0$0 == 0x00b9
                           0000B9  1376 _IP_1	=	0x00b9
                           0000BA  1377 G$IP_2$0$0 == 0x00ba
                           0000BA  1378 _IP_2	=	0x00ba
                           0000BB  1379 G$IP_3$0$0 == 0x00bb
                           0000BB  1380 _IP_3	=	0x00bb
                           0000BC  1381 G$IP_4$0$0 == 0x00bc
                           0000BC  1382 _IP_4	=	0x00bc
                           0000BD  1383 G$IP_5$0$0 == 0x00bd
                           0000BD  1384 _IP_5	=	0x00bd
                           0000BE  1385 G$IP_6$0$0 == 0x00be
                           0000BE  1386 _IP_6	=	0x00be
                           0000BF  1387 G$IP_7$0$0 == 0x00bf
                           0000BF  1388 _IP_7	=	0x00bf
                           0000D0  1389 G$P$0$0 == 0x00d0
                           0000D0  1390 _P	=	0x00d0
                           0000D1  1391 G$F1$0$0 == 0x00d1
                           0000D1  1392 _F1	=	0x00d1
                           0000D2  1393 G$OV$0$0 == 0x00d2
                           0000D2  1394 _OV	=	0x00d2
                           0000D3  1395 G$RS0$0$0 == 0x00d3
                           0000D3  1396 _RS0	=	0x00d3
                           0000D4  1397 G$RS1$0$0 == 0x00d4
                           0000D4  1398 _RS1	=	0x00d4
                           0000D5  1399 G$F0$0$0 == 0x00d5
                           0000D5  1400 _F0	=	0x00d5
                           0000D6  1401 G$AC$0$0 == 0x00d6
                           0000D6  1402 _AC	=	0x00d6
                           0000D7  1403 G$CY$0$0 == 0x00d7
                           0000D7  1404 _CY	=	0x00d7
                           0000C8  1405 G$PINA_0$0$0 == 0x00c8
                           0000C8  1406 _PINA_0	=	0x00c8
                           0000C9  1407 G$PINA_1$0$0 == 0x00c9
                           0000C9  1408 _PINA_1	=	0x00c9
                           0000CA  1409 G$PINA_2$0$0 == 0x00ca
                           0000CA  1410 _PINA_2	=	0x00ca
                           0000CB  1411 G$PINA_3$0$0 == 0x00cb
                           0000CB  1412 _PINA_3	=	0x00cb
                           0000CC  1413 G$PINA_4$0$0 == 0x00cc
                           0000CC  1414 _PINA_4	=	0x00cc
                           0000CD  1415 G$PINA_5$0$0 == 0x00cd
                           0000CD  1416 _PINA_5	=	0x00cd
                           0000CE  1417 G$PINA_6$0$0 == 0x00ce
                           0000CE  1418 _PINA_6	=	0x00ce
                           0000CF  1419 G$PINA_7$0$0 == 0x00cf
                           0000CF  1420 _PINA_7	=	0x00cf
                           0000E8  1421 G$PINB_0$0$0 == 0x00e8
                           0000E8  1422 _PINB_0	=	0x00e8
                           0000E9  1423 G$PINB_1$0$0 == 0x00e9
                           0000E9  1424 _PINB_1	=	0x00e9
                           0000EA  1425 G$PINB_2$0$0 == 0x00ea
                           0000EA  1426 _PINB_2	=	0x00ea
                           0000EB  1427 G$PINB_3$0$0 == 0x00eb
                           0000EB  1428 _PINB_3	=	0x00eb
                           0000EC  1429 G$PINB_4$0$0 == 0x00ec
                           0000EC  1430 _PINB_4	=	0x00ec
                           0000ED  1431 G$PINB_5$0$0 == 0x00ed
                           0000ED  1432 _PINB_5	=	0x00ed
                           0000EE  1433 G$PINB_6$0$0 == 0x00ee
                           0000EE  1434 _PINB_6	=	0x00ee
                           0000EF  1435 G$PINB_7$0$0 == 0x00ef
                           0000EF  1436 _PINB_7	=	0x00ef
                           0000F8  1437 G$PINC_0$0$0 == 0x00f8
                           0000F8  1438 _PINC_0	=	0x00f8
                           0000F9  1439 G$PINC_1$0$0 == 0x00f9
                           0000F9  1440 _PINC_1	=	0x00f9
                           0000FA  1441 G$PINC_2$0$0 == 0x00fa
                           0000FA  1442 _PINC_2	=	0x00fa
                           0000FB  1443 G$PINC_3$0$0 == 0x00fb
                           0000FB  1444 _PINC_3	=	0x00fb
                           0000FC  1445 G$PINC_4$0$0 == 0x00fc
                           0000FC  1446 _PINC_4	=	0x00fc
                           0000FD  1447 G$PINC_5$0$0 == 0x00fd
                           0000FD  1448 _PINC_5	=	0x00fd
                           0000FE  1449 G$PINC_6$0$0 == 0x00fe
                           0000FE  1450 _PINC_6	=	0x00fe
                           0000FF  1451 G$PINC_7$0$0 == 0x00ff
                           0000FF  1452 _PINC_7	=	0x00ff
                           000080  1453 G$PORTA_0$0$0 == 0x0080
                           000080  1454 _PORTA_0	=	0x0080
                           000081  1455 G$PORTA_1$0$0 == 0x0081
                           000081  1456 _PORTA_1	=	0x0081
                           000082  1457 G$PORTA_2$0$0 == 0x0082
                           000082  1458 _PORTA_2	=	0x0082
                           000083  1459 G$PORTA_3$0$0 == 0x0083
                           000083  1460 _PORTA_3	=	0x0083
                           000084  1461 G$PORTA_4$0$0 == 0x0084
                           000084  1462 _PORTA_4	=	0x0084
                           000085  1463 G$PORTA_5$0$0 == 0x0085
                           000085  1464 _PORTA_5	=	0x0085
                           000086  1465 G$PORTA_6$0$0 == 0x0086
                           000086  1466 _PORTA_6	=	0x0086
                           000087  1467 G$PORTA_7$0$0 == 0x0087
                           000087  1468 _PORTA_7	=	0x0087
                           000088  1469 G$PORTB_0$0$0 == 0x0088
                           000088  1470 _PORTB_0	=	0x0088
                           000089  1471 G$PORTB_1$0$0 == 0x0089
                           000089  1472 _PORTB_1	=	0x0089
                           00008A  1473 G$PORTB_2$0$0 == 0x008a
                           00008A  1474 _PORTB_2	=	0x008a
                           00008B  1475 G$PORTB_3$0$0 == 0x008b
                           00008B  1476 _PORTB_3	=	0x008b
                           00008C  1477 G$PORTB_4$0$0 == 0x008c
                           00008C  1478 _PORTB_4	=	0x008c
                           00008D  1479 G$PORTB_5$0$0 == 0x008d
                           00008D  1480 _PORTB_5	=	0x008d
                           00008E  1481 G$PORTB_6$0$0 == 0x008e
                           00008E  1482 _PORTB_6	=	0x008e
                           00008F  1483 G$PORTB_7$0$0 == 0x008f
                           00008F  1484 _PORTB_7	=	0x008f
                           000090  1485 G$PORTC_0$0$0 == 0x0090
                           000090  1486 _PORTC_0	=	0x0090
                           000091  1487 G$PORTC_1$0$0 == 0x0091
                           000091  1488 _PORTC_1	=	0x0091
                           000092  1489 G$PORTC_2$0$0 == 0x0092
                           000092  1490 _PORTC_2	=	0x0092
                           000093  1491 G$PORTC_3$0$0 == 0x0093
                           000093  1492 _PORTC_3	=	0x0093
                           000094  1493 G$PORTC_4$0$0 == 0x0094
                           000094  1494 _PORTC_4	=	0x0094
                           000095  1495 G$PORTC_5$0$0 == 0x0095
                           000095  1496 _PORTC_5	=	0x0095
                           000096  1497 G$PORTC_6$0$0 == 0x0096
                           000096  1498 _PORTC_6	=	0x0096
                           000097  1499 G$PORTC_7$0$0 == 0x0097
                           000097  1500 _PORTC_7	=	0x0097
                                   1501 ;--------------------------------------------------------
                                   1502 ; overlayable register banks
                                   1503 ;--------------------------------------------------------
                                   1504 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1505 	.ds 8
                                   1506 ;--------------------------------------------------------
                                   1507 ; internal ram data
                                   1508 ;--------------------------------------------------------
                                   1509 	.area DSEG    (DATA)
                           000000  1510 Ltransmit3copies.transmit_copies_wtimer$sloc0$1$0==.
      000034                       1511 _transmit_copies_wtimer_sloc0_1_0:
      000034                       1512 	.ds 4
                                   1513 ;--------------------------------------------------------
                                   1514 ; overlayable items in internal ram 
                                   1515 ;--------------------------------------------------------
                                   1516 ;--------------------------------------------------------
                                   1517 ; indirectly addressable internal ram data
                                   1518 ;--------------------------------------------------------
                                   1519 	.area ISEG    (DATA)
                                   1520 ;--------------------------------------------------------
                                   1521 ; absolute internal ram data
                                   1522 ;--------------------------------------------------------
                                   1523 	.area IABS    (ABS,DATA)
                                   1524 	.area IABS    (ABS,DATA)
                                   1525 ;--------------------------------------------------------
                                   1526 ; bit data
                                   1527 ;--------------------------------------------------------
                                   1528 	.area BSEG    (BIT)
                                   1529 ;--------------------------------------------------------
                                   1530 ; paged external ram data
                                   1531 ;--------------------------------------------------------
                                   1532 	.area PSEG    (PAG,XDATA)
                                   1533 ;--------------------------------------------------------
                                   1534 ; external ram data
                                   1535 ;--------------------------------------------------------
                                   1536 	.area XSEG    (XDATA)
                           007020  1537 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1538 _ADCCH0VAL0	=	0x7020
                           007021  1539 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1540 _ADCCH0VAL1	=	0x7021
                           007020  1541 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1542 _ADCCH0VAL	=	0x7020
                           007022  1543 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1544 _ADCCH1VAL0	=	0x7022
                           007023  1545 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1546 _ADCCH1VAL1	=	0x7023
                           007022  1547 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1548 _ADCCH1VAL	=	0x7022
                           007024  1549 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1550 _ADCCH2VAL0	=	0x7024
                           007025  1551 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1552 _ADCCH2VAL1	=	0x7025
                           007024  1553 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1554 _ADCCH2VAL	=	0x7024
                           007026  1555 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1556 _ADCCH3VAL0	=	0x7026
                           007027  1557 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1558 _ADCCH3VAL1	=	0x7027
                           007026  1559 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1560 _ADCCH3VAL	=	0x7026
                           007028  1561 G$ADCTUNE0$0$0 == 0x7028
                           007028  1562 _ADCTUNE0	=	0x7028
                           007029  1563 G$ADCTUNE1$0$0 == 0x7029
                           007029  1564 _ADCTUNE1	=	0x7029
                           00702A  1565 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1566 _ADCTUNE2	=	0x702a
                           007010  1567 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1568 _DMA0ADDR0	=	0x7010
                           007011  1569 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1570 _DMA0ADDR1	=	0x7011
                           007010  1571 G$DMA0ADDR$0$0 == 0x7010
                           007010  1572 _DMA0ADDR	=	0x7010
                           007014  1573 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1574 _DMA0CONFIG	=	0x7014
                           007012  1575 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1576 _DMA1ADDR0	=	0x7012
                           007013  1577 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1578 _DMA1ADDR1	=	0x7013
                           007012  1579 G$DMA1ADDR$0$0 == 0x7012
                           007012  1580 _DMA1ADDR	=	0x7012
                           007015  1581 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1582 _DMA1CONFIG	=	0x7015
                           007070  1583 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1584 _FRCOSCCONFIG	=	0x7070
                           007071  1585 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1586 _FRCOSCCTRL	=	0x7071
                           007076  1587 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1588 _FRCOSCFREQ0	=	0x7076
                           007077  1589 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1590 _FRCOSCFREQ1	=	0x7077
                           007076  1591 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1592 _FRCOSCFREQ	=	0x7076
                           007072  1593 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1594 _FRCOSCKFILT0	=	0x7072
                           007073  1595 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1596 _FRCOSCKFILT1	=	0x7073
                           007072  1597 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1598 _FRCOSCKFILT	=	0x7072
                           007078  1599 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1600 _FRCOSCPER0	=	0x7078
                           007079  1601 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1602 _FRCOSCPER1	=	0x7079
                           007078  1603 G$FRCOSCPER$0$0 == 0x7078
                           007078  1604 _FRCOSCPER	=	0x7078
                           007074  1605 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1606 _FRCOSCREF0	=	0x7074
                           007075  1607 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1608 _FRCOSCREF1	=	0x7075
                           007074  1609 G$FRCOSCREF$0$0 == 0x7074
                           007074  1610 _FRCOSCREF	=	0x7074
                           007007  1611 G$ANALOGA$0$0 == 0x7007
                           007007  1612 _ANALOGA	=	0x7007
                           00700C  1613 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1614 _GPIOENABLE	=	0x700c
                           007003  1615 G$EXTIRQ$0$0 == 0x7003
                           007003  1616 _EXTIRQ	=	0x7003
                           007000  1617 G$INTCHGA$0$0 == 0x7000
                           007000  1618 _INTCHGA	=	0x7000
                           007001  1619 G$INTCHGB$0$0 == 0x7001
                           007001  1620 _INTCHGB	=	0x7001
                           007002  1621 G$INTCHGC$0$0 == 0x7002
                           007002  1622 _INTCHGC	=	0x7002
                           007008  1623 G$PALTA$0$0 == 0x7008
                           007008  1624 _PALTA	=	0x7008
                           007009  1625 G$PALTB$0$0 == 0x7009
                           007009  1626 _PALTB	=	0x7009
                           00700A  1627 G$PALTC$0$0 == 0x700a
                           00700A  1628 _PALTC	=	0x700a
                           007046  1629 G$PALTRADIO$0$0 == 0x7046
                           007046  1630 _PALTRADIO	=	0x7046
                           007004  1631 G$PINCHGA$0$0 == 0x7004
                           007004  1632 _PINCHGA	=	0x7004
                           007005  1633 G$PINCHGB$0$0 == 0x7005
                           007005  1634 _PINCHGB	=	0x7005
                           007006  1635 G$PINCHGC$0$0 == 0x7006
                           007006  1636 _PINCHGC	=	0x7006
                           00700B  1637 G$PINSEL$0$0 == 0x700b
                           00700B  1638 _PINSEL	=	0x700b
                           007060  1639 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1640 _LPOSCCONFIG	=	0x7060
                           007066  1641 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1642 _LPOSCFREQ0	=	0x7066
                           007067  1643 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1644 _LPOSCFREQ1	=	0x7067
                           007066  1645 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1646 _LPOSCFREQ	=	0x7066
                           007062  1647 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1648 _LPOSCKFILT0	=	0x7062
                           007063  1649 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1650 _LPOSCKFILT1	=	0x7063
                           007062  1651 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1652 _LPOSCKFILT	=	0x7062
                           007068  1653 G$LPOSCPER0$0$0 == 0x7068
                           007068  1654 _LPOSCPER0	=	0x7068
                           007069  1655 G$LPOSCPER1$0$0 == 0x7069
                           007069  1656 _LPOSCPER1	=	0x7069
                           007068  1657 G$LPOSCPER$0$0 == 0x7068
                           007068  1658 _LPOSCPER	=	0x7068
                           007064  1659 G$LPOSCREF0$0$0 == 0x7064
                           007064  1660 _LPOSCREF0	=	0x7064
                           007065  1661 G$LPOSCREF1$0$0 == 0x7065
                           007065  1662 _LPOSCREF1	=	0x7065
                           007064  1663 G$LPOSCREF$0$0 == 0x7064
                           007064  1664 _LPOSCREF	=	0x7064
                           007054  1665 G$LPXOSCGM$0$0 == 0x7054
                           007054  1666 _LPXOSCGM	=	0x7054
                           007F01  1667 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1668 _MISCCTRL	=	0x7f01
                           007053  1669 G$OSCCALIB$0$0 == 0x7053
                           007053  1670 _OSCCALIB	=	0x7053
                           007050  1671 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1672 _OSCFORCERUN	=	0x7050
                           007052  1673 G$OSCREADY$0$0 == 0x7052
                           007052  1674 _OSCREADY	=	0x7052
                           007051  1675 G$OSCRUN$0$0 == 0x7051
                           007051  1676 _OSCRUN	=	0x7051
                           007040  1677 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1678 _RADIOFDATAADDR0	=	0x7040
                           007041  1679 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1680 _RADIOFDATAADDR1	=	0x7041
                           007040  1681 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1682 _RADIOFDATAADDR	=	0x7040
                           007042  1683 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1684 _RADIOFSTATADDR0	=	0x7042
                           007043  1685 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1686 _RADIOFSTATADDR1	=	0x7043
                           007042  1687 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1688 _RADIOFSTATADDR	=	0x7042
                           007044  1689 G$RADIOMUX$0$0 == 0x7044
                           007044  1690 _RADIOMUX	=	0x7044
                           007084  1691 G$SCRATCH0$0$0 == 0x7084
                           007084  1692 _SCRATCH0	=	0x7084
                           007085  1693 G$SCRATCH1$0$0 == 0x7085
                           007085  1694 _SCRATCH1	=	0x7085
                           007086  1695 G$SCRATCH2$0$0 == 0x7086
                           007086  1696 _SCRATCH2	=	0x7086
                           007087  1697 G$SCRATCH3$0$0 == 0x7087
                           007087  1698 _SCRATCH3	=	0x7087
                           007F00  1699 G$SILICONREV$0$0 == 0x7f00
                           007F00  1700 _SILICONREV	=	0x7f00
                           007F19  1701 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1702 _XTALAMPL	=	0x7f19
                           007F18  1703 G$XTALOSC$0$0 == 0x7f18
                           007F18  1704 _XTALOSC	=	0x7f18
                           007F1A  1705 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1706 _XTALREADY	=	0x7f1a
                           00FC06  1707 Ftransmit3copies$flash_deviceid$0$0 == 0xfc06
                           00FC06  1708 _flash_deviceid	=	0xfc06
                           00FC00  1709 Ftransmit3copies$flash_calsector$0$0 == 0xfc00
                           00FC00  1710 _flash_calsector	=	0xfc00
                           004114  1711 G$AX5043_AFSKCTRL$0$0 == 0x4114
                           004114  1712 _AX5043_AFSKCTRL	=	0x4114
                           004113  1713 G$AX5043_AFSKMARK0$0$0 == 0x4113
                           004113  1714 _AX5043_AFSKMARK0	=	0x4113
                           004112  1715 G$AX5043_AFSKMARK1$0$0 == 0x4112
                           004112  1716 _AX5043_AFSKMARK1	=	0x4112
                           004111  1717 G$AX5043_AFSKSPACE0$0$0 == 0x4111
                           004111  1718 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1719 G$AX5043_AFSKSPACE1$0$0 == 0x4110
                           004110  1720 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1721 G$AX5043_AGCCOUNTER$0$0 == 0x4043
                           004043  1722 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1723 G$AX5043_AMPLFILTER$0$0 == 0x4115
                           004115  1724 _AX5043_AMPLFILTER	=	0x4115
                           004189  1725 G$AX5043_BBOFFSCAP$0$0 == 0x4189
                           004189  1726 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1727 G$AX5043_BBTUNE$0$0 == 0x4188
                           004188  1728 _AX5043_BBTUNE	=	0x4188
                           004041  1729 G$AX5043_BGNDRSSI$0$0 == 0x4041
                           004041  1730 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1731 G$AX5043_BGNDRSSIGAIN$0$0 == 0x422e
                           00422E  1732 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1733 G$AX5043_BGNDRSSITHR$0$0 == 0x422f
                           00422F  1734 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1735 G$AX5043_CRCINIT0$0$0 == 0x4017
                           004017  1736 _AX5043_CRCINIT0	=	0x4017
                           004016  1737 G$AX5043_CRCINIT1$0$0 == 0x4016
                           004016  1738 _AX5043_CRCINIT1	=	0x4016
                           004015  1739 G$AX5043_CRCINIT2$0$0 == 0x4015
                           004015  1740 _AX5043_CRCINIT2	=	0x4015
                           004014  1741 G$AX5043_CRCINIT3$0$0 == 0x4014
                           004014  1742 _AX5043_CRCINIT3	=	0x4014
                           004332  1743 G$AX5043_DACCONFIG$0$0 == 0x4332
                           004332  1744 _AX5043_DACCONFIG	=	0x4332
                           004331  1745 G$AX5043_DACVALUE0$0$0 == 0x4331
                           004331  1746 _AX5043_DACVALUE0	=	0x4331
                           004330  1747 G$AX5043_DACVALUE1$0$0 == 0x4330
                           004330  1748 _AX5043_DACVALUE1	=	0x4330
                           004102  1749 G$AX5043_DECIMATION$0$0 == 0x4102
                           004102  1750 _AX5043_DECIMATION	=	0x4102
                           004042  1751 G$AX5043_DIVERSITY$0$0 == 0x4042
                           004042  1752 _AX5043_DIVERSITY	=	0x4042
                           004011  1753 G$AX5043_ENCODING$0$0 == 0x4011
                           004011  1754 _AX5043_ENCODING	=	0x4011
                           004018  1755 G$AX5043_FEC$0$0 == 0x4018
                           004018  1756 _AX5043_FEC	=	0x4018
                           00401A  1757 G$AX5043_FECSTATUS$0$0 == 0x401a
                           00401A  1758 _AX5043_FECSTATUS	=	0x401a
                           004019  1759 G$AX5043_FECSYNC$0$0 == 0x4019
                           004019  1760 _AX5043_FECSYNC	=	0x4019
                           00402B  1761 G$AX5043_FIFOCOUNT0$0$0 == 0x402b
                           00402B  1762 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1763 G$AX5043_FIFOCOUNT1$0$0 == 0x402a
                           00402A  1764 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1765 G$AX5043_FIFODATA$0$0 == 0x4029
                           004029  1766 _AX5043_FIFODATA	=	0x4029
                           00402D  1767 G$AX5043_FIFOFREE0$0$0 == 0x402d
                           00402D  1768 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1769 G$AX5043_FIFOFREE1$0$0 == 0x402c
                           00402C  1770 _AX5043_FIFOFREE1	=	0x402c
                           004028  1771 G$AX5043_FIFOSTAT$0$0 == 0x4028
                           004028  1772 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1773 G$AX5043_FIFOTHRESH0$0$0 == 0x402f
                           00402F  1774 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1775 G$AX5043_FIFOTHRESH1$0$0 == 0x402e
                           00402E  1776 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1777 G$AX5043_FRAMING$0$0 == 0x4012
                           004012  1778 _AX5043_FRAMING	=	0x4012
                           004037  1779 G$AX5043_FREQA0$0$0 == 0x4037
                           004037  1780 _AX5043_FREQA0	=	0x4037
                           004036  1781 G$AX5043_FREQA1$0$0 == 0x4036
                           004036  1782 _AX5043_FREQA1	=	0x4036
                           004035  1783 G$AX5043_FREQA2$0$0 == 0x4035
                           004035  1784 _AX5043_FREQA2	=	0x4035
                           004034  1785 G$AX5043_FREQA3$0$0 == 0x4034
                           004034  1786 _AX5043_FREQA3	=	0x4034
                           00403F  1787 G$AX5043_FREQB0$0$0 == 0x403f
                           00403F  1788 _AX5043_FREQB0	=	0x403f
                           00403E  1789 G$AX5043_FREQB1$0$0 == 0x403e
                           00403E  1790 _AX5043_FREQB1	=	0x403e
                           00403D  1791 G$AX5043_FREQB2$0$0 == 0x403d
                           00403D  1792 _AX5043_FREQB2	=	0x403d
                           00403C  1793 G$AX5043_FREQB3$0$0 == 0x403c
                           00403C  1794 _AX5043_FREQB3	=	0x403c
                           004163  1795 G$AX5043_FSKDEV0$0$0 == 0x4163
                           004163  1796 _AX5043_FSKDEV0	=	0x4163
                           004162  1797 G$AX5043_FSKDEV1$0$0 == 0x4162
                           004162  1798 _AX5043_FSKDEV1	=	0x4162
                           004161  1799 G$AX5043_FSKDEV2$0$0 == 0x4161
                           004161  1800 _AX5043_FSKDEV2	=	0x4161
                           00410D  1801 G$AX5043_FSKDMAX0$0$0 == 0x410d
                           00410D  1802 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1803 G$AX5043_FSKDMAX1$0$0 == 0x410c
                           00410C  1804 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1805 G$AX5043_FSKDMIN0$0$0 == 0x410f
                           00410F  1806 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1807 G$AX5043_FSKDMIN1$0$0 == 0x410e
                           00410E  1808 _AX5043_FSKDMIN1	=	0x410e
                           004309  1809 G$AX5043_GPADC13VALUE0$0$0 == 0x4309
                           004309  1810 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1811 G$AX5043_GPADC13VALUE1$0$0 == 0x4308
                           004308  1812 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1813 G$AX5043_GPADCCTRL$0$0 == 0x4300
                           004300  1814 _AX5043_GPADCCTRL	=	0x4300
                           004301  1815 G$AX5043_GPADCPERIOD$0$0 == 0x4301
                           004301  1816 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1817 G$AX5043_IFFREQ0$0$0 == 0x4101
                           004101  1818 _AX5043_IFFREQ0	=	0x4101
                           004100  1819 G$AX5043_IFFREQ1$0$0 == 0x4100
                           004100  1820 _AX5043_IFFREQ1	=	0x4100
                           00400B  1821 G$AX5043_IRQINVERSION0$0$0 == 0x400b
                           00400B  1822 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1823 G$AX5043_IRQINVERSION1$0$0 == 0x400a
                           00400A  1824 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1825 G$AX5043_IRQMASK0$0$0 == 0x4007
                           004007  1826 _AX5043_IRQMASK0	=	0x4007
                           004006  1827 G$AX5043_IRQMASK1$0$0 == 0x4006
                           004006  1828 _AX5043_IRQMASK1	=	0x4006
                           00400D  1829 G$AX5043_IRQREQUEST0$0$0 == 0x400d
                           00400D  1830 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1831 G$AX5043_IRQREQUEST1$0$0 == 0x400c
                           00400C  1832 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1833 G$AX5043_LPOSCCONFIG$0$0 == 0x4310
                           004310  1834 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1835 G$AX5043_LPOSCFREQ0$0$0 == 0x4317
                           004317  1836 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1837 G$AX5043_LPOSCFREQ1$0$0 == 0x4316
                           004316  1838 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1839 G$AX5043_LPOSCKFILT0$0$0 == 0x4313
                           004313  1840 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1841 G$AX5043_LPOSCKFILT1$0$0 == 0x4312
                           004312  1842 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1843 G$AX5043_LPOSCPER0$0$0 == 0x4319
                           004319  1844 _AX5043_LPOSCPER0	=	0x4319
                           004318  1845 G$AX5043_LPOSCPER1$0$0 == 0x4318
                           004318  1846 _AX5043_LPOSCPER1	=	0x4318
                           004315  1847 G$AX5043_LPOSCREF0$0$0 == 0x4315
                           004315  1848 _AX5043_LPOSCREF0	=	0x4315
                           004314  1849 G$AX5043_LPOSCREF1$0$0 == 0x4314
                           004314  1850 _AX5043_LPOSCREF1	=	0x4314
                           004311  1851 G$AX5043_LPOSCSTATUS$0$0 == 0x4311
                           004311  1852 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1853 G$AX5043_MATCH0LEN$0$0 == 0x4214
                           004214  1854 _AX5043_MATCH0LEN	=	0x4214
                           004216  1855 G$AX5043_MATCH0MAX$0$0 == 0x4216
                           004216  1856 _AX5043_MATCH0MAX	=	0x4216
                           004215  1857 G$AX5043_MATCH0MIN$0$0 == 0x4215
                           004215  1858 _AX5043_MATCH0MIN	=	0x4215
                           004213  1859 G$AX5043_MATCH0PAT0$0$0 == 0x4213
                           004213  1860 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1861 G$AX5043_MATCH0PAT1$0$0 == 0x4212
                           004212  1862 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1863 G$AX5043_MATCH0PAT2$0$0 == 0x4211
                           004211  1864 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1865 G$AX5043_MATCH0PAT3$0$0 == 0x4210
                           004210  1866 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1867 G$AX5043_MATCH1LEN$0$0 == 0x421c
                           00421C  1868 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1869 G$AX5043_MATCH1MAX$0$0 == 0x421e
                           00421E  1870 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1871 G$AX5043_MATCH1MIN$0$0 == 0x421d
                           00421D  1872 _AX5043_MATCH1MIN	=	0x421d
                           004219  1873 G$AX5043_MATCH1PAT0$0$0 == 0x4219
                           004219  1874 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1875 G$AX5043_MATCH1PAT1$0$0 == 0x4218
                           004218  1876 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1877 G$AX5043_MAXDROFFSET0$0$0 == 0x4108
                           004108  1878 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1879 G$AX5043_MAXDROFFSET1$0$0 == 0x4107
                           004107  1880 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1881 G$AX5043_MAXDROFFSET2$0$0 == 0x4106
                           004106  1882 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1883 G$AX5043_MAXRFOFFSET0$0$0 == 0x410b
                           00410B  1884 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1885 G$AX5043_MAXRFOFFSET1$0$0 == 0x410a
                           00410A  1886 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1887 G$AX5043_MAXRFOFFSET2$0$0 == 0x4109
                           004109  1888 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1889 G$AX5043_MODCFGA$0$0 == 0x4164
                           004164  1890 _AX5043_MODCFGA	=	0x4164
                           004160  1891 G$AX5043_MODCFGF$0$0 == 0x4160
                           004160  1892 _AX5043_MODCFGF	=	0x4160
                           004F5F  1893 G$AX5043_MODCFGP$0$0 == 0x4f5f
                           004F5F  1894 _AX5043_MODCFGP	=	0x4f5f
                           004010  1895 G$AX5043_MODULATION$0$0 == 0x4010
                           004010  1896 _AX5043_MODULATION	=	0x4010
                           004025  1897 G$AX5043_PINFUNCANTSEL$0$0 == 0x4025
                           004025  1898 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1899 G$AX5043_PINFUNCDATA$0$0 == 0x4023
                           004023  1900 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1901 G$AX5043_PINFUNCDCLK$0$0 == 0x4022
                           004022  1902 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1903 G$AX5043_PINFUNCIRQ$0$0 == 0x4024
                           004024  1904 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1905 G$AX5043_PINFUNCPWRAMP$0$0 == 0x4026
                           004026  1906 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1907 G$AX5043_PINFUNCSYSCLK$0$0 == 0x4021
                           004021  1908 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1909 G$AX5043_PINSTATE$0$0 == 0x4020
                           004020  1910 _AX5043_PINSTATE	=	0x4020
                           004233  1911 G$AX5043_PKTACCEPTFLAGS$0$0 == 0x4233
                           004233  1912 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1913 G$AX5043_PKTCHUNKSIZE$0$0 == 0x4230
                           004230  1914 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1915 G$AX5043_PKTMISCFLAGS$0$0 == 0x4231
                           004231  1916 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1917 G$AX5043_PKTSTOREFLAGS$0$0 == 0x4232
                           004232  1918 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1919 G$AX5043_PLLCPI$0$0 == 0x4031
                           004031  1920 _AX5043_PLLCPI	=	0x4031
                           004039  1921 G$AX5043_PLLCPIBOOST$0$0 == 0x4039
                           004039  1922 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1923 G$AX5043_PLLLOCKDET$0$0 == 0x4182
                           004182  1924 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1925 G$AX5043_PLLLOOP$0$0 == 0x4030
                           004030  1926 _AX5043_PLLLOOP	=	0x4030
                           004038  1927 G$AX5043_PLLLOOPBOOST$0$0 == 0x4038
                           004038  1928 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1929 G$AX5043_PLLRANGINGA$0$0 == 0x4033
                           004033  1930 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1931 G$AX5043_PLLRANGINGB$0$0 == 0x403b
                           00403B  1932 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1933 G$AX5043_PLLRNGCLK$0$0 == 0x4183
                           004183  1934 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1935 G$AX5043_PLLVCODIV$0$0 == 0x4032
                           004032  1936 _AX5043_PLLVCODIV	=	0x4032
                           004180  1937 G$AX5043_PLLVCOI$0$0 == 0x4180
                           004180  1938 _AX5043_PLLVCOI	=	0x4180
                           004181  1939 G$AX5043_PLLVCOIR$0$0 == 0x4181
                           004181  1940 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1941 G$AX5043_POWCTRL1$0$0 == 0x4f08
                           004F08  1942 _AX5043_POWCTRL1	=	0x4f08
                           004005  1943 G$AX5043_POWIRQMASK$0$0 == 0x4005
                           004005  1944 _AX5043_POWIRQMASK	=	0x4005
                           004003  1945 G$AX5043_POWSTAT$0$0 == 0x4003
                           004003  1946 _AX5043_POWSTAT	=	0x4003
                           004004  1947 G$AX5043_POWSTICKYSTAT$0$0 == 0x4004
                           004004  1948 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1949 G$AX5043_PWRAMP$0$0 == 0x4027
                           004027  1950 _AX5043_PWRAMP	=	0x4027
                           004002  1951 G$AX5043_PWRMODE$0$0 == 0x4002
                           004002  1952 _AX5043_PWRMODE	=	0x4002
                           004009  1953 G$AX5043_RADIOEVENTMASK0$0$0 == 0x4009
                           004009  1954 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1955 G$AX5043_RADIOEVENTMASK1$0$0 == 0x4008
                           004008  1956 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1957 G$AX5043_RADIOEVENTREQ0$0$0 == 0x400f
                           00400F  1958 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1959 G$AX5043_RADIOEVENTREQ1$0$0 == 0x400e
                           00400E  1960 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1961 G$AX5043_RADIOSTATE$0$0 == 0x401c
                           00401C  1962 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1963 G$AX5043_REF$0$0 == 0x4f0d
                           004F0D  1964 _AX5043_REF	=	0x4f0d
                           004040  1965 G$AX5043_RSSI$0$0 == 0x4040
                           004040  1966 _AX5043_RSSI	=	0x4040
                           00422D  1967 G$AX5043_RSSIABSTHR$0$0 == 0x422d
                           00422D  1968 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1969 G$AX5043_RSSIREFERENCE$0$0 == 0x422c
                           00422C  1970 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1971 G$AX5043_RXDATARATE0$0$0 == 0x4105
                           004105  1972 _AX5043_RXDATARATE0	=	0x4105
                           004104  1973 G$AX5043_RXDATARATE1$0$0 == 0x4104
                           004104  1974 _AX5043_RXDATARATE1	=	0x4104
                           004103  1975 G$AX5043_RXDATARATE2$0$0 == 0x4103
                           004103  1976 _AX5043_RXDATARATE2	=	0x4103
                           004001  1977 G$AX5043_SCRATCH$0$0 == 0x4001
                           004001  1978 _AX5043_SCRATCH	=	0x4001
                           004000  1979 G$AX5043_SILICONREVISION$0$0 == 0x4000
                           004000  1980 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1981 G$AX5043_TIMER0$0$0 == 0x405b
                           00405B  1982 _AX5043_TIMER0	=	0x405b
                           00405A  1983 G$AX5043_TIMER1$0$0 == 0x405a
                           00405A  1984 _AX5043_TIMER1	=	0x405a
                           004059  1985 G$AX5043_TIMER2$0$0 == 0x4059
                           004059  1986 _AX5043_TIMER2	=	0x4059
                           004227  1987 G$AX5043_TMGRXAGC$0$0 == 0x4227
                           004227  1988 _AX5043_TMGRXAGC	=	0x4227
                           004223  1989 G$AX5043_TMGRXBOOST$0$0 == 0x4223
                           004223  1990 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1991 G$AX5043_TMGRXCOARSEAGC$0$0 == 0x4226
                           004226  1992 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1993 G$AX5043_TMGRXOFFSACQ$0$0 == 0x4225
                           004225  1994 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1995 G$AX5043_TMGRXPREAMBLE1$0$0 == 0x4229
                           004229  1996 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1997 G$AX5043_TMGRXPREAMBLE2$0$0 == 0x422a
                           00422A  1998 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1999 G$AX5043_TMGRXPREAMBLE3$0$0 == 0x422b
                           00422B  2000 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  2001 G$AX5043_TMGRXRSSI$0$0 == 0x4228
                           004228  2002 _AX5043_TMGRXRSSI	=	0x4228
                           004224  2003 G$AX5043_TMGRXSETTLE$0$0 == 0x4224
                           004224  2004 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  2005 G$AX5043_TMGTXBOOST$0$0 == 0x4220
                           004220  2006 _AX5043_TMGTXBOOST	=	0x4220
                           004221  2007 G$AX5043_TMGTXSETTLE$0$0 == 0x4221
                           004221  2008 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  2009 G$AX5043_TRKAFSKDEMOD0$0$0 == 0x4055
                           004055  2010 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  2011 G$AX5043_TRKAFSKDEMOD1$0$0 == 0x4054
                           004054  2012 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  2013 G$AX5043_TRKAMPLITUDE0$0$0 == 0x4049
                           004049  2014 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  2015 G$AX5043_TRKAMPLITUDE1$0$0 == 0x4048
                           004048  2016 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  2017 G$AX5043_TRKDATARATE0$0$0 == 0x4047
                           004047  2018 _AX5043_TRKDATARATE0	=	0x4047
                           004046  2019 G$AX5043_TRKDATARATE1$0$0 == 0x4046
                           004046  2020 _AX5043_TRKDATARATE1	=	0x4046
                           004045  2021 G$AX5043_TRKDATARATE2$0$0 == 0x4045
                           004045  2022 _AX5043_TRKDATARATE2	=	0x4045
                           004051  2023 G$AX5043_TRKFREQ0$0$0 == 0x4051
                           004051  2024 _AX5043_TRKFREQ0	=	0x4051
                           004050  2025 G$AX5043_TRKFREQ1$0$0 == 0x4050
                           004050  2026 _AX5043_TRKFREQ1	=	0x4050
                           004053  2027 G$AX5043_TRKFSKDEMOD0$0$0 == 0x4053
                           004053  2028 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  2029 G$AX5043_TRKFSKDEMOD1$0$0 == 0x4052
                           004052  2030 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  2031 G$AX5043_TRKPHASE0$0$0 == 0x404b
                           00404B  2032 _AX5043_TRKPHASE0	=	0x404b
                           00404A  2033 G$AX5043_TRKPHASE1$0$0 == 0x404a
                           00404A  2034 _AX5043_TRKPHASE1	=	0x404a
                           00404F  2035 G$AX5043_TRKRFFREQ0$0$0 == 0x404f
                           00404F  2036 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  2037 G$AX5043_TRKRFFREQ1$0$0 == 0x404e
                           00404E  2038 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  2039 G$AX5043_TRKRFFREQ2$0$0 == 0x404d
                           00404D  2040 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  2041 G$AX5043_TXPWRCOEFFA0$0$0 == 0x4169
                           004169  2042 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  2043 G$AX5043_TXPWRCOEFFA1$0$0 == 0x4168
                           004168  2044 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  2045 G$AX5043_TXPWRCOEFFB0$0$0 == 0x416b
                           00416B  2046 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  2047 G$AX5043_TXPWRCOEFFB1$0$0 == 0x416a
                           00416A  2048 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  2049 G$AX5043_TXPWRCOEFFC0$0$0 == 0x416d
                           00416D  2050 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  2051 G$AX5043_TXPWRCOEFFC1$0$0 == 0x416c
                           00416C  2052 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  2053 G$AX5043_TXPWRCOEFFD0$0$0 == 0x416f
                           00416F  2054 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  2055 G$AX5043_TXPWRCOEFFD1$0$0 == 0x416e
                           00416E  2056 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  2057 G$AX5043_TXPWRCOEFFE0$0$0 == 0x4171
                           004171  2058 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  2059 G$AX5043_TXPWRCOEFFE1$0$0 == 0x4170
                           004170  2060 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  2061 G$AX5043_TXRATE0$0$0 == 0x4167
                           004167  2062 _AX5043_TXRATE0	=	0x4167
                           004166  2063 G$AX5043_TXRATE1$0$0 == 0x4166
                           004166  2064 _AX5043_TXRATE1	=	0x4166
                           004165  2065 G$AX5043_TXRATE2$0$0 == 0x4165
                           004165  2066 _AX5043_TXRATE2	=	0x4165
                           00406B  2067 G$AX5043_WAKEUP0$0$0 == 0x406b
                           00406B  2068 _AX5043_WAKEUP0	=	0x406b
                           00406A  2069 G$AX5043_WAKEUP1$0$0 == 0x406a
                           00406A  2070 _AX5043_WAKEUP1	=	0x406a
                           00406D  2071 G$AX5043_WAKEUPFREQ0$0$0 == 0x406d
                           00406D  2072 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  2073 G$AX5043_WAKEUPFREQ1$0$0 == 0x406c
                           00406C  2074 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  2075 G$AX5043_WAKEUPTIMER0$0$0 == 0x4069
                           004069  2076 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  2077 G$AX5043_WAKEUPTIMER1$0$0 == 0x4068
                           004068  2078 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  2079 G$AX5043_WAKEUPXOEARLY$0$0 == 0x406e
                           00406E  2080 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  2081 G$AX5043_XTALAMPL$0$0 == 0x4f11
                           004F11  2082 _AX5043_XTALAMPL	=	0x4f11
                           004184  2083 G$AX5043_XTALCAP$0$0 == 0x4184
                           004184  2084 _AX5043_XTALCAP	=	0x4184
                           004F10  2085 G$AX5043_XTALOSC$0$0 == 0x4f10
                           004F10  2086 _AX5043_XTALOSC	=	0x4f10
                           00401D  2087 G$AX5043_XTALSTATUS$0$0 == 0x401d
                           00401D  2088 _AX5043_XTALSTATUS	=	0x401d
                           004F00  2089 G$AX5043_0xF00$0$0 == 0x4f00
                           004F00  2090 _AX5043_0xF00	=	0x4f00
                           004F0C  2091 G$AX5043_0xF0C$0$0 == 0x4f0c
                           004F0C  2092 _AX5043_0xF0C	=	0x4f0c
                           004F18  2093 G$AX5043_0xF18$0$0 == 0x4f18
                           004F18  2094 _AX5043_0xF18	=	0x4f18
                           004F1C  2095 G$AX5043_0xF1C$0$0 == 0x4f1c
                           004F1C  2096 _AX5043_0xF1C	=	0x4f1c
                           004F21  2097 G$AX5043_0xF21$0$0 == 0x4f21
                           004F21  2098 _AX5043_0xF21	=	0x4f21
                           004F22  2099 G$AX5043_0xF22$0$0 == 0x4f22
                           004F22  2100 _AX5043_0xF22	=	0x4f22
                           004F23  2101 G$AX5043_0xF23$0$0 == 0x4f23
                           004F23  2102 _AX5043_0xF23	=	0x4f23
                           004F26  2103 G$AX5043_0xF26$0$0 == 0x4f26
                           004F26  2104 _AX5043_0xF26	=	0x4f26
                           004F30  2105 G$AX5043_0xF30$0$0 == 0x4f30
                           004F30  2106 _AX5043_0xF30	=	0x4f30
                           004F31  2107 G$AX5043_0xF31$0$0 == 0x4f31
                           004F31  2108 _AX5043_0xF31	=	0x4f31
                           004F32  2109 G$AX5043_0xF32$0$0 == 0x4f32
                           004F32  2110 _AX5043_0xF32	=	0x4f32
                           004F33  2111 G$AX5043_0xF33$0$0 == 0x4f33
                           004F33  2112 _AX5043_0xF33	=	0x4f33
                           004F34  2113 G$AX5043_0xF34$0$0 == 0x4f34
                           004F34  2114 _AX5043_0xF34	=	0x4f34
                           004F35  2115 G$AX5043_0xF35$0$0 == 0x4f35
                           004F35  2116 _AX5043_0xF35	=	0x4f35
                           004F44  2117 G$AX5043_0xF44$0$0 == 0x4f44
                           004F44  2118 _AX5043_0xF44	=	0x4f44
                           004122  2119 G$AX5043_AGCAHYST0$0$0 == 0x4122
                           004122  2120 _AX5043_AGCAHYST0	=	0x4122
                           004132  2121 G$AX5043_AGCAHYST1$0$0 == 0x4132
                           004132  2122 _AX5043_AGCAHYST1	=	0x4132
                           004142  2123 G$AX5043_AGCAHYST2$0$0 == 0x4142
                           004142  2124 _AX5043_AGCAHYST2	=	0x4142
                           004152  2125 G$AX5043_AGCAHYST3$0$0 == 0x4152
                           004152  2126 _AX5043_AGCAHYST3	=	0x4152
                           004120  2127 G$AX5043_AGCGAIN0$0$0 == 0x4120
                           004120  2128 _AX5043_AGCGAIN0	=	0x4120
                           004130  2129 G$AX5043_AGCGAIN1$0$0 == 0x4130
                           004130  2130 _AX5043_AGCGAIN1	=	0x4130
                           004140  2131 G$AX5043_AGCGAIN2$0$0 == 0x4140
                           004140  2132 _AX5043_AGCGAIN2	=	0x4140
                           004150  2133 G$AX5043_AGCGAIN3$0$0 == 0x4150
                           004150  2134 _AX5043_AGCGAIN3	=	0x4150
                           004123  2135 G$AX5043_AGCMINMAX0$0$0 == 0x4123
                           004123  2136 _AX5043_AGCMINMAX0	=	0x4123
                           004133  2137 G$AX5043_AGCMINMAX1$0$0 == 0x4133
                           004133  2138 _AX5043_AGCMINMAX1	=	0x4133
                           004143  2139 G$AX5043_AGCMINMAX2$0$0 == 0x4143
                           004143  2140 _AX5043_AGCMINMAX2	=	0x4143
                           004153  2141 G$AX5043_AGCMINMAX3$0$0 == 0x4153
                           004153  2142 _AX5043_AGCMINMAX3	=	0x4153
                           004121  2143 G$AX5043_AGCTARGET0$0$0 == 0x4121
                           004121  2144 _AX5043_AGCTARGET0	=	0x4121
                           004131  2145 G$AX5043_AGCTARGET1$0$0 == 0x4131
                           004131  2146 _AX5043_AGCTARGET1	=	0x4131
                           004141  2147 G$AX5043_AGCTARGET2$0$0 == 0x4141
                           004141  2148 _AX5043_AGCTARGET2	=	0x4141
                           004151  2149 G$AX5043_AGCTARGET3$0$0 == 0x4151
                           004151  2150 _AX5043_AGCTARGET3	=	0x4151
                           00412B  2151 G$AX5043_AMPLITUDEGAIN0$0$0 == 0x412b
                           00412B  2152 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  2153 G$AX5043_AMPLITUDEGAIN1$0$0 == 0x413b
                           00413B  2154 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  2155 G$AX5043_AMPLITUDEGAIN2$0$0 == 0x414b
                           00414B  2156 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  2157 G$AX5043_AMPLITUDEGAIN3$0$0 == 0x415b
                           00415B  2158 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  2159 G$AX5043_BBOFFSRES0$0$0 == 0x412f
                           00412F  2160 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  2161 G$AX5043_BBOFFSRES1$0$0 == 0x413f
                           00413F  2162 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  2163 G$AX5043_BBOFFSRES2$0$0 == 0x414f
                           00414F  2164 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  2165 G$AX5043_BBOFFSRES3$0$0 == 0x415f
                           00415F  2166 _AX5043_BBOFFSRES3	=	0x415f
                           004125  2167 G$AX5043_DRGAIN0$0$0 == 0x4125
                           004125  2168 _AX5043_DRGAIN0	=	0x4125
                           004135  2169 G$AX5043_DRGAIN1$0$0 == 0x4135
                           004135  2170 _AX5043_DRGAIN1	=	0x4135
                           004145  2171 G$AX5043_DRGAIN2$0$0 == 0x4145
                           004145  2172 _AX5043_DRGAIN2	=	0x4145
                           004155  2173 G$AX5043_DRGAIN3$0$0 == 0x4155
                           004155  2174 _AX5043_DRGAIN3	=	0x4155
                           00412E  2175 G$AX5043_FOURFSK0$0$0 == 0x412e
                           00412E  2176 _AX5043_FOURFSK0	=	0x412e
                           00413E  2177 G$AX5043_FOURFSK1$0$0 == 0x413e
                           00413E  2178 _AX5043_FOURFSK1	=	0x413e
                           00414E  2179 G$AX5043_FOURFSK2$0$0 == 0x414e
                           00414E  2180 _AX5043_FOURFSK2	=	0x414e
                           00415E  2181 G$AX5043_FOURFSK3$0$0 == 0x415e
                           00415E  2182 _AX5043_FOURFSK3	=	0x415e
                           00412D  2183 G$AX5043_FREQDEV00$0$0 == 0x412d
                           00412D  2184 _AX5043_FREQDEV00	=	0x412d
                           00413D  2185 G$AX5043_FREQDEV01$0$0 == 0x413d
                           00413D  2186 _AX5043_FREQDEV01	=	0x413d
                           00414D  2187 G$AX5043_FREQDEV02$0$0 == 0x414d
                           00414D  2188 _AX5043_FREQDEV02	=	0x414d
                           00415D  2189 G$AX5043_FREQDEV03$0$0 == 0x415d
                           00415D  2190 _AX5043_FREQDEV03	=	0x415d
                           00412C  2191 G$AX5043_FREQDEV10$0$0 == 0x412c
                           00412C  2192 _AX5043_FREQDEV10	=	0x412c
                           00413C  2193 G$AX5043_FREQDEV11$0$0 == 0x413c
                           00413C  2194 _AX5043_FREQDEV11	=	0x413c
                           00414C  2195 G$AX5043_FREQDEV12$0$0 == 0x414c
                           00414C  2196 _AX5043_FREQDEV12	=	0x414c
                           00415C  2197 G$AX5043_FREQDEV13$0$0 == 0x415c
                           00415C  2198 _AX5043_FREQDEV13	=	0x415c
                           004127  2199 G$AX5043_FREQUENCYGAINA0$0$0 == 0x4127
                           004127  2200 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  2201 G$AX5043_FREQUENCYGAINA1$0$0 == 0x4137
                           004137  2202 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  2203 G$AX5043_FREQUENCYGAINA2$0$0 == 0x4147
                           004147  2204 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  2205 G$AX5043_FREQUENCYGAINA3$0$0 == 0x4157
                           004157  2206 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  2207 G$AX5043_FREQUENCYGAINB0$0$0 == 0x4128
                           004128  2208 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  2209 G$AX5043_FREQUENCYGAINB1$0$0 == 0x4138
                           004138  2210 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  2211 G$AX5043_FREQUENCYGAINB2$0$0 == 0x4148
                           004148  2212 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  2213 G$AX5043_FREQUENCYGAINB3$0$0 == 0x4158
                           004158  2214 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  2215 G$AX5043_FREQUENCYGAINC0$0$0 == 0x4129
                           004129  2216 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  2217 G$AX5043_FREQUENCYGAINC1$0$0 == 0x4139
                           004139  2218 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  2219 G$AX5043_FREQUENCYGAINC2$0$0 == 0x4149
                           004149  2220 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  2221 G$AX5043_FREQUENCYGAINC3$0$0 == 0x4159
                           004159  2222 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  2223 G$AX5043_FREQUENCYGAIND0$0$0 == 0x412a
                           00412A  2224 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  2225 G$AX5043_FREQUENCYGAIND1$0$0 == 0x413a
                           00413A  2226 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  2227 G$AX5043_FREQUENCYGAIND2$0$0 == 0x414a
                           00414A  2228 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  2229 G$AX5043_FREQUENCYGAIND3$0$0 == 0x415a
                           00415A  2230 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  2231 G$AX5043_FREQUENCYLEAK$0$0 == 0x4116
                           004116  2232 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  2233 G$AX5043_PHASEGAIN0$0$0 == 0x4126
                           004126  2234 _AX5043_PHASEGAIN0	=	0x4126
                           004136  2235 G$AX5043_PHASEGAIN1$0$0 == 0x4136
                           004136  2236 _AX5043_PHASEGAIN1	=	0x4136
                           004146  2237 G$AX5043_PHASEGAIN2$0$0 == 0x4146
                           004146  2238 _AX5043_PHASEGAIN2	=	0x4146
                           004156  2239 G$AX5043_PHASEGAIN3$0$0 == 0x4156
                           004156  2240 _AX5043_PHASEGAIN3	=	0x4156
                           004207  2241 G$AX5043_PKTADDR0$0$0 == 0x4207
                           004207  2242 _AX5043_PKTADDR0	=	0x4207
                           004206  2243 G$AX5043_PKTADDR1$0$0 == 0x4206
                           004206  2244 _AX5043_PKTADDR1	=	0x4206
                           004205  2245 G$AX5043_PKTADDR2$0$0 == 0x4205
                           004205  2246 _AX5043_PKTADDR2	=	0x4205
                           004204  2247 G$AX5043_PKTADDR3$0$0 == 0x4204
                           004204  2248 _AX5043_PKTADDR3	=	0x4204
                           004200  2249 G$AX5043_PKTADDRCFG$0$0 == 0x4200
                           004200  2250 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  2251 G$AX5043_PKTADDRMASK0$0$0 == 0x420b
                           00420B  2252 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  2253 G$AX5043_PKTADDRMASK1$0$0 == 0x420a
                           00420A  2254 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  2255 G$AX5043_PKTADDRMASK2$0$0 == 0x4209
                           004209  2256 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  2257 G$AX5043_PKTADDRMASK3$0$0 == 0x4208
                           004208  2258 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  2259 G$AX5043_PKTLENCFG$0$0 == 0x4201
                           004201  2260 _AX5043_PKTLENCFG	=	0x4201
                           004202  2261 G$AX5043_PKTLENOFFSET$0$0 == 0x4202
                           004202  2262 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  2263 G$AX5043_PKTMAXLEN$0$0 == 0x4203
                           004203  2264 _AX5043_PKTMAXLEN	=	0x4203
                           004118  2265 G$AX5043_RXPARAMCURSET$0$0 == 0x4118
                           004118  2266 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  2267 G$AX5043_RXPARAMSETS$0$0 == 0x4117
                           004117  2268 _AX5043_RXPARAMSETS	=	0x4117
                           004124  2269 G$AX5043_TIMEGAIN0$0$0 == 0x4124
                           004124  2270 _AX5043_TIMEGAIN0	=	0x4124
                           004134  2271 G$AX5043_TIMEGAIN1$0$0 == 0x4134
                           004134  2272 _AX5043_TIMEGAIN1	=	0x4134
                           004144  2273 G$AX5043_TIMEGAIN2$0$0 == 0x4144
                           004144  2274 _AX5043_TIMEGAIN2	=	0x4144
                           004154  2275 G$AX5043_TIMEGAIN3$0$0 == 0x4154
                           004154  2276 _AX5043_TIMEGAIN3	=	0x4154
                           005114  2277 G$AX5043_AFSKCTRLNB$0$0 == 0x5114
                           005114  2278 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  2279 G$AX5043_AFSKMARK0NB$0$0 == 0x5113
                           005113  2280 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  2281 G$AX5043_AFSKMARK1NB$0$0 == 0x5112
                           005112  2282 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  2283 G$AX5043_AFSKSPACE0NB$0$0 == 0x5111
                           005111  2284 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  2285 G$AX5043_AFSKSPACE1NB$0$0 == 0x5110
                           005110  2286 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  2287 G$AX5043_AGCCOUNTERNB$0$0 == 0x5043
                           005043  2288 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  2289 G$AX5043_AMPLFILTERNB$0$0 == 0x5115
                           005115  2290 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  2291 G$AX5043_BBOFFSCAPNB$0$0 == 0x5189
                           005189  2292 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  2293 G$AX5043_BBTUNENB$0$0 == 0x5188
                           005188  2294 _AX5043_BBTUNENB	=	0x5188
                           005041  2295 G$AX5043_BGNDRSSINB$0$0 == 0x5041
                           005041  2296 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  2297 G$AX5043_BGNDRSSIGAINNB$0$0 == 0x522e
                           00522E  2298 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  2299 G$AX5043_BGNDRSSITHRNB$0$0 == 0x522f
                           00522F  2300 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2301 G$AX5043_CRCINIT0NB$0$0 == 0x5017
                           005017  2302 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2303 G$AX5043_CRCINIT1NB$0$0 == 0x5016
                           005016  2304 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2305 G$AX5043_CRCINIT2NB$0$0 == 0x5015
                           005015  2306 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2307 G$AX5043_CRCINIT3NB$0$0 == 0x5014
                           005014  2308 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2309 G$AX5043_DACCONFIGNB$0$0 == 0x5332
                           005332  2310 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2311 G$AX5043_DACVALUE0NB$0$0 == 0x5331
                           005331  2312 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2313 G$AX5043_DACVALUE1NB$0$0 == 0x5330
                           005330  2314 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2315 G$AX5043_DECIMATIONNB$0$0 == 0x5102
                           005102  2316 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2317 G$AX5043_DIVERSITYNB$0$0 == 0x5042
                           005042  2318 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2319 G$AX5043_ENCODINGNB$0$0 == 0x5011
                           005011  2320 _AX5043_ENCODINGNB	=	0x5011
                           005018  2321 G$AX5043_FECNB$0$0 == 0x5018
                           005018  2322 _AX5043_FECNB	=	0x5018
                           00501A  2323 G$AX5043_FECSTATUSNB$0$0 == 0x501a
                           00501A  2324 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2325 G$AX5043_FECSYNCNB$0$0 == 0x5019
                           005019  2326 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2327 G$AX5043_FIFOCOUNT0NB$0$0 == 0x502b
                           00502B  2328 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2329 G$AX5043_FIFOCOUNT1NB$0$0 == 0x502a
                           00502A  2330 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2331 G$AX5043_FIFODATANB$0$0 == 0x5029
                           005029  2332 _AX5043_FIFODATANB	=	0x5029
                           00502D  2333 G$AX5043_FIFOFREE0NB$0$0 == 0x502d
                           00502D  2334 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2335 G$AX5043_FIFOFREE1NB$0$0 == 0x502c
                           00502C  2336 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2337 G$AX5043_FIFOSTATNB$0$0 == 0x5028
                           005028  2338 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2339 G$AX5043_FIFOTHRESH0NB$0$0 == 0x502f
                           00502F  2340 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2341 G$AX5043_FIFOTHRESH1NB$0$0 == 0x502e
                           00502E  2342 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2343 G$AX5043_FRAMINGNB$0$0 == 0x5012
                           005012  2344 _AX5043_FRAMINGNB	=	0x5012
                           005037  2345 G$AX5043_FREQA0NB$0$0 == 0x5037
                           005037  2346 _AX5043_FREQA0NB	=	0x5037
                           005036  2347 G$AX5043_FREQA1NB$0$0 == 0x5036
                           005036  2348 _AX5043_FREQA1NB	=	0x5036
                           005035  2349 G$AX5043_FREQA2NB$0$0 == 0x5035
                           005035  2350 _AX5043_FREQA2NB	=	0x5035
                           005034  2351 G$AX5043_FREQA3NB$0$0 == 0x5034
                           005034  2352 _AX5043_FREQA3NB	=	0x5034
                           00503F  2353 G$AX5043_FREQB0NB$0$0 == 0x503f
                           00503F  2354 _AX5043_FREQB0NB	=	0x503f
                           00503E  2355 G$AX5043_FREQB1NB$0$0 == 0x503e
                           00503E  2356 _AX5043_FREQB1NB	=	0x503e
                           00503D  2357 G$AX5043_FREQB2NB$0$0 == 0x503d
                           00503D  2358 _AX5043_FREQB2NB	=	0x503d
                           00503C  2359 G$AX5043_FREQB3NB$0$0 == 0x503c
                           00503C  2360 _AX5043_FREQB3NB	=	0x503c
                           005163  2361 G$AX5043_FSKDEV0NB$0$0 == 0x5163
                           005163  2362 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2363 G$AX5043_FSKDEV1NB$0$0 == 0x5162
                           005162  2364 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2365 G$AX5043_FSKDEV2NB$0$0 == 0x5161
                           005161  2366 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2367 G$AX5043_FSKDMAX0NB$0$0 == 0x510d
                           00510D  2368 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2369 G$AX5043_FSKDMAX1NB$0$0 == 0x510c
                           00510C  2370 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2371 G$AX5043_FSKDMIN0NB$0$0 == 0x510f
                           00510F  2372 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2373 G$AX5043_FSKDMIN1NB$0$0 == 0x510e
                           00510E  2374 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2375 G$AX5043_GPADC13VALUE0NB$0$0 == 0x5309
                           005309  2376 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2377 G$AX5043_GPADC13VALUE1NB$0$0 == 0x5308
                           005308  2378 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2379 G$AX5043_GPADCCTRLNB$0$0 == 0x5300
                           005300  2380 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2381 G$AX5043_GPADCPERIODNB$0$0 == 0x5301
                           005301  2382 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2383 G$AX5043_IFFREQ0NB$0$0 == 0x5101
                           005101  2384 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2385 G$AX5043_IFFREQ1NB$0$0 == 0x5100
                           005100  2386 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2387 G$AX5043_IRQINVERSION0NB$0$0 == 0x500b
                           00500B  2388 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2389 G$AX5043_IRQINVERSION1NB$0$0 == 0x500a
                           00500A  2390 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2391 G$AX5043_IRQMASK0NB$0$0 == 0x5007
                           005007  2392 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2393 G$AX5043_IRQMASK1NB$0$0 == 0x5006
                           005006  2394 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2395 G$AX5043_IRQREQUEST0NB$0$0 == 0x500d
                           00500D  2396 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2397 G$AX5043_IRQREQUEST1NB$0$0 == 0x500c
                           00500C  2398 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2399 G$AX5043_LPOSCCONFIGNB$0$0 == 0x5310
                           005310  2400 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2401 G$AX5043_LPOSCFREQ0NB$0$0 == 0x5317
                           005317  2402 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2403 G$AX5043_LPOSCFREQ1NB$0$0 == 0x5316
                           005316  2404 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2405 G$AX5043_LPOSCKFILT0NB$0$0 == 0x5313
                           005313  2406 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2407 G$AX5043_LPOSCKFILT1NB$0$0 == 0x5312
                           005312  2408 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2409 G$AX5043_LPOSCPER0NB$0$0 == 0x5319
                           005319  2410 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2411 G$AX5043_LPOSCPER1NB$0$0 == 0x5318
                           005318  2412 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2413 G$AX5043_LPOSCREF0NB$0$0 == 0x5315
                           005315  2414 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2415 G$AX5043_LPOSCREF1NB$0$0 == 0x5314
                           005314  2416 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2417 G$AX5043_LPOSCSTATUSNB$0$0 == 0x5311
                           005311  2418 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2419 G$AX5043_MATCH0LENNB$0$0 == 0x5214
                           005214  2420 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2421 G$AX5043_MATCH0MAXNB$0$0 == 0x5216
                           005216  2422 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2423 G$AX5043_MATCH0MINNB$0$0 == 0x5215
                           005215  2424 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2425 G$AX5043_MATCH0PAT0NB$0$0 == 0x5213
                           005213  2426 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2427 G$AX5043_MATCH0PAT1NB$0$0 == 0x5212
                           005212  2428 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2429 G$AX5043_MATCH0PAT2NB$0$0 == 0x5211
                           005211  2430 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2431 G$AX5043_MATCH0PAT3NB$0$0 == 0x5210
                           005210  2432 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2433 G$AX5043_MATCH1LENNB$0$0 == 0x521c
                           00521C  2434 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2435 G$AX5043_MATCH1MAXNB$0$0 == 0x521e
                           00521E  2436 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2437 G$AX5043_MATCH1MINNB$0$0 == 0x521d
                           00521D  2438 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2439 G$AX5043_MATCH1PAT0NB$0$0 == 0x5219
                           005219  2440 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2441 G$AX5043_MATCH1PAT1NB$0$0 == 0x5218
                           005218  2442 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2443 G$AX5043_MAXDROFFSET0NB$0$0 == 0x5108
                           005108  2444 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2445 G$AX5043_MAXDROFFSET1NB$0$0 == 0x5107
                           005107  2446 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2447 G$AX5043_MAXDROFFSET2NB$0$0 == 0x5106
                           005106  2448 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2449 G$AX5043_MAXRFOFFSET0NB$0$0 == 0x510b
                           00510B  2450 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2451 G$AX5043_MAXRFOFFSET1NB$0$0 == 0x510a
                           00510A  2452 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2453 G$AX5043_MAXRFOFFSET2NB$0$0 == 0x5109
                           005109  2454 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2455 G$AX5043_MODCFGANB$0$0 == 0x5164
                           005164  2456 _AX5043_MODCFGANB	=	0x5164
                           005160  2457 G$AX5043_MODCFGFNB$0$0 == 0x5160
                           005160  2458 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2459 G$AX5043_MODCFGPNB$0$0 == 0x5f5f
                           005F5F  2460 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2461 G$AX5043_MODULATIONNB$0$0 == 0x5010
                           005010  2462 _AX5043_MODULATIONNB	=	0x5010
                           005025  2463 G$AX5043_PINFUNCANTSELNB$0$0 == 0x5025
                           005025  2464 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2465 G$AX5043_PINFUNCDATANB$0$0 == 0x5023
                           005023  2466 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2467 G$AX5043_PINFUNCDCLKNB$0$0 == 0x5022
                           005022  2468 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2469 G$AX5043_PINFUNCIRQNB$0$0 == 0x5024
                           005024  2470 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2471 G$AX5043_PINFUNCPWRAMPNB$0$0 == 0x5026
                           005026  2472 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2473 G$AX5043_PINFUNCSYSCLKNB$0$0 == 0x5021
                           005021  2474 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2475 G$AX5043_PINSTATENB$0$0 == 0x5020
                           005020  2476 _AX5043_PINSTATENB	=	0x5020
                           005233  2477 G$AX5043_PKTACCEPTFLAGSNB$0$0 == 0x5233
                           005233  2478 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2479 G$AX5043_PKTCHUNKSIZENB$0$0 == 0x5230
                           005230  2480 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2481 G$AX5043_PKTMISCFLAGSNB$0$0 == 0x5231
                           005231  2482 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2483 G$AX5043_PKTSTOREFLAGSNB$0$0 == 0x5232
                           005232  2484 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2485 G$AX5043_PLLCPINB$0$0 == 0x5031
                           005031  2486 _AX5043_PLLCPINB	=	0x5031
                           005039  2487 G$AX5043_PLLCPIBOOSTNB$0$0 == 0x5039
                           005039  2488 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2489 G$AX5043_PLLLOCKDETNB$0$0 == 0x5182
                           005182  2490 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2491 G$AX5043_PLLLOOPNB$0$0 == 0x5030
                           005030  2492 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2493 G$AX5043_PLLLOOPBOOSTNB$0$0 == 0x5038
                           005038  2494 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2495 G$AX5043_PLLRANGINGANB$0$0 == 0x5033
                           005033  2496 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2497 G$AX5043_PLLRANGINGBNB$0$0 == 0x503b
                           00503B  2498 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2499 G$AX5043_PLLRNGCLKNB$0$0 == 0x5183
                           005183  2500 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2501 G$AX5043_PLLVCODIVNB$0$0 == 0x5032
                           005032  2502 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2503 G$AX5043_PLLVCOINB$0$0 == 0x5180
                           005180  2504 _AX5043_PLLVCOINB	=	0x5180
                           005181  2505 G$AX5043_PLLVCOIRNB$0$0 == 0x5181
                           005181  2506 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2507 G$AX5043_POWCTRL1NB$0$0 == 0x5f08
                           005F08  2508 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2509 G$AX5043_POWIRQMASKNB$0$0 == 0x5005
                           005005  2510 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2511 G$AX5043_POWSTATNB$0$0 == 0x5003
                           005003  2512 _AX5043_POWSTATNB	=	0x5003
                           005004  2513 G$AX5043_POWSTICKYSTATNB$0$0 == 0x5004
                           005004  2514 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2515 G$AX5043_PWRAMPNB$0$0 == 0x5027
                           005027  2516 _AX5043_PWRAMPNB	=	0x5027
                           005002  2517 G$AX5043_PWRMODENB$0$0 == 0x5002
                           005002  2518 _AX5043_PWRMODENB	=	0x5002
                           005009  2519 G$AX5043_RADIOEVENTMASK0NB$0$0 == 0x5009
                           005009  2520 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2521 G$AX5043_RADIOEVENTMASK1NB$0$0 == 0x5008
                           005008  2522 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2523 G$AX5043_RADIOEVENTREQ0NB$0$0 == 0x500f
                           00500F  2524 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2525 G$AX5043_RADIOEVENTREQ1NB$0$0 == 0x500e
                           00500E  2526 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2527 G$AX5043_RADIOSTATENB$0$0 == 0x501c
                           00501C  2528 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2529 G$AX5043_REFNB$0$0 == 0x5f0d
                           005F0D  2530 _AX5043_REFNB	=	0x5f0d
                           005040  2531 G$AX5043_RSSINB$0$0 == 0x5040
                           005040  2532 _AX5043_RSSINB	=	0x5040
                           00522D  2533 G$AX5043_RSSIABSTHRNB$0$0 == 0x522d
                           00522D  2534 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2535 G$AX5043_RSSIREFERENCENB$0$0 == 0x522c
                           00522C  2536 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2537 G$AX5043_RXDATARATE0NB$0$0 == 0x5105
                           005105  2538 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2539 G$AX5043_RXDATARATE1NB$0$0 == 0x5104
                           005104  2540 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2541 G$AX5043_RXDATARATE2NB$0$0 == 0x5103
                           005103  2542 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2543 G$AX5043_SCRATCHNB$0$0 == 0x5001
                           005001  2544 _AX5043_SCRATCHNB	=	0x5001
                           005000  2545 G$AX5043_SILICONREVISIONNB$0$0 == 0x5000
                           005000  2546 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2547 G$AX5043_TIMER0NB$0$0 == 0x505b
                           00505B  2548 _AX5043_TIMER0NB	=	0x505b
                           00505A  2549 G$AX5043_TIMER1NB$0$0 == 0x505a
                           00505A  2550 _AX5043_TIMER1NB	=	0x505a
                           005059  2551 G$AX5043_TIMER2NB$0$0 == 0x5059
                           005059  2552 _AX5043_TIMER2NB	=	0x5059
                           005227  2553 G$AX5043_TMGRXAGCNB$0$0 == 0x5227
                           005227  2554 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2555 G$AX5043_TMGRXBOOSTNB$0$0 == 0x5223
                           005223  2556 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2557 G$AX5043_TMGRXCOARSEAGCNB$0$0 == 0x5226
                           005226  2558 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2559 G$AX5043_TMGRXOFFSACQNB$0$0 == 0x5225
                           005225  2560 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2561 G$AX5043_TMGRXPREAMBLE1NB$0$0 == 0x5229
                           005229  2562 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2563 G$AX5043_TMGRXPREAMBLE2NB$0$0 == 0x522a
                           00522A  2564 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2565 G$AX5043_TMGRXPREAMBLE3NB$0$0 == 0x522b
                           00522B  2566 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2567 G$AX5043_TMGRXRSSINB$0$0 == 0x5228
                           005228  2568 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2569 G$AX5043_TMGRXSETTLENB$0$0 == 0x5224
                           005224  2570 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2571 G$AX5043_TMGTXBOOSTNB$0$0 == 0x5220
                           005220  2572 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2573 G$AX5043_TMGTXSETTLENB$0$0 == 0x5221
                           005221  2574 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2575 G$AX5043_TRKAFSKDEMOD0NB$0$0 == 0x5055
                           005055  2576 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2577 G$AX5043_TRKAFSKDEMOD1NB$0$0 == 0x5054
                           005054  2578 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2579 G$AX5043_TRKAMPLITUDE0NB$0$0 == 0x5049
                           005049  2580 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2581 G$AX5043_TRKAMPLITUDE1NB$0$0 == 0x5048
                           005048  2582 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2583 G$AX5043_TRKDATARATE0NB$0$0 == 0x5047
                           005047  2584 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2585 G$AX5043_TRKDATARATE1NB$0$0 == 0x5046
                           005046  2586 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2587 G$AX5043_TRKDATARATE2NB$0$0 == 0x5045
                           005045  2588 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2589 G$AX5043_TRKFREQ0NB$0$0 == 0x5051
                           005051  2590 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2591 G$AX5043_TRKFREQ1NB$0$0 == 0x5050
                           005050  2592 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2593 G$AX5043_TRKFSKDEMOD0NB$0$0 == 0x5053
                           005053  2594 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2595 G$AX5043_TRKFSKDEMOD1NB$0$0 == 0x5052
                           005052  2596 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2597 G$AX5043_TRKPHASE0NB$0$0 == 0x504b
                           00504B  2598 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2599 G$AX5043_TRKPHASE1NB$0$0 == 0x504a
                           00504A  2600 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2601 G$AX5043_TRKRFFREQ0NB$0$0 == 0x504f
                           00504F  2602 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2603 G$AX5043_TRKRFFREQ1NB$0$0 == 0x504e
                           00504E  2604 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2605 G$AX5043_TRKRFFREQ2NB$0$0 == 0x504d
                           00504D  2606 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2607 G$AX5043_TXPWRCOEFFA0NB$0$0 == 0x5169
                           005169  2608 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2609 G$AX5043_TXPWRCOEFFA1NB$0$0 == 0x5168
                           005168  2610 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2611 G$AX5043_TXPWRCOEFFB0NB$0$0 == 0x516b
                           00516B  2612 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2613 G$AX5043_TXPWRCOEFFB1NB$0$0 == 0x516a
                           00516A  2614 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2615 G$AX5043_TXPWRCOEFFC0NB$0$0 == 0x516d
                           00516D  2616 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2617 G$AX5043_TXPWRCOEFFC1NB$0$0 == 0x516c
                           00516C  2618 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2619 G$AX5043_TXPWRCOEFFD0NB$0$0 == 0x516f
                           00516F  2620 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2621 G$AX5043_TXPWRCOEFFD1NB$0$0 == 0x516e
                           00516E  2622 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2623 G$AX5043_TXPWRCOEFFE0NB$0$0 == 0x5171
                           005171  2624 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2625 G$AX5043_TXPWRCOEFFE1NB$0$0 == 0x5170
                           005170  2626 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2627 G$AX5043_TXRATE0NB$0$0 == 0x5167
                           005167  2628 _AX5043_TXRATE0NB	=	0x5167
                           005166  2629 G$AX5043_TXRATE1NB$0$0 == 0x5166
                           005166  2630 _AX5043_TXRATE1NB	=	0x5166
                           005165  2631 G$AX5043_TXRATE2NB$0$0 == 0x5165
                           005165  2632 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2633 G$AX5043_WAKEUP0NB$0$0 == 0x506b
                           00506B  2634 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2635 G$AX5043_WAKEUP1NB$0$0 == 0x506a
                           00506A  2636 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2637 G$AX5043_WAKEUPFREQ0NB$0$0 == 0x506d
                           00506D  2638 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2639 G$AX5043_WAKEUPFREQ1NB$0$0 == 0x506c
                           00506C  2640 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2641 G$AX5043_WAKEUPTIMER0NB$0$0 == 0x5069
                           005069  2642 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2643 G$AX5043_WAKEUPTIMER1NB$0$0 == 0x5068
                           005068  2644 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2645 G$AX5043_WAKEUPXOEARLYNB$0$0 == 0x506e
                           00506E  2646 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2647 G$AX5043_XTALAMPLNB$0$0 == 0x5f11
                           005F11  2648 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2649 G$AX5043_XTALCAPNB$0$0 == 0x5184
                           005184  2650 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2651 G$AX5043_XTALOSCNB$0$0 == 0x5f10
                           005F10  2652 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2653 G$AX5043_XTALSTATUSNB$0$0 == 0x501d
                           00501D  2654 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2655 G$AX5043_0xF00NB$0$0 == 0x5f00
                           005F00  2656 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2657 G$AX5043_0xF0CNB$0$0 == 0x5f0c
                           005F0C  2658 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2659 G$AX5043_0xF18NB$0$0 == 0x5f18
                           005F18  2660 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2661 G$AX5043_0xF1CNB$0$0 == 0x5f1c
                           005F1C  2662 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2663 G$AX5043_0xF21NB$0$0 == 0x5f21
                           005F21  2664 _AX5043_0xF21NB	=	0x5f21
                           005F22  2665 G$AX5043_0xF22NB$0$0 == 0x5f22
                           005F22  2666 _AX5043_0xF22NB	=	0x5f22
                           005F23  2667 G$AX5043_0xF23NB$0$0 == 0x5f23
                           005F23  2668 _AX5043_0xF23NB	=	0x5f23
                           005F26  2669 G$AX5043_0xF26NB$0$0 == 0x5f26
                           005F26  2670 _AX5043_0xF26NB	=	0x5f26
                           005F30  2671 G$AX5043_0xF30NB$0$0 == 0x5f30
                           005F30  2672 _AX5043_0xF30NB	=	0x5f30
                           005F31  2673 G$AX5043_0xF31NB$0$0 == 0x5f31
                           005F31  2674 _AX5043_0xF31NB	=	0x5f31
                           005F32  2675 G$AX5043_0xF32NB$0$0 == 0x5f32
                           005F32  2676 _AX5043_0xF32NB	=	0x5f32
                           005F33  2677 G$AX5043_0xF33NB$0$0 == 0x5f33
                           005F33  2678 _AX5043_0xF33NB	=	0x5f33
                           005F34  2679 G$AX5043_0xF34NB$0$0 == 0x5f34
                           005F34  2680 _AX5043_0xF34NB	=	0x5f34
                           005F35  2681 G$AX5043_0xF35NB$0$0 == 0x5f35
                           005F35  2682 _AX5043_0xF35NB	=	0x5f35
                           005F44  2683 G$AX5043_0xF44NB$0$0 == 0x5f44
                           005F44  2684 _AX5043_0xF44NB	=	0x5f44
                           005122  2685 G$AX5043_AGCAHYST0NB$0$0 == 0x5122
                           005122  2686 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2687 G$AX5043_AGCAHYST1NB$0$0 == 0x5132
                           005132  2688 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2689 G$AX5043_AGCAHYST2NB$0$0 == 0x5142
                           005142  2690 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2691 G$AX5043_AGCAHYST3NB$0$0 == 0x5152
                           005152  2692 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2693 G$AX5043_AGCGAIN0NB$0$0 == 0x5120
                           005120  2694 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2695 G$AX5043_AGCGAIN1NB$0$0 == 0x5130
                           005130  2696 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2697 G$AX5043_AGCGAIN2NB$0$0 == 0x5140
                           005140  2698 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2699 G$AX5043_AGCGAIN3NB$0$0 == 0x5150
                           005150  2700 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2701 G$AX5043_AGCMINMAX0NB$0$0 == 0x5123
                           005123  2702 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2703 G$AX5043_AGCMINMAX1NB$0$0 == 0x5133
                           005133  2704 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2705 G$AX5043_AGCMINMAX2NB$0$0 == 0x5143
                           005143  2706 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2707 G$AX5043_AGCMINMAX3NB$0$0 == 0x5153
                           005153  2708 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2709 G$AX5043_AGCTARGET0NB$0$0 == 0x5121
                           005121  2710 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2711 G$AX5043_AGCTARGET1NB$0$0 == 0x5131
                           005131  2712 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2713 G$AX5043_AGCTARGET2NB$0$0 == 0x5141
                           005141  2714 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2715 G$AX5043_AGCTARGET3NB$0$0 == 0x5151
                           005151  2716 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2717 G$AX5043_AMPLITUDEGAIN0NB$0$0 == 0x512b
                           00512B  2718 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2719 G$AX5043_AMPLITUDEGAIN1NB$0$0 == 0x513b
                           00513B  2720 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2721 G$AX5043_AMPLITUDEGAIN2NB$0$0 == 0x514b
                           00514B  2722 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2723 G$AX5043_AMPLITUDEGAIN3NB$0$0 == 0x515b
                           00515B  2724 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2725 G$AX5043_BBOFFSRES0NB$0$0 == 0x512f
                           00512F  2726 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2727 G$AX5043_BBOFFSRES1NB$0$0 == 0x513f
                           00513F  2728 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2729 G$AX5043_BBOFFSRES2NB$0$0 == 0x514f
                           00514F  2730 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2731 G$AX5043_BBOFFSRES3NB$0$0 == 0x515f
                           00515F  2732 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2733 G$AX5043_DRGAIN0NB$0$0 == 0x5125
                           005125  2734 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2735 G$AX5043_DRGAIN1NB$0$0 == 0x5135
                           005135  2736 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2737 G$AX5043_DRGAIN2NB$0$0 == 0x5145
                           005145  2738 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2739 G$AX5043_DRGAIN3NB$0$0 == 0x5155
                           005155  2740 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2741 G$AX5043_FOURFSK0NB$0$0 == 0x512e
                           00512E  2742 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2743 G$AX5043_FOURFSK1NB$0$0 == 0x513e
                           00513E  2744 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2745 G$AX5043_FOURFSK2NB$0$0 == 0x514e
                           00514E  2746 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2747 G$AX5043_FOURFSK3NB$0$0 == 0x515e
                           00515E  2748 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2749 G$AX5043_FREQDEV00NB$0$0 == 0x512d
                           00512D  2750 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2751 G$AX5043_FREQDEV01NB$0$0 == 0x513d
                           00513D  2752 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2753 G$AX5043_FREQDEV02NB$0$0 == 0x514d
                           00514D  2754 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2755 G$AX5043_FREQDEV03NB$0$0 == 0x515d
                           00515D  2756 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2757 G$AX5043_FREQDEV10NB$0$0 == 0x512c
                           00512C  2758 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2759 G$AX5043_FREQDEV11NB$0$0 == 0x513c
                           00513C  2760 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2761 G$AX5043_FREQDEV12NB$0$0 == 0x514c
                           00514C  2762 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2763 G$AX5043_FREQDEV13NB$0$0 == 0x515c
                           00515C  2764 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2765 G$AX5043_FREQUENCYGAINA0NB$0$0 == 0x5127
                           005127  2766 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2767 G$AX5043_FREQUENCYGAINA1NB$0$0 == 0x5137
                           005137  2768 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2769 G$AX5043_FREQUENCYGAINA2NB$0$0 == 0x5147
                           005147  2770 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2771 G$AX5043_FREQUENCYGAINA3NB$0$0 == 0x5157
                           005157  2772 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2773 G$AX5043_FREQUENCYGAINB0NB$0$0 == 0x5128
                           005128  2774 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2775 G$AX5043_FREQUENCYGAINB1NB$0$0 == 0x5138
                           005138  2776 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2777 G$AX5043_FREQUENCYGAINB2NB$0$0 == 0x5148
                           005148  2778 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2779 G$AX5043_FREQUENCYGAINB3NB$0$0 == 0x5158
                           005158  2780 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2781 G$AX5043_FREQUENCYGAINC0NB$0$0 == 0x5129
                           005129  2782 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2783 G$AX5043_FREQUENCYGAINC1NB$0$0 == 0x5139
                           005139  2784 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2785 G$AX5043_FREQUENCYGAINC2NB$0$0 == 0x5149
                           005149  2786 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2787 G$AX5043_FREQUENCYGAINC3NB$0$0 == 0x5159
                           005159  2788 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2789 G$AX5043_FREQUENCYGAIND0NB$0$0 == 0x512a
                           00512A  2790 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2791 G$AX5043_FREQUENCYGAIND1NB$0$0 == 0x513a
                           00513A  2792 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2793 G$AX5043_FREQUENCYGAIND2NB$0$0 == 0x514a
                           00514A  2794 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2795 G$AX5043_FREQUENCYGAIND3NB$0$0 == 0x515a
                           00515A  2796 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2797 G$AX5043_FREQUENCYLEAKNB$0$0 == 0x5116
                           005116  2798 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2799 G$AX5043_PHASEGAIN0NB$0$0 == 0x5126
                           005126  2800 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2801 G$AX5043_PHASEGAIN1NB$0$0 == 0x5136
                           005136  2802 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2803 G$AX5043_PHASEGAIN2NB$0$0 == 0x5146
                           005146  2804 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2805 G$AX5043_PHASEGAIN3NB$0$0 == 0x5156
                           005156  2806 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2807 G$AX5043_PKTADDR0NB$0$0 == 0x5207
                           005207  2808 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2809 G$AX5043_PKTADDR1NB$0$0 == 0x5206
                           005206  2810 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2811 G$AX5043_PKTADDR2NB$0$0 == 0x5205
                           005205  2812 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2813 G$AX5043_PKTADDR3NB$0$0 == 0x5204
                           005204  2814 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2815 G$AX5043_PKTADDRCFGNB$0$0 == 0x5200
                           005200  2816 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2817 G$AX5043_PKTADDRMASK0NB$0$0 == 0x520b
                           00520B  2818 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2819 G$AX5043_PKTADDRMASK1NB$0$0 == 0x520a
                           00520A  2820 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2821 G$AX5043_PKTADDRMASK2NB$0$0 == 0x5209
                           005209  2822 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2823 G$AX5043_PKTADDRMASK3NB$0$0 == 0x5208
                           005208  2824 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2825 G$AX5043_PKTLENCFGNB$0$0 == 0x5201
                           005201  2826 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2827 G$AX5043_PKTLENOFFSETNB$0$0 == 0x5202
                           005202  2828 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2829 G$AX5043_PKTMAXLENNB$0$0 == 0x5203
                           005203  2830 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2831 G$AX5043_RXPARAMCURSETNB$0$0 == 0x5118
                           005118  2832 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2833 G$AX5043_RXPARAMSETSNB$0$0 == 0x5117
                           005117  2834 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2835 G$AX5043_TIMEGAIN0NB$0$0 == 0x5124
                           005124  2836 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2837 G$AX5043_TIMEGAIN1NB$0$0 == 0x5134
                           005134  2838 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2839 G$AX5043_TIMEGAIN2NB$0$0 == 0x5144
                           005144  2840 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2841 G$AX5043_TIMEGAIN3NB$0$0 == 0x5154
                           005154  2842 _AX5043_TIMEGAIN3NB	=	0x5154
                           000000  2843 G$msg1_tmr$0$0==.
      00037B                       2844 _msg1_tmr::
      00037B                       2845 	.ds 8
                           000008  2846 G$msg2_tmr$0$0==.
      000383                       2847 _msg2_tmr::
      000383                       2848 	.ds 8
                           000010  2849 G$msg3_tmr$0$0==.
      00038B                       2850 _msg3_tmr::
      00038B                       2851 	.ds 8
                                   2852 ;--------------------------------------------------------
                                   2853 ; absolute external ram data
                                   2854 ;--------------------------------------------------------
                                   2855 	.area XABS    (ABS,XDATA)
                                   2856 ;--------------------------------------------------------
                                   2857 ; external initialized ram data
                                   2858 ;--------------------------------------------------------
                                   2859 	.area XISEG   (XDATA)
                           000000  2860 G$trmID$0$0==.
      0009B7                       2861 _trmID::
      0009B7                       2862 	.ds 4
                           000004  2863 G$StdPrem$0$0==.
      0009BB                       2864 _StdPrem::
      0009BB                       2865 	.ds 1
                                   2866 	.area HOME    (CODE)
                                   2867 	.area GSINIT0 (CODE)
                                   2868 	.area GSINIT1 (CODE)
                                   2869 	.area GSINIT2 (CODE)
                                   2870 	.area GSINIT3 (CODE)
                                   2871 	.area GSINIT4 (CODE)
                                   2872 	.area GSINIT5 (CODE)
                                   2873 	.area GSINIT  (CODE)
                                   2874 	.area GSFINAL (CODE)
                                   2875 	.area CSEG    (CODE)
                                   2876 ;--------------------------------------------------------
                                   2877 ; global & static initialisations
                                   2878 ;--------------------------------------------------------
                                   2879 	.area HOME    (CODE)
                                   2880 	.area GSINIT  (CODE)
                                   2881 	.area GSFINAL (CODE)
                                   2882 	.area GSINIT  (CODE)
                                   2883 ;--------------------------------------------------------
                                   2884 ; Home
                                   2885 ;--------------------------------------------------------
                                   2886 	.area HOME    (CODE)
                                   2887 	.area HOME    (CODE)
                                   2888 ;--------------------------------------------------------
                                   2889 ; code
                                   2890 ;--------------------------------------------------------
                                   2891 	.area CSEG    (CODE)
                                   2892 ;------------------------------------------------------------
                                   2893 ;Allocation info for local variables in function 'msg1_callback'
                                   2894 ;------------------------------------------------------------
                                   2895 ;desc                      Allocated to registers 
                                   2896 ;------------------------------------------------------------
                           000000  2897 	G$msg1_callback$0$0 ==.
                           000000  2898 	C$transmit3copies.c$38$0$0 ==.
                                   2899 ;	..\COMMON\transmit3copies.c:38: void msg1_callback(struct wtimer_desc __xdata *desc)
                                   2900 ;	-----------------------------------------
                                   2901 ;	 function msg1_callback
                                   2902 ;	-----------------------------------------
      004C83                       2903 _msg1_callback:
                           000007  2904 	ar7 = 0x07
                           000006  2905 	ar6 = 0x06
                           000005  2906 	ar5 = 0x05
                           000004  2907 	ar4 = 0x04
                           000003  2908 	ar3 = 0x03
                           000002  2909 	ar2 = 0x02
                           000001  2910 	ar1 = 0x01
                           000000  2911 	ar0 = 0x00
                           000000  2912 	C$transmit3copies.c$44$1$333 ==.
                                   2913 ;	..\COMMON\transmit3copies.c:44: axradio_transmit(&remoteaddr, demoPacketBuffer, pktLENGTH);
      004C83 75 1B A5         [24] 2914 	mov	_axradio_transmit_PARM_2,#_demoPacketBuffer
      004C86 75 1C 02         [24] 2915 	mov	(_axradio_transmit_PARM_2 + 1),#(_demoPacketBuffer >> 8)
      004C89 75 1D 00         [24] 2916 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004C8C 75 1E 2D         [24] 2917 	mov	_axradio_transmit_PARM_3,#0x2d
      004C8F 75 1F 00         [24] 2918 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      004C92 90 7F 6F         [24] 2919 	mov	dptr,#_remoteaddr
      004C95 75 F0 80         [24] 2920 	mov	b,#0x80
      004C98 12 3A D2         [24] 2921 	lcall	_axradio_transmit
                           000018  2922 	C$transmit3copies.c$46$1$333 ==.
                                   2923 ;	..\COMMON\transmit3copies.c:46: dbglink_writestr("60_TX1_WT0: ");
      004C9B 90 81 07         [24] 2924 	mov	dptr,#___str_0
      004C9E 75 F0 80         [24] 2925 	mov	b,#0x80
      004CA1 12 73 05         [24] 2926 	lcall	_dbglink_writestr
                           000021  2927 	C$transmit3copies.c$47$1$333 ==.
                                   2928 ;	..\COMMON\transmit3copies.c:47: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004CA4 12 7D AD         [24] 2929 	lcall	_wtimer0_curtime
      004CA7 AC 82            [24] 2930 	mov	r4,dpl
      004CA9 AD 83            [24] 2931 	mov	r5,dph
      004CAB AE F0            [24] 2932 	mov	r6,b
      004CAD FF               [12] 2933 	mov	r7,a
      004CAE 74 08            [12] 2934 	mov	a,#0x08
      004CB0 C0 E0            [24] 2935 	push	acc
      004CB2 C0 E0            [24] 2936 	push	acc
      004CB4 8C 82            [24] 2937 	mov	dpl,r4
      004CB6 8D 83            [24] 2938 	mov	dph,r5
      004CB8 8E F0            [24] 2939 	mov	b,r6
      004CBA EF               [12] 2940 	mov	a,r7
      004CBB 12 75 88         [24] 2941 	lcall	_dbglink_writehex32
      004CBE 15 81            [12] 2942 	dec	sp
      004CC0 15 81            [12] 2943 	dec	sp
                           00003F  2944 	C$transmit3copies.c$48$1$333 ==.
                                   2945 ;	..\COMMON\transmit3copies.c:48: dbglink_tx('\n');
      004CC2 75 82 0A         [24] 2946 	mov	dpl,#0x0a
      004CC5 12 61 21         [24] 2947 	lcall	_dbglink_tx
                           000045  2948 	C$transmit3copies.c$49$1$333 ==.
                                   2949 ;	..\COMMON\transmit3copies.c:49: dbglink_writestr("61_TX1_delta: ");
      004CC8 90 81 14         [24] 2950 	mov	dptr,#___str_1
      004CCB 75 F0 80         [24] 2951 	mov	b,#0x80
      004CCE 12 73 05         [24] 2952 	lcall	_dbglink_writestr
                           00004E  2953 	C$transmit3copies.c$50$1$333 ==.
                                   2954 ;	..\COMMON\transmit3copies.c:50: dbglink_writehex32(wtimer0_curtime()-wt0_beacon, 4, WRNUM_PADZERO);
      004CD1 12 7D AD         [24] 2955 	lcall	_wtimer0_curtime
      004CD4 AC 82            [24] 2956 	mov	r4,dpl
      004CD6 AD 83            [24] 2957 	mov	r5,dph
      004CD8 AE F0            [24] 2958 	mov	r6,b
      004CDA FF               [12] 2959 	mov	r7,a
      004CDB 90 09 CF         [24] 2960 	mov	dptr,#_wt0_beacon
      004CDE E0               [24] 2961 	movx	a,@dptr
      004CDF F8               [12] 2962 	mov	r0,a
      004CE0 A3               [24] 2963 	inc	dptr
      004CE1 E0               [24] 2964 	movx	a,@dptr
      004CE2 F9               [12] 2965 	mov	r1,a
      004CE3 A3               [24] 2966 	inc	dptr
      004CE4 E0               [24] 2967 	movx	a,@dptr
      004CE5 FA               [12] 2968 	mov	r2,a
      004CE6 A3               [24] 2969 	inc	dptr
      004CE7 E0               [24] 2970 	movx	a,@dptr
      004CE8 FB               [12] 2971 	mov	r3,a
      004CE9 EC               [12] 2972 	mov	a,r4
      004CEA C3               [12] 2973 	clr	c
      004CEB 98               [12] 2974 	subb	a,r0
      004CEC FC               [12] 2975 	mov	r4,a
      004CED ED               [12] 2976 	mov	a,r5
      004CEE 99               [12] 2977 	subb	a,r1
      004CEF FD               [12] 2978 	mov	r5,a
      004CF0 EE               [12] 2979 	mov	a,r6
      004CF1 9A               [12] 2980 	subb	a,r2
      004CF2 FE               [12] 2981 	mov	r6,a
      004CF3 EF               [12] 2982 	mov	a,r7
      004CF4 9B               [12] 2983 	subb	a,r3
      004CF5 FF               [12] 2984 	mov	r7,a
      004CF6 74 08            [12] 2985 	mov	a,#0x08
      004CF8 C0 E0            [24] 2986 	push	acc
      004CFA 03               [12] 2987 	rr	a
      004CFB C0 E0            [24] 2988 	push	acc
      004CFD 8C 82            [24] 2989 	mov	dpl,r4
      004CFF 8D 83            [24] 2990 	mov	dph,r5
      004D01 8E F0            [24] 2991 	mov	b,r6
      004D03 EF               [12] 2992 	mov	a,r7
      004D04 12 75 88         [24] 2993 	lcall	_dbglink_writehex32
      004D07 15 81            [12] 2994 	dec	sp
      004D09 15 81            [12] 2995 	dec	sp
                           000088  2996 	C$transmit3copies.c$51$1$333 ==.
                                   2997 ;	..\COMMON\transmit3copies.c:51: dbglink_tx('\n');
      004D0B 75 82 0A         [24] 2998 	mov	dpl,#0x0a
      004D0E 12 61 21         [24] 2999 	lcall	_dbglink_tx
                           00008E  3000 	C$transmit3copies.c$54$1$333 ==.
                           00008E  3001 	XG$msg1_callback$0$0 ==.
      004D11 22               [24] 3002 	ret
                                   3003 ;------------------------------------------------------------
                                   3004 ;Allocation info for local variables in function 'msg2_callback'
                                   3005 ;------------------------------------------------------------
                                   3006 ;desc                      Allocated to registers 
                                   3007 ;------------------------------------------------------------
                           00008F  3008 	G$msg2_callback$0$0 ==.
                           00008F  3009 	C$transmit3copies.c$55$1$333 ==.
                                   3010 ;	..\COMMON\transmit3copies.c:55: void msg2_callback(struct wtimer_desc __xdata *desc)
                                   3011 ;	-----------------------------------------
                                   3012 ;	 function msg2_callback
                                   3013 ;	-----------------------------------------
      004D12                       3014 _msg2_callback:
                           00008F  3015 	C$transmit3copies.c$58$1$335 ==.
                                   3016 ;	..\COMMON\transmit3copies.c:58: axradio_transmit(&remoteaddr, demoPacketBuffer + pktLENGTH, pktLENGTH);
      004D12 75 1B D2         [24] 3017 	mov	_axradio_transmit_PARM_2,#(_demoPacketBuffer + 0x002d)
      004D15 75 1C 02         [24] 3018 	mov	(_axradio_transmit_PARM_2 + 1),#((_demoPacketBuffer + 0x002d) >> 8)
      004D18 75 1D 00         [24] 3019 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004D1B 75 1E 2D         [24] 3020 	mov	_axradio_transmit_PARM_3,#0x2d
      004D1E 75 1F 00         [24] 3021 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      004D21 90 7F 6F         [24] 3022 	mov	dptr,#_remoteaddr
      004D24 75 F0 80         [24] 3023 	mov	b,#0x80
      004D27 12 3A D2         [24] 3024 	lcall	_axradio_transmit
                           0000A7  3025 	C$transmit3copies.c$60$1$335 ==.
                                   3026 ;	..\COMMON\transmit3copies.c:60: dbglink_writestr("70_TX2_WT0: ");
      004D2A 90 81 23         [24] 3027 	mov	dptr,#___str_2
      004D2D 75 F0 80         [24] 3028 	mov	b,#0x80
      004D30 12 73 05         [24] 3029 	lcall	_dbglink_writestr
                           0000B0  3030 	C$transmit3copies.c$61$1$335 ==.
                                   3031 ;	..\COMMON\transmit3copies.c:61: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004D33 12 7D AD         [24] 3032 	lcall	_wtimer0_curtime
      004D36 AC 82            [24] 3033 	mov	r4,dpl
      004D38 AD 83            [24] 3034 	mov	r5,dph
      004D3A AE F0            [24] 3035 	mov	r6,b
      004D3C FF               [12] 3036 	mov	r7,a
      004D3D 74 08            [12] 3037 	mov	a,#0x08
      004D3F C0 E0            [24] 3038 	push	acc
      004D41 C0 E0            [24] 3039 	push	acc
      004D43 8C 82            [24] 3040 	mov	dpl,r4
      004D45 8D 83            [24] 3041 	mov	dph,r5
      004D47 8E F0            [24] 3042 	mov	b,r6
      004D49 EF               [12] 3043 	mov	a,r7
      004D4A 12 75 88         [24] 3044 	lcall	_dbglink_writehex32
      004D4D 15 81            [12] 3045 	dec	sp
      004D4F 15 81            [12] 3046 	dec	sp
                           0000CE  3047 	C$transmit3copies.c$62$1$335 ==.
                                   3048 ;	..\COMMON\transmit3copies.c:62: dbglink_tx('\n');
      004D51 75 82 0A         [24] 3049 	mov	dpl,#0x0a
      004D54 12 61 21         [24] 3050 	lcall	_dbglink_tx
                           0000D4  3051 	C$transmit3copies.c$63$1$335 ==.
                                   3052 ;	..\COMMON\transmit3copies.c:63: dbglink_writestr("71_TX2_delta: ");
      004D57 90 81 30         [24] 3053 	mov	dptr,#___str_3
      004D5A 75 F0 80         [24] 3054 	mov	b,#0x80
      004D5D 12 73 05         [24] 3055 	lcall	_dbglink_writestr
                           0000DD  3056 	C$transmit3copies.c$64$1$335 ==.
                                   3057 ;	..\COMMON\transmit3copies.c:64: dbglink_writehex32(wtimer0_curtime()-wt0_beacon, 4, WRNUM_PADZERO);
      004D60 12 7D AD         [24] 3058 	lcall	_wtimer0_curtime
      004D63 AC 82            [24] 3059 	mov	r4,dpl
      004D65 AD 83            [24] 3060 	mov	r5,dph
      004D67 AE F0            [24] 3061 	mov	r6,b
      004D69 FF               [12] 3062 	mov	r7,a
      004D6A 90 09 CF         [24] 3063 	mov	dptr,#_wt0_beacon
      004D6D E0               [24] 3064 	movx	a,@dptr
      004D6E F8               [12] 3065 	mov	r0,a
      004D6F A3               [24] 3066 	inc	dptr
      004D70 E0               [24] 3067 	movx	a,@dptr
      004D71 F9               [12] 3068 	mov	r1,a
      004D72 A3               [24] 3069 	inc	dptr
      004D73 E0               [24] 3070 	movx	a,@dptr
      004D74 FA               [12] 3071 	mov	r2,a
      004D75 A3               [24] 3072 	inc	dptr
      004D76 E0               [24] 3073 	movx	a,@dptr
      004D77 FB               [12] 3074 	mov	r3,a
      004D78 EC               [12] 3075 	mov	a,r4
      004D79 C3               [12] 3076 	clr	c
      004D7A 98               [12] 3077 	subb	a,r0
      004D7B FC               [12] 3078 	mov	r4,a
      004D7C ED               [12] 3079 	mov	a,r5
      004D7D 99               [12] 3080 	subb	a,r1
      004D7E FD               [12] 3081 	mov	r5,a
      004D7F EE               [12] 3082 	mov	a,r6
      004D80 9A               [12] 3083 	subb	a,r2
      004D81 FE               [12] 3084 	mov	r6,a
      004D82 EF               [12] 3085 	mov	a,r7
      004D83 9B               [12] 3086 	subb	a,r3
      004D84 FF               [12] 3087 	mov	r7,a
      004D85 74 08            [12] 3088 	mov	a,#0x08
      004D87 C0 E0            [24] 3089 	push	acc
      004D89 03               [12] 3090 	rr	a
      004D8A C0 E0            [24] 3091 	push	acc
      004D8C 8C 82            [24] 3092 	mov	dpl,r4
      004D8E 8D 83            [24] 3093 	mov	dph,r5
      004D90 8E F0            [24] 3094 	mov	b,r6
      004D92 EF               [12] 3095 	mov	a,r7
      004D93 12 75 88         [24] 3096 	lcall	_dbglink_writehex32
      004D96 15 81            [12] 3097 	dec	sp
      004D98 15 81            [12] 3098 	dec	sp
                           000117  3099 	C$transmit3copies.c$65$1$335 ==.
                                   3100 ;	..\COMMON\transmit3copies.c:65: dbglink_tx('\n');
      004D9A 75 82 0A         [24] 3101 	mov	dpl,#0x0a
      004D9D 12 61 21         [24] 3102 	lcall	_dbglink_tx
                           00011D  3103 	C$transmit3copies.c$68$1$335 ==.
                           00011D  3104 	XG$msg2_callback$0$0 ==.
      004DA0 22               [24] 3105 	ret
                                   3106 ;------------------------------------------------------------
                                   3107 ;Allocation info for local variables in function 'msg3_callback'
                                   3108 ;------------------------------------------------------------
                                   3109 ;desc                      Allocated to registers 
                                   3110 ;------------------------------------------------------------
                           00011E  3111 	G$msg3_callback$0$0 ==.
                           00011E  3112 	C$transmit3copies.c$69$1$335 ==.
                                   3113 ;	..\COMMON\transmit3copies.c:69: void msg3_callback(struct wtimer_desc __xdata *desc)
                                   3114 ;	-----------------------------------------
                                   3115 ;	 function msg3_callback
                                   3116 ;	-----------------------------------------
      004DA1                       3117 _msg3_callback:
                           00011E  3118 	C$transmit3copies.c$72$1$337 ==.
                                   3119 ;	..\COMMON\transmit3copies.c:72: axradio_transmit(&remoteaddr, demoPacketBuffer + (2*pktLENGTH), pktLENGTH);
      004DA1 75 1B FF         [24] 3120 	mov	_axradio_transmit_PARM_2,#(_demoPacketBuffer + 0x005a)
      004DA4 75 1C 02         [24] 3121 	mov	(_axradio_transmit_PARM_2 + 1),#((_demoPacketBuffer + 0x005a) >> 8)
      004DA7 75 1D 00         [24] 3122 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004DAA 75 1E 2D         [24] 3123 	mov	_axradio_transmit_PARM_3,#0x2d
      004DAD 75 1F 00         [24] 3124 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      004DB0 90 7F 6F         [24] 3125 	mov	dptr,#_remoteaddr
      004DB3 75 F0 80         [24] 3126 	mov	b,#0x80
      004DB6 12 3A D2         [24] 3127 	lcall	_axradio_transmit
                           000136  3128 	C$transmit3copies.c$74$1$337 ==.
                                   3129 ;	..\COMMON\transmit3copies.c:74: wt0_tx3 = wtimer0_curtime();
      004DB9 12 7D AD         [24] 3130 	lcall	_wtimer0_curtime
      004DBC AC 82            [24] 3131 	mov	r4,dpl
      004DBE AD 83            [24] 3132 	mov	r5,dph
      004DC0 AE F0            [24] 3133 	mov	r6,b
      004DC2 FF               [12] 3134 	mov	r7,a
      004DC3 90 09 D3         [24] 3135 	mov	dptr,#_wt0_tx3
      004DC6 EC               [12] 3136 	mov	a,r4
      004DC7 F0               [24] 3137 	movx	@dptr,a
      004DC8 ED               [12] 3138 	mov	a,r5
      004DC9 A3               [24] 3139 	inc	dptr
      004DCA F0               [24] 3140 	movx	@dptr,a
      004DCB EE               [12] 3141 	mov	a,r6
      004DCC A3               [24] 3142 	inc	dptr
      004DCD F0               [24] 3143 	movx	@dptr,a
      004DCE EF               [12] 3144 	mov	a,r7
      004DCF A3               [24] 3145 	inc	dptr
      004DD0 F0               [24] 3146 	movx	@dptr,a
                           00014E  3147 	C$transmit3copies.c$76$1$337 ==.
                                   3148 ;	..\COMMON\transmit3copies.c:76: dbglink_writestr("80_TX3_WT0: ");
      004DD1 90 81 3F         [24] 3149 	mov	dptr,#___str_4
      004DD4 75 F0 80         [24] 3150 	mov	b,#0x80
      004DD7 12 73 05         [24] 3151 	lcall	_dbglink_writestr
                           000157  3152 	C$transmit3copies.c$77$1$337 ==.
                                   3153 ;	..\COMMON\transmit3copies.c:77: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004DDA 12 7D AD         [24] 3154 	lcall	_wtimer0_curtime
      004DDD AC 82            [24] 3155 	mov	r4,dpl
      004DDF AD 83            [24] 3156 	mov	r5,dph
      004DE1 AE F0            [24] 3157 	mov	r6,b
      004DE3 FF               [12] 3158 	mov	r7,a
      004DE4 74 08            [12] 3159 	mov	a,#0x08
      004DE6 C0 E0            [24] 3160 	push	acc
      004DE8 C0 E0            [24] 3161 	push	acc
      004DEA 8C 82            [24] 3162 	mov	dpl,r4
      004DEC 8D 83            [24] 3163 	mov	dph,r5
      004DEE 8E F0            [24] 3164 	mov	b,r6
      004DF0 EF               [12] 3165 	mov	a,r7
      004DF1 12 75 88         [24] 3166 	lcall	_dbglink_writehex32
      004DF4 15 81            [12] 3167 	dec	sp
      004DF6 15 81            [12] 3168 	dec	sp
                           000175  3169 	C$transmit3copies.c$78$1$337 ==.
                                   3170 ;	..\COMMON\transmit3copies.c:78: dbglink_tx('\n');
      004DF8 75 82 0A         [24] 3171 	mov	dpl,#0x0a
      004DFB 12 61 21         [24] 3172 	lcall	_dbglink_tx
                           00017B  3173 	C$transmit3copies.c$79$1$337 ==.
                                   3174 ;	..\COMMON\transmit3copies.c:79: dbglink_writestr("81_TX3_delta: ");
      004DFE 90 81 4C         [24] 3175 	mov	dptr,#___str_5
      004E01 75 F0 80         [24] 3176 	mov	b,#0x80
      004E04 12 73 05         [24] 3177 	lcall	_dbglink_writestr
                           000184  3178 	C$transmit3copies.c$80$1$337 ==.
                                   3179 ;	..\COMMON\transmit3copies.c:80: dbglink_writehex32(wtimer0_curtime()-wt0_beacon, 4, WRNUM_PADZERO);
      004E07 12 7D AD         [24] 3180 	lcall	_wtimer0_curtime
      004E0A AC 82            [24] 3181 	mov	r4,dpl
      004E0C AD 83            [24] 3182 	mov	r5,dph
      004E0E AE F0            [24] 3183 	mov	r6,b
      004E10 FF               [12] 3184 	mov	r7,a
      004E11 90 09 CF         [24] 3185 	mov	dptr,#_wt0_beacon
      004E14 E0               [24] 3186 	movx	a,@dptr
      004E15 F8               [12] 3187 	mov	r0,a
      004E16 A3               [24] 3188 	inc	dptr
      004E17 E0               [24] 3189 	movx	a,@dptr
      004E18 F9               [12] 3190 	mov	r1,a
      004E19 A3               [24] 3191 	inc	dptr
      004E1A E0               [24] 3192 	movx	a,@dptr
      004E1B FA               [12] 3193 	mov	r2,a
      004E1C A3               [24] 3194 	inc	dptr
      004E1D E0               [24] 3195 	movx	a,@dptr
      004E1E FB               [12] 3196 	mov	r3,a
      004E1F EC               [12] 3197 	mov	a,r4
      004E20 C3               [12] 3198 	clr	c
      004E21 98               [12] 3199 	subb	a,r0
      004E22 FC               [12] 3200 	mov	r4,a
      004E23 ED               [12] 3201 	mov	a,r5
      004E24 99               [12] 3202 	subb	a,r1
      004E25 FD               [12] 3203 	mov	r5,a
      004E26 EE               [12] 3204 	mov	a,r6
      004E27 9A               [12] 3205 	subb	a,r2
      004E28 FE               [12] 3206 	mov	r6,a
      004E29 EF               [12] 3207 	mov	a,r7
      004E2A 9B               [12] 3208 	subb	a,r3
      004E2B FF               [12] 3209 	mov	r7,a
      004E2C 74 08            [12] 3210 	mov	a,#0x08
      004E2E C0 E0            [24] 3211 	push	acc
      004E30 03               [12] 3212 	rr	a
      004E31 C0 E0            [24] 3213 	push	acc
      004E33 8C 82            [24] 3214 	mov	dpl,r4
      004E35 8D 83            [24] 3215 	mov	dph,r5
      004E37 8E F0            [24] 3216 	mov	b,r6
      004E39 EF               [12] 3217 	mov	a,r7
      004E3A 12 75 88         [24] 3218 	lcall	_dbglink_writehex32
      004E3D 15 81            [12] 3219 	dec	sp
      004E3F 15 81            [12] 3220 	dec	sp
                           0001BE  3221 	C$transmit3copies.c$81$1$337 ==.
                                   3222 ;	..\COMMON\transmit3copies.c:81: dbglink_tx('\n');
      004E41 75 82 0A         [24] 3223 	mov	dpl,#0x0a
      004E44 12 61 21         [24] 3224 	lcall	_dbglink_tx
                           0001C4  3225 	C$transmit3copies.c$84$1$337 ==.
                           0001C4  3226 	XG$msg3_callback$0$0 ==.
      004E47 22               [24] 3227 	ret
                                   3228 ;------------------------------------------------------------
                                   3229 ;Allocation info for local variables in function 'transmit_copies_wtimer'
                                   3230 ;------------------------------------------------------------
                                   3231 ;sloc0                     Allocated with name '_transmit_copies_wtimer_sloc0_1_0'
                                   3232 ;time1_wtimer              Allocated with name '_transmit_copies_wtimer_time1_wtimer_1_339'
                                   3233 ;time2_wtimer              Allocated with name '_transmit_copies_wtimer_time2_wtimer_1_339'
                                   3234 ;time3_wtimer              Allocated with name '_transmit_copies_wtimer_time3_wtimer_1_339'
                                   3235 ;------------------------------------------------------------
                           0001C5  3236 	G$transmit_copies_wtimer$0$0 ==.
                           0001C5  3237 	C$transmit3copies.c$85$1$337 ==.
                                   3238 ;	..\COMMON\transmit3copies.c:85: void transmit_copies_wtimer(void)
                                   3239 ;	-----------------------------------------
                                   3240 ;	 function transmit_copies_wtimer
                                   3241 ;	-----------------------------------------
      004E48                       3242 _transmit_copies_wtimer:
                           0001C5  3243 	C$transmit3copies.c$92$1$339 ==.
                                   3244 ;	..\COMMON\transmit3copies.c:92: time1_wtimer = randomSet[0] * slotStdWIDTH_MS * 0.64; // warnings of overflow
      004E48 90 02 A0         [24] 3245 	mov	dptr,#_randomSet
      004E4B E0               [24] 3246 	movx	a,@dptr
      004E4C F5 82            [12] 3247 	mov	dpl,a
      004E4E 12 7C 4A         [24] 3248 	lcall	___uchar2fs
      004E51 AC 82            [24] 3249 	mov	r4,dpl
      004E53 AD 83            [24] 3250 	mov	r5,dph
      004E55 AE F0            [24] 3251 	mov	r6,b
      004E57 FF               [12] 3252 	mov	r7,a
      004E58 C0 04            [24] 3253 	push	ar4
      004E5A C0 05            [24] 3254 	push	ar5
      004E5C C0 06            [24] 3255 	push	ar6
      004E5E C0 07            [24] 3256 	push	ar7
      004E60 90 CC CD         [24] 3257 	mov	dptr,#0xcccd
      004E63 75 F0 4C         [24] 3258 	mov	b,#0x4c
      004E66 74 42            [12] 3259 	mov	a,#0x42
      004E68 12 60 27         [24] 3260 	lcall	___fsmul
      004E6B AC 82            [24] 3261 	mov	r4,dpl
      004E6D AD 83            [24] 3262 	mov	r5,dph
      004E6F AE F0            [24] 3263 	mov	r6,b
      004E71 FF               [12] 3264 	mov	r7,a
      004E72 E5 81            [12] 3265 	mov	a,sp
      004E74 24 FC            [12] 3266 	add	a,#0xfc
      004E76 F5 81            [12] 3267 	mov	sp,a
      004E78 8C 82            [24] 3268 	mov	dpl,r4
      004E7A 8D 83            [24] 3269 	mov	dph,r5
      004E7C 8E F0            [24] 3270 	mov	b,r6
      004E7E EF               [12] 3271 	mov	a,r7
      004E7F 12 6B C8         [24] 3272 	lcall	___fs2ulong
      004E82 AC 82            [24] 3273 	mov	r4,dpl
      004E84 AD 83            [24] 3274 	mov	r5,dph
      004E86 AE F0            [24] 3275 	mov	r6,b
      004E88 FF               [12] 3276 	mov	r7,a
                           000206  3277 	C$transmit3copies.c$93$1$339 ==.
                                   3278 ;	..\COMMON\transmit3copies.c:93: time2_wtimer = randomSet[1] * slotStdWIDTH_MS * 0.64;
      004E89 90 02 A1         [24] 3279 	mov	dptr,#(_randomSet + 0x0001)
      004E8C E0               [24] 3280 	movx	a,@dptr
      004E8D F5 82            [12] 3281 	mov	dpl,a
      004E8F C0 07            [24] 3282 	push	ar7
      004E91 C0 06            [24] 3283 	push	ar6
      004E93 C0 05            [24] 3284 	push	ar5
      004E95 C0 04            [24] 3285 	push	ar4
      004E97 12 7C 4A         [24] 3286 	lcall	___uchar2fs
      004E9A A8 82            [24] 3287 	mov	r0,dpl
      004E9C A9 83            [24] 3288 	mov	r1,dph
      004E9E AA F0            [24] 3289 	mov	r2,b
      004EA0 FB               [12] 3290 	mov	r3,a
      004EA1 C0 00            [24] 3291 	push	ar0
      004EA3 C0 01            [24] 3292 	push	ar1
      004EA5 C0 02            [24] 3293 	push	ar2
      004EA7 C0 03            [24] 3294 	push	ar3
      004EA9 90 CC CD         [24] 3295 	mov	dptr,#0xcccd
      004EAC 75 F0 4C         [24] 3296 	mov	b,#0x4c
      004EAF 74 42            [12] 3297 	mov	a,#0x42
      004EB1 12 60 27         [24] 3298 	lcall	___fsmul
      004EB4 A8 82            [24] 3299 	mov	r0,dpl
      004EB6 A9 83            [24] 3300 	mov	r1,dph
      004EB8 AA F0            [24] 3301 	mov	r2,b
      004EBA FB               [12] 3302 	mov	r3,a
      004EBB E5 81            [12] 3303 	mov	a,sp
      004EBD 24 FC            [12] 3304 	add	a,#0xfc
      004EBF F5 81            [12] 3305 	mov	sp,a
      004EC1 88 82            [24] 3306 	mov	dpl,r0
      004EC3 89 83            [24] 3307 	mov	dph,r1
      004EC5 8A F0            [24] 3308 	mov	b,r2
      004EC7 EB               [12] 3309 	mov	a,r3
      004EC8 12 6B C8         [24] 3310 	lcall	___fs2ulong
      004ECB 85 82 34         [24] 3311 	mov	_transmit_copies_wtimer_sloc0_1_0,dpl
      004ECE 85 83 35         [24] 3312 	mov	(_transmit_copies_wtimer_sloc0_1_0 + 1),dph
      004ED1 85 F0 36         [24] 3313 	mov	(_transmit_copies_wtimer_sloc0_1_0 + 2),b
      004ED4 F5 37            [12] 3314 	mov	(_transmit_copies_wtimer_sloc0_1_0 + 3),a
                           000253  3315 	C$transmit3copies.c$94$1$339 ==.
                                   3316 ;	..\COMMON\transmit3copies.c:94: time3_wtimer = randomSet[2] * slotStdWIDTH_MS * 0.64;
      004ED6 90 02 A2         [24] 3317 	mov	dptr,#(_randomSet + 0x0002)
      004ED9 E0               [24] 3318 	movx	a,@dptr
      004EDA F5 82            [12] 3319 	mov	dpl,a
      004EDC 12 7C 4A         [24] 3320 	lcall	___uchar2fs
      004EDF A8 82            [24] 3321 	mov	r0,dpl
      004EE1 A9 83            [24] 3322 	mov	r1,dph
      004EE3 AA F0            [24] 3323 	mov	r2,b
      004EE5 FB               [12] 3324 	mov	r3,a
      004EE6 C0 00            [24] 3325 	push	ar0
      004EE8 C0 01            [24] 3326 	push	ar1
      004EEA C0 02            [24] 3327 	push	ar2
      004EEC C0 03            [24] 3328 	push	ar3
      004EEE 90 CC CD         [24] 3329 	mov	dptr,#0xcccd
      004EF1 75 F0 4C         [24] 3330 	mov	b,#0x4c
      004EF4 74 42            [12] 3331 	mov	a,#0x42
      004EF6 12 60 27         [24] 3332 	lcall	___fsmul
      004EF9 A8 82            [24] 3333 	mov	r0,dpl
      004EFB A9 83            [24] 3334 	mov	r1,dph
      004EFD AA F0            [24] 3335 	mov	r2,b
      004EFF FB               [12] 3336 	mov	r3,a
      004F00 E5 81            [12] 3337 	mov	a,sp
      004F02 24 FC            [12] 3338 	add	a,#0xfc
      004F04 F5 81            [12] 3339 	mov	sp,a
      004F06 88 82            [24] 3340 	mov	dpl,r0
      004F08 89 83            [24] 3341 	mov	dph,r1
      004F0A 8A F0            [24] 3342 	mov	b,r2
      004F0C EB               [12] 3343 	mov	a,r3
      004F0D 12 6B C8         [24] 3344 	lcall	___fs2ulong
      004F10 A8 82            [24] 3345 	mov	r0,dpl
      004F12 A9 83            [24] 3346 	mov	r1,dph
      004F14 AA F0            [24] 3347 	mov	r2,b
      004F16 FB               [12] 3348 	mov	r3,a
                           000294  3349 	C$transmit3copies.c$109$1$339 ==.
                                   3350 ;	..\COMMON\transmit3copies.c:109: wtimer0_remove(&msg1_tmr);
      004F17 90 03 7B         [24] 3351 	mov	dptr,#_msg1_tmr
      004F1A C0 03            [24] 3352 	push	ar3
      004F1C C0 02            [24] 3353 	push	ar2
      004F1E C0 01            [24] 3354 	push	ar1
      004F20 C0 00            [24] 3355 	push	ar0
      004F22 12 6D 98         [24] 3356 	lcall	_wtimer0_remove
      004F25 D0 00            [24] 3357 	pop	ar0
      004F27 D0 01            [24] 3358 	pop	ar1
      004F29 D0 02            [24] 3359 	pop	ar2
      004F2B D0 03            [24] 3360 	pop	ar3
      004F2D D0 04            [24] 3361 	pop	ar4
      004F2F D0 05            [24] 3362 	pop	ar5
      004F31 D0 06            [24] 3363 	pop	ar6
      004F33 D0 07            [24] 3364 	pop	ar7
                           0002B2  3365 	C$transmit3copies.c$110$1$339 ==.
                                   3366 ;	..\COMMON\transmit3copies.c:110: msg1_tmr.handler = msg1_callback;
      004F35 90 03 7D         [24] 3367 	mov	dptr,#(_msg1_tmr + 0x0002)
      004F38 74 83            [12] 3368 	mov	a,#_msg1_callback
      004F3A F0               [24] 3369 	movx	@dptr,a
      004F3B 74 4C            [12] 3370 	mov	a,#(_msg1_callback >> 8)
      004F3D A3               [24] 3371 	inc	dptr
      004F3E F0               [24] 3372 	movx	@dptr,a
                           0002BC  3373 	C$transmit3copies.c$111$1$339 ==.
                                   3374 ;	..\COMMON\transmit3copies.c:111: msg1_tmr.time = time1_wtimer;
      004F3F 90 03 7F         [24] 3375 	mov	dptr,#(_msg1_tmr + 0x0004)
      004F42 EC               [12] 3376 	mov	a,r4
      004F43 F0               [24] 3377 	movx	@dptr,a
      004F44 ED               [12] 3378 	mov	a,r5
      004F45 A3               [24] 3379 	inc	dptr
      004F46 F0               [24] 3380 	movx	@dptr,a
      004F47 EE               [12] 3381 	mov	a,r6
      004F48 A3               [24] 3382 	inc	dptr
      004F49 F0               [24] 3383 	movx	@dptr,a
      004F4A EF               [12] 3384 	mov	a,r7
      004F4B A3               [24] 3385 	inc	dptr
      004F4C F0               [24] 3386 	movx	@dptr,a
                           0002CA  3387 	C$transmit3copies.c$112$1$339 ==.
                                   3388 ;	..\COMMON\transmit3copies.c:112: wtimer0_addrelative(&msg1_tmr);
      004F4D 90 03 7B         [24] 3389 	mov	dptr,#_msg1_tmr
      004F50 C0 03            [24] 3390 	push	ar3
      004F52 C0 02            [24] 3391 	push	ar2
      004F54 C0 01            [24] 3392 	push	ar1
      004F56 C0 00            [24] 3393 	push	ar0
      004F58 12 69 E7         [24] 3394 	lcall	_wtimer0_addrelative
                           0002D8  3395 	C$transmit3copies.c$114$1$339 ==.
                                   3396 ;	..\COMMON\transmit3copies.c:114: wtimer0_remove(&msg2_tmr);
      004F5B 90 03 83         [24] 3397 	mov	dptr,#_msg2_tmr
      004F5E 12 6D 98         [24] 3398 	lcall	_wtimer0_remove
                           0002DE  3399 	C$transmit3copies.c$115$1$339 ==.
                                   3400 ;	..\COMMON\transmit3copies.c:115: msg2_tmr.handler = msg2_callback;
      004F61 90 03 85         [24] 3401 	mov	dptr,#(_msg2_tmr + 0x0002)
      004F64 74 12            [12] 3402 	mov	a,#_msg2_callback
      004F66 F0               [24] 3403 	movx	@dptr,a
      004F67 74 4D            [12] 3404 	mov	a,#(_msg2_callback >> 8)
      004F69 A3               [24] 3405 	inc	dptr
      004F6A F0               [24] 3406 	movx	@dptr,a
                           0002E8  3407 	C$transmit3copies.c$116$1$339 ==.
                                   3408 ;	..\COMMON\transmit3copies.c:116: msg2_tmr.time = time2_wtimer;
      004F6B 90 03 87         [24] 3409 	mov	dptr,#(_msg2_tmr + 0x0004)
      004F6E E5 34            [12] 3410 	mov	a,_transmit_copies_wtimer_sloc0_1_0
      004F70 F0               [24] 3411 	movx	@dptr,a
      004F71 E5 35            [12] 3412 	mov	a,(_transmit_copies_wtimer_sloc0_1_0 + 1)
      004F73 A3               [24] 3413 	inc	dptr
      004F74 F0               [24] 3414 	movx	@dptr,a
      004F75 E5 36            [12] 3415 	mov	a,(_transmit_copies_wtimer_sloc0_1_0 + 2)
      004F77 A3               [24] 3416 	inc	dptr
      004F78 F0               [24] 3417 	movx	@dptr,a
      004F79 E5 37            [12] 3418 	mov	a,(_transmit_copies_wtimer_sloc0_1_0 + 3)
      004F7B A3               [24] 3419 	inc	dptr
      004F7C F0               [24] 3420 	movx	@dptr,a
                           0002FA  3421 	C$transmit3copies.c$117$1$339 ==.
                                   3422 ;	..\COMMON\transmit3copies.c:117: wtimer0_addrelative(&msg2_tmr);
      004F7D 90 03 83         [24] 3423 	mov	dptr,#_msg2_tmr
      004F80 12 69 E7         [24] 3424 	lcall	_wtimer0_addrelative
                           000300  3425 	C$transmit3copies.c$119$1$339 ==.
                                   3426 ;	..\COMMON\transmit3copies.c:119: wtimer0_remove(&msg3_tmr);
      004F83 90 03 8B         [24] 3427 	mov	dptr,#_msg3_tmr
      004F86 12 6D 98         [24] 3428 	lcall	_wtimer0_remove
      004F89 D0 00            [24] 3429 	pop	ar0
      004F8B D0 01            [24] 3430 	pop	ar1
      004F8D D0 02            [24] 3431 	pop	ar2
      004F8F D0 03            [24] 3432 	pop	ar3
                           00030E  3433 	C$transmit3copies.c$120$1$339 ==.
                                   3434 ;	..\COMMON\transmit3copies.c:120: msg3_tmr.handler = msg3_callback;
      004F91 90 03 8D         [24] 3435 	mov	dptr,#(_msg3_tmr + 0x0002)
      004F94 74 A1            [12] 3436 	mov	a,#_msg3_callback
      004F96 F0               [24] 3437 	movx	@dptr,a
      004F97 74 4D            [12] 3438 	mov	a,#(_msg3_callback >> 8)
      004F99 A3               [24] 3439 	inc	dptr
      004F9A F0               [24] 3440 	movx	@dptr,a
                           000318  3441 	C$transmit3copies.c$121$1$339 ==.
                                   3442 ;	..\COMMON\transmit3copies.c:121: msg3_tmr.time = time3_wtimer;
      004F9B 90 03 8F         [24] 3443 	mov	dptr,#(_msg3_tmr + 0x0004)
      004F9E E8               [12] 3444 	mov	a,r0
      004F9F F0               [24] 3445 	movx	@dptr,a
      004FA0 E9               [12] 3446 	mov	a,r1
      004FA1 A3               [24] 3447 	inc	dptr
      004FA2 F0               [24] 3448 	movx	@dptr,a
      004FA3 EA               [12] 3449 	mov	a,r2
      004FA4 A3               [24] 3450 	inc	dptr
      004FA5 F0               [24] 3451 	movx	@dptr,a
      004FA6 EB               [12] 3452 	mov	a,r3
      004FA7 A3               [24] 3453 	inc	dptr
      004FA8 F0               [24] 3454 	movx	@dptr,a
                           000326  3455 	C$transmit3copies.c$122$1$339 ==.
                                   3456 ;	..\COMMON\transmit3copies.c:122: wtimer0_addrelative(&msg3_tmr);
      004FA9 90 03 8B         [24] 3457 	mov	dptr,#_msg3_tmr
      004FAC 12 69 E7         [24] 3458 	lcall	_wtimer0_addrelative
                           00032C  3459 	C$transmit3copies.c$128$1$339 ==.
                           00032C  3460 	XG$transmit_copies_wtimer$0$0 ==.
      004FAF 22               [24] 3461 	ret
                                   3462 	.area CSEG    (CODE)
                                   3463 	.area CONST   (CODE)
                           000000  3464 Ftransmit3copies$__str_0$0$0 == .
      008107                       3465 ___str_0:
      008107 36 30 5F 54 58 31 5F  3466 	.ascii "60_TX1_WT0: "
             57 54 30 3A 20
      008113 00                    3467 	.db 0x00
                           00000D  3468 Ftransmit3copies$__str_1$0$0 == .
      008114                       3469 ___str_1:
      008114 36 31 5F 54 58 31 5F  3470 	.ascii "61_TX1_delta: "
             64 65 6C 74 61 3A 20
      008122 00                    3471 	.db 0x00
                           00001C  3472 Ftransmit3copies$__str_2$0$0 == .
      008123                       3473 ___str_2:
      008123 37 30 5F 54 58 32 5F  3474 	.ascii "70_TX2_WT0: "
             57 54 30 3A 20
      00812F 00                    3475 	.db 0x00
                           000029  3476 Ftransmit3copies$__str_3$0$0 == .
      008130                       3477 ___str_3:
      008130 37 31 5F 54 58 32 5F  3478 	.ascii "71_TX2_delta: "
             64 65 6C 74 61 3A 20
      00813E 00                    3479 	.db 0x00
                           000038  3480 Ftransmit3copies$__str_4$0$0 == .
      00813F                       3481 ___str_4:
      00813F 38 30 5F 54 58 33 5F  3482 	.ascii "80_TX3_WT0: "
             57 54 30 3A 20
      00814B 00                    3483 	.db 0x00
                           000045  3484 Ftransmit3copies$__str_5$0$0 == .
      00814C                       3485 ___str_5:
      00814C 38 31 5F 54 58 33 5F  3486 	.ascii "81_TX3_delta: "
             64 65 6C 74 61 3A 20
      00815A 00                    3487 	.db 0x00
                                   3488 	.area XINIT   (CODE)
                           000000  3489 Ftransmit3copies$__xinit_trmID$0$0 == .
      0087F2                       3490 __xinit__trmID:
      0087F2 01 00 10 00           3491 	.byte #0x01,#0x00,#0x10,#0x00	; 1048577
                           000004  3492 Ftransmit3copies$__xinit_StdPrem$0$0 == .
      0087F6                       3493 __xinit__StdPrem:
      0087F6 01                    3494 	.db #0x01	; 1
                                   3495 	.area CABS    (ABS,CODE)
