// libraries
#include "../AX_Radio_Lab_output/configslave.h"
#include <libmftypes.h>
#include <libmfradio.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include "libmfosc.h"
#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#include "../COMMON/display_com0.h"
#include <libminikitleds.h>
#include "../COMMON/misc.h"
#include "../COMMON/configcommon.h"
#include "../COMMON/easyax5043.h"
#include "../COMMON/pktframing.h"

// macros
//#define USE_DBGLINK
// functions

// variables
extern uint8_t __xdata premPacketBuffer[pktLENGTH];
extern uint8_t __xdata premSlot_counter;
extern uint32_t __xdata wt0_beacon;
struct wtimer_desc __xdata premPkt_tmr;


/**
*\brief using wtimer functions to regulate time intervals
*/

void premPkt_callback(struct wtimer_desc __xdata *desc)
{
    desc;
/*#ifdef USE_DBGLINK
        dbglink_writestr("msg1_tx\n");
#endif // USE_DBGLINK*/
    axradio_transmit(&remoteaddr, premPacketBuffer, pktLENGTH);
#ifdef USE_DBGLINK
            dbglink_writestr("6_TX0_WT0: ");
            dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
            dbglink_tx('\n');
            dbglink_writestr("61_TX0_delta: ");
            dbglink_writehex32((wtimer0_curtime()-wt0_beacon), 4, WRNUM_PADZERO);
            dbglink_tx('\n');
#endif // USE_DBGLINK

}

void transmit_prem_wtimer(void)
{
    uint32_t __xdata premSlot_wtimer = 0;

 //   premSlot_wtimer = ((uint8_t)(trmID&0x00000F)+2) * slotPremWIDTH_MS * 0.64;
 // added on 2021-08-30 for timing test
    if (StdPrem == 0x0)
    {
    // added on 2021-08-19 to test premium slots
        if (premSlot_counter == 10)
        {
            premSlot_counter = 0;
        }
        premSlot_wtimer = ((premSlot_counter+slotPremBASE) * slotPremWIDTH_MS) * 0.64;

#ifdef USE_DBGLINK
        dbglink_writestr("62_premSlot:");
        dbglink_writehex16(premSlot_counter, 1, WRNUM_PADZERO);
        dbglink_tx(' ');
        dbglink_writehex32(premSlot_wtimer, 8, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK

        premSlot_counter += 1;
 // added on 2021-08-30 for timing test
    }
    else if (StdPrem == 0x2)
    {
        premSlot_wtimer = (slotPremBASE + nslotPrem - 1) * slotPremWIDTH_MS * 0.64;
    }
 ///////////////////////////////////////////////////


    wtimer0_remove(&premPkt_tmr);
    premPkt_tmr.handler = premPkt_callback;
    premPkt_tmr.time = premSlot_wtimer;
    wtimer0_addrelative(&premPkt_tmr);
}
