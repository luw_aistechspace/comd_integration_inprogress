                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl __sdcc_external_startup
                                     13 	.globl _wait_dbglink_free
                                     14 	.globl _dbglink_display_radio_error
                                     15 	.globl _com0_display_radio_error
                                     16 	.globl _dbglink_received_packet
                                     17 	.globl _memcpy
                                     18 	.globl _com0_writestr
                                     19 	.globl _com0_setpos
                                     20 	.globl _com0_init
                                     21 	.globl _com0_portinit
                                     22 	.globl _dbglink_writehex32
                                     23 	.globl _dbglink_writehex16
                                     24 	.globl _dbglink_writenum16
                                     25 	.globl _dbglink_writestr
                                     26 	.globl _dbglink_tx
                                     27 	.globl _dbglink_init
                                     28 	.globl _dbglink_txidle
                                     29 	.globl _uart0_txidle
                                     30 	.globl _wtimer0_correctinterval
                                     31 	.globl _wtimer0_addrelative
                                     32 	.globl _wtimer0_addabsolute
                                     33 	.globl _wtimer0_curtime
                                     34 	.globl _wtimer_runcallbacks
                                     35 	.globl _wtimer_idle
                                     36 	.globl _wtimer_init
                                     37 	.globl _wtimer1_setconfig
                                     38 	.globl _wtimer0_setconfig
                                     39 	.globl _flash_apply_calibration
                                     40 	.globl _axradio_commsleepexit
                                     41 	.globl _axradio_setup_pincfg2
                                     42 	.globl _axradio_setup_pincfg1
                                     43 	.globl _axradio_transmit
                                     44 	.globl _axradio_set_default_remote_address
                                     45 	.globl _axradio_set_local_address
                                     46 	.globl _axradio_get_pllvcoi
                                     47 	.globl _axradio_get_pllrange
                                     48 	.globl _axradio_set_mode
                                     49 	.globl _axradio_cansleep
                                     50 	.globl _axradio_init
                                     51 	.globl _PORTC_7
                                     52 	.globl _PORTC_6
                                     53 	.globl _PORTC_5
                                     54 	.globl _PORTC_4
                                     55 	.globl _PORTC_3
                                     56 	.globl _PORTC_2
                                     57 	.globl _PORTC_1
                                     58 	.globl _PORTC_0
                                     59 	.globl _PORTB_7
                                     60 	.globl _PORTB_6
                                     61 	.globl _PORTB_5
                                     62 	.globl _PORTB_4
                                     63 	.globl _PORTB_3
                                     64 	.globl _PORTB_2
                                     65 	.globl _PORTB_1
                                     66 	.globl _PORTB_0
                                     67 	.globl _PORTA_7
                                     68 	.globl _PORTA_6
                                     69 	.globl _PORTA_5
                                     70 	.globl _PORTA_4
                                     71 	.globl _PORTA_3
                                     72 	.globl _PORTA_2
                                     73 	.globl _PORTA_1
                                     74 	.globl _PORTA_0
                                     75 	.globl _PINC_7
                                     76 	.globl _PINC_6
                                     77 	.globl _PINC_5
                                     78 	.globl _PINC_4
                                     79 	.globl _PINC_3
                                     80 	.globl _PINC_2
                                     81 	.globl _PINC_1
                                     82 	.globl _PINC_0
                                     83 	.globl _PINB_7
                                     84 	.globl _PINB_6
                                     85 	.globl _PINB_5
                                     86 	.globl _PINB_4
                                     87 	.globl _PINB_3
                                     88 	.globl _PINB_2
                                     89 	.globl _PINB_1
                                     90 	.globl _PINB_0
                                     91 	.globl _PINA_7
                                     92 	.globl _PINA_6
                                     93 	.globl _PINA_5
                                     94 	.globl _PINA_4
                                     95 	.globl _PINA_3
                                     96 	.globl _PINA_2
                                     97 	.globl _PINA_1
                                     98 	.globl _PINA_0
                                     99 	.globl _CY
                                    100 	.globl _AC
                                    101 	.globl _F0
                                    102 	.globl _RS1
                                    103 	.globl _RS0
                                    104 	.globl _OV
                                    105 	.globl _F1
                                    106 	.globl _P
                                    107 	.globl _IP_7
                                    108 	.globl _IP_6
                                    109 	.globl _IP_5
                                    110 	.globl _IP_4
                                    111 	.globl _IP_3
                                    112 	.globl _IP_2
                                    113 	.globl _IP_1
                                    114 	.globl _IP_0
                                    115 	.globl _EA
                                    116 	.globl _IE_7
                                    117 	.globl _IE_6
                                    118 	.globl _IE_5
                                    119 	.globl _IE_4
                                    120 	.globl _IE_3
                                    121 	.globl _IE_2
                                    122 	.globl _IE_1
                                    123 	.globl _IE_0
                                    124 	.globl _EIP_7
                                    125 	.globl _EIP_6
                                    126 	.globl _EIP_5
                                    127 	.globl _EIP_4
                                    128 	.globl _EIP_3
                                    129 	.globl _EIP_2
                                    130 	.globl _EIP_1
                                    131 	.globl _EIP_0
                                    132 	.globl _EIE_7
                                    133 	.globl _EIE_6
                                    134 	.globl _EIE_5
                                    135 	.globl _EIE_4
                                    136 	.globl _EIE_3
                                    137 	.globl _EIE_2
                                    138 	.globl _EIE_1
                                    139 	.globl _EIE_0
                                    140 	.globl _E2IP_7
                                    141 	.globl _E2IP_6
                                    142 	.globl _E2IP_5
                                    143 	.globl _E2IP_4
                                    144 	.globl _E2IP_3
                                    145 	.globl _E2IP_2
                                    146 	.globl _E2IP_1
                                    147 	.globl _E2IP_0
                                    148 	.globl _E2IE_7
                                    149 	.globl _E2IE_6
                                    150 	.globl _E2IE_5
                                    151 	.globl _E2IE_4
                                    152 	.globl _E2IE_3
                                    153 	.globl _E2IE_2
                                    154 	.globl _E2IE_1
                                    155 	.globl _E2IE_0
                                    156 	.globl _B_7
                                    157 	.globl _B_6
                                    158 	.globl _B_5
                                    159 	.globl _B_4
                                    160 	.globl _B_3
                                    161 	.globl _B_2
                                    162 	.globl _B_1
                                    163 	.globl _B_0
                                    164 	.globl _ACC_7
                                    165 	.globl _ACC_6
                                    166 	.globl _ACC_5
                                    167 	.globl _ACC_4
                                    168 	.globl _ACC_3
                                    169 	.globl _ACC_2
                                    170 	.globl _ACC_1
                                    171 	.globl _ACC_0
                                    172 	.globl _WTSTAT
                                    173 	.globl _WTIRQEN
                                    174 	.globl _WTEVTD
                                    175 	.globl _WTEVTD1
                                    176 	.globl _WTEVTD0
                                    177 	.globl _WTEVTC
                                    178 	.globl _WTEVTC1
                                    179 	.globl _WTEVTC0
                                    180 	.globl _WTEVTB
                                    181 	.globl _WTEVTB1
                                    182 	.globl _WTEVTB0
                                    183 	.globl _WTEVTA
                                    184 	.globl _WTEVTA1
                                    185 	.globl _WTEVTA0
                                    186 	.globl _WTCNTR1
                                    187 	.globl _WTCNTB
                                    188 	.globl _WTCNTB1
                                    189 	.globl _WTCNTB0
                                    190 	.globl _WTCNTA
                                    191 	.globl _WTCNTA1
                                    192 	.globl _WTCNTA0
                                    193 	.globl _WTCFGB
                                    194 	.globl _WTCFGA
                                    195 	.globl _WDTRESET
                                    196 	.globl _WDTCFG
                                    197 	.globl _U1STATUS
                                    198 	.globl _U1SHREG
                                    199 	.globl _U1MODE
                                    200 	.globl _U1CTRL
                                    201 	.globl _U0STATUS
                                    202 	.globl _U0SHREG
                                    203 	.globl _U0MODE
                                    204 	.globl _U0CTRL
                                    205 	.globl _T2STATUS
                                    206 	.globl _T2PERIOD
                                    207 	.globl _T2PERIOD1
                                    208 	.globl _T2PERIOD0
                                    209 	.globl _T2MODE
                                    210 	.globl _T2CNT
                                    211 	.globl _T2CNT1
                                    212 	.globl _T2CNT0
                                    213 	.globl _T2CLKSRC
                                    214 	.globl _T1STATUS
                                    215 	.globl _T1PERIOD
                                    216 	.globl _T1PERIOD1
                                    217 	.globl _T1PERIOD0
                                    218 	.globl _T1MODE
                                    219 	.globl _T1CNT
                                    220 	.globl _T1CNT1
                                    221 	.globl _T1CNT0
                                    222 	.globl _T1CLKSRC
                                    223 	.globl _T0STATUS
                                    224 	.globl _T0PERIOD
                                    225 	.globl _T0PERIOD1
                                    226 	.globl _T0PERIOD0
                                    227 	.globl _T0MODE
                                    228 	.globl _T0CNT
                                    229 	.globl _T0CNT1
                                    230 	.globl _T0CNT0
                                    231 	.globl _T0CLKSRC
                                    232 	.globl _SPSTATUS
                                    233 	.globl _SPSHREG
                                    234 	.globl _SPMODE
                                    235 	.globl _SPCLKSRC
                                    236 	.globl _RADIOSTAT
                                    237 	.globl _RADIOSTAT1
                                    238 	.globl _RADIOSTAT0
                                    239 	.globl _RADIODATA
                                    240 	.globl _RADIODATA3
                                    241 	.globl _RADIODATA2
                                    242 	.globl _RADIODATA1
                                    243 	.globl _RADIODATA0
                                    244 	.globl _RADIOADDR
                                    245 	.globl _RADIOADDR1
                                    246 	.globl _RADIOADDR0
                                    247 	.globl _RADIOACC
                                    248 	.globl _OC1STATUS
                                    249 	.globl _OC1PIN
                                    250 	.globl _OC1MODE
                                    251 	.globl _OC1COMP
                                    252 	.globl _OC1COMP1
                                    253 	.globl _OC1COMP0
                                    254 	.globl _OC0STATUS
                                    255 	.globl _OC0PIN
                                    256 	.globl _OC0MODE
                                    257 	.globl _OC0COMP
                                    258 	.globl _OC0COMP1
                                    259 	.globl _OC0COMP0
                                    260 	.globl _NVSTATUS
                                    261 	.globl _NVKEY
                                    262 	.globl _NVDATA
                                    263 	.globl _NVDATA1
                                    264 	.globl _NVDATA0
                                    265 	.globl _NVADDR
                                    266 	.globl _NVADDR1
                                    267 	.globl _NVADDR0
                                    268 	.globl _IC1STATUS
                                    269 	.globl _IC1MODE
                                    270 	.globl _IC1CAPT
                                    271 	.globl _IC1CAPT1
                                    272 	.globl _IC1CAPT0
                                    273 	.globl _IC0STATUS
                                    274 	.globl _IC0MODE
                                    275 	.globl _IC0CAPT
                                    276 	.globl _IC0CAPT1
                                    277 	.globl _IC0CAPT0
                                    278 	.globl _PORTR
                                    279 	.globl _PORTC
                                    280 	.globl _PORTB
                                    281 	.globl _PORTA
                                    282 	.globl _PINR
                                    283 	.globl _PINC
                                    284 	.globl _PINB
                                    285 	.globl _PINA
                                    286 	.globl _DIRR
                                    287 	.globl _DIRC
                                    288 	.globl _DIRB
                                    289 	.globl _DIRA
                                    290 	.globl _DBGLNKSTAT
                                    291 	.globl _DBGLNKBUF
                                    292 	.globl _CODECONFIG
                                    293 	.globl _CLKSTAT
                                    294 	.globl _CLKCON
                                    295 	.globl _ANALOGCOMP
                                    296 	.globl _ADCCONV
                                    297 	.globl _ADCCLKSRC
                                    298 	.globl _ADCCH3CONFIG
                                    299 	.globl _ADCCH2CONFIG
                                    300 	.globl _ADCCH1CONFIG
                                    301 	.globl _ADCCH0CONFIG
                                    302 	.globl __XPAGE
                                    303 	.globl _XPAGE
                                    304 	.globl _SP
                                    305 	.globl _PSW
                                    306 	.globl _PCON
                                    307 	.globl _IP
                                    308 	.globl _IE
                                    309 	.globl _EIP
                                    310 	.globl _EIE
                                    311 	.globl _E2IP
                                    312 	.globl _E2IE
                                    313 	.globl _DPS
                                    314 	.globl _DPTR1
                                    315 	.globl _DPTR0
                                    316 	.globl _DPL1
                                    317 	.globl _DPL
                                    318 	.globl _DPH1
                                    319 	.globl _DPH
                                    320 	.globl _B
                                    321 	.globl _ACC
                                    322 	.globl _wt0_beacon
                                    323 	.globl _rcv_flg_id3
                                    324 	.globl _rcv_flg_id1
                                    325 	.globl _pkts_id3
                                    326 	.globl _pkts_id1
                                    327 	.globl _rcv_flg
                                    328 	.globl _bc_counter
                                    329 	.globl _pkts_missing
                                    330 	.globl _pkts_received
                                    331 	.globl _ack_packet
                                    332 	.globl _beacon_packet
                                    333 	.globl _wakeup_desc
                                    334 	.globl _XTALREADY
                                    335 	.globl _XTALOSC
                                    336 	.globl _XTALAMPL
                                    337 	.globl _SILICONREV
                                    338 	.globl _SCRATCH3
                                    339 	.globl _SCRATCH2
                                    340 	.globl _SCRATCH1
                                    341 	.globl _SCRATCH0
                                    342 	.globl _RADIOMUX
                                    343 	.globl _RADIOFSTATADDR
                                    344 	.globl _RADIOFSTATADDR1
                                    345 	.globl _RADIOFSTATADDR0
                                    346 	.globl _RADIOFDATAADDR
                                    347 	.globl _RADIOFDATAADDR1
                                    348 	.globl _RADIOFDATAADDR0
                                    349 	.globl _OSCRUN
                                    350 	.globl _OSCREADY
                                    351 	.globl _OSCFORCERUN
                                    352 	.globl _OSCCALIB
                                    353 	.globl _MISCCTRL
                                    354 	.globl _LPXOSCGM
                                    355 	.globl _LPOSCREF
                                    356 	.globl _LPOSCREF1
                                    357 	.globl _LPOSCREF0
                                    358 	.globl _LPOSCPER
                                    359 	.globl _LPOSCPER1
                                    360 	.globl _LPOSCPER0
                                    361 	.globl _LPOSCKFILT
                                    362 	.globl _LPOSCKFILT1
                                    363 	.globl _LPOSCKFILT0
                                    364 	.globl _LPOSCFREQ
                                    365 	.globl _LPOSCFREQ1
                                    366 	.globl _LPOSCFREQ0
                                    367 	.globl _LPOSCCONFIG
                                    368 	.globl _PINSEL
                                    369 	.globl _PINCHGC
                                    370 	.globl _PINCHGB
                                    371 	.globl _PINCHGA
                                    372 	.globl _PALTRADIO
                                    373 	.globl _PALTC
                                    374 	.globl _PALTB
                                    375 	.globl _PALTA
                                    376 	.globl _INTCHGC
                                    377 	.globl _INTCHGB
                                    378 	.globl _INTCHGA
                                    379 	.globl _EXTIRQ
                                    380 	.globl _GPIOENABLE
                                    381 	.globl _ANALOGA
                                    382 	.globl _FRCOSCREF
                                    383 	.globl _FRCOSCREF1
                                    384 	.globl _FRCOSCREF0
                                    385 	.globl _FRCOSCPER
                                    386 	.globl _FRCOSCPER1
                                    387 	.globl _FRCOSCPER0
                                    388 	.globl _FRCOSCKFILT
                                    389 	.globl _FRCOSCKFILT1
                                    390 	.globl _FRCOSCKFILT0
                                    391 	.globl _FRCOSCFREQ
                                    392 	.globl _FRCOSCFREQ1
                                    393 	.globl _FRCOSCFREQ0
                                    394 	.globl _FRCOSCCTRL
                                    395 	.globl _FRCOSCCONFIG
                                    396 	.globl _DMA1CONFIG
                                    397 	.globl _DMA1ADDR
                                    398 	.globl _DMA1ADDR1
                                    399 	.globl _DMA1ADDR0
                                    400 	.globl _DMA0CONFIG
                                    401 	.globl _DMA0ADDR
                                    402 	.globl _DMA0ADDR1
                                    403 	.globl _DMA0ADDR0
                                    404 	.globl _ADCTUNE2
                                    405 	.globl _ADCTUNE1
                                    406 	.globl _ADCTUNE0
                                    407 	.globl _ADCCH3VAL
                                    408 	.globl _ADCCH3VAL1
                                    409 	.globl _ADCCH3VAL0
                                    410 	.globl _ADCCH2VAL
                                    411 	.globl _ADCCH2VAL1
                                    412 	.globl _ADCCH2VAL0
                                    413 	.globl _ADCCH1VAL
                                    414 	.globl _ADCCH1VAL1
                                    415 	.globl _ADCCH1VAL0
                                    416 	.globl _ADCCH0VAL
                                    417 	.globl _ADCCH0VAL1
                                    418 	.globl _ADCCH0VAL0
                                    419 	.globl _coldstart
                                    420 	.globl _pkt_counter
                                    421 	.globl _axradio_statuschange
                                    422 	.globl _enable_radio_interrupt_in_mcu_pin
                                    423 	.globl _disable_radio_interrupt_in_mcu_pin
                                    424 ;--------------------------------------------------------
                                    425 ; special function registers
                                    426 ;--------------------------------------------------------
                                    427 	.area RSEG    (ABS,DATA)
      000000                        428 	.org 0x0000
                           0000E0   429 G$ACC$0$0 == 0x00e0
                           0000E0   430 _ACC	=	0x00e0
                           0000F0   431 G$B$0$0 == 0x00f0
                           0000F0   432 _B	=	0x00f0
                           000083   433 G$DPH$0$0 == 0x0083
                           000083   434 _DPH	=	0x0083
                           000085   435 G$DPH1$0$0 == 0x0085
                           000085   436 _DPH1	=	0x0085
                           000082   437 G$DPL$0$0 == 0x0082
                           000082   438 _DPL	=	0x0082
                           000084   439 G$DPL1$0$0 == 0x0084
                           000084   440 _DPL1	=	0x0084
                           008382   441 G$DPTR0$0$0 == 0x8382
                           008382   442 _DPTR0	=	0x8382
                           008584   443 G$DPTR1$0$0 == 0x8584
                           008584   444 _DPTR1	=	0x8584
                           000086   445 G$DPS$0$0 == 0x0086
                           000086   446 _DPS	=	0x0086
                           0000A0   447 G$E2IE$0$0 == 0x00a0
                           0000A0   448 _E2IE	=	0x00a0
                           0000C0   449 G$E2IP$0$0 == 0x00c0
                           0000C0   450 _E2IP	=	0x00c0
                           000098   451 G$EIE$0$0 == 0x0098
                           000098   452 _EIE	=	0x0098
                           0000B0   453 G$EIP$0$0 == 0x00b0
                           0000B0   454 _EIP	=	0x00b0
                           0000A8   455 G$IE$0$0 == 0x00a8
                           0000A8   456 _IE	=	0x00a8
                           0000B8   457 G$IP$0$0 == 0x00b8
                           0000B8   458 _IP	=	0x00b8
                           000087   459 G$PCON$0$0 == 0x0087
                           000087   460 _PCON	=	0x0087
                           0000D0   461 G$PSW$0$0 == 0x00d0
                           0000D0   462 _PSW	=	0x00d0
                           000081   463 G$SP$0$0 == 0x0081
                           000081   464 _SP	=	0x0081
                           0000D9   465 G$XPAGE$0$0 == 0x00d9
                           0000D9   466 _XPAGE	=	0x00d9
                           0000D9   467 G$_XPAGE$0$0 == 0x00d9
                           0000D9   468 __XPAGE	=	0x00d9
                           0000CA   469 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   470 _ADCCH0CONFIG	=	0x00ca
                           0000CB   471 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   472 _ADCCH1CONFIG	=	0x00cb
                           0000D2   473 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   474 _ADCCH2CONFIG	=	0x00d2
                           0000D3   475 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   476 _ADCCH3CONFIG	=	0x00d3
                           0000D1   477 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   478 _ADCCLKSRC	=	0x00d1
                           0000C9   479 G$ADCCONV$0$0 == 0x00c9
                           0000C9   480 _ADCCONV	=	0x00c9
                           0000E1   481 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   482 _ANALOGCOMP	=	0x00e1
                           0000C6   483 G$CLKCON$0$0 == 0x00c6
                           0000C6   484 _CLKCON	=	0x00c6
                           0000C7   485 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   486 _CLKSTAT	=	0x00c7
                           000097   487 G$CODECONFIG$0$0 == 0x0097
                           000097   488 _CODECONFIG	=	0x0097
                           0000E3   489 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   490 _DBGLNKBUF	=	0x00e3
                           0000E2   491 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   492 _DBGLNKSTAT	=	0x00e2
                           000089   493 G$DIRA$0$0 == 0x0089
                           000089   494 _DIRA	=	0x0089
                           00008A   495 G$DIRB$0$0 == 0x008a
                           00008A   496 _DIRB	=	0x008a
                           00008B   497 G$DIRC$0$0 == 0x008b
                           00008B   498 _DIRC	=	0x008b
                           00008E   499 G$DIRR$0$0 == 0x008e
                           00008E   500 _DIRR	=	0x008e
                           0000C8   501 G$PINA$0$0 == 0x00c8
                           0000C8   502 _PINA	=	0x00c8
                           0000E8   503 G$PINB$0$0 == 0x00e8
                           0000E8   504 _PINB	=	0x00e8
                           0000F8   505 G$PINC$0$0 == 0x00f8
                           0000F8   506 _PINC	=	0x00f8
                           00008D   507 G$PINR$0$0 == 0x008d
                           00008D   508 _PINR	=	0x008d
                           000080   509 G$PORTA$0$0 == 0x0080
                           000080   510 _PORTA	=	0x0080
                           000088   511 G$PORTB$0$0 == 0x0088
                           000088   512 _PORTB	=	0x0088
                           000090   513 G$PORTC$0$0 == 0x0090
                           000090   514 _PORTC	=	0x0090
                           00008C   515 G$PORTR$0$0 == 0x008c
                           00008C   516 _PORTR	=	0x008c
                           0000CE   517 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   518 _IC0CAPT0	=	0x00ce
                           0000CF   519 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   520 _IC0CAPT1	=	0x00cf
                           00CFCE   521 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   522 _IC0CAPT	=	0xcfce
                           0000CC   523 G$IC0MODE$0$0 == 0x00cc
                           0000CC   524 _IC0MODE	=	0x00cc
                           0000CD   525 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   526 _IC0STATUS	=	0x00cd
                           0000D6   527 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   528 _IC1CAPT0	=	0x00d6
                           0000D7   529 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   530 _IC1CAPT1	=	0x00d7
                           00D7D6   531 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   532 _IC1CAPT	=	0xd7d6
                           0000D4   533 G$IC1MODE$0$0 == 0x00d4
                           0000D4   534 _IC1MODE	=	0x00d4
                           0000D5   535 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   536 _IC1STATUS	=	0x00d5
                           000092   537 G$NVADDR0$0$0 == 0x0092
                           000092   538 _NVADDR0	=	0x0092
                           000093   539 G$NVADDR1$0$0 == 0x0093
                           000093   540 _NVADDR1	=	0x0093
                           009392   541 G$NVADDR$0$0 == 0x9392
                           009392   542 _NVADDR	=	0x9392
                           000094   543 G$NVDATA0$0$0 == 0x0094
                           000094   544 _NVDATA0	=	0x0094
                           000095   545 G$NVDATA1$0$0 == 0x0095
                           000095   546 _NVDATA1	=	0x0095
                           009594   547 G$NVDATA$0$0 == 0x9594
                           009594   548 _NVDATA	=	0x9594
                           000096   549 G$NVKEY$0$0 == 0x0096
                           000096   550 _NVKEY	=	0x0096
                           000091   551 G$NVSTATUS$0$0 == 0x0091
                           000091   552 _NVSTATUS	=	0x0091
                           0000BC   553 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   554 _OC0COMP0	=	0x00bc
                           0000BD   555 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   556 _OC0COMP1	=	0x00bd
                           00BDBC   557 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   558 _OC0COMP	=	0xbdbc
                           0000B9   559 G$OC0MODE$0$0 == 0x00b9
                           0000B9   560 _OC0MODE	=	0x00b9
                           0000BA   561 G$OC0PIN$0$0 == 0x00ba
                           0000BA   562 _OC0PIN	=	0x00ba
                           0000BB   563 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   564 _OC0STATUS	=	0x00bb
                           0000C4   565 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   566 _OC1COMP0	=	0x00c4
                           0000C5   567 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   568 _OC1COMP1	=	0x00c5
                           00C5C4   569 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   570 _OC1COMP	=	0xc5c4
                           0000C1   571 G$OC1MODE$0$0 == 0x00c1
                           0000C1   572 _OC1MODE	=	0x00c1
                           0000C2   573 G$OC1PIN$0$0 == 0x00c2
                           0000C2   574 _OC1PIN	=	0x00c2
                           0000C3   575 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   576 _OC1STATUS	=	0x00c3
                           0000B1   577 G$RADIOACC$0$0 == 0x00b1
                           0000B1   578 _RADIOACC	=	0x00b1
                           0000B3   579 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   580 _RADIOADDR0	=	0x00b3
                           0000B2   581 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   582 _RADIOADDR1	=	0x00b2
                           00B2B3   583 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   584 _RADIOADDR	=	0xb2b3
                           0000B7   585 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   586 _RADIODATA0	=	0x00b7
                           0000B6   587 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   588 _RADIODATA1	=	0x00b6
                           0000B5   589 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   590 _RADIODATA2	=	0x00b5
                           0000B4   591 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   592 _RADIODATA3	=	0x00b4
                           B4B5B6B7   593 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   594 _RADIODATA	=	0xb4b5b6b7
                           0000BE   595 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   596 _RADIOSTAT0	=	0x00be
                           0000BF   597 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   598 _RADIOSTAT1	=	0x00bf
                           00BFBE   599 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   600 _RADIOSTAT	=	0xbfbe
                           0000DF   601 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   602 _SPCLKSRC	=	0x00df
                           0000DC   603 G$SPMODE$0$0 == 0x00dc
                           0000DC   604 _SPMODE	=	0x00dc
                           0000DE   605 G$SPSHREG$0$0 == 0x00de
                           0000DE   606 _SPSHREG	=	0x00de
                           0000DD   607 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   608 _SPSTATUS	=	0x00dd
                           00009A   609 G$T0CLKSRC$0$0 == 0x009a
                           00009A   610 _T0CLKSRC	=	0x009a
                           00009C   611 G$T0CNT0$0$0 == 0x009c
                           00009C   612 _T0CNT0	=	0x009c
                           00009D   613 G$T0CNT1$0$0 == 0x009d
                           00009D   614 _T0CNT1	=	0x009d
                           009D9C   615 G$T0CNT$0$0 == 0x9d9c
                           009D9C   616 _T0CNT	=	0x9d9c
                           000099   617 G$T0MODE$0$0 == 0x0099
                           000099   618 _T0MODE	=	0x0099
                           00009E   619 G$T0PERIOD0$0$0 == 0x009e
                           00009E   620 _T0PERIOD0	=	0x009e
                           00009F   621 G$T0PERIOD1$0$0 == 0x009f
                           00009F   622 _T0PERIOD1	=	0x009f
                           009F9E   623 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   624 _T0PERIOD	=	0x9f9e
                           00009B   625 G$T0STATUS$0$0 == 0x009b
                           00009B   626 _T0STATUS	=	0x009b
                           0000A2   627 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   628 _T1CLKSRC	=	0x00a2
                           0000A4   629 G$T1CNT0$0$0 == 0x00a4
                           0000A4   630 _T1CNT0	=	0x00a4
                           0000A5   631 G$T1CNT1$0$0 == 0x00a5
                           0000A5   632 _T1CNT1	=	0x00a5
                           00A5A4   633 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   634 _T1CNT	=	0xa5a4
                           0000A1   635 G$T1MODE$0$0 == 0x00a1
                           0000A1   636 _T1MODE	=	0x00a1
                           0000A6   637 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   638 _T1PERIOD0	=	0x00a6
                           0000A7   639 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   640 _T1PERIOD1	=	0x00a7
                           00A7A6   641 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   642 _T1PERIOD	=	0xa7a6
                           0000A3   643 G$T1STATUS$0$0 == 0x00a3
                           0000A3   644 _T1STATUS	=	0x00a3
                           0000AA   645 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   646 _T2CLKSRC	=	0x00aa
                           0000AC   647 G$T2CNT0$0$0 == 0x00ac
                           0000AC   648 _T2CNT0	=	0x00ac
                           0000AD   649 G$T2CNT1$0$0 == 0x00ad
                           0000AD   650 _T2CNT1	=	0x00ad
                           00ADAC   651 G$T2CNT$0$0 == 0xadac
                           00ADAC   652 _T2CNT	=	0xadac
                           0000A9   653 G$T2MODE$0$0 == 0x00a9
                           0000A9   654 _T2MODE	=	0x00a9
                           0000AE   655 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   656 _T2PERIOD0	=	0x00ae
                           0000AF   657 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   658 _T2PERIOD1	=	0x00af
                           00AFAE   659 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   660 _T2PERIOD	=	0xafae
                           0000AB   661 G$T2STATUS$0$0 == 0x00ab
                           0000AB   662 _T2STATUS	=	0x00ab
                           0000E4   663 G$U0CTRL$0$0 == 0x00e4
                           0000E4   664 _U0CTRL	=	0x00e4
                           0000E7   665 G$U0MODE$0$0 == 0x00e7
                           0000E7   666 _U0MODE	=	0x00e7
                           0000E6   667 G$U0SHREG$0$0 == 0x00e6
                           0000E6   668 _U0SHREG	=	0x00e6
                           0000E5   669 G$U0STATUS$0$0 == 0x00e5
                           0000E5   670 _U0STATUS	=	0x00e5
                           0000EC   671 G$U1CTRL$0$0 == 0x00ec
                           0000EC   672 _U1CTRL	=	0x00ec
                           0000EF   673 G$U1MODE$0$0 == 0x00ef
                           0000EF   674 _U1MODE	=	0x00ef
                           0000EE   675 G$U1SHREG$0$0 == 0x00ee
                           0000EE   676 _U1SHREG	=	0x00ee
                           0000ED   677 G$U1STATUS$0$0 == 0x00ed
                           0000ED   678 _U1STATUS	=	0x00ed
                           0000DA   679 G$WDTCFG$0$0 == 0x00da
                           0000DA   680 _WDTCFG	=	0x00da
                           0000DB   681 G$WDTRESET$0$0 == 0x00db
                           0000DB   682 _WDTRESET	=	0x00db
                           0000F1   683 G$WTCFGA$0$0 == 0x00f1
                           0000F1   684 _WTCFGA	=	0x00f1
                           0000F9   685 G$WTCFGB$0$0 == 0x00f9
                           0000F9   686 _WTCFGB	=	0x00f9
                           0000F2   687 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   688 _WTCNTA0	=	0x00f2
                           0000F3   689 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   690 _WTCNTA1	=	0x00f3
                           00F3F2   691 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   692 _WTCNTA	=	0xf3f2
                           0000FA   693 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   694 _WTCNTB0	=	0x00fa
                           0000FB   695 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   696 _WTCNTB1	=	0x00fb
                           00FBFA   697 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   698 _WTCNTB	=	0xfbfa
                           0000EB   699 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   700 _WTCNTR1	=	0x00eb
                           0000F4   701 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   702 _WTEVTA0	=	0x00f4
                           0000F5   703 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   704 _WTEVTA1	=	0x00f5
                           00F5F4   705 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   706 _WTEVTA	=	0xf5f4
                           0000F6   707 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   708 _WTEVTB0	=	0x00f6
                           0000F7   709 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   710 _WTEVTB1	=	0x00f7
                           00F7F6   711 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   712 _WTEVTB	=	0xf7f6
                           0000FC   713 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   714 _WTEVTC0	=	0x00fc
                           0000FD   715 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   716 _WTEVTC1	=	0x00fd
                           00FDFC   717 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   718 _WTEVTC	=	0xfdfc
                           0000FE   719 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   720 _WTEVTD0	=	0x00fe
                           0000FF   721 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   722 _WTEVTD1	=	0x00ff
                           00FFFE   723 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   724 _WTEVTD	=	0xfffe
                           0000E9   725 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   726 _WTIRQEN	=	0x00e9
                           0000EA   727 G$WTSTAT$0$0 == 0x00ea
                           0000EA   728 _WTSTAT	=	0x00ea
                                    729 ;--------------------------------------------------------
                                    730 ; special function bits
                                    731 ;--------------------------------------------------------
                                    732 	.area RSEG    (ABS,DATA)
      000000                        733 	.org 0x0000
                           0000E0   734 G$ACC_0$0$0 == 0x00e0
                           0000E0   735 _ACC_0	=	0x00e0
                           0000E1   736 G$ACC_1$0$0 == 0x00e1
                           0000E1   737 _ACC_1	=	0x00e1
                           0000E2   738 G$ACC_2$0$0 == 0x00e2
                           0000E2   739 _ACC_2	=	0x00e2
                           0000E3   740 G$ACC_3$0$0 == 0x00e3
                           0000E3   741 _ACC_3	=	0x00e3
                           0000E4   742 G$ACC_4$0$0 == 0x00e4
                           0000E4   743 _ACC_4	=	0x00e4
                           0000E5   744 G$ACC_5$0$0 == 0x00e5
                           0000E5   745 _ACC_5	=	0x00e5
                           0000E6   746 G$ACC_6$0$0 == 0x00e6
                           0000E6   747 _ACC_6	=	0x00e6
                           0000E7   748 G$ACC_7$0$0 == 0x00e7
                           0000E7   749 _ACC_7	=	0x00e7
                           0000F0   750 G$B_0$0$0 == 0x00f0
                           0000F0   751 _B_0	=	0x00f0
                           0000F1   752 G$B_1$0$0 == 0x00f1
                           0000F1   753 _B_1	=	0x00f1
                           0000F2   754 G$B_2$0$0 == 0x00f2
                           0000F2   755 _B_2	=	0x00f2
                           0000F3   756 G$B_3$0$0 == 0x00f3
                           0000F3   757 _B_3	=	0x00f3
                           0000F4   758 G$B_4$0$0 == 0x00f4
                           0000F4   759 _B_4	=	0x00f4
                           0000F5   760 G$B_5$0$0 == 0x00f5
                           0000F5   761 _B_5	=	0x00f5
                           0000F6   762 G$B_6$0$0 == 0x00f6
                           0000F6   763 _B_6	=	0x00f6
                           0000F7   764 G$B_7$0$0 == 0x00f7
                           0000F7   765 _B_7	=	0x00f7
                           0000A0   766 G$E2IE_0$0$0 == 0x00a0
                           0000A0   767 _E2IE_0	=	0x00a0
                           0000A1   768 G$E2IE_1$0$0 == 0x00a1
                           0000A1   769 _E2IE_1	=	0x00a1
                           0000A2   770 G$E2IE_2$0$0 == 0x00a2
                           0000A2   771 _E2IE_2	=	0x00a2
                           0000A3   772 G$E2IE_3$0$0 == 0x00a3
                           0000A3   773 _E2IE_3	=	0x00a3
                           0000A4   774 G$E2IE_4$0$0 == 0x00a4
                           0000A4   775 _E2IE_4	=	0x00a4
                           0000A5   776 G$E2IE_5$0$0 == 0x00a5
                           0000A5   777 _E2IE_5	=	0x00a5
                           0000A6   778 G$E2IE_6$0$0 == 0x00a6
                           0000A6   779 _E2IE_6	=	0x00a6
                           0000A7   780 G$E2IE_7$0$0 == 0x00a7
                           0000A7   781 _E2IE_7	=	0x00a7
                           0000C0   782 G$E2IP_0$0$0 == 0x00c0
                           0000C0   783 _E2IP_0	=	0x00c0
                           0000C1   784 G$E2IP_1$0$0 == 0x00c1
                           0000C1   785 _E2IP_1	=	0x00c1
                           0000C2   786 G$E2IP_2$0$0 == 0x00c2
                           0000C2   787 _E2IP_2	=	0x00c2
                           0000C3   788 G$E2IP_3$0$0 == 0x00c3
                           0000C3   789 _E2IP_3	=	0x00c3
                           0000C4   790 G$E2IP_4$0$0 == 0x00c4
                           0000C4   791 _E2IP_4	=	0x00c4
                           0000C5   792 G$E2IP_5$0$0 == 0x00c5
                           0000C5   793 _E2IP_5	=	0x00c5
                           0000C6   794 G$E2IP_6$0$0 == 0x00c6
                           0000C6   795 _E2IP_6	=	0x00c6
                           0000C7   796 G$E2IP_7$0$0 == 0x00c7
                           0000C7   797 _E2IP_7	=	0x00c7
                           000098   798 G$EIE_0$0$0 == 0x0098
                           000098   799 _EIE_0	=	0x0098
                           000099   800 G$EIE_1$0$0 == 0x0099
                           000099   801 _EIE_1	=	0x0099
                           00009A   802 G$EIE_2$0$0 == 0x009a
                           00009A   803 _EIE_2	=	0x009a
                           00009B   804 G$EIE_3$0$0 == 0x009b
                           00009B   805 _EIE_3	=	0x009b
                           00009C   806 G$EIE_4$0$0 == 0x009c
                           00009C   807 _EIE_4	=	0x009c
                           00009D   808 G$EIE_5$0$0 == 0x009d
                           00009D   809 _EIE_5	=	0x009d
                           00009E   810 G$EIE_6$0$0 == 0x009e
                           00009E   811 _EIE_6	=	0x009e
                           00009F   812 G$EIE_7$0$0 == 0x009f
                           00009F   813 _EIE_7	=	0x009f
                           0000B0   814 G$EIP_0$0$0 == 0x00b0
                           0000B0   815 _EIP_0	=	0x00b0
                           0000B1   816 G$EIP_1$0$0 == 0x00b1
                           0000B1   817 _EIP_1	=	0x00b1
                           0000B2   818 G$EIP_2$0$0 == 0x00b2
                           0000B2   819 _EIP_2	=	0x00b2
                           0000B3   820 G$EIP_3$0$0 == 0x00b3
                           0000B3   821 _EIP_3	=	0x00b3
                           0000B4   822 G$EIP_4$0$0 == 0x00b4
                           0000B4   823 _EIP_4	=	0x00b4
                           0000B5   824 G$EIP_5$0$0 == 0x00b5
                           0000B5   825 _EIP_5	=	0x00b5
                           0000B6   826 G$EIP_6$0$0 == 0x00b6
                           0000B6   827 _EIP_6	=	0x00b6
                           0000B7   828 G$EIP_7$0$0 == 0x00b7
                           0000B7   829 _EIP_7	=	0x00b7
                           0000A8   830 G$IE_0$0$0 == 0x00a8
                           0000A8   831 _IE_0	=	0x00a8
                           0000A9   832 G$IE_1$0$0 == 0x00a9
                           0000A9   833 _IE_1	=	0x00a9
                           0000AA   834 G$IE_2$0$0 == 0x00aa
                           0000AA   835 _IE_2	=	0x00aa
                           0000AB   836 G$IE_3$0$0 == 0x00ab
                           0000AB   837 _IE_3	=	0x00ab
                           0000AC   838 G$IE_4$0$0 == 0x00ac
                           0000AC   839 _IE_4	=	0x00ac
                           0000AD   840 G$IE_5$0$0 == 0x00ad
                           0000AD   841 _IE_5	=	0x00ad
                           0000AE   842 G$IE_6$0$0 == 0x00ae
                           0000AE   843 _IE_6	=	0x00ae
                           0000AF   844 G$IE_7$0$0 == 0x00af
                           0000AF   845 _IE_7	=	0x00af
                           0000AF   846 G$EA$0$0 == 0x00af
                           0000AF   847 _EA	=	0x00af
                           0000B8   848 G$IP_0$0$0 == 0x00b8
                           0000B8   849 _IP_0	=	0x00b8
                           0000B9   850 G$IP_1$0$0 == 0x00b9
                           0000B9   851 _IP_1	=	0x00b9
                           0000BA   852 G$IP_2$0$0 == 0x00ba
                           0000BA   853 _IP_2	=	0x00ba
                           0000BB   854 G$IP_3$0$0 == 0x00bb
                           0000BB   855 _IP_3	=	0x00bb
                           0000BC   856 G$IP_4$0$0 == 0x00bc
                           0000BC   857 _IP_4	=	0x00bc
                           0000BD   858 G$IP_5$0$0 == 0x00bd
                           0000BD   859 _IP_5	=	0x00bd
                           0000BE   860 G$IP_6$0$0 == 0x00be
                           0000BE   861 _IP_6	=	0x00be
                           0000BF   862 G$IP_7$0$0 == 0x00bf
                           0000BF   863 _IP_7	=	0x00bf
                           0000D0   864 G$P$0$0 == 0x00d0
                           0000D0   865 _P	=	0x00d0
                           0000D1   866 G$F1$0$0 == 0x00d1
                           0000D1   867 _F1	=	0x00d1
                           0000D2   868 G$OV$0$0 == 0x00d2
                           0000D2   869 _OV	=	0x00d2
                           0000D3   870 G$RS0$0$0 == 0x00d3
                           0000D3   871 _RS0	=	0x00d3
                           0000D4   872 G$RS1$0$0 == 0x00d4
                           0000D4   873 _RS1	=	0x00d4
                           0000D5   874 G$F0$0$0 == 0x00d5
                           0000D5   875 _F0	=	0x00d5
                           0000D6   876 G$AC$0$0 == 0x00d6
                           0000D6   877 _AC	=	0x00d6
                           0000D7   878 G$CY$0$0 == 0x00d7
                           0000D7   879 _CY	=	0x00d7
                           0000C8   880 G$PINA_0$0$0 == 0x00c8
                           0000C8   881 _PINA_0	=	0x00c8
                           0000C9   882 G$PINA_1$0$0 == 0x00c9
                           0000C9   883 _PINA_1	=	0x00c9
                           0000CA   884 G$PINA_2$0$0 == 0x00ca
                           0000CA   885 _PINA_2	=	0x00ca
                           0000CB   886 G$PINA_3$0$0 == 0x00cb
                           0000CB   887 _PINA_3	=	0x00cb
                           0000CC   888 G$PINA_4$0$0 == 0x00cc
                           0000CC   889 _PINA_4	=	0x00cc
                           0000CD   890 G$PINA_5$0$0 == 0x00cd
                           0000CD   891 _PINA_5	=	0x00cd
                           0000CE   892 G$PINA_6$0$0 == 0x00ce
                           0000CE   893 _PINA_6	=	0x00ce
                           0000CF   894 G$PINA_7$0$0 == 0x00cf
                           0000CF   895 _PINA_7	=	0x00cf
                           0000E8   896 G$PINB_0$0$0 == 0x00e8
                           0000E8   897 _PINB_0	=	0x00e8
                           0000E9   898 G$PINB_1$0$0 == 0x00e9
                           0000E9   899 _PINB_1	=	0x00e9
                           0000EA   900 G$PINB_2$0$0 == 0x00ea
                           0000EA   901 _PINB_2	=	0x00ea
                           0000EB   902 G$PINB_3$0$0 == 0x00eb
                           0000EB   903 _PINB_3	=	0x00eb
                           0000EC   904 G$PINB_4$0$0 == 0x00ec
                           0000EC   905 _PINB_4	=	0x00ec
                           0000ED   906 G$PINB_5$0$0 == 0x00ed
                           0000ED   907 _PINB_5	=	0x00ed
                           0000EE   908 G$PINB_6$0$0 == 0x00ee
                           0000EE   909 _PINB_6	=	0x00ee
                           0000EF   910 G$PINB_7$0$0 == 0x00ef
                           0000EF   911 _PINB_7	=	0x00ef
                           0000F8   912 G$PINC_0$0$0 == 0x00f8
                           0000F8   913 _PINC_0	=	0x00f8
                           0000F9   914 G$PINC_1$0$0 == 0x00f9
                           0000F9   915 _PINC_1	=	0x00f9
                           0000FA   916 G$PINC_2$0$0 == 0x00fa
                           0000FA   917 _PINC_2	=	0x00fa
                           0000FB   918 G$PINC_3$0$0 == 0x00fb
                           0000FB   919 _PINC_3	=	0x00fb
                           0000FC   920 G$PINC_4$0$0 == 0x00fc
                           0000FC   921 _PINC_4	=	0x00fc
                           0000FD   922 G$PINC_5$0$0 == 0x00fd
                           0000FD   923 _PINC_5	=	0x00fd
                           0000FE   924 G$PINC_6$0$0 == 0x00fe
                           0000FE   925 _PINC_6	=	0x00fe
                           0000FF   926 G$PINC_7$0$0 == 0x00ff
                           0000FF   927 _PINC_7	=	0x00ff
                           000080   928 G$PORTA_0$0$0 == 0x0080
                           000080   929 _PORTA_0	=	0x0080
                           000081   930 G$PORTA_1$0$0 == 0x0081
                           000081   931 _PORTA_1	=	0x0081
                           000082   932 G$PORTA_2$0$0 == 0x0082
                           000082   933 _PORTA_2	=	0x0082
                           000083   934 G$PORTA_3$0$0 == 0x0083
                           000083   935 _PORTA_3	=	0x0083
                           000084   936 G$PORTA_4$0$0 == 0x0084
                           000084   937 _PORTA_4	=	0x0084
                           000085   938 G$PORTA_5$0$0 == 0x0085
                           000085   939 _PORTA_5	=	0x0085
                           000086   940 G$PORTA_6$0$0 == 0x0086
                           000086   941 _PORTA_6	=	0x0086
                           000087   942 G$PORTA_7$0$0 == 0x0087
                           000087   943 _PORTA_7	=	0x0087
                           000088   944 G$PORTB_0$0$0 == 0x0088
                           000088   945 _PORTB_0	=	0x0088
                           000089   946 G$PORTB_1$0$0 == 0x0089
                           000089   947 _PORTB_1	=	0x0089
                           00008A   948 G$PORTB_2$0$0 == 0x008a
                           00008A   949 _PORTB_2	=	0x008a
                           00008B   950 G$PORTB_3$0$0 == 0x008b
                           00008B   951 _PORTB_3	=	0x008b
                           00008C   952 G$PORTB_4$0$0 == 0x008c
                           00008C   953 _PORTB_4	=	0x008c
                           00008D   954 G$PORTB_5$0$0 == 0x008d
                           00008D   955 _PORTB_5	=	0x008d
                           00008E   956 G$PORTB_6$0$0 == 0x008e
                           00008E   957 _PORTB_6	=	0x008e
                           00008F   958 G$PORTB_7$0$0 == 0x008f
                           00008F   959 _PORTB_7	=	0x008f
                           000090   960 G$PORTC_0$0$0 == 0x0090
                           000090   961 _PORTC_0	=	0x0090
                           000091   962 G$PORTC_1$0$0 == 0x0091
                           000091   963 _PORTC_1	=	0x0091
                           000092   964 G$PORTC_2$0$0 == 0x0092
                           000092   965 _PORTC_2	=	0x0092
                           000093   966 G$PORTC_3$0$0 == 0x0093
                           000093   967 _PORTC_3	=	0x0093
                           000094   968 G$PORTC_4$0$0 == 0x0094
                           000094   969 _PORTC_4	=	0x0094
                           000095   970 G$PORTC_5$0$0 == 0x0095
                           000095   971 _PORTC_5	=	0x0095
                           000096   972 G$PORTC_6$0$0 == 0x0096
                           000096   973 _PORTC_6	=	0x0096
                           000097   974 G$PORTC_7$0$0 == 0x0097
                           000097   975 _PORTC_7	=	0x0097
                                    976 ;--------------------------------------------------------
                                    977 ; overlayable register banks
                                    978 ;--------------------------------------------------------
                                    979 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        980 	.ds 8
                                    981 ;--------------------------------------------------------
                                    982 ; internal ram data
                                    983 ;--------------------------------------------------------
                                    984 	.area DSEG    (DATA)
                           000000   985 G$pkt_counter$0$0==.
      000022                        986 _pkt_counter::
      000022                        987 	.ds 2
                           000002   988 G$coldstart$0$0==.
      000024                        989 _coldstart::
      000024                        990 	.ds 1
                           000003   991 Lmain.axradio_statuschange$st$1$351==.
      000025                        992 _axradio_statuschange_st_1_351:
      000025                        993 	.ds 2
                           000005   994 Lmain.axradio_statuschange$pktdata$3$358==.
      000027                        995 _axradio_statuschange_pktdata_3_358:
      000027                        996 	.ds 2
                           000007   997 Lmain.main$saved_button_state$1$378==.
      000029                        998 _main_saved_button_state_1_378:
      000029                        999 	.ds 1
                                   1000 ;--------------------------------------------------------
                                   1001 ; overlayable items in internal ram 
                                   1002 ;--------------------------------------------------------
                                   1003 ;--------------------------------------------------------
                                   1004 ; Stack segment in internal ram 
                                   1005 ;--------------------------------------------------------
                                   1006 	.area	SSEG
      000042                       1007 __start__stack:
      000042                       1008 	.ds	1
                                   1009 
                                   1010 ;--------------------------------------------------------
                                   1011 ; indirectly addressable internal ram data
                                   1012 ;--------------------------------------------------------
                                   1013 	.area ISEG    (DATA)
                                   1014 ;--------------------------------------------------------
                                   1015 ; absolute internal ram data
                                   1016 ;--------------------------------------------------------
                                   1017 	.area IABS    (ABS,DATA)
                                   1018 	.area IABS    (ABS,DATA)
                                   1019 ;--------------------------------------------------------
                                   1020 ; bit data
                                   1021 ;--------------------------------------------------------
                                   1022 	.area BSEG    (BIT)
                           000000  1023 Lmain._sdcc_external_startup$sloc0$1$0==.
      000001                       1024 __sdcc_external_startup_sloc0_1_0:
      000001                       1025 	.ds 1
                                   1026 ;--------------------------------------------------------
                                   1027 ; paged external ram data
                                   1028 ;--------------------------------------------------------
                                   1029 	.area PSEG    (PAG,XDATA)
                                   1030 ;--------------------------------------------------------
                                   1031 ; external ram data
                                   1032 ;--------------------------------------------------------
                                   1033 	.area XSEG    (XDATA)
                           007020  1034 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1035 _ADCCH0VAL0	=	0x7020
                           007021  1036 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1037 _ADCCH0VAL1	=	0x7021
                           007020  1038 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1039 _ADCCH0VAL	=	0x7020
                           007022  1040 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1041 _ADCCH1VAL0	=	0x7022
                           007023  1042 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1043 _ADCCH1VAL1	=	0x7023
                           007022  1044 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1045 _ADCCH1VAL	=	0x7022
                           007024  1046 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1047 _ADCCH2VAL0	=	0x7024
                           007025  1048 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1049 _ADCCH2VAL1	=	0x7025
                           007024  1050 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1051 _ADCCH2VAL	=	0x7024
                           007026  1052 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1053 _ADCCH3VAL0	=	0x7026
                           007027  1054 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1055 _ADCCH3VAL1	=	0x7027
                           007026  1056 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1057 _ADCCH3VAL	=	0x7026
                           007028  1058 G$ADCTUNE0$0$0 == 0x7028
                           007028  1059 _ADCTUNE0	=	0x7028
                           007029  1060 G$ADCTUNE1$0$0 == 0x7029
                           007029  1061 _ADCTUNE1	=	0x7029
                           00702A  1062 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1063 _ADCTUNE2	=	0x702a
                           007010  1064 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1065 _DMA0ADDR0	=	0x7010
                           007011  1066 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1067 _DMA0ADDR1	=	0x7011
                           007010  1068 G$DMA0ADDR$0$0 == 0x7010
                           007010  1069 _DMA0ADDR	=	0x7010
                           007014  1070 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1071 _DMA0CONFIG	=	0x7014
                           007012  1072 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1073 _DMA1ADDR0	=	0x7012
                           007013  1074 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1075 _DMA1ADDR1	=	0x7013
                           007012  1076 G$DMA1ADDR$0$0 == 0x7012
                           007012  1077 _DMA1ADDR	=	0x7012
                           007015  1078 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1079 _DMA1CONFIG	=	0x7015
                           007070  1080 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1081 _FRCOSCCONFIG	=	0x7070
                           007071  1082 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1083 _FRCOSCCTRL	=	0x7071
                           007076  1084 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1085 _FRCOSCFREQ0	=	0x7076
                           007077  1086 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1087 _FRCOSCFREQ1	=	0x7077
                           007076  1088 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1089 _FRCOSCFREQ	=	0x7076
                           007072  1090 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1091 _FRCOSCKFILT0	=	0x7072
                           007073  1092 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1093 _FRCOSCKFILT1	=	0x7073
                           007072  1094 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1095 _FRCOSCKFILT	=	0x7072
                           007078  1096 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1097 _FRCOSCPER0	=	0x7078
                           007079  1098 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1099 _FRCOSCPER1	=	0x7079
                           007078  1100 G$FRCOSCPER$0$0 == 0x7078
                           007078  1101 _FRCOSCPER	=	0x7078
                           007074  1102 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1103 _FRCOSCREF0	=	0x7074
                           007075  1104 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1105 _FRCOSCREF1	=	0x7075
                           007074  1106 G$FRCOSCREF$0$0 == 0x7074
                           007074  1107 _FRCOSCREF	=	0x7074
                           007007  1108 G$ANALOGA$0$0 == 0x7007
                           007007  1109 _ANALOGA	=	0x7007
                           00700C  1110 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1111 _GPIOENABLE	=	0x700c
                           007003  1112 G$EXTIRQ$0$0 == 0x7003
                           007003  1113 _EXTIRQ	=	0x7003
                           007000  1114 G$INTCHGA$0$0 == 0x7000
                           007000  1115 _INTCHGA	=	0x7000
                           007001  1116 G$INTCHGB$0$0 == 0x7001
                           007001  1117 _INTCHGB	=	0x7001
                           007002  1118 G$INTCHGC$0$0 == 0x7002
                           007002  1119 _INTCHGC	=	0x7002
                           007008  1120 G$PALTA$0$0 == 0x7008
                           007008  1121 _PALTA	=	0x7008
                           007009  1122 G$PALTB$0$0 == 0x7009
                           007009  1123 _PALTB	=	0x7009
                           00700A  1124 G$PALTC$0$0 == 0x700a
                           00700A  1125 _PALTC	=	0x700a
                           007046  1126 G$PALTRADIO$0$0 == 0x7046
                           007046  1127 _PALTRADIO	=	0x7046
                           007004  1128 G$PINCHGA$0$0 == 0x7004
                           007004  1129 _PINCHGA	=	0x7004
                           007005  1130 G$PINCHGB$0$0 == 0x7005
                           007005  1131 _PINCHGB	=	0x7005
                           007006  1132 G$PINCHGC$0$0 == 0x7006
                           007006  1133 _PINCHGC	=	0x7006
                           00700B  1134 G$PINSEL$0$0 == 0x700b
                           00700B  1135 _PINSEL	=	0x700b
                           007060  1136 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1137 _LPOSCCONFIG	=	0x7060
                           007066  1138 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1139 _LPOSCFREQ0	=	0x7066
                           007067  1140 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1141 _LPOSCFREQ1	=	0x7067
                           007066  1142 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1143 _LPOSCFREQ	=	0x7066
                           007062  1144 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1145 _LPOSCKFILT0	=	0x7062
                           007063  1146 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1147 _LPOSCKFILT1	=	0x7063
                           007062  1148 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1149 _LPOSCKFILT	=	0x7062
                           007068  1150 G$LPOSCPER0$0$0 == 0x7068
                           007068  1151 _LPOSCPER0	=	0x7068
                           007069  1152 G$LPOSCPER1$0$0 == 0x7069
                           007069  1153 _LPOSCPER1	=	0x7069
                           007068  1154 G$LPOSCPER$0$0 == 0x7068
                           007068  1155 _LPOSCPER	=	0x7068
                           007064  1156 G$LPOSCREF0$0$0 == 0x7064
                           007064  1157 _LPOSCREF0	=	0x7064
                           007065  1158 G$LPOSCREF1$0$0 == 0x7065
                           007065  1159 _LPOSCREF1	=	0x7065
                           007064  1160 G$LPOSCREF$0$0 == 0x7064
                           007064  1161 _LPOSCREF	=	0x7064
                           007054  1162 G$LPXOSCGM$0$0 == 0x7054
                           007054  1163 _LPXOSCGM	=	0x7054
                           007F01  1164 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1165 _MISCCTRL	=	0x7f01
                           007053  1166 G$OSCCALIB$0$0 == 0x7053
                           007053  1167 _OSCCALIB	=	0x7053
                           007050  1168 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1169 _OSCFORCERUN	=	0x7050
                           007052  1170 G$OSCREADY$0$0 == 0x7052
                           007052  1171 _OSCREADY	=	0x7052
                           007051  1172 G$OSCRUN$0$0 == 0x7051
                           007051  1173 _OSCRUN	=	0x7051
                           007040  1174 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1175 _RADIOFDATAADDR0	=	0x7040
                           007041  1176 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1177 _RADIOFDATAADDR1	=	0x7041
                           007040  1178 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1179 _RADIOFDATAADDR	=	0x7040
                           007042  1180 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1181 _RADIOFSTATADDR0	=	0x7042
                           007043  1182 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1183 _RADIOFSTATADDR1	=	0x7043
                           007042  1184 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1185 _RADIOFSTATADDR	=	0x7042
                           007044  1186 G$RADIOMUX$0$0 == 0x7044
                           007044  1187 _RADIOMUX	=	0x7044
                           007084  1188 G$SCRATCH0$0$0 == 0x7084
                           007084  1189 _SCRATCH0	=	0x7084
                           007085  1190 G$SCRATCH1$0$0 == 0x7085
                           007085  1191 _SCRATCH1	=	0x7085
                           007086  1192 G$SCRATCH2$0$0 == 0x7086
                           007086  1193 _SCRATCH2	=	0x7086
                           007087  1194 G$SCRATCH3$0$0 == 0x7087
                           007087  1195 _SCRATCH3	=	0x7087
                           007F00  1196 G$SILICONREV$0$0 == 0x7f00
                           007F00  1197 _SILICONREV	=	0x7f00
                           007F19  1198 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1199 _XTALAMPL	=	0x7f19
                           007F18  1200 G$XTALOSC$0$0 == 0x7f18
                           007F18  1201 _XTALOSC	=	0x7f18
                           007F1A  1202 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1203 _XTALREADY	=	0x7f1a
                           00FC06  1204 Fmain$flash_deviceid$0$0 == 0xfc06
                           00FC06  1205 _flash_deviceid	=	0xfc06
                           00FC00  1206 Fmain$flash_calsector$0$0 == 0xfc00
                           00FC00  1207 _flash_calsector	=	0xfc00
                           000000  1208 G$wakeup_desc$0$0==.
      0002A0                       1209 _wakeup_desc::
      0002A0                       1210 	.ds 8
                           000008  1211 Lmain.transmit_packet$demo_packet_$1$342==.
      0002A8                       1212 _transmit_packet_demo_packet__1_342:
      0002A8                       1213 	.ds 1
                           000009  1214 Lmain.transmit_beacon$beacon_packet_$1$345==.
      0002A9                       1215 _transmit_beacon_beacon_packet__1_345:
      0002A9                       1216 	.ds 8
                           000011  1217 Lmain.transmit_ack$ack_packet_$1$348==.
      0002B1                       1218 _transmit_ack_ack_packet__1_348:
      0002B1                       1219 	.ds 4
                           000015  1220 Lmain.wakeup_callback$timer0Period$1$369==.
      0002B5                       1221 _wakeup_callback_timer0Period_1_369:
      0002B5                       1222 	.ds 2
                           000017  1223 Lmain.wakeup_callback$bc_flg$1$369==.
      0002B7                       1224 _wakeup_callback_bc_flg_1_369:
      0002B7                       1225 	.ds 1
                                   1226 ;--------------------------------------------------------
                                   1227 ; absolute external ram data
                                   1228 ;--------------------------------------------------------
                                   1229 	.area XABS    (ABS,XDATA)
                                   1230 ;--------------------------------------------------------
                                   1231 ; external initialized ram data
                                   1232 ;--------------------------------------------------------
                                   1233 	.area XISEG   (XDATA)
                           000000  1234 G$beacon_packet$0$0==.
      000470                       1235 _beacon_packet::
      000470                       1236 	.ds 8
                           000008  1237 G$ack_packet$0$0==.
      000478                       1238 _ack_packet::
      000478                       1239 	.ds 4
                           00000C  1240 G$pkts_received$0$0==.
      00047C                       1241 _pkts_received::
      00047C                       1242 	.ds 2
                           00000E  1243 G$pkts_missing$0$0==.
      00047E                       1244 _pkts_missing::
      00047E                       1245 	.ds 2
                           000010  1246 G$bc_counter$0$0==.
      000480                       1247 _bc_counter::
      000480                       1248 	.ds 2
                           000012  1249 G$rcv_flg$0$0==.
      000482                       1250 _rcv_flg::
      000482                       1251 	.ds 1
                           000013  1252 G$pkts_id1$0$0==.
      000483                       1253 _pkts_id1::
      000483                       1254 	.ds 2
                           000015  1255 G$pkts_id3$0$0==.
      000485                       1256 _pkts_id3::
      000485                       1257 	.ds 2
                           000017  1258 G$rcv_flg_id1$0$0==.
      000487                       1259 _rcv_flg_id1::
      000487                       1260 	.ds 1
                           000018  1261 G$rcv_flg_id3$0$0==.
      000488                       1262 _rcv_flg_id3::
      000488                       1263 	.ds 1
                           000019  1264 G$wt0_beacon$0$0==.
      000489                       1265 _wt0_beacon::
      000489                       1266 	.ds 4
                                   1267 	.area HOME    (CODE)
                                   1268 	.area GSINIT0 (CODE)
                                   1269 	.area GSINIT1 (CODE)
                                   1270 	.area GSINIT2 (CODE)
                                   1271 	.area GSINIT3 (CODE)
                                   1272 	.area GSINIT4 (CODE)
                                   1273 	.area GSINIT5 (CODE)
                                   1274 	.area GSINIT  (CODE)
                                   1275 	.area GSFINAL (CODE)
                                   1276 	.area CSEG    (CODE)
                                   1277 ;--------------------------------------------------------
                                   1278 ; interrupt vector 
                                   1279 ;--------------------------------------------------------
                                   1280 	.area HOME    (CODE)
      000000                       1281 __interrupt_vect:
      000000 02 03 11         [24] 1282 	ljmp	__sdcc_gsinit_startup
      000003 32               [24] 1283 	reti
      000004                       1284 	.ds	7
      00000B 02 00 B1         [24] 1285 	ljmp	_wtimer_irq
      00000E                       1286 	.ds	5
      000013 32               [24] 1287 	reti
      000014                       1288 	.ds	7
      00001B 32               [24] 1289 	reti
      00001C                       1290 	.ds	7
      000023 02 16 D5         [24] 1291 	ljmp	_axradio_isr
      000026                       1292 	.ds	5
      00002B 32               [24] 1293 	reti
      00002C                       1294 	.ds	7
      000033 02 41 E0         [24] 1295 	ljmp	_pwrmgmt_irq
      000036                       1296 	.ds	5
      00003B 32               [24] 1297 	reti
      00003C                       1298 	.ds	7
      000043 32               [24] 1299 	reti
      000044                       1300 	.ds	7
      00004B 32               [24] 1301 	reti
      00004C                       1302 	.ds	7
      000053 32               [24] 1303 	reti
      000054                       1304 	.ds	7
      00005B 02 02 A3         [24] 1305 	ljmp	_uart0_irq
      00005E                       1306 	.ds	5
      000063 02 02 DA         [24] 1307 	ljmp	_uart1_irq
      000066                       1308 	.ds	5
      00006B 32               [24] 1309 	reti
      00006C                       1310 	.ds	7
      000073 32               [24] 1311 	reti
      000074                       1312 	.ds	7
      00007B 32               [24] 1313 	reti
      00007C                       1314 	.ds	7
      000083 32               [24] 1315 	reti
      000084                       1316 	.ds	7
      00008B 32               [24] 1317 	reti
      00008C                       1318 	.ds	7
      000093 32               [24] 1319 	reti
      000094                       1320 	.ds	7
      00009B 32               [24] 1321 	reti
      00009C                       1322 	.ds	7
      0000A3 32               [24] 1323 	reti
      0000A4                       1324 	.ds	7
      0000AB 02 02 6C         [24] 1325 	ljmp	_dbglink_irq
                                   1326 ;--------------------------------------------------------
                                   1327 ; global & static initialisations
                                   1328 ;--------------------------------------------------------
                                   1329 	.area HOME    (CODE)
                                   1330 	.area GSINIT  (CODE)
                                   1331 	.area GSFINAL (CODE)
                                   1332 	.area GSINIT  (CODE)
                                   1333 	.globl __sdcc_gsinit_startup
                                   1334 	.globl __sdcc_program_startup
                                   1335 	.globl __start__stack
                                   1336 	.globl __mcs51_genXINIT
                                   1337 	.globl __mcs51_genXRAMCLEAR
                                   1338 	.globl __mcs51_genRAMCLEAR
                                   1339 ;------------------------------------------------------------
                                   1340 ;Allocation info for local variables in function 'main'
                                   1341 ;------------------------------------------------------------
                                   1342 ;saved_button_state        Allocated with name '_main_saved_button_state_1_378'
                                   1343 ;i                         Allocated to registers r7 
                                   1344 ;x                         Allocated to registers r6 
                                   1345 ;flg                       Allocated to registers r6 
                                   1346 ;flg                       Allocated to registers r7 
                                   1347 ;------------------------------------------------------------
                           000000  1348 	G$main$0$0 ==.
                           000000  1349 	C$main.c$428$1$378 ==.
                                   1350 ;	main.c:428: static uint8_t __data saved_button_state = 0xFF;
      000398 75 29 FF         [24] 1351 	mov	_main_saved_button_state_1_378,#0xff
                           000003  1352 	C$main.c$80$1$378 ==.
                                   1353 ;	main.c:80: uint16_t __data pkt_counter = 0;
      00039B E4               [12] 1354 	clr	a
      00039C F5 22            [12] 1355 	mov	_pkt_counter,a
      00039E F5 23            [12] 1356 	mov	(_pkt_counter + 1),a
                           000008  1357 	C$main.c$81$1$378 ==.
                                   1358 ;	main.c:81: uint8_t __data coldstart = 1; /* caution: initialization with 1 is necessary! Variables are initialized upon _sdcc_external_startup returning 0 -> the coldstart value returned from _sdcc_external startup does not survive in the coldstart case */
      0003A0 75 24 01         [24] 1359 	mov	_coldstart,#0x01
                                   1360 	.area GSFINAL (CODE)
      0003A3 02 00 AE         [24] 1361 	ljmp	__sdcc_program_startup
                                   1362 ;--------------------------------------------------------
                                   1363 ; Home
                                   1364 ;--------------------------------------------------------
                                   1365 	.area HOME    (CODE)
                                   1366 	.area HOME    (CODE)
      0000AE                       1367 __sdcc_program_startup:
      0000AE 02 47 84         [24] 1368 	ljmp	_main
                                   1369 ;	return from main will return to caller
                                   1370 ;--------------------------------------------------------
                                   1371 ; code
                                   1372 ;--------------------------------------------------------
                                   1373 	.area CSEG    (CODE)
                                   1374 ;------------------------------------------------------------
                                   1375 ;Allocation info for local variables in function 'pwrmgmt_irq'
                                   1376 ;------------------------------------------------------------
                                   1377 ;pc                        Allocated to registers r7 
                                   1378 ;------------------------------------------------------------
                           000000  1379 	Fmain$pwrmgmt_irq$0$0 ==.
                           000000  1380 	C$main.c$98$0$0 ==.
                                   1381 ;	main.c:98: static void pwrmgmt_irq(void) __interrupt(INT_POWERMGMT)
                                   1382 ;	-----------------------------------------
                                   1383 ;	 function pwrmgmt_irq
                                   1384 ;	-----------------------------------------
      0041E0                       1385 _pwrmgmt_irq:
                           000007  1386 	ar7 = 0x07
                           000006  1387 	ar6 = 0x06
                           000005  1388 	ar5 = 0x05
                           000004  1389 	ar4 = 0x04
                           000003  1390 	ar3 = 0x03
                           000002  1391 	ar2 = 0x02
                           000001  1392 	ar1 = 0x01
                           000000  1393 	ar0 = 0x00
      0041E0 C0 E0            [24] 1394 	push	acc
      0041E2 C0 82            [24] 1395 	push	dpl
      0041E4 C0 83            [24] 1396 	push	dph
      0041E6 C0 07            [24] 1397 	push	ar7
      0041E8 C0 D0            [24] 1398 	push	psw
      0041EA 75 D0 00         [24] 1399 	mov	psw,#0x00
                           00000D  1400 	C$main.c$100$1$0 ==.
                                   1401 ;	main.c:100: uint8_t pc = PCON;
                           00000D  1402 	C$main.c$102$1$340 ==.
                                   1403 ;	main.c:102: if (!(pc & 0x80))
      0041ED E5 87            [12] 1404 	mov	a,_PCON
      0041EF FF               [12] 1405 	mov	r7,a
      0041F0 20 E7 02         [24] 1406 	jb	acc.7,00102$
                           000013  1407 	C$main.c$103$1$340 ==.
                                   1408 ;	main.c:103: return;
      0041F3 80 10            [24] 1409 	sjmp	00106$
      0041F5                       1410 00102$:
                           000015  1411 	C$main.c$105$1$340 ==.
                                   1412 ;	main.c:105: GPIOENABLE = 0;
      0041F5 90 70 0C         [24] 1413 	mov	dptr,#_GPIOENABLE
      0041F8 E4               [12] 1414 	clr	a
      0041F9 F0               [24] 1415 	movx	@dptr,a
                           00001A  1416 	C$main.c$106$1$340 ==.
                                   1417 ;	main.c:106: IE = EIE = E2IE = 0;
                                   1418 ;	1-genFromRTrack replaced	mov	_E2IE,#0x00
      0041FA F5 A0            [12] 1419 	mov	_E2IE,a
                                   1420 ;	1-genFromRTrack replaced	mov	_EIE,#0x00
      0041FC F5 98            [12] 1421 	mov	_EIE,a
                                   1422 ;	1-genFromRTrack replaced	mov	_IE,#0x00
      0041FE F5 A8            [12] 1423 	mov	_IE,a
      004200                       1424 00104$:
                           000020  1425 	C$main.c$109$1$340 ==.
                                   1426 ;	main.c:109: PCON |= 0x01;
      004200 43 87 01         [24] 1427 	orl	_PCON,#0x01
      004203 80 FB            [24] 1428 	sjmp	00104$
      004205                       1429 00106$:
      004205 D0 D0            [24] 1430 	pop	psw
      004207 D0 07            [24] 1431 	pop	ar7
      004209 D0 83            [24] 1432 	pop	dph
      00420B D0 82            [24] 1433 	pop	dpl
      00420D D0 E0            [24] 1434 	pop	acc
                           00002F  1435 	C$main.c$110$1$340 ==.
                           00002F  1436 	XFmain$pwrmgmt_irq$0$0 ==.
      00420F 32               [24] 1437 	reti
                                   1438 ;	eliminated unneeded push/pop b
                                   1439 ;------------------------------------------------------------
                                   1440 ;Allocation info for local variables in function 'transmit_packet'
                                   1441 ;------------------------------------------------------------
                                   1442 ;demo_packet_              Allocated with name '_transmit_packet_demo_packet__1_342'
                                   1443 ;------------------------------------------------------------
                           000030  1444 	Fmain$transmit_packet$0$0 ==.
                           000030  1445 	C$main.c$112$1$340 ==.
                                   1446 ;	main.c:112: static void transmit_packet(void)
                                   1447 ;	-----------------------------------------
                                   1448 ;	 function transmit_packet
                                   1449 ;	-----------------------------------------
      004210                       1450 _transmit_packet:
                           000030  1451 	C$main.c$116$1$342 ==.
                                   1452 ;	main.c:116: ++pkt_counter;
      004210 05 22            [12] 1453 	inc	_pkt_counter
      004212 E4               [12] 1454 	clr	a
      004213 B5 22 02         [24] 1455 	cjne	a,_pkt_counter,00108$
      004216 05 23            [12] 1456 	inc	(_pkt_counter + 1)
      004218                       1457 00108$:
                           000038  1458 	C$main.c$117$1$342 ==.
                                   1459 ;	main.c:117: memcpy(demo_packet_, demo_packet, sizeof(demo_packet));
      004218 75 37 D0         [24] 1460 	mov	_memcpy_PARM_2,#_demo_packet
      00421B 75 38 64         [24] 1461 	mov	(_memcpy_PARM_2 + 1),#(_demo_packet >> 8)
      00421E 75 39 80         [24] 1462 	mov	(_memcpy_PARM_2 + 2),#0x80
      004221 75 3A 01         [24] 1463 	mov	_memcpy_PARM_3,#0x01
      004224 75 3B 00         [24] 1464 	mov	(_memcpy_PARM_3 + 1),#0x00
      004227 90 02 A8         [24] 1465 	mov	dptr,#_transmit_packet_demo_packet__1_342
      00422A 75 F0 00         [24] 1466 	mov	b,#0x00
      00422D 12 50 C9         [24] 1467 	lcall	_memcpy
                           000050  1468 	C$main.c$119$1$342 ==.
                                   1469 ;	main.c:119: if (framing_insert_counter)
      004230 90 64 CE         [24] 1470 	mov	dptr,#_framing_insert_counter
      004233 E4               [12] 1471 	clr	a
      004234 93               [24] 1472 	movc	a,@a+dptr
      004235 60 24            [24] 1473 	jz	00102$
                           000057  1474 	C$main.c$121$2$343 ==.
                                   1475 ;	main.c:121: demo_packet_[framing_counter_pos] = pkt_counter & 0xFF ;
      004237 90 64 CF         [24] 1476 	mov	dptr,#_framing_counter_pos
      00423A E4               [12] 1477 	clr	a
      00423B 93               [24] 1478 	movc	a,@a+dptr
      00423C FF               [12] 1479 	mov	r7,a
      00423D 24 A8            [12] 1480 	add	a,#_transmit_packet_demo_packet__1_342
      00423F F5 82            [12] 1481 	mov	dpl,a
      004241 E4               [12] 1482 	clr	a
      004242 34 02            [12] 1483 	addc	a,#(_transmit_packet_demo_packet__1_342 >> 8)
      004244 F5 83            [12] 1484 	mov	dph,a
      004246 AD 22            [24] 1485 	mov	r5,_pkt_counter
      004248 7E 00            [12] 1486 	mov	r6,#0x00
      00424A ED               [12] 1487 	mov	a,r5
      00424B F0               [24] 1488 	movx	@dptr,a
                           00006C  1489 	C$main.c$122$2$343 ==.
                                   1490 ;	main.c:122: demo_packet_[framing_counter_pos+1] = (pkt_counter>>8) & 0xFF;
      00424C EF               [12] 1491 	mov	a,r7
      00424D 04               [12] 1492 	inc	a
      00424E 24 A8            [12] 1493 	add	a,#_transmit_packet_demo_packet__1_342
      004250 F5 82            [12] 1494 	mov	dpl,a
      004252 E4               [12] 1495 	clr	a
      004253 34 02            [12] 1496 	addc	a,#(_transmit_packet_demo_packet__1_342 >> 8)
      004255 F5 83            [12] 1497 	mov	dph,a
      004257 E5 23            [12] 1498 	mov	a,(_pkt_counter + 1)
      004259 FF               [12] 1499 	mov	r7,a
      00425A F0               [24] 1500 	movx	@dptr,a
      00425B                       1501 00102$:
                           00007B  1502 	C$main.c$125$1$342 ==.
                                   1503 ;	main.c:125: axradio_transmit(&remoteaddr, demo_packet_, sizeof(demo_packet));
      00425B 75 1B A8         [24] 1504 	mov	_axradio_transmit_PARM_2,#_transmit_packet_demo_packet__1_342
      00425E 75 1C 02         [24] 1505 	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_packet_demo_packet__1_342 >> 8)
      004261 75 1D 00         [24] 1506 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004264 75 1E 01         [24] 1507 	mov	_axradio_transmit_PARM_3,#0x01
      004267 75 1F 00         [24] 1508 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      00426A 90 64 BF         [24] 1509 	mov	dptr,#_remoteaddr
      00426D 75 F0 80         [24] 1510 	mov	b,#0x80
      004270 12 3A DA         [24] 1511 	lcall	_axradio_transmit
                           000093  1512 	C$main.c$126$1$342 ==.
                           000093  1513 	XFmain$transmit_packet$0$0 ==.
      004273 22               [24] 1514 	ret
                                   1515 ;------------------------------------------------------------
                                   1516 ;Allocation info for local variables in function 'transmit_beacon'
                                   1517 ;------------------------------------------------------------
                                   1518 ;beacon_packet_            Allocated with name '_transmit_beacon_beacon_packet__1_345'
                                   1519 ;------------------------------------------------------------
                           000094  1520 	Fmain$transmit_beacon$0$0 ==.
                           000094  1521 	C$main.c$142$1$342 ==.
                                   1522 ;	main.c:142: static void transmit_beacon(void)
                                   1523 ;	-----------------------------------------
                                   1524 ;	 function transmit_beacon
                                   1525 ;	-----------------------------------------
      004274                       1526 _transmit_beacon:
                           000094  1527 	C$main.c$148$1$345 ==.
                                   1528 ;	main.c:148: ++bc_counter;
      004274 90 04 80         [24] 1529 	mov	dptr,#_bc_counter
      004277 E0               [24] 1530 	movx	a,@dptr
      004278 24 01            [12] 1531 	add	a,#0x01
      00427A F0               [24] 1532 	movx	@dptr,a
      00427B A3               [24] 1533 	inc	dptr
      00427C E0               [24] 1534 	movx	a,@dptr
      00427D 34 00            [12] 1535 	addc	a,#0x00
      00427F F0               [24] 1536 	movx	@dptr,a
                           0000A0  1537 	C$main.c$150$1$345 ==.
                                   1538 ;	main.c:150: memcpy(beacon_packet_, beacon_packet, sizeof(beacon_packet));
      004280 75 37 70         [24] 1539 	mov	_memcpy_PARM_2,#_beacon_packet
      004283 75 38 04         [24] 1540 	mov	(_memcpy_PARM_2 + 1),#(_beacon_packet >> 8)
      004286 75 39 00         [24] 1541 	mov	(_memcpy_PARM_2 + 2),#0x00
      004289 75 3A 08         [24] 1542 	mov	_memcpy_PARM_3,#0x08
      00428C 75 3B 00         [24] 1543 	mov	(_memcpy_PARM_3 + 1),#0x00
      00428F 90 02 A9         [24] 1544 	mov	dptr,#_transmit_beacon_beacon_packet__1_345
      004292 75 F0 00         [24] 1545 	mov	b,#0x00
      004295 12 50 C9         [24] 1546 	lcall	_memcpy
                           0000B8  1547 	C$main.c$152$1$345 ==.
                                   1548 ;	main.c:152: if (framing_insert_counter)
      004298 90 64 CE         [24] 1549 	mov	dptr,#_framing_insert_counter
      00429B E4               [12] 1550 	clr	a
      00429C 93               [24] 1551 	movc	a,@a+dptr
      00429D 60 30            [24] 1552 	jz	00102$
                           0000BF  1553 	C$main.c$154$2$346 ==.
                                   1554 ;	main.c:154: beacon_packet_[framing_counter_pos] = bc_counter & 0xFF;
      00429F 90 64 CF         [24] 1555 	mov	dptr,#_framing_counter_pos
      0042A2 E4               [12] 1556 	clr	a
      0042A3 93               [24] 1557 	movc	a,@a+dptr
      0042A4 FF               [12] 1558 	mov	r7,a
      0042A5 24 A9            [12] 1559 	add	a,#_transmit_beacon_beacon_packet__1_345
      0042A7 FD               [12] 1560 	mov	r5,a
      0042A8 E4               [12] 1561 	clr	a
      0042A9 34 02            [12] 1562 	addc	a,#(_transmit_beacon_beacon_packet__1_345 >> 8)
      0042AB FE               [12] 1563 	mov	r6,a
      0042AC 90 04 80         [24] 1564 	mov	dptr,#_bc_counter
      0042AF E0               [24] 1565 	movx	a,@dptr
      0042B0 FB               [12] 1566 	mov	r3,a
      0042B1 A3               [24] 1567 	inc	dptr
      0042B2 E0               [24] 1568 	movx	a,@dptr
      0042B3 8D 82            [24] 1569 	mov	dpl,r5
      0042B5 8E 83            [24] 1570 	mov	dph,r6
      0042B7 EB               [12] 1571 	mov	a,r3
      0042B8 F0               [24] 1572 	movx	@dptr,a
                           0000D9  1573 	C$main.c$155$2$346 ==.
                                   1574 ;	main.c:155: beacon_packet_[framing_counter_pos + 1] = (bc_counter >> 8) & 0xFF;
      0042B9 EF               [12] 1575 	mov	a,r7
      0042BA 04               [12] 1576 	inc	a
      0042BB 24 A9            [12] 1577 	add	a,#_transmit_beacon_beacon_packet__1_345
      0042BD FE               [12] 1578 	mov	r6,a
      0042BE E4               [12] 1579 	clr	a
      0042BF 34 02            [12] 1580 	addc	a,#(_transmit_beacon_beacon_packet__1_345 >> 8)
      0042C1 FF               [12] 1581 	mov	r7,a
      0042C2 90 04 80         [24] 1582 	mov	dptr,#_bc_counter
      0042C5 E0               [24] 1583 	movx	a,@dptr
      0042C6 A3               [24] 1584 	inc	dptr
      0042C7 E0               [24] 1585 	movx	a,@dptr
      0042C8 FD               [12] 1586 	mov	r5,a
      0042C9 FC               [12] 1587 	mov	r4,a
      0042CA 8E 82            [24] 1588 	mov	dpl,r6
      0042CC 8F 83            [24] 1589 	mov	dph,r7
      0042CE F0               [24] 1590 	movx	@dptr,a
      0042CF                       1591 00102$:
                           0000EF  1592 	C$main.c$157$1$345 ==.
                                   1593 ;	main.c:157: wt0_beacon = wtimer0_curtime();
      0042CF 12 63 49         [24] 1594 	lcall	_wtimer0_curtime
      0042D2 AC 82            [24] 1595 	mov	r4,dpl
      0042D4 AD 83            [24] 1596 	mov	r5,dph
      0042D6 AE F0            [24] 1597 	mov	r6,b
      0042D8 FF               [12] 1598 	mov	r7,a
      0042D9 90 04 89         [24] 1599 	mov	dptr,#_wt0_beacon
      0042DC EC               [12] 1600 	mov	a,r4
      0042DD F0               [24] 1601 	movx	@dptr,a
      0042DE ED               [12] 1602 	mov	a,r5
      0042DF A3               [24] 1603 	inc	dptr
      0042E0 F0               [24] 1604 	movx	@dptr,a
      0042E1 EE               [12] 1605 	mov	a,r6
      0042E2 A3               [24] 1606 	inc	dptr
      0042E3 F0               [24] 1607 	movx	@dptr,a
      0042E4 EF               [12] 1608 	mov	a,r7
      0042E5 A3               [24] 1609 	inc	dptr
      0042E6 F0               [24] 1610 	movx	@dptr,a
                           000107  1611 	C$main.c$158$1$345 ==.
                                   1612 ;	main.c:158: axradio_transmit(&remoteaddr, beacon_packet_, sizeof(beacon_packet));
      0042E7 75 1B A9         [24] 1613 	mov	_axradio_transmit_PARM_2,#_transmit_beacon_beacon_packet__1_345
      0042EA 75 1C 02         [24] 1614 	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_beacon_beacon_packet__1_345 >> 8)
      0042ED 75 1D 00         [24] 1615 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      0042F0 75 1E 08         [24] 1616 	mov	_axradio_transmit_PARM_3,#0x08
      0042F3 75 1F 00         [24] 1617 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      0042F6 90 64 BF         [24] 1618 	mov	dptr,#_remoteaddr
      0042F9 75 F0 80         [24] 1619 	mov	b,#0x80
      0042FC 12 3A DA         [24] 1620 	lcall	_axradio_transmit
                           00011F  1621 	C$main.c$161$1$345 ==.
                                   1622 ;	main.c:161: dbglink_writestr("Beacon: ");
      0042FF 90 66 40         [24] 1623 	mov	dptr,#___str_0
      004302 75 F0 80         [24] 1624 	mov	b,#0x80
      004305 12 59 F7         [24] 1625 	lcall	_dbglink_writestr
                           000128  1626 	C$main.c$162$1$345 ==.
                                   1627 ;	main.c:162: dbglink_writehex16(bc_counter, 4, WRNUM_PADZERO);
      004308 90 04 80         [24] 1628 	mov	dptr,#_bc_counter
      00430B E0               [24] 1629 	movx	a,@dptr
      00430C FE               [12] 1630 	mov	r6,a
      00430D A3               [24] 1631 	inc	dptr
      00430E E0               [24] 1632 	movx	a,@dptr
      00430F FF               [12] 1633 	mov	r7,a
      004310 74 08            [12] 1634 	mov	a,#0x08
      004312 C0 E0            [24] 1635 	push	acc
      004314 03               [12] 1636 	rr	a
      004315 C0 E0            [24] 1637 	push	acc
      004317 8E 82            [24] 1638 	mov	dpl,r6
      004319 8F 83            [24] 1639 	mov	dph,r7
      00431B 12 5D F4         [24] 1640 	lcall	_dbglink_writehex16
      00431E 15 81            [12] 1641 	dec	sp
      004320 15 81            [12] 1642 	dec	sp
                           000142  1643 	C$main.c$163$1$345 ==.
                                   1644 ;	main.c:163: dbglink_writestr("  ");
      004322 90 66 49         [24] 1645 	mov	dptr,#___str_1
      004325 75 F0 80         [24] 1646 	mov	b,#0x80
      004328 12 59 F7         [24] 1647 	lcall	_dbglink_writestr
                           00014B  1648 	C$main.c$165$1$345 ==.
                                   1649 ;	main.c:165: dbglink_writestr("\n");
      00432B 90 66 4C         [24] 1650 	mov	dptr,#___str_2
      00432E 75 F0 80         [24] 1651 	mov	b,#0x80
      004331 12 59 F7         [24] 1652 	lcall	_dbglink_writestr
                           000154  1653 	C$main.c$167$1$345 ==.
                           000154  1654 	XFmain$transmit_beacon$0$0 ==.
      004334 22               [24] 1655 	ret
                                   1656 ;------------------------------------------------------------
                                   1657 ;Allocation info for local variables in function 'transmit_ack'
                                   1658 ;------------------------------------------------------------
                                   1659 ;ack_packet_               Allocated with name '_transmit_ack_ack_packet__1_348'
                                   1660 ;------------------------------------------------------------
                           000155  1661 	Fmain$transmit_ack$0$0 ==.
                           000155  1662 	C$main.c$175$1$345 ==.
                                   1663 ;	main.c:175: static void transmit_ack(void)
                                   1664 ;	-----------------------------------------
                                   1665 ;	 function transmit_ack
                                   1666 ;	-----------------------------------------
      004335                       1667 _transmit_ack:
                           000155  1668 	C$main.c$179$1$348 ==.
                                   1669 ;	main.c:179: memcpy(ack_packet_, ack_packet, sizeof(ack_packet));
      004335 75 37 78         [24] 1670 	mov	_memcpy_PARM_2,#_ack_packet
      004338 75 38 04         [24] 1671 	mov	(_memcpy_PARM_2 + 1),#(_ack_packet >> 8)
      00433B 75 39 00         [24] 1672 	mov	(_memcpy_PARM_2 + 2),#0x00
      00433E 75 3A 04         [24] 1673 	mov	_memcpy_PARM_3,#0x04
      004341 75 3B 00         [24] 1674 	mov	(_memcpy_PARM_3 + 1),#0x00
      004344 90 02 B1         [24] 1675 	mov	dptr,#_transmit_ack_ack_packet__1_348
      004347 75 F0 00         [24] 1676 	mov	b,#0x00
      00434A 12 50 C9         [24] 1677 	lcall	_memcpy
                           00016D  1678 	C$main.c$180$1$348 ==.
                                   1679 ;	main.c:180: if(rcv_flg_id1 > 0)
      00434D 90 04 87         [24] 1680 	mov	dptr,#_rcv_flg_id1
      004350 E0               [24] 1681 	movx	a,@dptr
      004351 60 08            [24] 1682 	jz	00102$
                           000173  1683 	C$main.c$184$2$349 ==.
                                   1684 ;	main.c:184: ack_packet_[0] = ack_packet_[0]|0x01;
      004353 90 02 B1         [24] 1685 	mov	dptr,#_transmit_ack_ack_packet__1_348
      004356 E0               [24] 1686 	movx	a,@dptr
      004357 44 01            [12] 1687 	orl	a,#0x01
      004359 FF               [12] 1688 	mov	r7,a
      00435A F0               [24] 1689 	movx	@dptr,a
      00435B                       1690 00102$:
                           00017B  1691 	C$main.c$186$1$348 ==.
                                   1692 ;	main.c:186: if(rcv_flg_id3 > 0)
      00435B 90 04 88         [24] 1693 	mov	dptr,#_rcv_flg_id3
      00435E E0               [24] 1694 	movx	a,@dptr
      00435F 60 08            [24] 1695 	jz	00104$
                           000181  1696 	C$main.c$189$2$350 ==.
                                   1697 ;	main.c:189: ack_packet_[0] = ack_packet_[0]|0x04;
      004361 90 02 B1         [24] 1698 	mov	dptr,#_transmit_ack_ack_packet__1_348
      004364 E0               [24] 1699 	movx	a,@dptr
      004365 44 04            [12] 1700 	orl	a,#0x04
      004367 FF               [12] 1701 	mov	r7,a
      004368 F0               [24] 1702 	movx	@dptr,a
      004369                       1703 00104$:
                           000189  1704 	C$main.c$196$1$348 ==.
                                   1705 ;	main.c:196: axradio_transmit(&remoteaddr, ack_packet_, sizeof(ack_packet));
      004369 75 1B B1         [24] 1706 	mov	_axradio_transmit_PARM_2,#_transmit_ack_ack_packet__1_348
      00436C 75 1C 02         [24] 1707 	mov	(_axradio_transmit_PARM_2 + 1),#(_transmit_ack_ack_packet__1_348 >> 8)
      00436F 75 1D 00         [24] 1708 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004372 75 1E 04         [24] 1709 	mov	_axradio_transmit_PARM_3,#0x04
      004375 75 1F 00         [24] 1710 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      004378 90 64 BF         [24] 1711 	mov	dptr,#_remoteaddr
      00437B 75 F0 80         [24] 1712 	mov	b,#0x80
      00437E 12 3A DA         [24] 1713 	lcall	_axradio_transmit
                           0001A1  1714 	C$main.c$199$1$348 ==.
                                   1715 ;	main.c:199: wait_dbglink_free();
      004381 12 0B 99         [24] 1716 	lcall	_wait_dbglink_free
                           0001A4  1717 	C$main.c$200$1$348 ==.
                                   1718 ;	main.c:200: dbglink_writestr("ACK: ");
      004384 90 66 4E         [24] 1719 	mov	dptr,#___str_3
      004387 75 F0 80         [24] 1720 	mov	b,#0x80
      00438A 12 59 F7         [24] 1721 	lcall	_dbglink_writestr
                           0001AD  1722 	C$main.c$201$1$348 ==.
                                   1723 ;	main.c:201: dbglink_writehex16(rcv_flg, 2, WRNUM_SIGNED);
      00438D 90 04 82         [24] 1724 	mov	dptr,#_rcv_flg
      004390 E0               [24] 1725 	movx	a,@dptr
      004391 FF               [12] 1726 	mov	r7,a
      004392 7E 00            [12] 1727 	mov	r6,#0x00
      004394 74 01            [12] 1728 	mov	a,#0x01
      004396 C0 E0            [24] 1729 	push	acc
      004398 04               [12] 1730 	inc	a
      004399 C0 E0            [24] 1731 	push	acc
      00439B 8F 82            [24] 1732 	mov	dpl,r7
      00439D 8E 83            [24] 1733 	mov	dph,r6
      00439F 12 5D F4         [24] 1734 	lcall	_dbglink_writehex16
      0043A2 15 81            [12] 1735 	dec	sp
      0043A4 15 81            [12] 1736 	dec	sp
                           0001C6  1737 	C$main.c$202$1$348 ==.
                                   1738 ;	main.c:202: dbglink_writestr(" ");
      0043A6 90 66 54         [24] 1739 	mov	dptr,#___str_4
      0043A9 75 F0 80         [24] 1740 	mov	b,#0x80
      0043AC 12 59 F7         [24] 1741 	lcall	_dbglink_writestr
                           0001CF  1742 	C$main.c$203$1$348 ==.
                                   1743 ;	main.c:203: dbglink_writehex16(pkts_received, 4, WRNUM_PADZERO);
      0043AF 90 04 7C         [24] 1744 	mov	dptr,#_pkts_received
      0043B2 E0               [24] 1745 	movx	a,@dptr
      0043B3 FE               [12] 1746 	mov	r6,a
      0043B4 A3               [24] 1747 	inc	dptr
      0043B5 E0               [24] 1748 	movx	a,@dptr
      0043B6 FF               [12] 1749 	mov	r7,a
      0043B7 74 08            [12] 1750 	mov	a,#0x08
      0043B9 C0 E0            [24] 1751 	push	acc
      0043BB 03               [12] 1752 	rr	a
      0043BC C0 E0            [24] 1753 	push	acc
      0043BE 8E 82            [24] 1754 	mov	dpl,r6
      0043C0 8F 83            [24] 1755 	mov	dph,r7
      0043C2 12 5D F4         [24] 1756 	lcall	_dbglink_writehex16
      0043C5 15 81            [12] 1757 	dec	sp
      0043C7 15 81            [12] 1758 	dec	sp
                           0001E9  1759 	C$main.c$204$1$348 ==.
                                   1760 ;	main.c:204: dbglink_tx('\n');
      0043C9 75 82 0A         [24] 1761 	mov	dpl,#0x0a
      0043CC 12 4A 59         [24] 1762 	lcall	_dbglink_tx
                           0001EF  1763 	C$main.c$205$1$348 ==.
                                   1764 ;	main.c:205: dbglink_writestr("ID1: ");
      0043CF 90 66 56         [24] 1765 	mov	dptr,#___str_5
      0043D2 75 F0 80         [24] 1766 	mov	b,#0x80
      0043D5 12 59 F7         [24] 1767 	lcall	_dbglink_writestr
                           0001F8  1768 	C$main.c$206$1$348 ==.
                                   1769 ;	main.c:206: dbglink_writehex16(rcv_flg_id1, 2, WRNUM_SIGNED);
      0043D8 90 04 87         [24] 1770 	mov	dptr,#_rcv_flg_id1
      0043DB E0               [24] 1771 	movx	a,@dptr
      0043DC FF               [12] 1772 	mov	r7,a
      0043DD 7E 00            [12] 1773 	mov	r6,#0x00
      0043DF 74 01            [12] 1774 	mov	a,#0x01
      0043E1 C0 E0            [24] 1775 	push	acc
      0043E3 04               [12] 1776 	inc	a
      0043E4 C0 E0            [24] 1777 	push	acc
      0043E6 8F 82            [24] 1778 	mov	dpl,r7
      0043E8 8E 83            [24] 1779 	mov	dph,r6
      0043EA 12 5D F4         [24] 1780 	lcall	_dbglink_writehex16
      0043ED 15 81            [12] 1781 	dec	sp
      0043EF 15 81            [12] 1782 	dec	sp
                           000211  1783 	C$main.c$207$1$348 ==.
                                   1784 ;	main.c:207: dbglink_writestr(" ");
      0043F1 90 66 54         [24] 1785 	mov	dptr,#___str_4
      0043F4 75 F0 80         [24] 1786 	mov	b,#0x80
      0043F7 12 59 F7         [24] 1787 	lcall	_dbglink_writestr
                           00021A  1788 	C$main.c$208$1$348 ==.
                                   1789 ;	main.c:208: dbglink_writehex16(pkts_id1, 4, WRNUM_PADZERO);
      0043FA 90 04 83         [24] 1790 	mov	dptr,#_pkts_id1
      0043FD E0               [24] 1791 	movx	a,@dptr
      0043FE FE               [12] 1792 	mov	r6,a
      0043FF A3               [24] 1793 	inc	dptr
      004400 E0               [24] 1794 	movx	a,@dptr
      004401 FF               [12] 1795 	mov	r7,a
      004402 74 08            [12] 1796 	mov	a,#0x08
      004404 C0 E0            [24] 1797 	push	acc
      004406 03               [12] 1798 	rr	a
      004407 C0 E0            [24] 1799 	push	acc
      004409 8E 82            [24] 1800 	mov	dpl,r6
      00440B 8F 83            [24] 1801 	mov	dph,r7
      00440D 12 5D F4         [24] 1802 	lcall	_dbglink_writehex16
      004410 15 81            [12] 1803 	dec	sp
      004412 15 81            [12] 1804 	dec	sp
                           000234  1805 	C$main.c$209$1$348 ==.
                                   1806 ;	main.c:209: dbglink_writestr("\nID3: ");
      004414 90 66 5C         [24] 1807 	mov	dptr,#___str_6
      004417 75 F0 80         [24] 1808 	mov	b,#0x80
      00441A 12 59 F7         [24] 1809 	lcall	_dbglink_writestr
                           00023D  1810 	C$main.c$211$1$348 ==.
                                   1811 ;	main.c:211: dbglink_writehex16(rcv_flg_id3, 2, WRNUM_SIGNED);
      00441D 90 04 88         [24] 1812 	mov	dptr,#_rcv_flg_id3
      004420 E0               [24] 1813 	movx	a,@dptr
      004421 FF               [12] 1814 	mov	r7,a
      004422 7E 00            [12] 1815 	mov	r6,#0x00
      004424 74 01            [12] 1816 	mov	a,#0x01
      004426 C0 E0            [24] 1817 	push	acc
      004428 04               [12] 1818 	inc	a
      004429 C0 E0            [24] 1819 	push	acc
      00442B 8F 82            [24] 1820 	mov	dpl,r7
      00442D 8E 83            [24] 1821 	mov	dph,r6
      00442F 12 5D F4         [24] 1822 	lcall	_dbglink_writehex16
      004432 15 81            [12] 1823 	dec	sp
      004434 15 81            [12] 1824 	dec	sp
                           000256  1825 	C$main.c$212$1$348 ==.
                                   1826 ;	main.c:212: dbglink_writestr(" ");
      004436 90 66 54         [24] 1827 	mov	dptr,#___str_4
      004439 75 F0 80         [24] 1828 	mov	b,#0x80
      00443C 12 59 F7         [24] 1829 	lcall	_dbglink_writestr
                           00025F  1830 	C$main.c$213$1$348 ==.
                                   1831 ;	main.c:213: dbglink_writehex16(pkts_id3, 4, WRNUM_PADZERO);
      00443F 90 04 85         [24] 1832 	mov	dptr,#_pkts_id3
      004442 E0               [24] 1833 	movx	a,@dptr
      004443 FE               [12] 1834 	mov	r6,a
      004444 A3               [24] 1835 	inc	dptr
      004445 E0               [24] 1836 	movx	a,@dptr
      004446 FF               [12] 1837 	mov	r7,a
      004447 74 08            [12] 1838 	mov	a,#0x08
      004449 C0 E0            [24] 1839 	push	acc
      00444B 03               [12] 1840 	rr	a
      00444C C0 E0            [24] 1841 	push	acc
      00444E 8E 82            [24] 1842 	mov	dpl,r6
      004450 8F 83            [24] 1843 	mov	dph,r7
      004452 12 5D F4         [24] 1844 	lcall	_dbglink_writehex16
      004455 15 81            [12] 1845 	dec	sp
      004457 15 81            [12] 1846 	dec	sp
                           000279  1847 	C$main.c$215$1$348 ==.
                                   1848 ;	main.c:215: dbglink_writestr("\n\n");
      004459 90 66 63         [24] 1849 	mov	dptr,#___str_7
      00445C 75 F0 80         [24] 1850 	mov	b,#0x80
      00445F 12 59 F7         [24] 1851 	lcall	_dbglink_writestr
                           000282  1852 	C$main.c$217$1$348 ==.
                           000282  1853 	XFmain$transmit_ack$0$0 ==.
      004462 22               [24] 1854 	ret
                                   1855 ;------------------------------------------------------------
                                   1856 ;Allocation info for local variables in function 'axradio_statuschange'
                                   1857 ;------------------------------------------------------------
                                   1858 ;st                        Allocated with name '_axradio_statuschange_st_1_351'
                                   1859 ;pktdata                   Allocated with name '_axradio_statuschange_pktdata_3_358'
                                   1860 ;------------------------------------------------------------
                           000283  1861 	G$axradio_statuschange$0$0 ==.
                           000283  1862 	C$main.c$221$1$348 ==.
                                   1863 ;	main.c:221: void axradio_statuschange(struct axradio_status __xdata *st)
                                   1864 ;	-----------------------------------------
                                   1865 ;	 function axradio_statuschange
                                   1866 ;	-----------------------------------------
      004463                       1867 _axradio_statuschange:
                           000283  1868 	C$main.c$235$1$352 ==.
                                   1869 ;	main.c:235: switch (st->status)
      004463 85 82 25         [24] 1870 	mov	_axradio_statuschange_st_1_351,dpl
      004466 85 83 26         [24] 1871 	mov  (_axradio_statuschange_st_1_351 + 1),dph
      004469 E0               [24] 1872 	movx	a,@dptr
      00446A FD               [12] 1873 	mov	r5,a
      00446B 60 1D            [24] 1874 	jz	00116$
      00446D BD 02 03         [24] 1875 	cjne	r5,#0x02,00156$
      004470 02 45 BE         [24] 1876 	ljmp	00129$
      004473                       1877 00156$:
      004473 BD 03 02         [24] 1878 	cjne	r5,#0x03,00157$
      004476 80 08            [24] 1879 	sjmp	00105$
      004478                       1880 00157$:
      004478 BD 04 02         [24] 1881 	cjne	r5,#0x04,00158$
      00447B 80 08            [24] 1882 	sjmp	00112$
      00447D                       1883 00158$:
      00447D 02 45 BE         [24] 1884 	ljmp	00129$
                           0002A0  1885 	C$main.c$238$2$353 ==.
                                   1886 ;	main.c:238: led0_on();
      004480                       1887 00105$:
      004480 D2 89            [12] 1888 	setb	_PORTB_1
                           0002A2  1889 	C$main.c$239$2$353 ==.
                                   1890 ;	main.c:239: break;
      004482 02 45 BE         [24] 1891 	ljmp	00129$
                           0002A5  1892 	C$main.c$242$2$353 ==.
                                   1893 ;	main.c:242: led0_off();
      004485                       1894 00112$:
      004485 C2 89            [12] 1895 	clr	_PORTB_1
                           0002A7  1896 	C$main.c$243$2$353 ==.
                                   1897 ;	main.c:243: break;
      004487 02 45 BE         [24] 1898 	ljmp	00129$
                           0002AA  1899 	C$main.c$248$2$353 ==.
                                   1900 ;	main.c:248: case AXRADIO_STAT_RECEIVE:
      00448A                       1901 00116$:
                           0002AA  1902 	C$main.c$251$3$358 ==.
                                   1903 ;	main.c:251: const uint8_t __xdata *pktdata = st->u.rx.mac.raw;
      00448A 74 06            [12] 1904 	mov	a,#0x06
      00448C 25 25            [12] 1905 	add	a,_axradio_statuschange_st_1_351
      00448E FC               [12] 1906 	mov	r4,a
      00448F E4               [12] 1907 	clr	a
      004490 35 26            [12] 1908 	addc	a,(_axradio_statuschange_st_1_351 + 1)
      004492 FD               [12] 1909 	mov	r5,a
      004493 74 14            [12] 1910 	mov	a,#0x14
      004495 2C               [12] 1911 	add	a,r4
      004496 F5 82            [12] 1912 	mov	dpl,a
      004498 E4               [12] 1913 	clr	a
      004499 3D               [12] 1914 	addc	a,r5
      00449A F5 83            [12] 1915 	mov	dph,a
      00449C E0               [24] 1916 	movx	a,@dptr
      00449D F5 27            [12] 1917 	mov	_axradio_statuschange_pktdata_3_358,a
      00449F A3               [24] 1918 	inc	dptr
      0044A0 E0               [24] 1919 	movx	a,@dptr
      0044A1 F5 28            [12] 1920 	mov	(_axradio_statuschange_pktdata_3_358 + 1),a
                           0002C3  1921 	C$main.c$253$3$358 ==.
                                   1922 ;	main.c:253: if (st->error == AXRADIO_ERR_NOERROR)
      0044A3 74 01            [12] 1923 	mov	a,#0x01
      0044A5 25 25            [12] 1924 	add	a,_axradio_statuschange_st_1_351
      0044A7 FA               [12] 1925 	mov	r2,a
      0044A8 E4               [12] 1926 	clr	a
      0044A9 35 26            [12] 1927 	addc	a,(_axradio_statuschange_st_1_351 + 1)
      0044AB FB               [12] 1928 	mov	r3,a
      0044AC 8A 82            [24] 1929 	mov	dpl,r2
      0044AE 8B 83            [24] 1930 	mov	dph,r3
      0044B0 E0               [24] 1931 	movx	a,@dptr
      0044B1 60 03            [24] 1932 	jz	00159$
      0044B3 02 45 B0         [24] 1933 	ljmp	00125$
      0044B6                       1934 00159$:
                           0002D6  1935 	C$main.c$256$4$359 ==.
                                   1936 ;	main.c:256: dbglink_writestr("40_PKT_WT0:");
      0044B6 90 66 66         [24] 1937 	mov	dptr,#___str_8
      0044B9 75 F0 80         [24] 1938 	mov	b,#0x80
      0044BC 12 59 F7         [24] 1939 	lcall	_dbglink_writestr
                           0002DF  1940 	C$main.c$257$4$359 ==.
                                   1941 ;	main.c:257: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      0044BF 12 63 49         [24] 1942 	lcall	_wtimer0_curtime
      0044C2 A8 82            [24] 1943 	mov	r0,dpl
      0044C4 A9 83            [24] 1944 	mov	r1,dph
      0044C6 AA F0            [24] 1945 	mov	r2,b
      0044C8 FB               [12] 1946 	mov	r3,a
      0044C9 74 08            [12] 1947 	mov	a,#0x08
      0044CB C0 E0            [24] 1948 	push	acc
      0044CD C0 E0            [24] 1949 	push	acc
      0044CF 88 82            [24] 1950 	mov	dpl,r0
      0044D1 89 83            [24] 1951 	mov	dph,r1
      0044D3 8A F0            [24] 1952 	mov	b,r2
      0044D5 EB               [12] 1953 	mov	a,r3
      0044D6 12 5B D6         [24] 1954 	lcall	_dbglink_writehex32
      0044D9 15 81            [12] 1955 	dec	sp
      0044DB 15 81            [12] 1956 	dec	sp
                           0002FD  1957 	C$main.c$258$4$359 ==.
                                   1958 ;	main.c:258: dbglink_tx('\n');
      0044DD 75 82 0A         [24] 1959 	mov	dpl,#0x0a
      0044E0 12 4A 59         [24] 1960 	lcall	_dbglink_tx
                           000303  1961 	C$main.c$259$4$359 ==.
                                   1962 ;	main.c:259: dbglink_writestr("41_PKT_delta:");
      0044E3 90 66 72         [24] 1963 	mov	dptr,#___str_9
      0044E6 75 F0 80         [24] 1964 	mov	b,#0x80
      0044E9 12 59 F7         [24] 1965 	lcall	_dbglink_writestr
                           00030C  1966 	C$main.c$260$4$359 ==.
                                   1967 ;	main.c:260: dbglink_writehex32(wtimer0_curtime()-wt0_beacon, 4, WRNUM_PADZERO);
      0044EC 12 63 49         [24] 1968 	lcall	_wtimer0_curtime
      0044EF A8 82            [24] 1969 	mov	r0,dpl
      0044F1 A9 83            [24] 1970 	mov	r1,dph
      0044F3 AA F0            [24] 1971 	mov	r2,b
      0044F5 FB               [12] 1972 	mov	r3,a
      0044F6 90 04 89         [24] 1973 	mov	dptr,#_wt0_beacon
      0044F9 E0               [24] 1974 	movx	a,@dptr
      0044FA FC               [12] 1975 	mov	r4,a
      0044FB A3               [24] 1976 	inc	dptr
      0044FC E0               [24] 1977 	movx	a,@dptr
      0044FD FD               [12] 1978 	mov	r5,a
      0044FE A3               [24] 1979 	inc	dptr
      0044FF E0               [24] 1980 	movx	a,@dptr
      004500 FE               [12] 1981 	mov	r6,a
      004501 A3               [24] 1982 	inc	dptr
      004502 E0               [24] 1983 	movx	a,@dptr
      004503 FF               [12] 1984 	mov	r7,a
      004504 E8               [12] 1985 	mov	a,r0
      004505 C3               [12] 1986 	clr	c
      004506 9C               [12] 1987 	subb	a,r4
      004507 F8               [12] 1988 	mov	r0,a
      004508 E9               [12] 1989 	mov	a,r1
      004509 9D               [12] 1990 	subb	a,r5
      00450A F9               [12] 1991 	mov	r1,a
      00450B EA               [12] 1992 	mov	a,r2
      00450C 9E               [12] 1993 	subb	a,r6
      00450D FA               [12] 1994 	mov	r2,a
      00450E EB               [12] 1995 	mov	a,r3
      00450F 9F               [12] 1996 	subb	a,r7
      004510 FB               [12] 1997 	mov	r3,a
      004511 74 08            [12] 1998 	mov	a,#0x08
      004513 C0 E0            [24] 1999 	push	acc
      004515 03               [12] 2000 	rr	a
      004516 C0 E0            [24] 2001 	push	acc
      004518 88 82            [24] 2002 	mov	dpl,r0
      00451A 89 83            [24] 2003 	mov	dph,r1
      00451C 8A F0            [24] 2004 	mov	b,r2
      00451E EB               [12] 2005 	mov	a,r3
      00451F 12 5B D6         [24] 2006 	lcall	_dbglink_writehex32
      004522 15 81            [12] 2007 	dec	sp
      004524 15 81            [12] 2008 	dec	sp
                           000346  2009 	C$main.c$261$4$359 ==.
                                   2010 ;	main.c:261: dbglink_tx('\n');
      004526 75 82 0A         [24] 2011 	mov	dpl,#0x0a
      004529 12 4A 59         [24] 2012 	lcall	_dbglink_tx
                           00034C  2013 	C$main.c$262$4$359 ==.
                                   2014 ;	main.c:262: dbglink_received_packet(st);
      00452C 85 25 82         [24] 2015 	mov	dpl,_axradio_statuschange_st_1_351
      00452F 85 26 83         [24] 2016 	mov	dph,(_axradio_statuschange_st_1_351 + 1)
      004532 12 0B AF         [24] 2017 	lcall	_dbglink_received_packet
                           000355  2018 	C$main.c$268$4$359 ==.
                                   2019 ;	main.c:268: if (pktdata[0] == 0x32) // changed from 0x2E on 2021-08-22
      004535 85 27 82         [24] 2020 	mov	dpl,_axradio_statuschange_pktdata_3_358
      004538 85 28 83         [24] 2021 	mov	dph,(_axradio_statuschange_pktdata_3_358 + 1)
      00453B E0               [24] 2022 	movx	a,@dptr
      00453C FF               [12] 2023 	mov	r7,a
      00453D BF 32 02         [24] 2024 	cjne	r7,#0x32,00160$
      004540 80 02            [24] 2025 	sjmp	00161$
      004542                       2026 00160$:
      004542 80 7A            [24] 2027 	sjmp	00129$
      004544                       2028 00161$:
                           000364  2029 	C$main.c$271$5$360 ==.
                                   2030 ;	main.c:271: ++pkts_received;
      004544 90 04 7C         [24] 2031 	mov	dptr,#_pkts_received
      004547 E0               [24] 2032 	movx	a,@dptr
      004548 24 01            [12] 2033 	add	a,#0x01
      00454A F0               [24] 2034 	movx	@dptr,a
      00454B A3               [24] 2035 	inc	dptr
      00454C E0               [24] 2036 	movx	a,@dptr
      00454D 34 00            [12] 2037 	addc	a,#0x00
      00454F F0               [24] 2038 	movx	@dptr,a
                           000370  2039 	C$main.c$272$5$360 ==.
                                   2040 ;	main.c:272: rcv_flg += 1;
      004550 90 04 82         [24] 2041 	mov	dptr,#_rcv_flg
      004553 E0               [24] 2042 	movx	a,@dptr
      004554 04               [12] 2043 	inc	a
      004555 F0               [24] 2044 	movx	@dptr,a
                           000376  2045 	C$main.c$274$5$360 ==.
                                   2046 ;	main.c:274: if ((pktdata[10] & 0xF0) >> 4 ==  0x3) // changed from 6 to 10 on 2021-08-22
      004556 74 0A            [12] 2047 	mov	a,#0x0a
      004558 25 27            [12] 2048 	add	a,_axradio_statuschange_pktdata_3_358
      00455A F5 82            [12] 2049 	mov	dpl,a
      00455C E4               [12] 2050 	clr	a
      00455D 35 28            [12] 2051 	addc	a,(_axradio_statuschange_pktdata_3_358 + 1)
      00455F F5 83            [12] 2052 	mov	dph,a
      004561 E0               [24] 2053 	movx	a,@dptr
      004562 FF               [12] 2054 	mov	r7,a
      004563 74 F0            [12] 2055 	mov	a,#0xf0
      004565 5F               [12] 2056 	anl	a,r7
      004566 C4               [12] 2057 	swap	a
      004567 54 0F            [12] 2058 	anl	a,#0x0f
      004569 FE               [12] 2059 	mov	r6,a
      00456A BE 03 1C         [24] 2060 	cjne	r6,#0x03,00120$
                           00038D  2061 	C$main.c$276$6$361 ==.
                                   2062 ;	main.c:276: pkts_id3 += 1;
      00456D 90 04 85         [24] 2063 	mov	dptr,#_pkts_id3
      004570 E0               [24] 2064 	movx	a,@dptr
      004571 FD               [12] 2065 	mov	r5,a
      004572 A3               [24] 2066 	inc	dptr
      004573 E0               [24] 2067 	movx	a,@dptr
      004574 FE               [12] 2068 	mov	r6,a
      004575 90 04 85         [24] 2069 	mov	dptr,#_pkts_id3
      004578 74 01            [12] 2070 	mov	a,#0x01
      00457A 2D               [12] 2071 	add	a,r5
      00457B F0               [24] 2072 	movx	@dptr,a
      00457C E4               [12] 2073 	clr	a
      00457D 3E               [12] 2074 	addc	a,r6
      00457E A3               [24] 2075 	inc	dptr
      00457F F0               [24] 2076 	movx	@dptr,a
                           0003A0  2077 	C$main.c$277$6$361 ==.
                                   2078 ;	main.c:277: rcv_flg_id3 += 1;
      004580 90 04 88         [24] 2079 	mov	dptr,#_rcv_flg_id3
      004583 E0               [24] 2080 	movx	a,@dptr
      004584 FE               [12] 2081 	mov	r6,a
      004585 04               [12] 2082 	inc	a
      004586 F0               [24] 2083 	movx	@dptr,a
      004587 80 35            [24] 2084 	sjmp	00129$
      004589                       2085 00120$:
                           0003A9  2086 	C$main.c$279$5$360 ==.
                                   2087 ;	main.c:279: else if ((pktdata[10] & 0xF0) >> 4 == 0x1) // changed from 6 to 10 on 2021-08-22
      004589 53 07 F0         [24] 2088 	anl	ar7,#0xf0
      00458C EF               [12] 2089 	mov	a,r7
      00458D C4               [12] 2090 	swap	a
      00458E 54 0F            [12] 2091 	anl	a,#0x0f
      004590 FF               [12] 2092 	mov	r7,a
      004591 BF 01 2A         [24] 2093 	cjne	r7,#0x01,00129$
                           0003B4  2094 	C$main.c$281$6$362 ==.
                                   2095 ;	main.c:281: pkts_id1 += 1;
      004594 90 04 83         [24] 2096 	mov	dptr,#_pkts_id1
      004597 E0               [24] 2097 	movx	a,@dptr
      004598 FE               [12] 2098 	mov	r6,a
      004599 A3               [24] 2099 	inc	dptr
      00459A E0               [24] 2100 	movx	a,@dptr
      00459B FF               [12] 2101 	mov	r7,a
      00459C 90 04 83         [24] 2102 	mov	dptr,#_pkts_id1
      00459F 74 01            [12] 2103 	mov	a,#0x01
      0045A1 2E               [12] 2104 	add	a,r6
      0045A2 F0               [24] 2105 	movx	@dptr,a
      0045A3 E4               [12] 2106 	clr	a
      0045A4 3F               [12] 2107 	addc	a,r7
      0045A5 A3               [24] 2108 	inc	dptr
      0045A6 F0               [24] 2109 	movx	@dptr,a
                           0003C7  2110 	C$main.c$282$6$362 ==.
                                   2111 ;	main.c:282: rcv_flg_id1 += 1;
      0045A7 90 04 87         [24] 2112 	mov	dptr,#_rcv_flg_id1
      0045AA E0               [24] 2113 	movx	a,@dptr
      0045AB FF               [12] 2114 	mov	r7,a
      0045AC 04               [12] 2115 	inc	a
      0045AD F0               [24] 2116 	movx	@dptr,a
      0045AE 80 0E            [24] 2117 	sjmp	00129$
      0045B0                       2118 00125$:
                           0003D0  2119 	C$main.c$295$4$363 ==.
                                   2120 ;	main.c:295: rcv_flg = 0;
      0045B0 90 04 82         [24] 2121 	mov	dptr,#_rcv_flg
      0045B3 E4               [12] 2122 	clr	a
      0045B4 F0               [24] 2123 	movx	@dptr,a
                           0003D5  2124 	C$main.c$297$4$363 ==.
                                   2125 ;	main.c:297: dbglink_writestr("Invalid packet.\n\n");
      0045B5 90 66 80         [24] 2126 	mov	dptr,#___str_10
      0045B8 75 F0 80         [24] 2127 	mov	b,#0x80
      0045BB 12 59 F7         [24] 2128 	lcall	_dbglink_writestr
                           0003DE  2129 	C$main.c$325$1$352 ==.
                                   2130 ;	main.c:325: }
      0045BE                       2131 00129$:
                           0003DE  2132 	C$main.c$326$1$352 ==.
                           0003DE  2133 	XG$axradio_statuschange$0$0 ==.
      0045BE 22               [24] 2134 	ret
                                   2135 ;------------------------------------------------------------
                                   2136 ;Allocation info for local variables in function 'enable_radio_interrupt_in_mcu_pin'
                                   2137 ;------------------------------------------------------------
                           0003DF  2138 	G$enable_radio_interrupt_in_mcu_pin$0$0 ==.
                           0003DF  2139 	C$main.c$328$1$352 ==.
                                   2140 ;	main.c:328: void enable_radio_interrupt_in_mcu_pin(void)
                                   2141 ;	-----------------------------------------
                                   2142 ;	 function enable_radio_interrupt_in_mcu_pin
                                   2143 ;	-----------------------------------------
      0045BF                       2144 _enable_radio_interrupt_in_mcu_pin:
                           0003DF  2145 	C$main.c$330$1$365 ==.
                                   2146 ;	main.c:330: IE_4 = 1;
      0045BF D2 AC            [12] 2147 	setb	_IE_4
                           0003E1  2148 	C$main.c$331$1$365 ==.
                           0003E1  2149 	XG$enable_radio_interrupt_in_mcu_pin$0$0 ==.
      0045C1 22               [24] 2150 	ret
                                   2151 ;------------------------------------------------------------
                                   2152 ;Allocation info for local variables in function 'disable_radio_interrupt_in_mcu_pin'
                                   2153 ;------------------------------------------------------------
                           0003E2  2154 	G$disable_radio_interrupt_in_mcu_pin$0$0 ==.
                           0003E2  2155 	C$main.c$333$1$365 ==.
                                   2156 ;	main.c:333: void disable_radio_interrupt_in_mcu_pin(void)
                                   2157 ;	-----------------------------------------
                                   2158 ;	 function disable_radio_interrupt_in_mcu_pin
                                   2159 ;	-----------------------------------------
      0045C2                       2160 _disable_radio_interrupt_in_mcu_pin:
                           0003E2  2161 	C$main.c$335$1$367 ==.
                                   2162 ;	main.c:335: IE_4 = 0;
      0045C2 C2 AC            [12] 2163 	clr	_IE_4
                           0003E4  2164 	C$main.c$336$1$367 ==.
                           0003E4  2165 	XG$disable_radio_interrupt_in_mcu_pin$0$0 ==.
      0045C4 22               [24] 2166 	ret
                                   2167 ;------------------------------------------------------------
                                   2168 ;Allocation info for local variables in function 'wakeup_callback'
                                   2169 ;------------------------------------------------------------
                                   2170 ;desc                      Allocated to registers 
                                   2171 ;timer0Period              Allocated with name '_wakeup_callback_timer0Period_1_369'
                                   2172 ;bc_flg                    Allocated with name '_wakeup_callback_bc_flg_1_369'
                                   2173 ;------------------------------------------------------------
                           0003E5  2174 	Fmain$wakeup_callback$0$0 ==.
                           0003E5  2175 	C$main.c$338$1$367 ==.
                                   2176 ;	main.c:338: static void wakeup_callback(struct wtimer_desc __xdata *desc)
                                   2177 ;	-----------------------------------------
                                   2178 ;	 function wakeup_callback
                                   2179 ;	-----------------------------------------
      0045C5                       2180 _wakeup_callback:
                           0003E5  2181 	C$main.c$349$1$369 ==.
                                   2182 ;	main.c:349: if ((rcv_flg == 0)&&(bc_flg == 0))
      0045C5 90 04 82         [24] 2183 	mov	dptr,#_rcv_flg
      0045C8 E0               [24] 2184 	movx	a,@dptr
      0045C9 FF               [12] 2185 	mov	r7,a
      0045CA 70 4D            [24] 2186 	jnz	00109$
      0045CC 90 02 B7         [24] 2187 	mov	dptr,#_wakeup_callback_bc_flg_1_369
      0045CF E0               [24] 2188 	movx	a,@dptr
      0045D0 70 47            [24] 2189 	jnz	00109$
                           0003F2  2190 	C$main.c$351$2$370 ==.
                                   2191 ;	main.c:351: timer0Period = WTIMER0_UPLINKRCV;
      0045D2 90 02 B5         [24] 2192 	mov	dptr,#_wakeup_callback_timer0Period_1_369
      0045D5 74 80            [12] 2193 	mov	a,#0x80
      0045D7 F0               [24] 2194 	movx	@dptr,a
      0045D8 74 11            [12] 2195 	mov	a,#0x11
      0045DA A3               [24] 2196 	inc	dptr
      0045DB F0               [24] 2197 	movx	@dptr,a
                           0003FC  2198 	C$main.c$352$2$370 ==.
                                   2199 ;	main.c:352: transmit_beacon();
      0045DC 12 42 74         [24] 2200 	lcall	_transmit_beacon
                           0003FF  2201 	C$main.c$353$2$370 ==.
                                   2202 ;	main.c:353: bc_flg = 1;
      0045DF 90 02 B7         [24] 2203 	mov	dptr,#_wakeup_callback_bc_flg_1_369
      0045E2 74 01            [12] 2204 	mov	a,#0x01
      0045E4 F0               [24] 2205 	movx	@dptr,a
                           000405  2206 	C$main.c$356$2$370 ==.
                                   2207 ;	main.c:356: dbglink_writestr("1_Beacon_WT0: ");
      0045E5 90 66 92         [24] 2208 	mov	dptr,#___str_11
      0045E8 75 F0 80         [24] 2209 	mov	b,#0x80
      0045EB 12 59 F7         [24] 2210 	lcall	_dbglink_writestr
                           00040E  2211 	C$main.c$357$2$370 ==.
                                   2212 ;	main.c:357: dbglink_writehex32(/*wtimer0_curtime()*/wt0_beacon, 8, WRNUM_PADZERO);
      0045EE 90 04 89         [24] 2213 	mov	dptr,#_wt0_beacon
      0045F1 E0               [24] 2214 	movx	a,@dptr
      0045F2 FB               [12] 2215 	mov	r3,a
      0045F3 A3               [24] 2216 	inc	dptr
      0045F4 E0               [24] 2217 	movx	a,@dptr
      0045F5 FC               [12] 2218 	mov	r4,a
      0045F6 A3               [24] 2219 	inc	dptr
      0045F7 E0               [24] 2220 	movx	a,@dptr
      0045F8 FD               [12] 2221 	mov	r5,a
      0045F9 A3               [24] 2222 	inc	dptr
      0045FA E0               [24] 2223 	movx	a,@dptr
      0045FB FE               [12] 2224 	mov	r6,a
      0045FC 74 08            [12] 2225 	mov	a,#0x08
      0045FE C0 E0            [24] 2226 	push	acc
      004600 C0 E0            [24] 2227 	push	acc
      004602 8B 82            [24] 2228 	mov	dpl,r3
      004604 8C 83            [24] 2229 	mov	dph,r4
      004606 8D F0            [24] 2230 	mov	b,r5
      004608 EE               [12] 2231 	mov	a,r6
      004609 12 5B D6         [24] 2232 	lcall	_dbglink_writehex32
      00460C 15 81            [12] 2233 	dec	sp
      00460E 15 81            [12] 2234 	dec	sp
                           000430  2235 	C$main.c$358$2$370 ==.
                                   2236 ;	main.c:358: dbglink_tx('\n');
      004610 75 82 0A         [24] 2237 	mov	dpl,#0x0a
      004613 12 4A 59         [24] 2238 	lcall	_dbglink_tx
      004616 02 46 BA         [24] 2239 	ljmp	00110$
      004619                       2240 00109$:
                           000439  2241 	C$main.c$362$1$369 ==.
                                   2242 ;	main.c:362: else if ((rcv_flg >= 1)&&(bc_flg == 1)) // received data packets from COM-D terminals
      004619 BF 01 00         [24] 2243 	cjne	r7,#0x01,00134$
      00461C                       2244 00134$:
      00461C 40 55            [24] 2245 	jc	00105$
      00461E 90 02 B7         [24] 2246 	mov	dptr,#_wakeup_callback_bc_flg_1_369
      004621 E0               [24] 2247 	movx	a,@dptr
      004622 FE               [12] 2248 	mov	r6,a
      004623 BE 01 4D         [24] 2249 	cjne	r6,#0x01,00105$
                           000446  2250 	C$main.c$364$2$371 ==.
                                   2251 ;	main.c:364: timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
      004626 90 02 B5         [24] 2252 	mov	dptr,#_wakeup_callback_timer0Period_1_369
      004629 74 80            [12] 2253 	mov	a,#0x80
      00462B F0               [24] 2254 	movx	@dptr,a
      00462C 74 07            [12] 2255 	mov	a,#0x07
      00462E A3               [24] 2256 	inc	dptr
      00462F F0               [24] 2257 	movx	@dptr,a
                           000450  2258 	C$main.c$365$2$371 ==.
                                   2259 ;	main.c:365: transmit_ack();
      004630 12 43 35         [24] 2260 	lcall	_transmit_ack
                           000453  2261 	C$main.c$366$2$371 ==.
                                   2262 ;	main.c:366: rcv_flg = 0;
      004633 90 04 82         [24] 2263 	mov	dptr,#_rcv_flg
      004636 E4               [12] 2264 	clr	a
      004637 F0               [24] 2265 	movx	@dptr,a
                           000458  2266 	C$main.c$367$2$371 ==.
                                   2267 ;	main.c:367: bc_flg = 0;
      004638 90 02 B7         [24] 2268 	mov	dptr,#_wakeup_callback_bc_flg_1_369
      00463B F0               [24] 2269 	movx	@dptr,a
                           00045C  2270 	C$main.c$368$2$371 ==.
                                   2271 ;	main.c:368: rcv_flg_id1 = 0;
      00463C 90 04 87         [24] 2272 	mov	dptr,#_rcv_flg_id1
      00463F F0               [24] 2273 	movx	@dptr,a
                           000460  2274 	C$main.c$369$2$371 ==.
                                   2275 ;	main.c:369: rcv_flg_id3 = 0;
      004640 90 04 88         [24] 2276 	mov	dptr,#_rcv_flg_id3
      004643 F0               [24] 2277 	movx	@dptr,a
                           000464  2278 	C$main.c$372$2$371 ==.
                                   2279 ;	main.c:372: dbglink_writestr("2_ACK_WT0: ");
      004644 90 66 A1         [24] 2280 	mov	dptr,#___str_12
      004647 75 F0 80         [24] 2281 	mov	b,#0x80
      00464A 12 59 F7         [24] 2282 	lcall	_dbglink_writestr
                           00046D  2283 	C$main.c$373$2$371 ==.
                                   2284 ;	main.c:373: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      00464D 12 63 49         [24] 2285 	lcall	_wtimer0_curtime
      004650 AB 82            [24] 2286 	mov	r3,dpl
      004652 AC 83            [24] 2287 	mov	r4,dph
      004654 AD F0            [24] 2288 	mov	r5,b
      004656 FE               [12] 2289 	mov	r6,a
      004657 74 08            [12] 2290 	mov	a,#0x08
      004659 C0 E0            [24] 2291 	push	acc
      00465B C0 E0            [24] 2292 	push	acc
      00465D 8B 82            [24] 2293 	mov	dpl,r3
      00465F 8C 83            [24] 2294 	mov	dph,r4
      004661 8D F0            [24] 2295 	mov	b,r5
      004663 EE               [12] 2296 	mov	a,r6
      004664 12 5B D6         [24] 2297 	lcall	_dbglink_writehex32
      004667 15 81            [12] 2298 	dec	sp
      004669 15 81            [12] 2299 	dec	sp
                           00048B  2300 	C$main.c$374$2$371 ==.
                                   2301 ;	main.c:374: dbglink_tx('\n');
      00466B 75 82 0A         [24] 2302 	mov	dpl,#0x0a
      00466E 12 4A 59         [24] 2303 	lcall	_dbglink_tx
      004671 80 47            [24] 2304 	sjmp	00110$
      004673                       2305 00105$:
                           000493  2306 	C$main.c$378$1$369 ==.
                                   2307 ;	main.c:378: else if ((rcv_flg == 0)&&(bc_flg == 1))
      004673 EF               [12] 2308 	mov	a,r7
      004674 70 44            [24] 2309 	jnz	00110$
      004676 90 02 B7         [24] 2310 	mov	dptr,#_wakeup_callback_bc_flg_1_369
      004679 E0               [24] 2311 	movx	a,@dptr
      00467A FF               [12] 2312 	mov	r7,a
      00467B BF 01 3C         [24] 2313 	cjne	r7,#0x01,00110$
                           00049E  2314 	C$main.c$380$2$372 ==.
                                   2315 ;	main.c:380: timer0Period = WTIMER0_PERIOD-WTIMER0_UPLINKRCV;
      00467E 90 02 B5         [24] 2316 	mov	dptr,#_wakeup_callback_timer0Period_1_369
      004681 74 80            [12] 2317 	mov	a,#0x80
      004683 F0               [24] 2318 	movx	@dptr,a
      004684 74 07            [12] 2319 	mov	a,#0x07
      004686 A3               [24] 2320 	inc	dptr
      004687 F0               [24] 2321 	movx	@dptr,a
                           0004A8  2322 	C$main.c$381$2$372 ==.
                                   2323 ;	main.c:381: bc_flg = 0;
      004688 90 02 B7         [24] 2324 	mov	dptr,#_wakeup_callback_bc_flg_1_369
      00468B E4               [12] 2325 	clr	a
      00468C F0               [24] 2326 	movx	@dptr,a
                           0004AD  2327 	C$main.c$384$2$372 ==.
                                   2328 ;	main.c:384: dbglink_writestr("3_NoACK_WT0: ");
      00468D 90 66 AD         [24] 2329 	mov	dptr,#___str_13
      004690 75 F0 80         [24] 2330 	mov	b,#0x80
      004693 12 59 F7         [24] 2331 	lcall	_dbglink_writestr
                           0004B6  2332 	C$main.c$385$2$372 ==.
                                   2333 ;	main.c:385: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004696 12 63 49         [24] 2334 	lcall	_wtimer0_curtime
      004699 AC 82            [24] 2335 	mov	r4,dpl
      00469B AD 83            [24] 2336 	mov	r5,dph
      00469D AE F0            [24] 2337 	mov	r6,b
      00469F FF               [12] 2338 	mov	r7,a
      0046A0 74 08            [12] 2339 	mov	a,#0x08
      0046A2 C0 E0            [24] 2340 	push	acc
      0046A4 C0 E0            [24] 2341 	push	acc
      0046A6 8C 82            [24] 2342 	mov	dpl,r4
      0046A8 8D 83            [24] 2343 	mov	dph,r5
      0046AA 8E F0            [24] 2344 	mov	b,r6
      0046AC EF               [12] 2345 	mov	a,r7
      0046AD 12 5B D6         [24] 2346 	lcall	_dbglink_writehex32
      0046B0 15 81            [12] 2347 	dec	sp
      0046B2 15 81            [12] 2348 	dec	sp
                           0004D4  2349 	C$main.c$386$2$372 ==.
                                   2350 ;	main.c:386: dbglink_tx('\n');
      0046B4 75 82 0A         [24] 2351 	mov	dpl,#0x0a
      0046B7 12 4A 59         [24] 2352 	lcall	_dbglink_tx
      0046BA                       2353 00110$:
                           0004DA  2354 	C$main.c$390$1$369 ==.
                                   2355 ;	main.c:390: wakeup_desc.time += wtimer0_correctinterval(timer0Period);
      0046BA 90 02 A4         [24] 2356 	mov	dptr,#(_wakeup_desc + 0x0004)
      0046BD E0               [24] 2357 	movx	a,@dptr
      0046BE FC               [12] 2358 	mov	r4,a
      0046BF A3               [24] 2359 	inc	dptr
      0046C0 E0               [24] 2360 	movx	a,@dptr
      0046C1 FD               [12] 2361 	mov	r5,a
      0046C2 A3               [24] 2362 	inc	dptr
      0046C3 E0               [24] 2363 	movx	a,@dptr
      0046C4 FE               [12] 2364 	mov	r6,a
      0046C5 A3               [24] 2365 	inc	dptr
      0046C6 E0               [24] 2366 	movx	a,@dptr
      0046C7 FF               [12] 2367 	mov	r7,a
      0046C8 90 02 B5         [24] 2368 	mov	dptr,#_wakeup_callback_timer0Period_1_369
      0046CB E0               [24] 2369 	movx	a,@dptr
      0046CC FA               [12] 2370 	mov	r2,a
      0046CD A3               [24] 2371 	inc	dptr
      0046CE E0               [24] 2372 	movx	a,@dptr
      0046CF FB               [12] 2373 	mov	r3,a
      0046D0 8A 00            [24] 2374 	mov	ar0,r2
      0046D2 8B 01            [24] 2375 	mov	ar1,r3
      0046D4 7A 00            [12] 2376 	mov	r2,#0x00
      0046D6 7B 00            [12] 2377 	mov	r3,#0x00
      0046D8 88 82            [24] 2378 	mov	dpl,r0
      0046DA 89 83            [24] 2379 	mov	dph,r1
      0046DC 8A F0            [24] 2380 	mov	b,r2
      0046DE EB               [12] 2381 	mov	a,r3
      0046DF C0 07            [24] 2382 	push	ar7
      0046E1 C0 06            [24] 2383 	push	ar6
      0046E3 C0 05            [24] 2384 	push	ar5
      0046E5 C0 04            [24] 2385 	push	ar4
      0046E7 12 57 74         [24] 2386 	lcall	_wtimer0_correctinterval
      0046EA A8 82            [24] 2387 	mov	r0,dpl
      0046EC A9 83            [24] 2388 	mov	r1,dph
      0046EE AA F0            [24] 2389 	mov	r2,b
      0046F0 FB               [12] 2390 	mov	r3,a
      0046F1 D0 04            [24] 2391 	pop	ar4
      0046F3 D0 05            [24] 2392 	pop	ar5
      0046F5 D0 06            [24] 2393 	pop	ar6
      0046F7 D0 07            [24] 2394 	pop	ar7
      0046F9 E8               [12] 2395 	mov	a,r0
      0046FA 2C               [12] 2396 	add	a,r4
      0046FB FC               [12] 2397 	mov	r4,a
      0046FC E9               [12] 2398 	mov	a,r1
      0046FD 3D               [12] 2399 	addc	a,r5
      0046FE FD               [12] 2400 	mov	r5,a
      0046FF EA               [12] 2401 	mov	a,r2
      004700 3E               [12] 2402 	addc	a,r6
      004701 FE               [12] 2403 	mov	r6,a
      004702 EB               [12] 2404 	mov	a,r3
      004703 3F               [12] 2405 	addc	a,r7
      004704 FF               [12] 2406 	mov	r7,a
      004705 90 02 A4         [24] 2407 	mov	dptr,#(_wakeup_desc + 0x0004)
      004708 EC               [12] 2408 	mov	a,r4
      004709 F0               [24] 2409 	movx	@dptr,a
      00470A ED               [12] 2410 	mov	a,r5
      00470B A3               [24] 2411 	inc	dptr
      00470C F0               [24] 2412 	movx	@dptr,a
      00470D EE               [12] 2413 	mov	a,r6
      00470E A3               [24] 2414 	inc	dptr
      00470F F0               [24] 2415 	movx	@dptr,a
      004710 EF               [12] 2416 	mov	a,r7
      004711 A3               [24] 2417 	inc	dptr
      004712 F0               [24] 2418 	movx	@dptr,a
                           000533  2419 	C$main.c$391$1$369 ==.
                                   2420 ;	main.c:391: wtimer0_addabsolute(&wakeup_desc);
      004713 90 02 A0         [24] 2421 	mov	dptr,#_wakeup_desc
      004716 12 53 19         [24] 2422 	lcall	_wtimer0_addabsolute
                           000539  2423 	C$main.c$392$1$369 ==.
                           000539  2424 	XFmain$wakeup_callback$0$0 ==.
      004719 22               [24] 2425 	ret
                                   2426 ;------------------------------------------------------------
                                   2427 ;Allocation info for local variables in function '_sdcc_external_startup'
                                   2428 ;------------------------------------------------------------
                                   2429 ;c                         Allocated to registers 
                                   2430 ;p                         Allocated to registers 
                                   2431 ;c                         Allocated to registers 
                                   2432 ;p                         Allocated to registers 
                                   2433 ;------------------------------------------------------------
                           00053A  2434 	G$_sdcc_external_startup$0$0 ==.
                           00053A  2435 	C$main.c$394$1$369 ==.
                                   2436 ;	main.c:394: uint8_t _sdcc_external_startup(void)
                                   2437 ;	-----------------------------------------
                                   2438 ;	 function _sdcc_external_startup
                                   2439 ;	-----------------------------------------
      00471A                       2440 __sdcc_external_startup:
                           00053A  2441 	C$main.c$396$1$374 ==.
                                   2442 ;	main.c:396: LPXOSCGM = 0x8A;
      00471A 90 70 54         [24] 2443 	mov	dptr,#_LPXOSCGM
      00471D 74 8A            [12] 2444 	mov	a,#0x8a
      00471F F0               [24] 2445 	movx	@dptr,a
                           000540  2446 	C$main.c$397$2$375 ==.
                                   2447 ;	main.c:397: wtimer0_setclksrc(WTIMER0_CLKSRC, WTIMER0_PRESCALER);
      004720 75 82 09         [24] 2448 	mov	dpl,#0x09
      004723 12 4D A8         [24] 2449 	lcall	_wtimer0_setconfig
                           000546  2450 	C$main.c$398$2$376 ==.
                                   2451 ;	main.c:398: wtimer1_setclksrc(CLKSRC_FRCOSC, 7);
      004726 75 82 38         [24] 2452 	mov	dpl,#0x38
      004729 12 4D C2         [24] 2453 	lcall	_wtimer1_setconfig
                           00054C  2454 	C$main.c$400$1$374 ==.
                                   2455 ;	main.c:400: LPOSCCONFIG = 0x09; /* Slow, PRESC /1, no cal. Does NOT enable LPOSC. LPOSC is enabled upon configuring WTCFGA (MODE_TX_PERIODIC and receive_ack() ) */
      00472C 90 70 60         [24] 2456 	mov	dptr,#_LPOSCCONFIG
      00472F 74 09            [12] 2457 	mov	a,#0x09
      004731 F0               [24] 2458 	movx	@dptr,a
                           000552  2459 	C$main.c$402$1$374 ==.
                                   2460 ;	main.c:402: coldstart = !(PCON & 0x40);
      004732 E5 87            [12] 2461 	mov	a,_PCON
      004734 A2 E6            [12] 2462 	mov	c,acc[6]
      004736 B3               [12] 2463 	cpl	c
      004737 92 01            [24] 2464 	mov	__sdcc_external_startup_sloc0_1_0,c
      004739 E4               [12] 2465 	clr	a
      00473A 33               [12] 2466 	rlc	a
      00473B F5 24            [12] 2467 	mov	_coldstart,a
                           00055D  2468 	C$main.c$404$1$374 ==.
                                   2469 ;	main.c:404: ANALOGA = 0x18; /* PA[3,4] LPXOSC, other PA are used as digital pins */
      00473D 90 70 07         [24] 2470 	mov	dptr,#_ANALOGA
      004740 74 18            [12] 2471 	mov	a,#0x18
      004742 F0               [24] 2472 	movx	@dptr,a
                           000563  2473 	C$main.c$405$1$374 ==.
                                   2474 ;	main.c:405: PORTA = 0xE7; /* pull ups except for LPXOSC pin PA[3,4]; */
      004743 75 80 E7         [24] 2475 	mov	_PORTA,#0xe7
                           000566  2476 	C$main.c$406$1$374 ==.
                                   2477 ;	main.c:406: PORTB = 0xFD | (PINB & 0x02); /* init LEDs to previous (frozen) state */
      004746 74 02            [12] 2478 	mov	a,#0x02
      004748 55 E8            [12] 2479 	anl	a,_PINB
      00474A 44 FD            [12] 2480 	orl	a,#0xfd
      00474C F5 88            [12] 2481 	mov	_PORTB,a
                           00056E  2482 	C$main.c$407$1$374 ==.
                                   2483 ;	main.c:407: PORTC = 0xFF;
      00474E 75 90 FF         [24] 2484 	mov	_PORTC,#0xff
                           000571  2485 	C$main.c$408$1$374 ==.
                                   2486 ;	main.c:408: PORTR = 0x0B;
      004751 75 8C 0B         [24] 2487 	mov	_PORTR,#0x0b
                           000574  2488 	C$main.c$410$1$374 ==.
                                   2489 ;	main.c:410: DIRA = 0x00;
      004754 75 89 00         [24] 2490 	mov	_DIRA,#0x00
                           000577  2491 	C$main.c$411$1$374 ==.
                                   2492 ;	main.c:411: DIRB = 0x0e; /*  PB1 = LED; PB2 / PB3 are outputs (in case PWRAMP / ANSTSEL are used) */
      004757 75 8A 0E         [24] 2493 	mov	_DIRB,#0x0e
                           00057A  2494 	C$main.c$412$1$374 ==.
                                   2495 ;	main.c:412: DIRC = 0x00; /*  PC4 = button */
      00475A 75 8B 00         [24] 2496 	mov	_DIRC,#0x00
                           00057D  2497 	C$main.c$413$1$374 ==.
                                   2498 ;	main.c:413: DIRR = 0x15;
      00475D 75 8E 15         [24] 2499 	mov	_DIRR,#0x15
                           000580  2500 	C$main.c$414$1$374 ==.
                                   2501 ;	main.c:414: axradio_setup_pincfg1();
      004760 12 06 E3         [24] 2502 	lcall	_axradio_setup_pincfg1
                           000583  2503 	C$main.c$415$1$374 ==.
                                   2504 ;	main.c:415: DPS = 0;
      004763 75 86 00         [24] 2505 	mov	_DPS,#0x00
                           000586  2506 	C$main.c$416$1$374 ==.
                                   2507 ;	main.c:416: IE = 0x40;
      004766 75 A8 40         [24] 2508 	mov	_IE,#0x40
                           000589  2509 	C$main.c$417$1$374 ==.
                                   2510 ;	main.c:417: EIE = 0x00;
      004769 75 98 00         [24] 2511 	mov	_EIE,#0x00
                           00058C  2512 	C$main.c$418$1$374 ==.
                                   2513 ;	main.c:418: E2IE = 0x00;
      00476C 75 A0 00         [24] 2514 	mov	_E2IE,#0x00
                           00058F  2515 	C$main.c$420$1$374 ==.
                                   2516 ;	main.c:420: display_portinit();
      00476F 12 0D CB         [24] 2517 	lcall	_com0_portinit
                           000592  2518 	C$main.c$421$1$374 ==.
                                   2519 ;	main.c:421: GPIOENABLE = 1; /* unfreeze GPIO */
      004772 90 70 0C         [24] 2520 	mov	dptr,#_GPIOENABLE
      004775 74 01            [12] 2521 	mov	a,#0x01
      004777 F0               [24] 2522 	movx	@dptr,a
                           000598  2523 	C$main.c$422$1$374 ==.
                                   2524 ;	main.c:422: return !coldstart; /* coldstart -> return 0 -> var initialization; start from sleep -> return 1 -> no var initialization */
      004778 E5 24            [12] 2525 	mov	a,_coldstart
      00477A B4 01 00         [24] 2526 	cjne	a,#0x01,00109$
      00477D                       2527 00109$:
      00477D 92 01            [24] 2528 	mov  __sdcc_external_startup_sloc0_1_0,c
      00477F E4               [12] 2529 	clr	a
      004780 33               [12] 2530 	rlc	a
      004781 F5 82            [12] 2531 	mov	dpl,a
                           0005A3  2532 	C$main.c$423$1$374 ==.
                           0005A3  2533 	XG$_sdcc_external_startup$0$0 ==.
      004783 22               [24] 2534 	ret
                                   2535 ;------------------------------------------------------------
                                   2536 ;Allocation info for local variables in function 'main'
                                   2537 ;------------------------------------------------------------
                                   2538 ;saved_button_state        Allocated with name '_main_saved_button_state_1_378'
                                   2539 ;i                         Allocated to registers r7 
                                   2540 ;x                         Allocated to registers r6 
                                   2541 ;flg                       Allocated to registers r6 
                                   2542 ;flg                       Allocated to registers r7 
                                   2543 ;------------------------------------------------------------
                           0005A4  2544 	G$main$0$0 ==.
                           0005A4  2545 	C$main.c$425$1$374 ==.
                                   2546 ;	main.c:425: int main(void)
                                   2547 ;	-----------------------------------------
                                   2548 ;	 function main
                                   2549 ;	-----------------------------------------
      004784                       2550 _main:
                           0005A4  2551 	C$main.c$432$1$378 ==.
                                   2552 ;	main.c:432: __endasm;
                           000000  2553 	G$_start__stack$0$0	= __start__stack
                                   2554 	.globl	G$_start__stack$0$0
                           0005A4  2555 	C$main.c$434$1$378 ==.
                                   2556 ;	main.c:434: dbglink_init();
      004784 12 53 06         [24] 2557 	lcall	_dbglink_init
                           0005A7  2558 	C$libmftypes.h$368$4$394 ==.
                                   2559 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
      004787 D2 AF            [12] 2560 	setb	_EA
                           0005A9  2561 	C$main.c$437$1$378 ==.
                                   2562 ;	main.c:437: flash_apply_calibration();
      004789 12 55 DF         [24] 2563 	lcall	_flash_apply_calibration
                           0005AC  2564 	C$main.c$438$1$378 ==.
                                   2565 ;	main.c:438: CLKCON = 0x00;
      00478C 75 C6 00         [24] 2566 	mov	_CLKCON,#0x00
                           0005AF  2567 	C$main.c$439$1$378 ==.
                                   2568 ;	main.c:439: wtimer_init();
      00478F 12 4E 71         [24] 2569 	lcall	_wtimer_init
                           0005B2  2570 	C$main.c$441$1$378 ==.
                                   2571 ;	main.c:441: if (coldstart)
      004792 E5 24            [12] 2572 	mov	a,_coldstart
      004794 70 03            [24] 2573 	jnz	00193$
      004796 02 48 86         [24] 2574 	ljmp	00122$
      004799                       2575 00193$:
                           0005B9  2576 	C$main.c$443$4$381 ==.
                                   2577 ;	main.c:443: led0_off();
      004799 C2 89            [12] 2578 	clr	_PORTB_1
                           0005BB  2579 	C$main.c$445$2$379 ==.
                                   2580 ;	main.c:445: wakeup_desc.handler = wakeup_callback;
      00479B 90 02 A2         [24] 2581 	mov	dptr,#(_wakeup_desc + 0x0002)
      00479E 74 C5            [12] 2582 	mov	a,#_wakeup_callback
      0047A0 F0               [24] 2583 	movx	@dptr,a
      0047A1 74 45            [12] 2584 	mov	a,#(_wakeup_callback >> 8)
      0047A3 A3               [24] 2585 	inc	dptr
      0047A4 F0               [24] 2586 	movx	@dptr,a
                           0005C5  2587 	C$main.c$447$2$379 ==.
                                   2588 ;	main.c:447: display_init();
      0047A5 12 0E 19         [24] 2589 	lcall	_com0_init
                           0005C8  2590 	C$main.c$448$2$379 ==.
                                   2591 ;	main.c:448: display_setpos(0);
      0047A8 75 82 00         [24] 2592 	mov	dpl,#0x00
      0047AB 12 0E C1         [24] 2593 	lcall	_com0_setpos
                           0005CE  2594 	C$main.c$449$2$379 ==.
                                   2595 ;	main.c:449: i = axradio_init();
      0047AE 12 2F AB         [24] 2596 	lcall	_axradio_init
                           0005D1  2597 	C$main.c$451$2$379 ==.
                                   2598 ;	main.c:451: if (i != AXRADIO_ERR_NOERROR)
      0047B1 E5 82            [12] 2599 	mov	a,dpl
      0047B3 FF               [12] 2600 	mov	r7,a
      0047B4 60 25            [24] 2601 	jz	00112$
                           0005D6  2602 	C$main.c$453$3$382 ==.
                                   2603 ;	main.c:453: if (i == AXRADIO_ERR_NOCHIP)
      0047B6 BF 05 02         [24] 2604 	cjne	r7,#0x05,00195$
      0047B9 80 03            [24] 2605 	sjmp	00196$
      0047BB                       2606 00195$:
      0047BB 02 48 BB         [24] 2607 	ljmp	00129$
      0047BE                       2608 00196$:
                           0005DE  2609 	C$main.c$455$4$383 ==.
                                   2610 ;	main.c:455: display_writestr(radio_not_found_lcd_display);
      0047BE 90 04 5C         [24] 2611 	mov	dptr,#_radio_not_found_lcd_display
      0047C1 75 F0 00         [24] 2612 	mov	b,#0x00
      0047C4 12 0E DD         [24] 2613 	lcall	_com0_writestr
                           0005E7  2614 	C$main.c$458$4$383 ==.
                                   2615 ;	main.c:458: if(DBGLNKSTAT & 0x10)
      0047C7 E5 E2            [12] 2616 	mov	a,_DBGLNKSTAT
      0047C9 20 E4 03         [24] 2617 	jb	acc.4,00197$
      0047CC 02 48 C9         [24] 2618 	ljmp	00141$
      0047CF                       2619 00197$:
                           0005EF  2620 	C$main.c$459$4$383 ==.
                                   2621 ;	main.c:459: dbglink_writestr(radio_not_found_lcd_display);
      0047CF 90 04 5C         [24] 2622 	mov	dptr,#_radio_not_found_lcd_display
      0047D2 75 F0 00         [24] 2623 	mov	b,#0x00
      0047D5 12 59 F7         [24] 2624 	lcall	_dbglink_writestr
                           0005F8  2625 	C$main.c$462$4$383 ==.
                                   2626 ;	main.c:462: goto terminate_error;
      0047D8 02 48 C9         [24] 2627 	ljmp	00141$
                           0005FB  2628 	C$main.c$465$2$379 ==.
                                   2629 ;	main.c:465: goto terminate_radio_error;
      0047DB                       2630 00112$:
                           0005FB  2631 	C$main.c$468$2$379 ==.
                                   2632 ;	main.c:468: display_writestr(radio_lcd_display);
      0047DB 90 04 4E         [24] 2633 	mov	dptr,#_radio_lcd_display
      0047DE 75 F0 00         [24] 2634 	mov	b,#0x00
      0047E1 12 0E DD         [24] 2635 	lcall	_com0_writestr
                           000604  2636 	C$main.c$471$2$379 ==.
                                   2637 ;	main.c:471: if (DBGLNKSTAT & 0x10)
      0047E4 E5 E2            [12] 2638 	mov	a,_DBGLNKSTAT
      0047E6 30 E4 09         [24] 2639 	jnb	acc.4,00114$
                           000609  2640 	C$main.c$472$2$379 ==.
                                   2641 ;	main.c:472: dbglink_writestr(radio_lcd_display);
      0047E9 90 04 4E         [24] 2642 	mov	dptr,#_radio_lcd_display
      0047EC 75 F0 00         [24] 2643 	mov	b,#0x00
      0047EF 12 59 F7         [24] 2644 	lcall	_dbglink_writestr
      0047F2                       2645 00114$:
                           000612  2646 	C$main.c$475$2$379 ==.
                                   2647 ;	main.c:475: axradio_set_local_address(&localaddr);
      0047F2 90 64 C4         [24] 2648 	mov	dptr,#_localaddr
      0047F5 75 F0 80         [24] 2649 	mov	b,#0x80
      0047F8 12 3A 61         [24] 2650 	lcall	_axradio_set_local_address
                           00061B  2651 	C$main.c$476$2$379 ==.
                                   2652 ;	main.c:476: axradio_set_default_remote_address(&remoteaddr);
      0047FB 90 64 BF         [24] 2653 	mov	dptr,#_remoteaddr
      0047FE 75 F0 80         [24] 2654 	mov	b,#0x80
      004801 12 3A 9F         [24] 2655 	lcall	_axradio_set_default_remote_address
                           000624  2656 	C$main.c$480$2$379 ==.
                                   2657 ;	main.c:480: if (DBGLNKSTAT & 0x10)
      004804 E5 E2            [12] 2658 	mov	a,_DBGLNKSTAT
      004806 30 E4 4C         [24] 2659 	jnb	acc.4,00118$
                           000629  2660 	C$main.c$482$3$384 ==.
                                   2661 ;	main.c:482: dbglink_writestr("RNG = ");
      004809 90 66 BB         [24] 2662 	mov	dptr,#___str_14
      00480C 75 F0 80         [24] 2663 	mov	b,#0x80
      00480F 12 59 F7         [24] 2664 	lcall	_dbglink_writestr
                           000632  2665 	C$main.c$483$3$384 ==.
                                   2666 ;	main.c:483: dbglink_writenum16(axradio_get_pllrange(), 2, 0);
      004812 12 38 F3         [24] 2667 	lcall	_axradio_get_pllrange
      004815 E4               [12] 2668 	clr	a
      004816 C0 E0            [24] 2669 	push	acc
      004818 74 02            [12] 2670 	mov	a,#0x02
      00481A C0 E0            [24] 2671 	push	acc
      00481C 12 62 4D         [24] 2672 	lcall	_dbglink_writenum16
      00481F 15 81            [12] 2673 	dec	sp
      004821 15 81            [12] 2674 	dec	sp
                           000643  2675 	C$main.c$485$4$385 ==.
                                   2676 ;	main.c:485: uint8_t x = axradio_get_pllvcoi();
      004823 12 39 12         [24] 2677 	lcall	_axradio_get_pllvcoi
                           000646  2678 	C$main.c$487$4$385 ==.
                                   2679 ;	main.c:487: if (x & 0x80)
      004826 E5 82            [12] 2680 	mov	a,dpl
      004828 FE               [12] 2681 	mov	r6,a
      004829 30 E7 20         [24] 2682 	jnb	acc.7,00116$
                           00064C  2683 	C$main.c$489$5$386 ==.
                                   2684 ;	main.c:489: dbglink_writestr("\nVCOI = ");
      00482C 90 66 C2         [24] 2685 	mov	dptr,#___str_15
      00482F 75 F0 80         [24] 2686 	mov	b,#0x80
      004832 C0 06            [24] 2687 	push	ar6
      004834 12 59 F7         [24] 2688 	lcall	_dbglink_writestr
      004837 D0 06            [24] 2689 	pop	ar6
                           000659  2690 	C$main.c$490$5$386 ==.
                                   2691 ;	main.c:490: dbglink_writehex16(x, 2, 0);
      004839 E4               [12] 2692 	clr	a
      00483A FD               [12] 2693 	mov	r5,a
      00483B C0 E0            [24] 2694 	push	acc
      00483D 74 02            [12] 2695 	mov	a,#0x02
      00483F C0 E0            [24] 2696 	push	acc
      004841 8E 82            [24] 2697 	mov	dpl,r6
      004843 8D 83            [24] 2698 	mov	dph,r5
      004845 12 5D F4         [24] 2699 	lcall	_dbglink_writehex16
      004848 15 81            [12] 2700 	dec	sp
      00484A 15 81            [12] 2701 	dec	sp
      00484C                       2702 00116$:
                           00066C  2703 	C$main.c$493$3$384 ==.
                                   2704 ;	main.c:493: dbglink_writestr("\n\nMASTER\n");
      00484C 90 66 CB         [24] 2705 	mov	dptr,#___str_16
      00484F 75 F0 80         [24] 2706 	mov	b,#0x80
      004852 12 59 F7         [24] 2707 	lcall	_dbglink_writestr
      004855                       2708 00118$:
                           000675  2709 	C$main.c$497$2$379 ==.
                                   2710 ;	main.c:497: i = axradio_set_mode(AXRADIO_MODE_ASYNC_RECEIVE);
      004855 75 82 20         [24] 2711 	mov	dpl,#0x20
      004858 12 33 AF         [24] 2712 	lcall	_axradio_set_mode
                           00067B  2713 	C$main.c$499$2$379 ==.
                                   2714 ;	main.c:499: if (i != AXRADIO_ERR_NOERROR)
      00485B E5 82            [12] 2715 	mov	a,dpl
      00485D FF               [12] 2716 	mov	r7,a
      00485E 70 5B            [24] 2717 	jnz	00129$
                           000680  2718 	C$main.c$503$2$379 ==.
                                   2719 ;	main.c:503: wakeup_desc.time = wtimer0_correctinterval(WTIMER0_PERIOD);
      004860 90 19 00         [24] 2720 	mov	dptr,#0x1900
      004863 E4               [12] 2721 	clr	a
      004864 F5 F0            [12] 2722 	mov	b,a
      004866 12 57 74         [24] 2723 	lcall	_wtimer0_correctinterval
      004869 AB 82            [24] 2724 	mov	r3,dpl
      00486B AC 83            [24] 2725 	mov	r4,dph
      00486D AD F0            [24] 2726 	mov	r5,b
      00486F FE               [12] 2727 	mov	r6,a
      004870 90 02 A4         [24] 2728 	mov	dptr,#(_wakeup_desc + 0x0004)
      004873 EB               [12] 2729 	mov	a,r3
      004874 F0               [24] 2730 	movx	@dptr,a
      004875 EC               [12] 2731 	mov	a,r4
      004876 A3               [24] 2732 	inc	dptr
      004877 F0               [24] 2733 	movx	@dptr,a
      004878 ED               [12] 2734 	mov	a,r5
      004879 A3               [24] 2735 	inc	dptr
      00487A F0               [24] 2736 	movx	@dptr,a
      00487B EE               [12] 2737 	mov	a,r6
      00487C A3               [24] 2738 	inc	dptr
      00487D F0               [24] 2739 	movx	@dptr,a
                           00069E  2740 	C$main.c$504$2$379 ==.
                                   2741 ;	main.c:504: wtimer0_addrelative(&wakeup_desc);
      00487E 90 02 A0         [24] 2742 	mov	dptr,#_wakeup_desc
      004881 12 51 38         [24] 2743 	lcall	_wtimer0_addrelative
      004884 80 05            [24] 2744 	sjmp	00123$
      004886                       2745 00122$:
                           0006A6  2746 	C$main.c$510$2$387 ==.
                                   2747 ;	main.c:510: axradio_commsleepexit();
      004886 12 3F 62         [24] 2748 	lcall	_axradio_commsleepexit
                           0006A9  2749 	C$main.c$511$2$387 ==.
                                   2750 ;	main.c:511: IE_4 = 1; /* enable radio interrupt */
      004889 D2 AC            [12] 2751 	setb	_IE_4
      00488B                       2752 00123$:
                           0006AB  2753 	C$main.c$514$1$378 ==.
                                   2754 ;	main.c:514: axradio_setup_pincfg2();
      00488B 12 06 EA         [24] 2755 	lcall	_axradio_setup_pincfg2
      00488E                       2756 00139$:
                           0006AE  2757 	C$main.c$530$2$388 ==.
                                   2758 ;	main.c:530: wtimer_runcallbacks();
      00488E 12 50 29         [24] 2759 	lcall	_wtimer_runcallbacks
                           0006B1  2760 	C$libmftypes.h$373$5$397 ==.
                                   2761 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:373: EA = 0;
      004891 C2 AF            [12] 2762 	clr	_EA
                           0006B3  2763 	C$main.c$533$3$388 ==.
                                   2764 ;	main.c:533: uint8_t flg = WTFLAG_CANSTANDBY;
      004893 7E 02            [12] 2765 	mov	r6,#0x02
                           0006B5  2766 	C$main.c$536$3$389 ==.
                                   2767 ;	main.c:536: if (axradio_cansleep()
      004895 C0 06            [24] 2768 	push	ar6
      004897 12 33 9D         [24] 2769 	lcall	_axradio_cansleep
      00489A E5 82            [12] 2770 	mov	a,dpl
      00489C D0 06            [24] 2771 	pop	ar6
      00489E 60 10            [24] 2772 	jz	00125$
                           0006C0  2773 	C$main.c$538$3$389 ==.
                                   2774 ;	main.c:538: && dbglink_txidle()
      0048A0 12 52 E8         [24] 2775 	lcall	_dbglink_txidle
      0048A3 E5 82            [12] 2776 	mov	a,dpl
      0048A5 60 09            [24] 2777 	jz	00125$
                           0006C7  2778 	C$main.c$540$3$389 ==.
                                   2779 ;	main.c:540: && display_txidle())
      0048A7 12 55 71         [24] 2780 	lcall	_uart0_txidle
      0048AA E5 82            [12] 2781 	mov	a,dpl
      0048AC 60 02            [24] 2782 	jz	00125$
                           0006CE  2783 	C$main.c$541$3$389 ==.
                                   2784 ;	main.c:541: flg |= WTFLAG_CANSLEEP;
      0048AE 7E 03            [12] 2785 	mov	r6,#0x03
      0048B0                       2786 00125$:
                           0006D0  2787 	C$main.c$543$3$389 ==.
                                   2788 ;	main.c:543: wtimer_idle(flg);
      0048B0 8E 82            [24] 2789 	mov	dpl,r6
      0048B2 12 4F A5         [24] 2790 	lcall	_wtimer_idle
                           0006D5  2791 	C$main.c$545$2$388 ==.
                                   2792 ;	main.c:545: IE_3 = 0; /* no ISR! */
      0048B5 C2 AB            [12] 2793 	clr	_IE_3
                           0006D7  2794 	C$libmftypes.h$368$5$400 ==.
                                   2795 ;	C:/Program Files (x86)/ON Semiconductor/AXSDB/libmf/include/libmftypes.h:368: EA = 1;
      0048B7 D2 AF            [12] 2796 	setb	_EA
                           0006D9  2797 	C$main.c$546$4$399 ==.
                                   2798 ;	main.c:546: __enable_irq();
                           0006D9  2799 	C$main.c$549$1$378 ==.
                                   2800 ;	main.c:549: terminate_radio_error:
      0048B9 80 D3            [24] 2801 	sjmp	00139$
      0048BB                       2802 00129$:
                           0006DB  2803 	C$main.c$550$1$378 ==.
                                   2804 ;	main.c:550: display_radio_error(i);
      0048BB 8F 82            [24] 2805 	mov	dpl,r7
      0048BD C0 07            [24] 2806 	push	ar7
      0048BF 12 3F D4         [24] 2807 	lcall	_com0_display_radio_error
      0048C2 D0 07            [24] 2808 	pop	ar7
                           0006E4  2809 	C$main.c$552$1$378 ==.
                                   2810 ;	main.c:552: dbglink_display_radio_error(i);
      0048C4 8F 82            [24] 2811 	mov	dpl,r7
      0048C6 12 40 23         [24] 2812 	lcall	_dbglink_display_radio_error
                           0006E9  2813 	C$main.c$554$1$378 ==.
                                   2814 ;	main.c:554: terminate_error:
      0048C9                       2815 00141$:
                           0006E9  2816 	C$main.c$558$2$390 ==.
                                   2817 ;	main.c:558: wtimer_runcallbacks();
      0048C9 12 50 29         [24] 2818 	lcall	_wtimer_runcallbacks
                           0006EC  2819 	C$main.c$560$3$390 ==.
                                   2820 ;	main.c:560: uint8_t flg = WTFLAG_CANSTANDBY;
      0048CC 7F 02            [12] 2821 	mov	r7,#0x02
                           0006EE  2822 	C$main.c$563$3$391 ==.
                                   2823 ;	main.c:563: if (axradio_cansleep()
      0048CE C0 07            [24] 2824 	push	ar7
      0048D0 12 33 9D         [24] 2825 	lcall	_axradio_cansleep
      0048D3 E5 82            [12] 2826 	mov	a,dpl
      0048D5 D0 07            [24] 2827 	pop	ar7
      0048D7 60 10            [24] 2828 	jz	00132$
                           0006F9  2829 	C$main.c$565$3$391 ==.
                                   2830 ;	main.c:565: && dbglink_txidle()
      0048D9 12 52 E8         [24] 2831 	lcall	_dbglink_txidle
      0048DC E5 82            [12] 2832 	mov	a,dpl
      0048DE 60 09            [24] 2833 	jz	00132$
                           000700  2834 	C$main.c$567$3$391 ==.
                                   2835 ;	main.c:567: && display_txidle())
      0048E0 12 55 71         [24] 2836 	lcall	_uart0_txidle
      0048E3 E5 82            [12] 2837 	mov	a,dpl
      0048E5 60 02            [24] 2838 	jz	00132$
                           000707  2839 	C$main.c$568$3$391 ==.
                                   2840 ;	main.c:568: flg |= WTFLAG_CANSLEEP;
      0048E7 7F 03            [12] 2841 	mov	r7,#0x03
      0048E9                       2842 00132$:
                           000709  2843 	C$main.c$570$3$391 ==.
                                   2844 ;	main.c:570: wtimer_idle(flg);
      0048E9 8F 82            [24] 2845 	mov	dpl,r7
      0048EB 12 4F A5         [24] 2846 	lcall	_wtimer_idle
      0048EE 80 D9            [24] 2847 	sjmp	00141$
                           000710  2848 	C$main.c$573$1$378 ==.
                           000710  2849 	XG$main$0$0 ==.
      0048F0 22               [24] 2850 	ret
                                   2851 	.area CSEG    (CODE)
                                   2852 	.area CONST   (CODE)
                           000000  2853 Fmain$__str_0$0$0 == .
      006640                       2854 ___str_0:
      006640 42 65 61 63 6F 6E 3A  2855 	.ascii "Beacon: "
             20
      006648 00                    2856 	.db 0x00
                           000009  2857 Fmain$__str_1$0$0 == .
      006649                       2858 ___str_1:
      006649 20 20                 2859 	.ascii "  "
      00664B 00                    2860 	.db 0x00
                           00000C  2861 Fmain$__str_2$0$0 == .
      00664C                       2862 ___str_2:
      00664C 0A                    2863 	.db 0x0a
      00664D 00                    2864 	.db 0x00
                           00000E  2865 Fmain$__str_3$0$0 == .
      00664E                       2866 ___str_3:
      00664E 41 43 4B 3A 20        2867 	.ascii "ACK: "
      006653 00                    2868 	.db 0x00
                           000014  2869 Fmain$__str_4$0$0 == .
      006654                       2870 ___str_4:
      006654 20                    2871 	.ascii " "
      006655 00                    2872 	.db 0x00
                           000016  2873 Fmain$__str_5$0$0 == .
      006656                       2874 ___str_5:
      006656 49 44 31 3A 20        2875 	.ascii "ID1: "
      00665B 00                    2876 	.db 0x00
                           00001C  2877 Fmain$__str_6$0$0 == .
      00665C                       2878 ___str_6:
      00665C 0A                    2879 	.db 0x0a
      00665D 49 44 33 3A 20        2880 	.ascii "ID3: "
      006662 00                    2881 	.db 0x00
                           000023  2882 Fmain$__str_7$0$0 == .
      006663                       2883 ___str_7:
      006663 0A                    2884 	.db 0x0a
      006664 0A                    2885 	.db 0x0a
      006665 00                    2886 	.db 0x00
                           000026  2887 Fmain$__str_8$0$0 == .
      006666                       2888 ___str_8:
      006666 34 30 5F 50 4B 54 5F  2889 	.ascii "40_PKT_WT0:"
             57 54 30 3A
      006671 00                    2890 	.db 0x00
                           000032  2891 Fmain$__str_9$0$0 == .
      006672                       2892 ___str_9:
      006672 34 31 5F 50 4B 54 5F  2893 	.ascii "41_PKT_delta:"
             64 65 6C 74 61 3A
      00667F 00                    2894 	.db 0x00
                           000040  2895 Fmain$__str_10$0$0 == .
      006680                       2896 ___str_10:
      006680 49 6E 76 61 6C 69 64  2897 	.ascii "Invalid packet."
             20 70 61 63 6B 65 74
             2E
      00668F 0A                    2898 	.db 0x0a
      006690 0A                    2899 	.db 0x0a
      006691 00                    2900 	.db 0x00
                           000052  2901 Fmain$__str_11$0$0 == .
      006692                       2902 ___str_11:
      006692 31 5F 42 65 61 63 6F  2903 	.ascii "1_Beacon_WT0: "
             6E 5F 57 54 30 3A 20
      0066A0 00                    2904 	.db 0x00
                           000061  2905 Fmain$__str_12$0$0 == .
      0066A1                       2906 ___str_12:
      0066A1 32 5F 41 43 4B 5F 57  2907 	.ascii "2_ACK_WT0: "
             54 30 3A 20
      0066AC 00                    2908 	.db 0x00
                           00006D  2909 Fmain$__str_13$0$0 == .
      0066AD                       2910 ___str_13:
      0066AD 33 5F 4E 6F 41 43 4B  2911 	.ascii "3_NoACK_WT0: "
             5F 57 54 30 3A 20
      0066BA 00                    2912 	.db 0x00
                           00007B  2913 Fmain$__str_14$0$0 == .
      0066BB                       2914 ___str_14:
      0066BB 52 4E 47 20 3D 20     2915 	.ascii "RNG = "
      0066C1 00                    2916 	.db 0x00
                           000082  2917 Fmain$__str_15$0$0 == .
      0066C2                       2918 ___str_15:
      0066C2 0A                    2919 	.db 0x0a
      0066C3 56 43 4F 49 20 3D 20  2920 	.ascii "VCOI = "
      0066CA 00                    2921 	.db 0x00
                           00008B  2922 Fmain$__str_16$0$0 == .
      0066CB                       2923 ___str_16:
      0066CB 0A                    2924 	.db 0x0a
      0066CC 0A                    2925 	.db 0x0a
      0066CD 4D 41 53 54 45 52     2926 	.ascii "MASTER"
      0066D3 0A                    2927 	.db 0x0a
      0066D4 00                    2928 	.db 0x00
                                   2929 	.area XINIT   (CODE)
                           000000  2930 Fmain$__xinit_beacon_packet$0$0 == .
      0068CF                       2931 __xinit__beacon_packet:
      0068CF 00                    2932 	.db #0x00	; 0
      0068D0 00                    2933 	.db #0x00	; 0
      0068D1 11                    2934 	.db #0x11	; 17
      0068D2 22                    2935 	.db #0x22	; 34
      0068D3 33                    2936 	.db #0x33	; 51	'3'
      0068D4 44                    2937 	.db #0x44	; 68	'D'
      0068D5 55                    2938 	.db #0x55	; 85	'U'
      0068D6 66                    2939 	.db #0x66	; 102	'f'
                           000008  2940 Fmain$__xinit_ack_packet$0$0 == .
      0068D7                       2941 __xinit__ack_packet:
      0068D7 00                    2942 	.db #0x00	; 0
      0068D8 77                    2943 	.db #0x77	; 119	'w'
      0068D9 88                    2944 	.db #0x88	; 136
      0068DA 99                    2945 	.db #0x99	; 153
                           00000C  2946 Fmain$__xinit_pkts_received$0$0 == .
      0068DB                       2947 __xinit__pkts_received:
      0068DB 00 00                 2948 	.byte #0x00,#0x00	; 0
                           00000E  2949 Fmain$__xinit_pkts_missing$0$0 == .
      0068DD                       2950 __xinit__pkts_missing:
      0068DD 00 00                 2951 	.byte #0x00,#0x00	; 0
                           000010  2952 Fmain$__xinit_bc_counter$0$0 == .
      0068DF                       2953 __xinit__bc_counter:
      0068DF 00 00                 2954 	.byte #0x00,#0x00	; 0
                           000012  2955 Fmain$__xinit_rcv_flg$0$0 == .
      0068E1                       2956 __xinit__rcv_flg:
      0068E1 00                    2957 	.db #0x00	; 0
                           000013  2958 Fmain$__xinit_pkts_id1$0$0 == .
      0068E2                       2959 __xinit__pkts_id1:
      0068E2 00 00                 2960 	.byte #0x00,#0x00	; 0
                           000015  2961 Fmain$__xinit_pkts_id3$0$0 == .
      0068E4                       2962 __xinit__pkts_id3:
      0068E4 00 00                 2963 	.byte #0x00,#0x00	; 0
                           000017  2964 Fmain$__xinit_rcv_flg_id1$0$0 == .
      0068E6                       2965 __xinit__rcv_flg_id1:
      0068E6 00                    2966 	.db #0x00	; 0
                           000018  2967 Fmain$__xinit_rcv_flg_id3$0$0 == .
      0068E7                       2968 __xinit__rcv_flg_id3:
      0068E7 00                    2969 	.db #0x00	; 0
                           000019  2970 Fmain$__xinit_wt0_beacon$0$0 == .
      0068E8                       2971 __xinit__wt0_beacon:
      0068E8 00 00 00 00           2972 	.byte #0x00,#0x00,#0x00,#0x00	; 0
                                   2973 	.area CABS    (ABS,CODE)
