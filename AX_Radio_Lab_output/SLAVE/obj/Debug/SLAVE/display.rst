                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module display
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _com0_writestr
                                     12 	.globl _com0_setpos
                                     13 	.globl _dbglink_writenum32
                                     14 	.globl _dbglink_writehex16
                                     15 	.globl _dbglink_writenum16
                                     16 	.globl _dbglink_writestr
                                     17 	.globl _dbglink_tx
                                     18 	.globl _dbglink_txfree
                                     19 	.globl _uart0_writehex16
                                     20 	.globl _uart0_writenum16
                                     21 	.globl _wtimer_runcallbacks
                                     22 	.globl _wtimer_idle
                                     23 	.globl _axradio_conv_freq_tohz
                                     24 	.globl _axradio_get_freqoffset
                                     25 	.globl _PORTC_7
                                     26 	.globl _PORTC_6
                                     27 	.globl _PORTC_5
                                     28 	.globl _PORTC_4
                                     29 	.globl _PORTC_3
                                     30 	.globl _PORTC_2
                                     31 	.globl _PORTC_1
                                     32 	.globl _PORTC_0
                                     33 	.globl _PORTB_7
                                     34 	.globl _PORTB_6
                                     35 	.globl _PORTB_5
                                     36 	.globl _PORTB_4
                                     37 	.globl _PORTB_3
                                     38 	.globl _PORTB_2
                                     39 	.globl _PORTB_1
                                     40 	.globl _PORTB_0
                                     41 	.globl _PORTA_7
                                     42 	.globl _PORTA_6
                                     43 	.globl _PORTA_5
                                     44 	.globl _PORTA_4
                                     45 	.globl _PORTA_3
                                     46 	.globl _PORTA_2
                                     47 	.globl _PORTA_1
                                     48 	.globl _PORTA_0
                                     49 	.globl _PINC_7
                                     50 	.globl _PINC_6
                                     51 	.globl _PINC_5
                                     52 	.globl _PINC_4
                                     53 	.globl _PINC_3
                                     54 	.globl _PINC_2
                                     55 	.globl _PINC_1
                                     56 	.globl _PINC_0
                                     57 	.globl _PINB_7
                                     58 	.globl _PINB_6
                                     59 	.globl _PINB_5
                                     60 	.globl _PINB_4
                                     61 	.globl _PINB_3
                                     62 	.globl _PINB_2
                                     63 	.globl _PINB_1
                                     64 	.globl _PINB_0
                                     65 	.globl _PINA_7
                                     66 	.globl _PINA_6
                                     67 	.globl _PINA_5
                                     68 	.globl _PINA_4
                                     69 	.globl _PINA_3
                                     70 	.globl _PINA_2
                                     71 	.globl _PINA_1
                                     72 	.globl _PINA_0
                                     73 	.globl _CY
                                     74 	.globl _AC
                                     75 	.globl _F0
                                     76 	.globl _RS1
                                     77 	.globl _RS0
                                     78 	.globl _OV
                                     79 	.globl _F1
                                     80 	.globl _P
                                     81 	.globl _IP_7
                                     82 	.globl _IP_6
                                     83 	.globl _IP_5
                                     84 	.globl _IP_4
                                     85 	.globl _IP_3
                                     86 	.globl _IP_2
                                     87 	.globl _IP_1
                                     88 	.globl _IP_0
                                     89 	.globl _EA
                                     90 	.globl _IE_7
                                     91 	.globl _IE_6
                                     92 	.globl _IE_5
                                     93 	.globl _IE_4
                                     94 	.globl _IE_3
                                     95 	.globl _IE_2
                                     96 	.globl _IE_1
                                     97 	.globl _IE_0
                                     98 	.globl _EIP_7
                                     99 	.globl _EIP_6
                                    100 	.globl _EIP_5
                                    101 	.globl _EIP_4
                                    102 	.globl _EIP_3
                                    103 	.globl _EIP_2
                                    104 	.globl _EIP_1
                                    105 	.globl _EIP_0
                                    106 	.globl _EIE_7
                                    107 	.globl _EIE_6
                                    108 	.globl _EIE_5
                                    109 	.globl _EIE_4
                                    110 	.globl _EIE_3
                                    111 	.globl _EIE_2
                                    112 	.globl _EIE_1
                                    113 	.globl _EIE_0
                                    114 	.globl _E2IP_7
                                    115 	.globl _E2IP_6
                                    116 	.globl _E2IP_5
                                    117 	.globl _E2IP_4
                                    118 	.globl _E2IP_3
                                    119 	.globl _E2IP_2
                                    120 	.globl _E2IP_1
                                    121 	.globl _E2IP_0
                                    122 	.globl _E2IE_7
                                    123 	.globl _E2IE_6
                                    124 	.globl _E2IE_5
                                    125 	.globl _E2IE_4
                                    126 	.globl _E2IE_3
                                    127 	.globl _E2IE_2
                                    128 	.globl _E2IE_1
                                    129 	.globl _E2IE_0
                                    130 	.globl _B_7
                                    131 	.globl _B_6
                                    132 	.globl _B_5
                                    133 	.globl _B_4
                                    134 	.globl _B_3
                                    135 	.globl _B_2
                                    136 	.globl _B_1
                                    137 	.globl _B_0
                                    138 	.globl _ACC_7
                                    139 	.globl _ACC_6
                                    140 	.globl _ACC_5
                                    141 	.globl _ACC_4
                                    142 	.globl _ACC_3
                                    143 	.globl _ACC_2
                                    144 	.globl _ACC_1
                                    145 	.globl _ACC_0
                                    146 	.globl _WTSTAT
                                    147 	.globl _WTIRQEN
                                    148 	.globl _WTEVTD
                                    149 	.globl _WTEVTD1
                                    150 	.globl _WTEVTD0
                                    151 	.globl _WTEVTC
                                    152 	.globl _WTEVTC1
                                    153 	.globl _WTEVTC0
                                    154 	.globl _WTEVTB
                                    155 	.globl _WTEVTB1
                                    156 	.globl _WTEVTB0
                                    157 	.globl _WTEVTA
                                    158 	.globl _WTEVTA1
                                    159 	.globl _WTEVTA0
                                    160 	.globl _WTCNTR1
                                    161 	.globl _WTCNTB
                                    162 	.globl _WTCNTB1
                                    163 	.globl _WTCNTB0
                                    164 	.globl _WTCNTA
                                    165 	.globl _WTCNTA1
                                    166 	.globl _WTCNTA0
                                    167 	.globl _WTCFGB
                                    168 	.globl _WTCFGA
                                    169 	.globl _WDTRESET
                                    170 	.globl _WDTCFG
                                    171 	.globl _U1STATUS
                                    172 	.globl _U1SHREG
                                    173 	.globl _U1MODE
                                    174 	.globl _U1CTRL
                                    175 	.globl _U0STATUS
                                    176 	.globl _U0SHREG
                                    177 	.globl _U0MODE
                                    178 	.globl _U0CTRL
                                    179 	.globl _T2STATUS
                                    180 	.globl _T2PERIOD
                                    181 	.globl _T2PERIOD1
                                    182 	.globl _T2PERIOD0
                                    183 	.globl _T2MODE
                                    184 	.globl _T2CNT
                                    185 	.globl _T2CNT1
                                    186 	.globl _T2CNT0
                                    187 	.globl _T2CLKSRC
                                    188 	.globl _T1STATUS
                                    189 	.globl _T1PERIOD
                                    190 	.globl _T1PERIOD1
                                    191 	.globl _T1PERIOD0
                                    192 	.globl _T1MODE
                                    193 	.globl _T1CNT
                                    194 	.globl _T1CNT1
                                    195 	.globl _T1CNT0
                                    196 	.globl _T1CLKSRC
                                    197 	.globl _T0STATUS
                                    198 	.globl _T0PERIOD
                                    199 	.globl _T0PERIOD1
                                    200 	.globl _T0PERIOD0
                                    201 	.globl _T0MODE
                                    202 	.globl _T0CNT
                                    203 	.globl _T0CNT1
                                    204 	.globl _T0CNT0
                                    205 	.globl _T0CLKSRC
                                    206 	.globl _SPSTATUS
                                    207 	.globl _SPSHREG
                                    208 	.globl _SPMODE
                                    209 	.globl _SPCLKSRC
                                    210 	.globl _RADIOSTAT
                                    211 	.globl _RADIOSTAT1
                                    212 	.globl _RADIOSTAT0
                                    213 	.globl _RADIODATA
                                    214 	.globl _RADIODATA3
                                    215 	.globl _RADIODATA2
                                    216 	.globl _RADIODATA1
                                    217 	.globl _RADIODATA0
                                    218 	.globl _RADIOADDR
                                    219 	.globl _RADIOADDR1
                                    220 	.globl _RADIOADDR0
                                    221 	.globl _RADIOACC
                                    222 	.globl _OC1STATUS
                                    223 	.globl _OC1PIN
                                    224 	.globl _OC1MODE
                                    225 	.globl _OC1COMP
                                    226 	.globl _OC1COMP1
                                    227 	.globl _OC1COMP0
                                    228 	.globl _OC0STATUS
                                    229 	.globl _OC0PIN
                                    230 	.globl _OC0MODE
                                    231 	.globl _OC0COMP
                                    232 	.globl _OC0COMP1
                                    233 	.globl _OC0COMP0
                                    234 	.globl _NVSTATUS
                                    235 	.globl _NVKEY
                                    236 	.globl _NVDATA
                                    237 	.globl _NVDATA1
                                    238 	.globl _NVDATA0
                                    239 	.globl _NVADDR
                                    240 	.globl _NVADDR1
                                    241 	.globl _NVADDR0
                                    242 	.globl _IC1STATUS
                                    243 	.globl _IC1MODE
                                    244 	.globl _IC1CAPT
                                    245 	.globl _IC1CAPT1
                                    246 	.globl _IC1CAPT0
                                    247 	.globl _IC0STATUS
                                    248 	.globl _IC0MODE
                                    249 	.globl _IC0CAPT
                                    250 	.globl _IC0CAPT1
                                    251 	.globl _IC0CAPT0
                                    252 	.globl _PORTR
                                    253 	.globl _PORTC
                                    254 	.globl _PORTB
                                    255 	.globl _PORTA
                                    256 	.globl _PINR
                                    257 	.globl _PINC
                                    258 	.globl _PINB
                                    259 	.globl _PINA
                                    260 	.globl _DIRR
                                    261 	.globl _DIRC
                                    262 	.globl _DIRB
                                    263 	.globl _DIRA
                                    264 	.globl _DBGLNKSTAT
                                    265 	.globl _DBGLNKBUF
                                    266 	.globl _CODECONFIG
                                    267 	.globl _CLKSTAT
                                    268 	.globl _CLKCON
                                    269 	.globl _ANALOGCOMP
                                    270 	.globl _ADCCONV
                                    271 	.globl _ADCCLKSRC
                                    272 	.globl _ADCCH3CONFIG
                                    273 	.globl _ADCCH2CONFIG
                                    274 	.globl _ADCCH1CONFIG
                                    275 	.globl _ADCCH0CONFIG
                                    276 	.globl __XPAGE
                                    277 	.globl _XPAGE
                                    278 	.globl _SP
                                    279 	.globl _PSW
                                    280 	.globl _PCON
                                    281 	.globl _IP
                                    282 	.globl _IE
                                    283 	.globl _EIP
                                    284 	.globl _EIE
                                    285 	.globl _E2IP
                                    286 	.globl _E2IE
                                    287 	.globl _DPS
                                    288 	.globl _DPTR1
                                    289 	.globl _DPTR0
                                    290 	.globl _DPL1
                                    291 	.globl _DPL
                                    292 	.globl _DPH1
                                    293 	.globl _DPH
                                    294 	.globl _B
                                    295 	.globl _ACC
                                    296 	.globl _XTALREADY
                                    297 	.globl _XTALOSC
                                    298 	.globl _XTALAMPL
                                    299 	.globl _SILICONREV
                                    300 	.globl _SCRATCH3
                                    301 	.globl _SCRATCH2
                                    302 	.globl _SCRATCH1
                                    303 	.globl _SCRATCH0
                                    304 	.globl _RADIOMUX
                                    305 	.globl _RADIOFSTATADDR
                                    306 	.globl _RADIOFSTATADDR1
                                    307 	.globl _RADIOFSTATADDR0
                                    308 	.globl _RADIOFDATAADDR
                                    309 	.globl _RADIOFDATAADDR1
                                    310 	.globl _RADIOFDATAADDR0
                                    311 	.globl _OSCRUN
                                    312 	.globl _OSCREADY
                                    313 	.globl _OSCFORCERUN
                                    314 	.globl _OSCCALIB
                                    315 	.globl _MISCCTRL
                                    316 	.globl _LPXOSCGM
                                    317 	.globl _LPOSCREF
                                    318 	.globl _LPOSCREF1
                                    319 	.globl _LPOSCREF0
                                    320 	.globl _LPOSCPER
                                    321 	.globl _LPOSCPER1
                                    322 	.globl _LPOSCPER0
                                    323 	.globl _LPOSCKFILT
                                    324 	.globl _LPOSCKFILT1
                                    325 	.globl _LPOSCKFILT0
                                    326 	.globl _LPOSCFREQ
                                    327 	.globl _LPOSCFREQ1
                                    328 	.globl _LPOSCFREQ0
                                    329 	.globl _LPOSCCONFIG
                                    330 	.globl _PINSEL
                                    331 	.globl _PINCHGC
                                    332 	.globl _PINCHGB
                                    333 	.globl _PINCHGA
                                    334 	.globl _PALTRADIO
                                    335 	.globl _PALTC
                                    336 	.globl _PALTB
                                    337 	.globl _PALTA
                                    338 	.globl _INTCHGC
                                    339 	.globl _INTCHGB
                                    340 	.globl _INTCHGA
                                    341 	.globl _EXTIRQ
                                    342 	.globl _GPIOENABLE
                                    343 	.globl _ANALOGA
                                    344 	.globl _FRCOSCREF
                                    345 	.globl _FRCOSCREF1
                                    346 	.globl _FRCOSCREF0
                                    347 	.globl _FRCOSCPER
                                    348 	.globl _FRCOSCPER1
                                    349 	.globl _FRCOSCPER0
                                    350 	.globl _FRCOSCKFILT
                                    351 	.globl _FRCOSCKFILT1
                                    352 	.globl _FRCOSCKFILT0
                                    353 	.globl _FRCOSCFREQ
                                    354 	.globl _FRCOSCFREQ1
                                    355 	.globl _FRCOSCFREQ0
                                    356 	.globl _FRCOSCCTRL
                                    357 	.globl _FRCOSCCONFIG
                                    358 	.globl _DMA1CONFIG
                                    359 	.globl _DMA1ADDR
                                    360 	.globl _DMA1ADDR1
                                    361 	.globl _DMA1ADDR0
                                    362 	.globl _DMA0CONFIG
                                    363 	.globl _DMA0ADDR
                                    364 	.globl _DMA0ADDR1
                                    365 	.globl _DMA0ADDR0
                                    366 	.globl _ADCTUNE2
                                    367 	.globl _ADCTUNE1
                                    368 	.globl _ADCTUNE0
                                    369 	.globl _ADCCH3VAL
                                    370 	.globl _ADCCH3VAL1
                                    371 	.globl _ADCCH3VAL0
                                    372 	.globl _ADCCH2VAL
                                    373 	.globl _ADCCH2VAL1
                                    374 	.globl _ADCCH2VAL0
                                    375 	.globl _ADCCH1VAL
                                    376 	.globl _ADCCH1VAL1
                                    377 	.globl _ADCCH1VAL0
                                    378 	.globl _ADCCH0VAL
                                    379 	.globl _ADCCH0VAL1
                                    380 	.globl _ADCCH0VAL0
                                    381 	.globl _display_timing
                                    382 	.globl _per_test_counter_previous
                                    383 	.globl _per_test_counter
                                    384 	.globl _display_received_packet
                                    385 	.globl _dbglink_received_packet
                                    386 ;--------------------------------------------------------
                                    387 ; special function registers
                                    388 ;--------------------------------------------------------
                                    389 	.area RSEG    (ABS,DATA)
      000000                        390 	.org 0x0000
                           0000E0   391 G$ACC$0$0 == 0x00e0
                           0000E0   392 _ACC	=	0x00e0
                           0000F0   393 G$B$0$0 == 0x00f0
                           0000F0   394 _B	=	0x00f0
                           000083   395 G$DPH$0$0 == 0x0083
                           000083   396 _DPH	=	0x0083
                           000085   397 G$DPH1$0$0 == 0x0085
                           000085   398 _DPH1	=	0x0085
                           000082   399 G$DPL$0$0 == 0x0082
                           000082   400 _DPL	=	0x0082
                           000084   401 G$DPL1$0$0 == 0x0084
                           000084   402 _DPL1	=	0x0084
                           008382   403 G$DPTR0$0$0 == 0x8382
                           008382   404 _DPTR0	=	0x8382
                           008584   405 G$DPTR1$0$0 == 0x8584
                           008584   406 _DPTR1	=	0x8584
                           000086   407 G$DPS$0$0 == 0x0086
                           000086   408 _DPS	=	0x0086
                           0000A0   409 G$E2IE$0$0 == 0x00a0
                           0000A0   410 _E2IE	=	0x00a0
                           0000C0   411 G$E2IP$0$0 == 0x00c0
                           0000C0   412 _E2IP	=	0x00c0
                           000098   413 G$EIE$0$0 == 0x0098
                           000098   414 _EIE	=	0x0098
                           0000B0   415 G$EIP$0$0 == 0x00b0
                           0000B0   416 _EIP	=	0x00b0
                           0000A8   417 G$IE$0$0 == 0x00a8
                           0000A8   418 _IE	=	0x00a8
                           0000B8   419 G$IP$0$0 == 0x00b8
                           0000B8   420 _IP	=	0x00b8
                           000087   421 G$PCON$0$0 == 0x0087
                           000087   422 _PCON	=	0x0087
                           0000D0   423 G$PSW$0$0 == 0x00d0
                           0000D0   424 _PSW	=	0x00d0
                           000081   425 G$SP$0$0 == 0x0081
                           000081   426 _SP	=	0x0081
                           0000D9   427 G$XPAGE$0$0 == 0x00d9
                           0000D9   428 _XPAGE	=	0x00d9
                           0000D9   429 G$_XPAGE$0$0 == 0x00d9
                           0000D9   430 __XPAGE	=	0x00d9
                           0000CA   431 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   432 _ADCCH0CONFIG	=	0x00ca
                           0000CB   433 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   434 _ADCCH1CONFIG	=	0x00cb
                           0000D2   435 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   436 _ADCCH2CONFIG	=	0x00d2
                           0000D3   437 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   438 _ADCCH3CONFIG	=	0x00d3
                           0000D1   439 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   440 _ADCCLKSRC	=	0x00d1
                           0000C9   441 G$ADCCONV$0$0 == 0x00c9
                           0000C9   442 _ADCCONV	=	0x00c9
                           0000E1   443 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   444 _ANALOGCOMP	=	0x00e1
                           0000C6   445 G$CLKCON$0$0 == 0x00c6
                           0000C6   446 _CLKCON	=	0x00c6
                           0000C7   447 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   448 _CLKSTAT	=	0x00c7
                           000097   449 G$CODECONFIG$0$0 == 0x0097
                           000097   450 _CODECONFIG	=	0x0097
                           0000E3   451 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   452 _DBGLNKBUF	=	0x00e3
                           0000E2   453 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   454 _DBGLNKSTAT	=	0x00e2
                           000089   455 G$DIRA$0$0 == 0x0089
                           000089   456 _DIRA	=	0x0089
                           00008A   457 G$DIRB$0$0 == 0x008a
                           00008A   458 _DIRB	=	0x008a
                           00008B   459 G$DIRC$0$0 == 0x008b
                           00008B   460 _DIRC	=	0x008b
                           00008E   461 G$DIRR$0$0 == 0x008e
                           00008E   462 _DIRR	=	0x008e
                           0000C8   463 G$PINA$0$0 == 0x00c8
                           0000C8   464 _PINA	=	0x00c8
                           0000E8   465 G$PINB$0$0 == 0x00e8
                           0000E8   466 _PINB	=	0x00e8
                           0000F8   467 G$PINC$0$0 == 0x00f8
                           0000F8   468 _PINC	=	0x00f8
                           00008D   469 G$PINR$0$0 == 0x008d
                           00008D   470 _PINR	=	0x008d
                           000080   471 G$PORTA$0$0 == 0x0080
                           000080   472 _PORTA	=	0x0080
                           000088   473 G$PORTB$0$0 == 0x0088
                           000088   474 _PORTB	=	0x0088
                           000090   475 G$PORTC$0$0 == 0x0090
                           000090   476 _PORTC	=	0x0090
                           00008C   477 G$PORTR$0$0 == 0x008c
                           00008C   478 _PORTR	=	0x008c
                           0000CE   479 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   480 _IC0CAPT0	=	0x00ce
                           0000CF   481 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   482 _IC0CAPT1	=	0x00cf
                           00CFCE   483 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   484 _IC0CAPT	=	0xcfce
                           0000CC   485 G$IC0MODE$0$0 == 0x00cc
                           0000CC   486 _IC0MODE	=	0x00cc
                           0000CD   487 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   488 _IC0STATUS	=	0x00cd
                           0000D6   489 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   490 _IC1CAPT0	=	0x00d6
                           0000D7   491 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   492 _IC1CAPT1	=	0x00d7
                           00D7D6   493 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   494 _IC1CAPT	=	0xd7d6
                           0000D4   495 G$IC1MODE$0$0 == 0x00d4
                           0000D4   496 _IC1MODE	=	0x00d4
                           0000D5   497 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   498 _IC1STATUS	=	0x00d5
                           000092   499 G$NVADDR0$0$0 == 0x0092
                           000092   500 _NVADDR0	=	0x0092
                           000093   501 G$NVADDR1$0$0 == 0x0093
                           000093   502 _NVADDR1	=	0x0093
                           009392   503 G$NVADDR$0$0 == 0x9392
                           009392   504 _NVADDR	=	0x9392
                           000094   505 G$NVDATA0$0$0 == 0x0094
                           000094   506 _NVDATA0	=	0x0094
                           000095   507 G$NVDATA1$0$0 == 0x0095
                           000095   508 _NVDATA1	=	0x0095
                           009594   509 G$NVDATA$0$0 == 0x9594
                           009594   510 _NVDATA	=	0x9594
                           000096   511 G$NVKEY$0$0 == 0x0096
                           000096   512 _NVKEY	=	0x0096
                           000091   513 G$NVSTATUS$0$0 == 0x0091
                           000091   514 _NVSTATUS	=	0x0091
                           0000BC   515 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   516 _OC0COMP0	=	0x00bc
                           0000BD   517 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   518 _OC0COMP1	=	0x00bd
                           00BDBC   519 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   520 _OC0COMP	=	0xbdbc
                           0000B9   521 G$OC0MODE$0$0 == 0x00b9
                           0000B9   522 _OC0MODE	=	0x00b9
                           0000BA   523 G$OC0PIN$0$0 == 0x00ba
                           0000BA   524 _OC0PIN	=	0x00ba
                           0000BB   525 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   526 _OC0STATUS	=	0x00bb
                           0000C4   527 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   528 _OC1COMP0	=	0x00c4
                           0000C5   529 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   530 _OC1COMP1	=	0x00c5
                           00C5C4   531 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   532 _OC1COMP	=	0xc5c4
                           0000C1   533 G$OC1MODE$0$0 == 0x00c1
                           0000C1   534 _OC1MODE	=	0x00c1
                           0000C2   535 G$OC1PIN$0$0 == 0x00c2
                           0000C2   536 _OC1PIN	=	0x00c2
                           0000C3   537 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   538 _OC1STATUS	=	0x00c3
                           0000B1   539 G$RADIOACC$0$0 == 0x00b1
                           0000B1   540 _RADIOACC	=	0x00b1
                           0000B3   541 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   542 _RADIOADDR0	=	0x00b3
                           0000B2   543 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   544 _RADIOADDR1	=	0x00b2
                           00B2B3   545 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   546 _RADIOADDR	=	0xb2b3
                           0000B7   547 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   548 _RADIODATA0	=	0x00b7
                           0000B6   549 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   550 _RADIODATA1	=	0x00b6
                           0000B5   551 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   552 _RADIODATA2	=	0x00b5
                           0000B4   553 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   554 _RADIODATA3	=	0x00b4
                           B4B5B6B7   555 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   556 _RADIODATA	=	0xb4b5b6b7
                           0000BE   557 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   558 _RADIOSTAT0	=	0x00be
                           0000BF   559 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   560 _RADIOSTAT1	=	0x00bf
                           00BFBE   561 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   562 _RADIOSTAT	=	0xbfbe
                           0000DF   563 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   564 _SPCLKSRC	=	0x00df
                           0000DC   565 G$SPMODE$0$0 == 0x00dc
                           0000DC   566 _SPMODE	=	0x00dc
                           0000DE   567 G$SPSHREG$0$0 == 0x00de
                           0000DE   568 _SPSHREG	=	0x00de
                           0000DD   569 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   570 _SPSTATUS	=	0x00dd
                           00009A   571 G$T0CLKSRC$0$0 == 0x009a
                           00009A   572 _T0CLKSRC	=	0x009a
                           00009C   573 G$T0CNT0$0$0 == 0x009c
                           00009C   574 _T0CNT0	=	0x009c
                           00009D   575 G$T0CNT1$0$0 == 0x009d
                           00009D   576 _T0CNT1	=	0x009d
                           009D9C   577 G$T0CNT$0$0 == 0x9d9c
                           009D9C   578 _T0CNT	=	0x9d9c
                           000099   579 G$T0MODE$0$0 == 0x0099
                           000099   580 _T0MODE	=	0x0099
                           00009E   581 G$T0PERIOD0$0$0 == 0x009e
                           00009E   582 _T0PERIOD0	=	0x009e
                           00009F   583 G$T0PERIOD1$0$0 == 0x009f
                           00009F   584 _T0PERIOD1	=	0x009f
                           009F9E   585 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   586 _T0PERIOD	=	0x9f9e
                           00009B   587 G$T0STATUS$0$0 == 0x009b
                           00009B   588 _T0STATUS	=	0x009b
                           0000A2   589 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   590 _T1CLKSRC	=	0x00a2
                           0000A4   591 G$T1CNT0$0$0 == 0x00a4
                           0000A4   592 _T1CNT0	=	0x00a4
                           0000A5   593 G$T1CNT1$0$0 == 0x00a5
                           0000A5   594 _T1CNT1	=	0x00a5
                           00A5A4   595 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   596 _T1CNT	=	0xa5a4
                           0000A1   597 G$T1MODE$0$0 == 0x00a1
                           0000A1   598 _T1MODE	=	0x00a1
                           0000A6   599 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   600 _T1PERIOD0	=	0x00a6
                           0000A7   601 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   602 _T1PERIOD1	=	0x00a7
                           00A7A6   603 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   604 _T1PERIOD	=	0xa7a6
                           0000A3   605 G$T1STATUS$0$0 == 0x00a3
                           0000A3   606 _T1STATUS	=	0x00a3
                           0000AA   607 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   608 _T2CLKSRC	=	0x00aa
                           0000AC   609 G$T2CNT0$0$0 == 0x00ac
                           0000AC   610 _T2CNT0	=	0x00ac
                           0000AD   611 G$T2CNT1$0$0 == 0x00ad
                           0000AD   612 _T2CNT1	=	0x00ad
                           00ADAC   613 G$T2CNT$0$0 == 0xadac
                           00ADAC   614 _T2CNT	=	0xadac
                           0000A9   615 G$T2MODE$0$0 == 0x00a9
                           0000A9   616 _T2MODE	=	0x00a9
                           0000AE   617 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   618 _T2PERIOD0	=	0x00ae
                           0000AF   619 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   620 _T2PERIOD1	=	0x00af
                           00AFAE   621 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   622 _T2PERIOD	=	0xafae
                           0000AB   623 G$T2STATUS$0$0 == 0x00ab
                           0000AB   624 _T2STATUS	=	0x00ab
                           0000E4   625 G$U0CTRL$0$0 == 0x00e4
                           0000E4   626 _U0CTRL	=	0x00e4
                           0000E7   627 G$U0MODE$0$0 == 0x00e7
                           0000E7   628 _U0MODE	=	0x00e7
                           0000E6   629 G$U0SHREG$0$0 == 0x00e6
                           0000E6   630 _U0SHREG	=	0x00e6
                           0000E5   631 G$U0STATUS$0$0 == 0x00e5
                           0000E5   632 _U0STATUS	=	0x00e5
                           0000EC   633 G$U1CTRL$0$0 == 0x00ec
                           0000EC   634 _U1CTRL	=	0x00ec
                           0000EF   635 G$U1MODE$0$0 == 0x00ef
                           0000EF   636 _U1MODE	=	0x00ef
                           0000EE   637 G$U1SHREG$0$0 == 0x00ee
                           0000EE   638 _U1SHREG	=	0x00ee
                           0000ED   639 G$U1STATUS$0$0 == 0x00ed
                           0000ED   640 _U1STATUS	=	0x00ed
                           0000DA   641 G$WDTCFG$0$0 == 0x00da
                           0000DA   642 _WDTCFG	=	0x00da
                           0000DB   643 G$WDTRESET$0$0 == 0x00db
                           0000DB   644 _WDTRESET	=	0x00db
                           0000F1   645 G$WTCFGA$0$0 == 0x00f1
                           0000F1   646 _WTCFGA	=	0x00f1
                           0000F9   647 G$WTCFGB$0$0 == 0x00f9
                           0000F9   648 _WTCFGB	=	0x00f9
                           0000F2   649 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   650 _WTCNTA0	=	0x00f2
                           0000F3   651 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   652 _WTCNTA1	=	0x00f3
                           00F3F2   653 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   654 _WTCNTA	=	0xf3f2
                           0000FA   655 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   656 _WTCNTB0	=	0x00fa
                           0000FB   657 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   658 _WTCNTB1	=	0x00fb
                           00FBFA   659 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   660 _WTCNTB	=	0xfbfa
                           0000EB   661 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   662 _WTCNTR1	=	0x00eb
                           0000F4   663 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   664 _WTEVTA0	=	0x00f4
                           0000F5   665 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   666 _WTEVTA1	=	0x00f5
                           00F5F4   667 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   668 _WTEVTA	=	0xf5f4
                           0000F6   669 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   670 _WTEVTB0	=	0x00f6
                           0000F7   671 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   672 _WTEVTB1	=	0x00f7
                           00F7F6   673 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   674 _WTEVTB	=	0xf7f6
                           0000FC   675 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   676 _WTEVTC0	=	0x00fc
                           0000FD   677 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   678 _WTEVTC1	=	0x00fd
                           00FDFC   679 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   680 _WTEVTC	=	0xfdfc
                           0000FE   681 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   682 _WTEVTD0	=	0x00fe
                           0000FF   683 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   684 _WTEVTD1	=	0x00ff
                           00FFFE   685 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   686 _WTEVTD	=	0xfffe
                           0000E9   687 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   688 _WTIRQEN	=	0x00e9
                           0000EA   689 G$WTSTAT$0$0 == 0x00ea
                           0000EA   690 _WTSTAT	=	0x00ea
                                    691 ;--------------------------------------------------------
                                    692 ; special function bits
                                    693 ;--------------------------------------------------------
                                    694 	.area RSEG    (ABS,DATA)
      000000                        695 	.org 0x0000
                           0000E0   696 G$ACC_0$0$0 == 0x00e0
                           0000E0   697 _ACC_0	=	0x00e0
                           0000E1   698 G$ACC_1$0$0 == 0x00e1
                           0000E1   699 _ACC_1	=	0x00e1
                           0000E2   700 G$ACC_2$0$0 == 0x00e2
                           0000E2   701 _ACC_2	=	0x00e2
                           0000E3   702 G$ACC_3$0$0 == 0x00e3
                           0000E3   703 _ACC_3	=	0x00e3
                           0000E4   704 G$ACC_4$0$0 == 0x00e4
                           0000E4   705 _ACC_4	=	0x00e4
                           0000E5   706 G$ACC_5$0$0 == 0x00e5
                           0000E5   707 _ACC_5	=	0x00e5
                           0000E6   708 G$ACC_6$0$0 == 0x00e6
                           0000E6   709 _ACC_6	=	0x00e6
                           0000E7   710 G$ACC_7$0$0 == 0x00e7
                           0000E7   711 _ACC_7	=	0x00e7
                           0000F0   712 G$B_0$0$0 == 0x00f0
                           0000F0   713 _B_0	=	0x00f0
                           0000F1   714 G$B_1$0$0 == 0x00f1
                           0000F1   715 _B_1	=	0x00f1
                           0000F2   716 G$B_2$0$0 == 0x00f2
                           0000F2   717 _B_2	=	0x00f2
                           0000F3   718 G$B_3$0$0 == 0x00f3
                           0000F3   719 _B_3	=	0x00f3
                           0000F4   720 G$B_4$0$0 == 0x00f4
                           0000F4   721 _B_4	=	0x00f4
                           0000F5   722 G$B_5$0$0 == 0x00f5
                           0000F5   723 _B_5	=	0x00f5
                           0000F6   724 G$B_6$0$0 == 0x00f6
                           0000F6   725 _B_6	=	0x00f6
                           0000F7   726 G$B_7$0$0 == 0x00f7
                           0000F7   727 _B_7	=	0x00f7
                           0000A0   728 G$E2IE_0$0$0 == 0x00a0
                           0000A0   729 _E2IE_0	=	0x00a0
                           0000A1   730 G$E2IE_1$0$0 == 0x00a1
                           0000A1   731 _E2IE_1	=	0x00a1
                           0000A2   732 G$E2IE_2$0$0 == 0x00a2
                           0000A2   733 _E2IE_2	=	0x00a2
                           0000A3   734 G$E2IE_3$0$0 == 0x00a3
                           0000A3   735 _E2IE_3	=	0x00a3
                           0000A4   736 G$E2IE_4$0$0 == 0x00a4
                           0000A4   737 _E2IE_4	=	0x00a4
                           0000A5   738 G$E2IE_5$0$0 == 0x00a5
                           0000A5   739 _E2IE_5	=	0x00a5
                           0000A6   740 G$E2IE_6$0$0 == 0x00a6
                           0000A6   741 _E2IE_6	=	0x00a6
                           0000A7   742 G$E2IE_7$0$0 == 0x00a7
                           0000A7   743 _E2IE_7	=	0x00a7
                           0000C0   744 G$E2IP_0$0$0 == 0x00c0
                           0000C0   745 _E2IP_0	=	0x00c0
                           0000C1   746 G$E2IP_1$0$0 == 0x00c1
                           0000C1   747 _E2IP_1	=	0x00c1
                           0000C2   748 G$E2IP_2$0$0 == 0x00c2
                           0000C2   749 _E2IP_2	=	0x00c2
                           0000C3   750 G$E2IP_3$0$0 == 0x00c3
                           0000C3   751 _E2IP_3	=	0x00c3
                           0000C4   752 G$E2IP_4$0$0 == 0x00c4
                           0000C4   753 _E2IP_4	=	0x00c4
                           0000C5   754 G$E2IP_5$0$0 == 0x00c5
                           0000C5   755 _E2IP_5	=	0x00c5
                           0000C6   756 G$E2IP_6$0$0 == 0x00c6
                           0000C6   757 _E2IP_6	=	0x00c6
                           0000C7   758 G$E2IP_7$0$0 == 0x00c7
                           0000C7   759 _E2IP_7	=	0x00c7
                           000098   760 G$EIE_0$0$0 == 0x0098
                           000098   761 _EIE_0	=	0x0098
                           000099   762 G$EIE_1$0$0 == 0x0099
                           000099   763 _EIE_1	=	0x0099
                           00009A   764 G$EIE_2$0$0 == 0x009a
                           00009A   765 _EIE_2	=	0x009a
                           00009B   766 G$EIE_3$0$0 == 0x009b
                           00009B   767 _EIE_3	=	0x009b
                           00009C   768 G$EIE_4$0$0 == 0x009c
                           00009C   769 _EIE_4	=	0x009c
                           00009D   770 G$EIE_5$0$0 == 0x009d
                           00009D   771 _EIE_5	=	0x009d
                           00009E   772 G$EIE_6$0$0 == 0x009e
                           00009E   773 _EIE_6	=	0x009e
                           00009F   774 G$EIE_7$0$0 == 0x009f
                           00009F   775 _EIE_7	=	0x009f
                           0000B0   776 G$EIP_0$0$0 == 0x00b0
                           0000B0   777 _EIP_0	=	0x00b0
                           0000B1   778 G$EIP_1$0$0 == 0x00b1
                           0000B1   779 _EIP_1	=	0x00b1
                           0000B2   780 G$EIP_2$0$0 == 0x00b2
                           0000B2   781 _EIP_2	=	0x00b2
                           0000B3   782 G$EIP_3$0$0 == 0x00b3
                           0000B3   783 _EIP_3	=	0x00b3
                           0000B4   784 G$EIP_4$0$0 == 0x00b4
                           0000B4   785 _EIP_4	=	0x00b4
                           0000B5   786 G$EIP_5$0$0 == 0x00b5
                           0000B5   787 _EIP_5	=	0x00b5
                           0000B6   788 G$EIP_6$0$0 == 0x00b6
                           0000B6   789 _EIP_6	=	0x00b6
                           0000B7   790 G$EIP_7$0$0 == 0x00b7
                           0000B7   791 _EIP_7	=	0x00b7
                           0000A8   792 G$IE_0$0$0 == 0x00a8
                           0000A8   793 _IE_0	=	0x00a8
                           0000A9   794 G$IE_1$0$0 == 0x00a9
                           0000A9   795 _IE_1	=	0x00a9
                           0000AA   796 G$IE_2$0$0 == 0x00aa
                           0000AA   797 _IE_2	=	0x00aa
                           0000AB   798 G$IE_3$0$0 == 0x00ab
                           0000AB   799 _IE_3	=	0x00ab
                           0000AC   800 G$IE_4$0$0 == 0x00ac
                           0000AC   801 _IE_4	=	0x00ac
                           0000AD   802 G$IE_5$0$0 == 0x00ad
                           0000AD   803 _IE_5	=	0x00ad
                           0000AE   804 G$IE_6$0$0 == 0x00ae
                           0000AE   805 _IE_6	=	0x00ae
                           0000AF   806 G$IE_7$0$0 == 0x00af
                           0000AF   807 _IE_7	=	0x00af
                           0000AF   808 G$EA$0$0 == 0x00af
                           0000AF   809 _EA	=	0x00af
                           0000B8   810 G$IP_0$0$0 == 0x00b8
                           0000B8   811 _IP_0	=	0x00b8
                           0000B9   812 G$IP_1$0$0 == 0x00b9
                           0000B9   813 _IP_1	=	0x00b9
                           0000BA   814 G$IP_2$0$0 == 0x00ba
                           0000BA   815 _IP_2	=	0x00ba
                           0000BB   816 G$IP_3$0$0 == 0x00bb
                           0000BB   817 _IP_3	=	0x00bb
                           0000BC   818 G$IP_4$0$0 == 0x00bc
                           0000BC   819 _IP_4	=	0x00bc
                           0000BD   820 G$IP_5$0$0 == 0x00bd
                           0000BD   821 _IP_5	=	0x00bd
                           0000BE   822 G$IP_6$0$0 == 0x00be
                           0000BE   823 _IP_6	=	0x00be
                           0000BF   824 G$IP_7$0$0 == 0x00bf
                           0000BF   825 _IP_7	=	0x00bf
                           0000D0   826 G$P$0$0 == 0x00d0
                           0000D0   827 _P	=	0x00d0
                           0000D1   828 G$F1$0$0 == 0x00d1
                           0000D1   829 _F1	=	0x00d1
                           0000D2   830 G$OV$0$0 == 0x00d2
                           0000D2   831 _OV	=	0x00d2
                           0000D3   832 G$RS0$0$0 == 0x00d3
                           0000D3   833 _RS0	=	0x00d3
                           0000D4   834 G$RS1$0$0 == 0x00d4
                           0000D4   835 _RS1	=	0x00d4
                           0000D5   836 G$F0$0$0 == 0x00d5
                           0000D5   837 _F0	=	0x00d5
                           0000D6   838 G$AC$0$0 == 0x00d6
                           0000D6   839 _AC	=	0x00d6
                           0000D7   840 G$CY$0$0 == 0x00d7
                           0000D7   841 _CY	=	0x00d7
                           0000C8   842 G$PINA_0$0$0 == 0x00c8
                           0000C8   843 _PINA_0	=	0x00c8
                           0000C9   844 G$PINA_1$0$0 == 0x00c9
                           0000C9   845 _PINA_1	=	0x00c9
                           0000CA   846 G$PINA_2$0$0 == 0x00ca
                           0000CA   847 _PINA_2	=	0x00ca
                           0000CB   848 G$PINA_3$0$0 == 0x00cb
                           0000CB   849 _PINA_3	=	0x00cb
                           0000CC   850 G$PINA_4$0$0 == 0x00cc
                           0000CC   851 _PINA_4	=	0x00cc
                           0000CD   852 G$PINA_5$0$0 == 0x00cd
                           0000CD   853 _PINA_5	=	0x00cd
                           0000CE   854 G$PINA_6$0$0 == 0x00ce
                           0000CE   855 _PINA_6	=	0x00ce
                           0000CF   856 G$PINA_7$0$0 == 0x00cf
                           0000CF   857 _PINA_7	=	0x00cf
                           0000E8   858 G$PINB_0$0$0 == 0x00e8
                           0000E8   859 _PINB_0	=	0x00e8
                           0000E9   860 G$PINB_1$0$0 == 0x00e9
                           0000E9   861 _PINB_1	=	0x00e9
                           0000EA   862 G$PINB_2$0$0 == 0x00ea
                           0000EA   863 _PINB_2	=	0x00ea
                           0000EB   864 G$PINB_3$0$0 == 0x00eb
                           0000EB   865 _PINB_3	=	0x00eb
                           0000EC   866 G$PINB_4$0$0 == 0x00ec
                           0000EC   867 _PINB_4	=	0x00ec
                           0000ED   868 G$PINB_5$0$0 == 0x00ed
                           0000ED   869 _PINB_5	=	0x00ed
                           0000EE   870 G$PINB_6$0$0 == 0x00ee
                           0000EE   871 _PINB_6	=	0x00ee
                           0000EF   872 G$PINB_7$0$0 == 0x00ef
                           0000EF   873 _PINB_7	=	0x00ef
                           0000F8   874 G$PINC_0$0$0 == 0x00f8
                           0000F8   875 _PINC_0	=	0x00f8
                           0000F9   876 G$PINC_1$0$0 == 0x00f9
                           0000F9   877 _PINC_1	=	0x00f9
                           0000FA   878 G$PINC_2$0$0 == 0x00fa
                           0000FA   879 _PINC_2	=	0x00fa
                           0000FB   880 G$PINC_3$0$0 == 0x00fb
                           0000FB   881 _PINC_3	=	0x00fb
                           0000FC   882 G$PINC_4$0$0 == 0x00fc
                           0000FC   883 _PINC_4	=	0x00fc
                           0000FD   884 G$PINC_5$0$0 == 0x00fd
                           0000FD   885 _PINC_5	=	0x00fd
                           0000FE   886 G$PINC_6$0$0 == 0x00fe
                           0000FE   887 _PINC_6	=	0x00fe
                           0000FF   888 G$PINC_7$0$0 == 0x00ff
                           0000FF   889 _PINC_7	=	0x00ff
                           000080   890 G$PORTA_0$0$0 == 0x0080
                           000080   891 _PORTA_0	=	0x0080
                           000081   892 G$PORTA_1$0$0 == 0x0081
                           000081   893 _PORTA_1	=	0x0081
                           000082   894 G$PORTA_2$0$0 == 0x0082
                           000082   895 _PORTA_2	=	0x0082
                           000083   896 G$PORTA_3$0$0 == 0x0083
                           000083   897 _PORTA_3	=	0x0083
                           000084   898 G$PORTA_4$0$0 == 0x0084
                           000084   899 _PORTA_4	=	0x0084
                           000085   900 G$PORTA_5$0$0 == 0x0085
                           000085   901 _PORTA_5	=	0x0085
                           000086   902 G$PORTA_6$0$0 == 0x0086
                           000086   903 _PORTA_6	=	0x0086
                           000087   904 G$PORTA_7$0$0 == 0x0087
                           000087   905 _PORTA_7	=	0x0087
                           000088   906 G$PORTB_0$0$0 == 0x0088
                           000088   907 _PORTB_0	=	0x0088
                           000089   908 G$PORTB_1$0$0 == 0x0089
                           000089   909 _PORTB_1	=	0x0089
                           00008A   910 G$PORTB_2$0$0 == 0x008a
                           00008A   911 _PORTB_2	=	0x008a
                           00008B   912 G$PORTB_3$0$0 == 0x008b
                           00008B   913 _PORTB_3	=	0x008b
                           00008C   914 G$PORTB_4$0$0 == 0x008c
                           00008C   915 _PORTB_4	=	0x008c
                           00008D   916 G$PORTB_5$0$0 == 0x008d
                           00008D   917 _PORTB_5	=	0x008d
                           00008E   918 G$PORTB_6$0$0 == 0x008e
                           00008E   919 _PORTB_6	=	0x008e
                           00008F   920 G$PORTB_7$0$0 == 0x008f
                           00008F   921 _PORTB_7	=	0x008f
                           000090   922 G$PORTC_0$0$0 == 0x0090
                           000090   923 _PORTC_0	=	0x0090
                           000091   924 G$PORTC_1$0$0 == 0x0091
                           000091   925 _PORTC_1	=	0x0091
                           000092   926 G$PORTC_2$0$0 == 0x0092
                           000092   927 _PORTC_2	=	0x0092
                           000093   928 G$PORTC_3$0$0 == 0x0093
                           000093   929 _PORTC_3	=	0x0093
                           000094   930 G$PORTC_4$0$0 == 0x0094
                           000094   931 _PORTC_4	=	0x0094
                           000095   932 G$PORTC_5$0$0 == 0x0095
                           000095   933 _PORTC_5	=	0x0095
                           000096   934 G$PORTC_6$0$0 == 0x0096
                           000096   935 _PORTC_6	=	0x0096
                           000097   936 G$PORTC_7$0$0 == 0x0097
                           000097   937 _PORTC_7	=	0x0097
                                    938 ;--------------------------------------------------------
                                    939 ; overlayable register banks
                                    940 ;--------------------------------------------------------
                                    941 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        942 	.ds 8
                                    943 ;--------------------------------------------------------
                                    944 ; internal ram data
                                    945 ;--------------------------------------------------------
                                    946 	.area DSEG    (DATA)
                           000000   947 G$per_test_counter$0$0==.
      00001A                        948 _per_test_counter::
      00001A                        949 	.ds 2
                           000002   950 G$per_test_counter_previous$0$0==.
      00001C                        951 _per_test_counter_previous::
      00001C                        952 	.ds 2
                           000004   953 G$display_timing$0$0==.
      00001E                        954 _display_timing::
      00001E                        955 	.ds 1
                           000005   956 Fdisplay$dbglink_semaphore$0$0==.
      00001F                        957 _dbglink_semaphore:
      00001F                        958 	.ds 1
                                    959 ;--------------------------------------------------------
                                    960 ; overlayable items in internal ram 
                                    961 ;--------------------------------------------------------
                                    962 ;--------------------------------------------------------
                                    963 ; indirectly addressable internal ram data
                                    964 ;--------------------------------------------------------
                                    965 	.area ISEG    (DATA)
                                    966 ;--------------------------------------------------------
                                    967 ; absolute internal ram data
                                    968 ;--------------------------------------------------------
                                    969 	.area IABS    (ABS,DATA)
                                    970 	.area IABS    (ABS,DATA)
                                    971 ;--------------------------------------------------------
                                    972 ; bit data
                                    973 ;--------------------------------------------------------
                                    974 	.area BSEG    (BIT)
                                    975 ;--------------------------------------------------------
                                    976 ; paged external ram data
                                    977 ;--------------------------------------------------------
                                    978 	.area PSEG    (PAG,XDATA)
                                    979 ;--------------------------------------------------------
                                    980 ; external ram data
                                    981 ;--------------------------------------------------------
                                    982 	.area XSEG    (XDATA)
                           007020   983 G$ADCCH0VAL0$0$0 == 0x7020
                           007020   984 _ADCCH0VAL0	=	0x7020
                           007021   985 G$ADCCH0VAL1$0$0 == 0x7021
                           007021   986 _ADCCH0VAL1	=	0x7021
                           007020   987 G$ADCCH0VAL$0$0 == 0x7020
                           007020   988 _ADCCH0VAL	=	0x7020
                           007022   989 G$ADCCH1VAL0$0$0 == 0x7022
                           007022   990 _ADCCH1VAL0	=	0x7022
                           007023   991 G$ADCCH1VAL1$0$0 == 0x7023
                           007023   992 _ADCCH1VAL1	=	0x7023
                           007022   993 G$ADCCH1VAL$0$0 == 0x7022
                           007022   994 _ADCCH1VAL	=	0x7022
                           007024   995 G$ADCCH2VAL0$0$0 == 0x7024
                           007024   996 _ADCCH2VAL0	=	0x7024
                           007025   997 G$ADCCH2VAL1$0$0 == 0x7025
                           007025   998 _ADCCH2VAL1	=	0x7025
                           007024   999 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1000 _ADCCH2VAL	=	0x7024
                           007026  1001 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1002 _ADCCH3VAL0	=	0x7026
                           007027  1003 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1004 _ADCCH3VAL1	=	0x7027
                           007026  1005 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1006 _ADCCH3VAL	=	0x7026
                           007028  1007 G$ADCTUNE0$0$0 == 0x7028
                           007028  1008 _ADCTUNE0	=	0x7028
                           007029  1009 G$ADCTUNE1$0$0 == 0x7029
                           007029  1010 _ADCTUNE1	=	0x7029
                           00702A  1011 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1012 _ADCTUNE2	=	0x702a
                           007010  1013 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1014 _DMA0ADDR0	=	0x7010
                           007011  1015 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1016 _DMA0ADDR1	=	0x7011
                           007010  1017 G$DMA0ADDR$0$0 == 0x7010
                           007010  1018 _DMA0ADDR	=	0x7010
                           007014  1019 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1020 _DMA0CONFIG	=	0x7014
                           007012  1021 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1022 _DMA1ADDR0	=	0x7012
                           007013  1023 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1024 _DMA1ADDR1	=	0x7013
                           007012  1025 G$DMA1ADDR$0$0 == 0x7012
                           007012  1026 _DMA1ADDR	=	0x7012
                           007015  1027 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1028 _DMA1CONFIG	=	0x7015
                           007070  1029 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1030 _FRCOSCCONFIG	=	0x7070
                           007071  1031 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1032 _FRCOSCCTRL	=	0x7071
                           007076  1033 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1034 _FRCOSCFREQ0	=	0x7076
                           007077  1035 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1036 _FRCOSCFREQ1	=	0x7077
                           007076  1037 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1038 _FRCOSCFREQ	=	0x7076
                           007072  1039 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1040 _FRCOSCKFILT0	=	0x7072
                           007073  1041 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1042 _FRCOSCKFILT1	=	0x7073
                           007072  1043 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1044 _FRCOSCKFILT	=	0x7072
                           007078  1045 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1046 _FRCOSCPER0	=	0x7078
                           007079  1047 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1048 _FRCOSCPER1	=	0x7079
                           007078  1049 G$FRCOSCPER$0$0 == 0x7078
                           007078  1050 _FRCOSCPER	=	0x7078
                           007074  1051 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1052 _FRCOSCREF0	=	0x7074
                           007075  1053 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1054 _FRCOSCREF1	=	0x7075
                           007074  1055 G$FRCOSCREF$0$0 == 0x7074
                           007074  1056 _FRCOSCREF	=	0x7074
                           007007  1057 G$ANALOGA$0$0 == 0x7007
                           007007  1058 _ANALOGA	=	0x7007
                           00700C  1059 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1060 _GPIOENABLE	=	0x700c
                           007003  1061 G$EXTIRQ$0$0 == 0x7003
                           007003  1062 _EXTIRQ	=	0x7003
                           007000  1063 G$INTCHGA$0$0 == 0x7000
                           007000  1064 _INTCHGA	=	0x7000
                           007001  1065 G$INTCHGB$0$0 == 0x7001
                           007001  1066 _INTCHGB	=	0x7001
                           007002  1067 G$INTCHGC$0$0 == 0x7002
                           007002  1068 _INTCHGC	=	0x7002
                           007008  1069 G$PALTA$0$0 == 0x7008
                           007008  1070 _PALTA	=	0x7008
                           007009  1071 G$PALTB$0$0 == 0x7009
                           007009  1072 _PALTB	=	0x7009
                           00700A  1073 G$PALTC$0$0 == 0x700a
                           00700A  1074 _PALTC	=	0x700a
                           007046  1075 G$PALTRADIO$0$0 == 0x7046
                           007046  1076 _PALTRADIO	=	0x7046
                           007004  1077 G$PINCHGA$0$0 == 0x7004
                           007004  1078 _PINCHGA	=	0x7004
                           007005  1079 G$PINCHGB$0$0 == 0x7005
                           007005  1080 _PINCHGB	=	0x7005
                           007006  1081 G$PINCHGC$0$0 == 0x7006
                           007006  1082 _PINCHGC	=	0x7006
                           00700B  1083 G$PINSEL$0$0 == 0x700b
                           00700B  1084 _PINSEL	=	0x700b
                           007060  1085 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1086 _LPOSCCONFIG	=	0x7060
                           007066  1087 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1088 _LPOSCFREQ0	=	0x7066
                           007067  1089 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1090 _LPOSCFREQ1	=	0x7067
                           007066  1091 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1092 _LPOSCFREQ	=	0x7066
                           007062  1093 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1094 _LPOSCKFILT0	=	0x7062
                           007063  1095 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1096 _LPOSCKFILT1	=	0x7063
                           007062  1097 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1098 _LPOSCKFILT	=	0x7062
                           007068  1099 G$LPOSCPER0$0$0 == 0x7068
                           007068  1100 _LPOSCPER0	=	0x7068
                           007069  1101 G$LPOSCPER1$0$0 == 0x7069
                           007069  1102 _LPOSCPER1	=	0x7069
                           007068  1103 G$LPOSCPER$0$0 == 0x7068
                           007068  1104 _LPOSCPER	=	0x7068
                           007064  1105 G$LPOSCREF0$0$0 == 0x7064
                           007064  1106 _LPOSCREF0	=	0x7064
                           007065  1107 G$LPOSCREF1$0$0 == 0x7065
                           007065  1108 _LPOSCREF1	=	0x7065
                           007064  1109 G$LPOSCREF$0$0 == 0x7064
                           007064  1110 _LPOSCREF	=	0x7064
                           007054  1111 G$LPXOSCGM$0$0 == 0x7054
                           007054  1112 _LPXOSCGM	=	0x7054
                           007F01  1113 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1114 _MISCCTRL	=	0x7f01
                           007053  1115 G$OSCCALIB$0$0 == 0x7053
                           007053  1116 _OSCCALIB	=	0x7053
                           007050  1117 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1118 _OSCFORCERUN	=	0x7050
                           007052  1119 G$OSCREADY$0$0 == 0x7052
                           007052  1120 _OSCREADY	=	0x7052
                           007051  1121 G$OSCRUN$0$0 == 0x7051
                           007051  1122 _OSCRUN	=	0x7051
                           007040  1123 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1124 _RADIOFDATAADDR0	=	0x7040
                           007041  1125 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1126 _RADIOFDATAADDR1	=	0x7041
                           007040  1127 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1128 _RADIOFDATAADDR	=	0x7040
                           007042  1129 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1130 _RADIOFSTATADDR0	=	0x7042
                           007043  1131 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1132 _RADIOFSTATADDR1	=	0x7043
                           007042  1133 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1134 _RADIOFSTATADDR	=	0x7042
                           007044  1135 G$RADIOMUX$0$0 == 0x7044
                           007044  1136 _RADIOMUX	=	0x7044
                           007084  1137 G$SCRATCH0$0$0 == 0x7084
                           007084  1138 _SCRATCH0	=	0x7084
                           007085  1139 G$SCRATCH1$0$0 == 0x7085
                           007085  1140 _SCRATCH1	=	0x7085
                           007086  1141 G$SCRATCH2$0$0 == 0x7086
                           007086  1142 _SCRATCH2	=	0x7086
                           007087  1143 G$SCRATCH3$0$0 == 0x7087
                           007087  1144 _SCRATCH3	=	0x7087
                           007F00  1145 G$SILICONREV$0$0 == 0x7f00
                           007F00  1146 _SILICONREV	=	0x7f00
                           007F19  1147 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1148 _XTALAMPL	=	0x7f19
                           007F18  1149 G$XTALOSC$0$0 == 0x7f18
                           007F18  1150 _XTALOSC	=	0x7f18
                           007F1A  1151 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1152 _XTALREADY	=	0x7f1a
                           00FC06  1153 Fdisplay$flash_deviceid$0$0 == 0xfc06
                           00FC06  1154 _flash_deviceid	=	0xfc06
                           00FC00  1155 Fdisplay$flash_calsector$0$0 == 0xfc00
                           00FC00  1156 _flash_calsector	=	0xfc00
                                   1157 ;--------------------------------------------------------
                                   1158 ; absolute external ram data
                                   1159 ;--------------------------------------------------------
                                   1160 	.area XABS    (ABS,XDATA)
                                   1161 ;--------------------------------------------------------
                                   1162 ; external initialized ram data
                                   1163 ;--------------------------------------------------------
                                   1164 	.area XISEG   (XDATA)
                                   1165 	.area HOME    (CODE)
                                   1166 	.area GSINIT0 (CODE)
                                   1167 	.area GSINIT1 (CODE)
                                   1168 	.area GSINIT2 (CODE)
                                   1169 	.area GSINIT3 (CODE)
                                   1170 	.area GSINIT4 (CODE)
                                   1171 	.area GSINIT5 (CODE)
                                   1172 	.area GSINIT  (CODE)
                                   1173 	.area GSFINAL (CODE)
                                   1174 	.area CSEG    (CODE)
                                   1175 ;--------------------------------------------------------
                                   1176 ; global & static initialisations
                                   1177 ;--------------------------------------------------------
                                   1178 	.area HOME    (CODE)
                                   1179 	.area GSINIT  (CODE)
                                   1180 	.area GSFINAL (CODE)
                                   1181 	.area GSINIT  (CODE)
                           000000  1182 	C$display.c$78$1$348 ==.
                                   1183 ;	display.c:78: uint16_t __data per_test_counter = 0, per_test_counter_previous = 0;
      00038A E4               [12] 1184 	clr	a
      00038B F5 1A            [12] 1185 	mov	_per_test_counter,a
      00038D F5 1B            [12] 1186 	mov	(_per_test_counter + 1),a
                           000005  1187 	C$display.c$78$1$348 ==.
                                   1188 ;	display.c:78: extern uint16_t __data pkts_received, pkts_missing;
      00038F F5 1C            [12] 1189 	mov	_per_test_counter_previous,a
      000391 F5 1D            [12] 1190 	mov	(_per_test_counter_previous + 1),a
                           000009  1191 	C$display.c$80$1$348 ==.
                                   1192 ;	display.c:80: uint8_t __data display_timing = 2;
      000393 75 1E 02         [24] 1193 	mov	_display_timing,#0x02
                           00000C  1194 	C$display.c$158$1$348 ==.
                                   1195 ;	display.c:158: static volatile uint8_t dbglink_semaphore = 0;
                                   1196 ;	1-genFromRTrack replaced	mov	_dbglink_semaphore,#0x00
      000396 F5 1F            [12] 1197 	mov	_dbglink_semaphore,a
                                   1198 ;--------------------------------------------------------
                                   1199 ; Home
                                   1200 ;--------------------------------------------------------
                                   1201 	.area HOME    (CODE)
                                   1202 	.area HOME    (CODE)
                                   1203 ;--------------------------------------------------------
                                   1204 ; code
                                   1205 ;--------------------------------------------------------
                                   1206 	.area CSEG    (CODE)
                                   1207 ;------------------------------------------------------------
                                   1208 ;Allocation info for local variables in function 'display_received_packet'
                                   1209 ;------------------------------------------------------------
                                   1210 ;st                        Allocated to registers r6 r7 
                                   1211 ;retran                    Allocated to registers r5 
                                   1212 ;------------------------------------------------------------
                           000000  1213 	G$display_received_packet$0$0 ==.
                           000000  1214 	C$display.c$82$0$0 ==.
                                   1215 ;	display.c:82: uint8_t display_received_packet(struct axradio_status __xdata *st)
                                   1216 ;	-----------------------------------------
                                   1217 ;	 function display_received_packet
                                   1218 ;	-----------------------------------------
      003E09                       1219 _display_received_packet:
                           000007  1220 	ar7 = 0x07
                           000006  1221 	ar6 = 0x06
                           000005  1222 	ar5 = 0x05
                           000004  1223 	ar4 = 0x04
                           000003  1224 	ar3 = 0x03
                           000002  1225 	ar2 = 0x02
                           000001  1226 	ar1 = 0x01
                           000000  1227 	ar0 = 0x00
      003E09 AE 82            [24] 1228 	mov	r6,dpl
      003E0B AF 83            [24] 1229 	mov	r7,dph
                           000004  1230 	C$display.c$84$1$0 ==.
                                   1231 ;	display.c:84: uint8_t retran = 0;
      003E0D 7D 00            [12] 1232 	mov	r5,#0x00
                           000006  1233 	C$display.c$96$1$337 ==.
                                   1234 ;	display.c:96: if (display_timing & 0x02)
      003E0F E5 1E            [12] 1235 	mov	a,_display_timing
      003E11 30 E1 3A         [24] 1236 	jnb	acc.1,00102$
                           00000B  1237 	C$display.c$99$2$338 ==.
                                   1238 ;	display.c:99: display_timing &= 0x01;
      003E14 53 1E 01         [24] 1239 	anl	_display_timing,#0x01
                           00000E  1240 	C$display.c$100$2$338 ==.
                                   1241 ;	display.c:100: display_setpos(0);
      003E17 75 82 00         [24] 1242 	mov	dpl,#0x00
      003E1A C0 07            [24] 1243 	push	ar7
      003E1C C0 06            [24] 1244 	push	ar6
      003E1E C0 05            [24] 1245 	push	ar5
      003E20 12 0A EA         [24] 1246 	lcall	_com0_setpos
      003E23 D0 05            [24] 1247 	pop	ar5
      003E25 D0 06            [24] 1248 	pop	ar6
      003E27 D0 07            [24] 1249 	pop	ar7
                           000020  1250 	C$display.c$101$2$338 ==.
                                   1251 ;	display.c:101: display_writestr(display_timing ? "P:      T:      \nL:      t:      \n" : "P:      O:      \nL:      R:      \n");
      003E29 E5 1E            [12] 1252 	mov	a,_display_timing
      003E2B 60 06            [24] 1253 	jz	00113$
      003E2D 7B A9            [12] 1254 	mov	r3,#___str_0
      003E2F 7C 5E            [12] 1255 	mov	r4,#(___str_0 >> 8)
      003E31 80 04            [24] 1256 	sjmp	00114$
      003E33                       1257 00113$:
      003E33 7B CC            [12] 1258 	mov	r3,#___str_1
      003E35 7C 5E            [12] 1259 	mov	r4,#(___str_1 >> 8)
      003E37                       1260 00114$:
      003E37 7A 80            [12] 1261 	mov	r2,#0x80
      003E39 8B 82            [24] 1262 	mov	dpl,r3
      003E3B 8C 83            [24] 1263 	mov	dph,r4
      003E3D 8A F0            [24] 1264 	mov	b,r2
      003E3F C0 07            [24] 1265 	push	ar7
      003E41 C0 06            [24] 1266 	push	ar6
      003E43 C0 05            [24] 1267 	push	ar5
      003E45 12 0B 06         [24] 1268 	lcall	_com0_writestr
      003E48 D0 05            [24] 1269 	pop	ar5
      003E4A D0 06            [24] 1270 	pop	ar6
      003E4C D0 07            [24] 1271 	pop	ar7
      003E4E                       1272 00102$:
                           000045  1273 	C$display.c$103$1$337 ==.
                                   1274 ;	display.c:103: display_setpos(0x03);
      003E4E 75 82 03         [24] 1275 	mov	dpl,#0x03
      003E51 C0 07            [24] 1276 	push	ar7
      003E53 C0 06            [24] 1277 	push	ar6
      003E55 C0 05            [24] 1278 	push	ar5
      003E57 12 0A EA         [24] 1279 	lcall	_com0_setpos
                           000051  1280 	C$display.c$104$1$337 ==.
                                   1281 ;	display.c:104: display_writehex16(pkts_received, 4, WRNUM_PADZERO);
      003E5A 74 08            [12] 1282 	mov	a,#0x08
      003E5C C0 E0            [24] 1283 	push	acc
      003E5E 03               [12] 1284 	rr	a
      003E5F C0 E0            [24] 1285 	push	acc
      003E61 85 23 82         [24] 1286 	mov	dpl,_pkts_received
      003E64 85 24 83         [24] 1287 	mov	dph,(_pkts_received + 1)
      003E67 12 45 97         [24] 1288 	lcall	_uart0_writehex16
      003E6A 15 81            [12] 1289 	dec	sp
      003E6C 15 81            [12] 1290 	dec	sp
                           000065  1291 	C$display.c$115$2$339 ==.
                                   1292 ;	display.c:115: display_setpos(0x0A);
      003E6E 75 82 0A         [24] 1293 	mov	dpl,#0x0a
      003E71 12 0A EA         [24] 1294 	lcall	_com0_setpos
      003E74 D0 05            [24] 1295 	pop	ar5
      003E76 D0 06            [24] 1296 	pop	ar6
      003E78 D0 07            [24] 1297 	pop	ar7
                           000071  1298 	C$display.c$116$2$339 ==.
                                   1299 ;	display.c:116: display_writenum16(axradio_conv_freq_tohz(st->u.rx.phy.offset), 6, WRNUM_SIGNED);
      003E7A 74 06            [12] 1300 	mov	a,#0x06
      003E7C 2E               [12] 1301 	add	a,r6
      003E7D FE               [12] 1302 	mov	r6,a
      003E7E E4               [12] 1303 	clr	a
      003E7F 3F               [12] 1304 	addc	a,r7
      003E80 FF               [12] 1305 	mov	r7,a
      003E81 8E 82            [24] 1306 	mov	dpl,r6
      003E83 8F 83            [24] 1307 	mov	dph,r7
      003E85 A3               [24] 1308 	inc	dptr
      003E86 A3               [24] 1309 	inc	dptr
      003E87 E0               [24] 1310 	movx	a,@dptr
      003E88 F9               [12] 1311 	mov	r1,a
      003E89 A3               [24] 1312 	inc	dptr
      003E8A E0               [24] 1313 	movx	a,@dptr
      003E8B FA               [12] 1314 	mov	r2,a
      003E8C A3               [24] 1315 	inc	dptr
      003E8D E0               [24] 1316 	movx	a,@dptr
      003E8E FB               [12] 1317 	mov	r3,a
      003E8F A3               [24] 1318 	inc	dptr
      003E90 E0               [24] 1319 	movx	a,@dptr
      003E91 89 82            [24] 1320 	mov	dpl,r1
      003E93 8A 83            [24] 1321 	mov	dph,r2
      003E95 8B F0            [24] 1322 	mov	b,r3
      003E97 C0 07            [24] 1323 	push	ar7
      003E99 C0 06            [24] 1324 	push	ar6
      003E9B C0 05            [24] 1325 	push	ar5
      003E9D 12 07 A2         [24] 1326 	lcall	_axradio_conv_freq_tohz
      003EA0 74 01            [12] 1327 	mov	a,#0x01
      003EA2 C0 E0            [24] 1328 	push	acc
      003EA4 74 06            [12] 1329 	mov	a,#0x06
      003EA6 C0 E0            [24] 1330 	push	acc
      003EA8 12 4E 25         [24] 1331 	lcall	_uart0_writenum16
      003EAB 15 81            [12] 1332 	dec	sp
      003EAD 15 81            [12] 1333 	dec	sp
                           0000A6  1334 	C$display.c$129$2$340 ==.
                                   1335 ;	display.c:129: display_setpos(0x4C);
      003EAF 75 82 4C         [24] 1336 	mov	dpl,#0x4c
      003EB2 12 0A EA         [24] 1337 	lcall	_com0_setpos
      003EB5 D0 05            [24] 1338 	pop	ar5
      003EB7 D0 06            [24] 1339 	pop	ar6
      003EB9 D0 07            [24] 1340 	pop	ar7
                           0000B2  1341 	C$display.c$130$2$340 ==.
                                   1342 ;	display.c:130: display_writenum16(st->u.rx.phy.rssi, 4, WRNUM_SIGNED);
      003EBB 8E 82            [24] 1343 	mov	dpl,r6
      003EBD 8F 83            [24] 1344 	mov	dph,r7
      003EBF E0               [24] 1345 	movx	a,@dptr
      003EC0 FB               [12] 1346 	mov	r3,a
      003EC1 A3               [24] 1347 	inc	dptr
      003EC2 E0               [24] 1348 	movx	a,@dptr
      003EC3 FC               [12] 1349 	mov	r4,a
      003EC4 C0 07            [24] 1350 	push	ar7
      003EC6 C0 06            [24] 1351 	push	ar6
      003EC8 C0 05            [24] 1352 	push	ar5
      003ECA 74 01            [12] 1353 	mov	a,#0x01
      003ECC C0 E0            [24] 1354 	push	acc
      003ECE 74 04            [12] 1355 	mov	a,#0x04
      003ED0 C0 E0            [24] 1356 	push	acc
      003ED2 8B 82            [24] 1357 	mov	dpl,r3
      003ED4 8C 83            [24] 1358 	mov	dph,r4
      003ED6 12 4E 25         [24] 1359 	lcall	_uart0_writenum16
      003ED9 15 81            [12] 1360 	dec	sp
      003EDB 15 81            [12] 1361 	dec	sp
      003EDD D0 05            [24] 1362 	pop	ar5
      003EDF D0 06            [24] 1363 	pop	ar6
      003EE1 D0 07            [24] 1364 	pop	ar7
                           0000DA  1365 	C$display.c$133$1$337 ==.
                                   1366 ;	display.c:133: if (framing_insert_counter)
      003EE3 90 5D B7         [24] 1367 	mov	dptr,#_framing_insert_counter
      003EE6 E4               [12] 1368 	clr	a
      003EE7 93               [24] 1369 	movc	a,@a+dptr
      003EE8 70 03            [24] 1370 	jnz	00132$
      003EEA 02 3F 81         [24] 1371 	ljmp	00109$
      003EED                       1372 00132$:
                           0000E4  1373 	C$display.c$135$2$341 ==.
                                   1374 ;	display.c:135: per_test_counter_previous = per_test_counter;
      003EED 85 1A 1C         [24] 1375 	mov	_per_test_counter_previous,_per_test_counter
      003EF0 85 1B 1D         [24] 1376 	mov	(_per_test_counter_previous + 1),(_per_test_counter + 1)
                           0000EA  1377 	C$display.c$136$2$341 ==.
                                   1378 ;	display.c:136: per_test_counter = ((st->u.rx.pktdata[framing_counter_pos+1])<<8) | st->u.rx.pktdata[framing_counter_pos];
      003EF3 74 16            [12] 1379 	mov	a,#0x16
      003EF5 2E               [12] 1380 	add	a,r6
      003EF6 F5 82            [12] 1381 	mov	dpl,a
      003EF8 E4               [12] 1382 	clr	a
      003EF9 3F               [12] 1383 	addc	a,r7
      003EFA F5 83            [12] 1384 	mov	dph,a
      003EFC E0               [24] 1385 	movx	a,@dptr
      003EFD FE               [12] 1386 	mov	r6,a
      003EFE A3               [24] 1387 	inc	dptr
      003EFF E0               [24] 1388 	movx	a,@dptr
      003F00 FF               [12] 1389 	mov	r7,a
      003F01 90 5D B8         [24] 1390 	mov	dptr,#_framing_counter_pos
      003F04 E4               [12] 1391 	clr	a
      003F05 93               [24] 1392 	movc	a,@a+dptr
      003F06 FC               [12] 1393 	mov	r4,a
      003F07 FA               [12] 1394 	mov	r2,a
      003F08 7B 00            [12] 1395 	mov	r3,#0x00
      003F0A 0A               [12] 1396 	inc	r2
      003F0B BA 00 01         [24] 1397 	cjne	r2,#0x00,00133$
      003F0E 0B               [12] 1398 	inc	r3
      003F0F                       1399 00133$:
      003F0F EA               [12] 1400 	mov	a,r2
      003F10 2E               [12] 1401 	add	a,r6
      003F11 F5 82            [12] 1402 	mov	dpl,a
      003F13 EB               [12] 1403 	mov	a,r3
      003F14 3F               [12] 1404 	addc	a,r7
      003F15 F5 83            [12] 1405 	mov	dph,a
      003F17 E0               [24] 1406 	movx	a,@dptr
      003F18 FA               [12] 1407 	mov	r2,a
      003F19 7B 00            [12] 1408 	mov	r3,#0x00
      003F1B EC               [12] 1409 	mov	a,r4
      003F1C 2E               [12] 1410 	add	a,r6
      003F1D F5 82            [12] 1411 	mov	dpl,a
      003F1F E4               [12] 1412 	clr	a
      003F20 3F               [12] 1413 	addc	a,r7
      003F21 F5 83            [12] 1414 	mov	dph,a
      003F23 E0               [24] 1415 	movx	a,@dptr
      003F24 FF               [12] 1416 	mov	r7,a
      003F25 7E 00            [12] 1417 	mov	r6,#0x00
      003F27 4B               [12] 1418 	orl	a,r3
      003F28 F5 1A            [12] 1419 	mov	_per_test_counter,a
      003F2A EE               [12] 1420 	mov	a,r6
      003F2B 4A               [12] 1421 	orl	a,r2
      003F2C F5 1B            [12] 1422 	mov	(_per_test_counter + 1),a
                           000125  1423 	C$display.c$137$2$341 ==.
                                   1424 ;	display.c:137: if (pkts_received != 1)
      003F2E 74 01            [12] 1425 	mov	a,#0x01
      003F30 B5 23 06         [24] 1426 	cjne	a,_pkts_received,00134$
      003F33 14               [12] 1427 	dec	a
      003F34 B5 24 02         [24] 1428 	cjne	a,(_pkts_received + 1),00134$
      003F37 80 28            [24] 1429 	sjmp	00107$
      003F39                       1430 00134$:
                           000130  1431 	C$display.c$139$3$342 ==.
                                   1432 ;	display.c:139: if (per_test_counter == per_test_counter_previous)
      003F39 E5 1C            [12] 1433 	mov	a,_per_test_counter_previous
      003F3B B5 1A 09         [24] 1434 	cjne	a,_per_test_counter,00104$
      003F3E E5 1D            [12] 1435 	mov	a,(_per_test_counter_previous + 1)
      003F40 B5 1B 04         [24] 1436 	cjne	a,(_per_test_counter + 1),00104$
                           00013A  1437 	C$display.c$140$3$342 ==.
                                   1438 ;	display.c:140: retran = 1;
      003F43 7D 01            [12] 1439 	mov	r5,#0x01
      003F45 80 1A            [24] 1440 	sjmp	00107$
      003F47                       1441 00104$:
                           00013E  1442 	C$display.c$142$3$342 ==.
                                   1443 ;	display.c:142: pkts_missing += per_test_counter - per_test_counter_previous - 1;
      003F47 E5 1A            [12] 1444 	mov	a,_per_test_counter
      003F49 C3               [12] 1445 	clr	c
      003F4A 95 1C            [12] 1446 	subb	a,_per_test_counter_previous
      003F4C FE               [12] 1447 	mov	r6,a
      003F4D E5 1B            [12] 1448 	mov	a,(_per_test_counter + 1)
      003F4F 95 1D            [12] 1449 	subb	a,(_per_test_counter_previous + 1)
      003F51 FF               [12] 1450 	mov	r7,a
      003F52 1E               [12] 1451 	dec	r6
      003F53 BE FF 01         [24] 1452 	cjne	r6,#0xff,00137$
      003F56 1F               [12] 1453 	dec	r7
      003F57                       1454 00137$:
      003F57 EE               [12] 1455 	mov	a,r6
      003F58 25 25            [12] 1456 	add	a,_pkts_missing
      003F5A F5 25            [12] 1457 	mov	_pkts_missing,a
      003F5C EF               [12] 1458 	mov	a,r7
      003F5D 35 26            [12] 1459 	addc	a,(_pkts_missing + 1)
      003F5F F5 26            [12] 1460 	mov	(_pkts_missing + 1),a
      003F61                       1461 00107$:
                           000158  1462 	C$display.c$146$2$341 ==.
                                   1463 ;	display.c:146: display_setpos(0x43);
      003F61 75 82 43         [24] 1464 	mov	dpl,#0x43
      003F64 C0 05            [24] 1465 	push	ar5
      003F66 12 0A EA         [24] 1466 	lcall	_com0_setpos
                           000160  1467 	C$display.c$147$2$341 ==.
                                   1468 ;	display.c:147: display_writehex16(pkts_missing, 4, WRNUM_PADZERO);
      003F69 74 08            [12] 1469 	mov	a,#0x08
      003F6B C0 E0            [24] 1470 	push	acc
      003F6D 03               [12] 1471 	rr	a
      003F6E C0 E0            [24] 1472 	push	acc
      003F70 85 25 82         [24] 1473 	mov	dpl,_pkts_missing
      003F73 85 26 83         [24] 1474 	mov	dph,(_pkts_missing + 1)
      003F76 12 45 97         [24] 1475 	lcall	_uart0_writehex16
      003F79 15 81            [12] 1476 	dec	sp
      003F7B 15 81            [12] 1477 	dec	sp
      003F7D D0 05            [24] 1478 	pop	ar5
      003F7F 80 13            [24] 1479 	sjmp	00110$
      003F81                       1480 00109$:
                           000178  1481 	C$display.c$151$2$343 ==.
                                   1482 ;	display.c:151: display_setpos(0x43);
      003F81 75 82 43         [24] 1483 	mov	dpl,#0x43
      003F84 C0 05            [24] 1484 	push	ar5
      003F86 12 0A EA         [24] 1485 	lcall	_com0_setpos
                           000180  1486 	C$display.c$152$2$343 ==.
                                   1487 ;	display.c:152: display_writestr("?");
      003F89 90 5E EF         [24] 1488 	mov	dptr,#___str_2
      003F8C 75 F0 80         [24] 1489 	mov	b,#0x80
      003F8F 12 0B 06         [24] 1490 	lcall	_com0_writestr
      003F92 D0 05            [24] 1491 	pop	ar5
      003F94                       1492 00110$:
                           00018B  1493 	C$display.c$154$1$337 ==.
                                   1494 ;	display.c:154: return retran;
      003F94 8D 82            [24] 1495 	mov	dpl,r5
                           00018D  1496 	C$display.c$155$1$337 ==.
                           00018D  1497 	XG$display_received_packet$0$0 ==.
      003F96 22               [24] 1498 	ret
                                   1499 ;------------------------------------------------------------
                                   1500 ;Allocation info for local variables in function 'wait_dbglink_free'
                                   1501 ;------------------------------------------------------------
                           00018E  1502 	Fdisplay$wait_dbglink_free$0$0 ==.
                           00018E  1503 	C$display.c$160$1$337 ==.
                                   1504 ;	display.c:160: static void wait_dbglink_free(void)
                                   1505 ;	-----------------------------------------
                                   1506 ;	 function wait_dbglink_free
                                   1507 ;	-----------------------------------------
      003F97                       1508 _wait_dbglink_free:
      003F97                       1509 00104$:
                           00018E  1510 	C$display.c$163$2$346 ==.
                                   1511 ;	display.c:163: if (dbglink_txfree() >= 56)
      003F97 12 62 A9         [24] 1512 	lcall	_dbglink_txfree
      003F9A AF 82            [24] 1513 	mov	r7,dpl
      003F9C BF 38 00         [24] 1514 	cjne	r7,#0x38,00114$
      003F9F                       1515 00114$:
      003F9F 50 0B            [24] 1516 	jnc	00106$
                           000198  1517 	C$display.c$165$2$346 ==.
                                   1518 ;	display.c:165: wtimer_runcallbacks();
      003FA1 12 4C CF         [24] 1519 	lcall	_wtimer_runcallbacks
                           00019B  1520 	C$display.c$166$2$346 ==.
                                   1521 ;	display.c:166: wtimer_idle(WTFLAG_CANSTANDBY);
      003FA4 75 82 02         [24] 1522 	mov	dpl,#0x02
      003FA7 12 4C 4B         [24] 1523 	lcall	_wtimer_idle
      003FAA 80 EB            [24] 1524 	sjmp	00104$
      003FAC                       1525 00106$:
                           0001A3  1526 	C$display.c$168$1$345 ==.
                           0001A3  1527 	XFdisplay$wait_dbglink_free$0$0 ==.
      003FAC 22               [24] 1528 	ret
                                   1529 ;------------------------------------------------------------
                                   1530 ;Allocation info for local variables in function 'dbglink_received_packet'
                                   1531 ;------------------------------------------------------------
                                   1532 ;st                        Allocated to registers r6 r7 
                                   1533 ;pktlen                    Allocated to registers r4 r5 
                                   1534 ;i                         Allocated to registers r2 r3 
                                   1535 ;pktdata                   Allocated to registers r6 r7 
                                   1536 ;------------------------------------------------------------
                           0001A4  1537 	G$dbglink_received_packet$0$0 ==.
                           0001A4  1538 	C$display.c$171$1$345 ==.
                                   1539 ;	display.c:171: void dbglink_received_packet(struct axradio_status __xdata *st)
                                   1540 ;	-----------------------------------------
                                   1541 ;	 function dbglink_received_packet
                                   1542 ;	-----------------------------------------
      003FAD                       1543 _dbglink_received_packet:
      003FAD AE 82            [24] 1544 	mov	r6,dpl
      003FAF AF 83            [24] 1545 	mov	r7,dph
                           0001A8  1546 	C$display.c$175$1$348 ==.
                                   1547 ;	display.c:175: if (!(DBGLNKSTAT & 0x10))
      003FB1 E5 E2            [12] 1548 	mov	a,_DBGLNKSTAT
      003FB3 20 E4 03         [24] 1549 	jb	acc.4,00102$
                           0001AD  1550 	C$display.c$176$1$348 ==.
                                   1551 ;	display.c:176: return;
      003FB6 02 41 C2         [24] 1552 	ljmp	00111$
      003FB9                       1553 00102$:
                           0001B0  1554 	C$display.c$177$1$348 ==.
                                   1555 ;	display.c:177: ++dbglink_semaphore;
      003FB9 05 1F            [12] 1556 	inc	_dbglink_semaphore
                           0001B2  1557 	C$display.c$178$1$348 ==.
                                   1558 ;	display.c:178: if (dbglink_semaphore != 1) {
      003FBB 74 01            [12] 1559 	mov	a,#0x01
      003FBD B5 1F 02         [24] 1560 	cjne	a,_dbglink_semaphore,00130$
      003FC0 80 05            [24] 1561 	sjmp	00104$
      003FC2                       1562 00130$:
                           0001B9  1563 	C$display.c$179$2$349 ==.
                                   1564 ;	display.c:179: --dbglink_semaphore;
      003FC2 15 1F            [12] 1565 	dec	_dbglink_semaphore
                           0001BB  1566 	C$display.c$180$2$349 ==.
                                   1567 ;	display.c:180: return;
      003FC4 02 41 C2         [24] 1568 	ljmp	00111$
      003FC7                       1569 00104$:
                           0001BE  1570 	C$display.c$182$1$348 ==.
                                   1571 ;	display.c:182: pktlen = st->u.rx.pktlen + axradio_framing_maclen;
      003FC7 74 06            [12] 1572 	mov	a,#0x06
      003FC9 2E               [12] 1573 	add	a,r6
      003FCA FE               [12] 1574 	mov	r6,a
      003FCB E4               [12] 1575 	clr	a
      003FCC 3F               [12] 1576 	addc	a,r7
      003FCD FF               [12] 1577 	mov	r7,a
      003FCE 74 18            [12] 1578 	mov	a,#0x18
      003FD0 2E               [12] 1579 	add	a,r6
      003FD1 F5 82            [12] 1580 	mov	dpl,a
      003FD3 E4               [12] 1581 	clr	a
      003FD4 3F               [12] 1582 	addc	a,r7
      003FD5 F5 83            [12] 1583 	mov	dph,a
      003FD7 E0               [24] 1584 	movx	a,@dptr
      003FD8 FC               [12] 1585 	mov	r4,a
      003FD9 A3               [24] 1586 	inc	dptr
      003FDA E0               [24] 1587 	movx	a,@dptr
      003FDB FD               [12] 1588 	mov	r5,a
      003FDC 90 5D 52         [24] 1589 	mov	dptr,#_axradio_framing_maclen
      003FDF E4               [12] 1590 	clr	a
      003FE0 93               [24] 1591 	movc	a,@a+dptr
      003FE1 7A 00            [12] 1592 	mov	r2,#0x00
      003FE3 2C               [12] 1593 	add	a,r4
      003FE4 FC               [12] 1594 	mov	r4,a
      003FE5 EA               [12] 1595 	mov	a,r2
      003FE6 3D               [12] 1596 	addc	a,r5
      003FE7 FD               [12] 1597 	mov	r5,a
                           0001DF  1598 	C$display.c$183$1$348 ==.
                                   1599 ;	display.c:183: wait_dbglink_free();
      003FE8 C0 07            [24] 1600 	push	ar7
      003FEA C0 06            [24] 1601 	push	ar6
      003FEC C0 05            [24] 1602 	push	ar5
      003FEE C0 04            [24] 1603 	push	ar4
      003FF0 12 3F 97         [24] 1604 	lcall	_wait_dbglink_free
                           0001EA  1605 	C$display.c$184$1$348 ==.
                                   1606 ;	display.c:184: dbglink_writestr("RX counter=");
      003FF3 90 5E F1         [24] 1607 	mov	dptr,#___str_3
      003FF6 75 F0 80         [24] 1608 	mov	b,#0x80
      003FF9 12 54 1A         [24] 1609 	lcall	_dbglink_writestr
                           0001F3  1610 	C$display.c$186$1$348 ==.
                                   1611 ;	display.c:186: dbglink_writehex16(pkts_received, 4, WRNUM_PADZERO);
      003FFC 74 08            [12] 1612 	mov	a,#0x08
      003FFE C0 E0            [24] 1613 	push	acc
      004000 03               [12] 1614 	rr	a
      004001 C0 E0            [24] 1615 	push	acc
      004003 85 23 82         [24] 1616 	mov	dpl,_pkts_received
      004006 85 24 83         [24] 1617 	mov	dph,(_pkts_received + 1)
      004009 12 56 DC         [24] 1618 	lcall	_dbglink_writehex16
      00400C 15 81            [12] 1619 	dec	sp
      00400E 15 81            [12] 1620 	dec	sp
                           000207  1621 	C$display.c$187$1$348 ==.
                                   1622 ;	display.c:187: dbglink_writestr(" length=");
      004010 90 5E FD         [24] 1623 	mov	dptr,#___str_4
      004013 75 F0 80         [24] 1624 	mov	b,#0x80
      004016 12 54 1A         [24] 1625 	lcall	_dbglink_writestr
      004019 D0 04            [24] 1626 	pop	ar4
      00401B D0 05            [24] 1627 	pop	ar5
                           000214  1628 	C$display.c$188$1$348 ==.
                                   1629 ;	display.c:188: dbglink_writenum16(pktlen, 3, 0);
      00401D C0 05            [24] 1630 	push	ar5
      00401F C0 04            [24] 1631 	push	ar4
      004021 E4               [12] 1632 	clr	a
      004022 C0 E0            [24] 1633 	push	acc
      004024 74 03            [12] 1634 	mov	a,#0x03
      004026 C0 E0            [24] 1635 	push	acc
      004028 8C 82            [24] 1636 	mov	dpl,r4
      00402A 8D 83            [24] 1637 	mov	dph,r5
      00402C 12 5B 37         [24] 1638 	lcall	_dbglink_writenum16
      00402F 15 81            [12] 1639 	dec	sp
      004031 15 81            [12] 1640 	dec	sp
                           00022A  1641 	C$display.c$189$1$348 ==.
                                   1642 ;	display.c:189: wait_dbglink_free();
      004033 12 3F 97         [24] 1643 	lcall	_wait_dbglink_free
                           00022D  1644 	C$display.c$190$1$348 ==.
                                   1645 ;	display.c:190: dbglink_writestr(" RSSI=");
      004036 90 5F 06         [24] 1646 	mov	dptr,#___str_5
      004039 75 F0 80         [24] 1647 	mov	b,#0x80
      00403C 12 54 1A         [24] 1648 	lcall	_dbglink_writestr
      00403F D0 04            [24] 1649 	pop	ar4
      004041 D0 05            [24] 1650 	pop	ar5
      004043 D0 06            [24] 1651 	pop	ar6
      004045 D0 07            [24] 1652 	pop	ar7
                           00023E  1653 	C$display.c$191$1$348 ==.
                                   1654 ;	display.c:191: dbglink_writenum16(st->u.rx.phy.rssi, 3, WRNUM_SIGNED);
      004047 8E 82            [24] 1655 	mov	dpl,r6
      004049 8F 83            [24] 1656 	mov	dph,r7
      00404B E0               [24] 1657 	movx	a,@dptr
      00404C FA               [12] 1658 	mov	r2,a
      00404D A3               [24] 1659 	inc	dptr
      00404E E0               [24] 1660 	movx	a,@dptr
      00404F FB               [12] 1661 	mov	r3,a
      004050 C0 07            [24] 1662 	push	ar7
      004052 C0 06            [24] 1663 	push	ar6
      004054 C0 05            [24] 1664 	push	ar5
      004056 C0 04            [24] 1665 	push	ar4
      004058 74 01            [12] 1666 	mov	a,#0x01
      00405A C0 E0            [24] 1667 	push	acc
      00405C 74 03            [12] 1668 	mov	a,#0x03
      00405E C0 E0            [24] 1669 	push	acc
      004060 8A 82            [24] 1670 	mov	dpl,r2
      004062 8B 83            [24] 1671 	mov	dph,r3
      004064 12 5B 37         [24] 1672 	lcall	_dbglink_writenum16
      004067 15 81            [12] 1673 	dec	sp
      004069 15 81            [12] 1674 	dec	sp
                           000262  1675 	C$display.c$192$1$348 ==.
                                   1676 ;	display.c:192: dbglink_tx('\n');
      00406B 75 82 0A         [24] 1677 	mov	dpl,#0x0a
      00406E 12 46 FF         [24] 1678 	lcall	_dbglink_tx
                           000268  1679 	C$display.c$193$1$348 ==.
                                   1680 ;	display.c:193: wait_dbglink_free();
      004071 12 3F 97         [24] 1681 	lcall	_wait_dbglink_free
                           00026B  1682 	C$display.c$194$1$348 ==.
                                   1683 ;	display.c:194: dbglink_writestr("  freqoffset=");
      004074 90 5F 0D         [24] 1684 	mov	dptr,#___str_6
      004077 75 F0 80         [24] 1685 	mov	b,#0x80
      00407A 12 54 1A         [24] 1686 	lcall	_dbglink_writestr
      00407D D0 04            [24] 1687 	pop	ar4
      00407F D0 05            [24] 1688 	pop	ar5
      004081 D0 06            [24] 1689 	pop	ar6
      004083 D0 07            [24] 1690 	pop	ar7
                           00027C  1691 	C$display.c$195$1$348 ==.
                                   1692 ;	display.c:195: dbglink_writenum32(axradio_conv_freq_tohz(st->u.rx.phy.offset), 7, WRNUM_SIGNED);
      004085 8E 82            [24] 1693 	mov	dpl,r6
      004087 8F 83            [24] 1694 	mov	dph,r7
      004089 A3               [24] 1695 	inc	dptr
      00408A A3               [24] 1696 	inc	dptr
      00408B E0               [24] 1697 	movx	a,@dptr
      00408C F8               [12] 1698 	mov	r0,a
      00408D A3               [24] 1699 	inc	dptr
      00408E E0               [24] 1700 	movx	a,@dptr
      00408F F9               [12] 1701 	mov	r1,a
      004090 A3               [24] 1702 	inc	dptr
      004091 E0               [24] 1703 	movx	a,@dptr
      004092 FA               [12] 1704 	mov	r2,a
      004093 A3               [24] 1705 	inc	dptr
      004094 E0               [24] 1706 	movx	a,@dptr
      004095 88 82            [24] 1707 	mov	dpl,r0
      004097 89 83            [24] 1708 	mov	dph,r1
      004099 8A F0            [24] 1709 	mov	b,r2
      00409B C0 07            [24] 1710 	push	ar7
      00409D C0 06            [24] 1711 	push	ar6
      00409F C0 05            [24] 1712 	push	ar5
      0040A1 C0 04            [24] 1713 	push	ar4
      0040A3 12 07 A2         [24] 1714 	lcall	_axradio_conv_freq_tohz
      0040A6 A8 82            [24] 1715 	mov	r0,dpl
      0040A8 A9 83            [24] 1716 	mov	r1,dph
      0040AA AA F0            [24] 1717 	mov	r2,b
      0040AC FB               [12] 1718 	mov	r3,a
      0040AD 74 01            [12] 1719 	mov	a,#0x01
      0040AF C0 E0            [24] 1720 	push	acc
      0040B1 74 07            [12] 1721 	mov	a,#0x07
      0040B3 C0 E0            [24] 1722 	push	acc
      0040B5 88 82            [24] 1723 	mov	dpl,r0
      0040B7 89 83            [24] 1724 	mov	dph,r1
      0040B9 8A F0            [24] 1725 	mov	b,r2
      0040BB EB               [12] 1726 	mov	a,r3
      0040BC 12 59 C9         [24] 1727 	lcall	_dbglink_writenum32
      0040BF 15 81            [12] 1728 	dec	sp
      0040C1 15 81            [12] 1729 	dec	sp
                           0002BA  1730 	C$display.c$196$1$348 ==.
                                   1731 ;	display.c:196: dbglink_writestr("Hz/");
      0040C3 90 5F 1B         [24] 1732 	mov	dptr,#___str_7
      0040C6 75 F0 80         [24] 1733 	mov	b,#0x80
      0040C9 12 54 1A         [24] 1734 	lcall	_dbglink_writestr
                           0002C3  1735 	C$display.c$197$1$348 ==.
                                   1736 ;	display.c:197: dbglink_writenum32(axradio_conv_freq_tohz(axradio_get_freqoffset()), 7, WRNUM_SIGNED);
      0040CC 12 36 76         [24] 1737 	lcall	_axradio_get_freqoffset
      0040CF 12 07 A2         [24] 1738 	lcall	_axradio_conv_freq_tohz
      0040D2 A8 82            [24] 1739 	mov	r0,dpl
      0040D4 A9 83            [24] 1740 	mov	r1,dph
      0040D6 AA F0            [24] 1741 	mov	r2,b
      0040D8 FB               [12] 1742 	mov	r3,a
      0040D9 74 01            [12] 1743 	mov	a,#0x01
      0040DB C0 E0            [24] 1744 	push	acc
      0040DD 74 07            [12] 1745 	mov	a,#0x07
      0040DF C0 E0            [24] 1746 	push	acc
      0040E1 88 82            [24] 1747 	mov	dpl,r0
      0040E3 89 83            [24] 1748 	mov	dph,r1
      0040E5 8A F0            [24] 1749 	mov	b,r2
      0040E7 EB               [12] 1750 	mov	a,r3
      0040E8 12 59 C9         [24] 1751 	lcall	_dbglink_writenum32
      0040EB 15 81            [12] 1752 	dec	sp
      0040ED 15 81            [12] 1753 	dec	sp
                           0002E6  1754 	C$display.c$198$1$348 ==.
                                   1755 ;	display.c:198: dbglink_writestr("Hz");
      0040EF 90 5F 1F         [24] 1756 	mov	dptr,#___str_8
      0040F2 75 F0 80         [24] 1757 	mov	b,#0x80
      0040F5 12 54 1A         [24] 1758 	lcall	_dbglink_writestr
                           0002EF  1759 	C$display.c$199$1$348 ==.
                                   1760 ;	display.c:199: dbglink_tx('\n');
      0040F8 75 82 0A         [24] 1761 	mov	dpl,#0x0a
      0040FB 12 46 FF         [24] 1762 	lcall	_dbglink_tx
                           0002F5  1763 	C$display.c$200$1$348 ==.
                                   1764 ;	display.c:200: wait_dbglink_free();
      0040FE 12 3F 97         [24] 1765 	lcall	_wait_dbglink_free
                           0002F8  1766 	C$display.c$208$1$348 ==.
                                   1767 ;	display.c:208: wait_dbglink_free();
      004101 12 3F 97         [24] 1768 	lcall	_wait_dbglink_free
      004104 D0 04            [24] 1769 	pop	ar4
      004106 D0 05            [24] 1770 	pop	ar5
      004108 D0 06            [24] 1771 	pop	ar6
      00410A D0 07            [24] 1772 	pop	ar7
                           000303  1773 	C$display.c$211$2$350 ==.
                                   1774 ;	display.c:211: const uint8_t __xdata *pktdata = st->u.rx.mac.raw;
      00410C 74 14            [12] 1775 	mov	a,#0x14
      00410E 2E               [12] 1776 	add	a,r6
      00410F F5 82            [12] 1777 	mov	dpl,a
      004111 E4               [12] 1778 	clr	a
      004112 3F               [12] 1779 	addc	a,r7
      004113 F5 83            [12] 1780 	mov	dph,a
      004115 E0               [24] 1781 	movx	a,@dptr
      004116 FE               [12] 1782 	mov	r6,a
      004117 A3               [24] 1783 	inc	dptr
      004118 E0               [24] 1784 	movx	a,@dptr
      004119 FF               [12] 1785 	mov	r7,a
                           000311  1786 	C$display.c$212$4$352 ==.
                                   1787 ;	display.c:212: for (i=0; i < pktlen; ++i) {
      00411A 7A 00            [12] 1788 	mov	r2,#0x00
      00411C 7B 00            [12] 1789 	mov	r3,#0x00
      00411E                       1790 00109$:
      00411E C3               [12] 1791 	clr	c
      00411F EA               [12] 1792 	mov	a,r2
      004120 9C               [12] 1793 	subb	a,r4
      004121 EB               [12] 1794 	mov	a,r3
      004122 9D               [12] 1795 	subb	a,r5
      004123 40 03            [24] 1796 	jc	00131$
      004125 02 41 B7         [24] 1797 	ljmp	00107$
      004128                       1798 00131$:
                           00031F  1799 	C$display.c$213$3$351 ==.
                                   1800 ;	display.c:213: if (!(i & 0x000F)) {
      004128 EA               [12] 1801 	mov	a,r2
      004129 54 0F            [12] 1802 	anl	a,#0x0f
      00412B 70 45            [24] 1803 	jnz	00106$
                           000324  1804 	C$display.c$214$4$352 ==.
                                   1805 ;	display.c:214: dbglink_tx('\n');
      00412D 75 82 0A         [24] 1806 	mov	dpl,#0x0a
      004130 12 46 FF         [24] 1807 	lcall	_dbglink_tx
                           00032A  1808 	C$display.c$215$4$352 ==.
                                   1809 ;	display.c:215: wait_dbglink_free();
      004133 C0 07            [24] 1810 	push	ar7
      004135 C0 06            [24] 1811 	push	ar6
      004137 C0 05            [24] 1812 	push	ar5
      004139 C0 04            [24] 1813 	push	ar4
      00413B C0 03            [24] 1814 	push	ar3
      00413D C0 02            [24] 1815 	push	ar2
      00413F 12 3F 97         [24] 1816 	lcall	_wait_dbglink_free
      004142 D0 02            [24] 1817 	pop	ar2
      004144 D0 03            [24] 1818 	pop	ar3
                           00033D  1819 	C$display.c$216$4$352 ==.
                                   1820 ;	display.c:216: dbglink_writehex16(i, 3, WRNUM_PADZERO);
      004146 C0 03            [24] 1821 	push	ar3
      004148 C0 02            [24] 1822 	push	ar2
      00414A 74 08            [12] 1823 	mov	a,#0x08
      00414C C0 E0            [24] 1824 	push	acc
      00414E 74 03            [12] 1825 	mov	a,#0x03
      004150 C0 E0            [24] 1826 	push	acc
      004152 8A 82            [24] 1827 	mov	dpl,r2
      004154 8B 83            [24] 1828 	mov	dph,r3
      004156 12 56 DC         [24] 1829 	lcall	_dbglink_writehex16
      004159 15 81            [12] 1830 	dec	sp
      00415B 15 81            [12] 1831 	dec	sp
                           000354  1832 	C$display.c$217$4$352 ==.
                                   1833 ;	display.c:217: dbglink_writestr(": ");
      00415D 90 5F 22         [24] 1834 	mov	dptr,#___str_9
      004160 75 F0 80         [24] 1835 	mov	b,#0x80
      004163 12 54 1A         [24] 1836 	lcall	_dbglink_writestr
      004166 D0 02            [24] 1837 	pop	ar2
      004168 D0 03            [24] 1838 	pop	ar3
      00416A D0 04            [24] 1839 	pop	ar4
      00416C D0 05            [24] 1840 	pop	ar5
      00416E D0 06            [24] 1841 	pop	ar6
      004170 D0 07            [24] 1842 	pop	ar7
      004172                       1843 00106$:
                           000369  1844 	C$display.c$219$3$351 ==.
                                   1845 ;	display.c:219: dbglink_writehex16(pktdata[i], 2, WRNUM_PADZERO);
      004172 EA               [12] 1846 	mov	a,r2
      004173 2E               [12] 1847 	add	a,r6
      004174 F5 82            [12] 1848 	mov	dpl,a
      004176 EB               [12] 1849 	mov	a,r3
      004177 3F               [12] 1850 	addc	a,r7
      004178 F5 83            [12] 1851 	mov	dph,a
      00417A E0               [24] 1852 	movx	a,@dptr
      00417B F8               [12] 1853 	mov	r0,a
      00417C 79 00            [12] 1854 	mov	r1,#0x00
      00417E C0 07            [24] 1855 	push	ar7
      004180 C0 06            [24] 1856 	push	ar6
      004182 C0 05            [24] 1857 	push	ar5
      004184 C0 04            [24] 1858 	push	ar4
      004186 C0 03            [24] 1859 	push	ar3
      004188 C0 02            [24] 1860 	push	ar2
      00418A 74 08            [12] 1861 	mov	a,#0x08
      00418C C0 E0            [24] 1862 	push	acc
      00418E 74 02            [12] 1863 	mov	a,#0x02
      004190 C0 E0            [24] 1864 	push	acc
      004192 88 82            [24] 1865 	mov	dpl,r0
      004194 89 83            [24] 1866 	mov	dph,r1
      004196 12 56 DC         [24] 1867 	lcall	_dbglink_writehex16
      004199 15 81            [12] 1868 	dec	sp
      00419B 15 81            [12] 1869 	dec	sp
      00419D D0 02            [24] 1870 	pop	ar2
      00419F D0 03            [24] 1871 	pop	ar3
      0041A1 D0 04            [24] 1872 	pop	ar4
      0041A3 D0 05            [24] 1873 	pop	ar5
      0041A5 D0 06            [24] 1874 	pop	ar6
      0041A7 D0 07            [24] 1875 	pop	ar7
                           0003A0  1876 	C$display.c$220$3$351 ==.
                                   1877 ;	display.c:220: dbglink_tx(' ');
      0041A9 75 82 20         [24] 1878 	mov	dpl,#0x20
      0041AC 12 46 FF         [24] 1879 	lcall	_dbglink_tx
                           0003A6  1880 	C$display.c$212$2$350 ==.
                                   1881 ;	display.c:212: for (i=0; i < pktlen; ++i) {
      0041AF 0A               [12] 1882 	inc	r2
      0041B0 BA 00 01         [24] 1883 	cjne	r2,#0x00,00134$
      0041B3 0B               [12] 1884 	inc	r3
      0041B4                       1885 00134$:
      0041B4 02 41 1E         [24] 1886 	ljmp	00109$
      0041B7                       1887 00107$:
                           0003AE  1888 	C$display.c$223$1$348 ==.
                                   1889 ;	display.c:223: dbglink_writestr("\n\n");
      0041B7 90 5F 25         [24] 1890 	mov	dptr,#___str_10
      0041BA 75 F0 80         [24] 1891 	mov	b,#0x80
      0041BD 12 54 1A         [24] 1892 	lcall	_dbglink_writestr
                           0003B7  1893 	C$display.c$224$1$348 ==.
                                   1894 ;	display.c:224: --dbglink_semaphore;
      0041C0 15 1F            [12] 1895 	dec	_dbglink_semaphore
      0041C2                       1896 00111$:
                           0003B9  1897 	C$display.c$226$1$348 ==.
                           0003B9  1898 	XG$dbglink_received_packet$0$0 ==.
      0041C2 22               [24] 1899 	ret
                                   1900 	.area CSEG    (CODE)
                                   1901 	.area CONST   (CODE)
                           000000  1902 Fdisplay$__str_0$0$0 == .
      005EA9                       1903 ___str_0:
      005EA9 50 3A 20 20 20 20 20  1904 	.ascii "P:      T:      "
             20 54 3A 20 20 20 20
             20 20
      005EB9 0A                    1905 	.db 0x0a
      005EBA 4C 3A 20 20 20 20 20  1906 	.ascii "L:      t:      "
             20 74 3A 20 20 20 20
             20 20
      005ECA 0A                    1907 	.db 0x0a
      005ECB 00                    1908 	.db 0x00
                           000023  1909 Fdisplay$__str_1$0$0 == .
      005ECC                       1910 ___str_1:
      005ECC 50 3A 20 20 20 20 20  1911 	.ascii "P:      O:      "
             20 4F 3A 20 20 20 20
             20 20
      005EDC 0A                    1912 	.db 0x0a
      005EDD 4C 3A 20 20 20 20 20  1913 	.ascii "L:      R:      "
             20 52 3A 20 20 20 20
             20 20
      005EED 0A                    1914 	.db 0x0a
      005EEE 00                    1915 	.db 0x00
                           000046  1916 Fdisplay$__str_2$0$0 == .
      005EEF                       1917 ___str_2:
      005EEF 3F                    1918 	.ascii "?"
      005EF0 00                    1919 	.db 0x00
                           000048  1920 Fdisplay$__str_3$0$0 == .
      005EF1                       1921 ___str_3:
      005EF1 52 58 20 63 6F 75 6E  1922 	.ascii "RX counter="
             74 65 72 3D
      005EFC 00                    1923 	.db 0x00
                           000054  1924 Fdisplay$__str_4$0$0 == .
      005EFD                       1925 ___str_4:
      005EFD 20 6C 65 6E 67 74 68  1926 	.ascii " length="
             3D
      005F05 00                    1927 	.db 0x00
                           00005D  1928 Fdisplay$__str_5$0$0 == .
      005F06                       1929 ___str_5:
      005F06 20 52 53 53 49 3D     1930 	.ascii " RSSI="
      005F0C 00                    1931 	.db 0x00
                           000064  1932 Fdisplay$__str_6$0$0 == .
      005F0D                       1933 ___str_6:
      005F0D 20 20 66 72 65 71 6F  1934 	.ascii "  freqoffset="
             66 66 73 65 74 3D
      005F1A 00                    1935 	.db 0x00
                           000072  1936 Fdisplay$__str_7$0$0 == .
      005F1B                       1937 ___str_7:
      005F1B 48 7A 2F              1938 	.ascii "Hz/"
      005F1E 00                    1939 	.db 0x00
                           000076  1940 Fdisplay$__str_8$0$0 == .
      005F1F                       1941 ___str_8:
      005F1F 48 7A                 1942 	.ascii "Hz"
      005F21 00                    1943 	.db 0x00
                           000079  1944 Fdisplay$__str_9$0$0 == .
      005F22                       1945 ___str_9:
      005F22 3A 20                 1946 	.ascii ": "
      005F24 00                    1947 	.db 0x00
                           00007C  1948 Fdisplay$__str_10$0$0 == .
      005F25                       1949 ___str_10:
      005F25 0A                    1950 	.db 0x0a
      005F26 0A                    1951 	.db 0x0a
      005F27 00                    1952 	.db 0x00
                                   1953 	.area XINIT   (CODE)
                                   1954 	.area CABS    (ABS,CODE)
