                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module pktframing
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _swap_variables_PARM_2
                                     12 	.globl _packetPremFraming
                                     13 	.globl _GOLDSEQUENCE_Obtain
                                     14 	.globl _memcpy
                                     15 	.globl _delay_ms
                                     16 	.globl _crc_crc32_msb
                                     17 	.globl _dbglink_writehex32
                                     18 	.globl _dbglink_writestr
                                     19 	.globl _dbglink_tx
                                     20 	.globl _wtimer0_curtime
                                     21 	.globl _PORTC_7
                                     22 	.globl _PORTC_6
                                     23 	.globl _PORTC_5
                                     24 	.globl _PORTC_4
                                     25 	.globl _PORTC_3
                                     26 	.globl _PORTC_2
                                     27 	.globl _PORTC_1
                                     28 	.globl _PORTC_0
                                     29 	.globl _PORTB_7
                                     30 	.globl _PORTB_6
                                     31 	.globl _PORTB_5
                                     32 	.globl _PORTB_4
                                     33 	.globl _PORTB_3
                                     34 	.globl _PORTB_2
                                     35 	.globl _PORTB_1
                                     36 	.globl _PORTB_0
                                     37 	.globl _PORTA_7
                                     38 	.globl _PORTA_6
                                     39 	.globl _PORTA_5
                                     40 	.globl _PORTA_4
                                     41 	.globl _PORTA_3
                                     42 	.globl _PORTA_2
                                     43 	.globl _PORTA_1
                                     44 	.globl _PORTA_0
                                     45 	.globl _PINC_7
                                     46 	.globl _PINC_6
                                     47 	.globl _PINC_5
                                     48 	.globl _PINC_4
                                     49 	.globl _PINC_3
                                     50 	.globl _PINC_2
                                     51 	.globl _PINC_1
                                     52 	.globl _PINC_0
                                     53 	.globl _PINB_7
                                     54 	.globl _PINB_6
                                     55 	.globl _PINB_5
                                     56 	.globl _PINB_4
                                     57 	.globl _PINB_3
                                     58 	.globl _PINB_2
                                     59 	.globl _PINB_1
                                     60 	.globl _PINB_0
                                     61 	.globl _PINA_7
                                     62 	.globl _PINA_6
                                     63 	.globl _PINA_5
                                     64 	.globl _PINA_4
                                     65 	.globl _PINA_3
                                     66 	.globl _PINA_2
                                     67 	.globl _PINA_1
                                     68 	.globl _PINA_0
                                     69 	.globl _CY
                                     70 	.globl _AC
                                     71 	.globl _F0
                                     72 	.globl _RS1
                                     73 	.globl _RS0
                                     74 	.globl _OV
                                     75 	.globl _F1
                                     76 	.globl _P
                                     77 	.globl _IP_7
                                     78 	.globl _IP_6
                                     79 	.globl _IP_5
                                     80 	.globl _IP_4
                                     81 	.globl _IP_3
                                     82 	.globl _IP_2
                                     83 	.globl _IP_1
                                     84 	.globl _IP_0
                                     85 	.globl _EA
                                     86 	.globl _IE_7
                                     87 	.globl _IE_6
                                     88 	.globl _IE_5
                                     89 	.globl _IE_4
                                     90 	.globl _IE_3
                                     91 	.globl _IE_2
                                     92 	.globl _IE_1
                                     93 	.globl _IE_0
                                     94 	.globl _EIP_7
                                     95 	.globl _EIP_6
                                     96 	.globl _EIP_5
                                     97 	.globl _EIP_4
                                     98 	.globl _EIP_3
                                     99 	.globl _EIP_2
                                    100 	.globl _EIP_1
                                    101 	.globl _EIP_0
                                    102 	.globl _EIE_7
                                    103 	.globl _EIE_6
                                    104 	.globl _EIE_5
                                    105 	.globl _EIE_4
                                    106 	.globl _EIE_3
                                    107 	.globl _EIE_2
                                    108 	.globl _EIE_1
                                    109 	.globl _EIE_0
                                    110 	.globl _E2IP_7
                                    111 	.globl _E2IP_6
                                    112 	.globl _E2IP_5
                                    113 	.globl _E2IP_4
                                    114 	.globl _E2IP_3
                                    115 	.globl _E2IP_2
                                    116 	.globl _E2IP_1
                                    117 	.globl _E2IP_0
                                    118 	.globl _E2IE_7
                                    119 	.globl _E2IE_6
                                    120 	.globl _E2IE_5
                                    121 	.globl _E2IE_4
                                    122 	.globl _E2IE_3
                                    123 	.globl _E2IE_2
                                    124 	.globl _E2IE_1
                                    125 	.globl _E2IE_0
                                    126 	.globl _B_7
                                    127 	.globl _B_6
                                    128 	.globl _B_5
                                    129 	.globl _B_4
                                    130 	.globl _B_3
                                    131 	.globl _B_2
                                    132 	.globl _B_1
                                    133 	.globl _B_0
                                    134 	.globl _ACC_7
                                    135 	.globl _ACC_6
                                    136 	.globl _ACC_5
                                    137 	.globl _ACC_4
                                    138 	.globl _ACC_3
                                    139 	.globl _ACC_2
                                    140 	.globl _ACC_1
                                    141 	.globl _ACC_0
                                    142 	.globl _WTSTAT
                                    143 	.globl _WTIRQEN
                                    144 	.globl _WTEVTD
                                    145 	.globl _WTEVTD1
                                    146 	.globl _WTEVTD0
                                    147 	.globl _WTEVTC
                                    148 	.globl _WTEVTC1
                                    149 	.globl _WTEVTC0
                                    150 	.globl _WTEVTB
                                    151 	.globl _WTEVTB1
                                    152 	.globl _WTEVTB0
                                    153 	.globl _WTEVTA
                                    154 	.globl _WTEVTA1
                                    155 	.globl _WTEVTA0
                                    156 	.globl _WTCNTR1
                                    157 	.globl _WTCNTB
                                    158 	.globl _WTCNTB1
                                    159 	.globl _WTCNTB0
                                    160 	.globl _WTCNTA
                                    161 	.globl _WTCNTA1
                                    162 	.globl _WTCNTA0
                                    163 	.globl _WTCFGB
                                    164 	.globl _WTCFGA
                                    165 	.globl _WDTRESET
                                    166 	.globl _WDTCFG
                                    167 	.globl _U1STATUS
                                    168 	.globl _U1SHREG
                                    169 	.globl _U1MODE
                                    170 	.globl _U1CTRL
                                    171 	.globl _U0STATUS
                                    172 	.globl _U0SHREG
                                    173 	.globl _U0MODE
                                    174 	.globl _U0CTRL
                                    175 	.globl _T2STATUS
                                    176 	.globl _T2PERIOD
                                    177 	.globl _T2PERIOD1
                                    178 	.globl _T2PERIOD0
                                    179 	.globl _T2MODE
                                    180 	.globl _T2CNT
                                    181 	.globl _T2CNT1
                                    182 	.globl _T2CNT0
                                    183 	.globl _T2CLKSRC
                                    184 	.globl _T1STATUS
                                    185 	.globl _T1PERIOD
                                    186 	.globl _T1PERIOD1
                                    187 	.globl _T1PERIOD0
                                    188 	.globl _T1MODE
                                    189 	.globl _T1CNT
                                    190 	.globl _T1CNT1
                                    191 	.globl _T1CNT0
                                    192 	.globl _T1CLKSRC
                                    193 	.globl _T0STATUS
                                    194 	.globl _T0PERIOD
                                    195 	.globl _T0PERIOD1
                                    196 	.globl _T0PERIOD0
                                    197 	.globl _T0MODE
                                    198 	.globl _T0CNT
                                    199 	.globl _T0CNT1
                                    200 	.globl _T0CNT0
                                    201 	.globl _T0CLKSRC
                                    202 	.globl _SPSTATUS
                                    203 	.globl _SPSHREG
                                    204 	.globl _SPMODE
                                    205 	.globl _SPCLKSRC
                                    206 	.globl _RADIOSTAT
                                    207 	.globl _RADIOSTAT1
                                    208 	.globl _RADIOSTAT0
                                    209 	.globl _RADIODATA
                                    210 	.globl _RADIODATA3
                                    211 	.globl _RADIODATA2
                                    212 	.globl _RADIODATA1
                                    213 	.globl _RADIODATA0
                                    214 	.globl _RADIOADDR
                                    215 	.globl _RADIOADDR1
                                    216 	.globl _RADIOADDR0
                                    217 	.globl _RADIOACC
                                    218 	.globl _OC1STATUS
                                    219 	.globl _OC1PIN
                                    220 	.globl _OC1MODE
                                    221 	.globl _OC1COMP
                                    222 	.globl _OC1COMP1
                                    223 	.globl _OC1COMP0
                                    224 	.globl _OC0STATUS
                                    225 	.globl _OC0PIN
                                    226 	.globl _OC0MODE
                                    227 	.globl _OC0COMP
                                    228 	.globl _OC0COMP1
                                    229 	.globl _OC0COMP0
                                    230 	.globl _NVSTATUS
                                    231 	.globl _NVKEY
                                    232 	.globl _NVDATA
                                    233 	.globl _NVDATA1
                                    234 	.globl _NVDATA0
                                    235 	.globl _NVADDR
                                    236 	.globl _NVADDR1
                                    237 	.globl _NVADDR0
                                    238 	.globl _IC1STATUS
                                    239 	.globl _IC1MODE
                                    240 	.globl _IC1CAPT
                                    241 	.globl _IC1CAPT1
                                    242 	.globl _IC1CAPT0
                                    243 	.globl _IC0STATUS
                                    244 	.globl _IC0MODE
                                    245 	.globl _IC0CAPT
                                    246 	.globl _IC0CAPT1
                                    247 	.globl _IC0CAPT0
                                    248 	.globl _PORTR
                                    249 	.globl _PORTC
                                    250 	.globl _PORTB
                                    251 	.globl _PORTA
                                    252 	.globl _PINR
                                    253 	.globl _PINC
                                    254 	.globl _PINB
                                    255 	.globl _PINA
                                    256 	.globl _DIRR
                                    257 	.globl _DIRC
                                    258 	.globl _DIRB
                                    259 	.globl _DIRA
                                    260 	.globl _DBGLNKSTAT
                                    261 	.globl _DBGLNKBUF
                                    262 	.globl _CODECONFIG
                                    263 	.globl _CLKSTAT
                                    264 	.globl _CLKCON
                                    265 	.globl _ANALOGCOMP
                                    266 	.globl _ADCCONV
                                    267 	.globl _ADCCLKSRC
                                    268 	.globl _ADCCH3CONFIG
                                    269 	.globl _ADCCH2CONFIG
                                    270 	.globl _ADCCH1CONFIG
                                    271 	.globl _ADCCH0CONFIG
                                    272 	.globl __XPAGE
                                    273 	.globl _XPAGE
                                    274 	.globl _SP
                                    275 	.globl _PSW
                                    276 	.globl _PCON
                                    277 	.globl _IP
                                    278 	.globl _IE
                                    279 	.globl _EIP
                                    280 	.globl _EIE
                                    281 	.globl _E2IP
                                    282 	.globl _E2IE
                                    283 	.globl _DPS
                                    284 	.globl _DPTR1
                                    285 	.globl _DPTR0
                                    286 	.globl _DPL1
                                    287 	.globl _DPL
                                    288 	.globl _DPH1
                                    289 	.globl _DPH
                                    290 	.globl _B
                                    291 	.globl _ACC
                                    292 	.globl _StdPrem
                                    293 	.globl _trmID
                                    294 	.globl _dummyHMAC
                                    295 	.globl _msg3_tmr
                                    296 	.globl _msg2_tmr
                                    297 	.globl _msg1_tmr
                                    298 	.globl _premPacketBuffer
                                    299 	.globl _demoPacketBuffer
                                    300 	.globl _siblingSlots
                                    301 	.globl _randomSet
                                    302 	.globl _RNGMODE
                                    303 	.globl _RNGCLKSRC1
                                    304 	.globl _RNGCLKSRC0
                                    305 	.globl _RNGBYTE
                                    306 	.globl _AESOUTADDR
                                    307 	.globl _AESOUTADDR1
                                    308 	.globl _AESOUTADDR0
                                    309 	.globl _AESMODE
                                    310 	.globl _AESKEYADDR
                                    311 	.globl _AESKEYADDR1
                                    312 	.globl _AESKEYADDR0
                                    313 	.globl _AESINADDR
                                    314 	.globl _AESINADDR1
                                    315 	.globl _AESINADDR0
                                    316 	.globl _AESCURBLOCK
                                    317 	.globl _AESCONFIG
                                    318 	.globl _AX5043_TIMEGAIN3NB
                                    319 	.globl _AX5043_TIMEGAIN2NB
                                    320 	.globl _AX5043_TIMEGAIN1NB
                                    321 	.globl _AX5043_TIMEGAIN0NB
                                    322 	.globl _AX5043_RXPARAMSETSNB
                                    323 	.globl _AX5043_RXPARAMCURSETNB
                                    324 	.globl _AX5043_PKTMAXLENNB
                                    325 	.globl _AX5043_PKTLENOFFSETNB
                                    326 	.globl _AX5043_PKTLENCFGNB
                                    327 	.globl _AX5043_PKTADDRMASK3NB
                                    328 	.globl _AX5043_PKTADDRMASK2NB
                                    329 	.globl _AX5043_PKTADDRMASK1NB
                                    330 	.globl _AX5043_PKTADDRMASK0NB
                                    331 	.globl _AX5043_PKTADDRCFGNB
                                    332 	.globl _AX5043_PKTADDR3NB
                                    333 	.globl _AX5043_PKTADDR2NB
                                    334 	.globl _AX5043_PKTADDR1NB
                                    335 	.globl _AX5043_PKTADDR0NB
                                    336 	.globl _AX5043_PHASEGAIN3NB
                                    337 	.globl _AX5043_PHASEGAIN2NB
                                    338 	.globl _AX5043_PHASEGAIN1NB
                                    339 	.globl _AX5043_PHASEGAIN0NB
                                    340 	.globl _AX5043_FREQUENCYLEAKNB
                                    341 	.globl _AX5043_FREQUENCYGAIND3NB
                                    342 	.globl _AX5043_FREQUENCYGAIND2NB
                                    343 	.globl _AX5043_FREQUENCYGAIND1NB
                                    344 	.globl _AX5043_FREQUENCYGAIND0NB
                                    345 	.globl _AX5043_FREQUENCYGAINC3NB
                                    346 	.globl _AX5043_FREQUENCYGAINC2NB
                                    347 	.globl _AX5043_FREQUENCYGAINC1NB
                                    348 	.globl _AX5043_FREQUENCYGAINC0NB
                                    349 	.globl _AX5043_FREQUENCYGAINB3NB
                                    350 	.globl _AX5043_FREQUENCYGAINB2NB
                                    351 	.globl _AX5043_FREQUENCYGAINB1NB
                                    352 	.globl _AX5043_FREQUENCYGAINB0NB
                                    353 	.globl _AX5043_FREQUENCYGAINA3NB
                                    354 	.globl _AX5043_FREQUENCYGAINA2NB
                                    355 	.globl _AX5043_FREQUENCYGAINA1NB
                                    356 	.globl _AX5043_FREQUENCYGAINA0NB
                                    357 	.globl _AX5043_FREQDEV13NB
                                    358 	.globl _AX5043_FREQDEV12NB
                                    359 	.globl _AX5043_FREQDEV11NB
                                    360 	.globl _AX5043_FREQDEV10NB
                                    361 	.globl _AX5043_FREQDEV03NB
                                    362 	.globl _AX5043_FREQDEV02NB
                                    363 	.globl _AX5043_FREQDEV01NB
                                    364 	.globl _AX5043_FREQDEV00NB
                                    365 	.globl _AX5043_FOURFSK3NB
                                    366 	.globl _AX5043_FOURFSK2NB
                                    367 	.globl _AX5043_FOURFSK1NB
                                    368 	.globl _AX5043_FOURFSK0NB
                                    369 	.globl _AX5043_DRGAIN3NB
                                    370 	.globl _AX5043_DRGAIN2NB
                                    371 	.globl _AX5043_DRGAIN1NB
                                    372 	.globl _AX5043_DRGAIN0NB
                                    373 	.globl _AX5043_BBOFFSRES3NB
                                    374 	.globl _AX5043_BBOFFSRES2NB
                                    375 	.globl _AX5043_BBOFFSRES1NB
                                    376 	.globl _AX5043_BBOFFSRES0NB
                                    377 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    378 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    379 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    380 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    381 	.globl _AX5043_AGCTARGET3NB
                                    382 	.globl _AX5043_AGCTARGET2NB
                                    383 	.globl _AX5043_AGCTARGET1NB
                                    384 	.globl _AX5043_AGCTARGET0NB
                                    385 	.globl _AX5043_AGCMINMAX3NB
                                    386 	.globl _AX5043_AGCMINMAX2NB
                                    387 	.globl _AX5043_AGCMINMAX1NB
                                    388 	.globl _AX5043_AGCMINMAX0NB
                                    389 	.globl _AX5043_AGCGAIN3NB
                                    390 	.globl _AX5043_AGCGAIN2NB
                                    391 	.globl _AX5043_AGCGAIN1NB
                                    392 	.globl _AX5043_AGCGAIN0NB
                                    393 	.globl _AX5043_AGCAHYST3NB
                                    394 	.globl _AX5043_AGCAHYST2NB
                                    395 	.globl _AX5043_AGCAHYST1NB
                                    396 	.globl _AX5043_AGCAHYST0NB
                                    397 	.globl _AX5043_0xF44NB
                                    398 	.globl _AX5043_0xF35NB
                                    399 	.globl _AX5043_0xF34NB
                                    400 	.globl _AX5043_0xF33NB
                                    401 	.globl _AX5043_0xF32NB
                                    402 	.globl _AX5043_0xF31NB
                                    403 	.globl _AX5043_0xF30NB
                                    404 	.globl _AX5043_0xF26NB
                                    405 	.globl _AX5043_0xF23NB
                                    406 	.globl _AX5043_0xF22NB
                                    407 	.globl _AX5043_0xF21NB
                                    408 	.globl _AX5043_0xF1CNB
                                    409 	.globl _AX5043_0xF18NB
                                    410 	.globl _AX5043_0xF0CNB
                                    411 	.globl _AX5043_0xF00NB
                                    412 	.globl _AX5043_XTALSTATUSNB
                                    413 	.globl _AX5043_XTALOSCNB
                                    414 	.globl _AX5043_XTALCAPNB
                                    415 	.globl _AX5043_XTALAMPLNB
                                    416 	.globl _AX5043_WAKEUPXOEARLYNB
                                    417 	.globl _AX5043_WAKEUPTIMER1NB
                                    418 	.globl _AX5043_WAKEUPTIMER0NB
                                    419 	.globl _AX5043_WAKEUPFREQ1NB
                                    420 	.globl _AX5043_WAKEUPFREQ0NB
                                    421 	.globl _AX5043_WAKEUP1NB
                                    422 	.globl _AX5043_WAKEUP0NB
                                    423 	.globl _AX5043_TXRATE2NB
                                    424 	.globl _AX5043_TXRATE1NB
                                    425 	.globl _AX5043_TXRATE0NB
                                    426 	.globl _AX5043_TXPWRCOEFFE1NB
                                    427 	.globl _AX5043_TXPWRCOEFFE0NB
                                    428 	.globl _AX5043_TXPWRCOEFFD1NB
                                    429 	.globl _AX5043_TXPWRCOEFFD0NB
                                    430 	.globl _AX5043_TXPWRCOEFFC1NB
                                    431 	.globl _AX5043_TXPWRCOEFFC0NB
                                    432 	.globl _AX5043_TXPWRCOEFFB1NB
                                    433 	.globl _AX5043_TXPWRCOEFFB0NB
                                    434 	.globl _AX5043_TXPWRCOEFFA1NB
                                    435 	.globl _AX5043_TXPWRCOEFFA0NB
                                    436 	.globl _AX5043_TRKRFFREQ2NB
                                    437 	.globl _AX5043_TRKRFFREQ1NB
                                    438 	.globl _AX5043_TRKRFFREQ0NB
                                    439 	.globl _AX5043_TRKPHASE1NB
                                    440 	.globl _AX5043_TRKPHASE0NB
                                    441 	.globl _AX5043_TRKFSKDEMOD1NB
                                    442 	.globl _AX5043_TRKFSKDEMOD0NB
                                    443 	.globl _AX5043_TRKFREQ1NB
                                    444 	.globl _AX5043_TRKFREQ0NB
                                    445 	.globl _AX5043_TRKDATARATE2NB
                                    446 	.globl _AX5043_TRKDATARATE1NB
                                    447 	.globl _AX5043_TRKDATARATE0NB
                                    448 	.globl _AX5043_TRKAMPLITUDE1NB
                                    449 	.globl _AX5043_TRKAMPLITUDE0NB
                                    450 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    451 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    452 	.globl _AX5043_TMGTXSETTLENB
                                    453 	.globl _AX5043_TMGTXBOOSTNB
                                    454 	.globl _AX5043_TMGRXSETTLENB
                                    455 	.globl _AX5043_TMGRXRSSINB
                                    456 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    457 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    458 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    459 	.globl _AX5043_TMGRXOFFSACQNB
                                    460 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    461 	.globl _AX5043_TMGRXBOOSTNB
                                    462 	.globl _AX5043_TMGRXAGCNB
                                    463 	.globl _AX5043_TIMER2NB
                                    464 	.globl _AX5043_TIMER1NB
                                    465 	.globl _AX5043_TIMER0NB
                                    466 	.globl _AX5043_SILICONREVISIONNB
                                    467 	.globl _AX5043_SCRATCHNB
                                    468 	.globl _AX5043_RXDATARATE2NB
                                    469 	.globl _AX5043_RXDATARATE1NB
                                    470 	.globl _AX5043_RXDATARATE0NB
                                    471 	.globl _AX5043_RSSIREFERENCENB
                                    472 	.globl _AX5043_RSSIABSTHRNB
                                    473 	.globl _AX5043_RSSINB
                                    474 	.globl _AX5043_REFNB
                                    475 	.globl _AX5043_RADIOSTATENB
                                    476 	.globl _AX5043_RADIOEVENTREQ1NB
                                    477 	.globl _AX5043_RADIOEVENTREQ0NB
                                    478 	.globl _AX5043_RADIOEVENTMASK1NB
                                    479 	.globl _AX5043_RADIOEVENTMASK0NB
                                    480 	.globl _AX5043_PWRMODENB
                                    481 	.globl _AX5043_PWRAMPNB
                                    482 	.globl _AX5043_POWSTICKYSTATNB
                                    483 	.globl _AX5043_POWSTATNB
                                    484 	.globl _AX5043_POWIRQMASKNB
                                    485 	.globl _AX5043_POWCTRL1NB
                                    486 	.globl _AX5043_PLLVCOIRNB
                                    487 	.globl _AX5043_PLLVCOINB
                                    488 	.globl _AX5043_PLLVCODIVNB
                                    489 	.globl _AX5043_PLLRNGCLKNB
                                    490 	.globl _AX5043_PLLRANGINGBNB
                                    491 	.globl _AX5043_PLLRANGINGANB
                                    492 	.globl _AX5043_PLLLOOPBOOSTNB
                                    493 	.globl _AX5043_PLLLOOPNB
                                    494 	.globl _AX5043_PLLLOCKDETNB
                                    495 	.globl _AX5043_PLLCPIBOOSTNB
                                    496 	.globl _AX5043_PLLCPINB
                                    497 	.globl _AX5043_PKTSTOREFLAGSNB
                                    498 	.globl _AX5043_PKTMISCFLAGSNB
                                    499 	.globl _AX5043_PKTCHUNKSIZENB
                                    500 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    501 	.globl _AX5043_PINSTATENB
                                    502 	.globl _AX5043_PINFUNCSYSCLKNB
                                    503 	.globl _AX5043_PINFUNCPWRAMPNB
                                    504 	.globl _AX5043_PINFUNCIRQNB
                                    505 	.globl _AX5043_PINFUNCDCLKNB
                                    506 	.globl _AX5043_PINFUNCDATANB
                                    507 	.globl _AX5043_PINFUNCANTSELNB
                                    508 	.globl _AX5043_MODULATIONNB
                                    509 	.globl _AX5043_MODCFGPNB
                                    510 	.globl _AX5043_MODCFGFNB
                                    511 	.globl _AX5043_MODCFGANB
                                    512 	.globl _AX5043_MAXRFOFFSET2NB
                                    513 	.globl _AX5043_MAXRFOFFSET1NB
                                    514 	.globl _AX5043_MAXRFOFFSET0NB
                                    515 	.globl _AX5043_MAXDROFFSET2NB
                                    516 	.globl _AX5043_MAXDROFFSET1NB
                                    517 	.globl _AX5043_MAXDROFFSET0NB
                                    518 	.globl _AX5043_MATCH1PAT1NB
                                    519 	.globl _AX5043_MATCH1PAT0NB
                                    520 	.globl _AX5043_MATCH1MINNB
                                    521 	.globl _AX5043_MATCH1MAXNB
                                    522 	.globl _AX5043_MATCH1LENNB
                                    523 	.globl _AX5043_MATCH0PAT3NB
                                    524 	.globl _AX5043_MATCH0PAT2NB
                                    525 	.globl _AX5043_MATCH0PAT1NB
                                    526 	.globl _AX5043_MATCH0PAT0NB
                                    527 	.globl _AX5043_MATCH0MINNB
                                    528 	.globl _AX5043_MATCH0MAXNB
                                    529 	.globl _AX5043_MATCH0LENNB
                                    530 	.globl _AX5043_LPOSCSTATUSNB
                                    531 	.globl _AX5043_LPOSCREF1NB
                                    532 	.globl _AX5043_LPOSCREF0NB
                                    533 	.globl _AX5043_LPOSCPER1NB
                                    534 	.globl _AX5043_LPOSCPER0NB
                                    535 	.globl _AX5043_LPOSCKFILT1NB
                                    536 	.globl _AX5043_LPOSCKFILT0NB
                                    537 	.globl _AX5043_LPOSCFREQ1NB
                                    538 	.globl _AX5043_LPOSCFREQ0NB
                                    539 	.globl _AX5043_LPOSCCONFIGNB
                                    540 	.globl _AX5043_IRQREQUEST1NB
                                    541 	.globl _AX5043_IRQREQUEST0NB
                                    542 	.globl _AX5043_IRQMASK1NB
                                    543 	.globl _AX5043_IRQMASK0NB
                                    544 	.globl _AX5043_IRQINVERSION1NB
                                    545 	.globl _AX5043_IRQINVERSION0NB
                                    546 	.globl _AX5043_IFFREQ1NB
                                    547 	.globl _AX5043_IFFREQ0NB
                                    548 	.globl _AX5043_GPADCPERIODNB
                                    549 	.globl _AX5043_GPADCCTRLNB
                                    550 	.globl _AX5043_GPADC13VALUE1NB
                                    551 	.globl _AX5043_GPADC13VALUE0NB
                                    552 	.globl _AX5043_FSKDMIN1NB
                                    553 	.globl _AX5043_FSKDMIN0NB
                                    554 	.globl _AX5043_FSKDMAX1NB
                                    555 	.globl _AX5043_FSKDMAX0NB
                                    556 	.globl _AX5043_FSKDEV2NB
                                    557 	.globl _AX5043_FSKDEV1NB
                                    558 	.globl _AX5043_FSKDEV0NB
                                    559 	.globl _AX5043_FREQB3NB
                                    560 	.globl _AX5043_FREQB2NB
                                    561 	.globl _AX5043_FREQB1NB
                                    562 	.globl _AX5043_FREQB0NB
                                    563 	.globl _AX5043_FREQA3NB
                                    564 	.globl _AX5043_FREQA2NB
                                    565 	.globl _AX5043_FREQA1NB
                                    566 	.globl _AX5043_FREQA0NB
                                    567 	.globl _AX5043_FRAMINGNB
                                    568 	.globl _AX5043_FIFOTHRESH1NB
                                    569 	.globl _AX5043_FIFOTHRESH0NB
                                    570 	.globl _AX5043_FIFOSTATNB
                                    571 	.globl _AX5043_FIFOFREE1NB
                                    572 	.globl _AX5043_FIFOFREE0NB
                                    573 	.globl _AX5043_FIFODATANB
                                    574 	.globl _AX5043_FIFOCOUNT1NB
                                    575 	.globl _AX5043_FIFOCOUNT0NB
                                    576 	.globl _AX5043_FECSYNCNB
                                    577 	.globl _AX5043_FECSTATUSNB
                                    578 	.globl _AX5043_FECNB
                                    579 	.globl _AX5043_ENCODINGNB
                                    580 	.globl _AX5043_DIVERSITYNB
                                    581 	.globl _AX5043_DECIMATIONNB
                                    582 	.globl _AX5043_DACVALUE1NB
                                    583 	.globl _AX5043_DACVALUE0NB
                                    584 	.globl _AX5043_DACCONFIGNB
                                    585 	.globl _AX5043_CRCINIT3NB
                                    586 	.globl _AX5043_CRCINIT2NB
                                    587 	.globl _AX5043_CRCINIT1NB
                                    588 	.globl _AX5043_CRCINIT0NB
                                    589 	.globl _AX5043_BGNDRSSITHRNB
                                    590 	.globl _AX5043_BGNDRSSIGAINNB
                                    591 	.globl _AX5043_BGNDRSSINB
                                    592 	.globl _AX5043_BBTUNENB
                                    593 	.globl _AX5043_BBOFFSCAPNB
                                    594 	.globl _AX5043_AMPLFILTERNB
                                    595 	.globl _AX5043_AGCCOUNTERNB
                                    596 	.globl _AX5043_AFSKSPACE1NB
                                    597 	.globl _AX5043_AFSKSPACE0NB
                                    598 	.globl _AX5043_AFSKMARK1NB
                                    599 	.globl _AX5043_AFSKMARK0NB
                                    600 	.globl _AX5043_AFSKCTRLNB
                                    601 	.globl _AX5043_TIMEGAIN3
                                    602 	.globl _AX5043_TIMEGAIN2
                                    603 	.globl _AX5043_TIMEGAIN1
                                    604 	.globl _AX5043_TIMEGAIN0
                                    605 	.globl _AX5043_RXPARAMSETS
                                    606 	.globl _AX5043_RXPARAMCURSET
                                    607 	.globl _AX5043_PKTMAXLEN
                                    608 	.globl _AX5043_PKTLENOFFSET
                                    609 	.globl _AX5043_PKTLENCFG
                                    610 	.globl _AX5043_PKTADDRMASK3
                                    611 	.globl _AX5043_PKTADDRMASK2
                                    612 	.globl _AX5043_PKTADDRMASK1
                                    613 	.globl _AX5043_PKTADDRMASK0
                                    614 	.globl _AX5043_PKTADDRCFG
                                    615 	.globl _AX5043_PKTADDR3
                                    616 	.globl _AX5043_PKTADDR2
                                    617 	.globl _AX5043_PKTADDR1
                                    618 	.globl _AX5043_PKTADDR0
                                    619 	.globl _AX5043_PHASEGAIN3
                                    620 	.globl _AX5043_PHASEGAIN2
                                    621 	.globl _AX5043_PHASEGAIN1
                                    622 	.globl _AX5043_PHASEGAIN0
                                    623 	.globl _AX5043_FREQUENCYLEAK
                                    624 	.globl _AX5043_FREQUENCYGAIND3
                                    625 	.globl _AX5043_FREQUENCYGAIND2
                                    626 	.globl _AX5043_FREQUENCYGAIND1
                                    627 	.globl _AX5043_FREQUENCYGAIND0
                                    628 	.globl _AX5043_FREQUENCYGAINC3
                                    629 	.globl _AX5043_FREQUENCYGAINC2
                                    630 	.globl _AX5043_FREQUENCYGAINC1
                                    631 	.globl _AX5043_FREQUENCYGAINC0
                                    632 	.globl _AX5043_FREQUENCYGAINB3
                                    633 	.globl _AX5043_FREQUENCYGAINB2
                                    634 	.globl _AX5043_FREQUENCYGAINB1
                                    635 	.globl _AX5043_FREQUENCYGAINB0
                                    636 	.globl _AX5043_FREQUENCYGAINA3
                                    637 	.globl _AX5043_FREQUENCYGAINA2
                                    638 	.globl _AX5043_FREQUENCYGAINA1
                                    639 	.globl _AX5043_FREQUENCYGAINA0
                                    640 	.globl _AX5043_FREQDEV13
                                    641 	.globl _AX5043_FREQDEV12
                                    642 	.globl _AX5043_FREQDEV11
                                    643 	.globl _AX5043_FREQDEV10
                                    644 	.globl _AX5043_FREQDEV03
                                    645 	.globl _AX5043_FREQDEV02
                                    646 	.globl _AX5043_FREQDEV01
                                    647 	.globl _AX5043_FREQDEV00
                                    648 	.globl _AX5043_FOURFSK3
                                    649 	.globl _AX5043_FOURFSK2
                                    650 	.globl _AX5043_FOURFSK1
                                    651 	.globl _AX5043_FOURFSK0
                                    652 	.globl _AX5043_DRGAIN3
                                    653 	.globl _AX5043_DRGAIN2
                                    654 	.globl _AX5043_DRGAIN1
                                    655 	.globl _AX5043_DRGAIN0
                                    656 	.globl _AX5043_BBOFFSRES3
                                    657 	.globl _AX5043_BBOFFSRES2
                                    658 	.globl _AX5043_BBOFFSRES1
                                    659 	.globl _AX5043_BBOFFSRES0
                                    660 	.globl _AX5043_AMPLITUDEGAIN3
                                    661 	.globl _AX5043_AMPLITUDEGAIN2
                                    662 	.globl _AX5043_AMPLITUDEGAIN1
                                    663 	.globl _AX5043_AMPLITUDEGAIN0
                                    664 	.globl _AX5043_AGCTARGET3
                                    665 	.globl _AX5043_AGCTARGET2
                                    666 	.globl _AX5043_AGCTARGET1
                                    667 	.globl _AX5043_AGCTARGET0
                                    668 	.globl _AX5043_AGCMINMAX3
                                    669 	.globl _AX5043_AGCMINMAX2
                                    670 	.globl _AX5043_AGCMINMAX1
                                    671 	.globl _AX5043_AGCMINMAX0
                                    672 	.globl _AX5043_AGCGAIN3
                                    673 	.globl _AX5043_AGCGAIN2
                                    674 	.globl _AX5043_AGCGAIN1
                                    675 	.globl _AX5043_AGCGAIN0
                                    676 	.globl _AX5043_AGCAHYST3
                                    677 	.globl _AX5043_AGCAHYST2
                                    678 	.globl _AX5043_AGCAHYST1
                                    679 	.globl _AX5043_AGCAHYST0
                                    680 	.globl _AX5043_0xF44
                                    681 	.globl _AX5043_0xF35
                                    682 	.globl _AX5043_0xF34
                                    683 	.globl _AX5043_0xF33
                                    684 	.globl _AX5043_0xF32
                                    685 	.globl _AX5043_0xF31
                                    686 	.globl _AX5043_0xF30
                                    687 	.globl _AX5043_0xF26
                                    688 	.globl _AX5043_0xF23
                                    689 	.globl _AX5043_0xF22
                                    690 	.globl _AX5043_0xF21
                                    691 	.globl _AX5043_0xF1C
                                    692 	.globl _AX5043_0xF18
                                    693 	.globl _AX5043_0xF0C
                                    694 	.globl _AX5043_0xF00
                                    695 	.globl _AX5043_XTALSTATUS
                                    696 	.globl _AX5043_XTALOSC
                                    697 	.globl _AX5043_XTALCAP
                                    698 	.globl _AX5043_XTALAMPL
                                    699 	.globl _AX5043_WAKEUPXOEARLY
                                    700 	.globl _AX5043_WAKEUPTIMER1
                                    701 	.globl _AX5043_WAKEUPTIMER0
                                    702 	.globl _AX5043_WAKEUPFREQ1
                                    703 	.globl _AX5043_WAKEUPFREQ0
                                    704 	.globl _AX5043_WAKEUP1
                                    705 	.globl _AX5043_WAKEUP0
                                    706 	.globl _AX5043_TXRATE2
                                    707 	.globl _AX5043_TXRATE1
                                    708 	.globl _AX5043_TXRATE0
                                    709 	.globl _AX5043_TXPWRCOEFFE1
                                    710 	.globl _AX5043_TXPWRCOEFFE0
                                    711 	.globl _AX5043_TXPWRCOEFFD1
                                    712 	.globl _AX5043_TXPWRCOEFFD0
                                    713 	.globl _AX5043_TXPWRCOEFFC1
                                    714 	.globl _AX5043_TXPWRCOEFFC0
                                    715 	.globl _AX5043_TXPWRCOEFFB1
                                    716 	.globl _AX5043_TXPWRCOEFFB0
                                    717 	.globl _AX5043_TXPWRCOEFFA1
                                    718 	.globl _AX5043_TXPWRCOEFFA0
                                    719 	.globl _AX5043_TRKRFFREQ2
                                    720 	.globl _AX5043_TRKRFFREQ1
                                    721 	.globl _AX5043_TRKRFFREQ0
                                    722 	.globl _AX5043_TRKPHASE1
                                    723 	.globl _AX5043_TRKPHASE0
                                    724 	.globl _AX5043_TRKFSKDEMOD1
                                    725 	.globl _AX5043_TRKFSKDEMOD0
                                    726 	.globl _AX5043_TRKFREQ1
                                    727 	.globl _AX5043_TRKFREQ0
                                    728 	.globl _AX5043_TRKDATARATE2
                                    729 	.globl _AX5043_TRKDATARATE1
                                    730 	.globl _AX5043_TRKDATARATE0
                                    731 	.globl _AX5043_TRKAMPLITUDE1
                                    732 	.globl _AX5043_TRKAMPLITUDE0
                                    733 	.globl _AX5043_TRKAFSKDEMOD1
                                    734 	.globl _AX5043_TRKAFSKDEMOD0
                                    735 	.globl _AX5043_TMGTXSETTLE
                                    736 	.globl _AX5043_TMGTXBOOST
                                    737 	.globl _AX5043_TMGRXSETTLE
                                    738 	.globl _AX5043_TMGRXRSSI
                                    739 	.globl _AX5043_TMGRXPREAMBLE3
                                    740 	.globl _AX5043_TMGRXPREAMBLE2
                                    741 	.globl _AX5043_TMGRXPREAMBLE1
                                    742 	.globl _AX5043_TMGRXOFFSACQ
                                    743 	.globl _AX5043_TMGRXCOARSEAGC
                                    744 	.globl _AX5043_TMGRXBOOST
                                    745 	.globl _AX5043_TMGRXAGC
                                    746 	.globl _AX5043_TIMER2
                                    747 	.globl _AX5043_TIMER1
                                    748 	.globl _AX5043_TIMER0
                                    749 	.globl _AX5043_SILICONREVISION
                                    750 	.globl _AX5043_SCRATCH
                                    751 	.globl _AX5043_RXDATARATE2
                                    752 	.globl _AX5043_RXDATARATE1
                                    753 	.globl _AX5043_RXDATARATE0
                                    754 	.globl _AX5043_RSSIREFERENCE
                                    755 	.globl _AX5043_RSSIABSTHR
                                    756 	.globl _AX5043_RSSI
                                    757 	.globl _AX5043_REF
                                    758 	.globl _AX5043_RADIOSTATE
                                    759 	.globl _AX5043_RADIOEVENTREQ1
                                    760 	.globl _AX5043_RADIOEVENTREQ0
                                    761 	.globl _AX5043_RADIOEVENTMASK1
                                    762 	.globl _AX5043_RADIOEVENTMASK0
                                    763 	.globl _AX5043_PWRMODE
                                    764 	.globl _AX5043_PWRAMP
                                    765 	.globl _AX5043_POWSTICKYSTAT
                                    766 	.globl _AX5043_POWSTAT
                                    767 	.globl _AX5043_POWIRQMASK
                                    768 	.globl _AX5043_POWCTRL1
                                    769 	.globl _AX5043_PLLVCOIR
                                    770 	.globl _AX5043_PLLVCOI
                                    771 	.globl _AX5043_PLLVCODIV
                                    772 	.globl _AX5043_PLLRNGCLK
                                    773 	.globl _AX5043_PLLRANGINGB
                                    774 	.globl _AX5043_PLLRANGINGA
                                    775 	.globl _AX5043_PLLLOOPBOOST
                                    776 	.globl _AX5043_PLLLOOP
                                    777 	.globl _AX5043_PLLLOCKDET
                                    778 	.globl _AX5043_PLLCPIBOOST
                                    779 	.globl _AX5043_PLLCPI
                                    780 	.globl _AX5043_PKTSTOREFLAGS
                                    781 	.globl _AX5043_PKTMISCFLAGS
                                    782 	.globl _AX5043_PKTCHUNKSIZE
                                    783 	.globl _AX5043_PKTACCEPTFLAGS
                                    784 	.globl _AX5043_PINSTATE
                                    785 	.globl _AX5043_PINFUNCSYSCLK
                                    786 	.globl _AX5043_PINFUNCPWRAMP
                                    787 	.globl _AX5043_PINFUNCIRQ
                                    788 	.globl _AX5043_PINFUNCDCLK
                                    789 	.globl _AX5043_PINFUNCDATA
                                    790 	.globl _AX5043_PINFUNCANTSEL
                                    791 	.globl _AX5043_MODULATION
                                    792 	.globl _AX5043_MODCFGP
                                    793 	.globl _AX5043_MODCFGF
                                    794 	.globl _AX5043_MODCFGA
                                    795 	.globl _AX5043_MAXRFOFFSET2
                                    796 	.globl _AX5043_MAXRFOFFSET1
                                    797 	.globl _AX5043_MAXRFOFFSET0
                                    798 	.globl _AX5043_MAXDROFFSET2
                                    799 	.globl _AX5043_MAXDROFFSET1
                                    800 	.globl _AX5043_MAXDROFFSET0
                                    801 	.globl _AX5043_MATCH1PAT1
                                    802 	.globl _AX5043_MATCH1PAT0
                                    803 	.globl _AX5043_MATCH1MIN
                                    804 	.globl _AX5043_MATCH1MAX
                                    805 	.globl _AX5043_MATCH1LEN
                                    806 	.globl _AX5043_MATCH0PAT3
                                    807 	.globl _AX5043_MATCH0PAT2
                                    808 	.globl _AX5043_MATCH0PAT1
                                    809 	.globl _AX5043_MATCH0PAT0
                                    810 	.globl _AX5043_MATCH0MIN
                                    811 	.globl _AX5043_MATCH0MAX
                                    812 	.globl _AX5043_MATCH0LEN
                                    813 	.globl _AX5043_LPOSCSTATUS
                                    814 	.globl _AX5043_LPOSCREF1
                                    815 	.globl _AX5043_LPOSCREF0
                                    816 	.globl _AX5043_LPOSCPER1
                                    817 	.globl _AX5043_LPOSCPER0
                                    818 	.globl _AX5043_LPOSCKFILT1
                                    819 	.globl _AX5043_LPOSCKFILT0
                                    820 	.globl _AX5043_LPOSCFREQ1
                                    821 	.globl _AX5043_LPOSCFREQ0
                                    822 	.globl _AX5043_LPOSCCONFIG
                                    823 	.globl _AX5043_IRQREQUEST1
                                    824 	.globl _AX5043_IRQREQUEST0
                                    825 	.globl _AX5043_IRQMASK1
                                    826 	.globl _AX5043_IRQMASK0
                                    827 	.globl _AX5043_IRQINVERSION1
                                    828 	.globl _AX5043_IRQINVERSION0
                                    829 	.globl _AX5043_IFFREQ1
                                    830 	.globl _AX5043_IFFREQ0
                                    831 	.globl _AX5043_GPADCPERIOD
                                    832 	.globl _AX5043_GPADCCTRL
                                    833 	.globl _AX5043_GPADC13VALUE1
                                    834 	.globl _AX5043_GPADC13VALUE0
                                    835 	.globl _AX5043_FSKDMIN1
                                    836 	.globl _AX5043_FSKDMIN0
                                    837 	.globl _AX5043_FSKDMAX1
                                    838 	.globl _AX5043_FSKDMAX0
                                    839 	.globl _AX5043_FSKDEV2
                                    840 	.globl _AX5043_FSKDEV1
                                    841 	.globl _AX5043_FSKDEV0
                                    842 	.globl _AX5043_FREQB3
                                    843 	.globl _AX5043_FREQB2
                                    844 	.globl _AX5043_FREQB1
                                    845 	.globl _AX5043_FREQB0
                                    846 	.globl _AX5043_FREQA3
                                    847 	.globl _AX5043_FREQA2
                                    848 	.globl _AX5043_FREQA1
                                    849 	.globl _AX5043_FREQA0
                                    850 	.globl _AX5043_FRAMING
                                    851 	.globl _AX5043_FIFOTHRESH1
                                    852 	.globl _AX5043_FIFOTHRESH0
                                    853 	.globl _AX5043_FIFOSTAT
                                    854 	.globl _AX5043_FIFOFREE1
                                    855 	.globl _AX5043_FIFOFREE0
                                    856 	.globl _AX5043_FIFODATA
                                    857 	.globl _AX5043_FIFOCOUNT1
                                    858 	.globl _AX5043_FIFOCOUNT0
                                    859 	.globl _AX5043_FECSYNC
                                    860 	.globl _AX5043_FECSTATUS
                                    861 	.globl _AX5043_FEC
                                    862 	.globl _AX5043_ENCODING
                                    863 	.globl _AX5043_DIVERSITY
                                    864 	.globl _AX5043_DECIMATION
                                    865 	.globl _AX5043_DACVALUE1
                                    866 	.globl _AX5043_DACVALUE0
                                    867 	.globl _AX5043_DACCONFIG
                                    868 	.globl _AX5043_CRCINIT3
                                    869 	.globl _AX5043_CRCINIT2
                                    870 	.globl _AX5043_CRCINIT1
                                    871 	.globl _AX5043_CRCINIT0
                                    872 	.globl _AX5043_BGNDRSSITHR
                                    873 	.globl _AX5043_BGNDRSSIGAIN
                                    874 	.globl _AX5043_BGNDRSSI
                                    875 	.globl _AX5043_BBTUNE
                                    876 	.globl _AX5043_BBOFFSCAP
                                    877 	.globl _AX5043_AMPLFILTER
                                    878 	.globl _AX5043_AGCCOUNTER
                                    879 	.globl _AX5043_AFSKSPACE1
                                    880 	.globl _AX5043_AFSKSPACE0
                                    881 	.globl _AX5043_AFSKMARK1
                                    882 	.globl _AX5043_AFSKMARK0
                                    883 	.globl _AX5043_AFSKCTRL
                                    884 	.globl _XTALREADY
                                    885 	.globl _XTALOSC
                                    886 	.globl _XTALAMPL
                                    887 	.globl _SILICONREV
                                    888 	.globl _SCRATCH3
                                    889 	.globl _SCRATCH2
                                    890 	.globl _SCRATCH1
                                    891 	.globl _SCRATCH0
                                    892 	.globl _RADIOMUX
                                    893 	.globl _RADIOFSTATADDR
                                    894 	.globl _RADIOFSTATADDR1
                                    895 	.globl _RADIOFSTATADDR0
                                    896 	.globl _RADIOFDATAADDR
                                    897 	.globl _RADIOFDATAADDR1
                                    898 	.globl _RADIOFDATAADDR0
                                    899 	.globl _OSCRUN
                                    900 	.globl _OSCREADY
                                    901 	.globl _OSCFORCERUN
                                    902 	.globl _OSCCALIB
                                    903 	.globl _MISCCTRL
                                    904 	.globl _LPXOSCGM
                                    905 	.globl _LPOSCREF
                                    906 	.globl _LPOSCREF1
                                    907 	.globl _LPOSCREF0
                                    908 	.globl _LPOSCPER
                                    909 	.globl _LPOSCPER1
                                    910 	.globl _LPOSCPER0
                                    911 	.globl _LPOSCKFILT
                                    912 	.globl _LPOSCKFILT1
                                    913 	.globl _LPOSCKFILT0
                                    914 	.globl _LPOSCFREQ
                                    915 	.globl _LPOSCFREQ1
                                    916 	.globl _LPOSCFREQ0
                                    917 	.globl _LPOSCCONFIG
                                    918 	.globl _PINSEL
                                    919 	.globl _PINCHGC
                                    920 	.globl _PINCHGB
                                    921 	.globl _PINCHGA
                                    922 	.globl _PALTRADIO
                                    923 	.globl _PALTC
                                    924 	.globl _PALTB
                                    925 	.globl _PALTA
                                    926 	.globl _INTCHGC
                                    927 	.globl _INTCHGB
                                    928 	.globl _INTCHGA
                                    929 	.globl _EXTIRQ
                                    930 	.globl _GPIOENABLE
                                    931 	.globl _ANALOGA
                                    932 	.globl _FRCOSCREF
                                    933 	.globl _FRCOSCREF1
                                    934 	.globl _FRCOSCREF0
                                    935 	.globl _FRCOSCPER
                                    936 	.globl _FRCOSCPER1
                                    937 	.globl _FRCOSCPER0
                                    938 	.globl _FRCOSCKFILT
                                    939 	.globl _FRCOSCKFILT1
                                    940 	.globl _FRCOSCKFILT0
                                    941 	.globl _FRCOSCFREQ
                                    942 	.globl _FRCOSCFREQ1
                                    943 	.globl _FRCOSCFREQ0
                                    944 	.globl _FRCOSCCTRL
                                    945 	.globl _FRCOSCCONFIG
                                    946 	.globl _DMA1CONFIG
                                    947 	.globl _DMA1ADDR
                                    948 	.globl _DMA1ADDR1
                                    949 	.globl _DMA1ADDR0
                                    950 	.globl _DMA0CONFIG
                                    951 	.globl _DMA0ADDR
                                    952 	.globl _DMA0ADDR1
                                    953 	.globl _DMA0ADDR0
                                    954 	.globl _ADCTUNE2
                                    955 	.globl _ADCTUNE1
                                    956 	.globl _ADCTUNE0
                                    957 	.globl _ADCCH3VAL
                                    958 	.globl _ADCCH3VAL1
                                    959 	.globl _ADCCH3VAL0
                                    960 	.globl _ADCCH2VAL
                                    961 	.globl _ADCCH2VAL1
                                    962 	.globl _ADCCH2VAL0
                                    963 	.globl _ADCCH1VAL
                                    964 	.globl _ADCCH1VAL1
                                    965 	.globl _ADCCH1VAL0
                                    966 	.globl _ADCCH0VAL
                                    967 	.globl _ADCCH0VAL1
                                    968 	.globl _ADCCH0VAL0
                                    969 	.globl _swap_variables
                                    970 	.globl _randomset_generate
                                    971 	.globl _packetStdFraming
                                    972 ;--------------------------------------------------------
                                    973 ; special function registers
                                    974 ;--------------------------------------------------------
                                    975 	.area RSEG    (ABS,DATA)
      000000                        976 	.org 0x0000
                           0000E0   977 G$ACC$0$0 == 0x00e0
                           0000E0   978 _ACC	=	0x00e0
                           0000F0   979 G$B$0$0 == 0x00f0
                           0000F0   980 _B	=	0x00f0
                           000083   981 G$DPH$0$0 == 0x0083
                           000083   982 _DPH	=	0x0083
                           000085   983 G$DPH1$0$0 == 0x0085
                           000085   984 _DPH1	=	0x0085
                           000082   985 G$DPL$0$0 == 0x0082
                           000082   986 _DPL	=	0x0082
                           000084   987 G$DPL1$0$0 == 0x0084
                           000084   988 _DPL1	=	0x0084
                           008382   989 G$DPTR0$0$0 == 0x8382
                           008382   990 _DPTR0	=	0x8382
                           008584   991 G$DPTR1$0$0 == 0x8584
                           008584   992 _DPTR1	=	0x8584
                           000086   993 G$DPS$0$0 == 0x0086
                           000086   994 _DPS	=	0x0086
                           0000A0   995 G$E2IE$0$0 == 0x00a0
                           0000A0   996 _E2IE	=	0x00a0
                           0000C0   997 G$E2IP$0$0 == 0x00c0
                           0000C0   998 _E2IP	=	0x00c0
                           000098   999 G$EIE$0$0 == 0x0098
                           000098  1000 _EIE	=	0x0098
                           0000B0  1001 G$EIP$0$0 == 0x00b0
                           0000B0  1002 _EIP	=	0x00b0
                           0000A8  1003 G$IE$0$0 == 0x00a8
                           0000A8  1004 _IE	=	0x00a8
                           0000B8  1005 G$IP$0$0 == 0x00b8
                           0000B8  1006 _IP	=	0x00b8
                           000087  1007 G$PCON$0$0 == 0x0087
                           000087  1008 _PCON	=	0x0087
                           0000D0  1009 G$PSW$0$0 == 0x00d0
                           0000D0  1010 _PSW	=	0x00d0
                           000081  1011 G$SP$0$0 == 0x0081
                           000081  1012 _SP	=	0x0081
                           0000D9  1013 G$XPAGE$0$0 == 0x00d9
                           0000D9  1014 _XPAGE	=	0x00d9
                           0000D9  1015 G$_XPAGE$0$0 == 0x00d9
                           0000D9  1016 __XPAGE	=	0x00d9
                           0000CA  1017 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA  1018 _ADCCH0CONFIG	=	0x00ca
                           0000CB  1019 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB  1020 _ADCCH1CONFIG	=	0x00cb
                           0000D2  1021 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2  1022 _ADCCH2CONFIG	=	0x00d2
                           0000D3  1023 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3  1024 _ADCCH3CONFIG	=	0x00d3
                           0000D1  1025 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1  1026 _ADCCLKSRC	=	0x00d1
                           0000C9  1027 G$ADCCONV$0$0 == 0x00c9
                           0000C9  1028 _ADCCONV	=	0x00c9
                           0000E1  1029 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1  1030 _ANALOGCOMP	=	0x00e1
                           0000C6  1031 G$CLKCON$0$0 == 0x00c6
                           0000C6  1032 _CLKCON	=	0x00c6
                           0000C7  1033 G$CLKSTAT$0$0 == 0x00c7
                           0000C7  1034 _CLKSTAT	=	0x00c7
                           000097  1035 G$CODECONFIG$0$0 == 0x0097
                           000097  1036 _CODECONFIG	=	0x0097
                           0000E3  1037 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3  1038 _DBGLNKBUF	=	0x00e3
                           0000E2  1039 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2  1040 _DBGLNKSTAT	=	0x00e2
                           000089  1041 G$DIRA$0$0 == 0x0089
                           000089  1042 _DIRA	=	0x0089
                           00008A  1043 G$DIRB$0$0 == 0x008a
                           00008A  1044 _DIRB	=	0x008a
                           00008B  1045 G$DIRC$0$0 == 0x008b
                           00008B  1046 _DIRC	=	0x008b
                           00008E  1047 G$DIRR$0$0 == 0x008e
                           00008E  1048 _DIRR	=	0x008e
                           0000C8  1049 G$PINA$0$0 == 0x00c8
                           0000C8  1050 _PINA	=	0x00c8
                           0000E8  1051 G$PINB$0$0 == 0x00e8
                           0000E8  1052 _PINB	=	0x00e8
                           0000F8  1053 G$PINC$0$0 == 0x00f8
                           0000F8  1054 _PINC	=	0x00f8
                           00008D  1055 G$PINR$0$0 == 0x008d
                           00008D  1056 _PINR	=	0x008d
                           000080  1057 G$PORTA$0$0 == 0x0080
                           000080  1058 _PORTA	=	0x0080
                           000088  1059 G$PORTB$0$0 == 0x0088
                           000088  1060 _PORTB	=	0x0088
                           000090  1061 G$PORTC$0$0 == 0x0090
                           000090  1062 _PORTC	=	0x0090
                           00008C  1063 G$PORTR$0$0 == 0x008c
                           00008C  1064 _PORTR	=	0x008c
                           0000CE  1065 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE  1066 _IC0CAPT0	=	0x00ce
                           0000CF  1067 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF  1068 _IC0CAPT1	=	0x00cf
                           00CFCE  1069 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE  1070 _IC0CAPT	=	0xcfce
                           0000CC  1071 G$IC0MODE$0$0 == 0x00cc
                           0000CC  1072 _IC0MODE	=	0x00cc
                           0000CD  1073 G$IC0STATUS$0$0 == 0x00cd
                           0000CD  1074 _IC0STATUS	=	0x00cd
                           0000D6  1075 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6  1076 _IC1CAPT0	=	0x00d6
                           0000D7  1077 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7  1078 _IC1CAPT1	=	0x00d7
                           00D7D6  1079 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6  1080 _IC1CAPT	=	0xd7d6
                           0000D4  1081 G$IC1MODE$0$0 == 0x00d4
                           0000D4  1082 _IC1MODE	=	0x00d4
                           0000D5  1083 G$IC1STATUS$0$0 == 0x00d5
                           0000D5  1084 _IC1STATUS	=	0x00d5
                           000092  1085 G$NVADDR0$0$0 == 0x0092
                           000092  1086 _NVADDR0	=	0x0092
                           000093  1087 G$NVADDR1$0$0 == 0x0093
                           000093  1088 _NVADDR1	=	0x0093
                           009392  1089 G$NVADDR$0$0 == 0x9392
                           009392  1090 _NVADDR	=	0x9392
                           000094  1091 G$NVDATA0$0$0 == 0x0094
                           000094  1092 _NVDATA0	=	0x0094
                           000095  1093 G$NVDATA1$0$0 == 0x0095
                           000095  1094 _NVDATA1	=	0x0095
                           009594  1095 G$NVDATA$0$0 == 0x9594
                           009594  1096 _NVDATA	=	0x9594
                           000096  1097 G$NVKEY$0$0 == 0x0096
                           000096  1098 _NVKEY	=	0x0096
                           000091  1099 G$NVSTATUS$0$0 == 0x0091
                           000091  1100 _NVSTATUS	=	0x0091
                           0000BC  1101 G$OC0COMP0$0$0 == 0x00bc
                           0000BC  1102 _OC0COMP0	=	0x00bc
                           0000BD  1103 G$OC0COMP1$0$0 == 0x00bd
                           0000BD  1104 _OC0COMP1	=	0x00bd
                           00BDBC  1105 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC  1106 _OC0COMP	=	0xbdbc
                           0000B9  1107 G$OC0MODE$0$0 == 0x00b9
                           0000B9  1108 _OC0MODE	=	0x00b9
                           0000BA  1109 G$OC0PIN$0$0 == 0x00ba
                           0000BA  1110 _OC0PIN	=	0x00ba
                           0000BB  1111 G$OC0STATUS$0$0 == 0x00bb
                           0000BB  1112 _OC0STATUS	=	0x00bb
                           0000C4  1113 G$OC1COMP0$0$0 == 0x00c4
                           0000C4  1114 _OC1COMP0	=	0x00c4
                           0000C5  1115 G$OC1COMP1$0$0 == 0x00c5
                           0000C5  1116 _OC1COMP1	=	0x00c5
                           00C5C4  1117 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4  1118 _OC1COMP	=	0xc5c4
                           0000C1  1119 G$OC1MODE$0$0 == 0x00c1
                           0000C1  1120 _OC1MODE	=	0x00c1
                           0000C2  1121 G$OC1PIN$0$0 == 0x00c2
                           0000C2  1122 _OC1PIN	=	0x00c2
                           0000C3  1123 G$OC1STATUS$0$0 == 0x00c3
                           0000C3  1124 _OC1STATUS	=	0x00c3
                           0000B1  1125 G$RADIOACC$0$0 == 0x00b1
                           0000B1  1126 _RADIOACC	=	0x00b1
                           0000B3  1127 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3  1128 _RADIOADDR0	=	0x00b3
                           0000B2  1129 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2  1130 _RADIOADDR1	=	0x00b2
                           00B2B3  1131 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3  1132 _RADIOADDR	=	0xb2b3
                           0000B7  1133 G$RADIODATA0$0$0 == 0x00b7
                           0000B7  1134 _RADIODATA0	=	0x00b7
                           0000B6  1135 G$RADIODATA1$0$0 == 0x00b6
                           0000B6  1136 _RADIODATA1	=	0x00b6
                           0000B5  1137 G$RADIODATA2$0$0 == 0x00b5
                           0000B5  1138 _RADIODATA2	=	0x00b5
                           0000B4  1139 G$RADIODATA3$0$0 == 0x00b4
                           0000B4  1140 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1141 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7  1142 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1143 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE  1144 _RADIOSTAT0	=	0x00be
                           0000BF  1145 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF  1146 _RADIOSTAT1	=	0x00bf
                           00BFBE  1147 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE  1148 _RADIOSTAT	=	0xbfbe
                           0000DF  1149 G$SPCLKSRC$0$0 == 0x00df
                           0000DF  1150 _SPCLKSRC	=	0x00df
                           0000DC  1151 G$SPMODE$0$0 == 0x00dc
                           0000DC  1152 _SPMODE	=	0x00dc
                           0000DE  1153 G$SPSHREG$0$0 == 0x00de
                           0000DE  1154 _SPSHREG	=	0x00de
                           0000DD  1155 G$SPSTATUS$0$0 == 0x00dd
                           0000DD  1156 _SPSTATUS	=	0x00dd
                           00009A  1157 G$T0CLKSRC$0$0 == 0x009a
                           00009A  1158 _T0CLKSRC	=	0x009a
                           00009C  1159 G$T0CNT0$0$0 == 0x009c
                           00009C  1160 _T0CNT0	=	0x009c
                           00009D  1161 G$T0CNT1$0$0 == 0x009d
                           00009D  1162 _T0CNT1	=	0x009d
                           009D9C  1163 G$T0CNT$0$0 == 0x9d9c
                           009D9C  1164 _T0CNT	=	0x9d9c
                           000099  1165 G$T0MODE$0$0 == 0x0099
                           000099  1166 _T0MODE	=	0x0099
                           00009E  1167 G$T0PERIOD0$0$0 == 0x009e
                           00009E  1168 _T0PERIOD0	=	0x009e
                           00009F  1169 G$T0PERIOD1$0$0 == 0x009f
                           00009F  1170 _T0PERIOD1	=	0x009f
                           009F9E  1171 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E  1172 _T0PERIOD	=	0x9f9e
                           00009B  1173 G$T0STATUS$0$0 == 0x009b
                           00009B  1174 _T0STATUS	=	0x009b
                           0000A2  1175 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2  1176 _T1CLKSRC	=	0x00a2
                           0000A4  1177 G$T1CNT0$0$0 == 0x00a4
                           0000A4  1178 _T1CNT0	=	0x00a4
                           0000A5  1179 G$T1CNT1$0$0 == 0x00a5
                           0000A5  1180 _T1CNT1	=	0x00a5
                           00A5A4  1181 G$T1CNT$0$0 == 0xa5a4
                           00A5A4  1182 _T1CNT	=	0xa5a4
                           0000A1  1183 G$T1MODE$0$0 == 0x00a1
                           0000A1  1184 _T1MODE	=	0x00a1
                           0000A6  1185 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6  1186 _T1PERIOD0	=	0x00a6
                           0000A7  1187 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7  1188 _T1PERIOD1	=	0x00a7
                           00A7A6  1189 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6  1190 _T1PERIOD	=	0xa7a6
                           0000A3  1191 G$T1STATUS$0$0 == 0x00a3
                           0000A3  1192 _T1STATUS	=	0x00a3
                           0000AA  1193 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA  1194 _T2CLKSRC	=	0x00aa
                           0000AC  1195 G$T2CNT0$0$0 == 0x00ac
                           0000AC  1196 _T2CNT0	=	0x00ac
                           0000AD  1197 G$T2CNT1$0$0 == 0x00ad
                           0000AD  1198 _T2CNT1	=	0x00ad
                           00ADAC  1199 G$T2CNT$0$0 == 0xadac
                           00ADAC  1200 _T2CNT	=	0xadac
                           0000A9  1201 G$T2MODE$0$0 == 0x00a9
                           0000A9  1202 _T2MODE	=	0x00a9
                           0000AE  1203 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE  1204 _T2PERIOD0	=	0x00ae
                           0000AF  1205 G$T2PERIOD1$0$0 == 0x00af
                           0000AF  1206 _T2PERIOD1	=	0x00af
                           00AFAE  1207 G$T2PERIOD$0$0 == 0xafae
                           00AFAE  1208 _T2PERIOD	=	0xafae
                           0000AB  1209 G$T2STATUS$0$0 == 0x00ab
                           0000AB  1210 _T2STATUS	=	0x00ab
                           0000E4  1211 G$U0CTRL$0$0 == 0x00e4
                           0000E4  1212 _U0CTRL	=	0x00e4
                           0000E7  1213 G$U0MODE$0$0 == 0x00e7
                           0000E7  1214 _U0MODE	=	0x00e7
                           0000E6  1215 G$U0SHREG$0$0 == 0x00e6
                           0000E6  1216 _U0SHREG	=	0x00e6
                           0000E5  1217 G$U0STATUS$0$0 == 0x00e5
                           0000E5  1218 _U0STATUS	=	0x00e5
                           0000EC  1219 G$U1CTRL$0$0 == 0x00ec
                           0000EC  1220 _U1CTRL	=	0x00ec
                           0000EF  1221 G$U1MODE$0$0 == 0x00ef
                           0000EF  1222 _U1MODE	=	0x00ef
                           0000EE  1223 G$U1SHREG$0$0 == 0x00ee
                           0000EE  1224 _U1SHREG	=	0x00ee
                           0000ED  1225 G$U1STATUS$0$0 == 0x00ed
                           0000ED  1226 _U1STATUS	=	0x00ed
                           0000DA  1227 G$WDTCFG$0$0 == 0x00da
                           0000DA  1228 _WDTCFG	=	0x00da
                           0000DB  1229 G$WDTRESET$0$0 == 0x00db
                           0000DB  1230 _WDTRESET	=	0x00db
                           0000F1  1231 G$WTCFGA$0$0 == 0x00f1
                           0000F1  1232 _WTCFGA	=	0x00f1
                           0000F9  1233 G$WTCFGB$0$0 == 0x00f9
                           0000F9  1234 _WTCFGB	=	0x00f9
                           0000F2  1235 G$WTCNTA0$0$0 == 0x00f2
                           0000F2  1236 _WTCNTA0	=	0x00f2
                           0000F3  1237 G$WTCNTA1$0$0 == 0x00f3
                           0000F3  1238 _WTCNTA1	=	0x00f3
                           00F3F2  1239 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2  1240 _WTCNTA	=	0xf3f2
                           0000FA  1241 G$WTCNTB0$0$0 == 0x00fa
                           0000FA  1242 _WTCNTB0	=	0x00fa
                           0000FB  1243 G$WTCNTB1$0$0 == 0x00fb
                           0000FB  1244 _WTCNTB1	=	0x00fb
                           00FBFA  1245 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA  1246 _WTCNTB	=	0xfbfa
                           0000EB  1247 G$WTCNTR1$0$0 == 0x00eb
                           0000EB  1248 _WTCNTR1	=	0x00eb
                           0000F4  1249 G$WTEVTA0$0$0 == 0x00f4
                           0000F4  1250 _WTEVTA0	=	0x00f4
                           0000F5  1251 G$WTEVTA1$0$0 == 0x00f5
                           0000F5  1252 _WTEVTA1	=	0x00f5
                           00F5F4  1253 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4  1254 _WTEVTA	=	0xf5f4
                           0000F6  1255 G$WTEVTB0$0$0 == 0x00f6
                           0000F6  1256 _WTEVTB0	=	0x00f6
                           0000F7  1257 G$WTEVTB1$0$0 == 0x00f7
                           0000F7  1258 _WTEVTB1	=	0x00f7
                           00F7F6  1259 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6  1260 _WTEVTB	=	0xf7f6
                           0000FC  1261 G$WTEVTC0$0$0 == 0x00fc
                           0000FC  1262 _WTEVTC0	=	0x00fc
                           0000FD  1263 G$WTEVTC1$0$0 == 0x00fd
                           0000FD  1264 _WTEVTC1	=	0x00fd
                           00FDFC  1265 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC  1266 _WTEVTC	=	0xfdfc
                           0000FE  1267 G$WTEVTD0$0$0 == 0x00fe
                           0000FE  1268 _WTEVTD0	=	0x00fe
                           0000FF  1269 G$WTEVTD1$0$0 == 0x00ff
                           0000FF  1270 _WTEVTD1	=	0x00ff
                           00FFFE  1271 G$WTEVTD$0$0 == 0xfffe
                           00FFFE  1272 _WTEVTD	=	0xfffe
                           0000E9  1273 G$WTIRQEN$0$0 == 0x00e9
                           0000E9  1274 _WTIRQEN	=	0x00e9
                           0000EA  1275 G$WTSTAT$0$0 == 0x00ea
                           0000EA  1276 _WTSTAT	=	0x00ea
                                   1277 ;--------------------------------------------------------
                                   1278 ; special function bits
                                   1279 ;--------------------------------------------------------
                                   1280 	.area RSEG    (ABS,DATA)
      000000                       1281 	.org 0x0000
                           0000E0  1282 G$ACC_0$0$0 == 0x00e0
                           0000E0  1283 _ACC_0	=	0x00e0
                           0000E1  1284 G$ACC_1$0$0 == 0x00e1
                           0000E1  1285 _ACC_1	=	0x00e1
                           0000E2  1286 G$ACC_2$0$0 == 0x00e2
                           0000E2  1287 _ACC_2	=	0x00e2
                           0000E3  1288 G$ACC_3$0$0 == 0x00e3
                           0000E3  1289 _ACC_3	=	0x00e3
                           0000E4  1290 G$ACC_4$0$0 == 0x00e4
                           0000E4  1291 _ACC_4	=	0x00e4
                           0000E5  1292 G$ACC_5$0$0 == 0x00e5
                           0000E5  1293 _ACC_5	=	0x00e5
                           0000E6  1294 G$ACC_6$0$0 == 0x00e6
                           0000E6  1295 _ACC_6	=	0x00e6
                           0000E7  1296 G$ACC_7$0$0 == 0x00e7
                           0000E7  1297 _ACC_7	=	0x00e7
                           0000F0  1298 G$B_0$0$0 == 0x00f0
                           0000F0  1299 _B_0	=	0x00f0
                           0000F1  1300 G$B_1$0$0 == 0x00f1
                           0000F1  1301 _B_1	=	0x00f1
                           0000F2  1302 G$B_2$0$0 == 0x00f2
                           0000F2  1303 _B_2	=	0x00f2
                           0000F3  1304 G$B_3$0$0 == 0x00f3
                           0000F3  1305 _B_3	=	0x00f3
                           0000F4  1306 G$B_4$0$0 == 0x00f4
                           0000F4  1307 _B_4	=	0x00f4
                           0000F5  1308 G$B_5$0$0 == 0x00f5
                           0000F5  1309 _B_5	=	0x00f5
                           0000F6  1310 G$B_6$0$0 == 0x00f6
                           0000F6  1311 _B_6	=	0x00f6
                           0000F7  1312 G$B_7$0$0 == 0x00f7
                           0000F7  1313 _B_7	=	0x00f7
                           0000A0  1314 G$E2IE_0$0$0 == 0x00a0
                           0000A0  1315 _E2IE_0	=	0x00a0
                           0000A1  1316 G$E2IE_1$0$0 == 0x00a1
                           0000A1  1317 _E2IE_1	=	0x00a1
                           0000A2  1318 G$E2IE_2$0$0 == 0x00a2
                           0000A2  1319 _E2IE_2	=	0x00a2
                           0000A3  1320 G$E2IE_3$0$0 == 0x00a3
                           0000A3  1321 _E2IE_3	=	0x00a3
                           0000A4  1322 G$E2IE_4$0$0 == 0x00a4
                           0000A4  1323 _E2IE_4	=	0x00a4
                           0000A5  1324 G$E2IE_5$0$0 == 0x00a5
                           0000A5  1325 _E2IE_5	=	0x00a5
                           0000A6  1326 G$E2IE_6$0$0 == 0x00a6
                           0000A6  1327 _E2IE_6	=	0x00a6
                           0000A7  1328 G$E2IE_7$0$0 == 0x00a7
                           0000A7  1329 _E2IE_7	=	0x00a7
                           0000C0  1330 G$E2IP_0$0$0 == 0x00c0
                           0000C0  1331 _E2IP_0	=	0x00c0
                           0000C1  1332 G$E2IP_1$0$0 == 0x00c1
                           0000C1  1333 _E2IP_1	=	0x00c1
                           0000C2  1334 G$E2IP_2$0$0 == 0x00c2
                           0000C2  1335 _E2IP_2	=	0x00c2
                           0000C3  1336 G$E2IP_3$0$0 == 0x00c3
                           0000C3  1337 _E2IP_3	=	0x00c3
                           0000C4  1338 G$E2IP_4$0$0 == 0x00c4
                           0000C4  1339 _E2IP_4	=	0x00c4
                           0000C5  1340 G$E2IP_5$0$0 == 0x00c5
                           0000C5  1341 _E2IP_5	=	0x00c5
                           0000C6  1342 G$E2IP_6$0$0 == 0x00c6
                           0000C6  1343 _E2IP_6	=	0x00c6
                           0000C7  1344 G$E2IP_7$0$0 == 0x00c7
                           0000C7  1345 _E2IP_7	=	0x00c7
                           000098  1346 G$EIE_0$0$0 == 0x0098
                           000098  1347 _EIE_0	=	0x0098
                           000099  1348 G$EIE_1$0$0 == 0x0099
                           000099  1349 _EIE_1	=	0x0099
                           00009A  1350 G$EIE_2$0$0 == 0x009a
                           00009A  1351 _EIE_2	=	0x009a
                           00009B  1352 G$EIE_3$0$0 == 0x009b
                           00009B  1353 _EIE_3	=	0x009b
                           00009C  1354 G$EIE_4$0$0 == 0x009c
                           00009C  1355 _EIE_4	=	0x009c
                           00009D  1356 G$EIE_5$0$0 == 0x009d
                           00009D  1357 _EIE_5	=	0x009d
                           00009E  1358 G$EIE_6$0$0 == 0x009e
                           00009E  1359 _EIE_6	=	0x009e
                           00009F  1360 G$EIE_7$0$0 == 0x009f
                           00009F  1361 _EIE_7	=	0x009f
                           0000B0  1362 G$EIP_0$0$0 == 0x00b0
                           0000B0  1363 _EIP_0	=	0x00b0
                           0000B1  1364 G$EIP_1$0$0 == 0x00b1
                           0000B1  1365 _EIP_1	=	0x00b1
                           0000B2  1366 G$EIP_2$0$0 == 0x00b2
                           0000B2  1367 _EIP_2	=	0x00b2
                           0000B3  1368 G$EIP_3$0$0 == 0x00b3
                           0000B3  1369 _EIP_3	=	0x00b3
                           0000B4  1370 G$EIP_4$0$0 == 0x00b4
                           0000B4  1371 _EIP_4	=	0x00b4
                           0000B5  1372 G$EIP_5$0$0 == 0x00b5
                           0000B5  1373 _EIP_5	=	0x00b5
                           0000B6  1374 G$EIP_6$0$0 == 0x00b6
                           0000B6  1375 _EIP_6	=	0x00b6
                           0000B7  1376 G$EIP_7$0$0 == 0x00b7
                           0000B7  1377 _EIP_7	=	0x00b7
                           0000A8  1378 G$IE_0$0$0 == 0x00a8
                           0000A8  1379 _IE_0	=	0x00a8
                           0000A9  1380 G$IE_1$0$0 == 0x00a9
                           0000A9  1381 _IE_1	=	0x00a9
                           0000AA  1382 G$IE_2$0$0 == 0x00aa
                           0000AA  1383 _IE_2	=	0x00aa
                           0000AB  1384 G$IE_3$0$0 == 0x00ab
                           0000AB  1385 _IE_3	=	0x00ab
                           0000AC  1386 G$IE_4$0$0 == 0x00ac
                           0000AC  1387 _IE_4	=	0x00ac
                           0000AD  1388 G$IE_5$0$0 == 0x00ad
                           0000AD  1389 _IE_5	=	0x00ad
                           0000AE  1390 G$IE_6$0$0 == 0x00ae
                           0000AE  1391 _IE_6	=	0x00ae
                           0000AF  1392 G$IE_7$0$0 == 0x00af
                           0000AF  1393 _IE_7	=	0x00af
                           0000AF  1394 G$EA$0$0 == 0x00af
                           0000AF  1395 _EA	=	0x00af
                           0000B8  1396 G$IP_0$0$0 == 0x00b8
                           0000B8  1397 _IP_0	=	0x00b8
                           0000B9  1398 G$IP_1$0$0 == 0x00b9
                           0000B9  1399 _IP_1	=	0x00b9
                           0000BA  1400 G$IP_2$0$0 == 0x00ba
                           0000BA  1401 _IP_2	=	0x00ba
                           0000BB  1402 G$IP_3$0$0 == 0x00bb
                           0000BB  1403 _IP_3	=	0x00bb
                           0000BC  1404 G$IP_4$0$0 == 0x00bc
                           0000BC  1405 _IP_4	=	0x00bc
                           0000BD  1406 G$IP_5$0$0 == 0x00bd
                           0000BD  1407 _IP_5	=	0x00bd
                           0000BE  1408 G$IP_6$0$0 == 0x00be
                           0000BE  1409 _IP_6	=	0x00be
                           0000BF  1410 G$IP_7$0$0 == 0x00bf
                           0000BF  1411 _IP_7	=	0x00bf
                           0000D0  1412 G$P$0$0 == 0x00d0
                           0000D0  1413 _P	=	0x00d0
                           0000D1  1414 G$F1$0$0 == 0x00d1
                           0000D1  1415 _F1	=	0x00d1
                           0000D2  1416 G$OV$0$0 == 0x00d2
                           0000D2  1417 _OV	=	0x00d2
                           0000D3  1418 G$RS0$0$0 == 0x00d3
                           0000D3  1419 _RS0	=	0x00d3
                           0000D4  1420 G$RS1$0$0 == 0x00d4
                           0000D4  1421 _RS1	=	0x00d4
                           0000D5  1422 G$F0$0$0 == 0x00d5
                           0000D5  1423 _F0	=	0x00d5
                           0000D6  1424 G$AC$0$0 == 0x00d6
                           0000D6  1425 _AC	=	0x00d6
                           0000D7  1426 G$CY$0$0 == 0x00d7
                           0000D7  1427 _CY	=	0x00d7
                           0000C8  1428 G$PINA_0$0$0 == 0x00c8
                           0000C8  1429 _PINA_0	=	0x00c8
                           0000C9  1430 G$PINA_1$0$0 == 0x00c9
                           0000C9  1431 _PINA_1	=	0x00c9
                           0000CA  1432 G$PINA_2$0$0 == 0x00ca
                           0000CA  1433 _PINA_2	=	0x00ca
                           0000CB  1434 G$PINA_3$0$0 == 0x00cb
                           0000CB  1435 _PINA_3	=	0x00cb
                           0000CC  1436 G$PINA_4$0$0 == 0x00cc
                           0000CC  1437 _PINA_4	=	0x00cc
                           0000CD  1438 G$PINA_5$0$0 == 0x00cd
                           0000CD  1439 _PINA_5	=	0x00cd
                           0000CE  1440 G$PINA_6$0$0 == 0x00ce
                           0000CE  1441 _PINA_6	=	0x00ce
                           0000CF  1442 G$PINA_7$0$0 == 0x00cf
                           0000CF  1443 _PINA_7	=	0x00cf
                           0000E8  1444 G$PINB_0$0$0 == 0x00e8
                           0000E8  1445 _PINB_0	=	0x00e8
                           0000E9  1446 G$PINB_1$0$0 == 0x00e9
                           0000E9  1447 _PINB_1	=	0x00e9
                           0000EA  1448 G$PINB_2$0$0 == 0x00ea
                           0000EA  1449 _PINB_2	=	0x00ea
                           0000EB  1450 G$PINB_3$0$0 == 0x00eb
                           0000EB  1451 _PINB_3	=	0x00eb
                           0000EC  1452 G$PINB_4$0$0 == 0x00ec
                           0000EC  1453 _PINB_4	=	0x00ec
                           0000ED  1454 G$PINB_5$0$0 == 0x00ed
                           0000ED  1455 _PINB_5	=	0x00ed
                           0000EE  1456 G$PINB_6$0$0 == 0x00ee
                           0000EE  1457 _PINB_6	=	0x00ee
                           0000EF  1458 G$PINB_7$0$0 == 0x00ef
                           0000EF  1459 _PINB_7	=	0x00ef
                           0000F8  1460 G$PINC_0$0$0 == 0x00f8
                           0000F8  1461 _PINC_0	=	0x00f8
                           0000F9  1462 G$PINC_1$0$0 == 0x00f9
                           0000F9  1463 _PINC_1	=	0x00f9
                           0000FA  1464 G$PINC_2$0$0 == 0x00fa
                           0000FA  1465 _PINC_2	=	0x00fa
                           0000FB  1466 G$PINC_3$0$0 == 0x00fb
                           0000FB  1467 _PINC_3	=	0x00fb
                           0000FC  1468 G$PINC_4$0$0 == 0x00fc
                           0000FC  1469 _PINC_4	=	0x00fc
                           0000FD  1470 G$PINC_5$0$0 == 0x00fd
                           0000FD  1471 _PINC_5	=	0x00fd
                           0000FE  1472 G$PINC_6$0$0 == 0x00fe
                           0000FE  1473 _PINC_6	=	0x00fe
                           0000FF  1474 G$PINC_7$0$0 == 0x00ff
                           0000FF  1475 _PINC_7	=	0x00ff
                           000080  1476 G$PORTA_0$0$0 == 0x0080
                           000080  1477 _PORTA_0	=	0x0080
                           000081  1478 G$PORTA_1$0$0 == 0x0081
                           000081  1479 _PORTA_1	=	0x0081
                           000082  1480 G$PORTA_2$0$0 == 0x0082
                           000082  1481 _PORTA_2	=	0x0082
                           000083  1482 G$PORTA_3$0$0 == 0x0083
                           000083  1483 _PORTA_3	=	0x0083
                           000084  1484 G$PORTA_4$0$0 == 0x0084
                           000084  1485 _PORTA_4	=	0x0084
                           000085  1486 G$PORTA_5$0$0 == 0x0085
                           000085  1487 _PORTA_5	=	0x0085
                           000086  1488 G$PORTA_6$0$0 == 0x0086
                           000086  1489 _PORTA_6	=	0x0086
                           000087  1490 G$PORTA_7$0$0 == 0x0087
                           000087  1491 _PORTA_7	=	0x0087
                           000088  1492 G$PORTB_0$0$0 == 0x0088
                           000088  1493 _PORTB_0	=	0x0088
                           000089  1494 G$PORTB_1$0$0 == 0x0089
                           000089  1495 _PORTB_1	=	0x0089
                           00008A  1496 G$PORTB_2$0$0 == 0x008a
                           00008A  1497 _PORTB_2	=	0x008a
                           00008B  1498 G$PORTB_3$0$0 == 0x008b
                           00008B  1499 _PORTB_3	=	0x008b
                           00008C  1500 G$PORTB_4$0$0 == 0x008c
                           00008C  1501 _PORTB_4	=	0x008c
                           00008D  1502 G$PORTB_5$0$0 == 0x008d
                           00008D  1503 _PORTB_5	=	0x008d
                           00008E  1504 G$PORTB_6$0$0 == 0x008e
                           00008E  1505 _PORTB_6	=	0x008e
                           00008F  1506 G$PORTB_7$0$0 == 0x008f
                           00008F  1507 _PORTB_7	=	0x008f
                           000090  1508 G$PORTC_0$0$0 == 0x0090
                           000090  1509 _PORTC_0	=	0x0090
                           000091  1510 G$PORTC_1$0$0 == 0x0091
                           000091  1511 _PORTC_1	=	0x0091
                           000092  1512 G$PORTC_2$0$0 == 0x0092
                           000092  1513 _PORTC_2	=	0x0092
                           000093  1514 G$PORTC_3$0$0 == 0x0093
                           000093  1515 _PORTC_3	=	0x0093
                           000094  1516 G$PORTC_4$0$0 == 0x0094
                           000094  1517 _PORTC_4	=	0x0094
                           000095  1518 G$PORTC_5$0$0 == 0x0095
                           000095  1519 _PORTC_5	=	0x0095
                           000096  1520 G$PORTC_6$0$0 == 0x0096
                           000096  1521 _PORTC_6	=	0x0096
                           000097  1522 G$PORTC_7$0$0 == 0x0097
                           000097  1523 _PORTC_7	=	0x0097
                                   1524 ;--------------------------------------------------------
                                   1525 ; overlayable register banks
                                   1526 ;--------------------------------------------------------
                                   1527 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1528 	.ds 8
                                   1529 ;--------------------------------------------------------
                                   1530 ; internal ram data
                                   1531 ;--------------------------------------------------------
                                   1532 	.area DSEG    (DATA)
                           000000  1533 Lpktframing.packetStdFraming$sloc0$1$0==.
      000022                       1534 _packetStdFraming_sloc0_1_0:
      000022                       1535 	.ds 4
                           000004  1536 Lpktframing.packetStdFraming$sloc1$1$0==.
      000026                       1537 _packetStdFraming_sloc1_1_0:
      000026                       1538 	.ds 1
                           000005  1539 Lpktframing.packetStdFraming$sloc2$1$0==.
      000027                       1540 _packetStdFraming_sloc2_1_0:
      000027                       1541 	.ds 1
                           000006  1542 Lpktframing.packetStdFraming$sloc3$1$0==.
      000028                       1543 _packetStdFraming_sloc3_1_0:
      000028                       1544 	.ds 1
                           000007  1545 Lpktframing.packetStdFraming$sloc4$1$0==.
      000029                       1546 _packetStdFraming_sloc4_1_0:
      000029                       1547 	.ds 1
                           000008  1548 Lpktframing.packetStdFraming$sloc5$1$0==.
      00002A                       1549 _packetStdFraming_sloc5_1_0:
      00002A                       1550 	.ds 2
                           00000A  1551 Lpktframing.packetStdFraming$sloc6$1$0==.
      00002C                       1552 _packetStdFraming_sloc6_1_0:
      00002C                       1553 	.ds 2
                           00000C  1554 Lpktframing.packetStdFraming$sloc7$1$0==.
      00002E                       1555 _packetStdFraming_sloc7_1_0:
      00002E                       1556 	.ds 2
                           00000E  1557 Lpktframing.packetStdFraming$sloc8$1$0==.
      000030                       1558 _packetStdFraming_sloc8_1_0:
      000030                       1559 	.ds 4
                                   1560 ;--------------------------------------------------------
                                   1561 ; overlayable items in internal ram 
                                   1562 ;--------------------------------------------------------
                                   1563 	.area	OSEG    (OVR,DATA)
                           000000  1564 Lpktframing.swap_variables$b$1$370==.
      00006E                       1565 _swap_variables_PARM_2:
      00006E                       1566 	.ds 3
                                   1567 ;--------------------------------------------------------
                                   1568 ; indirectly addressable internal ram data
                                   1569 ;--------------------------------------------------------
                                   1570 	.area ISEG    (DATA)
                                   1571 ;--------------------------------------------------------
                                   1572 ; absolute internal ram data
                                   1573 ;--------------------------------------------------------
                                   1574 	.area IABS    (ABS,DATA)
                                   1575 	.area IABS    (ABS,DATA)
                                   1576 ;--------------------------------------------------------
                                   1577 ; bit data
                                   1578 ;--------------------------------------------------------
                                   1579 	.area BSEG    (BIT)
                                   1580 ;--------------------------------------------------------
                                   1581 ; paged external ram data
                                   1582 ;--------------------------------------------------------
                                   1583 	.area PSEG    (PAG,XDATA)
                                   1584 ;--------------------------------------------------------
                                   1585 ; external ram data
                                   1586 ;--------------------------------------------------------
                                   1587 	.area XSEG    (XDATA)
                           007020  1588 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1589 _ADCCH0VAL0	=	0x7020
                           007021  1590 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1591 _ADCCH0VAL1	=	0x7021
                           007020  1592 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1593 _ADCCH0VAL	=	0x7020
                           007022  1594 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1595 _ADCCH1VAL0	=	0x7022
                           007023  1596 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1597 _ADCCH1VAL1	=	0x7023
                           007022  1598 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1599 _ADCCH1VAL	=	0x7022
                           007024  1600 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1601 _ADCCH2VAL0	=	0x7024
                           007025  1602 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1603 _ADCCH2VAL1	=	0x7025
                           007024  1604 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1605 _ADCCH2VAL	=	0x7024
                           007026  1606 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1607 _ADCCH3VAL0	=	0x7026
                           007027  1608 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1609 _ADCCH3VAL1	=	0x7027
                           007026  1610 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1611 _ADCCH3VAL	=	0x7026
                           007028  1612 G$ADCTUNE0$0$0 == 0x7028
                           007028  1613 _ADCTUNE0	=	0x7028
                           007029  1614 G$ADCTUNE1$0$0 == 0x7029
                           007029  1615 _ADCTUNE1	=	0x7029
                           00702A  1616 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1617 _ADCTUNE2	=	0x702a
                           007010  1618 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1619 _DMA0ADDR0	=	0x7010
                           007011  1620 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1621 _DMA0ADDR1	=	0x7011
                           007010  1622 G$DMA0ADDR$0$0 == 0x7010
                           007010  1623 _DMA0ADDR	=	0x7010
                           007014  1624 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1625 _DMA0CONFIG	=	0x7014
                           007012  1626 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1627 _DMA1ADDR0	=	0x7012
                           007013  1628 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1629 _DMA1ADDR1	=	0x7013
                           007012  1630 G$DMA1ADDR$0$0 == 0x7012
                           007012  1631 _DMA1ADDR	=	0x7012
                           007015  1632 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1633 _DMA1CONFIG	=	0x7015
                           007070  1634 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1635 _FRCOSCCONFIG	=	0x7070
                           007071  1636 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1637 _FRCOSCCTRL	=	0x7071
                           007076  1638 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1639 _FRCOSCFREQ0	=	0x7076
                           007077  1640 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1641 _FRCOSCFREQ1	=	0x7077
                           007076  1642 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1643 _FRCOSCFREQ	=	0x7076
                           007072  1644 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1645 _FRCOSCKFILT0	=	0x7072
                           007073  1646 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1647 _FRCOSCKFILT1	=	0x7073
                           007072  1648 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1649 _FRCOSCKFILT	=	0x7072
                           007078  1650 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1651 _FRCOSCPER0	=	0x7078
                           007079  1652 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1653 _FRCOSCPER1	=	0x7079
                           007078  1654 G$FRCOSCPER$0$0 == 0x7078
                           007078  1655 _FRCOSCPER	=	0x7078
                           007074  1656 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1657 _FRCOSCREF0	=	0x7074
                           007075  1658 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1659 _FRCOSCREF1	=	0x7075
                           007074  1660 G$FRCOSCREF$0$0 == 0x7074
                           007074  1661 _FRCOSCREF	=	0x7074
                           007007  1662 G$ANALOGA$0$0 == 0x7007
                           007007  1663 _ANALOGA	=	0x7007
                           00700C  1664 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1665 _GPIOENABLE	=	0x700c
                           007003  1666 G$EXTIRQ$0$0 == 0x7003
                           007003  1667 _EXTIRQ	=	0x7003
                           007000  1668 G$INTCHGA$0$0 == 0x7000
                           007000  1669 _INTCHGA	=	0x7000
                           007001  1670 G$INTCHGB$0$0 == 0x7001
                           007001  1671 _INTCHGB	=	0x7001
                           007002  1672 G$INTCHGC$0$0 == 0x7002
                           007002  1673 _INTCHGC	=	0x7002
                           007008  1674 G$PALTA$0$0 == 0x7008
                           007008  1675 _PALTA	=	0x7008
                           007009  1676 G$PALTB$0$0 == 0x7009
                           007009  1677 _PALTB	=	0x7009
                           00700A  1678 G$PALTC$0$0 == 0x700a
                           00700A  1679 _PALTC	=	0x700a
                           007046  1680 G$PALTRADIO$0$0 == 0x7046
                           007046  1681 _PALTRADIO	=	0x7046
                           007004  1682 G$PINCHGA$0$0 == 0x7004
                           007004  1683 _PINCHGA	=	0x7004
                           007005  1684 G$PINCHGB$0$0 == 0x7005
                           007005  1685 _PINCHGB	=	0x7005
                           007006  1686 G$PINCHGC$0$0 == 0x7006
                           007006  1687 _PINCHGC	=	0x7006
                           00700B  1688 G$PINSEL$0$0 == 0x700b
                           00700B  1689 _PINSEL	=	0x700b
                           007060  1690 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1691 _LPOSCCONFIG	=	0x7060
                           007066  1692 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1693 _LPOSCFREQ0	=	0x7066
                           007067  1694 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1695 _LPOSCFREQ1	=	0x7067
                           007066  1696 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1697 _LPOSCFREQ	=	0x7066
                           007062  1698 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1699 _LPOSCKFILT0	=	0x7062
                           007063  1700 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1701 _LPOSCKFILT1	=	0x7063
                           007062  1702 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1703 _LPOSCKFILT	=	0x7062
                           007068  1704 G$LPOSCPER0$0$0 == 0x7068
                           007068  1705 _LPOSCPER0	=	0x7068
                           007069  1706 G$LPOSCPER1$0$0 == 0x7069
                           007069  1707 _LPOSCPER1	=	0x7069
                           007068  1708 G$LPOSCPER$0$0 == 0x7068
                           007068  1709 _LPOSCPER	=	0x7068
                           007064  1710 G$LPOSCREF0$0$0 == 0x7064
                           007064  1711 _LPOSCREF0	=	0x7064
                           007065  1712 G$LPOSCREF1$0$0 == 0x7065
                           007065  1713 _LPOSCREF1	=	0x7065
                           007064  1714 G$LPOSCREF$0$0 == 0x7064
                           007064  1715 _LPOSCREF	=	0x7064
                           007054  1716 G$LPXOSCGM$0$0 == 0x7054
                           007054  1717 _LPXOSCGM	=	0x7054
                           007F01  1718 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1719 _MISCCTRL	=	0x7f01
                           007053  1720 G$OSCCALIB$0$0 == 0x7053
                           007053  1721 _OSCCALIB	=	0x7053
                           007050  1722 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1723 _OSCFORCERUN	=	0x7050
                           007052  1724 G$OSCREADY$0$0 == 0x7052
                           007052  1725 _OSCREADY	=	0x7052
                           007051  1726 G$OSCRUN$0$0 == 0x7051
                           007051  1727 _OSCRUN	=	0x7051
                           007040  1728 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1729 _RADIOFDATAADDR0	=	0x7040
                           007041  1730 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1731 _RADIOFDATAADDR1	=	0x7041
                           007040  1732 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1733 _RADIOFDATAADDR	=	0x7040
                           007042  1734 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1735 _RADIOFSTATADDR0	=	0x7042
                           007043  1736 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1737 _RADIOFSTATADDR1	=	0x7043
                           007042  1738 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1739 _RADIOFSTATADDR	=	0x7042
                           007044  1740 G$RADIOMUX$0$0 == 0x7044
                           007044  1741 _RADIOMUX	=	0x7044
                           007084  1742 G$SCRATCH0$0$0 == 0x7084
                           007084  1743 _SCRATCH0	=	0x7084
                           007085  1744 G$SCRATCH1$0$0 == 0x7085
                           007085  1745 _SCRATCH1	=	0x7085
                           007086  1746 G$SCRATCH2$0$0 == 0x7086
                           007086  1747 _SCRATCH2	=	0x7086
                           007087  1748 G$SCRATCH3$0$0 == 0x7087
                           007087  1749 _SCRATCH3	=	0x7087
                           007F00  1750 G$SILICONREV$0$0 == 0x7f00
                           007F00  1751 _SILICONREV	=	0x7f00
                           007F19  1752 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1753 _XTALAMPL	=	0x7f19
                           007F18  1754 G$XTALOSC$0$0 == 0x7f18
                           007F18  1755 _XTALOSC	=	0x7f18
                           007F1A  1756 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1757 _XTALREADY	=	0x7f1a
                           00FC06  1758 Fpktframing$flash_deviceid$0$0 == 0xfc06
                           00FC06  1759 _flash_deviceid	=	0xfc06
                           00FC00  1760 Fpktframing$flash_calsector$0$0 == 0xfc00
                           00FC00  1761 _flash_calsector	=	0xfc00
                           004114  1762 G$AX5043_AFSKCTRL$0$0 == 0x4114
                           004114  1763 _AX5043_AFSKCTRL	=	0x4114
                           004113  1764 G$AX5043_AFSKMARK0$0$0 == 0x4113
                           004113  1765 _AX5043_AFSKMARK0	=	0x4113
                           004112  1766 G$AX5043_AFSKMARK1$0$0 == 0x4112
                           004112  1767 _AX5043_AFSKMARK1	=	0x4112
                           004111  1768 G$AX5043_AFSKSPACE0$0$0 == 0x4111
                           004111  1769 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1770 G$AX5043_AFSKSPACE1$0$0 == 0x4110
                           004110  1771 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1772 G$AX5043_AGCCOUNTER$0$0 == 0x4043
                           004043  1773 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1774 G$AX5043_AMPLFILTER$0$0 == 0x4115
                           004115  1775 _AX5043_AMPLFILTER	=	0x4115
                           004189  1776 G$AX5043_BBOFFSCAP$0$0 == 0x4189
                           004189  1777 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1778 G$AX5043_BBTUNE$0$0 == 0x4188
                           004188  1779 _AX5043_BBTUNE	=	0x4188
                           004041  1780 G$AX5043_BGNDRSSI$0$0 == 0x4041
                           004041  1781 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1782 G$AX5043_BGNDRSSIGAIN$0$0 == 0x422e
                           00422E  1783 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1784 G$AX5043_BGNDRSSITHR$0$0 == 0x422f
                           00422F  1785 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1786 G$AX5043_CRCINIT0$0$0 == 0x4017
                           004017  1787 _AX5043_CRCINIT0	=	0x4017
                           004016  1788 G$AX5043_CRCINIT1$0$0 == 0x4016
                           004016  1789 _AX5043_CRCINIT1	=	0x4016
                           004015  1790 G$AX5043_CRCINIT2$0$0 == 0x4015
                           004015  1791 _AX5043_CRCINIT2	=	0x4015
                           004014  1792 G$AX5043_CRCINIT3$0$0 == 0x4014
                           004014  1793 _AX5043_CRCINIT3	=	0x4014
                           004332  1794 G$AX5043_DACCONFIG$0$0 == 0x4332
                           004332  1795 _AX5043_DACCONFIG	=	0x4332
                           004331  1796 G$AX5043_DACVALUE0$0$0 == 0x4331
                           004331  1797 _AX5043_DACVALUE0	=	0x4331
                           004330  1798 G$AX5043_DACVALUE1$0$0 == 0x4330
                           004330  1799 _AX5043_DACVALUE1	=	0x4330
                           004102  1800 G$AX5043_DECIMATION$0$0 == 0x4102
                           004102  1801 _AX5043_DECIMATION	=	0x4102
                           004042  1802 G$AX5043_DIVERSITY$0$0 == 0x4042
                           004042  1803 _AX5043_DIVERSITY	=	0x4042
                           004011  1804 G$AX5043_ENCODING$0$0 == 0x4011
                           004011  1805 _AX5043_ENCODING	=	0x4011
                           004018  1806 G$AX5043_FEC$0$0 == 0x4018
                           004018  1807 _AX5043_FEC	=	0x4018
                           00401A  1808 G$AX5043_FECSTATUS$0$0 == 0x401a
                           00401A  1809 _AX5043_FECSTATUS	=	0x401a
                           004019  1810 G$AX5043_FECSYNC$0$0 == 0x4019
                           004019  1811 _AX5043_FECSYNC	=	0x4019
                           00402B  1812 G$AX5043_FIFOCOUNT0$0$0 == 0x402b
                           00402B  1813 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1814 G$AX5043_FIFOCOUNT1$0$0 == 0x402a
                           00402A  1815 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1816 G$AX5043_FIFODATA$0$0 == 0x4029
                           004029  1817 _AX5043_FIFODATA	=	0x4029
                           00402D  1818 G$AX5043_FIFOFREE0$0$0 == 0x402d
                           00402D  1819 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1820 G$AX5043_FIFOFREE1$0$0 == 0x402c
                           00402C  1821 _AX5043_FIFOFREE1	=	0x402c
                           004028  1822 G$AX5043_FIFOSTAT$0$0 == 0x4028
                           004028  1823 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1824 G$AX5043_FIFOTHRESH0$0$0 == 0x402f
                           00402F  1825 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1826 G$AX5043_FIFOTHRESH1$0$0 == 0x402e
                           00402E  1827 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1828 G$AX5043_FRAMING$0$0 == 0x4012
                           004012  1829 _AX5043_FRAMING	=	0x4012
                           004037  1830 G$AX5043_FREQA0$0$0 == 0x4037
                           004037  1831 _AX5043_FREQA0	=	0x4037
                           004036  1832 G$AX5043_FREQA1$0$0 == 0x4036
                           004036  1833 _AX5043_FREQA1	=	0x4036
                           004035  1834 G$AX5043_FREQA2$0$0 == 0x4035
                           004035  1835 _AX5043_FREQA2	=	0x4035
                           004034  1836 G$AX5043_FREQA3$0$0 == 0x4034
                           004034  1837 _AX5043_FREQA3	=	0x4034
                           00403F  1838 G$AX5043_FREQB0$0$0 == 0x403f
                           00403F  1839 _AX5043_FREQB0	=	0x403f
                           00403E  1840 G$AX5043_FREQB1$0$0 == 0x403e
                           00403E  1841 _AX5043_FREQB1	=	0x403e
                           00403D  1842 G$AX5043_FREQB2$0$0 == 0x403d
                           00403D  1843 _AX5043_FREQB2	=	0x403d
                           00403C  1844 G$AX5043_FREQB3$0$0 == 0x403c
                           00403C  1845 _AX5043_FREQB3	=	0x403c
                           004163  1846 G$AX5043_FSKDEV0$0$0 == 0x4163
                           004163  1847 _AX5043_FSKDEV0	=	0x4163
                           004162  1848 G$AX5043_FSKDEV1$0$0 == 0x4162
                           004162  1849 _AX5043_FSKDEV1	=	0x4162
                           004161  1850 G$AX5043_FSKDEV2$0$0 == 0x4161
                           004161  1851 _AX5043_FSKDEV2	=	0x4161
                           00410D  1852 G$AX5043_FSKDMAX0$0$0 == 0x410d
                           00410D  1853 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1854 G$AX5043_FSKDMAX1$0$0 == 0x410c
                           00410C  1855 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1856 G$AX5043_FSKDMIN0$0$0 == 0x410f
                           00410F  1857 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1858 G$AX5043_FSKDMIN1$0$0 == 0x410e
                           00410E  1859 _AX5043_FSKDMIN1	=	0x410e
                           004309  1860 G$AX5043_GPADC13VALUE0$0$0 == 0x4309
                           004309  1861 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1862 G$AX5043_GPADC13VALUE1$0$0 == 0x4308
                           004308  1863 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1864 G$AX5043_GPADCCTRL$0$0 == 0x4300
                           004300  1865 _AX5043_GPADCCTRL	=	0x4300
                           004301  1866 G$AX5043_GPADCPERIOD$0$0 == 0x4301
                           004301  1867 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1868 G$AX5043_IFFREQ0$0$0 == 0x4101
                           004101  1869 _AX5043_IFFREQ0	=	0x4101
                           004100  1870 G$AX5043_IFFREQ1$0$0 == 0x4100
                           004100  1871 _AX5043_IFFREQ1	=	0x4100
                           00400B  1872 G$AX5043_IRQINVERSION0$0$0 == 0x400b
                           00400B  1873 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1874 G$AX5043_IRQINVERSION1$0$0 == 0x400a
                           00400A  1875 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1876 G$AX5043_IRQMASK0$0$0 == 0x4007
                           004007  1877 _AX5043_IRQMASK0	=	0x4007
                           004006  1878 G$AX5043_IRQMASK1$0$0 == 0x4006
                           004006  1879 _AX5043_IRQMASK1	=	0x4006
                           00400D  1880 G$AX5043_IRQREQUEST0$0$0 == 0x400d
                           00400D  1881 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1882 G$AX5043_IRQREQUEST1$0$0 == 0x400c
                           00400C  1883 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1884 G$AX5043_LPOSCCONFIG$0$0 == 0x4310
                           004310  1885 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1886 G$AX5043_LPOSCFREQ0$0$0 == 0x4317
                           004317  1887 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1888 G$AX5043_LPOSCFREQ1$0$0 == 0x4316
                           004316  1889 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1890 G$AX5043_LPOSCKFILT0$0$0 == 0x4313
                           004313  1891 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1892 G$AX5043_LPOSCKFILT1$0$0 == 0x4312
                           004312  1893 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1894 G$AX5043_LPOSCPER0$0$0 == 0x4319
                           004319  1895 _AX5043_LPOSCPER0	=	0x4319
                           004318  1896 G$AX5043_LPOSCPER1$0$0 == 0x4318
                           004318  1897 _AX5043_LPOSCPER1	=	0x4318
                           004315  1898 G$AX5043_LPOSCREF0$0$0 == 0x4315
                           004315  1899 _AX5043_LPOSCREF0	=	0x4315
                           004314  1900 G$AX5043_LPOSCREF1$0$0 == 0x4314
                           004314  1901 _AX5043_LPOSCREF1	=	0x4314
                           004311  1902 G$AX5043_LPOSCSTATUS$0$0 == 0x4311
                           004311  1903 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1904 G$AX5043_MATCH0LEN$0$0 == 0x4214
                           004214  1905 _AX5043_MATCH0LEN	=	0x4214
                           004216  1906 G$AX5043_MATCH0MAX$0$0 == 0x4216
                           004216  1907 _AX5043_MATCH0MAX	=	0x4216
                           004215  1908 G$AX5043_MATCH0MIN$0$0 == 0x4215
                           004215  1909 _AX5043_MATCH0MIN	=	0x4215
                           004213  1910 G$AX5043_MATCH0PAT0$0$0 == 0x4213
                           004213  1911 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1912 G$AX5043_MATCH0PAT1$0$0 == 0x4212
                           004212  1913 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1914 G$AX5043_MATCH0PAT2$0$0 == 0x4211
                           004211  1915 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1916 G$AX5043_MATCH0PAT3$0$0 == 0x4210
                           004210  1917 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1918 G$AX5043_MATCH1LEN$0$0 == 0x421c
                           00421C  1919 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1920 G$AX5043_MATCH1MAX$0$0 == 0x421e
                           00421E  1921 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1922 G$AX5043_MATCH1MIN$0$0 == 0x421d
                           00421D  1923 _AX5043_MATCH1MIN	=	0x421d
                           004219  1924 G$AX5043_MATCH1PAT0$0$0 == 0x4219
                           004219  1925 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1926 G$AX5043_MATCH1PAT1$0$0 == 0x4218
                           004218  1927 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1928 G$AX5043_MAXDROFFSET0$0$0 == 0x4108
                           004108  1929 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1930 G$AX5043_MAXDROFFSET1$0$0 == 0x4107
                           004107  1931 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1932 G$AX5043_MAXDROFFSET2$0$0 == 0x4106
                           004106  1933 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1934 G$AX5043_MAXRFOFFSET0$0$0 == 0x410b
                           00410B  1935 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1936 G$AX5043_MAXRFOFFSET1$0$0 == 0x410a
                           00410A  1937 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1938 G$AX5043_MAXRFOFFSET2$0$0 == 0x4109
                           004109  1939 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1940 G$AX5043_MODCFGA$0$0 == 0x4164
                           004164  1941 _AX5043_MODCFGA	=	0x4164
                           004160  1942 G$AX5043_MODCFGF$0$0 == 0x4160
                           004160  1943 _AX5043_MODCFGF	=	0x4160
                           004F5F  1944 G$AX5043_MODCFGP$0$0 == 0x4f5f
                           004F5F  1945 _AX5043_MODCFGP	=	0x4f5f
                           004010  1946 G$AX5043_MODULATION$0$0 == 0x4010
                           004010  1947 _AX5043_MODULATION	=	0x4010
                           004025  1948 G$AX5043_PINFUNCANTSEL$0$0 == 0x4025
                           004025  1949 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1950 G$AX5043_PINFUNCDATA$0$0 == 0x4023
                           004023  1951 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1952 G$AX5043_PINFUNCDCLK$0$0 == 0x4022
                           004022  1953 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1954 G$AX5043_PINFUNCIRQ$0$0 == 0x4024
                           004024  1955 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1956 G$AX5043_PINFUNCPWRAMP$0$0 == 0x4026
                           004026  1957 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1958 G$AX5043_PINFUNCSYSCLK$0$0 == 0x4021
                           004021  1959 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1960 G$AX5043_PINSTATE$0$0 == 0x4020
                           004020  1961 _AX5043_PINSTATE	=	0x4020
                           004233  1962 G$AX5043_PKTACCEPTFLAGS$0$0 == 0x4233
                           004233  1963 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1964 G$AX5043_PKTCHUNKSIZE$0$0 == 0x4230
                           004230  1965 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1966 G$AX5043_PKTMISCFLAGS$0$0 == 0x4231
                           004231  1967 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1968 G$AX5043_PKTSTOREFLAGS$0$0 == 0x4232
                           004232  1969 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1970 G$AX5043_PLLCPI$0$0 == 0x4031
                           004031  1971 _AX5043_PLLCPI	=	0x4031
                           004039  1972 G$AX5043_PLLCPIBOOST$0$0 == 0x4039
                           004039  1973 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1974 G$AX5043_PLLLOCKDET$0$0 == 0x4182
                           004182  1975 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1976 G$AX5043_PLLLOOP$0$0 == 0x4030
                           004030  1977 _AX5043_PLLLOOP	=	0x4030
                           004038  1978 G$AX5043_PLLLOOPBOOST$0$0 == 0x4038
                           004038  1979 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1980 G$AX5043_PLLRANGINGA$0$0 == 0x4033
                           004033  1981 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1982 G$AX5043_PLLRANGINGB$0$0 == 0x403b
                           00403B  1983 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1984 G$AX5043_PLLRNGCLK$0$0 == 0x4183
                           004183  1985 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1986 G$AX5043_PLLVCODIV$0$0 == 0x4032
                           004032  1987 _AX5043_PLLVCODIV	=	0x4032
                           004180  1988 G$AX5043_PLLVCOI$0$0 == 0x4180
                           004180  1989 _AX5043_PLLVCOI	=	0x4180
                           004181  1990 G$AX5043_PLLVCOIR$0$0 == 0x4181
                           004181  1991 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1992 G$AX5043_POWCTRL1$0$0 == 0x4f08
                           004F08  1993 _AX5043_POWCTRL1	=	0x4f08
                           004005  1994 G$AX5043_POWIRQMASK$0$0 == 0x4005
                           004005  1995 _AX5043_POWIRQMASK	=	0x4005
                           004003  1996 G$AX5043_POWSTAT$0$0 == 0x4003
                           004003  1997 _AX5043_POWSTAT	=	0x4003
                           004004  1998 G$AX5043_POWSTICKYSTAT$0$0 == 0x4004
                           004004  1999 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  2000 G$AX5043_PWRAMP$0$0 == 0x4027
                           004027  2001 _AX5043_PWRAMP	=	0x4027
                           004002  2002 G$AX5043_PWRMODE$0$0 == 0x4002
                           004002  2003 _AX5043_PWRMODE	=	0x4002
                           004009  2004 G$AX5043_RADIOEVENTMASK0$0$0 == 0x4009
                           004009  2005 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  2006 G$AX5043_RADIOEVENTMASK1$0$0 == 0x4008
                           004008  2007 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  2008 G$AX5043_RADIOEVENTREQ0$0$0 == 0x400f
                           00400F  2009 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  2010 G$AX5043_RADIOEVENTREQ1$0$0 == 0x400e
                           00400E  2011 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  2012 G$AX5043_RADIOSTATE$0$0 == 0x401c
                           00401C  2013 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  2014 G$AX5043_REF$0$0 == 0x4f0d
                           004F0D  2015 _AX5043_REF	=	0x4f0d
                           004040  2016 G$AX5043_RSSI$0$0 == 0x4040
                           004040  2017 _AX5043_RSSI	=	0x4040
                           00422D  2018 G$AX5043_RSSIABSTHR$0$0 == 0x422d
                           00422D  2019 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  2020 G$AX5043_RSSIREFERENCE$0$0 == 0x422c
                           00422C  2021 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  2022 G$AX5043_RXDATARATE0$0$0 == 0x4105
                           004105  2023 _AX5043_RXDATARATE0	=	0x4105
                           004104  2024 G$AX5043_RXDATARATE1$0$0 == 0x4104
                           004104  2025 _AX5043_RXDATARATE1	=	0x4104
                           004103  2026 G$AX5043_RXDATARATE2$0$0 == 0x4103
                           004103  2027 _AX5043_RXDATARATE2	=	0x4103
                           004001  2028 G$AX5043_SCRATCH$0$0 == 0x4001
                           004001  2029 _AX5043_SCRATCH	=	0x4001
                           004000  2030 G$AX5043_SILICONREVISION$0$0 == 0x4000
                           004000  2031 _AX5043_SILICONREVISION	=	0x4000
                           00405B  2032 G$AX5043_TIMER0$0$0 == 0x405b
                           00405B  2033 _AX5043_TIMER0	=	0x405b
                           00405A  2034 G$AX5043_TIMER1$0$0 == 0x405a
                           00405A  2035 _AX5043_TIMER1	=	0x405a
                           004059  2036 G$AX5043_TIMER2$0$0 == 0x4059
                           004059  2037 _AX5043_TIMER2	=	0x4059
                           004227  2038 G$AX5043_TMGRXAGC$0$0 == 0x4227
                           004227  2039 _AX5043_TMGRXAGC	=	0x4227
                           004223  2040 G$AX5043_TMGRXBOOST$0$0 == 0x4223
                           004223  2041 _AX5043_TMGRXBOOST	=	0x4223
                           004226  2042 G$AX5043_TMGRXCOARSEAGC$0$0 == 0x4226
                           004226  2043 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  2044 G$AX5043_TMGRXOFFSACQ$0$0 == 0x4225
                           004225  2045 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  2046 G$AX5043_TMGRXPREAMBLE1$0$0 == 0x4229
                           004229  2047 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  2048 G$AX5043_TMGRXPREAMBLE2$0$0 == 0x422a
                           00422A  2049 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  2050 G$AX5043_TMGRXPREAMBLE3$0$0 == 0x422b
                           00422B  2051 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  2052 G$AX5043_TMGRXRSSI$0$0 == 0x4228
                           004228  2053 _AX5043_TMGRXRSSI	=	0x4228
                           004224  2054 G$AX5043_TMGRXSETTLE$0$0 == 0x4224
                           004224  2055 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  2056 G$AX5043_TMGTXBOOST$0$0 == 0x4220
                           004220  2057 _AX5043_TMGTXBOOST	=	0x4220
                           004221  2058 G$AX5043_TMGTXSETTLE$0$0 == 0x4221
                           004221  2059 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  2060 G$AX5043_TRKAFSKDEMOD0$0$0 == 0x4055
                           004055  2061 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  2062 G$AX5043_TRKAFSKDEMOD1$0$0 == 0x4054
                           004054  2063 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  2064 G$AX5043_TRKAMPLITUDE0$0$0 == 0x4049
                           004049  2065 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  2066 G$AX5043_TRKAMPLITUDE1$0$0 == 0x4048
                           004048  2067 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  2068 G$AX5043_TRKDATARATE0$0$0 == 0x4047
                           004047  2069 _AX5043_TRKDATARATE0	=	0x4047
                           004046  2070 G$AX5043_TRKDATARATE1$0$0 == 0x4046
                           004046  2071 _AX5043_TRKDATARATE1	=	0x4046
                           004045  2072 G$AX5043_TRKDATARATE2$0$0 == 0x4045
                           004045  2073 _AX5043_TRKDATARATE2	=	0x4045
                           004051  2074 G$AX5043_TRKFREQ0$0$0 == 0x4051
                           004051  2075 _AX5043_TRKFREQ0	=	0x4051
                           004050  2076 G$AX5043_TRKFREQ1$0$0 == 0x4050
                           004050  2077 _AX5043_TRKFREQ1	=	0x4050
                           004053  2078 G$AX5043_TRKFSKDEMOD0$0$0 == 0x4053
                           004053  2079 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  2080 G$AX5043_TRKFSKDEMOD1$0$0 == 0x4052
                           004052  2081 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  2082 G$AX5043_TRKPHASE0$0$0 == 0x404b
                           00404B  2083 _AX5043_TRKPHASE0	=	0x404b
                           00404A  2084 G$AX5043_TRKPHASE1$0$0 == 0x404a
                           00404A  2085 _AX5043_TRKPHASE1	=	0x404a
                           00404F  2086 G$AX5043_TRKRFFREQ0$0$0 == 0x404f
                           00404F  2087 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  2088 G$AX5043_TRKRFFREQ1$0$0 == 0x404e
                           00404E  2089 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  2090 G$AX5043_TRKRFFREQ2$0$0 == 0x404d
                           00404D  2091 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  2092 G$AX5043_TXPWRCOEFFA0$0$0 == 0x4169
                           004169  2093 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  2094 G$AX5043_TXPWRCOEFFA1$0$0 == 0x4168
                           004168  2095 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  2096 G$AX5043_TXPWRCOEFFB0$0$0 == 0x416b
                           00416B  2097 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  2098 G$AX5043_TXPWRCOEFFB1$0$0 == 0x416a
                           00416A  2099 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  2100 G$AX5043_TXPWRCOEFFC0$0$0 == 0x416d
                           00416D  2101 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  2102 G$AX5043_TXPWRCOEFFC1$0$0 == 0x416c
                           00416C  2103 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  2104 G$AX5043_TXPWRCOEFFD0$0$0 == 0x416f
                           00416F  2105 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  2106 G$AX5043_TXPWRCOEFFD1$0$0 == 0x416e
                           00416E  2107 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  2108 G$AX5043_TXPWRCOEFFE0$0$0 == 0x4171
                           004171  2109 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  2110 G$AX5043_TXPWRCOEFFE1$0$0 == 0x4170
                           004170  2111 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  2112 G$AX5043_TXRATE0$0$0 == 0x4167
                           004167  2113 _AX5043_TXRATE0	=	0x4167
                           004166  2114 G$AX5043_TXRATE1$0$0 == 0x4166
                           004166  2115 _AX5043_TXRATE1	=	0x4166
                           004165  2116 G$AX5043_TXRATE2$0$0 == 0x4165
                           004165  2117 _AX5043_TXRATE2	=	0x4165
                           00406B  2118 G$AX5043_WAKEUP0$0$0 == 0x406b
                           00406B  2119 _AX5043_WAKEUP0	=	0x406b
                           00406A  2120 G$AX5043_WAKEUP1$0$0 == 0x406a
                           00406A  2121 _AX5043_WAKEUP1	=	0x406a
                           00406D  2122 G$AX5043_WAKEUPFREQ0$0$0 == 0x406d
                           00406D  2123 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  2124 G$AX5043_WAKEUPFREQ1$0$0 == 0x406c
                           00406C  2125 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  2126 G$AX5043_WAKEUPTIMER0$0$0 == 0x4069
                           004069  2127 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  2128 G$AX5043_WAKEUPTIMER1$0$0 == 0x4068
                           004068  2129 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  2130 G$AX5043_WAKEUPXOEARLY$0$0 == 0x406e
                           00406E  2131 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  2132 G$AX5043_XTALAMPL$0$0 == 0x4f11
                           004F11  2133 _AX5043_XTALAMPL	=	0x4f11
                           004184  2134 G$AX5043_XTALCAP$0$0 == 0x4184
                           004184  2135 _AX5043_XTALCAP	=	0x4184
                           004F10  2136 G$AX5043_XTALOSC$0$0 == 0x4f10
                           004F10  2137 _AX5043_XTALOSC	=	0x4f10
                           00401D  2138 G$AX5043_XTALSTATUS$0$0 == 0x401d
                           00401D  2139 _AX5043_XTALSTATUS	=	0x401d
                           004F00  2140 G$AX5043_0xF00$0$0 == 0x4f00
                           004F00  2141 _AX5043_0xF00	=	0x4f00
                           004F0C  2142 G$AX5043_0xF0C$0$0 == 0x4f0c
                           004F0C  2143 _AX5043_0xF0C	=	0x4f0c
                           004F18  2144 G$AX5043_0xF18$0$0 == 0x4f18
                           004F18  2145 _AX5043_0xF18	=	0x4f18
                           004F1C  2146 G$AX5043_0xF1C$0$0 == 0x4f1c
                           004F1C  2147 _AX5043_0xF1C	=	0x4f1c
                           004F21  2148 G$AX5043_0xF21$0$0 == 0x4f21
                           004F21  2149 _AX5043_0xF21	=	0x4f21
                           004F22  2150 G$AX5043_0xF22$0$0 == 0x4f22
                           004F22  2151 _AX5043_0xF22	=	0x4f22
                           004F23  2152 G$AX5043_0xF23$0$0 == 0x4f23
                           004F23  2153 _AX5043_0xF23	=	0x4f23
                           004F26  2154 G$AX5043_0xF26$0$0 == 0x4f26
                           004F26  2155 _AX5043_0xF26	=	0x4f26
                           004F30  2156 G$AX5043_0xF30$0$0 == 0x4f30
                           004F30  2157 _AX5043_0xF30	=	0x4f30
                           004F31  2158 G$AX5043_0xF31$0$0 == 0x4f31
                           004F31  2159 _AX5043_0xF31	=	0x4f31
                           004F32  2160 G$AX5043_0xF32$0$0 == 0x4f32
                           004F32  2161 _AX5043_0xF32	=	0x4f32
                           004F33  2162 G$AX5043_0xF33$0$0 == 0x4f33
                           004F33  2163 _AX5043_0xF33	=	0x4f33
                           004F34  2164 G$AX5043_0xF34$0$0 == 0x4f34
                           004F34  2165 _AX5043_0xF34	=	0x4f34
                           004F35  2166 G$AX5043_0xF35$0$0 == 0x4f35
                           004F35  2167 _AX5043_0xF35	=	0x4f35
                           004F44  2168 G$AX5043_0xF44$0$0 == 0x4f44
                           004F44  2169 _AX5043_0xF44	=	0x4f44
                           004122  2170 G$AX5043_AGCAHYST0$0$0 == 0x4122
                           004122  2171 _AX5043_AGCAHYST0	=	0x4122
                           004132  2172 G$AX5043_AGCAHYST1$0$0 == 0x4132
                           004132  2173 _AX5043_AGCAHYST1	=	0x4132
                           004142  2174 G$AX5043_AGCAHYST2$0$0 == 0x4142
                           004142  2175 _AX5043_AGCAHYST2	=	0x4142
                           004152  2176 G$AX5043_AGCAHYST3$0$0 == 0x4152
                           004152  2177 _AX5043_AGCAHYST3	=	0x4152
                           004120  2178 G$AX5043_AGCGAIN0$0$0 == 0x4120
                           004120  2179 _AX5043_AGCGAIN0	=	0x4120
                           004130  2180 G$AX5043_AGCGAIN1$0$0 == 0x4130
                           004130  2181 _AX5043_AGCGAIN1	=	0x4130
                           004140  2182 G$AX5043_AGCGAIN2$0$0 == 0x4140
                           004140  2183 _AX5043_AGCGAIN2	=	0x4140
                           004150  2184 G$AX5043_AGCGAIN3$0$0 == 0x4150
                           004150  2185 _AX5043_AGCGAIN3	=	0x4150
                           004123  2186 G$AX5043_AGCMINMAX0$0$0 == 0x4123
                           004123  2187 _AX5043_AGCMINMAX0	=	0x4123
                           004133  2188 G$AX5043_AGCMINMAX1$0$0 == 0x4133
                           004133  2189 _AX5043_AGCMINMAX1	=	0x4133
                           004143  2190 G$AX5043_AGCMINMAX2$0$0 == 0x4143
                           004143  2191 _AX5043_AGCMINMAX2	=	0x4143
                           004153  2192 G$AX5043_AGCMINMAX3$0$0 == 0x4153
                           004153  2193 _AX5043_AGCMINMAX3	=	0x4153
                           004121  2194 G$AX5043_AGCTARGET0$0$0 == 0x4121
                           004121  2195 _AX5043_AGCTARGET0	=	0x4121
                           004131  2196 G$AX5043_AGCTARGET1$0$0 == 0x4131
                           004131  2197 _AX5043_AGCTARGET1	=	0x4131
                           004141  2198 G$AX5043_AGCTARGET2$0$0 == 0x4141
                           004141  2199 _AX5043_AGCTARGET2	=	0x4141
                           004151  2200 G$AX5043_AGCTARGET3$0$0 == 0x4151
                           004151  2201 _AX5043_AGCTARGET3	=	0x4151
                           00412B  2202 G$AX5043_AMPLITUDEGAIN0$0$0 == 0x412b
                           00412B  2203 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  2204 G$AX5043_AMPLITUDEGAIN1$0$0 == 0x413b
                           00413B  2205 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  2206 G$AX5043_AMPLITUDEGAIN2$0$0 == 0x414b
                           00414B  2207 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  2208 G$AX5043_AMPLITUDEGAIN3$0$0 == 0x415b
                           00415B  2209 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  2210 G$AX5043_BBOFFSRES0$0$0 == 0x412f
                           00412F  2211 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  2212 G$AX5043_BBOFFSRES1$0$0 == 0x413f
                           00413F  2213 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  2214 G$AX5043_BBOFFSRES2$0$0 == 0x414f
                           00414F  2215 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  2216 G$AX5043_BBOFFSRES3$0$0 == 0x415f
                           00415F  2217 _AX5043_BBOFFSRES3	=	0x415f
                           004125  2218 G$AX5043_DRGAIN0$0$0 == 0x4125
                           004125  2219 _AX5043_DRGAIN0	=	0x4125
                           004135  2220 G$AX5043_DRGAIN1$0$0 == 0x4135
                           004135  2221 _AX5043_DRGAIN1	=	0x4135
                           004145  2222 G$AX5043_DRGAIN2$0$0 == 0x4145
                           004145  2223 _AX5043_DRGAIN2	=	0x4145
                           004155  2224 G$AX5043_DRGAIN3$0$0 == 0x4155
                           004155  2225 _AX5043_DRGAIN3	=	0x4155
                           00412E  2226 G$AX5043_FOURFSK0$0$0 == 0x412e
                           00412E  2227 _AX5043_FOURFSK0	=	0x412e
                           00413E  2228 G$AX5043_FOURFSK1$0$0 == 0x413e
                           00413E  2229 _AX5043_FOURFSK1	=	0x413e
                           00414E  2230 G$AX5043_FOURFSK2$0$0 == 0x414e
                           00414E  2231 _AX5043_FOURFSK2	=	0x414e
                           00415E  2232 G$AX5043_FOURFSK3$0$0 == 0x415e
                           00415E  2233 _AX5043_FOURFSK3	=	0x415e
                           00412D  2234 G$AX5043_FREQDEV00$0$0 == 0x412d
                           00412D  2235 _AX5043_FREQDEV00	=	0x412d
                           00413D  2236 G$AX5043_FREQDEV01$0$0 == 0x413d
                           00413D  2237 _AX5043_FREQDEV01	=	0x413d
                           00414D  2238 G$AX5043_FREQDEV02$0$0 == 0x414d
                           00414D  2239 _AX5043_FREQDEV02	=	0x414d
                           00415D  2240 G$AX5043_FREQDEV03$0$0 == 0x415d
                           00415D  2241 _AX5043_FREQDEV03	=	0x415d
                           00412C  2242 G$AX5043_FREQDEV10$0$0 == 0x412c
                           00412C  2243 _AX5043_FREQDEV10	=	0x412c
                           00413C  2244 G$AX5043_FREQDEV11$0$0 == 0x413c
                           00413C  2245 _AX5043_FREQDEV11	=	0x413c
                           00414C  2246 G$AX5043_FREQDEV12$0$0 == 0x414c
                           00414C  2247 _AX5043_FREQDEV12	=	0x414c
                           00415C  2248 G$AX5043_FREQDEV13$0$0 == 0x415c
                           00415C  2249 _AX5043_FREQDEV13	=	0x415c
                           004127  2250 G$AX5043_FREQUENCYGAINA0$0$0 == 0x4127
                           004127  2251 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  2252 G$AX5043_FREQUENCYGAINA1$0$0 == 0x4137
                           004137  2253 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  2254 G$AX5043_FREQUENCYGAINA2$0$0 == 0x4147
                           004147  2255 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  2256 G$AX5043_FREQUENCYGAINA3$0$0 == 0x4157
                           004157  2257 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  2258 G$AX5043_FREQUENCYGAINB0$0$0 == 0x4128
                           004128  2259 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  2260 G$AX5043_FREQUENCYGAINB1$0$0 == 0x4138
                           004138  2261 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  2262 G$AX5043_FREQUENCYGAINB2$0$0 == 0x4148
                           004148  2263 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  2264 G$AX5043_FREQUENCYGAINB3$0$0 == 0x4158
                           004158  2265 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  2266 G$AX5043_FREQUENCYGAINC0$0$0 == 0x4129
                           004129  2267 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  2268 G$AX5043_FREQUENCYGAINC1$0$0 == 0x4139
                           004139  2269 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  2270 G$AX5043_FREQUENCYGAINC2$0$0 == 0x4149
                           004149  2271 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  2272 G$AX5043_FREQUENCYGAINC3$0$0 == 0x4159
                           004159  2273 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  2274 G$AX5043_FREQUENCYGAIND0$0$0 == 0x412a
                           00412A  2275 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  2276 G$AX5043_FREQUENCYGAIND1$0$0 == 0x413a
                           00413A  2277 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  2278 G$AX5043_FREQUENCYGAIND2$0$0 == 0x414a
                           00414A  2279 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  2280 G$AX5043_FREQUENCYGAIND3$0$0 == 0x415a
                           00415A  2281 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  2282 G$AX5043_FREQUENCYLEAK$0$0 == 0x4116
                           004116  2283 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  2284 G$AX5043_PHASEGAIN0$0$0 == 0x4126
                           004126  2285 _AX5043_PHASEGAIN0	=	0x4126
                           004136  2286 G$AX5043_PHASEGAIN1$0$0 == 0x4136
                           004136  2287 _AX5043_PHASEGAIN1	=	0x4136
                           004146  2288 G$AX5043_PHASEGAIN2$0$0 == 0x4146
                           004146  2289 _AX5043_PHASEGAIN2	=	0x4146
                           004156  2290 G$AX5043_PHASEGAIN3$0$0 == 0x4156
                           004156  2291 _AX5043_PHASEGAIN3	=	0x4156
                           004207  2292 G$AX5043_PKTADDR0$0$0 == 0x4207
                           004207  2293 _AX5043_PKTADDR0	=	0x4207
                           004206  2294 G$AX5043_PKTADDR1$0$0 == 0x4206
                           004206  2295 _AX5043_PKTADDR1	=	0x4206
                           004205  2296 G$AX5043_PKTADDR2$0$0 == 0x4205
                           004205  2297 _AX5043_PKTADDR2	=	0x4205
                           004204  2298 G$AX5043_PKTADDR3$0$0 == 0x4204
                           004204  2299 _AX5043_PKTADDR3	=	0x4204
                           004200  2300 G$AX5043_PKTADDRCFG$0$0 == 0x4200
                           004200  2301 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  2302 G$AX5043_PKTADDRMASK0$0$0 == 0x420b
                           00420B  2303 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  2304 G$AX5043_PKTADDRMASK1$0$0 == 0x420a
                           00420A  2305 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  2306 G$AX5043_PKTADDRMASK2$0$0 == 0x4209
                           004209  2307 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  2308 G$AX5043_PKTADDRMASK3$0$0 == 0x4208
                           004208  2309 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  2310 G$AX5043_PKTLENCFG$0$0 == 0x4201
                           004201  2311 _AX5043_PKTLENCFG	=	0x4201
                           004202  2312 G$AX5043_PKTLENOFFSET$0$0 == 0x4202
                           004202  2313 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  2314 G$AX5043_PKTMAXLEN$0$0 == 0x4203
                           004203  2315 _AX5043_PKTMAXLEN	=	0x4203
                           004118  2316 G$AX5043_RXPARAMCURSET$0$0 == 0x4118
                           004118  2317 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  2318 G$AX5043_RXPARAMSETS$0$0 == 0x4117
                           004117  2319 _AX5043_RXPARAMSETS	=	0x4117
                           004124  2320 G$AX5043_TIMEGAIN0$0$0 == 0x4124
                           004124  2321 _AX5043_TIMEGAIN0	=	0x4124
                           004134  2322 G$AX5043_TIMEGAIN1$0$0 == 0x4134
                           004134  2323 _AX5043_TIMEGAIN1	=	0x4134
                           004144  2324 G$AX5043_TIMEGAIN2$0$0 == 0x4144
                           004144  2325 _AX5043_TIMEGAIN2	=	0x4144
                           004154  2326 G$AX5043_TIMEGAIN3$0$0 == 0x4154
                           004154  2327 _AX5043_TIMEGAIN3	=	0x4154
                           005114  2328 G$AX5043_AFSKCTRLNB$0$0 == 0x5114
                           005114  2329 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  2330 G$AX5043_AFSKMARK0NB$0$0 == 0x5113
                           005113  2331 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  2332 G$AX5043_AFSKMARK1NB$0$0 == 0x5112
                           005112  2333 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  2334 G$AX5043_AFSKSPACE0NB$0$0 == 0x5111
                           005111  2335 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  2336 G$AX5043_AFSKSPACE1NB$0$0 == 0x5110
                           005110  2337 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  2338 G$AX5043_AGCCOUNTERNB$0$0 == 0x5043
                           005043  2339 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  2340 G$AX5043_AMPLFILTERNB$0$0 == 0x5115
                           005115  2341 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  2342 G$AX5043_BBOFFSCAPNB$0$0 == 0x5189
                           005189  2343 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  2344 G$AX5043_BBTUNENB$0$0 == 0x5188
                           005188  2345 _AX5043_BBTUNENB	=	0x5188
                           005041  2346 G$AX5043_BGNDRSSINB$0$0 == 0x5041
                           005041  2347 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  2348 G$AX5043_BGNDRSSIGAINNB$0$0 == 0x522e
                           00522E  2349 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  2350 G$AX5043_BGNDRSSITHRNB$0$0 == 0x522f
                           00522F  2351 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2352 G$AX5043_CRCINIT0NB$0$0 == 0x5017
                           005017  2353 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2354 G$AX5043_CRCINIT1NB$0$0 == 0x5016
                           005016  2355 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2356 G$AX5043_CRCINIT2NB$0$0 == 0x5015
                           005015  2357 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2358 G$AX5043_CRCINIT3NB$0$0 == 0x5014
                           005014  2359 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2360 G$AX5043_DACCONFIGNB$0$0 == 0x5332
                           005332  2361 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2362 G$AX5043_DACVALUE0NB$0$0 == 0x5331
                           005331  2363 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2364 G$AX5043_DACVALUE1NB$0$0 == 0x5330
                           005330  2365 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2366 G$AX5043_DECIMATIONNB$0$0 == 0x5102
                           005102  2367 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2368 G$AX5043_DIVERSITYNB$0$0 == 0x5042
                           005042  2369 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2370 G$AX5043_ENCODINGNB$0$0 == 0x5011
                           005011  2371 _AX5043_ENCODINGNB	=	0x5011
                           005018  2372 G$AX5043_FECNB$0$0 == 0x5018
                           005018  2373 _AX5043_FECNB	=	0x5018
                           00501A  2374 G$AX5043_FECSTATUSNB$0$0 == 0x501a
                           00501A  2375 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2376 G$AX5043_FECSYNCNB$0$0 == 0x5019
                           005019  2377 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2378 G$AX5043_FIFOCOUNT0NB$0$0 == 0x502b
                           00502B  2379 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2380 G$AX5043_FIFOCOUNT1NB$0$0 == 0x502a
                           00502A  2381 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2382 G$AX5043_FIFODATANB$0$0 == 0x5029
                           005029  2383 _AX5043_FIFODATANB	=	0x5029
                           00502D  2384 G$AX5043_FIFOFREE0NB$0$0 == 0x502d
                           00502D  2385 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2386 G$AX5043_FIFOFREE1NB$0$0 == 0x502c
                           00502C  2387 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2388 G$AX5043_FIFOSTATNB$0$0 == 0x5028
                           005028  2389 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2390 G$AX5043_FIFOTHRESH0NB$0$0 == 0x502f
                           00502F  2391 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2392 G$AX5043_FIFOTHRESH1NB$0$0 == 0x502e
                           00502E  2393 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2394 G$AX5043_FRAMINGNB$0$0 == 0x5012
                           005012  2395 _AX5043_FRAMINGNB	=	0x5012
                           005037  2396 G$AX5043_FREQA0NB$0$0 == 0x5037
                           005037  2397 _AX5043_FREQA0NB	=	0x5037
                           005036  2398 G$AX5043_FREQA1NB$0$0 == 0x5036
                           005036  2399 _AX5043_FREQA1NB	=	0x5036
                           005035  2400 G$AX5043_FREQA2NB$0$0 == 0x5035
                           005035  2401 _AX5043_FREQA2NB	=	0x5035
                           005034  2402 G$AX5043_FREQA3NB$0$0 == 0x5034
                           005034  2403 _AX5043_FREQA3NB	=	0x5034
                           00503F  2404 G$AX5043_FREQB0NB$0$0 == 0x503f
                           00503F  2405 _AX5043_FREQB0NB	=	0x503f
                           00503E  2406 G$AX5043_FREQB1NB$0$0 == 0x503e
                           00503E  2407 _AX5043_FREQB1NB	=	0x503e
                           00503D  2408 G$AX5043_FREQB2NB$0$0 == 0x503d
                           00503D  2409 _AX5043_FREQB2NB	=	0x503d
                           00503C  2410 G$AX5043_FREQB3NB$0$0 == 0x503c
                           00503C  2411 _AX5043_FREQB3NB	=	0x503c
                           005163  2412 G$AX5043_FSKDEV0NB$0$0 == 0x5163
                           005163  2413 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2414 G$AX5043_FSKDEV1NB$0$0 == 0x5162
                           005162  2415 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2416 G$AX5043_FSKDEV2NB$0$0 == 0x5161
                           005161  2417 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2418 G$AX5043_FSKDMAX0NB$0$0 == 0x510d
                           00510D  2419 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2420 G$AX5043_FSKDMAX1NB$0$0 == 0x510c
                           00510C  2421 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2422 G$AX5043_FSKDMIN0NB$0$0 == 0x510f
                           00510F  2423 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2424 G$AX5043_FSKDMIN1NB$0$0 == 0x510e
                           00510E  2425 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2426 G$AX5043_GPADC13VALUE0NB$0$0 == 0x5309
                           005309  2427 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2428 G$AX5043_GPADC13VALUE1NB$0$0 == 0x5308
                           005308  2429 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2430 G$AX5043_GPADCCTRLNB$0$0 == 0x5300
                           005300  2431 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2432 G$AX5043_GPADCPERIODNB$0$0 == 0x5301
                           005301  2433 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2434 G$AX5043_IFFREQ0NB$0$0 == 0x5101
                           005101  2435 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2436 G$AX5043_IFFREQ1NB$0$0 == 0x5100
                           005100  2437 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2438 G$AX5043_IRQINVERSION0NB$0$0 == 0x500b
                           00500B  2439 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2440 G$AX5043_IRQINVERSION1NB$0$0 == 0x500a
                           00500A  2441 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2442 G$AX5043_IRQMASK0NB$0$0 == 0x5007
                           005007  2443 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2444 G$AX5043_IRQMASK1NB$0$0 == 0x5006
                           005006  2445 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2446 G$AX5043_IRQREQUEST0NB$0$0 == 0x500d
                           00500D  2447 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2448 G$AX5043_IRQREQUEST1NB$0$0 == 0x500c
                           00500C  2449 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2450 G$AX5043_LPOSCCONFIGNB$0$0 == 0x5310
                           005310  2451 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2452 G$AX5043_LPOSCFREQ0NB$0$0 == 0x5317
                           005317  2453 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2454 G$AX5043_LPOSCFREQ1NB$0$0 == 0x5316
                           005316  2455 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2456 G$AX5043_LPOSCKFILT0NB$0$0 == 0x5313
                           005313  2457 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2458 G$AX5043_LPOSCKFILT1NB$0$0 == 0x5312
                           005312  2459 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2460 G$AX5043_LPOSCPER0NB$0$0 == 0x5319
                           005319  2461 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2462 G$AX5043_LPOSCPER1NB$0$0 == 0x5318
                           005318  2463 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2464 G$AX5043_LPOSCREF0NB$0$0 == 0x5315
                           005315  2465 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2466 G$AX5043_LPOSCREF1NB$0$0 == 0x5314
                           005314  2467 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2468 G$AX5043_LPOSCSTATUSNB$0$0 == 0x5311
                           005311  2469 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2470 G$AX5043_MATCH0LENNB$0$0 == 0x5214
                           005214  2471 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2472 G$AX5043_MATCH0MAXNB$0$0 == 0x5216
                           005216  2473 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2474 G$AX5043_MATCH0MINNB$0$0 == 0x5215
                           005215  2475 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2476 G$AX5043_MATCH0PAT0NB$0$0 == 0x5213
                           005213  2477 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2478 G$AX5043_MATCH0PAT1NB$0$0 == 0x5212
                           005212  2479 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2480 G$AX5043_MATCH0PAT2NB$0$0 == 0x5211
                           005211  2481 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2482 G$AX5043_MATCH0PAT3NB$0$0 == 0x5210
                           005210  2483 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2484 G$AX5043_MATCH1LENNB$0$0 == 0x521c
                           00521C  2485 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2486 G$AX5043_MATCH1MAXNB$0$0 == 0x521e
                           00521E  2487 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2488 G$AX5043_MATCH1MINNB$0$0 == 0x521d
                           00521D  2489 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2490 G$AX5043_MATCH1PAT0NB$0$0 == 0x5219
                           005219  2491 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2492 G$AX5043_MATCH1PAT1NB$0$0 == 0x5218
                           005218  2493 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2494 G$AX5043_MAXDROFFSET0NB$0$0 == 0x5108
                           005108  2495 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2496 G$AX5043_MAXDROFFSET1NB$0$0 == 0x5107
                           005107  2497 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2498 G$AX5043_MAXDROFFSET2NB$0$0 == 0x5106
                           005106  2499 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2500 G$AX5043_MAXRFOFFSET0NB$0$0 == 0x510b
                           00510B  2501 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2502 G$AX5043_MAXRFOFFSET1NB$0$0 == 0x510a
                           00510A  2503 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2504 G$AX5043_MAXRFOFFSET2NB$0$0 == 0x5109
                           005109  2505 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2506 G$AX5043_MODCFGANB$0$0 == 0x5164
                           005164  2507 _AX5043_MODCFGANB	=	0x5164
                           005160  2508 G$AX5043_MODCFGFNB$0$0 == 0x5160
                           005160  2509 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2510 G$AX5043_MODCFGPNB$0$0 == 0x5f5f
                           005F5F  2511 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2512 G$AX5043_MODULATIONNB$0$0 == 0x5010
                           005010  2513 _AX5043_MODULATIONNB	=	0x5010
                           005025  2514 G$AX5043_PINFUNCANTSELNB$0$0 == 0x5025
                           005025  2515 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2516 G$AX5043_PINFUNCDATANB$0$0 == 0x5023
                           005023  2517 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2518 G$AX5043_PINFUNCDCLKNB$0$0 == 0x5022
                           005022  2519 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2520 G$AX5043_PINFUNCIRQNB$0$0 == 0x5024
                           005024  2521 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2522 G$AX5043_PINFUNCPWRAMPNB$0$0 == 0x5026
                           005026  2523 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2524 G$AX5043_PINFUNCSYSCLKNB$0$0 == 0x5021
                           005021  2525 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2526 G$AX5043_PINSTATENB$0$0 == 0x5020
                           005020  2527 _AX5043_PINSTATENB	=	0x5020
                           005233  2528 G$AX5043_PKTACCEPTFLAGSNB$0$0 == 0x5233
                           005233  2529 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2530 G$AX5043_PKTCHUNKSIZENB$0$0 == 0x5230
                           005230  2531 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2532 G$AX5043_PKTMISCFLAGSNB$0$0 == 0x5231
                           005231  2533 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2534 G$AX5043_PKTSTOREFLAGSNB$0$0 == 0x5232
                           005232  2535 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2536 G$AX5043_PLLCPINB$0$0 == 0x5031
                           005031  2537 _AX5043_PLLCPINB	=	0x5031
                           005039  2538 G$AX5043_PLLCPIBOOSTNB$0$0 == 0x5039
                           005039  2539 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2540 G$AX5043_PLLLOCKDETNB$0$0 == 0x5182
                           005182  2541 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2542 G$AX5043_PLLLOOPNB$0$0 == 0x5030
                           005030  2543 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2544 G$AX5043_PLLLOOPBOOSTNB$0$0 == 0x5038
                           005038  2545 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2546 G$AX5043_PLLRANGINGANB$0$0 == 0x5033
                           005033  2547 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2548 G$AX5043_PLLRANGINGBNB$0$0 == 0x503b
                           00503B  2549 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2550 G$AX5043_PLLRNGCLKNB$0$0 == 0x5183
                           005183  2551 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2552 G$AX5043_PLLVCODIVNB$0$0 == 0x5032
                           005032  2553 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2554 G$AX5043_PLLVCOINB$0$0 == 0x5180
                           005180  2555 _AX5043_PLLVCOINB	=	0x5180
                           005181  2556 G$AX5043_PLLVCOIRNB$0$0 == 0x5181
                           005181  2557 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2558 G$AX5043_POWCTRL1NB$0$0 == 0x5f08
                           005F08  2559 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2560 G$AX5043_POWIRQMASKNB$0$0 == 0x5005
                           005005  2561 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2562 G$AX5043_POWSTATNB$0$0 == 0x5003
                           005003  2563 _AX5043_POWSTATNB	=	0x5003
                           005004  2564 G$AX5043_POWSTICKYSTATNB$0$0 == 0x5004
                           005004  2565 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2566 G$AX5043_PWRAMPNB$0$0 == 0x5027
                           005027  2567 _AX5043_PWRAMPNB	=	0x5027
                           005002  2568 G$AX5043_PWRMODENB$0$0 == 0x5002
                           005002  2569 _AX5043_PWRMODENB	=	0x5002
                           005009  2570 G$AX5043_RADIOEVENTMASK0NB$0$0 == 0x5009
                           005009  2571 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2572 G$AX5043_RADIOEVENTMASK1NB$0$0 == 0x5008
                           005008  2573 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2574 G$AX5043_RADIOEVENTREQ0NB$0$0 == 0x500f
                           00500F  2575 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2576 G$AX5043_RADIOEVENTREQ1NB$0$0 == 0x500e
                           00500E  2577 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2578 G$AX5043_RADIOSTATENB$0$0 == 0x501c
                           00501C  2579 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2580 G$AX5043_REFNB$0$0 == 0x5f0d
                           005F0D  2581 _AX5043_REFNB	=	0x5f0d
                           005040  2582 G$AX5043_RSSINB$0$0 == 0x5040
                           005040  2583 _AX5043_RSSINB	=	0x5040
                           00522D  2584 G$AX5043_RSSIABSTHRNB$0$0 == 0x522d
                           00522D  2585 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2586 G$AX5043_RSSIREFERENCENB$0$0 == 0x522c
                           00522C  2587 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2588 G$AX5043_RXDATARATE0NB$0$0 == 0x5105
                           005105  2589 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2590 G$AX5043_RXDATARATE1NB$0$0 == 0x5104
                           005104  2591 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2592 G$AX5043_RXDATARATE2NB$0$0 == 0x5103
                           005103  2593 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2594 G$AX5043_SCRATCHNB$0$0 == 0x5001
                           005001  2595 _AX5043_SCRATCHNB	=	0x5001
                           005000  2596 G$AX5043_SILICONREVISIONNB$0$0 == 0x5000
                           005000  2597 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2598 G$AX5043_TIMER0NB$0$0 == 0x505b
                           00505B  2599 _AX5043_TIMER0NB	=	0x505b
                           00505A  2600 G$AX5043_TIMER1NB$0$0 == 0x505a
                           00505A  2601 _AX5043_TIMER1NB	=	0x505a
                           005059  2602 G$AX5043_TIMER2NB$0$0 == 0x5059
                           005059  2603 _AX5043_TIMER2NB	=	0x5059
                           005227  2604 G$AX5043_TMGRXAGCNB$0$0 == 0x5227
                           005227  2605 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2606 G$AX5043_TMGRXBOOSTNB$0$0 == 0x5223
                           005223  2607 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2608 G$AX5043_TMGRXCOARSEAGCNB$0$0 == 0x5226
                           005226  2609 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2610 G$AX5043_TMGRXOFFSACQNB$0$0 == 0x5225
                           005225  2611 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2612 G$AX5043_TMGRXPREAMBLE1NB$0$0 == 0x5229
                           005229  2613 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2614 G$AX5043_TMGRXPREAMBLE2NB$0$0 == 0x522a
                           00522A  2615 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2616 G$AX5043_TMGRXPREAMBLE3NB$0$0 == 0x522b
                           00522B  2617 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2618 G$AX5043_TMGRXRSSINB$0$0 == 0x5228
                           005228  2619 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2620 G$AX5043_TMGRXSETTLENB$0$0 == 0x5224
                           005224  2621 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2622 G$AX5043_TMGTXBOOSTNB$0$0 == 0x5220
                           005220  2623 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2624 G$AX5043_TMGTXSETTLENB$0$0 == 0x5221
                           005221  2625 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2626 G$AX5043_TRKAFSKDEMOD0NB$0$0 == 0x5055
                           005055  2627 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2628 G$AX5043_TRKAFSKDEMOD1NB$0$0 == 0x5054
                           005054  2629 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2630 G$AX5043_TRKAMPLITUDE0NB$0$0 == 0x5049
                           005049  2631 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2632 G$AX5043_TRKAMPLITUDE1NB$0$0 == 0x5048
                           005048  2633 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2634 G$AX5043_TRKDATARATE0NB$0$0 == 0x5047
                           005047  2635 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2636 G$AX5043_TRKDATARATE1NB$0$0 == 0x5046
                           005046  2637 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2638 G$AX5043_TRKDATARATE2NB$0$0 == 0x5045
                           005045  2639 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2640 G$AX5043_TRKFREQ0NB$0$0 == 0x5051
                           005051  2641 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2642 G$AX5043_TRKFREQ1NB$0$0 == 0x5050
                           005050  2643 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2644 G$AX5043_TRKFSKDEMOD0NB$0$0 == 0x5053
                           005053  2645 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2646 G$AX5043_TRKFSKDEMOD1NB$0$0 == 0x5052
                           005052  2647 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2648 G$AX5043_TRKPHASE0NB$0$0 == 0x504b
                           00504B  2649 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2650 G$AX5043_TRKPHASE1NB$0$0 == 0x504a
                           00504A  2651 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2652 G$AX5043_TRKRFFREQ0NB$0$0 == 0x504f
                           00504F  2653 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2654 G$AX5043_TRKRFFREQ1NB$0$0 == 0x504e
                           00504E  2655 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2656 G$AX5043_TRKRFFREQ2NB$0$0 == 0x504d
                           00504D  2657 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2658 G$AX5043_TXPWRCOEFFA0NB$0$0 == 0x5169
                           005169  2659 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2660 G$AX5043_TXPWRCOEFFA1NB$0$0 == 0x5168
                           005168  2661 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2662 G$AX5043_TXPWRCOEFFB0NB$0$0 == 0x516b
                           00516B  2663 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2664 G$AX5043_TXPWRCOEFFB1NB$0$0 == 0x516a
                           00516A  2665 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2666 G$AX5043_TXPWRCOEFFC0NB$0$0 == 0x516d
                           00516D  2667 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2668 G$AX5043_TXPWRCOEFFC1NB$0$0 == 0x516c
                           00516C  2669 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2670 G$AX5043_TXPWRCOEFFD0NB$0$0 == 0x516f
                           00516F  2671 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2672 G$AX5043_TXPWRCOEFFD1NB$0$0 == 0x516e
                           00516E  2673 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2674 G$AX5043_TXPWRCOEFFE0NB$0$0 == 0x5171
                           005171  2675 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2676 G$AX5043_TXPWRCOEFFE1NB$0$0 == 0x5170
                           005170  2677 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2678 G$AX5043_TXRATE0NB$0$0 == 0x5167
                           005167  2679 _AX5043_TXRATE0NB	=	0x5167
                           005166  2680 G$AX5043_TXRATE1NB$0$0 == 0x5166
                           005166  2681 _AX5043_TXRATE1NB	=	0x5166
                           005165  2682 G$AX5043_TXRATE2NB$0$0 == 0x5165
                           005165  2683 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2684 G$AX5043_WAKEUP0NB$0$0 == 0x506b
                           00506B  2685 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2686 G$AX5043_WAKEUP1NB$0$0 == 0x506a
                           00506A  2687 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2688 G$AX5043_WAKEUPFREQ0NB$0$0 == 0x506d
                           00506D  2689 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2690 G$AX5043_WAKEUPFREQ1NB$0$0 == 0x506c
                           00506C  2691 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2692 G$AX5043_WAKEUPTIMER0NB$0$0 == 0x5069
                           005069  2693 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2694 G$AX5043_WAKEUPTIMER1NB$0$0 == 0x5068
                           005068  2695 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2696 G$AX5043_WAKEUPXOEARLYNB$0$0 == 0x506e
                           00506E  2697 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2698 G$AX5043_XTALAMPLNB$0$0 == 0x5f11
                           005F11  2699 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2700 G$AX5043_XTALCAPNB$0$0 == 0x5184
                           005184  2701 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2702 G$AX5043_XTALOSCNB$0$0 == 0x5f10
                           005F10  2703 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2704 G$AX5043_XTALSTATUSNB$0$0 == 0x501d
                           00501D  2705 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2706 G$AX5043_0xF00NB$0$0 == 0x5f00
                           005F00  2707 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2708 G$AX5043_0xF0CNB$0$0 == 0x5f0c
                           005F0C  2709 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2710 G$AX5043_0xF18NB$0$0 == 0x5f18
                           005F18  2711 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2712 G$AX5043_0xF1CNB$0$0 == 0x5f1c
                           005F1C  2713 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2714 G$AX5043_0xF21NB$0$0 == 0x5f21
                           005F21  2715 _AX5043_0xF21NB	=	0x5f21
                           005F22  2716 G$AX5043_0xF22NB$0$0 == 0x5f22
                           005F22  2717 _AX5043_0xF22NB	=	0x5f22
                           005F23  2718 G$AX5043_0xF23NB$0$0 == 0x5f23
                           005F23  2719 _AX5043_0xF23NB	=	0x5f23
                           005F26  2720 G$AX5043_0xF26NB$0$0 == 0x5f26
                           005F26  2721 _AX5043_0xF26NB	=	0x5f26
                           005F30  2722 G$AX5043_0xF30NB$0$0 == 0x5f30
                           005F30  2723 _AX5043_0xF30NB	=	0x5f30
                           005F31  2724 G$AX5043_0xF31NB$0$0 == 0x5f31
                           005F31  2725 _AX5043_0xF31NB	=	0x5f31
                           005F32  2726 G$AX5043_0xF32NB$0$0 == 0x5f32
                           005F32  2727 _AX5043_0xF32NB	=	0x5f32
                           005F33  2728 G$AX5043_0xF33NB$0$0 == 0x5f33
                           005F33  2729 _AX5043_0xF33NB	=	0x5f33
                           005F34  2730 G$AX5043_0xF34NB$0$0 == 0x5f34
                           005F34  2731 _AX5043_0xF34NB	=	0x5f34
                           005F35  2732 G$AX5043_0xF35NB$0$0 == 0x5f35
                           005F35  2733 _AX5043_0xF35NB	=	0x5f35
                           005F44  2734 G$AX5043_0xF44NB$0$0 == 0x5f44
                           005F44  2735 _AX5043_0xF44NB	=	0x5f44
                           005122  2736 G$AX5043_AGCAHYST0NB$0$0 == 0x5122
                           005122  2737 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2738 G$AX5043_AGCAHYST1NB$0$0 == 0x5132
                           005132  2739 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2740 G$AX5043_AGCAHYST2NB$0$0 == 0x5142
                           005142  2741 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2742 G$AX5043_AGCAHYST3NB$0$0 == 0x5152
                           005152  2743 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2744 G$AX5043_AGCGAIN0NB$0$0 == 0x5120
                           005120  2745 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2746 G$AX5043_AGCGAIN1NB$0$0 == 0x5130
                           005130  2747 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2748 G$AX5043_AGCGAIN2NB$0$0 == 0x5140
                           005140  2749 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2750 G$AX5043_AGCGAIN3NB$0$0 == 0x5150
                           005150  2751 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2752 G$AX5043_AGCMINMAX0NB$0$0 == 0x5123
                           005123  2753 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2754 G$AX5043_AGCMINMAX1NB$0$0 == 0x5133
                           005133  2755 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2756 G$AX5043_AGCMINMAX2NB$0$0 == 0x5143
                           005143  2757 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2758 G$AX5043_AGCMINMAX3NB$0$0 == 0x5153
                           005153  2759 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2760 G$AX5043_AGCTARGET0NB$0$0 == 0x5121
                           005121  2761 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2762 G$AX5043_AGCTARGET1NB$0$0 == 0x5131
                           005131  2763 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2764 G$AX5043_AGCTARGET2NB$0$0 == 0x5141
                           005141  2765 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2766 G$AX5043_AGCTARGET3NB$0$0 == 0x5151
                           005151  2767 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2768 G$AX5043_AMPLITUDEGAIN0NB$0$0 == 0x512b
                           00512B  2769 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2770 G$AX5043_AMPLITUDEGAIN1NB$0$0 == 0x513b
                           00513B  2771 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2772 G$AX5043_AMPLITUDEGAIN2NB$0$0 == 0x514b
                           00514B  2773 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2774 G$AX5043_AMPLITUDEGAIN3NB$0$0 == 0x515b
                           00515B  2775 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2776 G$AX5043_BBOFFSRES0NB$0$0 == 0x512f
                           00512F  2777 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2778 G$AX5043_BBOFFSRES1NB$0$0 == 0x513f
                           00513F  2779 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2780 G$AX5043_BBOFFSRES2NB$0$0 == 0x514f
                           00514F  2781 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2782 G$AX5043_BBOFFSRES3NB$0$0 == 0x515f
                           00515F  2783 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2784 G$AX5043_DRGAIN0NB$0$0 == 0x5125
                           005125  2785 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2786 G$AX5043_DRGAIN1NB$0$0 == 0x5135
                           005135  2787 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2788 G$AX5043_DRGAIN2NB$0$0 == 0x5145
                           005145  2789 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2790 G$AX5043_DRGAIN3NB$0$0 == 0x5155
                           005155  2791 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2792 G$AX5043_FOURFSK0NB$0$0 == 0x512e
                           00512E  2793 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2794 G$AX5043_FOURFSK1NB$0$0 == 0x513e
                           00513E  2795 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2796 G$AX5043_FOURFSK2NB$0$0 == 0x514e
                           00514E  2797 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2798 G$AX5043_FOURFSK3NB$0$0 == 0x515e
                           00515E  2799 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2800 G$AX5043_FREQDEV00NB$0$0 == 0x512d
                           00512D  2801 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2802 G$AX5043_FREQDEV01NB$0$0 == 0x513d
                           00513D  2803 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2804 G$AX5043_FREQDEV02NB$0$0 == 0x514d
                           00514D  2805 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2806 G$AX5043_FREQDEV03NB$0$0 == 0x515d
                           00515D  2807 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2808 G$AX5043_FREQDEV10NB$0$0 == 0x512c
                           00512C  2809 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2810 G$AX5043_FREQDEV11NB$0$0 == 0x513c
                           00513C  2811 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2812 G$AX5043_FREQDEV12NB$0$0 == 0x514c
                           00514C  2813 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2814 G$AX5043_FREQDEV13NB$0$0 == 0x515c
                           00515C  2815 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2816 G$AX5043_FREQUENCYGAINA0NB$0$0 == 0x5127
                           005127  2817 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2818 G$AX5043_FREQUENCYGAINA1NB$0$0 == 0x5137
                           005137  2819 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2820 G$AX5043_FREQUENCYGAINA2NB$0$0 == 0x5147
                           005147  2821 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2822 G$AX5043_FREQUENCYGAINA3NB$0$0 == 0x5157
                           005157  2823 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2824 G$AX5043_FREQUENCYGAINB0NB$0$0 == 0x5128
                           005128  2825 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2826 G$AX5043_FREQUENCYGAINB1NB$0$0 == 0x5138
                           005138  2827 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2828 G$AX5043_FREQUENCYGAINB2NB$0$0 == 0x5148
                           005148  2829 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2830 G$AX5043_FREQUENCYGAINB3NB$0$0 == 0x5158
                           005158  2831 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2832 G$AX5043_FREQUENCYGAINC0NB$0$0 == 0x5129
                           005129  2833 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2834 G$AX5043_FREQUENCYGAINC1NB$0$0 == 0x5139
                           005139  2835 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2836 G$AX5043_FREQUENCYGAINC2NB$0$0 == 0x5149
                           005149  2837 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2838 G$AX5043_FREQUENCYGAINC3NB$0$0 == 0x5159
                           005159  2839 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2840 G$AX5043_FREQUENCYGAIND0NB$0$0 == 0x512a
                           00512A  2841 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2842 G$AX5043_FREQUENCYGAIND1NB$0$0 == 0x513a
                           00513A  2843 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2844 G$AX5043_FREQUENCYGAIND2NB$0$0 == 0x514a
                           00514A  2845 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2846 G$AX5043_FREQUENCYGAIND3NB$0$0 == 0x515a
                           00515A  2847 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2848 G$AX5043_FREQUENCYLEAKNB$0$0 == 0x5116
                           005116  2849 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2850 G$AX5043_PHASEGAIN0NB$0$0 == 0x5126
                           005126  2851 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2852 G$AX5043_PHASEGAIN1NB$0$0 == 0x5136
                           005136  2853 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2854 G$AX5043_PHASEGAIN2NB$0$0 == 0x5146
                           005146  2855 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2856 G$AX5043_PHASEGAIN3NB$0$0 == 0x5156
                           005156  2857 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2858 G$AX5043_PKTADDR0NB$0$0 == 0x5207
                           005207  2859 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2860 G$AX5043_PKTADDR1NB$0$0 == 0x5206
                           005206  2861 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2862 G$AX5043_PKTADDR2NB$0$0 == 0x5205
                           005205  2863 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2864 G$AX5043_PKTADDR3NB$0$0 == 0x5204
                           005204  2865 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2866 G$AX5043_PKTADDRCFGNB$0$0 == 0x5200
                           005200  2867 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2868 G$AX5043_PKTADDRMASK0NB$0$0 == 0x520b
                           00520B  2869 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2870 G$AX5043_PKTADDRMASK1NB$0$0 == 0x520a
                           00520A  2871 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2872 G$AX5043_PKTADDRMASK2NB$0$0 == 0x5209
                           005209  2873 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2874 G$AX5043_PKTADDRMASK3NB$0$0 == 0x5208
                           005208  2875 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2876 G$AX5043_PKTLENCFGNB$0$0 == 0x5201
                           005201  2877 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2878 G$AX5043_PKTLENOFFSETNB$0$0 == 0x5202
                           005202  2879 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2880 G$AX5043_PKTMAXLENNB$0$0 == 0x5203
                           005203  2881 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2882 G$AX5043_RXPARAMCURSETNB$0$0 == 0x5118
                           005118  2883 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2884 G$AX5043_RXPARAMSETSNB$0$0 == 0x5117
                           005117  2885 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2886 G$AX5043_TIMEGAIN0NB$0$0 == 0x5124
                           005124  2887 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2888 G$AX5043_TIMEGAIN1NB$0$0 == 0x5134
                           005134  2889 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2890 G$AX5043_TIMEGAIN2NB$0$0 == 0x5144
                           005144  2891 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2892 G$AX5043_TIMEGAIN3NB$0$0 == 0x5154
                           005154  2893 _AX5043_TIMEGAIN3NB	=	0x5154
                           007091  2894 G$AESCONFIG$0$0 == 0x7091
                           007091  2895 _AESCONFIG	=	0x7091
                           007098  2896 G$AESCURBLOCK$0$0 == 0x7098
                           007098  2897 _AESCURBLOCK	=	0x7098
                           007094  2898 G$AESINADDR0$0$0 == 0x7094
                           007094  2899 _AESINADDR0	=	0x7094
                           007095  2900 G$AESINADDR1$0$0 == 0x7095
                           007095  2901 _AESINADDR1	=	0x7095
                           007094  2902 G$AESINADDR$0$0 == 0x7094
                           007094  2903 _AESINADDR	=	0x7094
                           007092  2904 G$AESKEYADDR0$0$0 == 0x7092
                           007092  2905 _AESKEYADDR0	=	0x7092
                           007093  2906 G$AESKEYADDR1$0$0 == 0x7093
                           007093  2907 _AESKEYADDR1	=	0x7093
                           007092  2908 G$AESKEYADDR$0$0 == 0x7092
                           007092  2909 _AESKEYADDR	=	0x7092
                           007090  2910 G$AESMODE$0$0 == 0x7090
                           007090  2911 _AESMODE	=	0x7090
                           007096  2912 G$AESOUTADDR0$0$0 == 0x7096
                           007096  2913 _AESOUTADDR0	=	0x7096
                           007097  2914 G$AESOUTADDR1$0$0 == 0x7097
                           007097  2915 _AESOUTADDR1	=	0x7097
                           007096  2916 G$AESOUTADDR$0$0 == 0x7096
                           007096  2917 _AESOUTADDR	=	0x7096
                           007081  2918 G$RNGBYTE$0$0 == 0x7081
                           007081  2919 _RNGBYTE	=	0x7081
                           007082  2920 G$RNGCLKSRC0$0$0 == 0x7082
                           007082  2921 _RNGCLKSRC0	=	0x7082
                           007083  2922 G$RNGCLKSRC1$0$0 == 0x7083
                           007083  2923 _RNGCLKSRC1	=	0x7083
                           007080  2924 G$RNGMODE$0$0 == 0x7080
                           007080  2925 _RNGMODE	=	0x7080
                           000000  2926 G$randomSet$0$0==.
      0002A0                       2927 _randomSet::
      0002A0                       2928 	.ds 3
                           000003  2929 G$siblingSlots$0$0==.
      0002A3                       2930 _siblingSlots::
      0002A3                       2931 	.ds 2
                           000005  2932 G$demoPacketBuffer$0$0==.
      0002A5                       2933 _demoPacketBuffer::
      0002A5                       2934 	.ds 135
                           00008C  2935 G$premPacketBuffer$0$0==.
      00032C                       2936 _premPacketBuffer::
      00032C                       2937 	.ds 45
                           0000B9  2938 G$msg1_tmr$0$0==.
      000359                       2939 _msg1_tmr::
      000359                       2940 	.ds 8
                           0000C1  2941 G$msg2_tmr$0$0==.
      000361                       2942 _msg2_tmr::
      000361                       2943 	.ds 8
                           0000C9  2944 G$msg3_tmr$0$0==.
      000369                       2945 _msg3_tmr::
      000369                       2946 	.ds 8
                           0000D1  2947 Lpktframing.packetPremFraming$CRC32$1$378==.
      000371                       2948 _packetPremFraming_CRC32_1_378:
      000371                       2949 	.ds 4
                           0000D5  2950 Lpktframing.packetStdFraming$sib0$1$380==.
      000375                       2951 _packetStdFraming_sib0_1_380:
      000375                       2952 	.ds 1
                           0000D6  2953 Lpktframing.packetStdFraming$sib1$1$380==.
      000376                       2954 _packetStdFraming_sib1_1_380:
      000376                       2955 	.ds 1
                           0000D7  2956 Lpktframing.packetStdFraming$CRC32$1$380==.
      000377                       2957 _packetStdFraming_CRC32_1_380:
      000377                       2958 	.ds 4
                                   2959 ;--------------------------------------------------------
                                   2960 ; absolute external ram data
                                   2961 ;--------------------------------------------------------
                                   2962 	.area XABS    (ABS,XDATA)
                                   2963 ;--------------------------------------------------------
                                   2964 ; external initialized ram data
                                   2965 ;--------------------------------------------------------
                                   2966 	.area XISEG   (XDATA)
                           000000  2967 G$dummyHMAC$0$0==.
      0009AE                       2968 _dummyHMAC::
      0009AE                       2969 	.ds 4
                           000004  2970 G$trmID$0$0==.
      0009B2                       2971 _trmID::
      0009B2                       2972 	.ds 4
                           000008  2973 G$StdPrem$0$0==.
      0009B6                       2974 _StdPrem::
      0009B6                       2975 	.ds 1
                                   2976 	.area HOME    (CODE)
                                   2977 	.area GSINIT0 (CODE)
                                   2978 	.area GSINIT1 (CODE)
                                   2979 	.area GSINIT2 (CODE)
                                   2980 	.area GSINIT3 (CODE)
                                   2981 	.area GSINIT4 (CODE)
                                   2982 	.area GSINIT5 (CODE)
                                   2983 	.area GSINIT  (CODE)
                                   2984 	.area GSFINAL (CODE)
                                   2985 	.area CSEG    (CODE)
                                   2986 ;--------------------------------------------------------
                                   2987 ; global & static initialisations
                                   2988 ;--------------------------------------------------------
                                   2989 	.area HOME    (CODE)
                                   2990 	.area GSINIT  (CODE)
                                   2991 	.area GSFINAL (CODE)
                                   2992 	.area GSINIT  (CODE)
                                   2993 ;--------------------------------------------------------
                                   2994 ; Home
                                   2995 ;--------------------------------------------------------
                                   2996 	.area HOME    (CODE)
                                   2997 	.area HOME    (CODE)
                                   2998 ;--------------------------------------------------------
                                   2999 ; code
                                   3000 ;--------------------------------------------------------
                                   3001 	.area CSEG    (CODE)
                                   3002 ;------------------------------------------------------------
                                   3003 ;Allocation info for local variables in function 'swap_variables'
                                   3004 ;------------------------------------------------------------
                                   3005 ;b                         Allocated with name '_swap_variables_PARM_2'
                                   3006 ;a                         Allocated to registers r5 r6 r7 
                                   3007 ;------------------------------------------------------------
                           000000  3008 	G$swap_variables$0$0 ==.
                           000000  3009 	C$pktframing.c$72$0$0 ==.
                                   3010 ;	..\COMMON\pktframing.c:72: void swap_variables(uint8_t *a, uint8_t *b)
                                   3011 ;	-----------------------------------------
                                   3012 ;	 function swap_variables
                                   3013 ;	-----------------------------------------
      00459E                       3014 _swap_variables:
                           000007  3015 	ar7 = 0x07
                           000006  3016 	ar6 = 0x06
                           000005  3017 	ar5 = 0x05
                           000004  3018 	ar4 = 0x04
                           000003  3019 	ar3 = 0x03
                           000002  3020 	ar2 = 0x02
                           000001  3021 	ar1 = 0x01
                           000000  3022 	ar0 = 0x00
                           000000  3023 	C$pktframing.c$74$1$371 ==.
                                   3024 ;	..\COMMON\pktframing.c:74: *a = *a + *b;
      00459E AD 82            [24] 3025 	mov	r5,dpl
      0045A0 AE 83            [24] 3026 	mov	r6,dph
      0045A2 AF F0            [24] 3027 	mov	r7,b
      0045A4 12 7C 55         [24] 3028 	lcall	__gptrget
      0045A7 FC               [12] 3029 	mov	r4,a
      0045A8 A9 6E            [24] 3030 	mov	r1,_swap_variables_PARM_2
      0045AA AA 6F            [24] 3031 	mov	r2,(_swap_variables_PARM_2 + 1)
      0045AC AB 70            [24] 3032 	mov	r3,(_swap_variables_PARM_2 + 2)
      0045AE 89 82            [24] 3033 	mov	dpl,r1
      0045B0 8A 83            [24] 3034 	mov	dph,r2
      0045B2 8B F0            [24] 3035 	mov	b,r3
      0045B4 12 7C 55         [24] 3036 	lcall	__gptrget
      0045B7 2C               [12] 3037 	add	a,r4
      0045B8 FC               [12] 3038 	mov	r4,a
      0045B9 8D 82            [24] 3039 	mov	dpl,r5
      0045BB 8E 83            [24] 3040 	mov	dph,r6
      0045BD 8F F0            [24] 3041 	mov	b,r7
      0045BF 12 6D D2         [24] 3042 	lcall	__gptrput
                           000024  3043 	C$pktframing.c$75$1$371 ==.
                                   3044 ;	..\COMMON\pktframing.c:75: *b = *a - *b;
      0045C2 89 82            [24] 3045 	mov	dpl,r1
      0045C4 8A 83            [24] 3046 	mov	dph,r2
      0045C6 8B F0            [24] 3047 	mov	b,r3
      0045C8 12 7C 55         [24] 3048 	lcall	__gptrget
      0045CB F8               [12] 3049 	mov	r0,a
      0045CC EC               [12] 3050 	mov	a,r4
      0045CD C3               [12] 3051 	clr	c
      0045CE 98               [12] 3052 	subb	a,r0
      0045CF F8               [12] 3053 	mov	r0,a
      0045D0 89 82            [24] 3054 	mov	dpl,r1
      0045D2 8A 83            [24] 3055 	mov	dph,r2
      0045D4 8B F0            [24] 3056 	mov	b,r3
      0045D6 12 6D D2         [24] 3057 	lcall	__gptrput
                           00003B  3058 	C$pktframing.c$76$1$371 ==.
                                   3059 ;	..\COMMON\pktframing.c:76: *a = *a - *b;
      0045D9 EC               [12] 3060 	mov	a,r4
      0045DA C3               [12] 3061 	clr	c
      0045DB 98               [12] 3062 	subb	a,r0
      0045DC 8D 82            [24] 3063 	mov	dpl,r5
      0045DE 8E 83            [24] 3064 	mov	dph,r6
      0045E0 8F F0            [24] 3065 	mov	b,r7
      0045E2 12 6D D2         [24] 3066 	lcall	__gptrput
                           000047  3067 	C$pktframing.c$77$1$371 ==.
                           000047  3068 	XG$swap_variables$0$0 ==.
      0045E5 22               [24] 3069 	ret
                                   3070 ;------------------------------------------------------------
                                   3071 ;Allocation info for local variables in function 'randomset_generate'
                                   3072 ;------------------------------------------------------------
                           000048  3073 	G$randomset_generate$0$0 ==.
                           000048  3074 	C$pktframing.c$81$1$371 ==.
                                   3075 ;	..\COMMON\pktframing.c:81: void randomset_generate(void)
                                   3076 ;	-----------------------------------------
                                   3077 ;	 function randomset_generate
                                   3078 ;	-----------------------------------------
      0045E6                       3079 _randomset_generate:
                           000048  3080 	C$pktframing.c$84$1$373 ==.
                                   3081 ;	..\COMMON\pktframing.c:84: randomSet[0] = RNGBYTE % rangeRANDOM + slotStdBASE;
      0045E6 90 70 81         [24] 3082 	mov	dptr,#_RNGBYTE
      0045E9 E0               [24] 3083 	movx	a,@dptr
      0045EA 75 F0 1E         [24] 3084 	mov	b,#0x1e
      0045ED 84               [48] 3085 	div	ab
      0045EE E5 F0            [12] 3086 	mov	a,b
      0045F0 24 1C            [12] 3087 	add	a,#0x1c
      0045F2 90 02 A0         [24] 3088 	mov	dptr,#_randomSet
      0045F5 F0               [24] 3089 	movx	@dptr,a
                           000058  3090 	C$pktframing.c$85$1$373 ==.
                                   3091 ;	..\COMMON\pktframing.c:85: delay_ms(rngDELAY_MS);
      0045F6 90 00 0A         [24] 3092 	mov	dptr,#0x000a
      0045F9 12 44 39         [24] 3093 	lcall	_delay_ms
                           00005E  3094 	C$pktframing.c$86$1$373 ==.
                                   3095 ;	..\COMMON\pktframing.c:86: randomSet[1] = RNGBYTE % rangeRANDOM + slotStdBASE;
      0045FC 90 70 81         [24] 3096 	mov	dptr,#_RNGBYTE
      0045FF E0               [24] 3097 	movx	a,@dptr
      004600 75 F0 1E         [24] 3098 	mov	b,#0x1e
      004603 84               [48] 3099 	div	ab
      004604 E5 F0            [12] 3100 	mov	a,b
      004606 24 1C            [12] 3101 	add	a,#0x1c
      004608 90 02 A1         [24] 3102 	mov	dptr,#(_randomSet + 0x0001)
      00460B F0               [24] 3103 	movx	@dptr,a
                           00006E  3104 	C$pktframing.c$87$1$373 ==.
                                   3105 ;	..\COMMON\pktframing.c:87: delay_ms(rngDELAY_MS);
      00460C 90 00 0A         [24] 3106 	mov	dptr,#0x000a
      00460F 12 44 39         [24] 3107 	lcall	_delay_ms
                           000074  3108 	C$pktframing.c$88$1$373 ==.
                                   3109 ;	..\COMMON\pktframing.c:88: randomSet[2] = RNGBYTE % rangeRANDOM + slotStdBASE;
      004612 90 70 81         [24] 3110 	mov	dptr,#_RNGBYTE
      004615 E0               [24] 3111 	movx	a,@dptr
      004616 75 F0 1E         [24] 3112 	mov	b,#0x1e
      004619 84               [48] 3113 	div	ab
      00461A E5 F0            [12] 3114 	mov	a,b
      00461C 24 1C            [12] 3115 	add	a,#0x1c
      00461E FF               [12] 3116 	mov	r7,a
      00461F 90 02 A2         [24] 3117 	mov	dptr,#(_randomSet + 0x0002)
      004622 F0               [24] 3118 	movx	@dptr,a
                           000085  3119 	C$pktframing.c$90$1$373 ==.
                                   3120 ;	..\COMMON\pktframing.c:90: while(randomSet[0] == randomSet[1])
      004623                       3121 00101$:
      004623 90 02 A0         [24] 3122 	mov	dptr,#_randomSet
      004626 E0               [24] 3123 	movx	a,@dptr
      004627 FF               [12] 3124 	mov	r7,a
      004628 90 02 A1         [24] 3125 	mov	dptr,#(_randomSet + 0x0001)
      00462B E0               [24] 3126 	movx	a,@dptr
      00462C FE               [12] 3127 	mov	r6,a
      00462D EF               [12] 3128 	mov	a,r7
      00462E B5 06 19         [24] 3129 	cjne	a,ar6,00105$
                           000093  3130 	C$pktframing.c$92$2$374 ==.
                                   3131 ;	..\COMMON\pktframing.c:92: delay_ms(rngDELAY_MS);
      004631 90 00 0A         [24] 3132 	mov	dptr,#0x000a
      004634 12 44 39         [24] 3133 	lcall	_delay_ms
                           000099  3134 	C$pktframing.c$93$2$374 ==.
                                   3135 ;	..\COMMON\pktframing.c:93: randomSet[1] = RNGBYTE % rangeRANDOM + slotStdBASE;
      004637 90 70 81         [24] 3136 	mov	dptr,#_RNGBYTE
      00463A E0               [24] 3137 	movx	a,@dptr
      00463B 75 F0 1E         [24] 3138 	mov	b,#0x1e
      00463E 84               [48] 3139 	div	ab
      00463F E5 F0            [12] 3140 	mov	a,b
      004641 24 1C            [12] 3141 	add	a,#0x1c
      004643 FF               [12] 3142 	mov	r7,a
      004644 90 02 A1         [24] 3143 	mov	dptr,#(_randomSet + 0x0001)
      004647 F0               [24] 3144 	movx	@dptr,a
                           0000AA  3145 	C$pktframing.c$95$1$373 ==.
                                   3146 ;	..\COMMON\pktframing.c:95: while ((randomSet[1] == randomSet[2]) || (randomSet[0] == randomSet[2]))
      004648 80 D9            [24] 3147 	sjmp	00101$
      00464A                       3148 00105$:
      00464A 90 02 A1         [24] 3149 	mov	dptr,#(_randomSet + 0x0001)
      00464D E0               [24] 3150 	movx	a,@dptr
      00464E FF               [12] 3151 	mov	r7,a
      00464F 90 02 A2         [24] 3152 	mov	dptr,#(_randomSet + 0x0002)
      004652 E0               [24] 3153 	movx	a,@dptr
      004653 FE               [12] 3154 	mov	r6,a
      004654 EF               [12] 3155 	mov	a,r7
      004655 B5 06 02         [24] 3156 	cjne	a,ar6,00144$
      004658 80 0E            [24] 3157 	sjmp	00106$
      00465A                       3158 00144$:
      00465A 90 02 A0         [24] 3159 	mov	dptr,#_randomSet
      00465D E0               [24] 3160 	movx	a,@dptr
      00465E FF               [12] 3161 	mov	r7,a
      00465F 90 02 A2         [24] 3162 	mov	dptr,#(_randomSet + 0x0002)
      004662 E0               [24] 3163 	movx	a,@dptr
      004663 FE               [12] 3164 	mov	r6,a
      004664 EF               [12] 3165 	mov	a,r7
      004665 B5 06 19         [24] 3166 	cjne	a,ar6,00107$
      004668                       3167 00106$:
                           0000CA  3168 	C$pktframing.c$97$2$375 ==.
                                   3169 ;	..\COMMON\pktframing.c:97: delay_ms(rngDELAY_MS);
      004668 90 00 0A         [24] 3170 	mov	dptr,#0x000a
      00466B 12 44 39         [24] 3171 	lcall	_delay_ms
                           0000D0  3172 	C$pktframing.c$98$2$375 ==.
                                   3173 ;	..\COMMON\pktframing.c:98: randomSet[2] = RNGBYTE % rangeRANDOM + slotStdBASE;
      00466E 90 70 81         [24] 3174 	mov	dptr,#_RNGBYTE
      004671 E0               [24] 3175 	movx	a,@dptr
      004672 75 F0 1E         [24] 3176 	mov	b,#0x1e
      004675 84               [48] 3177 	div	ab
      004676 E5 F0            [12] 3178 	mov	a,b
      004678 24 1C            [12] 3179 	add	a,#0x1c
      00467A FF               [12] 3180 	mov	r7,a
      00467B 90 02 A2         [24] 3181 	mov	dptr,#(_randomSet + 0x0002)
      00467E F0               [24] 3182 	movx	@dptr,a
      00467F 80 C9            [24] 3183 	sjmp	00105$
      004681                       3184 00107$:
                           0000E3  3185 	C$pktframing.c$102$1$373 ==.
                                   3186 ;	..\COMMON\pktframing.c:102: if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
      004681 90 02 A0         [24] 3187 	mov	dptr,#_randomSet
      004684 E0               [24] 3188 	movx	a,@dptr
      004685 FF               [12] 3189 	mov	r7,a
      004686 90 02 A1         [24] 3190 	mov	dptr,#(_randomSet + 0x0001)
      004689 E0               [24] 3191 	movx	a,@dptr
      00468A FE               [12] 3192 	mov	r6,a
      00468B C3               [12] 3193 	clr	c
      00468C 9F               [12] 3194 	subb	a,r7
      00468D 50 12            [24] 3195 	jnc	00109$
      00468F 75 6E A1         [24] 3196 	mov	_swap_variables_PARM_2,#(_randomSet + 0x0001)
      004692 75 6F 02         [24] 3197 	mov	(_swap_variables_PARM_2 + 1),#((_randomSet + 0x0001) >> 8)
      004695 75 70 00         [24] 3198 	mov	(_swap_variables_PARM_2 + 2),#0x00
      004698 90 02 A0         [24] 3199 	mov	dptr,#_randomSet
      00469B 75 F0 00         [24] 3200 	mov	b,#0x00
      00469E 12 45 9E         [24] 3201 	lcall	_swap_variables
      0046A1                       3202 00109$:
                           000103  3203 	C$pktframing.c$103$1$373 ==.
                                   3204 ;	..\COMMON\pktframing.c:103: if (randomSet[1] > randomSet[2]) swap_variables(&randomSet[1], &randomSet[2]);
      0046A1 90 02 A1         [24] 3205 	mov	dptr,#(_randomSet + 0x0001)
      0046A4 E0               [24] 3206 	movx	a,@dptr
      0046A5 FF               [12] 3207 	mov	r7,a
      0046A6 90 02 A2         [24] 3208 	mov	dptr,#(_randomSet + 0x0002)
      0046A9 E0               [24] 3209 	movx	a,@dptr
      0046AA FE               [12] 3210 	mov	r6,a
      0046AB C3               [12] 3211 	clr	c
      0046AC 9F               [12] 3212 	subb	a,r7
      0046AD 50 12            [24] 3213 	jnc	00111$
      0046AF 75 6E A2         [24] 3214 	mov	_swap_variables_PARM_2,#(_randomSet + 0x0002)
      0046B2 75 6F 02         [24] 3215 	mov	(_swap_variables_PARM_2 + 1),#((_randomSet + 0x0002) >> 8)
      0046B5 75 70 00         [24] 3216 	mov	(_swap_variables_PARM_2 + 2),#0x00
      0046B8 90 02 A1         [24] 3217 	mov	dptr,#(_randomSet + 0x0001)
      0046BB 75 F0 00         [24] 3218 	mov	b,#0x00
      0046BE 12 45 9E         [24] 3219 	lcall	_swap_variables
      0046C1                       3220 00111$:
                           000123  3221 	C$pktframing.c$104$1$373 ==.
                                   3222 ;	..\COMMON\pktframing.c:104: if (randomSet[0] > randomSet[1]) swap_variables(&randomSet[0], &randomSet[1]);
      0046C1 90 02 A0         [24] 3223 	mov	dptr,#_randomSet
      0046C4 E0               [24] 3224 	movx	a,@dptr
      0046C5 FF               [12] 3225 	mov	r7,a
      0046C6 90 02 A1         [24] 3226 	mov	dptr,#(_randomSet + 0x0001)
      0046C9 E0               [24] 3227 	movx	a,@dptr
      0046CA FE               [12] 3228 	mov	r6,a
      0046CB C3               [12] 3229 	clr	c
      0046CC 9F               [12] 3230 	subb	a,r7
      0046CD 50 12            [24] 3231 	jnc	00113$
      0046CF 75 6E A1         [24] 3232 	mov	_swap_variables_PARM_2,#(_randomSet + 0x0001)
      0046D2 75 6F 02         [24] 3233 	mov	(_swap_variables_PARM_2 + 1),#((_randomSet + 0x0001) >> 8)
      0046D5 75 70 00         [24] 3234 	mov	(_swap_variables_PARM_2 + 2),#0x00
      0046D8 90 02 A0         [24] 3235 	mov	dptr,#_randomSet
      0046DB 75 F0 00         [24] 3236 	mov	b,#0x00
      0046DE 12 45 9E         [24] 3237 	lcall	_swap_variables
      0046E1                       3238 00113$:
                           000143  3239 	C$pktframing.c$107$1$373 ==.
                                   3240 ;	..\COMMON\pktframing.c:107: if (StdPrem == 0x2)
      0046E1 90 09 B6         [24] 3241 	mov	dptr,#_StdPrem
      0046E4 E0               [24] 3242 	movx	a,@dptr
      0046E5 FF               [12] 3243 	mov	r7,a
      0046E6 BF 02 0C         [24] 3244 	cjne	r7,#0x02,00115$
                           00014B  3245 	C$pktframing.c$109$2$376 ==.
                                   3246 ;	..\COMMON\pktframing.c:109: randomSet[0] = 0 + slotStdBASE;
      0046E9 90 02 A0         [24] 3247 	mov	dptr,#_randomSet
      0046EC 74 1C            [12] 3248 	mov	a,#0x1c
      0046EE F0               [24] 3249 	movx	@dptr,a
                           000151  3250 	C$pktframing.c$110$2$376 ==.
                                   3251 ;	..\COMMON\pktframing.c:110: randomSet[2] = 29 + slotStdBASE;
      0046EF 90 02 A2         [24] 3252 	mov	dptr,#(_randomSet + 0x0002)
      0046F2 74 39            [12] 3253 	mov	a,#0x39
      0046F4 F0               [24] 3254 	movx	@dptr,a
      0046F5                       3255 00115$:
                           000157  3256 	C$pktframing.c$114$1$373 ==.
                                   3257 ;	..\COMMON\pktframing.c:114: dbglink_writestr("3_RNG_WT0: ");
      0046F5 90 80 EF         [24] 3258 	mov	dptr,#___str_0
      0046F8 75 F0 80         [24] 3259 	mov	b,#0x80
      0046FB 12 73 05         [24] 3260 	lcall	_dbglink_writestr
                           000160  3261 	C$pktframing.c$115$1$373 ==.
                                   3262 ;	..\COMMON\pktframing.c:115: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      0046FE 12 7D AD         [24] 3263 	lcall	_wtimer0_curtime
      004701 AC 82            [24] 3264 	mov	r4,dpl
      004703 AD 83            [24] 3265 	mov	r5,dph
      004705 AE F0            [24] 3266 	mov	r6,b
      004707 FF               [12] 3267 	mov	r7,a
      004708 74 08            [12] 3268 	mov	a,#0x08
      00470A C0 E0            [24] 3269 	push	acc
      00470C C0 E0            [24] 3270 	push	acc
      00470E 8C 82            [24] 3271 	mov	dpl,r4
      004710 8D 83            [24] 3272 	mov	dph,r5
      004712 8E F0            [24] 3273 	mov	b,r6
      004714 EF               [12] 3274 	mov	a,r7
      004715 12 75 88         [24] 3275 	lcall	_dbglink_writehex32
      004718 15 81            [12] 3276 	dec	sp
      00471A 15 81            [12] 3277 	dec	sp
                           00017E  3278 	C$pktframing.c$116$1$373 ==.
                                   3279 ;	..\COMMON\pktframing.c:116: dbglink_tx('\n');
      00471C 75 82 0A         [24] 3280 	mov	dpl,#0x0a
      00471F 12 61 21         [24] 3281 	lcall	_dbglink_tx
                           000184  3282 	C$pktframing.c$118$1$373 ==.
                           000184  3283 	XG$randomset_generate$0$0 ==.
      004722 22               [24] 3284 	ret
                                   3285 ;------------------------------------------------------------
                                   3286 ;Allocation info for local variables in function 'packetPremFraming'
                                   3287 ;------------------------------------------------------------
                                   3288 ;i                         Allocated with name '_packetPremFraming_i_1_378'
                                   3289 ;j                         Allocated with name '_packetPremFraming_j_1_378'
                                   3290 ;n                         Allocated with name '_packetPremFraming_n_1_378'
                                   3291 ;goldseq                   Allocated with name '_packetPremFraming_goldseq_1_378'
                                   3292 ;sib0                      Allocated with name '_packetPremFraming_sib0_1_378'
                                   3293 ;sib1                      Allocated with name '_packetPremFraming_sib1_1_378'
                                   3294 ;CRC32                     Allocated with name '_packetPremFraming_CRC32_1_378'
                                   3295 ;------------------------------------------------------------
                           000185  3296 	G$packetPremFraming$0$0 ==.
                           000185  3297 	C$pktframing.c$129$1$373 ==.
                                   3298 ;	..\COMMON\pktframing.c:129: void packetPremFraming(void)
                                   3299 ;	-----------------------------------------
                                   3300 ;	 function packetPremFraming
                                   3301 ;	-----------------------------------------
      004723                       3302 _packetPremFraming:
                           000185  3303 	C$pktframing.c$138$1$378 ==.
                                   3304 ;	..\COMMON\pktframing.c:138: goldseq = GOLDSEQUENCE_Obtain();
      004723 12 42 A2         [24] 3305 	lcall	_GOLDSEQUENCE_Obtain
      004726 AC 82            [24] 3306 	mov	r4,dpl
      004728 AD 83            [24] 3307 	mov	r5,dph
      00472A AE F0            [24] 3308 	mov	r6,b
      00472C FF               [12] 3309 	mov	r7,a
                           00018F  3310 	C$pktframing.c$141$1$378 ==.
                                   3311 ;	..\COMMON\pktframing.c:141: dbglink_writestr("2_Gold_WT0:");
      00472D 90 80 FB         [24] 3312 	mov	dptr,#___str_1
      004730 75 F0 80         [24] 3313 	mov	b,#0x80
      004733 C0 07            [24] 3314 	push	ar7
      004735 C0 06            [24] 3315 	push	ar6
      004737 C0 05            [24] 3316 	push	ar5
      004739 C0 04            [24] 3317 	push	ar4
      00473B 12 73 05         [24] 3318 	lcall	_dbglink_writestr
                           0001A0  3319 	C$pktframing.c$142$1$378 ==.
                                   3320 ;	..\COMMON\pktframing.c:142: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      00473E 12 7D AD         [24] 3321 	lcall	_wtimer0_curtime
      004741 A8 82            [24] 3322 	mov	r0,dpl
      004743 A9 83            [24] 3323 	mov	r1,dph
      004745 AA F0            [24] 3324 	mov	r2,b
      004747 FB               [12] 3325 	mov	r3,a
      004748 74 08            [12] 3326 	mov	a,#0x08
      00474A C0 E0            [24] 3327 	push	acc
      00474C C0 E0            [24] 3328 	push	acc
      00474E 88 82            [24] 3329 	mov	dpl,r0
      004750 89 83            [24] 3330 	mov	dph,r1
      004752 8A F0            [24] 3331 	mov	b,r2
      004754 EB               [12] 3332 	mov	a,r3
      004755 12 75 88         [24] 3333 	lcall	_dbglink_writehex32
      004758 15 81            [12] 3334 	dec	sp
      00475A 15 81            [12] 3335 	dec	sp
                           0001BE  3336 	C$pktframing.c$143$1$378 ==.
                                   3337 ;	..\COMMON\pktframing.c:143: dbglink_tx('\n');
      00475C 75 82 0A         [24] 3338 	mov	dpl,#0x0a
      00475F 12 61 21         [24] 3339 	lcall	_dbglink_tx
                           0001C4  3340 	C$pktframing.c$146$1$378 ==.
                                   3341 ;	..\COMMON\pktframing.c:146: randomset_generate();
      004762 12 45 E6         [24] 3342 	lcall	_randomset_generate
      004765 D0 04            [24] 3343 	pop	ar4
      004767 D0 05            [24] 3344 	pop	ar5
      004769 D0 06            [24] 3345 	pop	ar6
      00476B D0 07            [24] 3346 	pop	ar7
                           0001CF  3347 	C$pktframing.c$211$1$378 ==.
                                   3348 ;	..\COMMON\pktframing.c:211: *(premPacketBuffer) = (uint8_t) (goldseq & 0x000000FF);
      00476D 8C 00            [24] 3349 	mov	ar0,r4
      00476F 90 03 2C         [24] 3350 	mov	dptr,#_premPacketBuffer
      004772 E8               [12] 3351 	mov	a,r0
      004773 F0               [24] 3352 	movx	@dptr,a
                           0001D6  3353 	C$pktframing.c$212$1$378 ==.
                                   3354 ;	..\COMMON\pktframing.c:212: *(premPacketBuffer + 1) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
      004774 8D 01            [24] 3355 	mov	ar1,r5
      004776 89 00            [24] 3356 	mov	ar0,r1
      004778 90 03 2D         [24] 3357 	mov	dptr,#(_premPacketBuffer + 0x0001)
      00477B E8               [12] 3358 	mov	a,r0
      00477C F0               [24] 3359 	movx	@dptr,a
                           0001DF  3360 	C$pktframing.c$213$1$378 ==.
                                   3361 ;	..\COMMON\pktframing.c:213: *(premPacketBuffer + 2) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
      00477D 8E 02            [24] 3362 	mov	ar2,r6
      00477F 8A 00            [24] 3363 	mov	ar0,r2
      004781 90 03 2E         [24] 3364 	mov	dptr,#(_premPacketBuffer + 0x0002)
      004784 E8               [12] 3365 	mov	a,r0
      004785 F0               [24] 3366 	movx	@dptr,a
                           0001E8  3367 	C$pktframing.c$214$1$378 ==.
                                   3368 ;	..\COMMON\pktframing.c:214: *(premPacketBuffer + 3) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (StdPrem&0x0001) <<7);
      004786 53 07 7F         [24] 3369 	anl	ar7,#0x7f
      004789 8F 04            [24] 3370 	mov	ar4,r7
      00478B 90 09 B6         [24] 3371 	mov	dptr,#_StdPrem
      00478E E0               [24] 3372 	movx	a,@dptr
      00478F 54 01            [12] 3373 	anl	a,#0x01
      004791 03               [12] 3374 	rr	a
      004792 54 80            [12] 3375 	anl	a,#0x80
      004794 42 04            [12] 3376 	orl	ar4,a
      004796 90 03 2F         [24] 3377 	mov	dptr,#(_premPacketBuffer + 0x0003)
      004799 EC               [12] 3378 	mov	a,r4
      00479A F0               [24] 3379 	movx	@dptr,a
                           0001FD  3380 	C$pktframing.c$215$1$378 ==.
                                   3381 ;	..\COMMON\pktframing.c:215: *(premPacketBuffer + 4) = (uint8_t) ((sib0 & 0x3F) | ((sib1 & 0x03)<<6));
      00479B 90 03 30         [24] 3382 	mov	dptr,#(_premPacketBuffer + 0x0004)
      00479E 74 FF            [12] 3383 	mov	a,#0xff
      0047A0 F0               [24] 3384 	movx	@dptr,a
                           000203  3385 	C$pktframing.c$216$1$378 ==.
                                   3386 ;	..\COMMON\pktframing.c:216: *(premPacketBuffer + 5) = (uint8_t) (((sib1 & 0x3C) >> 2) | (trmID & 0x00000F)<<4);
      0047A1 90 09 B2         [24] 3387 	mov	dptr,#_trmID
      0047A4 E0               [24] 3388 	movx	a,@dptr
      0047A5 FC               [12] 3389 	mov	r4,a
      0047A6 A3               [24] 3390 	inc	dptr
      0047A7 E0               [24] 3391 	movx	a,@dptr
      0047A8 A3               [24] 3392 	inc	dptr
      0047A9 E0               [24] 3393 	movx	a,@dptr
      0047AA A3               [24] 3394 	inc	dptr
      0047AB E0               [24] 3395 	movx	a,@dptr
      0047AC 53 04 0F         [24] 3396 	anl	ar4,#0x0f
      0047AF E4               [12] 3397 	clr	a
      0047B0 EC               [12] 3398 	mov	a,r4
      0047B1 C4               [12] 3399 	swap	a
      0047B2 54 F0            [12] 3400 	anl	a,#0xf0
      0047B4 FC               [12] 3401 	mov	r4,a
      0047B5 43 04 0F         [24] 3402 	orl	ar4,#0x0f
      0047B8 90 03 31         [24] 3403 	mov	dptr,#(_premPacketBuffer + 0x0005)
      0047BB EC               [12] 3404 	mov	a,r4
      0047BC F0               [24] 3405 	movx	@dptr,a
                           00021F  3406 	C$pktframing.c$217$1$378 ==.
                                   3407 ;	..\COMMON\pktframing.c:217: *(premPacketBuffer + 6) = (uint8_t) ((trmID & 0x000FF0) >> 4);
      0047BD 90 09 B2         [24] 3408 	mov	dptr,#_trmID
      0047C0 E0               [24] 3409 	movx	a,@dptr
      0047C1 FC               [12] 3410 	mov	r4,a
      0047C2 A3               [24] 3411 	inc	dptr
      0047C3 E0               [24] 3412 	movx	a,@dptr
      0047C4 FD               [12] 3413 	mov	r5,a
      0047C5 A3               [24] 3414 	inc	dptr
      0047C6 E0               [24] 3415 	movx	a,@dptr
      0047C7 A3               [24] 3416 	inc	dptr
      0047C8 E0               [24] 3417 	movx	a,@dptr
      0047C9 53 04 F0         [24] 3418 	anl	ar4,#0xf0
      0047CC 53 05 0F         [24] 3419 	anl	ar5,#0x0f
      0047CF E4               [12] 3420 	clr	a
      0047D0 FE               [12] 3421 	mov	r6,a
      0047D1 FF               [12] 3422 	mov	r7,a
      0047D2 ED               [12] 3423 	mov	a,r5
      0047D3 C4               [12] 3424 	swap	a
      0047D4 CC               [12] 3425 	xch	a,r4
      0047D5 C4               [12] 3426 	swap	a
      0047D6 54 0F            [12] 3427 	anl	a,#0x0f
      0047D8 6C               [12] 3428 	xrl	a,r4
      0047D9 CC               [12] 3429 	xch	a,r4
      0047DA 54 0F            [12] 3430 	anl	a,#0x0f
      0047DC CC               [12] 3431 	xch	a,r4
      0047DD 6C               [12] 3432 	xrl	a,r4
      0047DE CC               [12] 3433 	xch	a,r4
      0047DF FD               [12] 3434 	mov	r5,a
      0047E0 EE               [12] 3435 	mov	a,r6
      0047E1 C4               [12] 3436 	swap	a
      0047E2 54 F0            [12] 3437 	anl	a,#0xf0
      0047E4 4D               [12] 3438 	orl	a,r5
      0047E5 EF               [12] 3439 	mov	a,r7
      0047E6 C4               [12] 3440 	swap	a
      0047E7 CE               [12] 3441 	xch	a,r6
      0047E8 C4               [12] 3442 	swap	a
      0047E9 54 0F            [12] 3443 	anl	a,#0x0f
      0047EB 6E               [12] 3444 	xrl	a,r6
      0047EC CE               [12] 3445 	xch	a,r6
      0047ED 54 0F            [12] 3446 	anl	a,#0x0f
      0047EF CE               [12] 3447 	xch	a,r6
      0047F0 6E               [12] 3448 	xrl	a,r6
      0047F1 CE               [12] 3449 	xch	a,r6
      0047F2 90 03 32         [24] 3450 	mov	dptr,#(_premPacketBuffer + 0x0006)
      0047F5 EC               [12] 3451 	mov	a,r4
      0047F6 F0               [24] 3452 	movx	@dptr,a
                           000259  3453 	C$pktframing.c$218$1$378 ==.
                                   3454 ;	..\COMMON\pktframing.c:218: *(premPacketBuffer + 7) = (uint8_t) ((trmID & 0x0FF000) >> 12);
      0047F7 90 09 B2         [24] 3455 	mov	dptr,#_trmID
      0047FA E0               [24] 3456 	movx	a,@dptr
      0047FB A3               [24] 3457 	inc	dptr
      0047FC E0               [24] 3458 	movx	a,@dptr
      0047FD FD               [12] 3459 	mov	r5,a
      0047FE A3               [24] 3460 	inc	dptr
      0047FF E0               [24] 3461 	movx	a,@dptr
      004800 FE               [12] 3462 	mov	r6,a
      004801 A3               [24] 3463 	inc	dptr
      004802 E0               [24] 3464 	movx	a,@dptr
      004803 53 05 F0         [24] 3465 	anl	ar5,#0xf0
      004806 53 06 0F         [24] 3466 	anl	ar6,#0x0f
      004809 7F 00            [12] 3467 	mov	r7,#0x00
      00480B 8D 04            [24] 3468 	mov	ar4,r5
      00480D EE               [12] 3469 	mov	a,r6
      00480E C4               [12] 3470 	swap	a
      00480F CC               [12] 3471 	xch	a,r4
      004810 C4               [12] 3472 	swap	a
      004811 54 0F            [12] 3473 	anl	a,#0x0f
      004813 6C               [12] 3474 	xrl	a,r4
      004814 CC               [12] 3475 	xch	a,r4
      004815 54 0F            [12] 3476 	anl	a,#0x0f
      004817 CC               [12] 3477 	xch	a,r4
      004818 6C               [12] 3478 	xrl	a,r4
      004819 CC               [12] 3479 	xch	a,r4
      00481A FD               [12] 3480 	mov	r5,a
      00481B EF               [12] 3481 	mov	a,r7
      00481C C4               [12] 3482 	swap	a
      00481D 54 F0            [12] 3483 	anl	a,#0xf0
      00481F 4D               [12] 3484 	orl	a,r5
      004820 EF               [12] 3485 	mov	a,r7
      004821 C4               [12] 3486 	swap	a
      004822 54 0F            [12] 3487 	anl	a,#0x0f
      004824 90 03 33         [24] 3488 	mov	dptr,#(_premPacketBuffer + 0x0007)
      004827 EC               [12] 3489 	mov	a,r4
      004828 F0               [24] 3490 	movx	@dptr,a
                           00028B  3491 	C$pktframing.c$219$1$378 ==.
                                   3492 ;	..\COMMON\pktframing.c:219: *(premPacketBuffer + 8) = (uint8_t) (((trmID & 0xF00000) >> 20) | (ack_flg << 4));
      004829 90 09 B2         [24] 3493 	mov	dptr,#_trmID
      00482C E0               [24] 3494 	movx	a,@dptr
      00482D A3               [24] 3495 	inc	dptr
      00482E E0               [24] 3496 	movx	a,@dptr
      00482F A3               [24] 3497 	inc	dptr
      004830 E0               [24] 3498 	movx	a,@dptr
      004831 FE               [12] 3499 	mov	r6,a
      004832 A3               [24] 3500 	inc	dptr
      004833 E0               [24] 3501 	movx	a,@dptr
      004834 53 06 F0         [24] 3502 	anl	ar6,#0xf0
      004837 7F 00            [12] 3503 	mov	r7,#0x00
      004839 8E 04            [24] 3504 	mov	ar4,r6
      00483B EF               [12] 3505 	mov	a,r7
      00483C C4               [12] 3506 	swap	a
      00483D CC               [12] 3507 	xch	a,r4
      00483E C4               [12] 3508 	swap	a
      00483F 54 0F            [12] 3509 	anl	a,#0x0f
      004841 6C               [12] 3510 	xrl	a,r4
      004842 CC               [12] 3511 	xch	a,r4
      004843 54 0F            [12] 3512 	anl	a,#0x0f
      004845 CC               [12] 3513 	xch	a,r4
      004846 6C               [12] 3514 	xrl	a,r4
      004847 CC               [12] 3515 	xch	a,r4
      004848 FD               [12] 3516 	mov	r5,a
      004849 7E 00            [12] 3517 	mov	r6,#0x00
      00484B 7F 00            [12] 3518 	mov	r7,#0x00
      00484D 90 09 CC         [24] 3519 	mov	dptr,#_ack_flg
      004850 E0               [24] 3520 	movx	a,@dptr
      004851 C4               [12] 3521 	swap	a
      004852 54 F0            [12] 3522 	anl	a,#0xf0
      004854 F8               [12] 3523 	mov	r0,a
      004855 E4               [12] 3524 	clr	a
      004856 F9               [12] 3525 	mov	r1,a
      004857 FA               [12] 3526 	mov	r2,a
      004858 FB               [12] 3527 	mov	r3,a
      004859 E8               [12] 3528 	mov	a,r0
      00485A 42 04            [12] 3529 	orl	ar4,a
      00485C E9               [12] 3530 	mov	a,r1
      00485D 42 05            [12] 3531 	orl	ar5,a
      00485F EA               [12] 3532 	mov	a,r2
      004860 42 06            [12] 3533 	orl	ar6,a
      004862 EB               [12] 3534 	mov	a,r3
      004863 42 07            [12] 3535 	orl	ar7,a
      004865 90 03 34         [24] 3536 	mov	dptr,#(_premPacketBuffer + 0x0008)
      004868 EC               [12] 3537 	mov	a,r4
      004869 F0               [24] 3538 	movx	@dptr,a
                           0002CC  3539 	C$pktframing.c$222$1$378 ==.
                                   3540 ;	..\COMMON\pktframing.c:222: memcpy((premPacketBuffer + 9), &dummyHMAC, sizeof(uint32_t));
      00486A 75 6E AE         [24] 3541 	mov	_memcpy_PARM_2,#_dummyHMAC
      00486D 75 6F 09         [24] 3542 	mov	(_memcpy_PARM_2 + 1),#(_dummyHMAC >> 8)
      004870 75 70 00         [24] 3543 	mov	(_memcpy_PARM_2 + 2),#0x00
      004873 75 71 04         [24] 3544 	mov	_memcpy_PARM_3,#0x04
      004876 75 72 00         [24] 3545 	mov	(_memcpy_PARM_3 + 1),#0x00
      004879 90 03 35         [24] 3546 	mov	dptr,#(_premPacketBuffer + 0x0009)
      00487C 75 F0 00         [24] 3547 	mov	b,#0x00
      00487F 12 69 78         [24] 3548 	lcall	_memcpy
                           0002E4  3549 	C$pktframing.c$223$1$378 ==.
                                   3550 ;	..\COMMON\pktframing.c:223: memcpy((premPacketBuffer + 13), payload, sizeof(uint8_t)*payload_len);
      004882 75 6E D6         [24] 3551 	mov	_memcpy_PARM_2,#_payload
      004885 75 6F 03         [24] 3552 	mov	(_memcpy_PARM_2 + 1),#(_payload >> 8)
      004888 75 70 00         [24] 3553 	mov	(_memcpy_PARM_2 + 2),#0x00
      00488B 90 03 F2         [24] 3554 	mov	dptr,#_payload_len
      00488E E0               [24] 3555 	movx	a,@dptr
      00488F F5 71            [12] 3556 	mov	_memcpy_PARM_3,a
      004891 A3               [24] 3557 	inc	dptr
      004892 E0               [24] 3558 	movx	a,@dptr
      004893 F5 72            [12] 3559 	mov	(_memcpy_PARM_3 + 1),a
      004895 90 03 39         [24] 3560 	mov	dptr,#(_premPacketBuffer + 0x000d)
      004898 75 F0 00         [24] 3561 	mov	b,#0x00
      00489B 12 69 78         [24] 3562 	lcall	_memcpy
                           000300  3563 	C$pktframing.c$225$1$378 ==.
                                   3564 ;	..\COMMON\pktframing.c:225: CRC32 = crc_crc32_msb(premPacketBuffer, (13+payload_len), 0xFFFFFFFF);
      00489E 90 03 F2         [24] 3565 	mov	dptr,#_payload_len
      0048A1 E0               [24] 3566 	movx	a,@dptr
      0048A2 FE               [12] 3567 	mov	r6,a
      0048A3 A3               [24] 3568 	inc	dptr
      0048A4 E0               [24] 3569 	movx	a,@dptr
      0048A5 FF               [12] 3570 	mov	r7,a
      0048A6 74 0D            [12] 3571 	mov	a,#0x0d
      0048A8 2E               [12] 3572 	add	a,r6
      0048A9 FE               [12] 3573 	mov	r6,a
      0048AA E4               [12] 3574 	clr	a
      0048AB 3F               [12] 3575 	addc	a,r7
      0048AC FF               [12] 3576 	mov	r7,a
      0048AD 74 FF            [12] 3577 	mov	a,#0xff
      0048AF C0 E0            [24] 3578 	push	acc
      0048B1 C0 E0            [24] 3579 	push	acc
      0048B3 C0 E0            [24] 3580 	push	acc
      0048B5 C0 E0            [24] 3581 	push	acc
      0048B7 C0 06            [24] 3582 	push	ar6
      0048B9 C0 07            [24] 3583 	push	ar7
      0048BB 90 03 2C         [24] 3584 	mov	dptr,#_premPacketBuffer
      0048BE 75 F0 00         [24] 3585 	mov	b,#0x00
      0048C1 12 79 5C         [24] 3586 	lcall	_crc_crc32_msb
      0048C4 AC 82            [24] 3587 	mov	r4,dpl
      0048C6 AD 83            [24] 3588 	mov	r5,dph
      0048C8 AE F0            [24] 3589 	mov	r6,b
      0048CA FF               [12] 3590 	mov	r7,a
      0048CB E5 81            [12] 3591 	mov	a,sp
      0048CD 24 FA            [12] 3592 	add	a,#0xfa
      0048CF F5 81            [12] 3593 	mov	sp,a
      0048D1 90 03 71         [24] 3594 	mov	dptr,#_packetPremFraming_CRC32_1_378
      0048D4 EC               [12] 3595 	mov	a,r4
      0048D5 F0               [24] 3596 	movx	@dptr,a
      0048D6 ED               [12] 3597 	mov	a,r5
      0048D7 A3               [24] 3598 	inc	dptr
      0048D8 F0               [24] 3599 	movx	@dptr,a
      0048D9 EE               [12] 3600 	mov	a,r6
      0048DA A3               [24] 3601 	inc	dptr
      0048DB F0               [24] 3602 	movx	@dptr,a
      0048DC EF               [12] 3603 	mov	a,r7
      0048DD A3               [24] 3604 	inc	dptr
      0048DE F0               [24] 3605 	movx	@dptr,a
                           000341  3606 	C$pktframing.c$226$1$378 ==.
                                   3607 ;	..\COMMON\pktframing.c:226: memcpy((premPacketBuffer+13+payload_len), &CRC32, sizeof(uint32_t));
      0048DF 90 03 F2         [24] 3608 	mov	dptr,#_payload_len
      0048E2 E0               [24] 3609 	movx	a,@dptr
      0048E3 FE               [12] 3610 	mov	r6,a
      0048E4 A3               [24] 3611 	inc	dptr
      0048E5 E0               [24] 3612 	movx	a,@dptr
      0048E6 FF               [12] 3613 	mov	r7,a
      0048E7 EE               [12] 3614 	mov	a,r6
      0048E8 24 39            [12] 3615 	add	a,#(_premPacketBuffer + 0x000d)
      0048EA FE               [12] 3616 	mov	r6,a
      0048EB EF               [12] 3617 	mov	a,r7
      0048EC 34 03            [12] 3618 	addc	a,#((_premPacketBuffer + 0x000d) >> 8)
      0048EE FF               [12] 3619 	mov	r7,a
      0048EF 7D 00            [12] 3620 	mov	r5,#0x00
      0048F1 75 6E 71         [24] 3621 	mov	_memcpy_PARM_2,#_packetPremFraming_CRC32_1_378
      0048F4 75 6F 03         [24] 3622 	mov	(_memcpy_PARM_2 + 1),#(_packetPremFraming_CRC32_1_378 >> 8)
                                   3623 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      0048F7 8D 70            [24] 3624 	mov	(_memcpy_PARM_2 + 2),r5
      0048F9 75 71 04         [24] 3625 	mov	_memcpy_PARM_3,#0x04
                                   3626 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_3 + 1),#0x00
      0048FC 8D 72            [24] 3627 	mov	(_memcpy_PARM_3 + 1),r5
      0048FE 8E 82            [24] 3628 	mov	dpl,r6
      004900 8F 83            [24] 3629 	mov	dph,r7
      004902 8D F0            [24] 3630 	mov	b,r5
      004904 12 69 78         [24] 3631 	lcall	_memcpy
                           000369  3632 	C$pktframing.c$230$1$378 ==.
                                   3633 ;	..\COMMON\pktframing.c:230: ack_flg = 0; // clear the flag after a new series of packets have been prepared.
      004907 90 09 CC         [24] 3634 	mov	dptr,#_ack_flg
      00490A E4               [12] 3635 	clr	a
      00490B F0               [24] 3636 	movx	@dptr,a
                           00036E  3637 	C$pktframing.c$232$1$378 ==.
                           00036E  3638 	XG$packetPremFraming$0$0 ==.
      00490C 22               [24] 3639 	ret
                                   3640 ;------------------------------------------------------------
                                   3641 ;Allocation info for local variables in function 'packetStdFraming'
                                   3642 ;------------------------------------------------------------
                                   3643 ;sloc0                     Allocated with name '_packetStdFraming_sloc0_1_0'
                                   3644 ;sloc1                     Allocated with name '_packetStdFraming_sloc1_1_0'
                                   3645 ;sloc2                     Allocated with name '_packetStdFraming_sloc2_1_0'
                                   3646 ;sloc3                     Allocated with name '_packetStdFraming_sloc3_1_0'
                                   3647 ;sloc4                     Allocated with name '_packetStdFraming_sloc4_1_0'
                                   3648 ;sloc5                     Allocated with name '_packetStdFraming_sloc5_1_0'
                                   3649 ;sloc6                     Allocated with name '_packetStdFraming_sloc6_1_0'
                                   3650 ;sloc7                     Allocated with name '_packetStdFraming_sloc7_1_0'
                                   3651 ;sloc8                     Allocated with name '_packetStdFraming_sloc8_1_0'
                                   3652 ;i                         Allocated with name '_packetStdFraming_i_1_380'
                                   3653 ;j                         Allocated with name '_packetStdFraming_j_1_380'
                                   3654 ;n                         Allocated with name '_packetStdFraming_n_1_380'
                                   3655 ;goldseq                   Allocated with name '_packetStdFraming_goldseq_1_380'
                                   3656 ;sib0                      Allocated with name '_packetStdFraming_sib0_1_380'
                                   3657 ;sib1                      Allocated with name '_packetStdFraming_sib1_1_380'
                                   3658 ;CRC32                     Allocated with name '_packetStdFraming_CRC32_1_380'
                                   3659 ;------------------------------------------------------------
                           00036F  3660 	G$packetStdFraming$0$0 ==.
                           00036F  3661 	C$pktframing.c$243$1$378 ==.
                                   3662 ;	..\COMMON\pktframing.c:243: void packetStdFraming(void)
                                   3663 ;	-----------------------------------------
                                   3664 ;	 function packetStdFraming
                                   3665 ;	-----------------------------------------
      00490D                       3666 _packetStdFraming:
                           00036F  3667 	C$pktframing.c$248$1$378 ==.
                                   3668 ;	..\COMMON\pktframing.c:248: uint8_t __xdata sib0 = 0, sib1 = 0;
      00490D 90 03 75         [24] 3669 	mov	dptr,#_packetStdFraming_sib0_1_380
      004910 E4               [12] 3670 	clr	a
      004911 F0               [24] 3671 	movx	@dptr,a
      004912 90 03 76         [24] 3672 	mov	dptr,#_packetStdFraming_sib1_1_380
      004915 F0               [24] 3673 	movx	@dptr,a
                           000378  3674 	C$pktframing.c$253$1$380 ==.
                                   3675 ;	..\COMMON\pktframing.c:253: goldseq = GOLDSEQUENCE_Obtain();
      004916 12 42 A2         [24] 3676 	lcall	_GOLDSEQUENCE_Obtain
      004919 AC 82            [24] 3677 	mov	r4,dpl
      00491B AD 83            [24] 3678 	mov	r5,dph
      00491D AE F0            [24] 3679 	mov	r6,b
      00491F FF               [12] 3680 	mov	r7,a
                           000382  3681 	C$pktframing.c$256$1$380 ==.
                                   3682 ;	..\COMMON\pktframing.c:256: dbglink_writestr("2_Gold_WT0:");
      004920 90 80 FB         [24] 3683 	mov	dptr,#___str_1
      004923 75 F0 80         [24] 3684 	mov	b,#0x80
      004926 C0 07            [24] 3685 	push	ar7
      004928 C0 06            [24] 3686 	push	ar6
      00492A C0 05            [24] 3687 	push	ar5
      00492C C0 04            [24] 3688 	push	ar4
      00492E 12 73 05         [24] 3689 	lcall	_dbglink_writestr
                           000393  3690 	C$pktframing.c$257$1$380 ==.
                                   3691 ;	..\COMMON\pktframing.c:257: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004931 12 7D AD         [24] 3692 	lcall	_wtimer0_curtime
      004934 A8 82            [24] 3693 	mov	r0,dpl
      004936 A9 83            [24] 3694 	mov	r1,dph
      004938 AA F0            [24] 3695 	mov	r2,b
      00493A FB               [12] 3696 	mov	r3,a
      00493B 74 08            [12] 3697 	mov	a,#0x08
      00493D C0 E0            [24] 3698 	push	acc
      00493F C0 E0            [24] 3699 	push	acc
      004941 88 82            [24] 3700 	mov	dpl,r0
      004943 89 83            [24] 3701 	mov	dph,r1
      004945 8A F0            [24] 3702 	mov	b,r2
      004947 EB               [12] 3703 	mov	a,r3
      004948 12 75 88         [24] 3704 	lcall	_dbglink_writehex32
      00494B 15 81            [12] 3705 	dec	sp
      00494D 15 81            [12] 3706 	dec	sp
                           0003B1  3707 	C$pktframing.c$258$1$380 ==.
                                   3708 ;	..\COMMON\pktframing.c:258: dbglink_tx('\n');
      00494F 75 82 0A         [24] 3709 	mov	dpl,#0x0a
      004952 12 61 21         [24] 3710 	lcall	_dbglink_tx
                           0003B7  3711 	C$pktframing.c$261$1$380 ==.
                                   3712 ;	..\COMMON\pktframing.c:261: randomset_generate();
      004955 12 45 E6         [24] 3713 	lcall	_randomset_generate
      004958 D0 04            [24] 3714 	pop	ar4
      00495A D0 05            [24] 3715 	pop	ar5
      00495C D0 06            [24] 3716 	pop	ar6
      00495E D0 07            [24] 3717 	pop	ar7
                           0003C2  3718 	C$pktframing.c$283$3$382 ==.
                                   3719 ;	..\COMMON\pktframing.c:283: for (n = 0; n < 3; n++)
      004960 8C 22            [24] 3720 	mov	_packetStdFraming_sloc0_1_0,r4
      004962 75 23 00         [24] 3721 	mov	(_packetStdFraming_sloc0_1_0 + 1),#0x00
      004965 75 24 00         [24] 3722 	mov	(_packetStdFraming_sloc0_1_0 + 2),#0x00
      004968 75 25 00         [24] 3723 	mov	(_packetStdFraming_sloc0_1_0 + 3),#0x00
      00496B 8D 01            [24] 3724 	mov	ar1,r5
      00496D 89 26            [24] 3725 	mov	_packetStdFraming_sloc1_1_0,r1
      00496F 78 00            [12] 3726 	mov	r0,#0x00
      004971 79 00            [12] 3727 	mov	r1,#0x00
      004973 8E 02            [24] 3728 	mov	ar2,r6
      004975 8A 03            [24] 3729 	mov	ar3,r2
      004977 7C 00            [12] 3730 	mov	r4,#0x00
      004979 7D 00            [12] 3731 	mov	r5,#0x00
      00497B 53 07 7F         [24] 3732 	anl	ar7,#0x7f
      00497E 8F 27            [24] 3733 	mov	_packetStdFraming_sloc2_1_0,r7
      004980 7E 00            [12] 3734 	mov	r6,#0x00
      004982                       3735 00107$:
                           0003E4  3736 	C$pktframing.c$289$2$381 ==.
                                   3737 ;	..\COMMON\pktframing.c:289: switch(n)
      004982 BE 00 02         [24] 3738 	cjne	r6,#0x00,00127$
      004985 80 0A            [24] 3739 	sjmp	00101$
      004987                       3740 00127$:
      004987 BE 01 02         [24] 3741 	cjne	r6,#0x01,00128$
      00498A 80 18            [24] 3742 	sjmp	00102$
      00498C                       3743 00128$:
                           0003EE  3744 	C$pktframing.c$291$3$382 ==.
                                   3745 ;	..\COMMON\pktframing.c:291: case 0:
      00498C BE 02 39         [24] 3746 	cjne	r6,#0x02,00105$
      00498F 80 26            [24] 3747 	sjmp	00103$
      004991                       3748 00101$:
                           0003F3  3749 	C$pktframing.c$292$3$382 ==.
                                   3750 ;	..\COMMON\pktframing.c:292: sib0 = randomSet[1];
      004991 90 02 A1         [24] 3751 	mov	dptr,#(_randomSet + 0x0001)
      004994 E0               [24] 3752 	movx	a,@dptr
      004995 90 03 75         [24] 3753 	mov	dptr,#_packetStdFraming_sib0_1_380
      004998 F0               [24] 3754 	movx	@dptr,a
                           0003FB  3755 	C$pktframing.c$293$3$382 ==.
                                   3756 ;	..\COMMON\pktframing.c:293: sib1 = randomSet[2];
      004999 90 02 A2         [24] 3757 	mov	dptr,#(_randomSet + 0x0002)
      00499C E0               [24] 3758 	movx	a,@dptr
      00499D FD               [12] 3759 	mov	r5,a
      00499E 90 03 76         [24] 3760 	mov	dptr,#_packetStdFraming_sib1_1_380
      0049A1 F0               [24] 3761 	movx	@dptr,a
                           000404  3762 	C$pktframing.c$294$3$382 ==.
                                   3763 ;	..\COMMON\pktframing.c:294: break;
                           000404  3764 	C$pktframing.c$296$3$382 ==.
                                   3765 ;	..\COMMON\pktframing.c:296: case 1:
      0049A2 80 24            [24] 3766 	sjmp	00105$
      0049A4                       3767 00102$:
                           000406  3768 	C$pktframing.c$297$3$382 ==.
                                   3769 ;	..\COMMON\pktframing.c:297: sib0 = randomSet[0];
      0049A4 90 02 A0         [24] 3770 	mov	dptr,#_randomSet
      0049A7 E0               [24] 3771 	movx	a,@dptr
      0049A8 90 03 75         [24] 3772 	mov	dptr,#_packetStdFraming_sib0_1_380
      0049AB F0               [24] 3773 	movx	@dptr,a
                           00040E  3774 	C$pktframing.c$298$3$382 ==.
                                   3775 ;	..\COMMON\pktframing.c:298: sib1 = randomSet[2];
      0049AC 90 02 A2         [24] 3776 	mov	dptr,#(_randomSet + 0x0002)
      0049AF E0               [24] 3777 	movx	a,@dptr
      0049B0 FD               [12] 3778 	mov	r5,a
      0049B1 90 03 76         [24] 3779 	mov	dptr,#_packetStdFraming_sib1_1_380
      0049B4 F0               [24] 3780 	movx	@dptr,a
                           000417  3781 	C$pktframing.c$299$3$382 ==.
                                   3782 ;	..\COMMON\pktframing.c:299: break;
                           000417  3783 	C$pktframing.c$301$3$382 ==.
                                   3784 ;	..\COMMON\pktframing.c:301: case 2:
      0049B5 80 11            [24] 3785 	sjmp	00105$
      0049B7                       3786 00103$:
                           000419  3787 	C$pktframing.c$302$3$382 ==.
                                   3788 ;	..\COMMON\pktframing.c:302: sib0 = randomSet[0];
      0049B7 90 02 A0         [24] 3789 	mov	dptr,#_randomSet
      0049BA E0               [24] 3790 	movx	a,@dptr
      0049BB 90 03 75         [24] 3791 	mov	dptr,#_packetStdFraming_sib0_1_380
      0049BE F0               [24] 3792 	movx	@dptr,a
                           000421  3793 	C$pktframing.c$303$3$382 ==.
                                   3794 ;	..\COMMON\pktframing.c:303: sib1 = randomSet[1];
      0049BF 90 02 A1         [24] 3795 	mov	dptr,#(_randomSet + 0x0001)
      0049C2 E0               [24] 3796 	movx	a,@dptr
      0049C3 FD               [12] 3797 	mov	r5,a
      0049C4 90 03 76         [24] 3798 	mov	dptr,#_packetStdFraming_sib1_1_380
      0049C7 F0               [24] 3799 	movx	@dptr,a
                           00042A  3800 	C$pktframing.c$308$2$381 ==.
                                   3801 ;	..\COMMON\pktframing.c:308: }
      0049C8                       3802 00105$:
                           00042A  3803 	C$pktframing.c$326$2$381 ==.
                                   3804 ;	..\COMMON\pktframing.c:326: *(demoPacketBuffer + (n*pktLENGTH)) = (uint8_t) (goldseq & 0x000000FF);
      0049C8 EE               [12] 3805 	mov	a,r6
      0049C9 75 F0 2D         [24] 3806 	mov	b,#0x2d
      0049CC A4               [48] 3807 	mul	ab
      0049CD FC               [12] 3808 	mov	r4,a
      0049CE AD F0            [24] 3809 	mov	r5,b
      0049D0 24 A5            [12] 3810 	add	a,#_demoPacketBuffer
      0049D2 F5 2E            [12] 3811 	mov	_packetStdFraming_sloc7_1_0,a
      0049D4 ED               [12] 3812 	mov	a,r5
      0049D5 34 02            [12] 3813 	addc	a,#(_demoPacketBuffer >> 8)
      0049D7 F5 2F            [12] 3814 	mov	(_packetStdFraming_sloc7_1_0 + 1),a
      0049D9 A8 22            [24] 3815 	mov	r0,_packetStdFraming_sloc0_1_0
      0049DB 85 2E 82         [24] 3816 	mov	dpl,_packetStdFraming_sloc7_1_0
      0049DE 85 2F 83         [24] 3817 	mov	dph,(_packetStdFraming_sloc7_1_0 + 1)
      0049E1 E8               [12] 3818 	mov	a,r0
      0049E2 F0               [24] 3819 	movx	@dptr,a
                           000445  3820 	C$pktframing.c$327$2$381 ==.
                                   3821 ;	..\COMMON\pktframing.c:327: *(demoPacketBuffer + (n*pktLENGTH + 1)) = (uint8_t) ((goldseq & 0x0000FF00) >> 8);
      0049E3 74 01            [12] 3822 	mov	a,#0x01
      0049E5 2C               [12] 3823 	add	a,r4
      0049E6 F8               [12] 3824 	mov	r0,a
      0049E7 E4               [12] 3825 	clr	a
      0049E8 3D               [12] 3826 	addc	a,r5
      0049E9 FF               [12] 3827 	mov	r7,a
      0049EA E8               [12] 3828 	mov	a,r0
      0049EB 24 A5            [12] 3829 	add	a,#_demoPacketBuffer
      0049ED F5 82            [12] 3830 	mov	dpl,a
      0049EF EF               [12] 3831 	mov	a,r7
      0049F0 34 02            [12] 3832 	addc	a,#(_demoPacketBuffer >> 8)
      0049F2 F5 83            [12] 3833 	mov	dph,a
      0049F4 E5 26            [12] 3834 	mov	a,_packetStdFraming_sloc1_1_0
      0049F6 F0               [24] 3835 	movx	@dptr,a
                           000459  3836 	C$pktframing.c$328$2$381 ==.
                                   3837 ;	..\COMMON\pktframing.c:328: *(demoPacketBuffer + (n*pktLENGTH + 2)) = (uint8_t) ((goldseq & 0x00FF0000) >> 16);
      0049F7 74 02            [12] 3838 	mov	a,#0x02
      0049F9 2C               [12] 3839 	add	a,r4
      0049FA F8               [12] 3840 	mov	r0,a
      0049FB E4               [12] 3841 	clr	a
      0049FC 3D               [12] 3842 	addc	a,r5
      0049FD FF               [12] 3843 	mov	r7,a
      0049FE E8               [12] 3844 	mov	a,r0
      0049FF 24 A5            [12] 3845 	add	a,#_demoPacketBuffer
      004A01 F5 82            [12] 3846 	mov	dpl,a
      004A03 EF               [12] 3847 	mov	a,r7
      004A04 34 02            [12] 3848 	addc	a,#(_demoPacketBuffer >> 8)
      004A06 F5 83            [12] 3849 	mov	dph,a
      004A08 EB               [12] 3850 	mov	a,r3
      004A09 F0               [24] 3851 	movx	@dptr,a
                           00046C  3852 	C$pktframing.c$329$2$381 ==.
                                   3853 ;	..\COMMON\pktframing.c:329: *(demoPacketBuffer + (n*pktLENGTH + 3)) = (uint8_t) (((goldseq & 0x7F000000) >> 24) | (StdPrem&0x0001) <<7);
      004A0A 74 03            [12] 3854 	mov	a,#0x03
      004A0C 2C               [12] 3855 	add	a,r4
      004A0D F8               [12] 3856 	mov	r0,a
      004A0E E4               [12] 3857 	clr	a
      004A0F 3D               [12] 3858 	addc	a,r5
      004A10 FF               [12] 3859 	mov	r7,a
      004A11 E8               [12] 3860 	mov	a,r0
      004A12 24 A5            [12] 3861 	add	a,#_demoPacketBuffer
      004A14 F8               [12] 3862 	mov	r0,a
      004A15 EF               [12] 3863 	mov	a,r7
      004A16 34 02            [12] 3864 	addc	a,#(_demoPacketBuffer >> 8)
      004A18 FF               [12] 3865 	mov	r7,a
      004A19 C0 03            [24] 3866 	push	ar3
      004A1B 90 09 B6         [24] 3867 	mov	dptr,#_StdPrem
      004A1E E0               [24] 3868 	movx	a,@dptr
      004A1F 54 01            [12] 3869 	anl	a,#0x01
      004A21 03               [12] 3870 	rr	a
      004A22 54 80            [12] 3871 	anl	a,#0x80
      004A24 F5 28            [12] 3872 	mov	_packetStdFraming_sloc3_1_0,a
      004A26 85 27 29         [24] 3873 	mov	_packetStdFraming_sloc4_1_0,_packetStdFraming_sloc2_1_0
      004A29 AB 28            [24] 3874 	mov	r3,_packetStdFraming_sloc3_1_0
      004A2B E5 29            [12] 3875 	mov	a,_packetStdFraming_sloc4_1_0
      004A2D 42 03            [12] 3876 	orl	ar3,a
      004A2F 88 82            [24] 3877 	mov	dpl,r0
      004A31 8F 83            [24] 3878 	mov	dph,r7
      004A33 EB               [12] 3879 	mov	a,r3
      004A34 F0               [24] 3880 	movx	@dptr,a
                           000497  3881 	C$pktframing.c$330$2$381 ==.
                                   3882 ;	..\COMMON\pktframing.c:330: *(demoPacketBuffer + (n*pktLENGTH + 4)) = (uint8_t) ((sib0 & 0x3F) | ((sib1 & 0x03)<<6));
      004A35 74 04            [12] 3883 	mov	a,#0x04
      004A37 2C               [12] 3884 	add	a,r4
      004A38 FB               [12] 3885 	mov	r3,a
      004A39 E4               [12] 3886 	clr	a
      004A3A 3D               [12] 3887 	addc	a,r5
      004A3B FF               [12] 3888 	mov	r7,a
      004A3C EB               [12] 3889 	mov	a,r3
      004A3D 24 A5            [12] 3890 	add	a,#_demoPacketBuffer
      004A3F F5 2A            [12] 3891 	mov	_packetStdFraming_sloc5_1_0,a
      004A41 EF               [12] 3892 	mov	a,r7
      004A42 34 02            [12] 3893 	addc	a,#(_demoPacketBuffer >> 8)
      004A44 F5 2B            [12] 3894 	mov	(_packetStdFraming_sloc5_1_0 + 1),a
      004A46 90 03 75         [24] 3895 	mov	dptr,#_packetStdFraming_sib0_1_380
      004A49 E0               [24] 3896 	movx	a,@dptr
      004A4A F8               [12] 3897 	mov	r0,a
      004A4B 74 3F            [12] 3898 	mov	a,#0x3f
      004A4D 58               [12] 3899 	anl	a,r0
      004A4E F5 29            [12] 3900 	mov	_packetStdFraming_sloc4_1_0,a
      004A50 90 03 76         [24] 3901 	mov	dptr,#_packetStdFraming_sib1_1_380
      004A53 E0               [24] 3902 	movx	a,@dptr
      004A54 F8               [12] 3903 	mov	r0,a
      004A55 74 03            [12] 3904 	mov	a,#0x03
      004A57 58               [12] 3905 	anl	a,r0
      004A58 03               [12] 3906 	rr	a
      004A59 03               [12] 3907 	rr	a
      004A5A 54 C0            [12] 3908 	anl	a,#0xc0
      004A5C FF               [12] 3909 	mov	r7,a
      004A5D E5 29            [12] 3910 	mov	a,_packetStdFraming_sloc4_1_0
      004A5F 42 07            [12] 3911 	orl	ar7,a
      004A61 85 2A 82         [24] 3912 	mov	dpl,_packetStdFraming_sloc5_1_0
      004A64 85 2B 83         [24] 3913 	mov	dph,(_packetStdFraming_sloc5_1_0 + 1)
      004A67 EF               [12] 3914 	mov	a,r7
      004A68 F0               [24] 3915 	movx	@dptr,a
                           0004CB  3916 	C$pktframing.c$331$2$381 ==.
                                   3917 ;	..\COMMON\pktframing.c:331: *(demoPacketBuffer + (n*pktLENGTH + 5)) = (uint8_t) (((sib1 & 0x3C) >> 2) | (trmID & 0x00000F)<<4);
      004A69 74 05            [12] 3918 	mov	a,#0x05
      004A6B 2C               [12] 3919 	add	a,r4
      004A6C FB               [12] 3920 	mov	r3,a
      004A6D E4               [12] 3921 	clr	a
      004A6E 3D               [12] 3922 	addc	a,r5
      004A6F FF               [12] 3923 	mov	r7,a
      004A70 EB               [12] 3924 	mov	a,r3
      004A71 24 A5            [12] 3925 	add	a,#_demoPacketBuffer
      004A73 F5 2C            [12] 3926 	mov	_packetStdFraming_sloc6_1_0,a
      004A75 EF               [12] 3927 	mov	a,r7
      004A76 34 02            [12] 3928 	addc	a,#(_demoPacketBuffer >> 8)
      004A78 F5 2D            [12] 3929 	mov	(_packetStdFraming_sloc6_1_0 + 1),a
      004A7A 53 00 3C         [24] 3930 	anl	ar0,#0x3c
      004A7D E8               [12] 3931 	mov	a,r0
      004A7E 03               [12] 3932 	rr	a
      004A7F 03               [12] 3933 	rr	a
      004A80 54 3F            [12] 3934 	anl	a,#0x3f
      004A82 F5 2A            [12] 3935 	mov	_packetStdFraming_sloc5_1_0,a
      004A84 90 09 B2         [24] 3936 	mov	dptr,#_trmID
      004A87 E0               [24] 3937 	movx	a,@dptr
      004A88 F8               [12] 3938 	mov	r0,a
      004A89 A3               [24] 3939 	inc	dptr
      004A8A E0               [24] 3940 	movx	a,@dptr
      004A8B A3               [24] 3941 	inc	dptr
      004A8C E0               [24] 3942 	movx	a,@dptr
      004A8D A3               [24] 3943 	inc	dptr
      004A8E E0               [24] 3944 	movx	a,@dptr
      004A8F 53 00 0F         [24] 3945 	anl	ar0,#0x0f
      004A92 E4               [12] 3946 	clr	a
      004A93 E8               [12] 3947 	mov	a,r0
      004A94 C4               [12] 3948 	swap	a
      004A95 54 F0            [12] 3949 	anl	a,#0xf0
      004A97 F8               [12] 3950 	mov	r0,a
      004A98 E5 2A            [12] 3951 	mov	a,_packetStdFraming_sloc5_1_0
      004A9A 42 00            [12] 3952 	orl	ar0,a
      004A9C 85 2C 82         [24] 3953 	mov	dpl,_packetStdFraming_sloc6_1_0
      004A9F 85 2D 83         [24] 3954 	mov	dph,(_packetStdFraming_sloc6_1_0 + 1)
      004AA2 E8               [12] 3955 	mov	a,r0
      004AA3 F0               [24] 3956 	movx	@dptr,a
                           000506  3957 	C$pktframing.c$332$2$381 ==.
                                   3958 ;	..\COMMON\pktframing.c:332: *(demoPacketBuffer + (n*pktLENGTH + 6)) = (uint8_t) ((trmID & 0x000FF0) >> 4);
      004AA4 74 06            [12] 3959 	mov	a,#0x06
      004AA6 2C               [12] 3960 	add	a,r4
      004AA7 FB               [12] 3961 	mov	r3,a
      004AA8 E4               [12] 3962 	clr	a
      004AA9 3D               [12] 3963 	addc	a,r5
      004AAA FF               [12] 3964 	mov	r7,a
      004AAB EB               [12] 3965 	mov	a,r3
      004AAC 24 A5            [12] 3966 	add	a,#_demoPacketBuffer
      004AAE F5 2C            [12] 3967 	mov	_packetStdFraming_sloc6_1_0,a
      004AB0 EF               [12] 3968 	mov	a,r7
      004AB1 34 02            [12] 3969 	addc	a,#(_demoPacketBuffer >> 8)
      004AB3 F5 2D            [12] 3970 	mov	(_packetStdFraming_sloc6_1_0 + 1),a
      004AB5 90 09 B2         [24] 3971 	mov	dptr,#_trmID
      004AB8 E0               [24] 3972 	movx	a,@dptr
      004AB9 F8               [12] 3973 	mov	r0,a
      004ABA A3               [24] 3974 	inc	dptr
      004ABB E0               [24] 3975 	movx	a,@dptr
      004ABC F9               [12] 3976 	mov	r1,a
      004ABD A3               [24] 3977 	inc	dptr
      004ABE E0               [24] 3978 	movx	a,@dptr
      004ABF A3               [24] 3979 	inc	dptr
      004AC0 E0               [24] 3980 	movx	a,@dptr
      004AC1 53 00 F0         [24] 3981 	anl	ar0,#0xf0
      004AC4 53 01 0F         [24] 3982 	anl	ar1,#0x0f
      004AC7 E4               [12] 3983 	clr	a
      004AC8 FA               [12] 3984 	mov	r2,a
      004AC9 FF               [12] 3985 	mov	r7,a
      004ACA E9               [12] 3986 	mov	a,r1
      004ACB C4               [12] 3987 	swap	a
      004ACC C8               [12] 3988 	xch	a,r0
      004ACD C4               [12] 3989 	swap	a
      004ACE 54 0F            [12] 3990 	anl	a,#0x0f
      004AD0 68               [12] 3991 	xrl	a,r0
      004AD1 C8               [12] 3992 	xch	a,r0
      004AD2 54 0F            [12] 3993 	anl	a,#0x0f
      004AD4 C8               [12] 3994 	xch	a,r0
      004AD5 68               [12] 3995 	xrl	a,r0
      004AD6 C8               [12] 3996 	xch	a,r0
      004AD7 F9               [12] 3997 	mov	r1,a
      004AD8 EA               [12] 3998 	mov	a,r2
      004AD9 C4               [12] 3999 	swap	a
      004ADA 54 F0            [12] 4000 	anl	a,#0xf0
      004ADC 49               [12] 4001 	orl	a,r1
      004ADD EF               [12] 4002 	mov	a,r7
      004ADE C4               [12] 4003 	swap	a
      004ADF CA               [12] 4004 	xch	a,r2
      004AE0 C4               [12] 4005 	swap	a
      004AE1 54 0F            [12] 4006 	anl	a,#0x0f
      004AE3 6A               [12] 4007 	xrl	a,r2
      004AE4 CA               [12] 4008 	xch	a,r2
      004AE5 54 0F            [12] 4009 	anl	a,#0x0f
      004AE7 CA               [12] 4010 	xch	a,r2
      004AE8 6A               [12] 4011 	xrl	a,r2
      004AE9 CA               [12] 4012 	xch	a,r2
      004AEA 85 2C 82         [24] 4013 	mov	dpl,_packetStdFraming_sloc6_1_0
      004AED 85 2D 83         [24] 4014 	mov	dph,(_packetStdFraming_sloc6_1_0 + 1)
      004AF0 E8               [12] 4015 	mov	a,r0
      004AF1 F0               [24] 4016 	movx	@dptr,a
                           000554  4017 	C$pktframing.c$333$2$381 ==.
                                   4018 ;	..\COMMON\pktframing.c:333: *(demoPacketBuffer + (n*pktLENGTH + 7)) = (uint8_t) ((trmID & 0x0FF000) >> 12);
      004AF2 74 07            [12] 4019 	mov	a,#0x07
      004AF4 2C               [12] 4020 	add	a,r4
      004AF5 FB               [12] 4021 	mov	r3,a
      004AF6 E4               [12] 4022 	clr	a
      004AF7 3D               [12] 4023 	addc	a,r5
      004AF8 FF               [12] 4024 	mov	r7,a
      004AF9 EB               [12] 4025 	mov	a,r3
      004AFA 24 A5            [12] 4026 	add	a,#_demoPacketBuffer
      004AFC F5 2C            [12] 4027 	mov	_packetStdFraming_sloc6_1_0,a
      004AFE EF               [12] 4028 	mov	a,r7
      004AFF 34 02            [12] 4029 	addc	a,#(_demoPacketBuffer >> 8)
      004B01 F5 2D            [12] 4030 	mov	(_packetStdFraming_sloc6_1_0 + 1),a
      004B03 90 09 B2         [24] 4031 	mov	dptr,#_trmID
      004B06 E0               [24] 4032 	movx	a,@dptr
      004B07 A3               [24] 4033 	inc	dptr
      004B08 E0               [24] 4034 	movx	a,@dptr
      004B09 F9               [12] 4035 	mov	r1,a
      004B0A A3               [24] 4036 	inc	dptr
      004B0B E0               [24] 4037 	movx	a,@dptr
      004B0C FA               [12] 4038 	mov	r2,a
      004B0D A3               [24] 4039 	inc	dptr
      004B0E E0               [24] 4040 	movx	a,@dptr
      004B0F 53 01 F0         [24] 4041 	anl	ar1,#0xf0
      004B12 53 02 0F         [24] 4042 	anl	ar2,#0x0f
      004B15 7F 00            [12] 4043 	mov	r7,#0x00
      004B17 89 00            [24] 4044 	mov	ar0,r1
      004B19 EA               [12] 4045 	mov	a,r2
      004B1A C4               [12] 4046 	swap	a
      004B1B C8               [12] 4047 	xch	a,r0
      004B1C C4               [12] 4048 	swap	a
      004B1D 54 0F            [12] 4049 	anl	a,#0x0f
      004B1F 68               [12] 4050 	xrl	a,r0
      004B20 C8               [12] 4051 	xch	a,r0
      004B21 54 0F            [12] 4052 	anl	a,#0x0f
      004B23 C8               [12] 4053 	xch	a,r0
      004B24 68               [12] 4054 	xrl	a,r0
      004B25 C8               [12] 4055 	xch	a,r0
      004B26 F9               [12] 4056 	mov	r1,a
      004B27 EF               [12] 4057 	mov	a,r7
      004B28 C4               [12] 4058 	swap	a
      004B29 54 F0            [12] 4059 	anl	a,#0xf0
      004B2B 49               [12] 4060 	orl	a,r1
      004B2C EF               [12] 4061 	mov	a,r7
      004B2D C4               [12] 4062 	swap	a
      004B2E 54 0F            [12] 4063 	anl	a,#0x0f
      004B30 85 2C 82         [24] 4064 	mov	dpl,_packetStdFraming_sloc6_1_0
      004B33 85 2D 83         [24] 4065 	mov	dph,(_packetStdFraming_sloc6_1_0 + 1)
      004B36 E8               [12] 4066 	mov	a,r0
      004B37 F0               [24] 4067 	movx	@dptr,a
                           00059A  4068 	C$pktframing.c$334$2$381 ==.
                                   4069 ;	..\COMMON\pktframing.c:334: *(demoPacketBuffer + (n*pktLENGTH + 8)) = (uint8_t) (((trmID & 0xF00000) >> 20) | (ack_flg << 4));
      004B38 74 08            [12] 4070 	mov	a,#0x08
      004B3A 2C               [12] 4071 	add	a,r4
      004B3B FC               [12] 4072 	mov	r4,a
      004B3C E4               [12] 4073 	clr	a
      004B3D 3D               [12] 4074 	addc	a,r5
      004B3E FD               [12] 4075 	mov	r5,a
      004B3F EC               [12] 4076 	mov	a,r4
      004B40 24 A5            [12] 4077 	add	a,#_demoPacketBuffer
      004B42 F5 2C            [12] 4078 	mov	_packetStdFraming_sloc6_1_0,a
      004B44 ED               [12] 4079 	mov	a,r5
      004B45 34 02            [12] 4080 	addc	a,#(_demoPacketBuffer >> 8)
      004B47 F5 2D            [12] 4081 	mov	(_packetStdFraming_sloc6_1_0 + 1),a
      004B49 90 09 B2         [24] 4082 	mov	dptr,#_trmID
      004B4C E0               [24] 4083 	movx	a,@dptr
      004B4D A3               [24] 4084 	inc	dptr
      004B4E E0               [24] 4085 	movx	a,@dptr
      004B4F A3               [24] 4086 	inc	dptr
      004B50 E0               [24] 4087 	movx	a,@dptr
      004B51 FB               [12] 4088 	mov	r3,a
      004B52 A3               [24] 4089 	inc	dptr
      004B53 E0               [24] 4090 	movx	a,@dptr
      004B54 53 03 F0         [24] 4091 	anl	ar3,#0xf0
      004B57 7F 00            [12] 4092 	mov	r7,#0x00
      004B59 8B 30            [24] 4093 	mov	_packetStdFraming_sloc8_1_0,r3
      004B5B EF               [12] 4094 	mov	a,r7
      004B5C C4               [12] 4095 	swap	a
      004B5D C5 30            [12] 4096 	xch	a,_packetStdFraming_sloc8_1_0
      004B5F C4               [12] 4097 	swap	a
      004B60 54 0F            [12] 4098 	anl	a,#0x0f
      004B62 65 30            [12] 4099 	xrl	a,_packetStdFraming_sloc8_1_0
      004B64 C5 30            [12] 4100 	xch	a,_packetStdFraming_sloc8_1_0
      004B66 54 0F            [12] 4101 	anl	a,#0x0f
      004B68 C5 30            [12] 4102 	xch	a,_packetStdFraming_sloc8_1_0
      004B6A 65 30            [12] 4103 	xrl	a,_packetStdFraming_sloc8_1_0
      004B6C C5 30            [12] 4104 	xch	a,_packetStdFraming_sloc8_1_0
      004B6E F5 31            [12] 4105 	mov	(_packetStdFraming_sloc8_1_0 + 1),a
      004B70 75 32 00         [24] 4106 	mov	(_packetStdFraming_sloc8_1_0 + 2),#0x00
      004B73 75 33 00         [24] 4107 	mov	(_packetStdFraming_sloc8_1_0 + 3),#0x00
      004B76 90 09 CC         [24] 4108 	mov	dptr,#_ack_flg
      004B79 E0               [24] 4109 	movx	a,@dptr
      004B7A C4               [12] 4110 	swap	a
      004B7B 54 F0            [12] 4111 	anl	a,#0xf0
      004B7D F8               [12] 4112 	mov	r0,a
      004B7E E4               [12] 4113 	clr	a
      004B7F FC               [12] 4114 	mov	r4,a
      004B80 FD               [12] 4115 	mov	r5,a
      004B81 FF               [12] 4116 	mov	r7,a
      004B82 E5 30            [12] 4117 	mov	a,_packetStdFraming_sloc8_1_0
      004B84 42 00            [12] 4118 	orl	ar0,a
      004B86 E5 31            [12] 4119 	mov	a,(_packetStdFraming_sloc8_1_0 + 1)
      004B88 42 04            [12] 4120 	orl	ar4,a
      004B8A E5 32            [12] 4121 	mov	a,(_packetStdFraming_sloc8_1_0 + 2)
      004B8C 42 05            [12] 4122 	orl	ar5,a
      004B8E E5 33            [12] 4123 	mov	a,(_packetStdFraming_sloc8_1_0 + 3)
      004B90 42 07            [12] 4124 	orl	ar7,a
      004B92 85 2C 82         [24] 4125 	mov	dpl,_packetStdFraming_sloc6_1_0
      004B95 85 2D 83         [24] 4126 	mov	dph,(_packetStdFraming_sloc6_1_0 + 1)
      004B98 E8               [12] 4127 	mov	a,r0
      004B99 F0               [24] 4128 	movx	@dptr,a
                           0005FC  4129 	C$pktframing.c$337$2$381 ==.
                                   4130 ;	..\COMMON\pktframing.c:337: memcpy((demoPacketBuffer +  n*pktLENGTH + 9), &dummyHMAC, sizeof(uint32_t));
      004B9A 74 09            [12] 4131 	mov	a,#0x09
      004B9C 25 2E            [12] 4132 	add	a,_packetStdFraming_sloc7_1_0
      004B9E FD               [12] 4133 	mov	r5,a
      004B9F E4               [12] 4134 	clr	a
      004BA0 35 2F            [12] 4135 	addc	a,(_packetStdFraming_sloc7_1_0 + 1)
      004BA2 FF               [12] 4136 	mov	r7,a
      004BA3 7C 00            [12] 4137 	mov	r4,#0x00
      004BA5 75 6E AE         [24] 4138 	mov	_memcpy_PARM_2,#_dummyHMAC
      004BA8 75 6F 09         [24] 4139 	mov	(_memcpy_PARM_2 + 1),#(_dummyHMAC >> 8)
                                   4140 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      004BAB 8C 70            [24] 4141 	mov	(_memcpy_PARM_2 + 2),r4
      004BAD 75 71 04         [24] 4142 	mov	_memcpy_PARM_3,#0x04
                                   4143 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_3 + 1),#0x00
      004BB0 8C 72            [24] 4144 	mov	(_memcpy_PARM_3 + 1),r4
      004BB2 8D 82            [24] 4145 	mov	dpl,r5
      004BB4 8F 83            [24] 4146 	mov	dph,r7
      004BB6 8C F0            [24] 4147 	mov	b,r4
      004BB8 C0 06            [24] 4148 	push	ar6
      004BBA C0 03            [24] 4149 	push	ar3
      004BBC 12 69 78         [24] 4150 	lcall	_memcpy
      004BBF D0 03            [24] 4151 	pop	ar3
      004BC1 D0 06            [24] 4152 	pop	ar6
                           000625  4153 	C$pktframing.c$338$2$381 ==.
                                   4154 ;	..\COMMON\pktframing.c:338: memcpy((demoPacketBuffer +  n*pktLENGTH + 13), payload, sizeof(uint8_t)*payload_len);
      004BC3 74 0D            [12] 4155 	mov	a,#0x0d
      004BC5 25 2E            [12] 4156 	add	a,_packetStdFraming_sloc7_1_0
      004BC7 FD               [12] 4157 	mov	r5,a
      004BC8 E4               [12] 4158 	clr	a
      004BC9 35 2F            [12] 4159 	addc	a,(_packetStdFraming_sloc7_1_0 + 1)
      004BCB FF               [12] 4160 	mov	r7,a
      004BCC 8D 02            [24] 4161 	mov	ar2,r5
      004BCE 8F 03            [24] 4162 	mov	ar3,r7
      004BD0 7C 00            [12] 4163 	mov	r4,#0x00
      004BD2 75 6E D6         [24] 4164 	mov	_memcpy_PARM_2,#_payload
      004BD5 75 6F 03         [24] 4165 	mov	(_memcpy_PARM_2 + 1),#(_payload >> 8)
                                   4166 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      004BD8 8C 70            [24] 4167 	mov	(_memcpy_PARM_2 + 2),r4
      004BDA 90 03 F2         [24] 4168 	mov	dptr,#_payload_len
      004BDD E0               [24] 4169 	movx	a,@dptr
      004BDE F5 71            [12] 4170 	mov	_memcpy_PARM_3,a
      004BE0 A3               [24] 4171 	inc	dptr
      004BE1 E0               [24] 4172 	movx	a,@dptr
      004BE2 F5 72            [12] 4173 	mov	(_memcpy_PARM_3 + 1),a
      004BE4 8A 82            [24] 4174 	mov	dpl,r2
      004BE6 8B 83            [24] 4175 	mov	dph,r3
      004BE8 8C F0            [24] 4176 	mov	b,r4
      004BEA C0 07            [24] 4177 	push	ar7
      004BEC C0 06            [24] 4178 	push	ar6
      004BEE C0 05            [24] 4179 	push	ar5
      004BF0 C0 03            [24] 4180 	push	ar3
      004BF2 12 69 78         [24] 4181 	lcall	_memcpy
      004BF5 D0 03            [24] 4182 	pop	ar3
                           000659  4183 	C$pktframing.c$340$2$381 ==.
                                   4184 ;	..\COMMON\pktframing.c:340: CRC32 = crc_crc32_msb((demoPacketBuffer+ n*pktLENGTH), (13+payload_len), 0xFFFFFFFF);
      004BF7 90 03 F2         [24] 4185 	mov	dptr,#_payload_len
      004BFA E0               [24] 4186 	movx	a,@dptr
      004BFB FB               [12] 4187 	mov	r3,a
      004BFC A3               [24] 4188 	inc	dptr
      004BFD E0               [24] 4189 	movx	a,@dptr
      004BFE FC               [12] 4190 	mov	r4,a
      004BFF 74 0D            [12] 4191 	mov	a,#0x0d
      004C01 2B               [12] 4192 	add	a,r3
      004C02 FB               [12] 4193 	mov	r3,a
      004C03 E4               [12] 4194 	clr	a
      004C04 3C               [12] 4195 	addc	a,r4
      004C05 FC               [12] 4196 	mov	r4,a
      004C06 A9 2E            [24] 4197 	mov	r1,_packetStdFraming_sloc7_1_0
      004C08 A8 2F            [24] 4198 	mov	r0,(_packetStdFraming_sloc7_1_0 + 1)
      004C0A 7A 00            [12] 4199 	mov	r2,#0x00
      004C0C 74 FF            [12] 4200 	mov	a,#0xff
      004C0E C0 E0            [24] 4201 	push	acc
      004C10 C0 E0            [24] 4202 	push	acc
      004C12 C0 E0            [24] 4203 	push	acc
      004C14 C0 E0            [24] 4204 	push	acc
      004C16 C0 03            [24] 4205 	push	ar3
      004C18 C0 04            [24] 4206 	push	ar4
      004C1A 89 82            [24] 4207 	mov	dpl,r1
      004C1C 88 83            [24] 4208 	mov	dph,r0
      004C1E 8A F0            [24] 4209 	mov	b,r2
      004C20 12 79 5C         [24] 4210 	lcall	_crc_crc32_msb
      004C23 A9 82            [24] 4211 	mov	r1,dpl
      004C25 AA 83            [24] 4212 	mov	r2,dph
      004C27 AB F0            [24] 4213 	mov	r3,b
      004C29 FC               [12] 4214 	mov	r4,a
      004C2A E5 81            [12] 4215 	mov	a,sp
      004C2C 24 FA            [12] 4216 	add	a,#0xfa
      004C2E F5 81            [12] 4217 	mov	sp,a
      004C30 D0 05            [24] 4218 	pop	ar5
      004C32 D0 06            [24] 4219 	pop	ar6
      004C34 D0 07            [24] 4220 	pop	ar7
      004C36 90 03 77         [24] 4221 	mov	dptr,#_packetStdFraming_CRC32_1_380
      004C39 E9               [12] 4222 	mov	a,r1
      004C3A F0               [24] 4223 	movx	@dptr,a
      004C3B EA               [12] 4224 	mov	a,r2
      004C3C A3               [24] 4225 	inc	dptr
      004C3D F0               [24] 4226 	movx	@dptr,a
      004C3E EB               [12] 4227 	mov	a,r3
      004C3F A3               [24] 4228 	inc	dptr
      004C40 F0               [24] 4229 	movx	@dptr,a
      004C41 EC               [12] 4230 	mov	a,r4
      004C42 A3               [24] 4231 	inc	dptr
      004C43 F0               [24] 4232 	movx	@dptr,a
                           0006A6  4233 	C$pktframing.c$341$2$381 ==.
                                   4234 ;	..\COMMON\pktframing.c:341: memcpy((demoPacketBuffer+n*pktLENGTH+13+payload_len), &CRC32, sizeof(uint32_t));
      004C44 90 03 F2         [24] 4235 	mov	dptr,#_payload_len
      004C47 E0               [24] 4236 	movx	a,@dptr
      004C48 FB               [12] 4237 	mov	r3,a
      004C49 A3               [24] 4238 	inc	dptr
      004C4A E0               [24] 4239 	movx	a,@dptr
      004C4B FC               [12] 4240 	mov	r4,a
      004C4C EB               [12] 4241 	mov	a,r3
      004C4D 2D               [12] 4242 	add	a,r5
      004C4E FD               [12] 4243 	mov	r5,a
      004C4F EC               [12] 4244 	mov	a,r4
      004C50 3F               [12] 4245 	addc	a,r7
      004C51 FF               [12] 4246 	mov	r7,a
      004C52 7C 00            [12] 4247 	mov	r4,#0x00
      004C54 75 6E 77         [24] 4248 	mov	_memcpy_PARM_2,#_packetStdFraming_CRC32_1_380
      004C57 75 6F 03         [24] 4249 	mov	(_memcpy_PARM_2 + 1),#(_packetStdFraming_CRC32_1_380 >> 8)
                                   4250 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_2 + 2),#0x00
      004C5A 8C 70            [24] 4251 	mov	(_memcpy_PARM_2 + 2),r4
      004C5C 75 71 04         [24] 4252 	mov	_memcpy_PARM_3,#0x04
                                   4253 ;	1-genFromRTrack replaced	mov	(_memcpy_PARM_3 + 1),#0x00
      004C5F 8C 72            [24] 4254 	mov	(_memcpy_PARM_3 + 1),r4
      004C61 8D 82            [24] 4255 	mov	dpl,r5
      004C63 8F 83            [24] 4256 	mov	dph,r7
      004C65 8C F0            [24] 4257 	mov	b,r4
      004C67 C0 06            [24] 4258 	push	ar6
      004C69 C0 03            [24] 4259 	push	ar3
      004C6B 12 69 78         [24] 4260 	lcall	_memcpy
      004C6E D0 03            [24] 4261 	pop	ar3
      004C70 D0 06            [24] 4262 	pop	ar6
                           0006D4  4263 	C$pktframing.c$283$1$380 ==.
                                   4264 ;	..\COMMON\pktframing.c:283: for (n = 0; n < 3; n++)
      004C72 0E               [12] 4265 	inc	r6
      004C73 BE 03 00         [24] 4266 	cjne	r6,#0x03,00130$
      004C76                       4267 00130$:
      004C76 D0 03            [24] 4268 	pop	ar3
      004C78 50 03            [24] 4269 	jnc	00131$
      004C7A 02 49 82         [24] 4270 	ljmp	00107$
      004C7D                       4271 00131$:
                           0006DF  4272 	C$pktframing.c$345$1$380 ==.
                                   4273 ;	..\COMMON\pktframing.c:345: ack_flg = 0; // clear the flag after a new series of packets have been prepared.
      004C7D 90 09 CC         [24] 4274 	mov	dptr,#_ack_flg
      004C80 E4               [12] 4275 	clr	a
      004C81 F0               [24] 4276 	movx	@dptr,a
                           0006E4  4277 	C$pktframing.c$347$1$380 ==.
                           0006E4  4278 	XG$packetStdFraming$0$0 ==.
      004C82 22               [24] 4279 	ret
                                   4280 	.area CSEG    (CODE)
                                   4281 	.area CONST   (CODE)
                           000000  4282 Fpktframing$__str_0$0$0 == .
      0080EF                       4283 ___str_0:
      0080EF 33 5F 52 4E 47 5F 57  4284 	.ascii "3_RNG_WT0: "
             54 30 3A 20
      0080FA 00                    4285 	.db 0x00
                           00000C  4286 Fpktframing$__str_1$0$0 == .
      0080FB                       4287 ___str_1:
      0080FB 32 5F 47 6F 6C 64 5F  4288 	.ascii "2_Gold_WT0:"
             57 54 30 3A
      008106 00                    4289 	.db 0x00
                                   4290 	.area XINIT   (CODE)
                           000000  4291 Fpktframing$__xinit_dummyHMAC$0$0 == .
      0087E9                       4292 __xinit__dummyHMAC:
      0087E9 C0 C1 C2 C3           4293 	.byte #0xc0,#0xc1,#0xc2,#0xc3	; 3284320704
                           000004  4294 Fpktframing$__xinit_trmID$0$0 == .
      0087ED                       4295 __xinit__trmID:
      0087ED 01 00 10 00           4296 	.byte #0x01,#0x00,#0x10,#0x00	; 1048577
                           000008  4297 Fpktframing$__xinit_StdPrem$0$0 == .
      0087F1                       4298 __xinit__StdPrem:
      0087F1 01                    4299 	.db #0x01	; 1
                                   4300 	.area CABS    (ABS,CODE)
