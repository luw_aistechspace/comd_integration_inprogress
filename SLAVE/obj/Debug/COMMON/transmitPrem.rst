                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module transmitPrem
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _transmit_prem_wtimer
                                     12 	.globl _premPkt_callback
                                     13 	.globl _dbglink_writehex32
                                     14 	.globl _dbglink_writehex16
                                     15 	.globl _dbglink_writestr
                                     16 	.globl _dbglink_tx
                                     17 	.globl _wtimer0_remove
                                     18 	.globl _wtimer0_addrelative
                                     19 	.globl _wtimer0_curtime
                                     20 	.globl _axradio_transmit
                                     21 	.globl _PORTC_7
                                     22 	.globl _PORTC_6
                                     23 	.globl _PORTC_5
                                     24 	.globl _PORTC_4
                                     25 	.globl _PORTC_3
                                     26 	.globl _PORTC_2
                                     27 	.globl _PORTC_1
                                     28 	.globl _PORTC_0
                                     29 	.globl _PORTB_7
                                     30 	.globl _PORTB_6
                                     31 	.globl _PORTB_5
                                     32 	.globl _PORTB_4
                                     33 	.globl _PORTB_3
                                     34 	.globl _PORTB_2
                                     35 	.globl _PORTB_1
                                     36 	.globl _PORTB_0
                                     37 	.globl _PORTA_7
                                     38 	.globl _PORTA_6
                                     39 	.globl _PORTA_5
                                     40 	.globl _PORTA_4
                                     41 	.globl _PORTA_3
                                     42 	.globl _PORTA_2
                                     43 	.globl _PORTA_1
                                     44 	.globl _PORTA_0
                                     45 	.globl _PINC_7
                                     46 	.globl _PINC_6
                                     47 	.globl _PINC_5
                                     48 	.globl _PINC_4
                                     49 	.globl _PINC_3
                                     50 	.globl _PINC_2
                                     51 	.globl _PINC_1
                                     52 	.globl _PINC_0
                                     53 	.globl _PINB_7
                                     54 	.globl _PINB_6
                                     55 	.globl _PINB_5
                                     56 	.globl _PINB_4
                                     57 	.globl _PINB_3
                                     58 	.globl _PINB_2
                                     59 	.globl _PINB_1
                                     60 	.globl _PINB_0
                                     61 	.globl _PINA_7
                                     62 	.globl _PINA_6
                                     63 	.globl _PINA_5
                                     64 	.globl _PINA_4
                                     65 	.globl _PINA_3
                                     66 	.globl _PINA_2
                                     67 	.globl _PINA_1
                                     68 	.globl _PINA_0
                                     69 	.globl _CY
                                     70 	.globl _AC
                                     71 	.globl _F0
                                     72 	.globl _RS1
                                     73 	.globl _RS0
                                     74 	.globl _OV
                                     75 	.globl _F1
                                     76 	.globl _P
                                     77 	.globl _IP_7
                                     78 	.globl _IP_6
                                     79 	.globl _IP_5
                                     80 	.globl _IP_4
                                     81 	.globl _IP_3
                                     82 	.globl _IP_2
                                     83 	.globl _IP_1
                                     84 	.globl _IP_0
                                     85 	.globl _EA
                                     86 	.globl _IE_7
                                     87 	.globl _IE_6
                                     88 	.globl _IE_5
                                     89 	.globl _IE_4
                                     90 	.globl _IE_3
                                     91 	.globl _IE_2
                                     92 	.globl _IE_1
                                     93 	.globl _IE_0
                                     94 	.globl _EIP_7
                                     95 	.globl _EIP_6
                                     96 	.globl _EIP_5
                                     97 	.globl _EIP_4
                                     98 	.globl _EIP_3
                                     99 	.globl _EIP_2
                                    100 	.globl _EIP_1
                                    101 	.globl _EIP_0
                                    102 	.globl _EIE_7
                                    103 	.globl _EIE_6
                                    104 	.globl _EIE_5
                                    105 	.globl _EIE_4
                                    106 	.globl _EIE_3
                                    107 	.globl _EIE_2
                                    108 	.globl _EIE_1
                                    109 	.globl _EIE_0
                                    110 	.globl _E2IP_7
                                    111 	.globl _E2IP_6
                                    112 	.globl _E2IP_5
                                    113 	.globl _E2IP_4
                                    114 	.globl _E2IP_3
                                    115 	.globl _E2IP_2
                                    116 	.globl _E2IP_1
                                    117 	.globl _E2IP_0
                                    118 	.globl _E2IE_7
                                    119 	.globl _E2IE_6
                                    120 	.globl _E2IE_5
                                    121 	.globl _E2IE_4
                                    122 	.globl _E2IE_3
                                    123 	.globl _E2IE_2
                                    124 	.globl _E2IE_1
                                    125 	.globl _E2IE_0
                                    126 	.globl _B_7
                                    127 	.globl _B_6
                                    128 	.globl _B_5
                                    129 	.globl _B_4
                                    130 	.globl _B_3
                                    131 	.globl _B_2
                                    132 	.globl _B_1
                                    133 	.globl _B_0
                                    134 	.globl _ACC_7
                                    135 	.globl _ACC_6
                                    136 	.globl _ACC_5
                                    137 	.globl _ACC_4
                                    138 	.globl _ACC_3
                                    139 	.globl _ACC_2
                                    140 	.globl _ACC_1
                                    141 	.globl _ACC_0
                                    142 	.globl _WTSTAT
                                    143 	.globl _WTIRQEN
                                    144 	.globl _WTEVTD
                                    145 	.globl _WTEVTD1
                                    146 	.globl _WTEVTD0
                                    147 	.globl _WTEVTC
                                    148 	.globl _WTEVTC1
                                    149 	.globl _WTEVTC0
                                    150 	.globl _WTEVTB
                                    151 	.globl _WTEVTB1
                                    152 	.globl _WTEVTB0
                                    153 	.globl _WTEVTA
                                    154 	.globl _WTEVTA1
                                    155 	.globl _WTEVTA0
                                    156 	.globl _WTCNTR1
                                    157 	.globl _WTCNTB
                                    158 	.globl _WTCNTB1
                                    159 	.globl _WTCNTB0
                                    160 	.globl _WTCNTA
                                    161 	.globl _WTCNTA1
                                    162 	.globl _WTCNTA0
                                    163 	.globl _WTCFGB
                                    164 	.globl _WTCFGA
                                    165 	.globl _WDTRESET
                                    166 	.globl _WDTCFG
                                    167 	.globl _U1STATUS
                                    168 	.globl _U1SHREG
                                    169 	.globl _U1MODE
                                    170 	.globl _U1CTRL
                                    171 	.globl _U0STATUS
                                    172 	.globl _U0SHREG
                                    173 	.globl _U0MODE
                                    174 	.globl _U0CTRL
                                    175 	.globl _T2STATUS
                                    176 	.globl _T2PERIOD
                                    177 	.globl _T2PERIOD1
                                    178 	.globl _T2PERIOD0
                                    179 	.globl _T2MODE
                                    180 	.globl _T2CNT
                                    181 	.globl _T2CNT1
                                    182 	.globl _T2CNT0
                                    183 	.globl _T2CLKSRC
                                    184 	.globl _T1STATUS
                                    185 	.globl _T1PERIOD
                                    186 	.globl _T1PERIOD1
                                    187 	.globl _T1PERIOD0
                                    188 	.globl _T1MODE
                                    189 	.globl _T1CNT
                                    190 	.globl _T1CNT1
                                    191 	.globl _T1CNT0
                                    192 	.globl _T1CLKSRC
                                    193 	.globl _T0STATUS
                                    194 	.globl _T0PERIOD
                                    195 	.globl _T0PERIOD1
                                    196 	.globl _T0PERIOD0
                                    197 	.globl _T0MODE
                                    198 	.globl _T0CNT
                                    199 	.globl _T0CNT1
                                    200 	.globl _T0CNT0
                                    201 	.globl _T0CLKSRC
                                    202 	.globl _SPSTATUS
                                    203 	.globl _SPSHREG
                                    204 	.globl _SPMODE
                                    205 	.globl _SPCLKSRC
                                    206 	.globl _RADIOSTAT
                                    207 	.globl _RADIOSTAT1
                                    208 	.globl _RADIOSTAT0
                                    209 	.globl _RADIODATA
                                    210 	.globl _RADIODATA3
                                    211 	.globl _RADIODATA2
                                    212 	.globl _RADIODATA1
                                    213 	.globl _RADIODATA0
                                    214 	.globl _RADIOADDR
                                    215 	.globl _RADIOADDR1
                                    216 	.globl _RADIOADDR0
                                    217 	.globl _RADIOACC
                                    218 	.globl _OC1STATUS
                                    219 	.globl _OC1PIN
                                    220 	.globl _OC1MODE
                                    221 	.globl _OC1COMP
                                    222 	.globl _OC1COMP1
                                    223 	.globl _OC1COMP0
                                    224 	.globl _OC0STATUS
                                    225 	.globl _OC0PIN
                                    226 	.globl _OC0MODE
                                    227 	.globl _OC0COMP
                                    228 	.globl _OC0COMP1
                                    229 	.globl _OC0COMP0
                                    230 	.globl _NVSTATUS
                                    231 	.globl _NVKEY
                                    232 	.globl _NVDATA
                                    233 	.globl _NVDATA1
                                    234 	.globl _NVDATA0
                                    235 	.globl _NVADDR
                                    236 	.globl _NVADDR1
                                    237 	.globl _NVADDR0
                                    238 	.globl _IC1STATUS
                                    239 	.globl _IC1MODE
                                    240 	.globl _IC1CAPT
                                    241 	.globl _IC1CAPT1
                                    242 	.globl _IC1CAPT0
                                    243 	.globl _IC0STATUS
                                    244 	.globl _IC0MODE
                                    245 	.globl _IC0CAPT
                                    246 	.globl _IC0CAPT1
                                    247 	.globl _IC0CAPT0
                                    248 	.globl _PORTR
                                    249 	.globl _PORTC
                                    250 	.globl _PORTB
                                    251 	.globl _PORTA
                                    252 	.globl _PINR
                                    253 	.globl _PINC
                                    254 	.globl _PINB
                                    255 	.globl _PINA
                                    256 	.globl _DIRR
                                    257 	.globl _DIRC
                                    258 	.globl _DIRB
                                    259 	.globl _DIRA
                                    260 	.globl _DBGLNKSTAT
                                    261 	.globl _DBGLNKBUF
                                    262 	.globl _CODECONFIG
                                    263 	.globl _CLKSTAT
                                    264 	.globl _CLKCON
                                    265 	.globl _ANALOGCOMP
                                    266 	.globl _ADCCONV
                                    267 	.globl _ADCCLKSRC
                                    268 	.globl _ADCCH3CONFIG
                                    269 	.globl _ADCCH2CONFIG
                                    270 	.globl _ADCCH1CONFIG
                                    271 	.globl _ADCCH0CONFIG
                                    272 	.globl __XPAGE
                                    273 	.globl _XPAGE
                                    274 	.globl _SP
                                    275 	.globl _PSW
                                    276 	.globl _PCON
                                    277 	.globl _IP
                                    278 	.globl _IE
                                    279 	.globl _EIP
                                    280 	.globl _EIE
                                    281 	.globl _E2IP
                                    282 	.globl _E2IE
                                    283 	.globl _DPS
                                    284 	.globl _DPTR1
                                    285 	.globl _DPTR0
                                    286 	.globl _DPL1
                                    287 	.globl _DPL
                                    288 	.globl _DPH1
                                    289 	.globl _DPH
                                    290 	.globl _B
                                    291 	.globl _ACC
                                    292 	.globl _StdPrem
                                    293 	.globl _trmID
                                    294 	.globl _premPkt_tmr
                                    295 	.globl _AX5043_TIMEGAIN3NB
                                    296 	.globl _AX5043_TIMEGAIN2NB
                                    297 	.globl _AX5043_TIMEGAIN1NB
                                    298 	.globl _AX5043_TIMEGAIN0NB
                                    299 	.globl _AX5043_RXPARAMSETSNB
                                    300 	.globl _AX5043_RXPARAMCURSETNB
                                    301 	.globl _AX5043_PKTMAXLENNB
                                    302 	.globl _AX5043_PKTLENOFFSETNB
                                    303 	.globl _AX5043_PKTLENCFGNB
                                    304 	.globl _AX5043_PKTADDRMASK3NB
                                    305 	.globl _AX5043_PKTADDRMASK2NB
                                    306 	.globl _AX5043_PKTADDRMASK1NB
                                    307 	.globl _AX5043_PKTADDRMASK0NB
                                    308 	.globl _AX5043_PKTADDRCFGNB
                                    309 	.globl _AX5043_PKTADDR3NB
                                    310 	.globl _AX5043_PKTADDR2NB
                                    311 	.globl _AX5043_PKTADDR1NB
                                    312 	.globl _AX5043_PKTADDR0NB
                                    313 	.globl _AX5043_PHASEGAIN3NB
                                    314 	.globl _AX5043_PHASEGAIN2NB
                                    315 	.globl _AX5043_PHASEGAIN1NB
                                    316 	.globl _AX5043_PHASEGAIN0NB
                                    317 	.globl _AX5043_FREQUENCYLEAKNB
                                    318 	.globl _AX5043_FREQUENCYGAIND3NB
                                    319 	.globl _AX5043_FREQUENCYGAIND2NB
                                    320 	.globl _AX5043_FREQUENCYGAIND1NB
                                    321 	.globl _AX5043_FREQUENCYGAIND0NB
                                    322 	.globl _AX5043_FREQUENCYGAINC3NB
                                    323 	.globl _AX5043_FREQUENCYGAINC2NB
                                    324 	.globl _AX5043_FREQUENCYGAINC1NB
                                    325 	.globl _AX5043_FREQUENCYGAINC0NB
                                    326 	.globl _AX5043_FREQUENCYGAINB3NB
                                    327 	.globl _AX5043_FREQUENCYGAINB2NB
                                    328 	.globl _AX5043_FREQUENCYGAINB1NB
                                    329 	.globl _AX5043_FREQUENCYGAINB0NB
                                    330 	.globl _AX5043_FREQUENCYGAINA3NB
                                    331 	.globl _AX5043_FREQUENCYGAINA2NB
                                    332 	.globl _AX5043_FREQUENCYGAINA1NB
                                    333 	.globl _AX5043_FREQUENCYGAINA0NB
                                    334 	.globl _AX5043_FREQDEV13NB
                                    335 	.globl _AX5043_FREQDEV12NB
                                    336 	.globl _AX5043_FREQDEV11NB
                                    337 	.globl _AX5043_FREQDEV10NB
                                    338 	.globl _AX5043_FREQDEV03NB
                                    339 	.globl _AX5043_FREQDEV02NB
                                    340 	.globl _AX5043_FREQDEV01NB
                                    341 	.globl _AX5043_FREQDEV00NB
                                    342 	.globl _AX5043_FOURFSK3NB
                                    343 	.globl _AX5043_FOURFSK2NB
                                    344 	.globl _AX5043_FOURFSK1NB
                                    345 	.globl _AX5043_FOURFSK0NB
                                    346 	.globl _AX5043_DRGAIN3NB
                                    347 	.globl _AX5043_DRGAIN2NB
                                    348 	.globl _AX5043_DRGAIN1NB
                                    349 	.globl _AX5043_DRGAIN0NB
                                    350 	.globl _AX5043_BBOFFSRES3NB
                                    351 	.globl _AX5043_BBOFFSRES2NB
                                    352 	.globl _AX5043_BBOFFSRES1NB
                                    353 	.globl _AX5043_BBOFFSRES0NB
                                    354 	.globl _AX5043_AMPLITUDEGAIN3NB
                                    355 	.globl _AX5043_AMPLITUDEGAIN2NB
                                    356 	.globl _AX5043_AMPLITUDEGAIN1NB
                                    357 	.globl _AX5043_AMPLITUDEGAIN0NB
                                    358 	.globl _AX5043_AGCTARGET3NB
                                    359 	.globl _AX5043_AGCTARGET2NB
                                    360 	.globl _AX5043_AGCTARGET1NB
                                    361 	.globl _AX5043_AGCTARGET0NB
                                    362 	.globl _AX5043_AGCMINMAX3NB
                                    363 	.globl _AX5043_AGCMINMAX2NB
                                    364 	.globl _AX5043_AGCMINMAX1NB
                                    365 	.globl _AX5043_AGCMINMAX0NB
                                    366 	.globl _AX5043_AGCGAIN3NB
                                    367 	.globl _AX5043_AGCGAIN2NB
                                    368 	.globl _AX5043_AGCGAIN1NB
                                    369 	.globl _AX5043_AGCGAIN0NB
                                    370 	.globl _AX5043_AGCAHYST3NB
                                    371 	.globl _AX5043_AGCAHYST2NB
                                    372 	.globl _AX5043_AGCAHYST1NB
                                    373 	.globl _AX5043_AGCAHYST0NB
                                    374 	.globl _AX5043_0xF44NB
                                    375 	.globl _AX5043_0xF35NB
                                    376 	.globl _AX5043_0xF34NB
                                    377 	.globl _AX5043_0xF33NB
                                    378 	.globl _AX5043_0xF32NB
                                    379 	.globl _AX5043_0xF31NB
                                    380 	.globl _AX5043_0xF30NB
                                    381 	.globl _AX5043_0xF26NB
                                    382 	.globl _AX5043_0xF23NB
                                    383 	.globl _AX5043_0xF22NB
                                    384 	.globl _AX5043_0xF21NB
                                    385 	.globl _AX5043_0xF1CNB
                                    386 	.globl _AX5043_0xF18NB
                                    387 	.globl _AX5043_0xF0CNB
                                    388 	.globl _AX5043_0xF00NB
                                    389 	.globl _AX5043_XTALSTATUSNB
                                    390 	.globl _AX5043_XTALOSCNB
                                    391 	.globl _AX5043_XTALCAPNB
                                    392 	.globl _AX5043_XTALAMPLNB
                                    393 	.globl _AX5043_WAKEUPXOEARLYNB
                                    394 	.globl _AX5043_WAKEUPTIMER1NB
                                    395 	.globl _AX5043_WAKEUPTIMER0NB
                                    396 	.globl _AX5043_WAKEUPFREQ1NB
                                    397 	.globl _AX5043_WAKEUPFREQ0NB
                                    398 	.globl _AX5043_WAKEUP1NB
                                    399 	.globl _AX5043_WAKEUP0NB
                                    400 	.globl _AX5043_TXRATE2NB
                                    401 	.globl _AX5043_TXRATE1NB
                                    402 	.globl _AX5043_TXRATE0NB
                                    403 	.globl _AX5043_TXPWRCOEFFE1NB
                                    404 	.globl _AX5043_TXPWRCOEFFE0NB
                                    405 	.globl _AX5043_TXPWRCOEFFD1NB
                                    406 	.globl _AX5043_TXPWRCOEFFD0NB
                                    407 	.globl _AX5043_TXPWRCOEFFC1NB
                                    408 	.globl _AX5043_TXPWRCOEFFC0NB
                                    409 	.globl _AX5043_TXPWRCOEFFB1NB
                                    410 	.globl _AX5043_TXPWRCOEFFB0NB
                                    411 	.globl _AX5043_TXPWRCOEFFA1NB
                                    412 	.globl _AX5043_TXPWRCOEFFA0NB
                                    413 	.globl _AX5043_TRKRFFREQ2NB
                                    414 	.globl _AX5043_TRKRFFREQ1NB
                                    415 	.globl _AX5043_TRKRFFREQ0NB
                                    416 	.globl _AX5043_TRKPHASE1NB
                                    417 	.globl _AX5043_TRKPHASE0NB
                                    418 	.globl _AX5043_TRKFSKDEMOD1NB
                                    419 	.globl _AX5043_TRKFSKDEMOD0NB
                                    420 	.globl _AX5043_TRKFREQ1NB
                                    421 	.globl _AX5043_TRKFREQ0NB
                                    422 	.globl _AX5043_TRKDATARATE2NB
                                    423 	.globl _AX5043_TRKDATARATE1NB
                                    424 	.globl _AX5043_TRKDATARATE0NB
                                    425 	.globl _AX5043_TRKAMPLITUDE1NB
                                    426 	.globl _AX5043_TRKAMPLITUDE0NB
                                    427 	.globl _AX5043_TRKAFSKDEMOD1NB
                                    428 	.globl _AX5043_TRKAFSKDEMOD0NB
                                    429 	.globl _AX5043_TMGTXSETTLENB
                                    430 	.globl _AX5043_TMGTXBOOSTNB
                                    431 	.globl _AX5043_TMGRXSETTLENB
                                    432 	.globl _AX5043_TMGRXRSSINB
                                    433 	.globl _AX5043_TMGRXPREAMBLE3NB
                                    434 	.globl _AX5043_TMGRXPREAMBLE2NB
                                    435 	.globl _AX5043_TMGRXPREAMBLE1NB
                                    436 	.globl _AX5043_TMGRXOFFSACQNB
                                    437 	.globl _AX5043_TMGRXCOARSEAGCNB
                                    438 	.globl _AX5043_TMGRXBOOSTNB
                                    439 	.globl _AX5043_TMGRXAGCNB
                                    440 	.globl _AX5043_TIMER2NB
                                    441 	.globl _AX5043_TIMER1NB
                                    442 	.globl _AX5043_TIMER0NB
                                    443 	.globl _AX5043_SILICONREVISIONNB
                                    444 	.globl _AX5043_SCRATCHNB
                                    445 	.globl _AX5043_RXDATARATE2NB
                                    446 	.globl _AX5043_RXDATARATE1NB
                                    447 	.globl _AX5043_RXDATARATE0NB
                                    448 	.globl _AX5043_RSSIREFERENCENB
                                    449 	.globl _AX5043_RSSIABSTHRNB
                                    450 	.globl _AX5043_RSSINB
                                    451 	.globl _AX5043_REFNB
                                    452 	.globl _AX5043_RADIOSTATENB
                                    453 	.globl _AX5043_RADIOEVENTREQ1NB
                                    454 	.globl _AX5043_RADIOEVENTREQ0NB
                                    455 	.globl _AX5043_RADIOEVENTMASK1NB
                                    456 	.globl _AX5043_RADIOEVENTMASK0NB
                                    457 	.globl _AX5043_PWRMODENB
                                    458 	.globl _AX5043_PWRAMPNB
                                    459 	.globl _AX5043_POWSTICKYSTATNB
                                    460 	.globl _AX5043_POWSTATNB
                                    461 	.globl _AX5043_POWIRQMASKNB
                                    462 	.globl _AX5043_POWCTRL1NB
                                    463 	.globl _AX5043_PLLVCOIRNB
                                    464 	.globl _AX5043_PLLVCOINB
                                    465 	.globl _AX5043_PLLVCODIVNB
                                    466 	.globl _AX5043_PLLRNGCLKNB
                                    467 	.globl _AX5043_PLLRANGINGBNB
                                    468 	.globl _AX5043_PLLRANGINGANB
                                    469 	.globl _AX5043_PLLLOOPBOOSTNB
                                    470 	.globl _AX5043_PLLLOOPNB
                                    471 	.globl _AX5043_PLLLOCKDETNB
                                    472 	.globl _AX5043_PLLCPIBOOSTNB
                                    473 	.globl _AX5043_PLLCPINB
                                    474 	.globl _AX5043_PKTSTOREFLAGSNB
                                    475 	.globl _AX5043_PKTMISCFLAGSNB
                                    476 	.globl _AX5043_PKTCHUNKSIZENB
                                    477 	.globl _AX5043_PKTACCEPTFLAGSNB
                                    478 	.globl _AX5043_PINSTATENB
                                    479 	.globl _AX5043_PINFUNCSYSCLKNB
                                    480 	.globl _AX5043_PINFUNCPWRAMPNB
                                    481 	.globl _AX5043_PINFUNCIRQNB
                                    482 	.globl _AX5043_PINFUNCDCLKNB
                                    483 	.globl _AX5043_PINFUNCDATANB
                                    484 	.globl _AX5043_PINFUNCANTSELNB
                                    485 	.globl _AX5043_MODULATIONNB
                                    486 	.globl _AX5043_MODCFGPNB
                                    487 	.globl _AX5043_MODCFGFNB
                                    488 	.globl _AX5043_MODCFGANB
                                    489 	.globl _AX5043_MAXRFOFFSET2NB
                                    490 	.globl _AX5043_MAXRFOFFSET1NB
                                    491 	.globl _AX5043_MAXRFOFFSET0NB
                                    492 	.globl _AX5043_MAXDROFFSET2NB
                                    493 	.globl _AX5043_MAXDROFFSET1NB
                                    494 	.globl _AX5043_MAXDROFFSET0NB
                                    495 	.globl _AX5043_MATCH1PAT1NB
                                    496 	.globl _AX5043_MATCH1PAT0NB
                                    497 	.globl _AX5043_MATCH1MINNB
                                    498 	.globl _AX5043_MATCH1MAXNB
                                    499 	.globl _AX5043_MATCH1LENNB
                                    500 	.globl _AX5043_MATCH0PAT3NB
                                    501 	.globl _AX5043_MATCH0PAT2NB
                                    502 	.globl _AX5043_MATCH0PAT1NB
                                    503 	.globl _AX5043_MATCH0PAT0NB
                                    504 	.globl _AX5043_MATCH0MINNB
                                    505 	.globl _AX5043_MATCH0MAXNB
                                    506 	.globl _AX5043_MATCH0LENNB
                                    507 	.globl _AX5043_LPOSCSTATUSNB
                                    508 	.globl _AX5043_LPOSCREF1NB
                                    509 	.globl _AX5043_LPOSCREF0NB
                                    510 	.globl _AX5043_LPOSCPER1NB
                                    511 	.globl _AX5043_LPOSCPER0NB
                                    512 	.globl _AX5043_LPOSCKFILT1NB
                                    513 	.globl _AX5043_LPOSCKFILT0NB
                                    514 	.globl _AX5043_LPOSCFREQ1NB
                                    515 	.globl _AX5043_LPOSCFREQ0NB
                                    516 	.globl _AX5043_LPOSCCONFIGNB
                                    517 	.globl _AX5043_IRQREQUEST1NB
                                    518 	.globl _AX5043_IRQREQUEST0NB
                                    519 	.globl _AX5043_IRQMASK1NB
                                    520 	.globl _AX5043_IRQMASK0NB
                                    521 	.globl _AX5043_IRQINVERSION1NB
                                    522 	.globl _AX5043_IRQINVERSION0NB
                                    523 	.globl _AX5043_IFFREQ1NB
                                    524 	.globl _AX5043_IFFREQ0NB
                                    525 	.globl _AX5043_GPADCPERIODNB
                                    526 	.globl _AX5043_GPADCCTRLNB
                                    527 	.globl _AX5043_GPADC13VALUE1NB
                                    528 	.globl _AX5043_GPADC13VALUE0NB
                                    529 	.globl _AX5043_FSKDMIN1NB
                                    530 	.globl _AX5043_FSKDMIN0NB
                                    531 	.globl _AX5043_FSKDMAX1NB
                                    532 	.globl _AX5043_FSKDMAX0NB
                                    533 	.globl _AX5043_FSKDEV2NB
                                    534 	.globl _AX5043_FSKDEV1NB
                                    535 	.globl _AX5043_FSKDEV0NB
                                    536 	.globl _AX5043_FREQB3NB
                                    537 	.globl _AX5043_FREQB2NB
                                    538 	.globl _AX5043_FREQB1NB
                                    539 	.globl _AX5043_FREQB0NB
                                    540 	.globl _AX5043_FREQA3NB
                                    541 	.globl _AX5043_FREQA2NB
                                    542 	.globl _AX5043_FREQA1NB
                                    543 	.globl _AX5043_FREQA0NB
                                    544 	.globl _AX5043_FRAMINGNB
                                    545 	.globl _AX5043_FIFOTHRESH1NB
                                    546 	.globl _AX5043_FIFOTHRESH0NB
                                    547 	.globl _AX5043_FIFOSTATNB
                                    548 	.globl _AX5043_FIFOFREE1NB
                                    549 	.globl _AX5043_FIFOFREE0NB
                                    550 	.globl _AX5043_FIFODATANB
                                    551 	.globl _AX5043_FIFOCOUNT1NB
                                    552 	.globl _AX5043_FIFOCOUNT0NB
                                    553 	.globl _AX5043_FECSYNCNB
                                    554 	.globl _AX5043_FECSTATUSNB
                                    555 	.globl _AX5043_FECNB
                                    556 	.globl _AX5043_ENCODINGNB
                                    557 	.globl _AX5043_DIVERSITYNB
                                    558 	.globl _AX5043_DECIMATIONNB
                                    559 	.globl _AX5043_DACVALUE1NB
                                    560 	.globl _AX5043_DACVALUE0NB
                                    561 	.globl _AX5043_DACCONFIGNB
                                    562 	.globl _AX5043_CRCINIT3NB
                                    563 	.globl _AX5043_CRCINIT2NB
                                    564 	.globl _AX5043_CRCINIT1NB
                                    565 	.globl _AX5043_CRCINIT0NB
                                    566 	.globl _AX5043_BGNDRSSITHRNB
                                    567 	.globl _AX5043_BGNDRSSIGAINNB
                                    568 	.globl _AX5043_BGNDRSSINB
                                    569 	.globl _AX5043_BBTUNENB
                                    570 	.globl _AX5043_BBOFFSCAPNB
                                    571 	.globl _AX5043_AMPLFILTERNB
                                    572 	.globl _AX5043_AGCCOUNTERNB
                                    573 	.globl _AX5043_AFSKSPACE1NB
                                    574 	.globl _AX5043_AFSKSPACE0NB
                                    575 	.globl _AX5043_AFSKMARK1NB
                                    576 	.globl _AX5043_AFSKMARK0NB
                                    577 	.globl _AX5043_AFSKCTRLNB
                                    578 	.globl _AX5043_TIMEGAIN3
                                    579 	.globl _AX5043_TIMEGAIN2
                                    580 	.globl _AX5043_TIMEGAIN1
                                    581 	.globl _AX5043_TIMEGAIN0
                                    582 	.globl _AX5043_RXPARAMSETS
                                    583 	.globl _AX5043_RXPARAMCURSET
                                    584 	.globl _AX5043_PKTMAXLEN
                                    585 	.globl _AX5043_PKTLENOFFSET
                                    586 	.globl _AX5043_PKTLENCFG
                                    587 	.globl _AX5043_PKTADDRMASK3
                                    588 	.globl _AX5043_PKTADDRMASK2
                                    589 	.globl _AX5043_PKTADDRMASK1
                                    590 	.globl _AX5043_PKTADDRMASK0
                                    591 	.globl _AX5043_PKTADDRCFG
                                    592 	.globl _AX5043_PKTADDR3
                                    593 	.globl _AX5043_PKTADDR2
                                    594 	.globl _AX5043_PKTADDR1
                                    595 	.globl _AX5043_PKTADDR0
                                    596 	.globl _AX5043_PHASEGAIN3
                                    597 	.globl _AX5043_PHASEGAIN2
                                    598 	.globl _AX5043_PHASEGAIN1
                                    599 	.globl _AX5043_PHASEGAIN0
                                    600 	.globl _AX5043_FREQUENCYLEAK
                                    601 	.globl _AX5043_FREQUENCYGAIND3
                                    602 	.globl _AX5043_FREQUENCYGAIND2
                                    603 	.globl _AX5043_FREQUENCYGAIND1
                                    604 	.globl _AX5043_FREQUENCYGAIND0
                                    605 	.globl _AX5043_FREQUENCYGAINC3
                                    606 	.globl _AX5043_FREQUENCYGAINC2
                                    607 	.globl _AX5043_FREQUENCYGAINC1
                                    608 	.globl _AX5043_FREQUENCYGAINC0
                                    609 	.globl _AX5043_FREQUENCYGAINB3
                                    610 	.globl _AX5043_FREQUENCYGAINB2
                                    611 	.globl _AX5043_FREQUENCYGAINB1
                                    612 	.globl _AX5043_FREQUENCYGAINB0
                                    613 	.globl _AX5043_FREQUENCYGAINA3
                                    614 	.globl _AX5043_FREQUENCYGAINA2
                                    615 	.globl _AX5043_FREQUENCYGAINA1
                                    616 	.globl _AX5043_FREQUENCYGAINA0
                                    617 	.globl _AX5043_FREQDEV13
                                    618 	.globl _AX5043_FREQDEV12
                                    619 	.globl _AX5043_FREQDEV11
                                    620 	.globl _AX5043_FREQDEV10
                                    621 	.globl _AX5043_FREQDEV03
                                    622 	.globl _AX5043_FREQDEV02
                                    623 	.globl _AX5043_FREQDEV01
                                    624 	.globl _AX5043_FREQDEV00
                                    625 	.globl _AX5043_FOURFSK3
                                    626 	.globl _AX5043_FOURFSK2
                                    627 	.globl _AX5043_FOURFSK1
                                    628 	.globl _AX5043_FOURFSK0
                                    629 	.globl _AX5043_DRGAIN3
                                    630 	.globl _AX5043_DRGAIN2
                                    631 	.globl _AX5043_DRGAIN1
                                    632 	.globl _AX5043_DRGAIN0
                                    633 	.globl _AX5043_BBOFFSRES3
                                    634 	.globl _AX5043_BBOFFSRES2
                                    635 	.globl _AX5043_BBOFFSRES1
                                    636 	.globl _AX5043_BBOFFSRES0
                                    637 	.globl _AX5043_AMPLITUDEGAIN3
                                    638 	.globl _AX5043_AMPLITUDEGAIN2
                                    639 	.globl _AX5043_AMPLITUDEGAIN1
                                    640 	.globl _AX5043_AMPLITUDEGAIN0
                                    641 	.globl _AX5043_AGCTARGET3
                                    642 	.globl _AX5043_AGCTARGET2
                                    643 	.globl _AX5043_AGCTARGET1
                                    644 	.globl _AX5043_AGCTARGET0
                                    645 	.globl _AX5043_AGCMINMAX3
                                    646 	.globl _AX5043_AGCMINMAX2
                                    647 	.globl _AX5043_AGCMINMAX1
                                    648 	.globl _AX5043_AGCMINMAX0
                                    649 	.globl _AX5043_AGCGAIN3
                                    650 	.globl _AX5043_AGCGAIN2
                                    651 	.globl _AX5043_AGCGAIN1
                                    652 	.globl _AX5043_AGCGAIN0
                                    653 	.globl _AX5043_AGCAHYST3
                                    654 	.globl _AX5043_AGCAHYST2
                                    655 	.globl _AX5043_AGCAHYST1
                                    656 	.globl _AX5043_AGCAHYST0
                                    657 	.globl _AX5043_0xF44
                                    658 	.globl _AX5043_0xF35
                                    659 	.globl _AX5043_0xF34
                                    660 	.globl _AX5043_0xF33
                                    661 	.globl _AX5043_0xF32
                                    662 	.globl _AX5043_0xF31
                                    663 	.globl _AX5043_0xF30
                                    664 	.globl _AX5043_0xF26
                                    665 	.globl _AX5043_0xF23
                                    666 	.globl _AX5043_0xF22
                                    667 	.globl _AX5043_0xF21
                                    668 	.globl _AX5043_0xF1C
                                    669 	.globl _AX5043_0xF18
                                    670 	.globl _AX5043_0xF0C
                                    671 	.globl _AX5043_0xF00
                                    672 	.globl _AX5043_XTALSTATUS
                                    673 	.globl _AX5043_XTALOSC
                                    674 	.globl _AX5043_XTALCAP
                                    675 	.globl _AX5043_XTALAMPL
                                    676 	.globl _AX5043_WAKEUPXOEARLY
                                    677 	.globl _AX5043_WAKEUPTIMER1
                                    678 	.globl _AX5043_WAKEUPTIMER0
                                    679 	.globl _AX5043_WAKEUPFREQ1
                                    680 	.globl _AX5043_WAKEUPFREQ0
                                    681 	.globl _AX5043_WAKEUP1
                                    682 	.globl _AX5043_WAKEUP0
                                    683 	.globl _AX5043_TXRATE2
                                    684 	.globl _AX5043_TXRATE1
                                    685 	.globl _AX5043_TXRATE0
                                    686 	.globl _AX5043_TXPWRCOEFFE1
                                    687 	.globl _AX5043_TXPWRCOEFFE0
                                    688 	.globl _AX5043_TXPWRCOEFFD1
                                    689 	.globl _AX5043_TXPWRCOEFFD0
                                    690 	.globl _AX5043_TXPWRCOEFFC1
                                    691 	.globl _AX5043_TXPWRCOEFFC0
                                    692 	.globl _AX5043_TXPWRCOEFFB1
                                    693 	.globl _AX5043_TXPWRCOEFFB0
                                    694 	.globl _AX5043_TXPWRCOEFFA1
                                    695 	.globl _AX5043_TXPWRCOEFFA0
                                    696 	.globl _AX5043_TRKRFFREQ2
                                    697 	.globl _AX5043_TRKRFFREQ1
                                    698 	.globl _AX5043_TRKRFFREQ0
                                    699 	.globl _AX5043_TRKPHASE1
                                    700 	.globl _AX5043_TRKPHASE0
                                    701 	.globl _AX5043_TRKFSKDEMOD1
                                    702 	.globl _AX5043_TRKFSKDEMOD0
                                    703 	.globl _AX5043_TRKFREQ1
                                    704 	.globl _AX5043_TRKFREQ0
                                    705 	.globl _AX5043_TRKDATARATE2
                                    706 	.globl _AX5043_TRKDATARATE1
                                    707 	.globl _AX5043_TRKDATARATE0
                                    708 	.globl _AX5043_TRKAMPLITUDE1
                                    709 	.globl _AX5043_TRKAMPLITUDE0
                                    710 	.globl _AX5043_TRKAFSKDEMOD1
                                    711 	.globl _AX5043_TRKAFSKDEMOD0
                                    712 	.globl _AX5043_TMGTXSETTLE
                                    713 	.globl _AX5043_TMGTXBOOST
                                    714 	.globl _AX5043_TMGRXSETTLE
                                    715 	.globl _AX5043_TMGRXRSSI
                                    716 	.globl _AX5043_TMGRXPREAMBLE3
                                    717 	.globl _AX5043_TMGRXPREAMBLE2
                                    718 	.globl _AX5043_TMGRXPREAMBLE1
                                    719 	.globl _AX5043_TMGRXOFFSACQ
                                    720 	.globl _AX5043_TMGRXCOARSEAGC
                                    721 	.globl _AX5043_TMGRXBOOST
                                    722 	.globl _AX5043_TMGRXAGC
                                    723 	.globl _AX5043_TIMER2
                                    724 	.globl _AX5043_TIMER1
                                    725 	.globl _AX5043_TIMER0
                                    726 	.globl _AX5043_SILICONREVISION
                                    727 	.globl _AX5043_SCRATCH
                                    728 	.globl _AX5043_RXDATARATE2
                                    729 	.globl _AX5043_RXDATARATE1
                                    730 	.globl _AX5043_RXDATARATE0
                                    731 	.globl _AX5043_RSSIREFERENCE
                                    732 	.globl _AX5043_RSSIABSTHR
                                    733 	.globl _AX5043_RSSI
                                    734 	.globl _AX5043_REF
                                    735 	.globl _AX5043_RADIOSTATE
                                    736 	.globl _AX5043_RADIOEVENTREQ1
                                    737 	.globl _AX5043_RADIOEVENTREQ0
                                    738 	.globl _AX5043_RADIOEVENTMASK1
                                    739 	.globl _AX5043_RADIOEVENTMASK0
                                    740 	.globl _AX5043_PWRMODE
                                    741 	.globl _AX5043_PWRAMP
                                    742 	.globl _AX5043_POWSTICKYSTAT
                                    743 	.globl _AX5043_POWSTAT
                                    744 	.globl _AX5043_POWIRQMASK
                                    745 	.globl _AX5043_POWCTRL1
                                    746 	.globl _AX5043_PLLVCOIR
                                    747 	.globl _AX5043_PLLVCOI
                                    748 	.globl _AX5043_PLLVCODIV
                                    749 	.globl _AX5043_PLLRNGCLK
                                    750 	.globl _AX5043_PLLRANGINGB
                                    751 	.globl _AX5043_PLLRANGINGA
                                    752 	.globl _AX5043_PLLLOOPBOOST
                                    753 	.globl _AX5043_PLLLOOP
                                    754 	.globl _AX5043_PLLLOCKDET
                                    755 	.globl _AX5043_PLLCPIBOOST
                                    756 	.globl _AX5043_PLLCPI
                                    757 	.globl _AX5043_PKTSTOREFLAGS
                                    758 	.globl _AX5043_PKTMISCFLAGS
                                    759 	.globl _AX5043_PKTCHUNKSIZE
                                    760 	.globl _AX5043_PKTACCEPTFLAGS
                                    761 	.globl _AX5043_PINSTATE
                                    762 	.globl _AX5043_PINFUNCSYSCLK
                                    763 	.globl _AX5043_PINFUNCPWRAMP
                                    764 	.globl _AX5043_PINFUNCIRQ
                                    765 	.globl _AX5043_PINFUNCDCLK
                                    766 	.globl _AX5043_PINFUNCDATA
                                    767 	.globl _AX5043_PINFUNCANTSEL
                                    768 	.globl _AX5043_MODULATION
                                    769 	.globl _AX5043_MODCFGP
                                    770 	.globl _AX5043_MODCFGF
                                    771 	.globl _AX5043_MODCFGA
                                    772 	.globl _AX5043_MAXRFOFFSET2
                                    773 	.globl _AX5043_MAXRFOFFSET1
                                    774 	.globl _AX5043_MAXRFOFFSET0
                                    775 	.globl _AX5043_MAXDROFFSET2
                                    776 	.globl _AX5043_MAXDROFFSET1
                                    777 	.globl _AX5043_MAXDROFFSET0
                                    778 	.globl _AX5043_MATCH1PAT1
                                    779 	.globl _AX5043_MATCH1PAT0
                                    780 	.globl _AX5043_MATCH1MIN
                                    781 	.globl _AX5043_MATCH1MAX
                                    782 	.globl _AX5043_MATCH1LEN
                                    783 	.globl _AX5043_MATCH0PAT3
                                    784 	.globl _AX5043_MATCH0PAT2
                                    785 	.globl _AX5043_MATCH0PAT1
                                    786 	.globl _AX5043_MATCH0PAT0
                                    787 	.globl _AX5043_MATCH0MIN
                                    788 	.globl _AX5043_MATCH0MAX
                                    789 	.globl _AX5043_MATCH0LEN
                                    790 	.globl _AX5043_LPOSCSTATUS
                                    791 	.globl _AX5043_LPOSCREF1
                                    792 	.globl _AX5043_LPOSCREF0
                                    793 	.globl _AX5043_LPOSCPER1
                                    794 	.globl _AX5043_LPOSCPER0
                                    795 	.globl _AX5043_LPOSCKFILT1
                                    796 	.globl _AX5043_LPOSCKFILT0
                                    797 	.globl _AX5043_LPOSCFREQ1
                                    798 	.globl _AX5043_LPOSCFREQ0
                                    799 	.globl _AX5043_LPOSCCONFIG
                                    800 	.globl _AX5043_IRQREQUEST1
                                    801 	.globl _AX5043_IRQREQUEST0
                                    802 	.globl _AX5043_IRQMASK1
                                    803 	.globl _AX5043_IRQMASK0
                                    804 	.globl _AX5043_IRQINVERSION1
                                    805 	.globl _AX5043_IRQINVERSION0
                                    806 	.globl _AX5043_IFFREQ1
                                    807 	.globl _AX5043_IFFREQ0
                                    808 	.globl _AX5043_GPADCPERIOD
                                    809 	.globl _AX5043_GPADCCTRL
                                    810 	.globl _AX5043_GPADC13VALUE1
                                    811 	.globl _AX5043_GPADC13VALUE0
                                    812 	.globl _AX5043_FSKDMIN1
                                    813 	.globl _AX5043_FSKDMIN0
                                    814 	.globl _AX5043_FSKDMAX1
                                    815 	.globl _AX5043_FSKDMAX0
                                    816 	.globl _AX5043_FSKDEV2
                                    817 	.globl _AX5043_FSKDEV1
                                    818 	.globl _AX5043_FSKDEV0
                                    819 	.globl _AX5043_FREQB3
                                    820 	.globl _AX5043_FREQB2
                                    821 	.globl _AX5043_FREQB1
                                    822 	.globl _AX5043_FREQB0
                                    823 	.globl _AX5043_FREQA3
                                    824 	.globl _AX5043_FREQA2
                                    825 	.globl _AX5043_FREQA1
                                    826 	.globl _AX5043_FREQA0
                                    827 	.globl _AX5043_FRAMING
                                    828 	.globl _AX5043_FIFOTHRESH1
                                    829 	.globl _AX5043_FIFOTHRESH0
                                    830 	.globl _AX5043_FIFOSTAT
                                    831 	.globl _AX5043_FIFOFREE1
                                    832 	.globl _AX5043_FIFOFREE0
                                    833 	.globl _AX5043_FIFODATA
                                    834 	.globl _AX5043_FIFOCOUNT1
                                    835 	.globl _AX5043_FIFOCOUNT0
                                    836 	.globl _AX5043_FECSYNC
                                    837 	.globl _AX5043_FECSTATUS
                                    838 	.globl _AX5043_FEC
                                    839 	.globl _AX5043_ENCODING
                                    840 	.globl _AX5043_DIVERSITY
                                    841 	.globl _AX5043_DECIMATION
                                    842 	.globl _AX5043_DACVALUE1
                                    843 	.globl _AX5043_DACVALUE0
                                    844 	.globl _AX5043_DACCONFIG
                                    845 	.globl _AX5043_CRCINIT3
                                    846 	.globl _AX5043_CRCINIT2
                                    847 	.globl _AX5043_CRCINIT1
                                    848 	.globl _AX5043_CRCINIT0
                                    849 	.globl _AX5043_BGNDRSSITHR
                                    850 	.globl _AX5043_BGNDRSSIGAIN
                                    851 	.globl _AX5043_BGNDRSSI
                                    852 	.globl _AX5043_BBTUNE
                                    853 	.globl _AX5043_BBOFFSCAP
                                    854 	.globl _AX5043_AMPLFILTER
                                    855 	.globl _AX5043_AGCCOUNTER
                                    856 	.globl _AX5043_AFSKSPACE1
                                    857 	.globl _AX5043_AFSKSPACE0
                                    858 	.globl _AX5043_AFSKMARK1
                                    859 	.globl _AX5043_AFSKMARK0
                                    860 	.globl _AX5043_AFSKCTRL
                                    861 	.globl _XTALREADY
                                    862 	.globl _XTALOSC
                                    863 	.globl _XTALAMPL
                                    864 	.globl _SILICONREV
                                    865 	.globl _SCRATCH3
                                    866 	.globl _SCRATCH2
                                    867 	.globl _SCRATCH1
                                    868 	.globl _SCRATCH0
                                    869 	.globl _RADIOMUX
                                    870 	.globl _RADIOFSTATADDR
                                    871 	.globl _RADIOFSTATADDR1
                                    872 	.globl _RADIOFSTATADDR0
                                    873 	.globl _RADIOFDATAADDR
                                    874 	.globl _RADIOFDATAADDR1
                                    875 	.globl _RADIOFDATAADDR0
                                    876 	.globl _OSCRUN
                                    877 	.globl _OSCREADY
                                    878 	.globl _OSCFORCERUN
                                    879 	.globl _OSCCALIB
                                    880 	.globl _MISCCTRL
                                    881 	.globl _LPXOSCGM
                                    882 	.globl _LPOSCREF
                                    883 	.globl _LPOSCREF1
                                    884 	.globl _LPOSCREF0
                                    885 	.globl _LPOSCPER
                                    886 	.globl _LPOSCPER1
                                    887 	.globl _LPOSCPER0
                                    888 	.globl _LPOSCKFILT
                                    889 	.globl _LPOSCKFILT1
                                    890 	.globl _LPOSCKFILT0
                                    891 	.globl _LPOSCFREQ
                                    892 	.globl _LPOSCFREQ1
                                    893 	.globl _LPOSCFREQ0
                                    894 	.globl _LPOSCCONFIG
                                    895 	.globl _PINSEL
                                    896 	.globl _PINCHGC
                                    897 	.globl _PINCHGB
                                    898 	.globl _PINCHGA
                                    899 	.globl _PALTRADIO
                                    900 	.globl _PALTC
                                    901 	.globl _PALTB
                                    902 	.globl _PALTA
                                    903 	.globl _INTCHGC
                                    904 	.globl _INTCHGB
                                    905 	.globl _INTCHGA
                                    906 	.globl _EXTIRQ
                                    907 	.globl _GPIOENABLE
                                    908 	.globl _ANALOGA
                                    909 	.globl _FRCOSCREF
                                    910 	.globl _FRCOSCREF1
                                    911 	.globl _FRCOSCREF0
                                    912 	.globl _FRCOSCPER
                                    913 	.globl _FRCOSCPER1
                                    914 	.globl _FRCOSCPER0
                                    915 	.globl _FRCOSCKFILT
                                    916 	.globl _FRCOSCKFILT1
                                    917 	.globl _FRCOSCKFILT0
                                    918 	.globl _FRCOSCFREQ
                                    919 	.globl _FRCOSCFREQ1
                                    920 	.globl _FRCOSCFREQ0
                                    921 	.globl _FRCOSCCTRL
                                    922 	.globl _FRCOSCCONFIG
                                    923 	.globl _DMA1CONFIG
                                    924 	.globl _DMA1ADDR
                                    925 	.globl _DMA1ADDR1
                                    926 	.globl _DMA1ADDR0
                                    927 	.globl _DMA0CONFIG
                                    928 	.globl _DMA0ADDR
                                    929 	.globl _DMA0ADDR1
                                    930 	.globl _DMA0ADDR0
                                    931 	.globl _ADCTUNE2
                                    932 	.globl _ADCTUNE1
                                    933 	.globl _ADCTUNE0
                                    934 	.globl _ADCCH3VAL
                                    935 	.globl _ADCCH3VAL1
                                    936 	.globl _ADCCH3VAL0
                                    937 	.globl _ADCCH2VAL
                                    938 	.globl _ADCCH2VAL1
                                    939 	.globl _ADCCH2VAL0
                                    940 	.globl _ADCCH1VAL
                                    941 	.globl _ADCCH1VAL1
                                    942 	.globl _ADCCH1VAL0
                                    943 	.globl _ADCCH0VAL
                                    944 	.globl _ADCCH0VAL1
                                    945 	.globl _ADCCH0VAL0
                                    946 ;--------------------------------------------------------
                                    947 ; special function registers
                                    948 ;--------------------------------------------------------
                                    949 	.area RSEG    (ABS,DATA)
      000000                        950 	.org 0x0000
                           0000E0   951 G$ACC$0$0 == 0x00e0
                           0000E0   952 _ACC	=	0x00e0
                           0000F0   953 G$B$0$0 == 0x00f0
                           0000F0   954 _B	=	0x00f0
                           000083   955 G$DPH$0$0 == 0x0083
                           000083   956 _DPH	=	0x0083
                           000085   957 G$DPH1$0$0 == 0x0085
                           000085   958 _DPH1	=	0x0085
                           000082   959 G$DPL$0$0 == 0x0082
                           000082   960 _DPL	=	0x0082
                           000084   961 G$DPL1$0$0 == 0x0084
                           000084   962 _DPL1	=	0x0084
                           008382   963 G$DPTR0$0$0 == 0x8382
                           008382   964 _DPTR0	=	0x8382
                           008584   965 G$DPTR1$0$0 == 0x8584
                           008584   966 _DPTR1	=	0x8584
                           000086   967 G$DPS$0$0 == 0x0086
                           000086   968 _DPS	=	0x0086
                           0000A0   969 G$E2IE$0$0 == 0x00a0
                           0000A0   970 _E2IE	=	0x00a0
                           0000C0   971 G$E2IP$0$0 == 0x00c0
                           0000C0   972 _E2IP	=	0x00c0
                           000098   973 G$EIE$0$0 == 0x0098
                           000098   974 _EIE	=	0x0098
                           0000B0   975 G$EIP$0$0 == 0x00b0
                           0000B0   976 _EIP	=	0x00b0
                           0000A8   977 G$IE$0$0 == 0x00a8
                           0000A8   978 _IE	=	0x00a8
                           0000B8   979 G$IP$0$0 == 0x00b8
                           0000B8   980 _IP	=	0x00b8
                           000087   981 G$PCON$0$0 == 0x0087
                           000087   982 _PCON	=	0x0087
                           0000D0   983 G$PSW$0$0 == 0x00d0
                           0000D0   984 _PSW	=	0x00d0
                           000081   985 G$SP$0$0 == 0x0081
                           000081   986 _SP	=	0x0081
                           0000D9   987 G$XPAGE$0$0 == 0x00d9
                           0000D9   988 _XPAGE	=	0x00d9
                           0000D9   989 G$_XPAGE$0$0 == 0x00d9
                           0000D9   990 __XPAGE	=	0x00d9
                           0000CA   991 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   992 _ADCCH0CONFIG	=	0x00ca
                           0000CB   993 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   994 _ADCCH1CONFIG	=	0x00cb
                           0000D2   995 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   996 _ADCCH2CONFIG	=	0x00d2
                           0000D3   997 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   998 _ADCCH3CONFIG	=	0x00d3
                           0000D1   999 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1  1000 _ADCCLKSRC	=	0x00d1
                           0000C9  1001 G$ADCCONV$0$0 == 0x00c9
                           0000C9  1002 _ADCCONV	=	0x00c9
                           0000E1  1003 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1  1004 _ANALOGCOMP	=	0x00e1
                           0000C6  1005 G$CLKCON$0$0 == 0x00c6
                           0000C6  1006 _CLKCON	=	0x00c6
                           0000C7  1007 G$CLKSTAT$0$0 == 0x00c7
                           0000C7  1008 _CLKSTAT	=	0x00c7
                           000097  1009 G$CODECONFIG$0$0 == 0x0097
                           000097  1010 _CODECONFIG	=	0x0097
                           0000E3  1011 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3  1012 _DBGLNKBUF	=	0x00e3
                           0000E2  1013 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2  1014 _DBGLNKSTAT	=	0x00e2
                           000089  1015 G$DIRA$0$0 == 0x0089
                           000089  1016 _DIRA	=	0x0089
                           00008A  1017 G$DIRB$0$0 == 0x008a
                           00008A  1018 _DIRB	=	0x008a
                           00008B  1019 G$DIRC$0$0 == 0x008b
                           00008B  1020 _DIRC	=	0x008b
                           00008E  1021 G$DIRR$0$0 == 0x008e
                           00008E  1022 _DIRR	=	0x008e
                           0000C8  1023 G$PINA$0$0 == 0x00c8
                           0000C8  1024 _PINA	=	0x00c8
                           0000E8  1025 G$PINB$0$0 == 0x00e8
                           0000E8  1026 _PINB	=	0x00e8
                           0000F8  1027 G$PINC$0$0 == 0x00f8
                           0000F8  1028 _PINC	=	0x00f8
                           00008D  1029 G$PINR$0$0 == 0x008d
                           00008D  1030 _PINR	=	0x008d
                           000080  1031 G$PORTA$0$0 == 0x0080
                           000080  1032 _PORTA	=	0x0080
                           000088  1033 G$PORTB$0$0 == 0x0088
                           000088  1034 _PORTB	=	0x0088
                           000090  1035 G$PORTC$0$0 == 0x0090
                           000090  1036 _PORTC	=	0x0090
                           00008C  1037 G$PORTR$0$0 == 0x008c
                           00008C  1038 _PORTR	=	0x008c
                           0000CE  1039 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE  1040 _IC0CAPT0	=	0x00ce
                           0000CF  1041 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF  1042 _IC0CAPT1	=	0x00cf
                           00CFCE  1043 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE  1044 _IC0CAPT	=	0xcfce
                           0000CC  1045 G$IC0MODE$0$0 == 0x00cc
                           0000CC  1046 _IC0MODE	=	0x00cc
                           0000CD  1047 G$IC0STATUS$0$0 == 0x00cd
                           0000CD  1048 _IC0STATUS	=	0x00cd
                           0000D6  1049 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6  1050 _IC1CAPT0	=	0x00d6
                           0000D7  1051 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7  1052 _IC1CAPT1	=	0x00d7
                           00D7D6  1053 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6  1054 _IC1CAPT	=	0xd7d6
                           0000D4  1055 G$IC1MODE$0$0 == 0x00d4
                           0000D4  1056 _IC1MODE	=	0x00d4
                           0000D5  1057 G$IC1STATUS$0$0 == 0x00d5
                           0000D5  1058 _IC1STATUS	=	0x00d5
                           000092  1059 G$NVADDR0$0$0 == 0x0092
                           000092  1060 _NVADDR0	=	0x0092
                           000093  1061 G$NVADDR1$0$0 == 0x0093
                           000093  1062 _NVADDR1	=	0x0093
                           009392  1063 G$NVADDR$0$0 == 0x9392
                           009392  1064 _NVADDR	=	0x9392
                           000094  1065 G$NVDATA0$0$0 == 0x0094
                           000094  1066 _NVDATA0	=	0x0094
                           000095  1067 G$NVDATA1$0$0 == 0x0095
                           000095  1068 _NVDATA1	=	0x0095
                           009594  1069 G$NVDATA$0$0 == 0x9594
                           009594  1070 _NVDATA	=	0x9594
                           000096  1071 G$NVKEY$0$0 == 0x0096
                           000096  1072 _NVKEY	=	0x0096
                           000091  1073 G$NVSTATUS$0$0 == 0x0091
                           000091  1074 _NVSTATUS	=	0x0091
                           0000BC  1075 G$OC0COMP0$0$0 == 0x00bc
                           0000BC  1076 _OC0COMP0	=	0x00bc
                           0000BD  1077 G$OC0COMP1$0$0 == 0x00bd
                           0000BD  1078 _OC0COMP1	=	0x00bd
                           00BDBC  1079 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC  1080 _OC0COMP	=	0xbdbc
                           0000B9  1081 G$OC0MODE$0$0 == 0x00b9
                           0000B9  1082 _OC0MODE	=	0x00b9
                           0000BA  1083 G$OC0PIN$0$0 == 0x00ba
                           0000BA  1084 _OC0PIN	=	0x00ba
                           0000BB  1085 G$OC0STATUS$0$0 == 0x00bb
                           0000BB  1086 _OC0STATUS	=	0x00bb
                           0000C4  1087 G$OC1COMP0$0$0 == 0x00c4
                           0000C4  1088 _OC1COMP0	=	0x00c4
                           0000C5  1089 G$OC1COMP1$0$0 == 0x00c5
                           0000C5  1090 _OC1COMP1	=	0x00c5
                           00C5C4  1091 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4  1092 _OC1COMP	=	0xc5c4
                           0000C1  1093 G$OC1MODE$0$0 == 0x00c1
                           0000C1  1094 _OC1MODE	=	0x00c1
                           0000C2  1095 G$OC1PIN$0$0 == 0x00c2
                           0000C2  1096 _OC1PIN	=	0x00c2
                           0000C3  1097 G$OC1STATUS$0$0 == 0x00c3
                           0000C3  1098 _OC1STATUS	=	0x00c3
                           0000B1  1099 G$RADIOACC$0$0 == 0x00b1
                           0000B1  1100 _RADIOACC	=	0x00b1
                           0000B3  1101 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3  1102 _RADIOADDR0	=	0x00b3
                           0000B2  1103 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2  1104 _RADIOADDR1	=	0x00b2
                           00B2B3  1105 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3  1106 _RADIOADDR	=	0xb2b3
                           0000B7  1107 G$RADIODATA0$0$0 == 0x00b7
                           0000B7  1108 _RADIODATA0	=	0x00b7
                           0000B6  1109 G$RADIODATA1$0$0 == 0x00b6
                           0000B6  1110 _RADIODATA1	=	0x00b6
                           0000B5  1111 G$RADIODATA2$0$0 == 0x00b5
                           0000B5  1112 _RADIODATA2	=	0x00b5
                           0000B4  1113 G$RADIODATA3$0$0 == 0x00b4
                           0000B4  1114 _RADIODATA3	=	0x00b4
                           B4B5B6B7  1115 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7  1116 _RADIODATA	=	0xb4b5b6b7
                           0000BE  1117 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE  1118 _RADIOSTAT0	=	0x00be
                           0000BF  1119 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF  1120 _RADIOSTAT1	=	0x00bf
                           00BFBE  1121 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE  1122 _RADIOSTAT	=	0xbfbe
                           0000DF  1123 G$SPCLKSRC$0$0 == 0x00df
                           0000DF  1124 _SPCLKSRC	=	0x00df
                           0000DC  1125 G$SPMODE$0$0 == 0x00dc
                           0000DC  1126 _SPMODE	=	0x00dc
                           0000DE  1127 G$SPSHREG$0$0 == 0x00de
                           0000DE  1128 _SPSHREG	=	0x00de
                           0000DD  1129 G$SPSTATUS$0$0 == 0x00dd
                           0000DD  1130 _SPSTATUS	=	0x00dd
                           00009A  1131 G$T0CLKSRC$0$0 == 0x009a
                           00009A  1132 _T0CLKSRC	=	0x009a
                           00009C  1133 G$T0CNT0$0$0 == 0x009c
                           00009C  1134 _T0CNT0	=	0x009c
                           00009D  1135 G$T0CNT1$0$0 == 0x009d
                           00009D  1136 _T0CNT1	=	0x009d
                           009D9C  1137 G$T0CNT$0$0 == 0x9d9c
                           009D9C  1138 _T0CNT	=	0x9d9c
                           000099  1139 G$T0MODE$0$0 == 0x0099
                           000099  1140 _T0MODE	=	0x0099
                           00009E  1141 G$T0PERIOD0$0$0 == 0x009e
                           00009E  1142 _T0PERIOD0	=	0x009e
                           00009F  1143 G$T0PERIOD1$0$0 == 0x009f
                           00009F  1144 _T0PERIOD1	=	0x009f
                           009F9E  1145 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E  1146 _T0PERIOD	=	0x9f9e
                           00009B  1147 G$T0STATUS$0$0 == 0x009b
                           00009B  1148 _T0STATUS	=	0x009b
                           0000A2  1149 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2  1150 _T1CLKSRC	=	0x00a2
                           0000A4  1151 G$T1CNT0$0$0 == 0x00a4
                           0000A4  1152 _T1CNT0	=	0x00a4
                           0000A5  1153 G$T1CNT1$0$0 == 0x00a5
                           0000A5  1154 _T1CNT1	=	0x00a5
                           00A5A4  1155 G$T1CNT$0$0 == 0xa5a4
                           00A5A4  1156 _T1CNT	=	0xa5a4
                           0000A1  1157 G$T1MODE$0$0 == 0x00a1
                           0000A1  1158 _T1MODE	=	0x00a1
                           0000A6  1159 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6  1160 _T1PERIOD0	=	0x00a6
                           0000A7  1161 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7  1162 _T1PERIOD1	=	0x00a7
                           00A7A6  1163 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6  1164 _T1PERIOD	=	0xa7a6
                           0000A3  1165 G$T1STATUS$0$0 == 0x00a3
                           0000A3  1166 _T1STATUS	=	0x00a3
                           0000AA  1167 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA  1168 _T2CLKSRC	=	0x00aa
                           0000AC  1169 G$T2CNT0$0$0 == 0x00ac
                           0000AC  1170 _T2CNT0	=	0x00ac
                           0000AD  1171 G$T2CNT1$0$0 == 0x00ad
                           0000AD  1172 _T2CNT1	=	0x00ad
                           00ADAC  1173 G$T2CNT$0$0 == 0xadac
                           00ADAC  1174 _T2CNT	=	0xadac
                           0000A9  1175 G$T2MODE$0$0 == 0x00a9
                           0000A9  1176 _T2MODE	=	0x00a9
                           0000AE  1177 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE  1178 _T2PERIOD0	=	0x00ae
                           0000AF  1179 G$T2PERIOD1$0$0 == 0x00af
                           0000AF  1180 _T2PERIOD1	=	0x00af
                           00AFAE  1181 G$T2PERIOD$0$0 == 0xafae
                           00AFAE  1182 _T2PERIOD	=	0xafae
                           0000AB  1183 G$T2STATUS$0$0 == 0x00ab
                           0000AB  1184 _T2STATUS	=	0x00ab
                           0000E4  1185 G$U0CTRL$0$0 == 0x00e4
                           0000E4  1186 _U0CTRL	=	0x00e4
                           0000E7  1187 G$U0MODE$0$0 == 0x00e7
                           0000E7  1188 _U0MODE	=	0x00e7
                           0000E6  1189 G$U0SHREG$0$0 == 0x00e6
                           0000E6  1190 _U0SHREG	=	0x00e6
                           0000E5  1191 G$U0STATUS$0$0 == 0x00e5
                           0000E5  1192 _U0STATUS	=	0x00e5
                           0000EC  1193 G$U1CTRL$0$0 == 0x00ec
                           0000EC  1194 _U1CTRL	=	0x00ec
                           0000EF  1195 G$U1MODE$0$0 == 0x00ef
                           0000EF  1196 _U1MODE	=	0x00ef
                           0000EE  1197 G$U1SHREG$0$0 == 0x00ee
                           0000EE  1198 _U1SHREG	=	0x00ee
                           0000ED  1199 G$U1STATUS$0$0 == 0x00ed
                           0000ED  1200 _U1STATUS	=	0x00ed
                           0000DA  1201 G$WDTCFG$0$0 == 0x00da
                           0000DA  1202 _WDTCFG	=	0x00da
                           0000DB  1203 G$WDTRESET$0$0 == 0x00db
                           0000DB  1204 _WDTRESET	=	0x00db
                           0000F1  1205 G$WTCFGA$0$0 == 0x00f1
                           0000F1  1206 _WTCFGA	=	0x00f1
                           0000F9  1207 G$WTCFGB$0$0 == 0x00f9
                           0000F9  1208 _WTCFGB	=	0x00f9
                           0000F2  1209 G$WTCNTA0$0$0 == 0x00f2
                           0000F2  1210 _WTCNTA0	=	0x00f2
                           0000F3  1211 G$WTCNTA1$0$0 == 0x00f3
                           0000F3  1212 _WTCNTA1	=	0x00f3
                           00F3F2  1213 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2  1214 _WTCNTA	=	0xf3f2
                           0000FA  1215 G$WTCNTB0$0$0 == 0x00fa
                           0000FA  1216 _WTCNTB0	=	0x00fa
                           0000FB  1217 G$WTCNTB1$0$0 == 0x00fb
                           0000FB  1218 _WTCNTB1	=	0x00fb
                           00FBFA  1219 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA  1220 _WTCNTB	=	0xfbfa
                           0000EB  1221 G$WTCNTR1$0$0 == 0x00eb
                           0000EB  1222 _WTCNTR1	=	0x00eb
                           0000F4  1223 G$WTEVTA0$0$0 == 0x00f4
                           0000F4  1224 _WTEVTA0	=	0x00f4
                           0000F5  1225 G$WTEVTA1$0$0 == 0x00f5
                           0000F5  1226 _WTEVTA1	=	0x00f5
                           00F5F4  1227 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4  1228 _WTEVTA	=	0xf5f4
                           0000F6  1229 G$WTEVTB0$0$0 == 0x00f6
                           0000F6  1230 _WTEVTB0	=	0x00f6
                           0000F7  1231 G$WTEVTB1$0$0 == 0x00f7
                           0000F7  1232 _WTEVTB1	=	0x00f7
                           00F7F6  1233 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6  1234 _WTEVTB	=	0xf7f6
                           0000FC  1235 G$WTEVTC0$0$0 == 0x00fc
                           0000FC  1236 _WTEVTC0	=	0x00fc
                           0000FD  1237 G$WTEVTC1$0$0 == 0x00fd
                           0000FD  1238 _WTEVTC1	=	0x00fd
                           00FDFC  1239 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC  1240 _WTEVTC	=	0xfdfc
                           0000FE  1241 G$WTEVTD0$0$0 == 0x00fe
                           0000FE  1242 _WTEVTD0	=	0x00fe
                           0000FF  1243 G$WTEVTD1$0$0 == 0x00ff
                           0000FF  1244 _WTEVTD1	=	0x00ff
                           00FFFE  1245 G$WTEVTD$0$0 == 0xfffe
                           00FFFE  1246 _WTEVTD	=	0xfffe
                           0000E9  1247 G$WTIRQEN$0$0 == 0x00e9
                           0000E9  1248 _WTIRQEN	=	0x00e9
                           0000EA  1249 G$WTSTAT$0$0 == 0x00ea
                           0000EA  1250 _WTSTAT	=	0x00ea
                                   1251 ;--------------------------------------------------------
                                   1252 ; special function bits
                                   1253 ;--------------------------------------------------------
                                   1254 	.area RSEG    (ABS,DATA)
      000000                       1255 	.org 0x0000
                           0000E0  1256 G$ACC_0$0$0 == 0x00e0
                           0000E0  1257 _ACC_0	=	0x00e0
                           0000E1  1258 G$ACC_1$0$0 == 0x00e1
                           0000E1  1259 _ACC_1	=	0x00e1
                           0000E2  1260 G$ACC_2$0$0 == 0x00e2
                           0000E2  1261 _ACC_2	=	0x00e2
                           0000E3  1262 G$ACC_3$0$0 == 0x00e3
                           0000E3  1263 _ACC_3	=	0x00e3
                           0000E4  1264 G$ACC_4$0$0 == 0x00e4
                           0000E4  1265 _ACC_4	=	0x00e4
                           0000E5  1266 G$ACC_5$0$0 == 0x00e5
                           0000E5  1267 _ACC_5	=	0x00e5
                           0000E6  1268 G$ACC_6$0$0 == 0x00e6
                           0000E6  1269 _ACC_6	=	0x00e6
                           0000E7  1270 G$ACC_7$0$0 == 0x00e7
                           0000E7  1271 _ACC_7	=	0x00e7
                           0000F0  1272 G$B_0$0$0 == 0x00f0
                           0000F0  1273 _B_0	=	0x00f0
                           0000F1  1274 G$B_1$0$0 == 0x00f1
                           0000F1  1275 _B_1	=	0x00f1
                           0000F2  1276 G$B_2$0$0 == 0x00f2
                           0000F2  1277 _B_2	=	0x00f2
                           0000F3  1278 G$B_3$0$0 == 0x00f3
                           0000F3  1279 _B_3	=	0x00f3
                           0000F4  1280 G$B_4$0$0 == 0x00f4
                           0000F4  1281 _B_4	=	0x00f4
                           0000F5  1282 G$B_5$0$0 == 0x00f5
                           0000F5  1283 _B_5	=	0x00f5
                           0000F6  1284 G$B_6$0$0 == 0x00f6
                           0000F6  1285 _B_6	=	0x00f6
                           0000F7  1286 G$B_7$0$0 == 0x00f7
                           0000F7  1287 _B_7	=	0x00f7
                           0000A0  1288 G$E2IE_0$0$0 == 0x00a0
                           0000A0  1289 _E2IE_0	=	0x00a0
                           0000A1  1290 G$E2IE_1$0$0 == 0x00a1
                           0000A1  1291 _E2IE_1	=	0x00a1
                           0000A2  1292 G$E2IE_2$0$0 == 0x00a2
                           0000A2  1293 _E2IE_2	=	0x00a2
                           0000A3  1294 G$E2IE_3$0$0 == 0x00a3
                           0000A3  1295 _E2IE_3	=	0x00a3
                           0000A4  1296 G$E2IE_4$0$0 == 0x00a4
                           0000A4  1297 _E2IE_4	=	0x00a4
                           0000A5  1298 G$E2IE_5$0$0 == 0x00a5
                           0000A5  1299 _E2IE_5	=	0x00a5
                           0000A6  1300 G$E2IE_6$0$0 == 0x00a6
                           0000A6  1301 _E2IE_6	=	0x00a6
                           0000A7  1302 G$E2IE_7$0$0 == 0x00a7
                           0000A7  1303 _E2IE_7	=	0x00a7
                           0000C0  1304 G$E2IP_0$0$0 == 0x00c0
                           0000C0  1305 _E2IP_0	=	0x00c0
                           0000C1  1306 G$E2IP_1$0$0 == 0x00c1
                           0000C1  1307 _E2IP_1	=	0x00c1
                           0000C2  1308 G$E2IP_2$0$0 == 0x00c2
                           0000C2  1309 _E2IP_2	=	0x00c2
                           0000C3  1310 G$E2IP_3$0$0 == 0x00c3
                           0000C3  1311 _E2IP_3	=	0x00c3
                           0000C4  1312 G$E2IP_4$0$0 == 0x00c4
                           0000C4  1313 _E2IP_4	=	0x00c4
                           0000C5  1314 G$E2IP_5$0$0 == 0x00c5
                           0000C5  1315 _E2IP_5	=	0x00c5
                           0000C6  1316 G$E2IP_6$0$0 == 0x00c6
                           0000C6  1317 _E2IP_6	=	0x00c6
                           0000C7  1318 G$E2IP_7$0$0 == 0x00c7
                           0000C7  1319 _E2IP_7	=	0x00c7
                           000098  1320 G$EIE_0$0$0 == 0x0098
                           000098  1321 _EIE_0	=	0x0098
                           000099  1322 G$EIE_1$0$0 == 0x0099
                           000099  1323 _EIE_1	=	0x0099
                           00009A  1324 G$EIE_2$0$0 == 0x009a
                           00009A  1325 _EIE_2	=	0x009a
                           00009B  1326 G$EIE_3$0$0 == 0x009b
                           00009B  1327 _EIE_3	=	0x009b
                           00009C  1328 G$EIE_4$0$0 == 0x009c
                           00009C  1329 _EIE_4	=	0x009c
                           00009D  1330 G$EIE_5$0$0 == 0x009d
                           00009D  1331 _EIE_5	=	0x009d
                           00009E  1332 G$EIE_6$0$0 == 0x009e
                           00009E  1333 _EIE_6	=	0x009e
                           00009F  1334 G$EIE_7$0$0 == 0x009f
                           00009F  1335 _EIE_7	=	0x009f
                           0000B0  1336 G$EIP_0$0$0 == 0x00b0
                           0000B0  1337 _EIP_0	=	0x00b0
                           0000B1  1338 G$EIP_1$0$0 == 0x00b1
                           0000B1  1339 _EIP_1	=	0x00b1
                           0000B2  1340 G$EIP_2$0$0 == 0x00b2
                           0000B2  1341 _EIP_2	=	0x00b2
                           0000B3  1342 G$EIP_3$0$0 == 0x00b3
                           0000B3  1343 _EIP_3	=	0x00b3
                           0000B4  1344 G$EIP_4$0$0 == 0x00b4
                           0000B4  1345 _EIP_4	=	0x00b4
                           0000B5  1346 G$EIP_5$0$0 == 0x00b5
                           0000B5  1347 _EIP_5	=	0x00b5
                           0000B6  1348 G$EIP_6$0$0 == 0x00b6
                           0000B6  1349 _EIP_6	=	0x00b6
                           0000B7  1350 G$EIP_7$0$0 == 0x00b7
                           0000B7  1351 _EIP_7	=	0x00b7
                           0000A8  1352 G$IE_0$0$0 == 0x00a8
                           0000A8  1353 _IE_0	=	0x00a8
                           0000A9  1354 G$IE_1$0$0 == 0x00a9
                           0000A9  1355 _IE_1	=	0x00a9
                           0000AA  1356 G$IE_2$0$0 == 0x00aa
                           0000AA  1357 _IE_2	=	0x00aa
                           0000AB  1358 G$IE_3$0$0 == 0x00ab
                           0000AB  1359 _IE_3	=	0x00ab
                           0000AC  1360 G$IE_4$0$0 == 0x00ac
                           0000AC  1361 _IE_4	=	0x00ac
                           0000AD  1362 G$IE_5$0$0 == 0x00ad
                           0000AD  1363 _IE_5	=	0x00ad
                           0000AE  1364 G$IE_6$0$0 == 0x00ae
                           0000AE  1365 _IE_6	=	0x00ae
                           0000AF  1366 G$IE_7$0$0 == 0x00af
                           0000AF  1367 _IE_7	=	0x00af
                           0000AF  1368 G$EA$0$0 == 0x00af
                           0000AF  1369 _EA	=	0x00af
                           0000B8  1370 G$IP_0$0$0 == 0x00b8
                           0000B8  1371 _IP_0	=	0x00b8
                           0000B9  1372 G$IP_1$0$0 == 0x00b9
                           0000B9  1373 _IP_1	=	0x00b9
                           0000BA  1374 G$IP_2$0$0 == 0x00ba
                           0000BA  1375 _IP_2	=	0x00ba
                           0000BB  1376 G$IP_3$0$0 == 0x00bb
                           0000BB  1377 _IP_3	=	0x00bb
                           0000BC  1378 G$IP_4$0$0 == 0x00bc
                           0000BC  1379 _IP_4	=	0x00bc
                           0000BD  1380 G$IP_5$0$0 == 0x00bd
                           0000BD  1381 _IP_5	=	0x00bd
                           0000BE  1382 G$IP_6$0$0 == 0x00be
                           0000BE  1383 _IP_6	=	0x00be
                           0000BF  1384 G$IP_7$0$0 == 0x00bf
                           0000BF  1385 _IP_7	=	0x00bf
                           0000D0  1386 G$P$0$0 == 0x00d0
                           0000D0  1387 _P	=	0x00d0
                           0000D1  1388 G$F1$0$0 == 0x00d1
                           0000D1  1389 _F1	=	0x00d1
                           0000D2  1390 G$OV$0$0 == 0x00d2
                           0000D2  1391 _OV	=	0x00d2
                           0000D3  1392 G$RS0$0$0 == 0x00d3
                           0000D3  1393 _RS0	=	0x00d3
                           0000D4  1394 G$RS1$0$0 == 0x00d4
                           0000D4  1395 _RS1	=	0x00d4
                           0000D5  1396 G$F0$0$0 == 0x00d5
                           0000D5  1397 _F0	=	0x00d5
                           0000D6  1398 G$AC$0$0 == 0x00d6
                           0000D6  1399 _AC	=	0x00d6
                           0000D7  1400 G$CY$0$0 == 0x00d7
                           0000D7  1401 _CY	=	0x00d7
                           0000C8  1402 G$PINA_0$0$0 == 0x00c8
                           0000C8  1403 _PINA_0	=	0x00c8
                           0000C9  1404 G$PINA_1$0$0 == 0x00c9
                           0000C9  1405 _PINA_1	=	0x00c9
                           0000CA  1406 G$PINA_2$0$0 == 0x00ca
                           0000CA  1407 _PINA_2	=	0x00ca
                           0000CB  1408 G$PINA_3$0$0 == 0x00cb
                           0000CB  1409 _PINA_3	=	0x00cb
                           0000CC  1410 G$PINA_4$0$0 == 0x00cc
                           0000CC  1411 _PINA_4	=	0x00cc
                           0000CD  1412 G$PINA_5$0$0 == 0x00cd
                           0000CD  1413 _PINA_5	=	0x00cd
                           0000CE  1414 G$PINA_6$0$0 == 0x00ce
                           0000CE  1415 _PINA_6	=	0x00ce
                           0000CF  1416 G$PINA_7$0$0 == 0x00cf
                           0000CF  1417 _PINA_7	=	0x00cf
                           0000E8  1418 G$PINB_0$0$0 == 0x00e8
                           0000E8  1419 _PINB_0	=	0x00e8
                           0000E9  1420 G$PINB_1$0$0 == 0x00e9
                           0000E9  1421 _PINB_1	=	0x00e9
                           0000EA  1422 G$PINB_2$0$0 == 0x00ea
                           0000EA  1423 _PINB_2	=	0x00ea
                           0000EB  1424 G$PINB_3$0$0 == 0x00eb
                           0000EB  1425 _PINB_3	=	0x00eb
                           0000EC  1426 G$PINB_4$0$0 == 0x00ec
                           0000EC  1427 _PINB_4	=	0x00ec
                           0000ED  1428 G$PINB_5$0$0 == 0x00ed
                           0000ED  1429 _PINB_5	=	0x00ed
                           0000EE  1430 G$PINB_6$0$0 == 0x00ee
                           0000EE  1431 _PINB_6	=	0x00ee
                           0000EF  1432 G$PINB_7$0$0 == 0x00ef
                           0000EF  1433 _PINB_7	=	0x00ef
                           0000F8  1434 G$PINC_0$0$0 == 0x00f8
                           0000F8  1435 _PINC_0	=	0x00f8
                           0000F9  1436 G$PINC_1$0$0 == 0x00f9
                           0000F9  1437 _PINC_1	=	0x00f9
                           0000FA  1438 G$PINC_2$0$0 == 0x00fa
                           0000FA  1439 _PINC_2	=	0x00fa
                           0000FB  1440 G$PINC_3$0$0 == 0x00fb
                           0000FB  1441 _PINC_3	=	0x00fb
                           0000FC  1442 G$PINC_4$0$0 == 0x00fc
                           0000FC  1443 _PINC_4	=	0x00fc
                           0000FD  1444 G$PINC_5$0$0 == 0x00fd
                           0000FD  1445 _PINC_5	=	0x00fd
                           0000FE  1446 G$PINC_6$0$0 == 0x00fe
                           0000FE  1447 _PINC_6	=	0x00fe
                           0000FF  1448 G$PINC_7$0$0 == 0x00ff
                           0000FF  1449 _PINC_7	=	0x00ff
                           000080  1450 G$PORTA_0$0$0 == 0x0080
                           000080  1451 _PORTA_0	=	0x0080
                           000081  1452 G$PORTA_1$0$0 == 0x0081
                           000081  1453 _PORTA_1	=	0x0081
                           000082  1454 G$PORTA_2$0$0 == 0x0082
                           000082  1455 _PORTA_2	=	0x0082
                           000083  1456 G$PORTA_3$0$0 == 0x0083
                           000083  1457 _PORTA_3	=	0x0083
                           000084  1458 G$PORTA_4$0$0 == 0x0084
                           000084  1459 _PORTA_4	=	0x0084
                           000085  1460 G$PORTA_5$0$0 == 0x0085
                           000085  1461 _PORTA_5	=	0x0085
                           000086  1462 G$PORTA_6$0$0 == 0x0086
                           000086  1463 _PORTA_6	=	0x0086
                           000087  1464 G$PORTA_7$0$0 == 0x0087
                           000087  1465 _PORTA_7	=	0x0087
                           000088  1466 G$PORTB_0$0$0 == 0x0088
                           000088  1467 _PORTB_0	=	0x0088
                           000089  1468 G$PORTB_1$0$0 == 0x0089
                           000089  1469 _PORTB_1	=	0x0089
                           00008A  1470 G$PORTB_2$0$0 == 0x008a
                           00008A  1471 _PORTB_2	=	0x008a
                           00008B  1472 G$PORTB_3$0$0 == 0x008b
                           00008B  1473 _PORTB_3	=	0x008b
                           00008C  1474 G$PORTB_4$0$0 == 0x008c
                           00008C  1475 _PORTB_4	=	0x008c
                           00008D  1476 G$PORTB_5$0$0 == 0x008d
                           00008D  1477 _PORTB_5	=	0x008d
                           00008E  1478 G$PORTB_6$0$0 == 0x008e
                           00008E  1479 _PORTB_6	=	0x008e
                           00008F  1480 G$PORTB_7$0$0 == 0x008f
                           00008F  1481 _PORTB_7	=	0x008f
                           000090  1482 G$PORTC_0$0$0 == 0x0090
                           000090  1483 _PORTC_0	=	0x0090
                           000091  1484 G$PORTC_1$0$0 == 0x0091
                           000091  1485 _PORTC_1	=	0x0091
                           000092  1486 G$PORTC_2$0$0 == 0x0092
                           000092  1487 _PORTC_2	=	0x0092
                           000093  1488 G$PORTC_3$0$0 == 0x0093
                           000093  1489 _PORTC_3	=	0x0093
                           000094  1490 G$PORTC_4$0$0 == 0x0094
                           000094  1491 _PORTC_4	=	0x0094
                           000095  1492 G$PORTC_5$0$0 == 0x0095
                           000095  1493 _PORTC_5	=	0x0095
                           000096  1494 G$PORTC_6$0$0 == 0x0096
                           000096  1495 _PORTC_6	=	0x0096
                           000097  1496 G$PORTC_7$0$0 == 0x0097
                           000097  1497 _PORTC_7	=	0x0097
                                   1498 ;--------------------------------------------------------
                                   1499 ; overlayable register banks
                                   1500 ;--------------------------------------------------------
                                   1501 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                       1502 	.ds 8
                                   1503 ;--------------------------------------------------------
                                   1504 ; internal ram data
                                   1505 ;--------------------------------------------------------
                                   1506 	.area DSEG    (DATA)
                                   1507 ;--------------------------------------------------------
                                   1508 ; overlayable items in internal ram 
                                   1509 ;--------------------------------------------------------
                                   1510 ;--------------------------------------------------------
                                   1511 ; indirectly addressable internal ram data
                                   1512 ;--------------------------------------------------------
                                   1513 	.area ISEG    (DATA)
                                   1514 ;--------------------------------------------------------
                                   1515 ; absolute internal ram data
                                   1516 ;--------------------------------------------------------
                                   1517 	.area IABS    (ABS,DATA)
                                   1518 	.area IABS    (ABS,DATA)
                                   1519 ;--------------------------------------------------------
                                   1520 ; bit data
                                   1521 ;--------------------------------------------------------
                                   1522 	.area BSEG    (BIT)
                                   1523 ;--------------------------------------------------------
                                   1524 ; paged external ram data
                                   1525 ;--------------------------------------------------------
                                   1526 	.area PSEG    (PAG,XDATA)
                                   1527 ;--------------------------------------------------------
                                   1528 ; external ram data
                                   1529 ;--------------------------------------------------------
                                   1530 	.area XSEG    (XDATA)
                           007020  1531 G$ADCCH0VAL0$0$0 == 0x7020
                           007020  1532 _ADCCH0VAL0	=	0x7020
                           007021  1533 G$ADCCH0VAL1$0$0 == 0x7021
                           007021  1534 _ADCCH0VAL1	=	0x7021
                           007020  1535 G$ADCCH0VAL$0$0 == 0x7020
                           007020  1536 _ADCCH0VAL	=	0x7020
                           007022  1537 G$ADCCH1VAL0$0$0 == 0x7022
                           007022  1538 _ADCCH1VAL0	=	0x7022
                           007023  1539 G$ADCCH1VAL1$0$0 == 0x7023
                           007023  1540 _ADCCH1VAL1	=	0x7023
                           007022  1541 G$ADCCH1VAL$0$0 == 0x7022
                           007022  1542 _ADCCH1VAL	=	0x7022
                           007024  1543 G$ADCCH2VAL0$0$0 == 0x7024
                           007024  1544 _ADCCH2VAL0	=	0x7024
                           007025  1545 G$ADCCH2VAL1$0$0 == 0x7025
                           007025  1546 _ADCCH2VAL1	=	0x7025
                           007024  1547 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1548 _ADCCH2VAL	=	0x7024
                           007026  1549 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1550 _ADCCH3VAL0	=	0x7026
                           007027  1551 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1552 _ADCCH3VAL1	=	0x7027
                           007026  1553 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1554 _ADCCH3VAL	=	0x7026
                           007028  1555 G$ADCTUNE0$0$0 == 0x7028
                           007028  1556 _ADCTUNE0	=	0x7028
                           007029  1557 G$ADCTUNE1$0$0 == 0x7029
                           007029  1558 _ADCTUNE1	=	0x7029
                           00702A  1559 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1560 _ADCTUNE2	=	0x702a
                           007010  1561 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1562 _DMA0ADDR0	=	0x7010
                           007011  1563 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1564 _DMA0ADDR1	=	0x7011
                           007010  1565 G$DMA0ADDR$0$0 == 0x7010
                           007010  1566 _DMA0ADDR	=	0x7010
                           007014  1567 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1568 _DMA0CONFIG	=	0x7014
                           007012  1569 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1570 _DMA1ADDR0	=	0x7012
                           007013  1571 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1572 _DMA1ADDR1	=	0x7013
                           007012  1573 G$DMA1ADDR$0$0 == 0x7012
                           007012  1574 _DMA1ADDR	=	0x7012
                           007015  1575 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1576 _DMA1CONFIG	=	0x7015
                           007070  1577 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1578 _FRCOSCCONFIG	=	0x7070
                           007071  1579 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1580 _FRCOSCCTRL	=	0x7071
                           007076  1581 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1582 _FRCOSCFREQ0	=	0x7076
                           007077  1583 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1584 _FRCOSCFREQ1	=	0x7077
                           007076  1585 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1586 _FRCOSCFREQ	=	0x7076
                           007072  1587 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1588 _FRCOSCKFILT0	=	0x7072
                           007073  1589 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1590 _FRCOSCKFILT1	=	0x7073
                           007072  1591 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1592 _FRCOSCKFILT	=	0x7072
                           007078  1593 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1594 _FRCOSCPER0	=	0x7078
                           007079  1595 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1596 _FRCOSCPER1	=	0x7079
                           007078  1597 G$FRCOSCPER$0$0 == 0x7078
                           007078  1598 _FRCOSCPER	=	0x7078
                           007074  1599 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1600 _FRCOSCREF0	=	0x7074
                           007075  1601 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1602 _FRCOSCREF1	=	0x7075
                           007074  1603 G$FRCOSCREF$0$0 == 0x7074
                           007074  1604 _FRCOSCREF	=	0x7074
                           007007  1605 G$ANALOGA$0$0 == 0x7007
                           007007  1606 _ANALOGA	=	0x7007
                           00700C  1607 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1608 _GPIOENABLE	=	0x700c
                           007003  1609 G$EXTIRQ$0$0 == 0x7003
                           007003  1610 _EXTIRQ	=	0x7003
                           007000  1611 G$INTCHGA$0$0 == 0x7000
                           007000  1612 _INTCHGA	=	0x7000
                           007001  1613 G$INTCHGB$0$0 == 0x7001
                           007001  1614 _INTCHGB	=	0x7001
                           007002  1615 G$INTCHGC$0$0 == 0x7002
                           007002  1616 _INTCHGC	=	0x7002
                           007008  1617 G$PALTA$0$0 == 0x7008
                           007008  1618 _PALTA	=	0x7008
                           007009  1619 G$PALTB$0$0 == 0x7009
                           007009  1620 _PALTB	=	0x7009
                           00700A  1621 G$PALTC$0$0 == 0x700a
                           00700A  1622 _PALTC	=	0x700a
                           007046  1623 G$PALTRADIO$0$0 == 0x7046
                           007046  1624 _PALTRADIO	=	0x7046
                           007004  1625 G$PINCHGA$0$0 == 0x7004
                           007004  1626 _PINCHGA	=	0x7004
                           007005  1627 G$PINCHGB$0$0 == 0x7005
                           007005  1628 _PINCHGB	=	0x7005
                           007006  1629 G$PINCHGC$0$0 == 0x7006
                           007006  1630 _PINCHGC	=	0x7006
                           00700B  1631 G$PINSEL$0$0 == 0x700b
                           00700B  1632 _PINSEL	=	0x700b
                           007060  1633 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1634 _LPOSCCONFIG	=	0x7060
                           007066  1635 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1636 _LPOSCFREQ0	=	0x7066
                           007067  1637 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1638 _LPOSCFREQ1	=	0x7067
                           007066  1639 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1640 _LPOSCFREQ	=	0x7066
                           007062  1641 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1642 _LPOSCKFILT0	=	0x7062
                           007063  1643 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1644 _LPOSCKFILT1	=	0x7063
                           007062  1645 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1646 _LPOSCKFILT	=	0x7062
                           007068  1647 G$LPOSCPER0$0$0 == 0x7068
                           007068  1648 _LPOSCPER0	=	0x7068
                           007069  1649 G$LPOSCPER1$0$0 == 0x7069
                           007069  1650 _LPOSCPER1	=	0x7069
                           007068  1651 G$LPOSCPER$0$0 == 0x7068
                           007068  1652 _LPOSCPER	=	0x7068
                           007064  1653 G$LPOSCREF0$0$0 == 0x7064
                           007064  1654 _LPOSCREF0	=	0x7064
                           007065  1655 G$LPOSCREF1$0$0 == 0x7065
                           007065  1656 _LPOSCREF1	=	0x7065
                           007064  1657 G$LPOSCREF$0$0 == 0x7064
                           007064  1658 _LPOSCREF	=	0x7064
                           007054  1659 G$LPXOSCGM$0$0 == 0x7054
                           007054  1660 _LPXOSCGM	=	0x7054
                           007F01  1661 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1662 _MISCCTRL	=	0x7f01
                           007053  1663 G$OSCCALIB$0$0 == 0x7053
                           007053  1664 _OSCCALIB	=	0x7053
                           007050  1665 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1666 _OSCFORCERUN	=	0x7050
                           007052  1667 G$OSCREADY$0$0 == 0x7052
                           007052  1668 _OSCREADY	=	0x7052
                           007051  1669 G$OSCRUN$0$0 == 0x7051
                           007051  1670 _OSCRUN	=	0x7051
                           007040  1671 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1672 _RADIOFDATAADDR0	=	0x7040
                           007041  1673 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1674 _RADIOFDATAADDR1	=	0x7041
                           007040  1675 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1676 _RADIOFDATAADDR	=	0x7040
                           007042  1677 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1678 _RADIOFSTATADDR0	=	0x7042
                           007043  1679 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1680 _RADIOFSTATADDR1	=	0x7043
                           007042  1681 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1682 _RADIOFSTATADDR	=	0x7042
                           007044  1683 G$RADIOMUX$0$0 == 0x7044
                           007044  1684 _RADIOMUX	=	0x7044
                           007084  1685 G$SCRATCH0$0$0 == 0x7084
                           007084  1686 _SCRATCH0	=	0x7084
                           007085  1687 G$SCRATCH1$0$0 == 0x7085
                           007085  1688 _SCRATCH1	=	0x7085
                           007086  1689 G$SCRATCH2$0$0 == 0x7086
                           007086  1690 _SCRATCH2	=	0x7086
                           007087  1691 G$SCRATCH3$0$0 == 0x7087
                           007087  1692 _SCRATCH3	=	0x7087
                           007F00  1693 G$SILICONREV$0$0 == 0x7f00
                           007F00  1694 _SILICONREV	=	0x7f00
                           007F19  1695 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1696 _XTALAMPL	=	0x7f19
                           007F18  1697 G$XTALOSC$0$0 == 0x7f18
                           007F18  1698 _XTALOSC	=	0x7f18
                           007F1A  1699 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1700 _XTALREADY	=	0x7f1a
                           00FC06  1701 FtransmitPrem$flash_deviceid$0$0 == 0xfc06
                           00FC06  1702 _flash_deviceid	=	0xfc06
                           00FC00  1703 FtransmitPrem$flash_calsector$0$0 == 0xfc00
                           00FC00  1704 _flash_calsector	=	0xfc00
                           004114  1705 G$AX5043_AFSKCTRL$0$0 == 0x4114
                           004114  1706 _AX5043_AFSKCTRL	=	0x4114
                           004113  1707 G$AX5043_AFSKMARK0$0$0 == 0x4113
                           004113  1708 _AX5043_AFSKMARK0	=	0x4113
                           004112  1709 G$AX5043_AFSKMARK1$0$0 == 0x4112
                           004112  1710 _AX5043_AFSKMARK1	=	0x4112
                           004111  1711 G$AX5043_AFSKSPACE0$0$0 == 0x4111
                           004111  1712 _AX5043_AFSKSPACE0	=	0x4111
                           004110  1713 G$AX5043_AFSKSPACE1$0$0 == 0x4110
                           004110  1714 _AX5043_AFSKSPACE1	=	0x4110
                           004043  1715 G$AX5043_AGCCOUNTER$0$0 == 0x4043
                           004043  1716 _AX5043_AGCCOUNTER	=	0x4043
                           004115  1717 G$AX5043_AMPLFILTER$0$0 == 0x4115
                           004115  1718 _AX5043_AMPLFILTER	=	0x4115
                           004189  1719 G$AX5043_BBOFFSCAP$0$0 == 0x4189
                           004189  1720 _AX5043_BBOFFSCAP	=	0x4189
                           004188  1721 G$AX5043_BBTUNE$0$0 == 0x4188
                           004188  1722 _AX5043_BBTUNE	=	0x4188
                           004041  1723 G$AX5043_BGNDRSSI$0$0 == 0x4041
                           004041  1724 _AX5043_BGNDRSSI	=	0x4041
                           00422E  1725 G$AX5043_BGNDRSSIGAIN$0$0 == 0x422e
                           00422E  1726 _AX5043_BGNDRSSIGAIN	=	0x422e
                           00422F  1727 G$AX5043_BGNDRSSITHR$0$0 == 0x422f
                           00422F  1728 _AX5043_BGNDRSSITHR	=	0x422f
                           004017  1729 G$AX5043_CRCINIT0$0$0 == 0x4017
                           004017  1730 _AX5043_CRCINIT0	=	0x4017
                           004016  1731 G$AX5043_CRCINIT1$0$0 == 0x4016
                           004016  1732 _AX5043_CRCINIT1	=	0x4016
                           004015  1733 G$AX5043_CRCINIT2$0$0 == 0x4015
                           004015  1734 _AX5043_CRCINIT2	=	0x4015
                           004014  1735 G$AX5043_CRCINIT3$0$0 == 0x4014
                           004014  1736 _AX5043_CRCINIT3	=	0x4014
                           004332  1737 G$AX5043_DACCONFIG$0$0 == 0x4332
                           004332  1738 _AX5043_DACCONFIG	=	0x4332
                           004331  1739 G$AX5043_DACVALUE0$0$0 == 0x4331
                           004331  1740 _AX5043_DACVALUE0	=	0x4331
                           004330  1741 G$AX5043_DACVALUE1$0$0 == 0x4330
                           004330  1742 _AX5043_DACVALUE1	=	0x4330
                           004102  1743 G$AX5043_DECIMATION$0$0 == 0x4102
                           004102  1744 _AX5043_DECIMATION	=	0x4102
                           004042  1745 G$AX5043_DIVERSITY$0$0 == 0x4042
                           004042  1746 _AX5043_DIVERSITY	=	0x4042
                           004011  1747 G$AX5043_ENCODING$0$0 == 0x4011
                           004011  1748 _AX5043_ENCODING	=	0x4011
                           004018  1749 G$AX5043_FEC$0$0 == 0x4018
                           004018  1750 _AX5043_FEC	=	0x4018
                           00401A  1751 G$AX5043_FECSTATUS$0$0 == 0x401a
                           00401A  1752 _AX5043_FECSTATUS	=	0x401a
                           004019  1753 G$AX5043_FECSYNC$0$0 == 0x4019
                           004019  1754 _AX5043_FECSYNC	=	0x4019
                           00402B  1755 G$AX5043_FIFOCOUNT0$0$0 == 0x402b
                           00402B  1756 _AX5043_FIFOCOUNT0	=	0x402b
                           00402A  1757 G$AX5043_FIFOCOUNT1$0$0 == 0x402a
                           00402A  1758 _AX5043_FIFOCOUNT1	=	0x402a
                           004029  1759 G$AX5043_FIFODATA$0$0 == 0x4029
                           004029  1760 _AX5043_FIFODATA	=	0x4029
                           00402D  1761 G$AX5043_FIFOFREE0$0$0 == 0x402d
                           00402D  1762 _AX5043_FIFOFREE0	=	0x402d
                           00402C  1763 G$AX5043_FIFOFREE1$0$0 == 0x402c
                           00402C  1764 _AX5043_FIFOFREE1	=	0x402c
                           004028  1765 G$AX5043_FIFOSTAT$0$0 == 0x4028
                           004028  1766 _AX5043_FIFOSTAT	=	0x4028
                           00402F  1767 G$AX5043_FIFOTHRESH0$0$0 == 0x402f
                           00402F  1768 _AX5043_FIFOTHRESH0	=	0x402f
                           00402E  1769 G$AX5043_FIFOTHRESH1$0$0 == 0x402e
                           00402E  1770 _AX5043_FIFOTHRESH1	=	0x402e
                           004012  1771 G$AX5043_FRAMING$0$0 == 0x4012
                           004012  1772 _AX5043_FRAMING	=	0x4012
                           004037  1773 G$AX5043_FREQA0$0$0 == 0x4037
                           004037  1774 _AX5043_FREQA0	=	0x4037
                           004036  1775 G$AX5043_FREQA1$0$0 == 0x4036
                           004036  1776 _AX5043_FREQA1	=	0x4036
                           004035  1777 G$AX5043_FREQA2$0$0 == 0x4035
                           004035  1778 _AX5043_FREQA2	=	0x4035
                           004034  1779 G$AX5043_FREQA3$0$0 == 0x4034
                           004034  1780 _AX5043_FREQA3	=	0x4034
                           00403F  1781 G$AX5043_FREQB0$0$0 == 0x403f
                           00403F  1782 _AX5043_FREQB0	=	0x403f
                           00403E  1783 G$AX5043_FREQB1$0$0 == 0x403e
                           00403E  1784 _AX5043_FREQB1	=	0x403e
                           00403D  1785 G$AX5043_FREQB2$0$0 == 0x403d
                           00403D  1786 _AX5043_FREQB2	=	0x403d
                           00403C  1787 G$AX5043_FREQB3$0$0 == 0x403c
                           00403C  1788 _AX5043_FREQB3	=	0x403c
                           004163  1789 G$AX5043_FSKDEV0$0$0 == 0x4163
                           004163  1790 _AX5043_FSKDEV0	=	0x4163
                           004162  1791 G$AX5043_FSKDEV1$0$0 == 0x4162
                           004162  1792 _AX5043_FSKDEV1	=	0x4162
                           004161  1793 G$AX5043_FSKDEV2$0$0 == 0x4161
                           004161  1794 _AX5043_FSKDEV2	=	0x4161
                           00410D  1795 G$AX5043_FSKDMAX0$0$0 == 0x410d
                           00410D  1796 _AX5043_FSKDMAX0	=	0x410d
                           00410C  1797 G$AX5043_FSKDMAX1$0$0 == 0x410c
                           00410C  1798 _AX5043_FSKDMAX1	=	0x410c
                           00410F  1799 G$AX5043_FSKDMIN0$0$0 == 0x410f
                           00410F  1800 _AX5043_FSKDMIN0	=	0x410f
                           00410E  1801 G$AX5043_FSKDMIN1$0$0 == 0x410e
                           00410E  1802 _AX5043_FSKDMIN1	=	0x410e
                           004309  1803 G$AX5043_GPADC13VALUE0$0$0 == 0x4309
                           004309  1804 _AX5043_GPADC13VALUE0	=	0x4309
                           004308  1805 G$AX5043_GPADC13VALUE1$0$0 == 0x4308
                           004308  1806 _AX5043_GPADC13VALUE1	=	0x4308
                           004300  1807 G$AX5043_GPADCCTRL$0$0 == 0x4300
                           004300  1808 _AX5043_GPADCCTRL	=	0x4300
                           004301  1809 G$AX5043_GPADCPERIOD$0$0 == 0x4301
                           004301  1810 _AX5043_GPADCPERIOD	=	0x4301
                           004101  1811 G$AX5043_IFFREQ0$0$0 == 0x4101
                           004101  1812 _AX5043_IFFREQ0	=	0x4101
                           004100  1813 G$AX5043_IFFREQ1$0$0 == 0x4100
                           004100  1814 _AX5043_IFFREQ1	=	0x4100
                           00400B  1815 G$AX5043_IRQINVERSION0$0$0 == 0x400b
                           00400B  1816 _AX5043_IRQINVERSION0	=	0x400b
                           00400A  1817 G$AX5043_IRQINVERSION1$0$0 == 0x400a
                           00400A  1818 _AX5043_IRQINVERSION1	=	0x400a
                           004007  1819 G$AX5043_IRQMASK0$0$0 == 0x4007
                           004007  1820 _AX5043_IRQMASK0	=	0x4007
                           004006  1821 G$AX5043_IRQMASK1$0$0 == 0x4006
                           004006  1822 _AX5043_IRQMASK1	=	0x4006
                           00400D  1823 G$AX5043_IRQREQUEST0$0$0 == 0x400d
                           00400D  1824 _AX5043_IRQREQUEST0	=	0x400d
                           00400C  1825 G$AX5043_IRQREQUEST1$0$0 == 0x400c
                           00400C  1826 _AX5043_IRQREQUEST1	=	0x400c
                           004310  1827 G$AX5043_LPOSCCONFIG$0$0 == 0x4310
                           004310  1828 _AX5043_LPOSCCONFIG	=	0x4310
                           004317  1829 G$AX5043_LPOSCFREQ0$0$0 == 0x4317
                           004317  1830 _AX5043_LPOSCFREQ0	=	0x4317
                           004316  1831 G$AX5043_LPOSCFREQ1$0$0 == 0x4316
                           004316  1832 _AX5043_LPOSCFREQ1	=	0x4316
                           004313  1833 G$AX5043_LPOSCKFILT0$0$0 == 0x4313
                           004313  1834 _AX5043_LPOSCKFILT0	=	0x4313
                           004312  1835 G$AX5043_LPOSCKFILT1$0$0 == 0x4312
                           004312  1836 _AX5043_LPOSCKFILT1	=	0x4312
                           004319  1837 G$AX5043_LPOSCPER0$0$0 == 0x4319
                           004319  1838 _AX5043_LPOSCPER0	=	0x4319
                           004318  1839 G$AX5043_LPOSCPER1$0$0 == 0x4318
                           004318  1840 _AX5043_LPOSCPER1	=	0x4318
                           004315  1841 G$AX5043_LPOSCREF0$0$0 == 0x4315
                           004315  1842 _AX5043_LPOSCREF0	=	0x4315
                           004314  1843 G$AX5043_LPOSCREF1$0$0 == 0x4314
                           004314  1844 _AX5043_LPOSCREF1	=	0x4314
                           004311  1845 G$AX5043_LPOSCSTATUS$0$0 == 0x4311
                           004311  1846 _AX5043_LPOSCSTATUS	=	0x4311
                           004214  1847 G$AX5043_MATCH0LEN$0$0 == 0x4214
                           004214  1848 _AX5043_MATCH0LEN	=	0x4214
                           004216  1849 G$AX5043_MATCH0MAX$0$0 == 0x4216
                           004216  1850 _AX5043_MATCH0MAX	=	0x4216
                           004215  1851 G$AX5043_MATCH0MIN$0$0 == 0x4215
                           004215  1852 _AX5043_MATCH0MIN	=	0x4215
                           004213  1853 G$AX5043_MATCH0PAT0$0$0 == 0x4213
                           004213  1854 _AX5043_MATCH0PAT0	=	0x4213
                           004212  1855 G$AX5043_MATCH0PAT1$0$0 == 0x4212
                           004212  1856 _AX5043_MATCH0PAT1	=	0x4212
                           004211  1857 G$AX5043_MATCH0PAT2$0$0 == 0x4211
                           004211  1858 _AX5043_MATCH0PAT2	=	0x4211
                           004210  1859 G$AX5043_MATCH0PAT3$0$0 == 0x4210
                           004210  1860 _AX5043_MATCH0PAT3	=	0x4210
                           00421C  1861 G$AX5043_MATCH1LEN$0$0 == 0x421c
                           00421C  1862 _AX5043_MATCH1LEN	=	0x421c
                           00421E  1863 G$AX5043_MATCH1MAX$0$0 == 0x421e
                           00421E  1864 _AX5043_MATCH1MAX	=	0x421e
                           00421D  1865 G$AX5043_MATCH1MIN$0$0 == 0x421d
                           00421D  1866 _AX5043_MATCH1MIN	=	0x421d
                           004219  1867 G$AX5043_MATCH1PAT0$0$0 == 0x4219
                           004219  1868 _AX5043_MATCH1PAT0	=	0x4219
                           004218  1869 G$AX5043_MATCH1PAT1$0$0 == 0x4218
                           004218  1870 _AX5043_MATCH1PAT1	=	0x4218
                           004108  1871 G$AX5043_MAXDROFFSET0$0$0 == 0x4108
                           004108  1872 _AX5043_MAXDROFFSET0	=	0x4108
                           004107  1873 G$AX5043_MAXDROFFSET1$0$0 == 0x4107
                           004107  1874 _AX5043_MAXDROFFSET1	=	0x4107
                           004106  1875 G$AX5043_MAXDROFFSET2$0$0 == 0x4106
                           004106  1876 _AX5043_MAXDROFFSET2	=	0x4106
                           00410B  1877 G$AX5043_MAXRFOFFSET0$0$0 == 0x410b
                           00410B  1878 _AX5043_MAXRFOFFSET0	=	0x410b
                           00410A  1879 G$AX5043_MAXRFOFFSET1$0$0 == 0x410a
                           00410A  1880 _AX5043_MAXRFOFFSET1	=	0x410a
                           004109  1881 G$AX5043_MAXRFOFFSET2$0$0 == 0x4109
                           004109  1882 _AX5043_MAXRFOFFSET2	=	0x4109
                           004164  1883 G$AX5043_MODCFGA$0$0 == 0x4164
                           004164  1884 _AX5043_MODCFGA	=	0x4164
                           004160  1885 G$AX5043_MODCFGF$0$0 == 0x4160
                           004160  1886 _AX5043_MODCFGF	=	0x4160
                           004F5F  1887 G$AX5043_MODCFGP$0$0 == 0x4f5f
                           004F5F  1888 _AX5043_MODCFGP	=	0x4f5f
                           004010  1889 G$AX5043_MODULATION$0$0 == 0x4010
                           004010  1890 _AX5043_MODULATION	=	0x4010
                           004025  1891 G$AX5043_PINFUNCANTSEL$0$0 == 0x4025
                           004025  1892 _AX5043_PINFUNCANTSEL	=	0x4025
                           004023  1893 G$AX5043_PINFUNCDATA$0$0 == 0x4023
                           004023  1894 _AX5043_PINFUNCDATA	=	0x4023
                           004022  1895 G$AX5043_PINFUNCDCLK$0$0 == 0x4022
                           004022  1896 _AX5043_PINFUNCDCLK	=	0x4022
                           004024  1897 G$AX5043_PINFUNCIRQ$0$0 == 0x4024
                           004024  1898 _AX5043_PINFUNCIRQ	=	0x4024
                           004026  1899 G$AX5043_PINFUNCPWRAMP$0$0 == 0x4026
                           004026  1900 _AX5043_PINFUNCPWRAMP	=	0x4026
                           004021  1901 G$AX5043_PINFUNCSYSCLK$0$0 == 0x4021
                           004021  1902 _AX5043_PINFUNCSYSCLK	=	0x4021
                           004020  1903 G$AX5043_PINSTATE$0$0 == 0x4020
                           004020  1904 _AX5043_PINSTATE	=	0x4020
                           004233  1905 G$AX5043_PKTACCEPTFLAGS$0$0 == 0x4233
                           004233  1906 _AX5043_PKTACCEPTFLAGS	=	0x4233
                           004230  1907 G$AX5043_PKTCHUNKSIZE$0$0 == 0x4230
                           004230  1908 _AX5043_PKTCHUNKSIZE	=	0x4230
                           004231  1909 G$AX5043_PKTMISCFLAGS$0$0 == 0x4231
                           004231  1910 _AX5043_PKTMISCFLAGS	=	0x4231
                           004232  1911 G$AX5043_PKTSTOREFLAGS$0$0 == 0x4232
                           004232  1912 _AX5043_PKTSTOREFLAGS	=	0x4232
                           004031  1913 G$AX5043_PLLCPI$0$0 == 0x4031
                           004031  1914 _AX5043_PLLCPI	=	0x4031
                           004039  1915 G$AX5043_PLLCPIBOOST$0$0 == 0x4039
                           004039  1916 _AX5043_PLLCPIBOOST	=	0x4039
                           004182  1917 G$AX5043_PLLLOCKDET$0$0 == 0x4182
                           004182  1918 _AX5043_PLLLOCKDET	=	0x4182
                           004030  1919 G$AX5043_PLLLOOP$0$0 == 0x4030
                           004030  1920 _AX5043_PLLLOOP	=	0x4030
                           004038  1921 G$AX5043_PLLLOOPBOOST$0$0 == 0x4038
                           004038  1922 _AX5043_PLLLOOPBOOST	=	0x4038
                           004033  1923 G$AX5043_PLLRANGINGA$0$0 == 0x4033
                           004033  1924 _AX5043_PLLRANGINGA	=	0x4033
                           00403B  1925 G$AX5043_PLLRANGINGB$0$0 == 0x403b
                           00403B  1926 _AX5043_PLLRANGINGB	=	0x403b
                           004183  1927 G$AX5043_PLLRNGCLK$0$0 == 0x4183
                           004183  1928 _AX5043_PLLRNGCLK	=	0x4183
                           004032  1929 G$AX5043_PLLVCODIV$0$0 == 0x4032
                           004032  1930 _AX5043_PLLVCODIV	=	0x4032
                           004180  1931 G$AX5043_PLLVCOI$0$0 == 0x4180
                           004180  1932 _AX5043_PLLVCOI	=	0x4180
                           004181  1933 G$AX5043_PLLVCOIR$0$0 == 0x4181
                           004181  1934 _AX5043_PLLVCOIR	=	0x4181
                           004F08  1935 G$AX5043_POWCTRL1$0$0 == 0x4f08
                           004F08  1936 _AX5043_POWCTRL1	=	0x4f08
                           004005  1937 G$AX5043_POWIRQMASK$0$0 == 0x4005
                           004005  1938 _AX5043_POWIRQMASK	=	0x4005
                           004003  1939 G$AX5043_POWSTAT$0$0 == 0x4003
                           004003  1940 _AX5043_POWSTAT	=	0x4003
                           004004  1941 G$AX5043_POWSTICKYSTAT$0$0 == 0x4004
                           004004  1942 _AX5043_POWSTICKYSTAT	=	0x4004
                           004027  1943 G$AX5043_PWRAMP$0$0 == 0x4027
                           004027  1944 _AX5043_PWRAMP	=	0x4027
                           004002  1945 G$AX5043_PWRMODE$0$0 == 0x4002
                           004002  1946 _AX5043_PWRMODE	=	0x4002
                           004009  1947 G$AX5043_RADIOEVENTMASK0$0$0 == 0x4009
                           004009  1948 _AX5043_RADIOEVENTMASK0	=	0x4009
                           004008  1949 G$AX5043_RADIOEVENTMASK1$0$0 == 0x4008
                           004008  1950 _AX5043_RADIOEVENTMASK1	=	0x4008
                           00400F  1951 G$AX5043_RADIOEVENTREQ0$0$0 == 0x400f
                           00400F  1952 _AX5043_RADIOEVENTREQ0	=	0x400f
                           00400E  1953 G$AX5043_RADIOEVENTREQ1$0$0 == 0x400e
                           00400E  1954 _AX5043_RADIOEVENTREQ1	=	0x400e
                           00401C  1955 G$AX5043_RADIOSTATE$0$0 == 0x401c
                           00401C  1956 _AX5043_RADIOSTATE	=	0x401c
                           004F0D  1957 G$AX5043_REF$0$0 == 0x4f0d
                           004F0D  1958 _AX5043_REF	=	0x4f0d
                           004040  1959 G$AX5043_RSSI$0$0 == 0x4040
                           004040  1960 _AX5043_RSSI	=	0x4040
                           00422D  1961 G$AX5043_RSSIABSTHR$0$0 == 0x422d
                           00422D  1962 _AX5043_RSSIABSTHR	=	0x422d
                           00422C  1963 G$AX5043_RSSIREFERENCE$0$0 == 0x422c
                           00422C  1964 _AX5043_RSSIREFERENCE	=	0x422c
                           004105  1965 G$AX5043_RXDATARATE0$0$0 == 0x4105
                           004105  1966 _AX5043_RXDATARATE0	=	0x4105
                           004104  1967 G$AX5043_RXDATARATE1$0$0 == 0x4104
                           004104  1968 _AX5043_RXDATARATE1	=	0x4104
                           004103  1969 G$AX5043_RXDATARATE2$0$0 == 0x4103
                           004103  1970 _AX5043_RXDATARATE2	=	0x4103
                           004001  1971 G$AX5043_SCRATCH$0$0 == 0x4001
                           004001  1972 _AX5043_SCRATCH	=	0x4001
                           004000  1973 G$AX5043_SILICONREVISION$0$0 == 0x4000
                           004000  1974 _AX5043_SILICONREVISION	=	0x4000
                           00405B  1975 G$AX5043_TIMER0$0$0 == 0x405b
                           00405B  1976 _AX5043_TIMER0	=	0x405b
                           00405A  1977 G$AX5043_TIMER1$0$0 == 0x405a
                           00405A  1978 _AX5043_TIMER1	=	0x405a
                           004059  1979 G$AX5043_TIMER2$0$0 == 0x4059
                           004059  1980 _AX5043_TIMER2	=	0x4059
                           004227  1981 G$AX5043_TMGRXAGC$0$0 == 0x4227
                           004227  1982 _AX5043_TMGRXAGC	=	0x4227
                           004223  1983 G$AX5043_TMGRXBOOST$0$0 == 0x4223
                           004223  1984 _AX5043_TMGRXBOOST	=	0x4223
                           004226  1985 G$AX5043_TMGRXCOARSEAGC$0$0 == 0x4226
                           004226  1986 _AX5043_TMGRXCOARSEAGC	=	0x4226
                           004225  1987 G$AX5043_TMGRXOFFSACQ$0$0 == 0x4225
                           004225  1988 _AX5043_TMGRXOFFSACQ	=	0x4225
                           004229  1989 G$AX5043_TMGRXPREAMBLE1$0$0 == 0x4229
                           004229  1990 _AX5043_TMGRXPREAMBLE1	=	0x4229
                           00422A  1991 G$AX5043_TMGRXPREAMBLE2$0$0 == 0x422a
                           00422A  1992 _AX5043_TMGRXPREAMBLE2	=	0x422a
                           00422B  1993 G$AX5043_TMGRXPREAMBLE3$0$0 == 0x422b
                           00422B  1994 _AX5043_TMGRXPREAMBLE3	=	0x422b
                           004228  1995 G$AX5043_TMGRXRSSI$0$0 == 0x4228
                           004228  1996 _AX5043_TMGRXRSSI	=	0x4228
                           004224  1997 G$AX5043_TMGRXSETTLE$0$0 == 0x4224
                           004224  1998 _AX5043_TMGRXSETTLE	=	0x4224
                           004220  1999 G$AX5043_TMGTXBOOST$0$0 == 0x4220
                           004220  2000 _AX5043_TMGTXBOOST	=	0x4220
                           004221  2001 G$AX5043_TMGTXSETTLE$0$0 == 0x4221
                           004221  2002 _AX5043_TMGTXSETTLE	=	0x4221
                           004055  2003 G$AX5043_TRKAFSKDEMOD0$0$0 == 0x4055
                           004055  2004 _AX5043_TRKAFSKDEMOD0	=	0x4055
                           004054  2005 G$AX5043_TRKAFSKDEMOD1$0$0 == 0x4054
                           004054  2006 _AX5043_TRKAFSKDEMOD1	=	0x4054
                           004049  2007 G$AX5043_TRKAMPLITUDE0$0$0 == 0x4049
                           004049  2008 _AX5043_TRKAMPLITUDE0	=	0x4049
                           004048  2009 G$AX5043_TRKAMPLITUDE1$0$0 == 0x4048
                           004048  2010 _AX5043_TRKAMPLITUDE1	=	0x4048
                           004047  2011 G$AX5043_TRKDATARATE0$0$0 == 0x4047
                           004047  2012 _AX5043_TRKDATARATE0	=	0x4047
                           004046  2013 G$AX5043_TRKDATARATE1$0$0 == 0x4046
                           004046  2014 _AX5043_TRKDATARATE1	=	0x4046
                           004045  2015 G$AX5043_TRKDATARATE2$0$0 == 0x4045
                           004045  2016 _AX5043_TRKDATARATE2	=	0x4045
                           004051  2017 G$AX5043_TRKFREQ0$0$0 == 0x4051
                           004051  2018 _AX5043_TRKFREQ0	=	0x4051
                           004050  2019 G$AX5043_TRKFREQ1$0$0 == 0x4050
                           004050  2020 _AX5043_TRKFREQ1	=	0x4050
                           004053  2021 G$AX5043_TRKFSKDEMOD0$0$0 == 0x4053
                           004053  2022 _AX5043_TRKFSKDEMOD0	=	0x4053
                           004052  2023 G$AX5043_TRKFSKDEMOD1$0$0 == 0x4052
                           004052  2024 _AX5043_TRKFSKDEMOD1	=	0x4052
                           00404B  2025 G$AX5043_TRKPHASE0$0$0 == 0x404b
                           00404B  2026 _AX5043_TRKPHASE0	=	0x404b
                           00404A  2027 G$AX5043_TRKPHASE1$0$0 == 0x404a
                           00404A  2028 _AX5043_TRKPHASE1	=	0x404a
                           00404F  2029 G$AX5043_TRKRFFREQ0$0$0 == 0x404f
                           00404F  2030 _AX5043_TRKRFFREQ0	=	0x404f
                           00404E  2031 G$AX5043_TRKRFFREQ1$0$0 == 0x404e
                           00404E  2032 _AX5043_TRKRFFREQ1	=	0x404e
                           00404D  2033 G$AX5043_TRKRFFREQ2$0$0 == 0x404d
                           00404D  2034 _AX5043_TRKRFFREQ2	=	0x404d
                           004169  2035 G$AX5043_TXPWRCOEFFA0$0$0 == 0x4169
                           004169  2036 _AX5043_TXPWRCOEFFA0	=	0x4169
                           004168  2037 G$AX5043_TXPWRCOEFFA1$0$0 == 0x4168
                           004168  2038 _AX5043_TXPWRCOEFFA1	=	0x4168
                           00416B  2039 G$AX5043_TXPWRCOEFFB0$0$0 == 0x416b
                           00416B  2040 _AX5043_TXPWRCOEFFB0	=	0x416b
                           00416A  2041 G$AX5043_TXPWRCOEFFB1$0$0 == 0x416a
                           00416A  2042 _AX5043_TXPWRCOEFFB1	=	0x416a
                           00416D  2043 G$AX5043_TXPWRCOEFFC0$0$0 == 0x416d
                           00416D  2044 _AX5043_TXPWRCOEFFC0	=	0x416d
                           00416C  2045 G$AX5043_TXPWRCOEFFC1$0$0 == 0x416c
                           00416C  2046 _AX5043_TXPWRCOEFFC1	=	0x416c
                           00416F  2047 G$AX5043_TXPWRCOEFFD0$0$0 == 0x416f
                           00416F  2048 _AX5043_TXPWRCOEFFD0	=	0x416f
                           00416E  2049 G$AX5043_TXPWRCOEFFD1$0$0 == 0x416e
                           00416E  2050 _AX5043_TXPWRCOEFFD1	=	0x416e
                           004171  2051 G$AX5043_TXPWRCOEFFE0$0$0 == 0x4171
                           004171  2052 _AX5043_TXPWRCOEFFE0	=	0x4171
                           004170  2053 G$AX5043_TXPWRCOEFFE1$0$0 == 0x4170
                           004170  2054 _AX5043_TXPWRCOEFFE1	=	0x4170
                           004167  2055 G$AX5043_TXRATE0$0$0 == 0x4167
                           004167  2056 _AX5043_TXRATE0	=	0x4167
                           004166  2057 G$AX5043_TXRATE1$0$0 == 0x4166
                           004166  2058 _AX5043_TXRATE1	=	0x4166
                           004165  2059 G$AX5043_TXRATE2$0$0 == 0x4165
                           004165  2060 _AX5043_TXRATE2	=	0x4165
                           00406B  2061 G$AX5043_WAKEUP0$0$0 == 0x406b
                           00406B  2062 _AX5043_WAKEUP0	=	0x406b
                           00406A  2063 G$AX5043_WAKEUP1$0$0 == 0x406a
                           00406A  2064 _AX5043_WAKEUP1	=	0x406a
                           00406D  2065 G$AX5043_WAKEUPFREQ0$0$0 == 0x406d
                           00406D  2066 _AX5043_WAKEUPFREQ0	=	0x406d
                           00406C  2067 G$AX5043_WAKEUPFREQ1$0$0 == 0x406c
                           00406C  2068 _AX5043_WAKEUPFREQ1	=	0x406c
                           004069  2069 G$AX5043_WAKEUPTIMER0$0$0 == 0x4069
                           004069  2070 _AX5043_WAKEUPTIMER0	=	0x4069
                           004068  2071 G$AX5043_WAKEUPTIMER1$0$0 == 0x4068
                           004068  2072 _AX5043_WAKEUPTIMER1	=	0x4068
                           00406E  2073 G$AX5043_WAKEUPXOEARLY$0$0 == 0x406e
                           00406E  2074 _AX5043_WAKEUPXOEARLY	=	0x406e
                           004F11  2075 G$AX5043_XTALAMPL$0$0 == 0x4f11
                           004F11  2076 _AX5043_XTALAMPL	=	0x4f11
                           004184  2077 G$AX5043_XTALCAP$0$0 == 0x4184
                           004184  2078 _AX5043_XTALCAP	=	0x4184
                           004F10  2079 G$AX5043_XTALOSC$0$0 == 0x4f10
                           004F10  2080 _AX5043_XTALOSC	=	0x4f10
                           00401D  2081 G$AX5043_XTALSTATUS$0$0 == 0x401d
                           00401D  2082 _AX5043_XTALSTATUS	=	0x401d
                           004F00  2083 G$AX5043_0xF00$0$0 == 0x4f00
                           004F00  2084 _AX5043_0xF00	=	0x4f00
                           004F0C  2085 G$AX5043_0xF0C$0$0 == 0x4f0c
                           004F0C  2086 _AX5043_0xF0C	=	0x4f0c
                           004F18  2087 G$AX5043_0xF18$0$0 == 0x4f18
                           004F18  2088 _AX5043_0xF18	=	0x4f18
                           004F1C  2089 G$AX5043_0xF1C$0$0 == 0x4f1c
                           004F1C  2090 _AX5043_0xF1C	=	0x4f1c
                           004F21  2091 G$AX5043_0xF21$0$0 == 0x4f21
                           004F21  2092 _AX5043_0xF21	=	0x4f21
                           004F22  2093 G$AX5043_0xF22$0$0 == 0x4f22
                           004F22  2094 _AX5043_0xF22	=	0x4f22
                           004F23  2095 G$AX5043_0xF23$0$0 == 0x4f23
                           004F23  2096 _AX5043_0xF23	=	0x4f23
                           004F26  2097 G$AX5043_0xF26$0$0 == 0x4f26
                           004F26  2098 _AX5043_0xF26	=	0x4f26
                           004F30  2099 G$AX5043_0xF30$0$0 == 0x4f30
                           004F30  2100 _AX5043_0xF30	=	0x4f30
                           004F31  2101 G$AX5043_0xF31$0$0 == 0x4f31
                           004F31  2102 _AX5043_0xF31	=	0x4f31
                           004F32  2103 G$AX5043_0xF32$0$0 == 0x4f32
                           004F32  2104 _AX5043_0xF32	=	0x4f32
                           004F33  2105 G$AX5043_0xF33$0$0 == 0x4f33
                           004F33  2106 _AX5043_0xF33	=	0x4f33
                           004F34  2107 G$AX5043_0xF34$0$0 == 0x4f34
                           004F34  2108 _AX5043_0xF34	=	0x4f34
                           004F35  2109 G$AX5043_0xF35$0$0 == 0x4f35
                           004F35  2110 _AX5043_0xF35	=	0x4f35
                           004F44  2111 G$AX5043_0xF44$0$0 == 0x4f44
                           004F44  2112 _AX5043_0xF44	=	0x4f44
                           004122  2113 G$AX5043_AGCAHYST0$0$0 == 0x4122
                           004122  2114 _AX5043_AGCAHYST0	=	0x4122
                           004132  2115 G$AX5043_AGCAHYST1$0$0 == 0x4132
                           004132  2116 _AX5043_AGCAHYST1	=	0x4132
                           004142  2117 G$AX5043_AGCAHYST2$0$0 == 0x4142
                           004142  2118 _AX5043_AGCAHYST2	=	0x4142
                           004152  2119 G$AX5043_AGCAHYST3$0$0 == 0x4152
                           004152  2120 _AX5043_AGCAHYST3	=	0x4152
                           004120  2121 G$AX5043_AGCGAIN0$0$0 == 0x4120
                           004120  2122 _AX5043_AGCGAIN0	=	0x4120
                           004130  2123 G$AX5043_AGCGAIN1$0$0 == 0x4130
                           004130  2124 _AX5043_AGCGAIN1	=	0x4130
                           004140  2125 G$AX5043_AGCGAIN2$0$0 == 0x4140
                           004140  2126 _AX5043_AGCGAIN2	=	0x4140
                           004150  2127 G$AX5043_AGCGAIN3$0$0 == 0x4150
                           004150  2128 _AX5043_AGCGAIN3	=	0x4150
                           004123  2129 G$AX5043_AGCMINMAX0$0$0 == 0x4123
                           004123  2130 _AX5043_AGCMINMAX0	=	0x4123
                           004133  2131 G$AX5043_AGCMINMAX1$0$0 == 0x4133
                           004133  2132 _AX5043_AGCMINMAX1	=	0x4133
                           004143  2133 G$AX5043_AGCMINMAX2$0$0 == 0x4143
                           004143  2134 _AX5043_AGCMINMAX2	=	0x4143
                           004153  2135 G$AX5043_AGCMINMAX3$0$0 == 0x4153
                           004153  2136 _AX5043_AGCMINMAX3	=	0x4153
                           004121  2137 G$AX5043_AGCTARGET0$0$0 == 0x4121
                           004121  2138 _AX5043_AGCTARGET0	=	0x4121
                           004131  2139 G$AX5043_AGCTARGET1$0$0 == 0x4131
                           004131  2140 _AX5043_AGCTARGET1	=	0x4131
                           004141  2141 G$AX5043_AGCTARGET2$0$0 == 0x4141
                           004141  2142 _AX5043_AGCTARGET2	=	0x4141
                           004151  2143 G$AX5043_AGCTARGET3$0$0 == 0x4151
                           004151  2144 _AX5043_AGCTARGET3	=	0x4151
                           00412B  2145 G$AX5043_AMPLITUDEGAIN0$0$0 == 0x412b
                           00412B  2146 _AX5043_AMPLITUDEGAIN0	=	0x412b
                           00413B  2147 G$AX5043_AMPLITUDEGAIN1$0$0 == 0x413b
                           00413B  2148 _AX5043_AMPLITUDEGAIN1	=	0x413b
                           00414B  2149 G$AX5043_AMPLITUDEGAIN2$0$0 == 0x414b
                           00414B  2150 _AX5043_AMPLITUDEGAIN2	=	0x414b
                           00415B  2151 G$AX5043_AMPLITUDEGAIN3$0$0 == 0x415b
                           00415B  2152 _AX5043_AMPLITUDEGAIN3	=	0x415b
                           00412F  2153 G$AX5043_BBOFFSRES0$0$0 == 0x412f
                           00412F  2154 _AX5043_BBOFFSRES0	=	0x412f
                           00413F  2155 G$AX5043_BBOFFSRES1$0$0 == 0x413f
                           00413F  2156 _AX5043_BBOFFSRES1	=	0x413f
                           00414F  2157 G$AX5043_BBOFFSRES2$0$0 == 0x414f
                           00414F  2158 _AX5043_BBOFFSRES2	=	0x414f
                           00415F  2159 G$AX5043_BBOFFSRES3$0$0 == 0x415f
                           00415F  2160 _AX5043_BBOFFSRES3	=	0x415f
                           004125  2161 G$AX5043_DRGAIN0$0$0 == 0x4125
                           004125  2162 _AX5043_DRGAIN0	=	0x4125
                           004135  2163 G$AX5043_DRGAIN1$0$0 == 0x4135
                           004135  2164 _AX5043_DRGAIN1	=	0x4135
                           004145  2165 G$AX5043_DRGAIN2$0$0 == 0x4145
                           004145  2166 _AX5043_DRGAIN2	=	0x4145
                           004155  2167 G$AX5043_DRGAIN3$0$0 == 0x4155
                           004155  2168 _AX5043_DRGAIN3	=	0x4155
                           00412E  2169 G$AX5043_FOURFSK0$0$0 == 0x412e
                           00412E  2170 _AX5043_FOURFSK0	=	0x412e
                           00413E  2171 G$AX5043_FOURFSK1$0$0 == 0x413e
                           00413E  2172 _AX5043_FOURFSK1	=	0x413e
                           00414E  2173 G$AX5043_FOURFSK2$0$0 == 0x414e
                           00414E  2174 _AX5043_FOURFSK2	=	0x414e
                           00415E  2175 G$AX5043_FOURFSK3$0$0 == 0x415e
                           00415E  2176 _AX5043_FOURFSK3	=	0x415e
                           00412D  2177 G$AX5043_FREQDEV00$0$0 == 0x412d
                           00412D  2178 _AX5043_FREQDEV00	=	0x412d
                           00413D  2179 G$AX5043_FREQDEV01$0$0 == 0x413d
                           00413D  2180 _AX5043_FREQDEV01	=	0x413d
                           00414D  2181 G$AX5043_FREQDEV02$0$0 == 0x414d
                           00414D  2182 _AX5043_FREQDEV02	=	0x414d
                           00415D  2183 G$AX5043_FREQDEV03$0$0 == 0x415d
                           00415D  2184 _AX5043_FREQDEV03	=	0x415d
                           00412C  2185 G$AX5043_FREQDEV10$0$0 == 0x412c
                           00412C  2186 _AX5043_FREQDEV10	=	0x412c
                           00413C  2187 G$AX5043_FREQDEV11$0$0 == 0x413c
                           00413C  2188 _AX5043_FREQDEV11	=	0x413c
                           00414C  2189 G$AX5043_FREQDEV12$0$0 == 0x414c
                           00414C  2190 _AX5043_FREQDEV12	=	0x414c
                           00415C  2191 G$AX5043_FREQDEV13$0$0 == 0x415c
                           00415C  2192 _AX5043_FREQDEV13	=	0x415c
                           004127  2193 G$AX5043_FREQUENCYGAINA0$0$0 == 0x4127
                           004127  2194 _AX5043_FREQUENCYGAINA0	=	0x4127
                           004137  2195 G$AX5043_FREQUENCYGAINA1$0$0 == 0x4137
                           004137  2196 _AX5043_FREQUENCYGAINA1	=	0x4137
                           004147  2197 G$AX5043_FREQUENCYGAINA2$0$0 == 0x4147
                           004147  2198 _AX5043_FREQUENCYGAINA2	=	0x4147
                           004157  2199 G$AX5043_FREQUENCYGAINA3$0$0 == 0x4157
                           004157  2200 _AX5043_FREQUENCYGAINA3	=	0x4157
                           004128  2201 G$AX5043_FREQUENCYGAINB0$0$0 == 0x4128
                           004128  2202 _AX5043_FREQUENCYGAINB0	=	0x4128
                           004138  2203 G$AX5043_FREQUENCYGAINB1$0$0 == 0x4138
                           004138  2204 _AX5043_FREQUENCYGAINB1	=	0x4138
                           004148  2205 G$AX5043_FREQUENCYGAINB2$0$0 == 0x4148
                           004148  2206 _AX5043_FREQUENCYGAINB2	=	0x4148
                           004158  2207 G$AX5043_FREQUENCYGAINB3$0$0 == 0x4158
                           004158  2208 _AX5043_FREQUENCYGAINB3	=	0x4158
                           004129  2209 G$AX5043_FREQUENCYGAINC0$0$0 == 0x4129
                           004129  2210 _AX5043_FREQUENCYGAINC0	=	0x4129
                           004139  2211 G$AX5043_FREQUENCYGAINC1$0$0 == 0x4139
                           004139  2212 _AX5043_FREQUENCYGAINC1	=	0x4139
                           004149  2213 G$AX5043_FREQUENCYGAINC2$0$0 == 0x4149
                           004149  2214 _AX5043_FREQUENCYGAINC2	=	0x4149
                           004159  2215 G$AX5043_FREQUENCYGAINC3$0$0 == 0x4159
                           004159  2216 _AX5043_FREQUENCYGAINC3	=	0x4159
                           00412A  2217 G$AX5043_FREQUENCYGAIND0$0$0 == 0x412a
                           00412A  2218 _AX5043_FREQUENCYGAIND0	=	0x412a
                           00413A  2219 G$AX5043_FREQUENCYGAIND1$0$0 == 0x413a
                           00413A  2220 _AX5043_FREQUENCYGAIND1	=	0x413a
                           00414A  2221 G$AX5043_FREQUENCYGAIND2$0$0 == 0x414a
                           00414A  2222 _AX5043_FREQUENCYGAIND2	=	0x414a
                           00415A  2223 G$AX5043_FREQUENCYGAIND3$0$0 == 0x415a
                           00415A  2224 _AX5043_FREQUENCYGAIND3	=	0x415a
                           004116  2225 G$AX5043_FREQUENCYLEAK$0$0 == 0x4116
                           004116  2226 _AX5043_FREQUENCYLEAK	=	0x4116
                           004126  2227 G$AX5043_PHASEGAIN0$0$0 == 0x4126
                           004126  2228 _AX5043_PHASEGAIN0	=	0x4126
                           004136  2229 G$AX5043_PHASEGAIN1$0$0 == 0x4136
                           004136  2230 _AX5043_PHASEGAIN1	=	0x4136
                           004146  2231 G$AX5043_PHASEGAIN2$0$0 == 0x4146
                           004146  2232 _AX5043_PHASEGAIN2	=	0x4146
                           004156  2233 G$AX5043_PHASEGAIN3$0$0 == 0x4156
                           004156  2234 _AX5043_PHASEGAIN3	=	0x4156
                           004207  2235 G$AX5043_PKTADDR0$0$0 == 0x4207
                           004207  2236 _AX5043_PKTADDR0	=	0x4207
                           004206  2237 G$AX5043_PKTADDR1$0$0 == 0x4206
                           004206  2238 _AX5043_PKTADDR1	=	0x4206
                           004205  2239 G$AX5043_PKTADDR2$0$0 == 0x4205
                           004205  2240 _AX5043_PKTADDR2	=	0x4205
                           004204  2241 G$AX5043_PKTADDR3$0$0 == 0x4204
                           004204  2242 _AX5043_PKTADDR3	=	0x4204
                           004200  2243 G$AX5043_PKTADDRCFG$0$0 == 0x4200
                           004200  2244 _AX5043_PKTADDRCFG	=	0x4200
                           00420B  2245 G$AX5043_PKTADDRMASK0$0$0 == 0x420b
                           00420B  2246 _AX5043_PKTADDRMASK0	=	0x420b
                           00420A  2247 G$AX5043_PKTADDRMASK1$0$0 == 0x420a
                           00420A  2248 _AX5043_PKTADDRMASK1	=	0x420a
                           004209  2249 G$AX5043_PKTADDRMASK2$0$0 == 0x4209
                           004209  2250 _AX5043_PKTADDRMASK2	=	0x4209
                           004208  2251 G$AX5043_PKTADDRMASK3$0$0 == 0x4208
                           004208  2252 _AX5043_PKTADDRMASK3	=	0x4208
                           004201  2253 G$AX5043_PKTLENCFG$0$0 == 0x4201
                           004201  2254 _AX5043_PKTLENCFG	=	0x4201
                           004202  2255 G$AX5043_PKTLENOFFSET$0$0 == 0x4202
                           004202  2256 _AX5043_PKTLENOFFSET	=	0x4202
                           004203  2257 G$AX5043_PKTMAXLEN$0$0 == 0x4203
                           004203  2258 _AX5043_PKTMAXLEN	=	0x4203
                           004118  2259 G$AX5043_RXPARAMCURSET$0$0 == 0x4118
                           004118  2260 _AX5043_RXPARAMCURSET	=	0x4118
                           004117  2261 G$AX5043_RXPARAMSETS$0$0 == 0x4117
                           004117  2262 _AX5043_RXPARAMSETS	=	0x4117
                           004124  2263 G$AX5043_TIMEGAIN0$0$0 == 0x4124
                           004124  2264 _AX5043_TIMEGAIN0	=	0x4124
                           004134  2265 G$AX5043_TIMEGAIN1$0$0 == 0x4134
                           004134  2266 _AX5043_TIMEGAIN1	=	0x4134
                           004144  2267 G$AX5043_TIMEGAIN2$0$0 == 0x4144
                           004144  2268 _AX5043_TIMEGAIN2	=	0x4144
                           004154  2269 G$AX5043_TIMEGAIN3$0$0 == 0x4154
                           004154  2270 _AX5043_TIMEGAIN3	=	0x4154
                           005114  2271 G$AX5043_AFSKCTRLNB$0$0 == 0x5114
                           005114  2272 _AX5043_AFSKCTRLNB	=	0x5114
                           005113  2273 G$AX5043_AFSKMARK0NB$0$0 == 0x5113
                           005113  2274 _AX5043_AFSKMARK0NB	=	0x5113
                           005112  2275 G$AX5043_AFSKMARK1NB$0$0 == 0x5112
                           005112  2276 _AX5043_AFSKMARK1NB	=	0x5112
                           005111  2277 G$AX5043_AFSKSPACE0NB$0$0 == 0x5111
                           005111  2278 _AX5043_AFSKSPACE0NB	=	0x5111
                           005110  2279 G$AX5043_AFSKSPACE1NB$0$0 == 0x5110
                           005110  2280 _AX5043_AFSKSPACE1NB	=	0x5110
                           005043  2281 G$AX5043_AGCCOUNTERNB$0$0 == 0x5043
                           005043  2282 _AX5043_AGCCOUNTERNB	=	0x5043
                           005115  2283 G$AX5043_AMPLFILTERNB$0$0 == 0x5115
                           005115  2284 _AX5043_AMPLFILTERNB	=	0x5115
                           005189  2285 G$AX5043_BBOFFSCAPNB$0$0 == 0x5189
                           005189  2286 _AX5043_BBOFFSCAPNB	=	0x5189
                           005188  2287 G$AX5043_BBTUNENB$0$0 == 0x5188
                           005188  2288 _AX5043_BBTUNENB	=	0x5188
                           005041  2289 G$AX5043_BGNDRSSINB$0$0 == 0x5041
                           005041  2290 _AX5043_BGNDRSSINB	=	0x5041
                           00522E  2291 G$AX5043_BGNDRSSIGAINNB$0$0 == 0x522e
                           00522E  2292 _AX5043_BGNDRSSIGAINNB	=	0x522e
                           00522F  2293 G$AX5043_BGNDRSSITHRNB$0$0 == 0x522f
                           00522F  2294 _AX5043_BGNDRSSITHRNB	=	0x522f
                           005017  2295 G$AX5043_CRCINIT0NB$0$0 == 0x5017
                           005017  2296 _AX5043_CRCINIT0NB	=	0x5017
                           005016  2297 G$AX5043_CRCINIT1NB$0$0 == 0x5016
                           005016  2298 _AX5043_CRCINIT1NB	=	0x5016
                           005015  2299 G$AX5043_CRCINIT2NB$0$0 == 0x5015
                           005015  2300 _AX5043_CRCINIT2NB	=	0x5015
                           005014  2301 G$AX5043_CRCINIT3NB$0$0 == 0x5014
                           005014  2302 _AX5043_CRCINIT3NB	=	0x5014
                           005332  2303 G$AX5043_DACCONFIGNB$0$0 == 0x5332
                           005332  2304 _AX5043_DACCONFIGNB	=	0x5332
                           005331  2305 G$AX5043_DACVALUE0NB$0$0 == 0x5331
                           005331  2306 _AX5043_DACVALUE0NB	=	0x5331
                           005330  2307 G$AX5043_DACVALUE1NB$0$0 == 0x5330
                           005330  2308 _AX5043_DACVALUE1NB	=	0x5330
                           005102  2309 G$AX5043_DECIMATIONNB$0$0 == 0x5102
                           005102  2310 _AX5043_DECIMATIONNB	=	0x5102
                           005042  2311 G$AX5043_DIVERSITYNB$0$0 == 0x5042
                           005042  2312 _AX5043_DIVERSITYNB	=	0x5042
                           005011  2313 G$AX5043_ENCODINGNB$0$0 == 0x5011
                           005011  2314 _AX5043_ENCODINGNB	=	0x5011
                           005018  2315 G$AX5043_FECNB$0$0 == 0x5018
                           005018  2316 _AX5043_FECNB	=	0x5018
                           00501A  2317 G$AX5043_FECSTATUSNB$0$0 == 0x501a
                           00501A  2318 _AX5043_FECSTATUSNB	=	0x501a
                           005019  2319 G$AX5043_FECSYNCNB$0$0 == 0x5019
                           005019  2320 _AX5043_FECSYNCNB	=	0x5019
                           00502B  2321 G$AX5043_FIFOCOUNT0NB$0$0 == 0x502b
                           00502B  2322 _AX5043_FIFOCOUNT0NB	=	0x502b
                           00502A  2323 G$AX5043_FIFOCOUNT1NB$0$0 == 0x502a
                           00502A  2324 _AX5043_FIFOCOUNT1NB	=	0x502a
                           005029  2325 G$AX5043_FIFODATANB$0$0 == 0x5029
                           005029  2326 _AX5043_FIFODATANB	=	0x5029
                           00502D  2327 G$AX5043_FIFOFREE0NB$0$0 == 0x502d
                           00502D  2328 _AX5043_FIFOFREE0NB	=	0x502d
                           00502C  2329 G$AX5043_FIFOFREE1NB$0$0 == 0x502c
                           00502C  2330 _AX5043_FIFOFREE1NB	=	0x502c
                           005028  2331 G$AX5043_FIFOSTATNB$0$0 == 0x5028
                           005028  2332 _AX5043_FIFOSTATNB	=	0x5028
                           00502F  2333 G$AX5043_FIFOTHRESH0NB$0$0 == 0x502f
                           00502F  2334 _AX5043_FIFOTHRESH0NB	=	0x502f
                           00502E  2335 G$AX5043_FIFOTHRESH1NB$0$0 == 0x502e
                           00502E  2336 _AX5043_FIFOTHRESH1NB	=	0x502e
                           005012  2337 G$AX5043_FRAMINGNB$0$0 == 0x5012
                           005012  2338 _AX5043_FRAMINGNB	=	0x5012
                           005037  2339 G$AX5043_FREQA0NB$0$0 == 0x5037
                           005037  2340 _AX5043_FREQA0NB	=	0x5037
                           005036  2341 G$AX5043_FREQA1NB$0$0 == 0x5036
                           005036  2342 _AX5043_FREQA1NB	=	0x5036
                           005035  2343 G$AX5043_FREQA2NB$0$0 == 0x5035
                           005035  2344 _AX5043_FREQA2NB	=	0x5035
                           005034  2345 G$AX5043_FREQA3NB$0$0 == 0x5034
                           005034  2346 _AX5043_FREQA3NB	=	0x5034
                           00503F  2347 G$AX5043_FREQB0NB$0$0 == 0x503f
                           00503F  2348 _AX5043_FREQB0NB	=	0x503f
                           00503E  2349 G$AX5043_FREQB1NB$0$0 == 0x503e
                           00503E  2350 _AX5043_FREQB1NB	=	0x503e
                           00503D  2351 G$AX5043_FREQB2NB$0$0 == 0x503d
                           00503D  2352 _AX5043_FREQB2NB	=	0x503d
                           00503C  2353 G$AX5043_FREQB3NB$0$0 == 0x503c
                           00503C  2354 _AX5043_FREQB3NB	=	0x503c
                           005163  2355 G$AX5043_FSKDEV0NB$0$0 == 0x5163
                           005163  2356 _AX5043_FSKDEV0NB	=	0x5163
                           005162  2357 G$AX5043_FSKDEV1NB$0$0 == 0x5162
                           005162  2358 _AX5043_FSKDEV1NB	=	0x5162
                           005161  2359 G$AX5043_FSKDEV2NB$0$0 == 0x5161
                           005161  2360 _AX5043_FSKDEV2NB	=	0x5161
                           00510D  2361 G$AX5043_FSKDMAX0NB$0$0 == 0x510d
                           00510D  2362 _AX5043_FSKDMAX0NB	=	0x510d
                           00510C  2363 G$AX5043_FSKDMAX1NB$0$0 == 0x510c
                           00510C  2364 _AX5043_FSKDMAX1NB	=	0x510c
                           00510F  2365 G$AX5043_FSKDMIN0NB$0$0 == 0x510f
                           00510F  2366 _AX5043_FSKDMIN0NB	=	0x510f
                           00510E  2367 G$AX5043_FSKDMIN1NB$0$0 == 0x510e
                           00510E  2368 _AX5043_FSKDMIN1NB	=	0x510e
                           005309  2369 G$AX5043_GPADC13VALUE0NB$0$0 == 0x5309
                           005309  2370 _AX5043_GPADC13VALUE0NB	=	0x5309
                           005308  2371 G$AX5043_GPADC13VALUE1NB$0$0 == 0x5308
                           005308  2372 _AX5043_GPADC13VALUE1NB	=	0x5308
                           005300  2373 G$AX5043_GPADCCTRLNB$0$0 == 0x5300
                           005300  2374 _AX5043_GPADCCTRLNB	=	0x5300
                           005301  2375 G$AX5043_GPADCPERIODNB$0$0 == 0x5301
                           005301  2376 _AX5043_GPADCPERIODNB	=	0x5301
                           005101  2377 G$AX5043_IFFREQ0NB$0$0 == 0x5101
                           005101  2378 _AX5043_IFFREQ0NB	=	0x5101
                           005100  2379 G$AX5043_IFFREQ1NB$0$0 == 0x5100
                           005100  2380 _AX5043_IFFREQ1NB	=	0x5100
                           00500B  2381 G$AX5043_IRQINVERSION0NB$0$0 == 0x500b
                           00500B  2382 _AX5043_IRQINVERSION0NB	=	0x500b
                           00500A  2383 G$AX5043_IRQINVERSION1NB$0$0 == 0x500a
                           00500A  2384 _AX5043_IRQINVERSION1NB	=	0x500a
                           005007  2385 G$AX5043_IRQMASK0NB$0$0 == 0x5007
                           005007  2386 _AX5043_IRQMASK0NB	=	0x5007
                           005006  2387 G$AX5043_IRQMASK1NB$0$0 == 0x5006
                           005006  2388 _AX5043_IRQMASK1NB	=	0x5006
                           00500D  2389 G$AX5043_IRQREQUEST0NB$0$0 == 0x500d
                           00500D  2390 _AX5043_IRQREQUEST0NB	=	0x500d
                           00500C  2391 G$AX5043_IRQREQUEST1NB$0$0 == 0x500c
                           00500C  2392 _AX5043_IRQREQUEST1NB	=	0x500c
                           005310  2393 G$AX5043_LPOSCCONFIGNB$0$0 == 0x5310
                           005310  2394 _AX5043_LPOSCCONFIGNB	=	0x5310
                           005317  2395 G$AX5043_LPOSCFREQ0NB$0$0 == 0x5317
                           005317  2396 _AX5043_LPOSCFREQ0NB	=	0x5317
                           005316  2397 G$AX5043_LPOSCFREQ1NB$0$0 == 0x5316
                           005316  2398 _AX5043_LPOSCFREQ1NB	=	0x5316
                           005313  2399 G$AX5043_LPOSCKFILT0NB$0$0 == 0x5313
                           005313  2400 _AX5043_LPOSCKFILT0NB	=	0x5313
                           005312  2401 G$AX5043_LPOSCKFILT1NB$0$0 == 0x5312
                           005312  2402 _AX5043_LPOSCKFILT1NB	=	0x5312
                           005319  2403 G$AX5043_LPOSCPER0NB$0$0 == 0x5319
                           005319  2404 _AX5043_LPOSCPER0NB	=	0x5319
                           005318  2405 G$AX5043_LPOSCPER1NB$0$0 == 0x5318
                           005318  2406 _AX5043_LPOSCPER1NB	=	0x5318
                           005315  2407 G$AX5043_LPOSCREF0NB$0$0 == 0x5315
                           005315  2408 _AX5043_LPOSCREF0NB	=	0x5315
                           005314  2409 G$AX5043_LPOSCREF1NB$0$0 == 0x5314
                           005314  2410 _AX5043_LPOSCREF1NB	=	0x5314
                           005311  2411 G$AX5043_LPOSCSTATUSNB$0$0 == 0x5311
                           005311  2412 _AX5043_LPOSCSTATUSNB	=	0x5311
                           005214  2413 G$AX5043_MATCH0LENNB$0$0 == 0x5214
                           005214  2414 _AX5043_MATCH0LENNB	=	0x5214
                           005216  2415 G$AX5043_MATCH0MAXNB$0$0 == 0x5216
                           005216  2416 _AX5043_MATCH0MAXNB	=	0x5216
                           005215  2417 G$AX5043_MATCH0MINNB$0$0 == 0x5215
                           005215  2418 _AX5043_MATCH0MINNB	=	0x5215
                           005213  2419 G$AX5043_MATCH0PAT0NB$0$0 == 0x5213
                           005213  2420 _AX5043_MATCH0PAT0NB	=	0x5213
                           005212  2421 G$AX5043_MATCH0PAT1NB$0$0 == 0x5212
                           005212  2422 _AX5043_MATCH0PAT1NB	=	0x5212
                           005211  2423 G$AX5043_MATCH0PAT2NB$0$0 == 0x5211
                           005211  2424 _AX5043_MATCH0PAT2NB	=	0x5211
                           005210  2425 G$AX5043_MATCH0PAT3NB$0$0 == 0x5210
                           005210  2426 _AX5043_MATCH0PAT3NB	=	0x5210
                           00521C  2427 G$AX5043_MATCH1LENNB$0$0 == 0x521c
                           00521C  2428 _AX5043_MATCH1LENNB	=	0x521c
                           00521E  2429 G$AX5043_MATCH1MAXNB$0$0 == 0x521e
                           00521E  2430 _AX5043_MATCH1MAXNB	=	0x521e
                           00521D  2431 G$AX5043_MATCH1MINNB$0$0 == 0x521d
                           00521D  2432 _AX5043_MATCH1MINNB	=	0x521d
                           005219  2433 G$AX5043_MATCH1PAT0NB$0$0 == 0x5219
                           005219  2434 _AX5043_MATCH1PAT0NB	=	0x5219
                           005218  2435 G$AX5043_MATCH1PAT1NB$0$0 == 0x5218
                           005218  2436 _AX5043_MATCH1PAT1NB	=	0x5218
                           005108  2437 G$AX5043_MAXDROFFSET0NB$0$0 == 0x5108
                           005108  2438 _AX5043_MAXDROFFSET0NB	=	0x5108
                           005107  2439 G$AX5043_MAXDROFFSET1NB$0$0 == 0x5107
                           005107  2440 _AX5043_MAXDROFFSET1NB	=	0x5107
                           005106  2441 G$AX5043_MAXDROFFSET2NB$0$0 == 0x5106
                           005106  2442 _AX5043_MAXDROFFSET2NB	=	0x5106
                           00510B  2443 G$AX5043_MAXRFOFFSET0NB$0$0 == 0x510b
                           00510B  2444 _AX5043_MAXRFOFFSET0NB	=	0x510b
                           00510A  2445 G$AX5043_MAXRFOFFSET1NB$0$0 == 0x510a
                           00510A  2446 _AX5043_MAXRFOFFSET1NB	=	0x510a
                           005109  2447 G$AX5043_MAXRFOFFSET2NB$0$0 == 0x5109
                           005109  2448 _AX5043_MAXRFOFFSET2NB	=	0x5109
                           005164  2449 G$AX5043_MODCFGANB$0$0 == 0x5164
                           005164  2450 _AX5043_MODCFGANB	=	0x5164
                           005160  2451 G$AX5043_MODCFGFNB$0$0 == 0x5160
                           005160  2452 _AX5043_MODCFGFNB	=	0x5160
                           005F5F  2453 G$AX5043_MODCFGPNB$0$0 == 0x5f5f
                           005F5F  2454 _AX5043_MODCFGPNB	=	0x5f5f
                           005010  2455 G$AX5043_MODULATIONNB$0$0 == 0x5010
                           005010  2456 _AX5043_MODULATIONNB	=	0x5010
                           005025  2457 G$AX5043_PINFUNCANTSELNB$0$0 == 0x5025
                           005025  2458 _AX5043_PINFUNCANTSELNB	=	0x5025
                           005023  2459 G$AX5043_PINFUNCDATANB$0$0 == 0x5023
                           005023  2460 _AX5043_PINFUNCDATANB	=	0x5023
                           005022  2461 G$AX5043_PINFUNCDCLKNB$0$0 == 0x5022
                           005022  2462 _AX5043_PINFUNCDCLKNB	=	0x5022
                           005024  2463 G$AX5043_PINFUNCIRQNB$0$0 == 0x5024
                           005024  2464 _AX5043_PINFUNCIRQNB	=	0x5024
                           005026  2465 G$AX5043_PINFUNCPWRAMPNB$0$0 == 0x5026
                           005026  2466 _AX5043_PINFUNCPWRAMPNB	=	0x5026
                           005021  2467 G$AX5043_PINFUNCSYSCLKNB$0$0 == 0x5021
                           005021  2468 _AX5043_PINFUNCSYSCLKNB	=	0x5021
                           005020  2469 G$AX5043_PINSTATENB$0$0 == 0x5020
                           005020  2470 _AX5043_PINSTATENB	=	0x5020
                           005233  2471 G$AX5043_PKTACCEPTFLAGSNB$0$0 == 0x5233
                           005233  2472 _AX5043_PKTACCEPTFLAGSNB	=	0x5233
                           005230  2473 G$AX5043_PKTCHUNKSIZENB$0$0 == 0x5230
                           005230  2474 _AX5043_PKTCHUNKSIZENB	=	0x5230
                           005231  2475 G$AX5043_PKTMISCFLAGSNB$0$0 == 0x5231
                           005231  2476 _AX5043_PKTMISCFLAGSNB	=	0x5231
                           005232  2477 G$AX5043_PKTSTOREFLAGSNB$0$0 == 0x5232
                           005232  2478 _AX5043_PKTSTOREFLAGSNB	=	0x5232
                           005031  2479 G$AX5043_PLLCPINB$0$0 == 0x5031
                           005031  2480 _AX5043_PLLCPINB	=	0x5031
                           005039  2481 G$AX5043_PLLCPIBOOSTNB$0$0 == 0x5039
                           005039  2482 _AX5043_PLLCPIBOOSTNB	=	0x5039
                           005182  2483 G$AX5043_PLLLOCKDETNB$0$0 == 0x5182
                           005182  2484 _AX5043_PLLLOCKDETNB	=	0x5182
                           005030  2485 G$AX5043_PLLLOOPNB$0$0 == 0x5030
                           005030  2486 _AX5043_PLLLOOPNB	=	0x5030
                           005038  2487 G$AX5043_PLLLOOPBOOSTNB$0$0 == 0x5038
                           005038  2488 _AX5043_PLLLOOPBOOSTNB	=	0x5038
                           005033  2489 G$AX5043_PLLRANGINGANB$0$0 == 0x5033
                           005033  2490 _AX5043_PLLRANGINGANB	=	0x5033
                           00503B  2491 G$AX5043_PLLRANGINGBNB$0$0 == 0x503b
                           00503B  2492 _AX5043_PLLRANGINGBNB	=	0x503b
                           005183  2493 G$AX5043_PLLRNGCLKNB$0$0 == 0x5183
                           005183  2494 _AX5043_PLLRNGCLKNB	=	0x5183
                           005032  2495 G$AX5043_PLLVCODIVNB$0$0 == 0x5032
                           005032  2496 _AX5043_PLLVCODIVNB	=	0x5032
                           005180  2497 G$AX5043_PLLVCOINB$0$0 == 0x5180
                           005180  2498 _AX5043_PLLVCOINB	=	0x5180
                           005181  2499 G$AX5043_PLLVCOIRNB$0$0 == 0x5181
                           005181  2500 _AX5043_PLLVCOIRNB	=	0x5181
                           005F08  2501 G$AX5043_POWCTRL1NB$0$0 == 0x5f08
                           005F08  2502 _AX5043_POWCTRL1NB	=	0x5f08
                           005005  2503 G$AX5043_POWIRQMASKNB$0$0 == 0x5005
                           005005  2504 _AX5043_POWIRQMASKNB	=	0x5005
                           005003  2505 G$AX5043_POWSTATNB$0$0 == 0x5003
                           005003  2506 _AX5043_POWSTATNB	=	0x5003
                           005004  2507 G$AX5043_POWSTICKYSTATNB$0$0 == 0x5004
                           005004  2508 _AX5043_POWSTICKYSTATNB	=	0x5004
                           005027  2509 G$AX5043_PWRAMPNB$0$0 == 0x5027
                           005027  2510 _AX5043_PWRAMPNB	=	0x5027
                           005002  2511 G$AX5043_PWRMODENB$0$0 == 0x5002
                           005002  2512 _AX5043_PWRMODENB	=	0x5002
                           005009  2513 G$AX5043_RADIOEVENTMASK0NB$0$0 == 0x5009
                           005009  2514 _AX5043_RADIOEVENTMASK0NB	=	0x5009
                           005008  2515 G$AX5043_RADIOEVENTMASK1NB$0$0 == 0x5008
                           005008  2516 _AX5043_RADIOEVENTMASK1NB	=	0x5008
                           00500F  2517 G$AX5043_RADIOEVENTREQ0NB$0$0 == 0x500f
                           00500F  2518 _AX5043_RADIOEVENTREQ0NB	=	0x500f
                           00500E  2519 G$AX5043_RADIOEVENTREQ1NB$0$0 == 0x500e
                           00500E  2520 _AX5043_RADIOEVENTREQ1NB	=	0x500e
                           00501C  2521 G$AX5043_RADIOSTATENB$0$0 == 0x501c
                           00501C  2522 _AX5043_RADIOSTATENB	=	0x501c
                           005F0D  2523 G$AX5043_REFNB$0$0 == 0x5f0d
                           005F0D  2524 _AX5043_REFNB	=	0x5f0d
                           005040  2525 G$AX5043_RSSINB$0$0 == 0x5040
                           005040  2526 _AX5043_RSSINB	=	0x5040
                           00522D  2527 G$AX5043_RSSIABSTHRNB$0$0 == 0x522d
                           00522D  2528 _AX5043_RSSIABSTHRNB	=	0x522d
                           00522C  2529 G$AX5043_RSSIREFERENCENB$0$0 == 0x522c
                           00522C  2530 _AX5043_RSSIREFERENCENB	=	0x522c
                           005105  2531 G$AX5043_RXDATARATE0NB$0$0 == 0x5105
                           005105  2532 _AX5043_RXDATARATE0NB	=	0x5105
                           005104  2533 G$AX5043_RXDATARATE1NB$0$0 == 0x5104
                           005104  2534 _AX5043_RXDATARATE1NB	=	0x5104
                           005103  2535 G$AX5043_RXDATARATE2NB$0$0 == 0x5103
                           005103  2536 _AX5043_RXDATARATE2NB	=	0x5103
                           005001  2537 G$AX5043_SCRATCHNB$0$0 == 0x5001
                           005001  2538 _AX5043_SCRATCHNB	=	0x5001
                           005000  2539 G$AX5043_SILICONREVISIONNB$0$0 == 0x5000
                           005000  2540 _AX5043_SILICONREVISIONNB	=	0x5000
                           00505B  2541 G$AX5043_TIMER0NB$0$0 == 0x505b
                           00505B  2542 _AX5043_TIMER0NB	=	0x505b
                           00505A  2543 G$AX5043_TIMER1NB$0$0 == 0x505a
                           00505A  2544 _AX5043_TIMER1NB	=	0x505a
                           005059  2545 G$AX5043_TIMER2NB$0$0 == 0x5059
                           005059  2546 _AX5043_TIMER2NB	=	0x5059
                           005227  2547 G$AX5043_TMGRXAGCNB$0$0 == 0x5227
                           005227  2548 _AX5043_TMGRXAGCNB	=	0x5227
                           005223  2549 G$AX5043_TMGRXBOOSTNB$0$0 == 0x5223
                           005223  2550 _AX5043_TMGRXBOOSTNB	=	0x5223
                           005226  2551 G$AX5043_TMGRXCOARSEAGCNB$0$0 == 0x5226
                           005226  2552 _AX5043_TMGRXCOARSEAGCNB	=	0x5226
                           005225  2553 G$AX5043_TMGRXOFFSACQNB$0$0 == 0x5225
                           005225  2554 _AX5043_TMGRXOFFSACQNB	=	0x5225
                           005229  2555 G$AX5043_TMGRXPREAMBLE1NB$0$0 == 0x5229
                           005229  2556 _AX5043_TMGRXPREAMBLE1NB	=	0x5229
                           00522A  2557 G$AX5043_TMGRXPREAMBLE2NB$0$0 == 0x522a
                           00522A  2558 _AX5043_TMGRXPREAMBLE2NB	=	0x522a
                           00522B  2559 G$AX5043_TMGRXPREAMBLE3NB$0$0 == 0x522b
                           00522B  2560 _AX5043_TMGRXPREAMBLE3NB	=	0x522b
                           005228  2561 G$AX5043_TMGRXRSSINB$0$0 == 0x5228
                           005228  2562 _AX5043_TMGRXRSSINB	=	0x5228
                           005224  2563 G$AX5043_TMGRXSETTLENB$0$0 == 0x5224
                           005224  2564 _AX5043_TMGRXSETTLENB	=	0x5224
                           005220  2565 G$AX5043_TMGTXBOOSTNB$0$0 == 0x5220
                           005220  2566 _AX5043_TMGTXBOOSTNB	=	0x5220
                           005221  2567 G$AX5043_TMGTXSETTLENB$0$0 == 0x5221
                           005221  2568 _AX5043_TMGTXSETTLENB	=	0x5221
                           005055  2569 G$AX5043_TRKAFSKDEMOD0NB$0$0 == 0x5055
                           005055  2570 _AX5043_TRKAFSKDEMOD0NB	=	0x5055
                           005054  2571 G$AX5043_TRKAFSKDEMOD1NB$0$0 == 0x5054
                           005054  2572 _AX5043_TRKAFSKDEMOD1NB	=	0x5054
                           005049  2573 G$AX5043_TRKAMPLITUDE0NB$0$0 == 0x5049
                           005049  2574 _AX5043_TRKAMPLITUDE0NB	=	0x5049
                           005048  2575 G$AX5043_TRKAMPLITUDE1NB$0$0 == 0x5048
                           005048  2576 _AX5043_TRKAMPLITUDE1NB	=	0x5048
                           005047  2577 G$AX5043_TRKDATARATE0NB$0$0 == 0x5047
                           005047  2578 _AX5043_TRKDATARATE0NB	=	0x5047
                           005046  2579 G$AX5043_TRKDATARATE1NB$0$0 == 0x5046
                           005046  2580 _AX5043_TRKDATARATE1NB	=	0x5046
                           005045  2581 G$AX5043_TRKDATARATE2NB$0$0 == 0x5045
                           005045  2582 _AX5043_TRKDATARATE2NB	=	0x5045
                           005051  2583 G$AX5043_TRKFREQ0NB$0$0 == 0x5051
                           005051  2584 _AX5043_TRKFREQ0NB	=	0x5051
                           005050  2585 G$AX5043_TRKFREQ1NB$0$0 == 0x5050
                           005050  2586 _AX5043_TRKFREQ1NB	=	0x5050
                           005053  2587 G$AX5043_TRKFSKDEMOD0NB$0$0 == 0x5053
                           005053  2588 _AX5043_TRKFSKDEMOD0NB	=	0x5053
                           005052  2589 G$AX5043_TRKFSKDEMOD1NB$0$0 == 0x5052
                           005052  2590 _AX5043_TRKFSKDEMOD1NB	=	0x5052
                           00504B  2591 G$AX5043_TRKPHASE0NB$0$0 == 0x504b
                           00504B  2592 _AX5043_TRKPHASE0NB	=	0x504b
                           00504A  2593 G$AX5043_TRKPHASE1NB$0$0 == 0x504a
                           00504A  2594 _AX5043_TRKPHASE1NB	=	0x504a
                           00504F  2595 G$AX5043_TRKRFFREQ0NB$0$0 == 0x504f
                           00504F  2596 _AX5043_TRKRFFREQ0NB	=	0x504f
                           00504E  2597 G$AX5043_TRKRFFREQ1NB$0$0 == 0x504e
                           00504E  2598 _AX5043_TRKRFFREQ1NB	=	0x504e
                           00504D  2599 G$AX5043_TRKRFFREQ2NB$0$0 == 0x504d
                           00504D  2600 _AX5043_TRKRFFREQ2NB	=	0x504d
                           005169  2601 G$AX5043_TXPWRCOEFFA0NB$0$0 == 0x5169
                           005169  2602 _AX5043_TXPWRCOEFFA0NB	=	0x5169
                           005168  2603 G$AX5043_TXPWRCOEFFA1NB$0$0 == 0x5168
                           005168  2604 _AX5043_TXPWRCOEFFA1NB	=	0x5168
                           00516B  2605 G$AX5043_TXPWRCOEFFB0NB$0$0 == 0x516b
                           00516B  2606 _AX5043_TXPWRCOEFFB0NB	=	0x516b
                           00516A  2607 G$AX5043_TXPWRCOEFFB1NB$0$0 == 0x516a
                           00516A  2608 _AX5043_TXPWRCOEFFB1NB	=	0x516a
                           00516D  2609 G$AX5043_TXPWRCOEFFC0NB$0$0 == 0x516d
                           00516D  2610 _AX5043_TXPWRCOEFFC0NB	=	0x516d
                           00516C  2611 G$AX5043_TXPWRCOEFFC1NB$0$0 == 0x516c
                           00516C  2612 _AX5043_TXPWRCOEFFC1NB	=	0x516c
                           00516F  2613 G$AX5043_TXPWRCOEFFD0NB$0$0 == 0x516f
                           00516F  2614 _AX5043_TXPWRCOEFFD0NB	=	0x516f
                           00516E  2615 G$AX5043_TXPWRCOEFFD1NB$0$0 == 0x516e
                           00516E  2616 _AX5043_TXPWRCOEFFD1NB	=	0x516e
                           005171  2617 G$AX5043_TXPWRCOEFFE0NB$0$0 == 0x5171
                           005171  2618 _AX5043_TXPWRCOEFFE0NB	=	0x5171
                           005170  2619 G$AX5043_TXPWRCOEFFE1NB$0$0 == 0x5170
                           005170  2620 _AX5043_TXPWRCOEFFE1NB	=	0x5170
                           005167  2621 G$AX5043_TXRATE0NB$0$0 == 0x5167
                           005167  2622 _AX5043_TXRATE0NB	=	0x5167
                           005166  2623 G$AX5043_TXRATE1NB$0$0 == 0x5166
                           005166  2624 _AX5043_TXRATE1NB	=	0x5166
                           005165  2625 G$AX5043_TXRATE2NB$0$0 == 0x5165
                           005165  2626 _AX5043_TXRATE2NB	=	0x5165
                           00506B  2627 G$AX5043_WAKEUP0NB$0$0 == 0x506b
                           00506B  2628 _AX5043_WAKEUP0NB	=	0x506b
                           00506A  2629 G$AX5043_WAKEUP1NB$0$0 == 0x506a
                           00506A  2630 _AX5043_WAKEUP1NB	=	0x506a
                           00506D  2631 G$AX5043_WAKEUPFREQ0NB$0$0 == 0x506d
                           00506D  2632 _AX5043_WAKEUPFREQ0NB	=	0x506d
                           00506C  2633 G$AX5043_WAKEUPFREQ1NB$0$0 == 0x506c
                           00506C  2634 _AX5043_WAKEUPFREQ1NB	=	0x506c
                           005069  2635 G$AX5043_WAKEUPTIMER0NB$0$0 == 0x5069
                           005069  2636 _AX5043_WAKEUPTIMER0NB	=	0x5069
                           005068  2637 G$AX5043_WAKEUPTIMER1NB$0$0 == 0x5068
                           005068  2638 _AX5043_WAKEUPTIMER1NB	=	0x5068
                           00506E  2639 G$AX5043_WAKEUPXOEARLYNB$0$0 == 0x506e
                           00506E  2640 _AX5043_WAKEUPXOEARLYNB	=	0x506e
                           005F11  2641 G$AX5043_XTALAMPLNB$0$0 == 0x5f11
                           005F11  2642 _AX5043_XTALAMPLNB	=	0x5f11
                           005184  2643 G$AX5043_XTALCAPNB$0$0 == 0x5184
                           005184  2644 _AX5043_XTALCAPNB	=	0x5184
                           005F10  2645 G$AX5043_XTALOSCNB$0$0 == 0x5f10
                           005F10  2646 _AX5043_XTALOSCNB	=	0x5f10
                           00501D  2647 G$AX5043_XTALSTATUSNB$0$0 == 0x501d
                           00501D  2648 _AX5043_XTALSTATUSNB	=	0x501d
                           005F00  2649 G$AX5043_0xF00NB$0$0 == 0x5f00
                           005F00  2650 _AX5043_0xF00NB	=	0x5f00
                           005F0C  2651 G$AX5043_0xF0CNB$0$0 == 0x5f0c
                           005F0C  2652 _AX5043_0xF0CNB	=	0x5f0c
                           005F18  2653 G$AX5043_0xF18NB$0$0 == 0x5f18
                           005F18  2654 _AX5043_0xF18NB	=	0x5f18
                           005F1C  2655 G$AX5043_0xF1CNB$0$0 == 0x5f1c
                           005F1C  2656 _AX5043_0xF1CNB	=	0x5f1c
                           005F21  2657 G$AX5043_0xF21NB$0$0 == 0x5f21
                           005F21  2658 _AX5043_0xF21NB	=	0x5f21
                           005F22  2659 G$AX5043_0xF22NB$0$0 == 0x5f22
                           005F22  2660 _AX5043_0xF22NB	=	0x5f22
                           005F23  2661 G$AX5043_0xF23NB$0$0 == 0x5f23
                           005F23  2662 _AX5043_0xF23NB	=	0x5f23
                           005F26  2663 G$AX5043_0xF26NB$0$0 == 0x5f26
                           005F26  2664 _AX5043_0xF26NB	=	0x5f26
                           005F30  2665 G$AX5043_0xF30NB$0$0 == 0x5f30
                           005F30  2666 _AX5043_0xF30NB	=	0x5f30
                           005F31  2667 G$AX5043_0xF31NB$0$0 == 0x5f31
                           005F31  2668 _AX5043_0xF31NB	=	0x5f31
                           005F32  2669 G$AX5043_0xF32NB$0$0 == 0x5f32
                           005F32  2670 _AX5043_0xF32NB	=	0x5f32
                           005F33  2671 G$AX5043_0xF33NB$0$0 == 0x5f33
                           005F33  2672 _AX5043_0xF33NB	=	0x5f33
                           005F34  2673 G$AX5043_0xF34NB$0$0 == 0x5f34
                           005F34  2674 _AX5043_0xF34NB	=	0x5f34
                           005F35  2675 G$AX5043_0xF35NB$0$0 == 0x5f35
                           005F35  2676 _AX5043_0xF35NB	=	0x5f35
                           005F44  2677 G$AX5043_0xF44NB$0$0 == 0x5f44
                           005F44  2678 _AX5043_0xF44NB	=	0x5f44
                           005122  2679 G$AX5043_AGCAHYST0NB$0$0 == 0x5122
                           005122  2680 _AX5043_AGCAHYST0NB	=	0x5122
                           005132  2681 G$AX5043_AGCAHYST1NB$0$0 == 0x5132
                           005132  2682 _AX5043_AGCAHYST1NB	=	0x5132
                           005142  2683 G$AX5043_AGCAHYST2NB$0$0 == 0x5142
                           005142  2684 _AX5043_AGCAHYST2NB	=	0x5142
                           005152  2685 G$AX5043_AGCAHYST3NB$0$0 == 0x5152
                           005152  2686 _AX5043_AGCAHYST3NB	=	0x5152
                           005120  2687 G$AX5043_AGCGAIN0NB$0$0 == 0x5120
                           005120  2688 _AX5043_AGCGAIN0NB	=	0x5120
                           005130  2689 G$AX5043_AGCGAIN1NB$0$0 == 0x5130
                           005130  2690 _AX5043_AGCGAIN1NB	=	0x5130
                           005140  2691 G$AX5043_AGCGAIN2NB$0$0 == 0x5140
                           005140  2692 _AX5043_AGCGAIN2NB	=	0x5140
                           005150  2693 G$AX5043_AGCGAIN3NB$0$0 == 0x5150
                           005150  2694 _AX5043_AGCGAIN3NB	=	0x5150
                           005123  2695 G$AX5043_AGCMINMAX0NB$0$0 == 0x5123
                           005123  2696 _AX5043_AGCMINMAX0NB	=	0x5123
                           005133  2697 G$AX5043_AGCMINMAX1NB$0$0 == 0x5133
                           005133  2698 _AX5043_AGCMINMAX1NB	=	0x5133
                           005143  2699 G$AX5043_AGCMINMAX2NB$0$0 == 0x5143
                           005143  2700 _AX5043_AGCMINMAX2NB	=	0x5143
                           005153  2701 G$AX5043_AGCMINMAX3NB$0$0 == 0x5153
                           005153  2702 _AX5043_AGCMINMAX3NB	=	0x5153
                           005121  2703 G$AX5043_AGCTARGET0NB$0$0 == 0x5121
                           005121  2704 _AX5043_AGCTARGET0NB	=	0x5121
                           005131  2705 G$AX5043_AGCTARGET1NB$0$0 == 0x5131
                           005131  2706 _AX5043_AGCTARGET1NB	=	0x5131
                           005141  2707 G$AX5043_AGCTARGET2NB$0$0 == 0x5141
                           005141  2708 _AX5043_AGCTARGET2NB	=	0x5141
                           005151  2709 G$AX5043_AGCTARGET3NB$0$0 == 0x5151
                           005151  2710 _AX5043_AGCTARGET3NB	=	0x5151
                           00512B  2711 G$AX5043_AMPLITUDEGAIN0NB$0$0 == 0x512b
                           00512B  2712 _AX5043_AMPLITUDEGAIN0NB	=	0x512b
                           00513B  2713 G$AX5043_AMPLITUDEGAIN1NB$0$0 == 0x513b
                           00513B  2714 _AX5043_AMPLITUDEGAIN1NB	=	0x513b
                           00514B  2715 G$AX5043_AMPLITUDEGAIN2NB$0$0 == 0x514b
                           00514B  2716 _AX5043_AMPLITUDEGAIN2NB	=	0x514b
                           00515B  2717 G$AX5043_AMPLITUDEGAIN3NB$0$0 == 0x515b
                           00515B  2718 _AX5043_AMPLITUDEGAIN3NB	=	0x515b
                           00512F  2719 G$AX5043_BBOFFSRES0NB$0$0 == 0x512f
                           00512F  2720 _AX5043_BBOFFSRES0NB	=	0x512f
                           00513F  2721 G$AX5043_BBOFFSRES1NB$0$0 == 0x513f
                           00513F  2722 _AX5043_BBOFFSRES1NB	=	0x513f
                           00514F  2723 G$AX5043_BBOFFSRES2NB$0$0 == 0x514f
                           00514F  2724 _AX5043_BBOFFSRES2NB	=	0x514f
                           00515F  2725 G$AX5043_BBOFFSRES3NB$0$0 == 0x515f
                           00515F  2726 _AX5043_BBOFFSRES3NB	=	0x515f
                           005125  2727 G$AX5043_DRGAIN0NB$0$0 == 0x5125
                           005125  2728 _AX5043_DRGAIN0NB	=	0x5125
                           005135  2729 G$AX5043_DRGAIN1NB$0$0 == 0x5135
                           005135  2730 _AX5043_DRGAIN1NB	=	0x5135
                           005145  2731 G$AX5043_DRGAIN2NB$0$0 == 0x5145
                           005145  2732 _AX5043_DRGAIN2NB	=	0x5145
                           005155  2733 G$AX5043_DRGAIN3NB$0$0 == 0x5155
                           005155  2734 _AX5043_DRGAIN3NB	=	0x5155
                           00512E  2735 G$AX5043_FOURFSK0NB$0$0 == 0x512e
                           00512E  2736 _AX5043_FOURFSK0NB	=	0x512e
                           00513E  2737 G$AX5043_FOURFSK1NB$0$0 == 0x513e
                           00513E  2738 _AX5043_FOURFSK1NB	=	0x513e
                           00514E  2739 G$AX5043_FOURFSK2NB$0$0 == 0x514e
                           00514E  2740 _AX5043_FOURFSK2NB	=	0x514e
                           00515E  2741 G$AX5043_FOURFSK3NB$0$0 == 0x515e
                           00515E  2742 _AX5043_FOURFSK3NB	=	0x515e
                           00512D  2743 G$AX5043_FREQDEV00NB$0$0 == 0x512d
                           00512D  2744 _AX5043_FREQDEV00NB	=	0x512d
                           00513D  2745 G$AX5043_FREQDEV01NB$0$0 == 0x513d
                           00513D  2746 _AX5043_FREQDEV01NB	=	0x513d
                           00514D  2747 G$AX5043_FREQDEV02NB$0$0 == 0x514d
                           00514D  2748 _AX5043_FREQDEV02NB	=	0x514d
                           00515D  2749 G$AX5043_FREQDEV03NB$0$0 == 0x515d
                           00515D  2750 _AX5043_FREQDEV03NB	=	0x515d
                           00512C  2751 G$AX5043_FREQDEV10NB$0$0 == 0x512c
                           00512C  2752 _AX5043_FREQDEV10NB	=	0x512c
                           00513C  2753 G$AX5043_FREQDEV11NB$0$0 == 0x513c
                           00513C  2754 _AX5043_FREQDEV11NB	=	0x513c
                           00514C  2755 G$AX5043_FREQDEV12NB$0$0 == 0x514c
                           00514C  2756 _AX5043_FREQDEV12NB	=	0x514c
                           00515C  2757 G$AX5043_FREQDEV13NB$0$0 == 0x515c
                           00515C  2758 _AX5043_FREQDEV13NB	=	0x515c
                           005127  2759 G$AX5043_FREQUENCYGAINA0NB$0$0 == 0x5127
                           005127  2760 _AX5043_FREQUENCYGAINA0NB	=	0x5127
                           005137  2761 G$AX5043_FREQUENCYGAINA1NB$0$0 == 0x5137
                           005137  2762 _AX5043_FREQUENCYGAINA1NB	=	0x5137
                           005147  2763 G$AX5043_FREQUENCYGAINA2NB$0$0 == 0x5147
                           005147  2764 _AX5043_FREQUENCYGAINA2NB	=	0x5147
                           005157  2765 G$AX5043_FREQUENCYGAINA3NB$0$0 == 0x5157
                           005157  2766 _AX5043_FREQUENCYGAINA3NB	=	0x5157
                           005128  2767 G$AX5043_FREQUENCYGAINB0NB$0$0 == 0x5128
                           005128  2768 _AX5043_FREQUENCYGAINB0NB	=	0x5128
                           005138  2769 G$AX5043_FREQUENCYGAINB1NB$0$0 == 0x5138
                           005138  2770 _AX5043_FREQUENCYGAINB1NB	=	0x5138
                           005148  2771 G$AX5043_FREQUENCYGAINB2NB$0$0 == 0x5148
                           005148  2772 _AX5043_FREQUENCYGAINB2NB	=	0x5148
                           005158  2773 G$AX5043_FREQUENCYGAINB3NB$0$0 == 0x5158
                           005158  2774 _AX5043_FREQUENCYGAINB3NB	=	0x5158
                           005129  2775 G$AX5043_FREQUENCYGAINC0NB$0$0 == 0x5129
                           005129  2776 _AX5043_FREQUENCYGAINC0NB	=	0x5129
                           005139  2777 G$AX5043_FREQUENCYGAINC1NB$0$0 == 0x5139
                           005139  2778 _AX5043_FREQUENCYGAINC1NB	=	0x5139
                           005149  2779 G$AX5043_FREQUENCYGAINC2NB$0$0 == 0x5149
                           005149  2780 _AX5043_FREQUENCYGAINC2NB	=	0x5149
                           005159  2781 G$AX5043_FREQUENCYGAINC3NB$0$0 == 0x5159
                           005159  2782 _AX5043_FREQUENCYGAINC3NB	=	0x5159
                           00512A  2783 G$AX5043_FREQUENCYGAIND0NB$0$0 == 0x512a
                           00512A  2784 _AX5043_FREQUENCYGAIND0NB	=	0x512a
                           00513A  2785 G$AX5043_FREQUENCYGAIND1NB$0$0 == 0x513a
                           00513A  2786 _AX5043_FREQUENCYGAIND1NB	=	0x513a
                           00514A  2787 G$AX5043_FREQUENCYGAIND2NB$0$0 == 0x514a
                           00514A  2788 _AX5043_FREQUENCYGAIND2NB	=	0x514a
                           00515A  2789 G$AX5043_FREQUENCYGAIND3NB$0$0 == 0x515a
                           00515A  2790 _AX5043_FREQUENCYGAIND3NB	=	0x515a
                           005116  2791 G$AX5043_FREQUENCYLEAKNB$0$0 == 0x5116
                           005116  2792 _AX5043_FREQUENCYLEAKNB	=	0x5116
                           005126  2793 G$AX5043_PHASEGAIN0NB$0$0 == 0x5126
                           005126  2794 _AX5043_PHASEGAIN0NB	=	0x5126
                           005136  2795 G$AX5043_PHASEGAIN1NB$0$0 == 0x5136
                           005136  2796 _AX5043_PHASEGAIN1NB	=	0x5136
                           005146  2797 G$AX5043_PHASEGAIN2NB$0$0 == 0x5146
                           005146  2798 _AX5043_PHASEGAIN2NB	=	0x5146
                           005156  2799 G$AX5043_PHASEGAIN3NB$0$0 == 0x5156
                           005156  2800 _AX5043_PHASEGAIN3NB	=	0x5156
                           005207  2801 G$AX5043_PKTADDR0NB$0$0 == 0x5207
                           005207  2802 _AX5043_PKTADDR0NB	=	0x5207
                           005206  2803 G$AX5043_PKTADDR1NB$0$0 == 0x5206
                           005206  2804 _AX5043_PKTADDR1NB	=	0x5206
                           005205  2805 G$AX5043_PKTADDR2NB$0$0 == 0x5205
                           005205  2806 _AX5043_PKTADDR2NB	=	0x5205
                           005204  2807 G$AX5043_PKTADDR3NB$0$0 == 0x5204
                           005204  2808 _AX5043_PKTADDR3NB	=	0x5204
                           005200  2809 G$AX5043_PKTADDRCFGNB$0$0 == 0x5200
                           005200  2810 _AX5043_PKTADDRCFGNB	=	0x5200
                           00520B  2811 G$AX5043_PKTADDRMASK0NB$0$0 == 0x520b
                           00520B  2812 _AX5043_PKTADDRMASK0NB	=	0x520b
                           00520A  2813 G$AX5043_PKTADDRMASK1NB$0$0 == 0x520a
                           00520A  2814 _AX5043_PKTADDRMASK1NB	=	0x520a
                           005209  2815 G$AX5043_PKTADDRMASK2NB$0$0 == 0x5209
                           005209  2816 _AX5043_PKTADDRMASK2NB	=	0x5209
                           005208  2817 G$AX5043_PKTADDRMASK3NB$0$0 == 0x5208
                           005208  2818 _AX5043_PKTADDRMASK3NB	=	0x5208
                           005201  2819 G$AX5043_PKTLENCFGNB$0$0 == 0x5201
                           005201  2820 _AX5043_PKTLENCFGNB	=	0x5201
                           005202  2821 G$AX5043_PKTLENOFFSETNB$0$0 == 0x5202
                           005202  2822 _AX5043_PKTLENOFFSETNB	=	0x5202
                           005203  2823 G$AX5043_PKTMAXLENNB$0$0 == 0x5203
                           005203  2824 _AX5043_PKTMAXLENNB	=	0x5203
                           005118  2825 G$AX5043_RXPARAMCURSETNB$0$0 == 0x5118
                           005118  2826 _AX5043_RXPARAMCURSETNB	=	0x5118
                           005117  2827 G$AX5043_RXPARAMSETSNB$0$0 == 0x5117
                           005117  2828 _AX5043_RXPARAMSETSNB	=	0x5117
                           005124  2829 G$AX5043_TIMEGAIN0NB$0$0 == 0x5124
                           005124  2830 _AX5043_TIMEGAIN0NB	=	0x5124
                           005134  2831 G$AX5043_TIMEGAIN1NB$0$0 == 0x5134
                           005134  2832 _AX5043_TIMEGAIN1NB	=	0x5134
                           005144  2833 G$AX5043_TIMEGAIN2NB$0$0 == 0x5144
                           005144  2834 _AX5043_TIMEGAIN2NB	=	0x5144
                           005154  2835 G$AX5043_TIMEGAIN3NB$0$0 == 0x5154
                           005154  2836 _AX5043_TIMEGAIN3NB	=	0x5154
                           000000  2837 G$premPkt_tmr$0$0==.
      000393                       2838 _premPkt_tmr::
      000393                       2839 	.ds 8
                           000008  2840 LtransmitPrem.transmit_prem_wtimer$premSlot_wtimer$1$335==.
      00039B                       2841 _transmit_prem_wtimer_premSlot_wtimer_1_335:
      00039B                       2842 	.ds 4
                                   2843 ;--------------------------------------------------------
                                   2844 ; absolute external ram data
                                   2845 ;--------------------------------------------------------
                                   2846 	.area XABS    (ABS,XDATA)
                                   2847 ;--------------------------------------------------------
                                   2848 ; external initialized ram data
                                   2849 ;--------------------------------------------------------
                                   2850 	.area XISEG   (XDATA)
                           000000  2851 G$trmID$0$0==.
      0009BC                       2852 _trmID::
      0009BC                       2853 	.ds 4
                           000004  2854 G$StdPrem$0$0==.
      0009C0                       2855 _StdPrem::
      0009C0                       2856 	.ds 1
                                   2857 	.area HOME    (CODE)
                                   2858 	.area GSINIT0 (CODE)
                                   2859 	.area GSINIT1 (CODE)
                                   2860 	.area GSINIT2 (CODE)
                                   2861 	.area GSINIT3 (CODE)
                                   2862 	.area GSINIT4 (CODE)
                                   2863 	.area GSINIT5 (CODE)
                                   2864 	.area GSINIT  (CODE)
                                   2865 	.area GSFINAL (CODE)
                                   2866 	.area CSEG    (CODE)
                                   2867 ;--------------------------------------------------------
                                   2868 ; global & static initialisations
                                   2869 ;--------------------------------------------------------
                                   2870 	.area HOME    (CODE)
                                   2871 	.area GSINIT  (CODE)
                                   2872 	.area GSFINAL (CODE)
                                   2873 	.area GSINIT  (CODE)
                                   2874 ;--------------------------------------------------------
                                   2875 ; Home
                                   2876 ;--------------------------------------------------------
                                   2877 	.area HOME    (CODE)
                                   2878 	.area HOME    (CODE)
                                   2879 ;--------------------------------------------------------
                                   2880 ; code
                                   2881 ;--------------------------------------------------------
                                   2882 	.area CSEG    (CODE)
                                   2883 ;------------------------------------------------------------
                                   2884 ;Allocation info for local variables in function 'premPkt_callback'
                                   2885 ;------------------------------------------------------------
                                   2886 ;desc                      Allocated to registers 
                                   2887 ;------------------------------------------------------------
                           000000  2888 	G$premPkt_callback$0$0 ==.
                           000000  2889 	C$transmitPrem.c$34$0$0 ==.
                                   2890 ;	..\COMMON\transmitPrem.c:34: void premPkt_callback(struct wtimer_desc __xdata *desc)
                                   2891 ;	-----------------------------------------
                                   2892 ;	 function premPkt_callback
                                   2893 ;	-----------------------------------------
      004FB0                       2894 _premPkt_callback:
                           000007  2895 	ar7 = 0x07
                           000006  2896 	ar6 = 0x06
                           000005  2897 	ar5 = 0x05
                           000004  2898 	ar4 = 0x04
                           000003  2899 	ar3 = 0x03
                           000002  2900 	ar2 = 0x02
                           000001  2901 	ar1 = 0x01
                           000000  2902 	ar0 = 0x00
                           000000  2903 	C$transmitPrem.c$40$1$333 ==.
                                   2904 ;	..\COMMON\transmitPrem.c:40: axradio_transmit(&remoteaddr, premPacketBuffer, pktLENGTH);
      004FB0 75 1B 2C         [24] 2905 	mov	_axradio_transmit_PARM_2,#_premPacketBuffer
      004FB3 75 1C 03         [24] 2906 	mov	(_axradio_transmit_PARM_2 + 1),#(_premPacketBuffer >> 8)
      004FB6 75 1D 00         [24] 2907 	mov	(_axradio_transmit_PARM_2 + 2),#0x00
      004FB9 75 1E 2D         [24] 2908 	mov	_axradio_transmit_PARM_3,#0x2d
      004FBC 75 1F 00         [24] 2909 	mov	(_axradio_transmit_PARM_3 + 1),#0x00
      004FBF 90 7F 6F         [24] 2910 	mov	dptr,#_remoteaddr
      004FC2 75 F0 80         [24] 2911 	mov	b,#0x80
      004FC5 12 3A D2         [24] 2912 	lcall	_axradio_transmit
                           000018  2913 	C$transmitPrem.c$42$1$333 ==.
                                   2914 ;	..\COMMON\transmitPrem.c:42: dbglink_writestr("6_TX0_WT0: ");
      004FC8 90 81 5B         [24] 2915 	mov	dptr,#___str_0
      004FCB 75 F0 80         [24] 2916 	mov	b,#0x80
      004FCE 12 73 05         [24] 2917 	lcall	_dbglink_writestr
                           000021  2918 	C$transmitPrem.c$43$1$333 ==.
                                   2919 ;	..\COMMON\transmitPrem.c:43: dbglink_writehex32(wtimer0_curtime(), 8, WRNUM_PADZERO);
      004FD1 12 7D AD         [24] 2920 	lcall	_wtimer0_curtime
      004FD4 AC 82            [24] 2921 	mov	r4,dpl
      004FD6 AD 83            [24] 2922 	mov	r5,dph
      004FD8 AE F0            [24] 2923 	mov	r6,b
      004FDA FF               [12] 2924 	mov	r7,a
      004FDB 74 08            [12] 2925 	mov	a,#0x08
      004FDD C0 E0            [24] 2926 	push	acc
      004FDF C0 E0            [24] 2927 	push	acc
      004FE1 8C 82            [24] 2928 	mov	dpl,r4
      004FE3 8D 83            [24] 2929 	mov	dph,r5
      004FE5 8E F0            [24] 2930 	mov	b,r6
      004FE7 EF               [12] 2931 	mov	a,r7
      004FE8 12 75 88         [24] 2932 	lcall	_dbglink_writehex32
      004FEB 15 81            [12] 2933 	dec	sp
      004FED 15 81            [12] 2934 	dec	sp
                           00003F  2935 	C$transmitPrem.c$44$1$333 ==.
                                   2936 ;	..\COMMON\transmitPrem.c:44: dbglink_tx('\n');
      004FEF 75 82 0A         [24] 2937 	mov	dpl,#0x0a
      004FF2 12 61 21         [24] 2938 	lcall	_dbglink_tx
                           000045  2939 	C$transmitPrem.c$45$1$333 ==.
                                   2940 ;	..\COMMON\transmitPrem.c:45: dbglink_writestr("61_TX0_delta: ");
      004FF5 90 81 67         [24] 2941 	mov	dptr,#___str_1
      004FF8 75 F0 80         [24] 2942 	mov	b,#0x80
      004FFB 12 73 05         [24] 2943 	lcall	_dbglink_writestr
                           00004E  2944 	C$transmitPrem.c$46$1$333 ==.
                                   2945 ;	..\COMMON\transmitPrem.c:46: dbglink_writehex32((wtimer0_curtime()-wt0_beacon), 4, WRNUM_PADZERO);
      004FFE 12 7D AD         [24] 2946 	lcall	_wtimer0_curtime
      005001 AC 82            [24] 2947 	mov	r4,dpl
      005003 AD 83            [24] 2948 	mov	r5,dph
      005005 AE F0            [24] 2949 	mov	r6,b
      005007 FF               [12] 2950 	mov	r7,a
      005008 90 09 CF         [24] 2951 	mov	dptr,#_wt0_beacon
      00500B E0               [24] 2952 	movx	a,@dptr
      00500C F8               [12] 2953 	mov	r0,a
      00500D A3               [24] 2954 	inc	dptr
      00500E E0               [24] 2955 	movx	a,@dptr
      00500F F9               [12] 2956 	mov	r1,a
      005010 A3               [24] 2957 	inc	dptr
      005011 E0               [24] 2958 	movx	a,@dptr
      005012 FA               [12] 2959 	mov	r2,a
      005013 A3               [24] 2960 	inc	dptr
      005014 E0               [24] 2961 	movx	a,@dptr
      005015 FB               [12] 2962 	mov	r3,a
      005016 EC               [12] 2963 	mov	a,r4
      005017 C3               [12] 2964 	clr	c
      005018 98               [12] 2965 	subb	a,r0
      005019 FC               [12] 2966 	mov	r4,a
      00501A ED               [12] 2967 	mov	a,r5
      00501B 99               [12] 2968 	subb	a,r1
      00501C FD               [12] 2969 	mov	r5,a
      00501D EE               [12] 2970 	mov	a,r6
      00501E 9A               [12] 2971 	subb	a,r2
      00501F FE               [12] 2972 	mov	r6,a
      005020 EF               [12] 2973 	mov	a,r7
      005021 9B               [12] 2974 	subb	a,r3
      005022 FF               [12] 2975 	mov	r7,a
      005023 74 08            [12] 2976 	mov	a,#0x08
      005025 C0 E0            [24] 2977 	push	acc
      005027 03               [12] 2978 	rr	a
      005028 C0 E0            [24] 2979 	push	acc
      00502A 8C 82            [24] 2980 	mov	dpl,r4
      00502C 8D 83            [24] 2981 	mov	dph,r5
      00502E 8E F0            [24] 2982 	mov	b,r6
      005030 EF               [12] 2983 	mov	a,r7
      005031 12 75 88         [24] 2984 	lcall	_dbglink_writehex32
      005034 15 81            [12] 2985 	dec	sp
      005036 15 81            [12] 2986 	dec	sp
                           000088  2987 	C$transmitPrem.c$47$1$333 ==.
                                   2988 ;	..\COMMON\transmitPrem.c:47: dbglink_tx('\n');
      005038 75 82 0A         [24] 2989 	mov	dpl,#0x0a
      00503B 12 61 21         [24] 2990 	lcall	_dbglink_tx
                           00008E  2991 	C$transmitPrem.c$50$1$333 ==.
                           00008E  2992 	XG$premPkt_callback$0$0 ==.
      00503E 22               [24] 2993 	ret
                                   2994 ;------------------------------------------------------------
                                   2995 ;Allocation info for local variables in function 'transmit_prem_wtimer'
                                   2996 ;------------------------------------------------------------
                                   2997 ;premSlot_wtimer           Allocated with name '_transmit_prem_wtimer_premSlot_wtimer_1_335'
                                   2998 ;------------------------------------------------------------
                           00008F  2999 	G$transmit_prem_wtimer$0$0 ==.
                           00008F  3000 	C$transmitPrem.c$52$1$333 ==.
                                   3001 ;	..\COMMON\transmitPrem.c:52: void transmit_prem_wtimer(void)
                                   3002 ;	-----------------------------------------
                                   3003 ;	 function transmit_prem_wtimer
                                   3004 ;	-----------------------------------------
      00503F                       3005 _transmit_prem_wtimer:
                           00008F  3006 	C$transmitPrem.c$54$1$333 ==.
                                   3007 ;	..\COMMON\transmitPrem.c:54: uint32_t __xdata premSlot_wtimer = 0;
      00503F 90 03 9B         [24] 3008 	mov	dptr,#_transmit_prem_wtimer_premSlot_wtimer_1_335
      005042 E4               [12] 3009 	clr	a
      005043 F0               [24] 3010 	movx	@dptr,a
      005044 A3               [24] 3011 	inc	dptr
      005045 F0               [24] 3012 	movx	@dptr,a
      005046 A3               [24] 3013 	inc	dptr
      005047 F0               [24] 3014 	movx	@dptr,a
      005048 A3               [24] 3015 	inc	dptr
      005049 F0               [24] 3016 	movx	@dptr,a
                           00009A  3017 	C$transmitPrem.c$58$1$335 ==.
                                   3018 ;	..\COMMON\transmitPrem.c:58: if (StdPrem == 0x0)
      00504A 90 09 C0         [24] 3019 	mov	dptr,#_StdPrem
      00504D E0               [24] 3020 	movx	a,@dptr
      00504E FF               [12] 3021 	mov	r7,a
      00504F E0               [24] 3022 	movx	a,@dptr
      005050 60 03            [24] 3023 	jz	00119$
      005052 02 51 12         [24] 3024 	ljmp	00106$
      005055                       3025 00119$:
                           0000A5  3026 	C$transmitPrem.c$61$2$336 ==.
                                   3027 ;	..\COMMON\transmitPrem.c:61: if (premSlot_counter == 10)
      005055 90 09 CE         [24] 3028 	mov	dptr,#_premSlot_counter
      005058 E0               [24] 3029 	movx	a,@dptr
      005059 FE               [12] 3030 	mov	r6,a
      00505A BE 0A 05         [24] 3031 	cjne	r6,#0x0a,00102$
                           0000AD  3032 	C$transmitPrem.c$63$3$337 ==.
                                   3033 ;	..\COMMON\transmitPrem.c:63: premSlot_counter = 0;
      00505D 90 09 CE         [24] 3034 	mov	dptr,#_premSlot_counter
      005060 E4               [12] 3035 	clr	a
      005061 F0               [24] 3036 	movx	@dptr,a
      005062                       3037 00102$:
                           0000B2  3038 	C$transmitPrem.c$65$2$336 ==.
                                   3039 ;	..\COMMON\transmitPrem.c:65: premSlot_wtimer = ((premSlot_counter+slotPremBASE) * slotPremWIDTH_MS) * 0.64;
      005062 90 09 CE         [24] 3040 	mov	dptr,#_premSlot_counter
      005065 E0               [24] 3041 	movx	a,@dptr
      005066 FE               [12] 3042 	mov	r6,a
      005067 7D 00            [12] 3043 	mov	r5,#0x00
      005069 8E 82            [24] 3044 	mov	dpl,r6
      00506B 8D 83            [24] 3045 	mov	dph,r5
      00506D A3               [24] 3046 	inc	dptr
      00506E A3               [24] 3047 	inc	dptr
      00506F 12 75 51         [24] 3048 	lcall	___sint2fs
      005072 AB 82            [24] 3049 	mov	r3,dpl
      005074 AC 83            [24] 3050 	mov	r4,dph
      005076 AD F0            [24] 3051 	mov	r5,b
      005078 FE               [12] 3052 	mov	r6,a
      005079 C0 03            [24] 3053 	push	ar3
      00507B C0 04            [24] 3054 	push	ar4
      00507D C0 05            [24] 3055 	push	ar5
      00507F C0 06            [24] 3056 	push	ar6
      005081 90 CC CD         [24] 3057 	mov	dptr,#0xcccd
      005084 75 F0 CC         [24] 3058 	mov	b,#0xcc
      005087 74 42            [12] 3059 	mov	a,#0x42
      005089 12 60 27         [24] 3060 	lcall	___fsmul
      00508C AB 82            [24] 3061 	mov	r3,dpl
      00508E AC 83            [24] 3062 	mov	r4,dph
      005090 AD F0            [24] 3063 	mov	r5,b
      005092 FE               [12] 3064 	mov	r6,a
      005093 E5 81            [12] 3065 	mov	a,sp
      005095 24 FC            [12] 3066 	add	a,#0xfc
      005097 F5 81            [12] 3067 	mov	sp,a
      005099 8B 82            [24] 3068 	mov	dpl,r3
      00509B 8C 83            [24] 3069 	mov	dph,r4
      00509D 8D F0            [24] 3070 	mov	b,r5
      00509F EE               [12] 3071 	mov	a,r6
      0050A0 12 6B C8         [24] 3072 	lcall	___fs2ulong
      0050A3 AB 82            [24] 3073 	mov	r3,dpl
      0050A5 AC 83            [24] 3074 	mov	r4,dph
      0050A7 AD F0            [24] 3075 	mov	r5,b
      0050A9 FE               [12] 3076 	mov	r6,a
      0050AA 90 03 9B         [24] 3077 	mov	dptr,#_transmit_prem_wtimer_premSlot_wtimer_1_335
      0050AD EB               [12] 3078 	mov	a,r3
      0050AE F0               [24] 3079 	movx	@dptr,a
      0050AF EC               [12] 3080 	mov	a,r4
      0050B0 A3               [24] 3081 	inc	dptr
      0050B1 F0               [24] 3082 	movx	@dptr,a
      0050B2 ED               [12] 3083 	mov	a,r5
      0050B3 A3               [24] 3084 	inc	dptr
      0050B4 F0               [24] 3085 	movx	@dptr,a
      0050B5 EE               [12] 3086 	mov	a,r6
      0050B6 A3               [24] 3087 	inc	dptr
      0050B7 F0               [24] 3088 	movx	@dptr,a
                           000108  3089 	C$transmitPrem.c$68$2$336 ==.
                                   3090 ;	..\COMMON\transmitPrem.c:68: dbglink_writestr("62_premSlot:");
      0050B8 90 81 76         [24] 3091 	mov	dptr,#___str_2
      0050BB 75 F0 80         [24] 3092 	mov	b,#0x80
      0050BE 12 73 05         [24] 3093 	lcall	_dbglink_writestr
                           000111  3094 	C$transmitPrem.c$69$2$336 ==.
                                   3095 ;	..\COMMON\transmitPrem.c:69: dbglink_writehex16(premSlot_counter, 1, WRNUM_PADZERO);
      0050C1 90 09 CE         [24] 3096 	mov	dptr,#_premSlot_counter
      0050C4 E0               [24] 3097 	movx	a,@dptr
      0050C5 FE               [12] 3098 	mov	r6,a
      0050C6 7D 00            [12] 3099 	mov	r5,#0x00
      0050C8 74 08            [12] 3100 	mov	a,#0x08
      0050CA C0 E0            [24] 3101 	push	acc
      0050CC 74 01            [12] 3102 	mov	a,#0x01
      0050CE C0 E0            [24] 3103 	push	acc
      0050D0 8E 82            [24] 3104 	mov	dpl,r6
      0050D2 8D 83            [24] 3105 	mov	dph,r5
      0050D4 12 77 A6         [24] 3106 	lcall	_dbglink_writehex16
      0050D7 15 81            [12] 3107 	dec	sp
      0050D9 15 81            [12] 3108 	dec	sp
                           00012B  3109 	C$transmitPrem.c$70$2$336 ==.
                                   3110 ;	..\COMMON\transmitPrem.c:70: dbglink_tx(' ');
      0050DB 75 82 20         [24] 3111 	mov	dpl,#0x20
      0050DE 12 61 21         [24] 3112 	lcall	_dbglink_tx
                           000131  3113 	C$transmitPrem.c$71$2$336 ==.
                                   3114 ;	..\COMMON\transmitPrem.c:71: dbglink_writehex32(premSlot_wtimer, 8, WRNUM_PADZERO);
      0050E1 90 03 9B         [24] 3115 	mov	dptr,#_transmit_prem_wtimer_premSlot_wtimer_1_335
      0050E4 E0               [24] 3116 	movx	a,@dptr
      0050E5 FB               [12] 3117 	mov	r3,a
      0050E6 A3               [24] 3118 	inc	dptr
      0050E7 E0               [24] 3119 	movx	a,@dptr
      0050E8 FC               [12] 3120 	mov	r4,a
      0050E9 A3               [24] 3121 	inc	dptr
      0050EA E0               [24] 3122 	movx	a,@dptr
      0050EB FD               [12] 3123 	mov	r5,a
      0050EC A3               [24] 3124 	inc	dptr
      0050ED E0               [24] 3125 	movx	a,@dptr
      0050EE FE               [12] 3126 	mov	r6,a
      0050EF 74 08            [12] 3127 	mov	a,#0x08
      0050F1 C0 E0            [24] 3128 	push	acc
      0050F3 C0 E0            [24] 3129 	push	acc
      0050F5 8B 82            [24] 3130 	mov	dpl,r3
      0050F7 8C 83            [24] 3131 	mov	dph,r4
      0050F9 8D F0            [24] 3132 	mov	b,r5
      0050FB EE               [12] 3133 	mov	a,r6
      0050FC 12 75 88         [24] 3134 	lcall	_dbglink_writehex32
      0050FF 15 81            [12] 3135 	dec	sp
      005101 15 81            [12] 3136 	dec	sp
                           000153  3137 	C$transmitPrem.c$72$2$336 ==.
                                   3138 ;	..\COMMON\transmitPrem.c:72: dbglink_tx('\n');
      005103 75 82 0A         [24] 3139 	mov	dpl,#0x0a
      005106 12 61 21         [24] 3140 	lcall	_dbglink_tx
                           000159  3141 	C$transmitPrem.c$75$2$336 ==.
                                   3142 ;	..\COMMON\transmitPrem.c:75: premSlot_counter += 1;
      005109 90 09 CE         [24] 3143 	mov	dptr,#_premSlot_counter
      00510C E0               [24] 3144 	movx	a,@dptr
      00510D FE               [12] 3145 	mov	r6,a
      00510E 04               [12] 3146 	inc	a
      00510F F0               [24] 3147 	movx	@dptr,a
      005110 80 12            [24] 3148 	sjmp	00107$
      005112                       3149 00106$:
                           000162  3150 	C$transmitPrem.c$78$1$335 ==.
                                   3151 ;	..\COMMON\transmitPrem.c:78: else if (StdPrem == 0x2)
      005112 BF 02 0F         [24] 3152 	cjne	r7,#0x02,00107$
                           000165  3153 	C$transmitPrem.c$80$2$338 ==.
                                   3154 ;	..\COMMON\transmitPrem.c:80: premSlot_wtimer = (slotPremBASE + nslotPrem - 1) * slotPremWIDTH_MS * 0.64;
      005115 90 03 9B         [24] 3155 	mov	dptr,#_transmit_prem_wtimer_premSlot_wtimer_1_335
      005118 74 66            [12] 3156 	mov	a,#0x66
      00511A F0               [24] 3157 	movx	@dptr,a
      00511B 74 04            [12] 3158 	mov	a,#0x04
      00511D A3               [24] 3159 	inc	dptr
      00511E F0               [24] 3160 	movx	@dptr,a
      00511F E4               [12] 3161 	clr	a
      005120 A3               [24] 3162 	inc	dptr
      005121 F0               [24] 3163 	movx	@dptr,a
      005122 A3               [24] 3164 	inc	dptr
      005123 F0               [24] 3165 	movx	@dptr,a
      005124                       3166 00107$:
                           000174  3167 	C$transmitPrem.c$85$1$335 ==.
                                   3168 ;	..\COMMON\transmitPrem.c:85: wtimer0_remove(&premPkt_tmr);
      005124 90 03 93         [24] 3169 	mov	dptr,#_premPkt_tmr
      005127 12 6D 98         [24] 3170 	lcall	_wtimer0_remove
                           00017A  3171 	C$transmitPrem.c$86$1$335 ==.
                                   3172 ;	..\COMMON\transmitPrem.c:86: premPkt_tmr.handler = premPkt_callback;
      00512A 90 03 95         [24] 3173 	mov	dptr,#(_premPkt_tmr + 0x0002)
      00512D 74 B0            [12] 3174 	mov	a,#_premPkt_callback
      00512F F0               [24] 3175 	movx	@dptr,a
      005130 74 4F            [12] 3176 	mov	a,#(_premPkt_callback >> 8)
      005132 A3               [24] 3177 	inc	dptr
      005133 F0               [24] 3178 	movx	@dptr,a
                           000184  3179 	C$transmitPrem.c$87$1$335 ==.
                                   3180 ;	..\COMMON\transmitPrem.c:87: premPkt_tmr.time = premSlot_wtimer;
      005134 90 03 9B         [24] 3181 	mov	dptr,#_transmit_prem_wtimer_premSlot_wtimer_1_335
      005137 E0               [24] 3182 	movx	a,@dptr
      005138 FC               [12] 3183 	mov	r4,a
      005139 A3               [24] 3184 	inc	dptr
      00513A E0               [24] 3185 	movx	a,@dptr
      00513B FD               [12] 3186 	mov	r5,a
      00513C A3               [24] 3187 	inc	dptr
      00513D E0               [24] 3188 	movx	a,@dptr
      00513E FE               [12] 3189 	mov	r6,a
      00513F A3               [24] 3190 	inc	dptr
      005140 E0               [24] 3191 	movx	a,@dptr
      005141 FF               [12] 3192 	mov	r7,a
      005142 90 03 97         [24] 3193 	mov	dptr,#(_premPkt_tmr + 0x0004)
      005145 EC               [12] 3194 	mov	a,r4
      005146 F0               [24] 3195 	movx	@dptr,a
      005147 ED               [12] 3196 	mov	a,r5
      005148 A3               [24] 3197 	inc	dptr
      005149 F0               [24] 3198 	movx	@dptr,a
      00514A EE               [12] 3199 	mov	a,r6
      00514B A3               [24] 3200 	inc	dptr
      00514C F0               [24] 3201 	movx	@dptr,a
      00514D EF               [12] 3202 	mov	a,r7
      00514E A3               [24] 3203 	inc	dptr
      00514F F0               [24] 3204 	movx	@dptr,a
                           0001A0  3205 	C$transmitPrem.c$88$1$335 ==.
                                   3206 ;	..\COMMON\transmitPrem.c:88: wtimer0_addrelative(&premPkt_tmr);
      005150 90 03 93         [24] 3207 	mov	dptr,#_premPkt_tmr
      005153 12 69 E7         [24] 3208 	lcall	_wtimer0_addrelative
                           0001A6  3209 	C$transmitPrem.c$89$1$335 ==.
                           0001A6  3210 	XG$transmit_prem_wtimer$0$0 ==.
      005156 22               [24] 3211 	ret
                                   3212 	.area CSEG    (CODE)
                                   3213 	.area CONST   (CODE)
                           000000  3214 FtransmitPrem$__str_0$0$0 == .
      00815B                       3215 ___str_0:
      00815B 36 5F 54 58 30 5F 57  3216 	.ascii "6_TX0_WT0: "
             54 30 3A 20
      008166 00                    3217 	.db 0x00
                           00000C  3218 FtransmitPrem$__str_1$0$0 == .
      008167                       3219 ___str_1:
      008167 36 31 5F 54 58 30 5F  3220 	.ascii "61_TX0_delta: "
             64 65 6C 74 61 3A 20
      008175 00                    3221 	.db 0x00
                           00001B  3222 FtransmitPrem$__str_2$0$0 == .
      008176                       3223 ___str_2:
      008176 36 32 5F 70 72 65 6D  3224 	.ascii "62_premSlot:"
             53 6C 6F 74 3A
      008182 00                    3225 	.db 0x00
                                   3226 	.area XINIT   (CODE)
                           000000  3227 FtransmitPrem$__xinit_trmID$0$0 == .
      0087F7                       3228 __xinit__trmID:
      0087F7 01 00 10 00           3229 	.byte #0x01,#0x00,#0x10,#0x00	; 1048577
                           000004  3230 FtransmitPrem$__xinit_StdPrem$0$0 == .
      0087FB                       3231 __xinit__StdPrem:
      0087FB 01                    3232 	.db #0x01	; 1
                                   3233 	.area CABS    (ABS,CODE)
