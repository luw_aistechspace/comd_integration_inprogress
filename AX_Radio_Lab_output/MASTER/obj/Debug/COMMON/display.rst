                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.0 #9615 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module display
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _wait_dbglink_free
                                     12 	.globl _com0_writestr
                                     13 	.globl _com0_setpos
                                     14 	.globl _uart0_writehex16
                                     15 	.globl _uart0_writenum16
                                     16 	.globl _dbglink_writenum32
                                     17 	.globl _dbglink_writehex16
                                     18 	.globl _dbglink_writenum16
                                     19 	.globl _dbglink_writestr
                                     20 	.globl _dbglink_tx
                                     21 	.globl _dbglink_txfree
                                     22 	.globl _axradio_conv_freq_tohz
                                     23 	.globl _axradio_get_freqoffset
                                     24 	.globl _wtimer_runcallbacks
                                     25 	.globl _wtimer_idle
                                     26 	.globl _PORTC_7
                                     27 	.globl _PORTC_6
                                     28 	.globl _PORTC_5
                                     29 	.globl _PORTC_4
                                     30 	.globl _PORTC_3
                                     31 	.globl _PORTC_2
                                     32 	.globl _PORTC_1
                                     33 	.globl _PORTC_0
                                     34 	.globl _PORTB_7
                                     35 	.globl _PORTB_6
                                     36 	.globl _PORTB_5
                                     37 	.globl _PORTB_4
                                     38 	.globl _PORTB_3
                                     39 	.globl _PORTB_2
                                     40 	.globl _PORTB_1
                                     41 	.globl _PORTB_0
                                     42 	.globl _PORTA_7
                                     43 	.globl _PORTA_6
                                     44 	.globl _PORTA_5
                                     45 	.globl _PORTA_4
                                     46 	.globl _PORTA_3
                                     47 	.globl _PORTA_2
                                     48 	.globl _PORTA_1
                                     49 	.globl _PORTA_0
                                     50 	.globl _PINC_7
                                     51 	.globl _PINC_6
                                     52 	.globl _PINC_5
                                     53 	.globl _PINC_4
                                     54 	.globl _PINC_3
                                     55 	.globl _PINC_2
                                     56 	.globl _PINC_1
                                     57 	.globl _PINC_0
                                     58 	.globl _PINB_7
                                     59 	.globl _PINB_6
                                     60 	.globl _PINB_5
                                     61 	.globl _PINB_4
                                     62 	.globl _PINB_3
                                     63 	.globl _PINB_2
                                     64 	.globl _PINB_1
                                     65 	.globl _PINB_0
                                     66 	.globl _PINA_7
                                     67 	.globl _PINA_6
                                     68 	.globl _PINA_5
                                     69 	.globl _PINA_4
                                     70 	.globl _PINA_3
                                     71 	.globl _PINA_2
                                     72 	.globl _PINA_1
                                     73 	.globl _PINA_0
                                     74 	.globl _CY
                                     75 	.globl _AC
                                     76 	.globl _F0
                                     77 	.globl _RS1
                                     78 	.globl _RS0
                                     79 	.globl _OV
                                     80 	.globl _F1
                                     81 	.globl _P
                                     82 	.globl _IP_7
                                     83 	.globl _IP_6
                                     84 	.globl _IP_5
                                     85 	.globl _IP_4
                                     86 	.globl _IP_3
                                     87 	.globl _IP_2
                                     88 	.globl _IP_1
                                     89 	.globl _IP_0
                                     90 	.globl _EA
                                     91 	.globl _IE_7
                                     92 	.globl _IE_6
                                     93 	.globl _IE_5
                                     94 	.globl _IE_4
                                     95 	.globl _IE_3
                                     96 	.globl _IE_2
                                     97 	.globl _IE_1
                                     98 	.globl _IE_0
                                     99 	.globl _EIP_7
                                    100 	.globl _EIP_6
                                    101 	.globl _EIP_5
                                    102 	.globl _EIP_4
                                    103 	.globl _EIP_3
                                    104 	.globl _EIP_2
                                    105 	.globl _EIP_1
                                    106 	.globl _EIP_0
                                    107 	.globl _EIE_7
                                    108 	.globl _EIE_6
                                    109 	.globl _EIE_5
                                    110 	.globl _EIE_4
                                    111 	.globl _EIE_3
                                    112 	.globl _EIE_2
                                    113 	.globl _EIE_1
                                    114 	.globl _EIE_0
                                    115 	.globl _E2IP_7
                                    116 	.globl _E2IP_6
                                    117 	.globl _E2IP_5
                                    118 	.globl _E2IP_4
                                    119 	.globl _E2IP_3
                                    120 	.globl _E2IP_2
                                    121 	.globl _E2IP_1
                                    122 	.globl _E2IP_0
                                    123 	.globl _E2IE_7
                                    124 	.globl _E2IE_6
                                    125 	.globl _E2IE_5
                                    126 	.globl _E2IE_4
                                    127 	.globl _E2IE_3
                                    128 	.globl _E2IE_2
                                    129 	.globl _E2IE_1
                                    130 	.globl _E2IE_0
                                    131 	.globl _B_7
                                    132 	.globl _B_6
                                    133 	.globl _B_5
                                    134 	.globl _B_4
                                    135 	.globl _B_3
                                    136 	.globl _B_2
                                    137 	.globl _B_1
                                    138 	.globl _B_0
                                    139 	.globl _ACC_7
                                    140 	.globl _ACC_6
                                    141 	.globl _ACC_5
                                    142 	.globl _ACC_4
                                    143 	.globl _ACC_3
                                    144 	.globl _ACC_2
                                    145 	.globl _ACC_1
                                    146 	.globl _ACC_0
                                    147 	.globl _WTSTAT
                                    148 	.globl _WTIRQEN
                                    149 	.globl _WTEVTD
                                    150 	.globl _WTEVTD1
                                    151 	.globl _WTEVTD0
                                    152 	.globl _WTEVTC
                                    153 	.globl _WTEVTC1
                                    154 	.globl _WTEVTC0
                                    155 	.globl _WTEVTB
                                    156 	.globl _WTEVTB1
                                    157 	.globl _WTEVTB0
                                    158 	.globl _WTEVTA
                                    159 	.globl _WTEVTA1
                                    160 	.globl _WTEVTA0
                                    161 	.globl _WTCNTR1
                                    162 	.globl _WTCNTB
                                    163 	.globl _WTCNTB1
                                    164 	.globl _WTCNTB0
                                    165 	.globl _WTCNTA
                                    166 	.globl _WTCNTA1
                                    167 	.globl _WTCNTA0
                                    168 	.globl _WTCFGB
                                    169 	.globl _WTCFGA
                                    170 	.globl _WDTRESET
                                    171 	.globl _WDTCFG
                                    172 	.globl _U1STATUS
                                    173 	.globl _U1SHREG
                                    174 	.globl _U1MODE
                                    175 	.globl _U1CTRL
                                    176 	.globl _U0STATUS
                                    177 	.globl _U0SHREG
                                    178 	.globl _U0MODE
                                    179 	.globl _U0CTRL
                                    180 	.globl _T2STATUS
                                    181 	.globl _T2PERIOD
                                    182 	.globl _T2PERIOD1
                                    183 	.globl _T2PERIOD0
                                    184 	.globl _T2MODE
                                    185 	.globl _T2CNT
                                    186 	.globl _T2CNT1
                                    187 	.globl _T2CNT0
                                    188 	.globl _T2CLKSRC
                                    189 	.globl _T1STATUS
                                    190 	.globl _T1PERIOD
                                    191 	.globl _T1PERIOD1
                                    192 	.globl _T1PERIOD0
                                    193 	.globl _T1MODE
                                    194 	.globl _T1CNT
                                    195 	.globl _T1CNT1
                                    196 	.globl _T1CNT0
                                    197 	.globl _T1CLKSRC
                                    198 	.globl _T0STATUS
                                    199 	.globl _T0PERIOD
                                    200 	.globl _T0PERIOD1
                                    201 	.globl _T0PERIOD0
                                    202 	.globl _T0MODE
                                    203 	.globl _T0CNT
                                    204 	.globl _T0CNT1
                                    205 	.globl _T0CNT0
                                    206 	.globl _T0CLKSRC
                                    207 	.globl _SPSTATUS
                                    208 	.globl _SPSHREG
                                    209 	.globl _SPMODE
                                    210 	.globl _SPCLKSRC
                                    211 	.globl _RADIOSTAT
                                    212 	.globl _RADIOSTAT1
                                    213 	.globl _RADIOSTAT0
                                    214 	.globl _RADIODATA
                                    215 	.globl _RADIODATA3
                                    216 	.globl _RADIODATA2
                                    217 	.globl _RADIODATA1
                                    218 	.globl _RADIODATA0
                                    219 	.globl _RADIOADDR
                                    220 	.globl _RADIOADDR1
                                    221 	.globl _RADIOADDR0
                                    222 	.globl _RADIOACC
                                    223 	.globl _OC1STATUS
                                    224 	.globl _OC1PIN
                                    225 	.globl _OC1MODE
                                    226 	.globl _OC1COMP
                                    227 	.globl _OC1COMP1
                                    228 	.globl _OC1COMP0
                                    229 	.globl _OC0STATUS
                                    230 	.globl _OC0PIN
                                    231 	.globl _OC0MODE
                                    232 	.globl _OC0COMP
                                    233 	.globl _OC0COMP1
                                    234 	.globl _OC0COMP0
                                    235 	.globl _NVSTATUS
                                    236 	.globl _NVKEY
                                    237 	.globl _NVDATA
                                    238 	.globl _NVDATA1
                                    239 	.globl _NVDATA0
                                    240 	.globl _NVADDR
                                    241 	.globl _NVADDR1
                                    242 	.globl _NVADDR0
                                    243 	.globl _IC1STATUS
                                    244 	.globl _IC1MODE
                                    245 	.globl _IC1CAPT
                                    246 	.globl _IC1CAPT1
                                    247 	.globl _IC1CAPT0
                                    248 	.globl _IC0STATUS
                                    249 	.globl _IC0MODE
                                    250 	.globl _IC0CAPT
                                    251 	.globl _IC0CAPT1
                                    252 	.globl _IC0CAPT0
                                    253 	.globl _PORTR
                                    254 	.globl _PORTC
                                    255 	.globl _PORTB
                                    256 	.globl _PORTA
                                    257 	.globl _PINR
                                    258 	.globl _PINC
                                    259 	.globl _PINB
                                    260 	.globl _PINA
                                    261 	.globl _DIRR
                                    262 	.globl _DIRC
                                    263 	.globl _DIRB
                                    264 	.globl _DIRA
                                    265 	.globl _DBGLNKSTAT
                                    266 	.globl _DBGLNKBUF
                                    267 	.globl _CODECONFIG
                                    268 	.globl _CLKSTAT
                                    269 	.globl _CLKCON
                                    270 	.globl _ANALOGCOMP
                                    271 	.globl _ADCCONV
                                    272 	.globl _ADCCLKSRC
                                    273 	.globl _ADCCH3CONFIG
                                    274 	.globl _ADCCH2CONFIG
                                    275 	.globl _ADCCH1CONFIG
                                    276 	.globl _ADCCH0CONFIG
                                    277 	.globl __XPAGE
                                    278 	.globl _XPAGE
                                    279 	.globl _SP
                                    280 	.globl _PSW
                                    281 	.globl _PCON
                                    282 	.globl _IP
                                    283 	.globl _IE
                                    284 	.globl _EIP
                                    285 	.globl _EIE
                                    286 	.globl _E2IP
                                    287 	.globl _E2IE
                                    288 	.globl _DPS
                                    289 	.globl _DPTR1
                                    290 	.globl _DPTR0
                                    291 	.globl _DPL1
                                    292 	.globl _DPL
                                    293 	.globl _DPH1
                                    294 	.globl _DPH
                                    295 	.globl _B
                                    296 	.globl _ACC
                                    297 	.globl _XTALREADY
                                    298 	.globl _XTALOSC
                                    299 	.globl _XTALAMPL
                                    300 	.globl _SILICONREV
                                    301 	.globl _SCRATCH3
                                    302 	.globl _SCRATCH2
                                    303 	.globl _SCRATCH1
                                    304 	.globl _SCRATCH0
                                    305 	.globl _RADIOMUX
                                    306 	.globl _RADIOFSTATADDR
                                    307 	.globl _RADIOFSTATADDR1
                                    308 	.globl _RADIOFSTATADDR0
                                    309 	.globl _RADIOFDATAADDR
                                    310 	.globl _RADIOFDATAADDR1
                                    311 	.globl _RADIOFDATAADDR0
                                    312 	.globl _OSCRUN
                                    313 	.globl _OSCREADY
                                    314 	.globl _OSCFORCERUN
                                    315 	.globl _OSCCALIB
                                    316 	.globl _MISCCTRL
                                    317 	.globl _LPXOSCGM
                                    318 	.globl _LPOSCREF
                                    319 	.globl _LPOSCREF1
                                    320 	.globl _LPOSCREF0
                                    321 	.globl _LPOSCPER
                                    322 	.globl _LPOSCPER1
                                    323 	.globl _LPOSCPER0
                                    324 	.globl _LPOSCKFILT
                                    325 	.globl _LPOSCKFILT1
                                    326 	.globl _LPOSCKFILT0
                                    327 	.globl _LPOSCFREQ
                                    328 	.globl _LPOSCFREQ1
                                    329 	.globl _LPOSCFREQ0
                                    330 	.globl _LPOSCCONFIG
                                    331 	.globl _PINSEL
                                    332 	.globl _PINCHGC
                                    333 	.globl _PINCHGB
                                    334 	.globl _PINCHGA
                                    335 	.globl _PALTRADIO
                                    336 	.globl _PALTC
                                    337 	.globl _PALTB
                                    338 	.globl _PALTA
                                    339 	.globl _INTCHGC
                                    340 	.globl _INTCHGB
                                    341 	.globl _INTCHGA
                                    342 	.globl _EXTIRQ
                                    343 	.globl _GPIOENABLE
                                    344 	.globl _ANALOGA
                                    345 	.globl _FRCOSCREF
                                    346 	.globl _FRCOSCREF1
                                    347 	.globl _FRCOSCREF0
                                    348 	.globl _FRCOSCPER
                                    349 	.globl _FRCOSCPER1
                                    350 	.globl _FRCOSCPER0
                                    351 	.globl _FRCOSCKFILT
                                    352 	.globl _FRCOSCKFILT1
                                    353 	.globl _FRCOSCKFILT0
                                    354 	.globl _FRCOSCFREQ
                                    355 	.globl _FRCOSCFREQ1
                                    356 	.globl _FRCOSCFREQ0
                                    357 	.globl _FRCOSCCTRL
                                    358 	.globl _FRCOSCCONFIG
                                    359 	.globl _DMA1CONFIG
                                    360 	.globl _DMA1ADDR
                                    361 	.globl _DMA1ADDR1
                                    362 	.globl _DMA1ADDR0
                                    363 	.globl _DMA0CONFIG
                                    364 	.globl _DMA0ADDR
                                    365 	.globl _DMA0ADDR1
                                    366 	.globl _DMA0ADDR0
                                    367 	.globl _ADCTUNE2
                                    368 	.globl _ADCTUNE1
                                    369 	.globl _ADCTUNE0
                                    370 	.globl _ADCCH3VAL
                                    371 	.globl _ADCCH3VAL1
                                    372 	.globl _ADCCH3VAL0
                                    373 	.globl _ADCCH2VAL
                                    374 	.globl _ADCCH2VAL1
                                    375 	.globl _ADCCH2VAL0
                                    376 	.globl _ADCCH1VAL
                                    377 	.globl _ADCCH1VAL1
                                    378 	.globl _ADCCH1VAL0
                                    379 	.globl _ADCCH0VAL
                                    380 	.globl _ADCCH0VAL1
                                    381 	.globl _ADCCH0VAL0
                                    382 	.globl _display_timing
                                    383 	.globl _per_test_counter_previous
                                    384 	.globl _per_test_counter
                                    385 	.globl _display_received_packet
                                    386 	.globl _dbglink_received_packet
                                    387 ;--------------------------------------------------------
                                    388 ; special function registers
                                    389 ;--------------------------------------------------------
                                    390 	.area RSEG    (ABS,DATA)
      000000                        391 	.org 0x0000
                           0000E0   392 G$ACC$0$0 == 0x00e0
                           0000E0   393 _ACC	=	0x00e0
                           0000F0   394 G$B$0$0 == 0x00f0
                           0000F0   395 _B	=	0x00f0
                           000083   396 G$DPH$0$0 == 0x0083
                           000083   397 _DPH	=	0x0083
                           000085   398 G$DPH1$0$0 == 0x0085
                           000085   399 _DPH1	=	0x0085
                           000082   400 G$DPL$0$0 == 0x0082
                           000082   401 _DPL	=	0x0082
                           000084   402 G$DPL1$0$0 == 0x0084
                           000084   403 _DPL1	=	0x0084
                           008382   404 G$DPTR0$0$0 == 0x8382
                           008382   405 _DPTR0	=	0x8382
                           008584   406 G$DPTR1$0$0 == 0x8584
                           008584   407 _DPTR1	=	0x8584
                           000086   408 G$DPS$0$0 == 0x0086
                           000086   409 _DPS	=	0x0086
                           0000A0   410 G$E2IE$0$0 == 0x00a0
                           0000A0   411 _E2IE	=	0x00a0
                           0000C0   412 G$E2IP$0$0 == 0x00c0
                           0000C0   413 _E2IP	=	0x00c0
                           000098   414 G$EIE$0$0 == 0x0098
                           000098   415 _EIE	=	0x0098
                           0000B0   416 G$EIP$0$0 == 0x00b0
                           0000B0   417 _EIP	=	0x00b0
                           0000A8   418 G$IE$0$0 == 0x00a8
                           0000A8   419 _IE	=	0x00a8
                           0000B8   420 G$IP$0$0 == 0x00b8
                           0000B8   421 _IP	=	0x00b8
                           000087   422 G$PCON$0$0 == 0x0087
                           000087   423 _PCON	=	0x0087
                           0000D0   424 G$PSW$0$0 == 0x00d0
                           0000D0   425 _PSW	=	0x00d0
                           000081   426 G$SP$0$0 == 0x0081
                           000081   427 _SP	=	0x0081
                           0000D9   428 G$XPAGE$0$0 == 0x00d9
                           0000D9   429 _XPAGE	=	0x00d9
                           0000D9   430 G$_XPAGE$0$0 == 0x00d9
                           0000D9   431 __XPAGE	=	0x00d9
                           0000CA   432 G$ADCCH0CONFIG$0$0 == 0x00ca
                           0000CA   433 _ADCCH0CONFIG	=	0x00ca
                           0000CB   434 G$ADCCH1CONFIG$0$0 == 0x00cb
                           0000CB   435 _ADCCH1CONFIG	=	0x00cb
                           0000D2   436 G$ADCCH2CONFIG$0$0 == 0x00d2
                           0000D2   437 _ADCCH2CONFIG	=	0x00d2
                           0000D3   438 G$ADCCH3CONFIG$0$0 == 0x00d3
                           0000D3   439 _ADCCH3CONFIG	=	0x00d3
                           0000D1   440 G$ADCCLKSRC$0$0 == 0x00d1
                           0000D1   441 _ADCCLKSRC	=	0x00d1
                           0000C9   442 G$ADCCONV$0$0 == 0x00c9
                           0000C9   443 _ADCCONV	=	0x00c9
                           0000E1   444 G$ANALOGCOMP$0$0 == 0x00e1
                           0000E1   445 _ANALOGCOMP	=	0x00e1
                           0000C6   446 G$CLKCON$0$0 == 0x00c6
                           0000C6   447 _CLKCON	=	0x00c6
                           0000C7   448 G$CLKSTAT$0$0 == 0x00c7
                           0000C7   449 _CLKSTAT	=	0x00c7
                           000097   450 G$CODECONFIG$0$0 == 0x0097
                           000097   451 _CODECONFIG	=	0x0097
                           0000E3   452 G$DBGLNKBUF$0$0 == 0x00e3
                           0000E3   453 _DBGLNKBUF	=	0x00e3
                           0000E2   454 G$DBGLNKSTAT$0$0 == 0x00e2
                           0000E2   455 _DBGLNKSTAT	=	0x00e2
                           000089   456 G$DIRA$0$0 == 0x0089
                           000089   457 _DIRA	=	0x0089
                           00008A   458 G$DIRB$0$0 == 0x008a
                           00008A   459 _DIRB	=	0x008a
                           00008B   460 G$DIRC$0$0 == 0x008b
                           00008B   461 _DIRC	=	0x008b
                           00008E   462 G$DIRR$0$0 == 0x008e
                           00008E   463 _DIRR	=	0x008e
                           0000C8   464 G$PINA$0$0 == 0x00c8
                           0000C8   465 _PINA	=	0x00c8
                           0000E8   466 G$PINB$0$0 == 0x00e8
                           0000E8   467 _PINB	=	0x00e8
                           0000F8   468 G$PINC$0$0 == 0x00f8
                           0000F8   469 _PINC	=	0x00f8
                           00008D   470 G$PINR$0$0 == 0x008d
                           00008D   471 _PINR	=	0x008d
                           000080   472 G$PORTA$0$0 == 0x0080
                           000080   473 _PORTA	=	0x0080
                           000088   474 G$PORTB$0$0 == 0x0088
                           000088   475 _PORTB	=	0x0088
                           000090   476 G$PORTC$0$0 == 0x0090
                           000090   477 _PORTC	=	0x0090
                           00008C   478 G$PORTR$0$0 == 0x008c
                           00008C   479 _PORTR	=	0x008c
                           0000CE   480 G$IC0CAPT0$0$0 == 0x00ce
                           0000CE   481 _IC0CAPT0	=	0x00ce
                           0000CF   482 G$IC0CAPT1$0$0 == 0x00cf
                           0000CF   483 _IC0CAPT1	=	0x00cf
                           00CFCE   484 G$IC0CAPT$0$0 == 0xcfce
                           00CFCE   485 _IC0CAPT	=	0xcfce
                           0000CC   486 G$IC0MODE$0$0 == 0x00cc
                           0000CC   487 _IC0MODE	=	0x00cc
                           0000CD   488 G$IC0STATUS$0$0 == 0x00cd
                           0000CD   489 _IC0STATUS	=	0x00cd
                           0000D6   490 G$IC1CAPT0$0$0 == 0x00d6
                           0000D6   491 _IC1CAPT0	=	0x00d6
                           0000D7   492 G$IC1CAPT1$0$0 == 0x00d7
                           0000D7   493 _IC1CAPT1	=	0x00d7
                           00D7D6   494 G$IC1CAPT$0$0 == 0xd7d6
                           00D7D6   495 _IC1CAPT	=	0xd7d6
                           0000D4   496 G$IC1MODE$0$0 == 0x00d4
                           0000D4   497 _IC1MODE	=	0x00d4
                           0000D5   498 G$IC1STATUS$0$0 == 0x00d5
                           0000D5   499 _IC1STATUS	=	0x00d5
                           000092   500 G$NVADDR0$0$0 == 0x0092
                           000092   501 _NVADDR0	=	0x0092
                           000093   502 G$NVADDR1$0$0 == 0x0093
                           000093   503 _NVADDR1	=	0x0093
                           009392   504 G$NVADDR$0$0 == 0x9392
                           009392   505 _NVADDR	=	0x9392
                           000094   506 G$NVDATA0$0$0 == 0x0094
                           000094   507 _NVDATA0	=	0x0094
                           000095   508 G$NVDATA1$0$0 == 0x0095
                           000095   509 _NVDATA1	=	0x0095
                           009594   510 G$NVDATA$0$0 == 0x9594
                           009594   511 _NVDATA	=	0x9594
                           000096   512 G$NVKEY$0$0 == 0x0096
                           000096   513 _NVKEY	=	0x0096
                           000091   514 G$NVSTATUS$0$0 == 0x0091
                           000091   515 _NVSTATUS	=	0x0091
                           0000BC   516 G$OC0COMP0$0$0 == 0x00bc
                           0000BC   517 _OC0COMP0	=	0x00bc
                           0000BD   518 G$OC0COMP1$0$0 == 0x00bd
                           0000BD   519 _OC0COMP1	=	0x00bd
                           00BDBC   520 G$OC0COMP$0$0 == 0xbdbc
                           00BDBC   521 _OC0COMP	=	0xbdbc
                           0000B9   522 G$OC0MODE$0$0 == 0x00b9
                           0000B9   523 _OC0MODE	=	0x00b9
                           0000BA   524 G$OC0PIN$0$0 == 0x00ba
                           0000BA   525 _OC0PIN	=	0x00ba
                           0000BB   526 G$OC0STATUS$0$0 == 0x00bb
                           0000BB   527 _OC0STATUS	=	0x00bb
                           0000C4   528 G$OC1COMP0$0$0 == 0x00c4
                           0000C4   529 _OC1COMP0	=	0x00c4
                           0000C5   530 G$OC1COMP1$0$0 == 0x00c5
                           0000C5   531 _OC1COMP1	=	0x00c5
                           00C5C4   532 G$OC1COMP$0$0 == 0xc5c4
                           00C5C4   533 _OC1COMP	=	0xc5c4
                           0000C1   534 G$OC1MODE$0$0 == 0x00c1
                           0000C1   535 _OC1MODE	=	0x00c1
                           0000C2   536 G$OC1PIN$0$0 == 0x00c2
                           0000C2   537 _OC1PIN	=	0x00c2
                           0000C3   538 G$OC1STATUS$0$0 == 0x00c3
                           0000C3   539 _OC1STATUS	=	0x00c3
                           0000B1   540 G$RADIOACC$0$0 == 0x00b1
                           0000B1   541 _RADIOACC	=	0x00b1
                           0000B3   542 G$RADIOADDR0$0$0 == 0x00b3
                           0000B3   543 _RADIOADDR0	=	0x00b3
                           0000B2   544 G$RADIOADDR1$0$0 == 0x00b2
                           0000B2   545 _RADIOADDR1	=	0x00b2
                           00B2B3   546 G$RADIOADDR$0$0 == 0xb2b3
                           00B2B3   547 _RADIOADDR	=	0xb2b3
                           0000B7   548 G$RADIODATA0$0$0 == 0x00b7
                           0000B7   549 _RADIODATA0	=	0x00b7
                           0000B6   550 G$RADIODATA1$0$0 == 0x00b6
                           0000B6   551 _RADIODATA1	=	0x00b6
                           0000B5   552 G$RADIODATA2$0$0 == 0x00b5
                           0000B5   553 _RADIODATA2	=	0x00b5
                           0000B4   554 G$RADIODATA3$0$0 == 0x00b4
                           0000B4   555 _RADIODATA3	=	0x00b4
                           B4B5B6B7   556 G$RADIODATA$0$0 == 0xb4b5b6b7
                           B4B5B6B7   557 _RADIODATA	=	0xb4b5b6b7
                           0000BE   558 G$RADIOSTAT0$0$0 == 0x00be
                           0000BE   559 _RADIOSTAT0	=	0x00be
                           0000BF   560 G$RADIOSTAT1$0$0 == 0x00bf
                           0000BF   561 _RADIOSTAT1	=	0x00bf
                           00BFBE   562 G$RADIOSTAT$0$0 == 0xbfbe
                           00BFBE   563 _RADIOSTAT	=	0xbfbe
                           0000DF   564 G$SPCLKSRC$0$0 == 0x00df
                           0000DF   565 _SPCLKSRC	=	0x00df
                           0000DC   566 G$SPMODE$0$0 == 0x00dc
                           0000DC   567 _SPMODE	=	0x00dc
                           0000DE   568 G$SPSHREG$0$0 == 0x00de
                           0000DE   569 _SPSHREG	=	0x00de
                           0000DD   570 G$SPSTATUS$0$0 == 0x00dd
                           0000DD   571 _SPSTATUS	=	0x00dd
                           00009A   572 G$T0CLKSRC$0$0 == 0x009a
                           00009A   573 _T0CLKSRC	=	0x009a
                           00009C   574 G$T0CNT0$0$0 == 0x009c
                           00009C   575 _T0CNT0	=	0x009c
                           00009D   576 G$T0CNT1$0$0 == 0x009d
                           00009D   577 _T0CNT1	=	0x009d
                           009D9C   578 G$T0CNT$0$0 == 0x9d9c
                           009D9C   579 _T0CNT	=	0x9d9c
                           000099   580 G$T0MODE$0$0 == 0x0099
                           000099   581 _T0MODE	=	0x0099
                           00009E   582 G$T0PERIOD0$0$0 == 0x009e
                           00009E   583 _T0PERIOD0	=	0x009e
                           00009F   584 G$T0PERIOD1$0$0 == 0x009f
                           00009F   585 _T0PERIOD1	=	0x009f
                           009F9E   586 G$T0PERIOD$0$0 == 0x9f9e
                           009F9E   587 _T0PERIOD	=	0x9f9e
                           00009B   588 G$T0STATUS$0$0 == 0x009b
                           00009B   589 _T0STATUS	=	0x009b
                           0000A2   590 G$T1CLKSRC$0$0 == 0x00a2
                           0000A2   591 _T1CLKSRC	=	0x00a2
                           0000A4   592 G$T1CNT0$0$0 == 0x00a4
                           0000A4   593 _T1CNT0	=	0x00a4
                           0000A5   594 G$T1CNT1$0$0 == 0x00a5
                           0000A5   595 _T1CNT1	=	0x00a5
                           00A5A4   596 G$T1CNT$0$0 == 0xa5a4
                           00A5A4   597 _T1CNT	=	0xa5a4
                           0000A1   598 G$T1MODE$0$0 == 0x00a1
                           0000A1   599 _T1MODE	=	0x00a1
                           0000A6   600 G$T1PERIOD0$0$0 == 0x00a6
                           0000A6   601 _T1PERIOD0	=	0x00a6
                           0000A7   602 G$T1PERIOD1$0$0 == 0x00a7
                           0000A7   603 _T1PERIOD1	=	0x00a7
                           00A7A6   604 G$T1PERIOD$0$0 == 0xa7a6
                           00A7A6   605 _T1PERIOD	=	0xa7a6
                           0000A3   606 G$T1STATUS$0$0 == 0x00a3
                           0000A3   607 _T1STATUS	=	0x00a3
                           0000AA   608 G$T2CLKSRC$0$0 == 0x00aa
                           0000AA   609 _T2CLKSRC	=	0x00aa
                           0000AC   610 G$T2CNT0$0$0 == 0x00ac
                           0000AC   611 _T2CNT0	=	0x00ac
                           0000AD   612 G$T2CNT1$0$0 == 0x00ad
                           0000AD   613 _T2CNT1	=	0x00ad
                           00ADAC   614 G$T2CNT$0$0 == 0xadac
                           00ADAC   615 _T2CNT	=	0xadac
                           0000A9   616 G$T2MODE$0$0 == 0x00a9
                           0000A9   617 _T2MODE	=	0x00a9
                           0000AE   618 G$T2PERIOD0$0$0 == 0x00ae
                           0000AE   619 _T2PERIOD0	=	0x00ae
                           0000AF   620 G$T2PERIOD1$0$0 == 0x00af
                           0000AF   621 _T2PERIOD1	=	0x00af
                           00AFAE   622 G$T2PERIOD$0$0 == 0xafae
                           00AFAE   623 _T2PERIOD	=	0xafae
                           0000AB   624 G$T2STATUS$0$0 == 0x00ab
                           0000AB   625 _T2STATUS	=	0x00ab
                           0000E4   626 G$U0CTRL$0$0 == 0x00e4
                           0000E4   627 _U0CTRL	=	0x00e4
                           0000E7   628 G$U0MODE$0$0 == 0x00e7
                           0000E7   629 _U0MODE	=	0x00e7
                           0000E6   630 G$U0SHREG$0$0 == 0x00e6
                           0000E6   631 _U0SHREG	=	0x00e6
                           0000E5   632 G$U0STATUS$0$0 == 0x00e5
                           0000E5   633 _U0STATUS	=	0x00e5
                           0000EC   634 G$U1CTRL$0$0 == 0x00ec
                           0000EC   635 _U1CTRL	=	0x00ec
                           0000EF   636 G$U1MODE$0$0 == 0x00ef
                           0000EF   637 _U1MODE	=	0x00ef
                           0000EE   638 G$U1SHREG$0$0 == 0x00ee
                           0000EE   639 _U1SHREG	=	0x00ee
                           0000ED   640 G$U1STATUS$0$0 == 0x00ed
                           0000ED   641 _U1STATUS	=	0x00ed
                           0000DA   642 G$WDTCFG$0$0 == 0x00da
                           0000DA   643 _WDTCFG	=	0x00da
                           0000DB   644 G$WDTRESET$0$0 == 0x00db
                           0000DB   645 _WDTRESET	=	0x00db
                           0000F1   646 G$WTCFGA$0$0 == 0x00f1
                           0000F1   647 _WTCFGA	=	0x00f1
                           0000F9   648 G$WTCFGB$0$0 == 0x00f9
                           0000F9   649 _WTCFGB	=	0x00f9
                           0000F2   650 G$WTCNTA0$0$0 == 0x00f2
                           0000F2   651 _WTCNTA0	=	0x00f2
                           0000F3   652 G$WTCNTA1$0$0 == 0x00f3
                           0000F3   653 _WTCNTA1	=	0x00f3
                           00F3F2   654 G$WTCNTA$0$0 == 0xf3f2
                           00F3F2   655 _WTCNTA	=	0xf3f2
                           0000FA   656 G$WTCNTB0$0$0 == 0x00fa
                           0000FA   657 _WTCNTB0	=	0x00fa
                           0000FB   658 G$WTCNTB1$0$0 == 0x00fb
                           0000FB   659 _WTCNTB1	=	0x00fb
                           00FBFA   660 G$WTCNTB$0$0 == 0xfbfa
                           00FBFA   661 _WTCNTB	=	0xfbfa
                           0000EB   662 G$WTCNTR1$0$0 == 0x00eb
                           0000EB   663 _WTCNTR1	=	0x00eb
                           0000F4   664 G$WTEVTA0$0$0 == 0x00f4
                           0000F4   665 _WTEVTA0	=	0x00f4
                           0000F5   666 G$WTEVTA1$0$0 == 0x00f5
                           0000F5   667 _WTEVTA1	=	0x00f5
                           00F5F4   668 G$WTEVTA$0$0 == 0xf5f4
                           00F5F4   669 _WTEVTA	=	0xf5f4
                           0000F6   670 G$WTEVTB0$0$0 == 0x00f6
                           0000F6   671 _WTEVTB0	=	0x00f6
                           0000F7   672 G$WTEVTB1$0$0 == 0x00f7
                           0000F7   673 _WTEVTB1	=	0x00f7
                           00F7F6   674 G$WTEVTB$0$0 == 0xf7f6
                           00F7F6   675 _WTEVTB	=	0xf7f6
                           0000FC   676 G$WTEVTC0$0$0 == 0x00fc
                           0000FC   677 _WTEVTC0	=	0x00fc
                           0000FD   678 G$WTEVTC1$0$0 == 0x00fd
                           0000FD   679 _WTEVTC1	=	0x00fd
                           00FDFC   680 G$WTEVTC$0$0 == 0xfdfc
                           00FDFC   681 _WTEVTC	=	0xfdfc
                           0000FE   682 G$WTEVTD0$0$0 == 0x00fe
                           0000FE   683 _WTEVTD0	=	0x00fe
                           0000FF   684 G$WTEVTD1$0$0 == 0x00ff
                           0000FF   685 _WTEVTD1	=	0x00ff
                           00FFFE   686 G$WTEVTD$0$0 == 0xfffe
                           00FFFE   687 _WTEVTD	=	0xfffe
                           0000E9   688 G$WTIRQEN$0$0 == 0x00e9
                           0000E9   689 _WTIRQEN	=	0x00e9
                           0000EA   690 G$WTSTAT$0$0 == 0x00ea
                           0000EA   691 _WTSTAT	=	0x00ea
                                    692 ;--------------------------------------------------------
                                    693 ; special function bits
                                    694 ;--------------------------------------------------------
                                    695 	.area RSEG    (ABS,DATA)
      000000                        696 	.org 0x0000
                           0000E0   697 G$ACC_0$0$0 == 0x00e0
                           0000E0   698 _ACC_0	=	0x00e0
                           0000E1   699 G$ACC_1$0$0 == 0x00e1
                           0000E1   700 _ACC_1	=	0x00e1
                           0000E2   701 G$ACC_2$0$0 == 0x00e2
                           0000E2   702 _ACC_2	=	0x00e2
                           0000E3   703 G$ACC_3$0$0 == 0x00e3
                           0000E3   704 _ACC_3	=	0x00e3
                           0000E4   705 G$ACC_4$0$0 == 0x00e4
                           0000E4   706 _ACC_4	=	0x00e4
                           0000E5   707 G$ACC_5$0$0 == 0x00e5
                           0000E5   708 _ACC_5	=	0x00e5
                           0000E6   709 G$ACC_6$0$0 == 0x00e6
                           0000E6   710 _ACC_6	=	0x00e6
                           0000E7   711 G$ACC_7$0$0 == 0x00e7
                           0000E7   712 _ACC_7	=	0x00e7
                           0000F0   713 G$B_0$0$0 == 0x00f0
                           0000F0   714 _B_0	=	0x00f0
                           0000F1   715 G$B_1$0$0 == 0x00f1
                           0000F1   716 _B_1	=	0x00f1
                           0000F2   717 G$B_2$0$0 == 0x00f2
                           0000F2   718 _B_2	=	0x00f2
                           0000F3   719 G$B_3$0$0 == 0x00f3
                           0000F3   720 _B_3	=	0x00f3
                           0000F4   721 G$B_4$0$0 == 0x00f4
                           0000F4   722 _B_4	=	0x00f4
                           0000F5   723 G$B_5$0$0 == 0x00f5
                           0000F5   724 _B_5	=	0x00f5
                           0000F6   725 G$B_6$0$0 == 0x00f6
                           0000F6   726 _B_6	=	0x00f6
                           0000F7   727 G$B_7$0$0 == 0x00f7
                           0000F7   728 _B_7	=	0x00f7
                           0000A0   729 G$E2IE_0$0$0 == 0x00a0
                           0000A0   730 _E2IE_0	=	0x00a0
                           0000A1   731 G$E2IE_1$0$0 == 0x00a1
                           0000A1   732 _E2IE_1	=	0x00a1
                           0000A2   733 G$E2IE_2$0$0 == 0x00a2
                           0000A2   734 _E2IE_2	=	0x00a2
                           0000A3   735 G$E2IE_3$0$0 == 0x00a3
                           0000A3   736 _E2IE_3	=	0x00a3
                           0000A4   737 G$E2IE_4$0$0 == 0x00a4
                           0000A4   738 _E2IE_4	=	0x00a4
                           0000A5   739 G$E2IE_5$0$0 == 0x00a5
                           0000A5   740 _E2IE_5	=	0x00a5
                           0000A6   741 G$E2IE_6$0$0 == 0x00a6
                           0000A6   742 _E2IE_6	=	0x00a6
                           0000A7   743 G$E2IE_7$0$0 == 0x00a7
                           0000A7   744 _E2IE_7	=	0x00a7
                           0000C0   745 G$E2IP_0$0$0 == 0x00c0
                           0000C0   746 _E2IP_0	=	0x00c0
                           0000C1   747 G$E2IP_1$0$0 == 0x00c1
                           0000C1   748 _E2IP_1	=	0x00c1
                           0000C2   749 G$E2IP_2$0$0 == 0x00c2
                           0000C2   750 _E2IP_2	=	0x00c2
                           0000C3   751 G$E2IP_3$0$0 == 0x00c3
                           0000C3   752 _E2IP_3	=	0x00c3
                           0000C4   753 G$E2IP_4$0$0 == 0x00c4
                           0000C4   754 _E2IP_4	=	0x00c4
                           0000C5   755 G$E2IP_5$0$0 == 0x00c5
                           0000C5   756 _E2IP_5	=	0x00c5
                           0000C6   757 G$E2IP_6$0$0 == 0x00c6
                           0000C6   758 _E2IP_6	=	0x00c6
                           0000C7   759 G$E2IP_7$0$0 == 0x00c7
                           0000C7   760 _E2IP_7	=	0x00c7
                           000098   761 G$EIE_0$0$0 == 0x0098
                           000098   762 _EIE_0	=	0x0098
                           000099   763 G$EIE_1$0$0 == 0x0099
                           000099   764 _EIE_1	=	0x0099
                           00009A   765 G$EIE_2$0$0 == 0x009a
                           00009A   766 _EIE_2	=	0x009a
                           00009B   767 G$EIE_3$0$0 == 0x009b
                           00009B   768 _EIE_3	=	0x009b
                           00009C   769 G$EIE_4$0$0 == 0x009c
                           00009C   770 _EIE_4	=	0x009c
                           00009D   771 G$EIE_5$0$0 == 0x009d
                           00009D   772 _EIE_5	=	0x009d
                           00009E   773 G$EIE_6$0$0 == 0x009e
                           00009E   774 _EIE_6	=	0x009e
                           00009F   775 G$EIE_7$0$0 == 0x009f
                           00009F   776 _EIE_7	=	0x009f
                           0000B0   777 G$EIP_0$0$0 == 0x00b0
                           0000B0   778 _EIP_0	=	0x00b0
                           0000B1   779 G$EIP_1$0$0 == 0x00b1
                           0000B1   780 _EIP_1	=	0x00b1
                           0000B2   781 G$EIP_2$0$0 == 0x00b2
                           0000B2   782 _EIP_2	=	0x00b2
                           0000B3   783 G$EIP_3$0$0 == 0x00b3
                           0000B3   784 _EIP_3	=	0x00b3
                           0000B4   785 G$EIP_4$0$0 == 0x00b4
                           0000B4   786 _EIP_4	=	0x00b4
                           0000B5   787 G$EIP_5$0$0 == 0x00b5
                           0000B5   788 _EIP_5	=	0x00b5
                           0000B6   789 G$EIP_6$0$0 == 0x00b6
                           0000B6   790 _EIP_6	=	0x00b6
                           0000B7   791 G$EIP_7$0$0 == 0x00b7
                           0000B7   792 _EIP_7	=	0x00b7
                           0000A8   793 G$IE_0$0$0 == 0x00a8
                           0000A8   794 _IE_0	=	0x00a8
                           0000A9   795 G$IE_1$0$0 == 0x00a9
                           0000A9   796 _IE_1	=	0x00a9
                           0000AA   797 G$IE_2$0$0 == 0x00aa
                           0000AA   798 _IE_2	=	0x00aa
                           0000AB   799 G$IE_3$0$0 == 0x00ab
                           0000AB   800 _IE_3	=	0x00ab
                           0000AC   801 G$IE_4$0$0 == 0x00ac
                           0000AC   802 _IE_4	=	0x00ac
                           0000AD   803 G$IE_5$0$0 == 0x00ad
                           0000AD   804 _IE_5	=	0x00ad
                           0000AE   805 G$IE_6$0$0 == 0x00ae
                           0000AE   806 _IE_6	=	0x00ae
                           0000AF   807 G$IE_7$0$0 == 0x00af
                           0000AF   808 _IE_7	=	0x00af
                           0000AF   809 G$EA$0$0 == 0x00af
                           0000AF   810 _EA	=	0x00af
                           0000B8   811 G$IP_0$0$0 == 0x00b8
                           0000B8   812 _IP_0	=	0x00b8
                           0000B9   813 G$IP_1$0$0 == 0x00b9
                           0000B9   814 _IP_1	=	0x00b9
                           0000BA   815 G$IP_2$0$0 == 0x00ba
                           0000BA   816 _IP_2	=	0x00ba
                           0000BB   817 G$IP_3$0$0 == 0x00bb
                           0000BB   818 _IP_3	=	0x00bb
                           0000BC   819 G$IP_4$0$0 == 0x00bc
                           0000BC   820 _IP_4	=	0x00bc
                           0000BD   821 G$IP_5$0$0 == 0x00bd
                           0000BD   822 _IP_5	=	0x00bd
                           0000BE   823 G$IP_6$0$0 == 0x00be
                           0000BE   824 _IP_6	=	0x00be
                           0000BF   825 G$IP_7$0$0 == 0x00bf
                           0000BF   826 _IP_7	=	0x00bf
                           0000D0   827 G$P$0$0 == 0x00d0
                           0000D0   828 _P	=	0x00d0
                           0000D1   829 G$F1$0$0 == 0x00d1
                           0000D1   830 _F1	=	0x00d1
                           0000D2   831 G$OV$0$0 == 0x00d2
                           0000D2   832 _OV	=	0x00d2
                           0000D3   833 G$RS0$0$0 == 0x00d3
                           0000D3   834 _RS0	=	0x00d3
                           0000D4   835 G$RS1$0$0 == 0x00d4
                           0000D4   836 _RS1	=	0x00d4
                           0000D5   837 G$F0$0$0 == 0x00d5
                           0000D5   838 _F0	=	0x00d5
                           0000D6   839 G$AC$0$0 == 0x00d6
                           0000D6   840 _AC	=	0x00d6
                           0000D7   841 G$CY$0$0 == 0x00d7
                           0000D7   842 _CY	=	0x00d7
                           0000C8   843 G$PINA_0$0$0 == 0x00c8
                           0000C8   844 _PINA_0	=	0x00c8
                           0000C9   845 G$PINA_1$0$0 == 0x00c9
                           0000C9   846 _PINA_1	=	0x00c9
                           0000CA   847 G$PINA_2$0$0 == 0x00ca
                           0000CA   848 _PINA_2	=	0x00ca
                           0000CB   849 G$PINA_3$0$0 == 0x00cb
                           0000CB   850 _PINA_3	=	0x00cb
                           0000CC   851 G$PINA_4$0$0 == 0x00cc
                           0000CC   852 _PINA_4	=	0x00cc
                           0000CD   853 G$PINA_5$0$0 == 0x00cd
                           0000CD   854 _PINA_5	=	0x00cd
                           0000CE   855 G$PINA_6$0$0 == 0x00ce
                           0000CE   856 _PINA_6	=	0x00ce
                           0000CF   857 G$PINA_7$0$0 == 0x00cf
                           0000CF   858 _PINA_7	=	0x00cf
                           0000E8   859 G$PINB_0$0$0 == 0x00e8
                           0000E8   860 _PINB_0	=	0x00e8
                           0000E9   861 G$PINB_1$0$0 == 0x00e9
                           0000E9   862 _PINB_1	=	0x00e9
                           0000EA   863 G$PINB_2$0$0 == 0x00ea
                           0000EA   864 _PINB_2	=	0x00ea
                           0000EB   865 G$PINB_3$0$0 == 0x00eb
                           0000EB   866 _PINB_3	=	0x00eb
                           0000EC   867 G$PINB_4$0$0 == 0x00ec
                           0000EC   868 _PINB_4	=	0x00ec
                           0000ED   869 G$PINB_5$0$0 == 0x00ed
                           0000ED   870 _PINB_5	=	0x00ed
                           0000EE   871 G$PINB_6$0$0 == 0x00ee
                           0000EE   872 _PINB_6	=	0x00ee
                           0000EF   873 G$PINB_7$0$0 == 0x00ef
                           0000EF   874 _PINB_7	=	0x00ef
                           0000F8   875 G$PINC_0$0$0 == 0x00f8
                           0000F8   876 _PINC_0	=	0x00f8
                           0000F9   877 G$PINC_1$0$0 == 0x00f9
                           0000F9   878 _PINC_1	=	0x00f9
                           0000FA   879 G$PINC_2$0$0 == 0x00fa
                           0000FA   880 _PINC_2	=	0x00fa
                           0000FB   881 G$PINC_3$0$0 == 0x00fb
                           0000FB   882 _PINC_3	=	0x00fb
                           0000FC   883 G$PINC_4$0$0 == 0x00fc
                           0000FC   884 _PINC_4	=	0x00fc
                           0000FD   885 G$PINC_5$0$0 == 0x00fd
                           0000FD   886 _PINC_5	=	0x00fd
                           0000FE   887 G$PINC_6$0$0 == 0x00fe
                           0000FE   888 _PINC_6	=	0x00fe
                           0000FF   889 G$PINC_7$0$0 == 0x00ff
                           0000FF   890 _PINC_7	=	0x00ff
                           000080   891 G$PORTA_0$0$0 == 0x0080
                           000080   892 _PORTA_0	=	0x0080
                           000081   893 G$PORTA_1$0$0 == 0x0081
                           000081   894 _PORTA_1	=	0x0081
                           000082   895 G$PORTA_2$0$0 == 0x0082
                           000082   896 _PORTA_2	=	0x0082
                           000083   897 G$PORTA_3$0$0 == 0x0083
                           000083   898 _PORTA_3	=	0x0083
                           000084   899 G$PORTA_4$0$0 == 0x0084
                           000084   900 _PORTA_4	=	0x0084
                           000085   901 G$PORTA_5$0$0 == 0x0085
                           000085   902 _PORTA_5	=	0x0085
                           000086   903 G$PORTA_6$0$0 == 0x0086
                           000086   904 _PORTA_6	=	0x0086
                           000087   905 G$PORTA_7$0$0 == 0x0087
                           000087   906 _PORTA_7	=	0x0087
                           000088   907 G$PORTB_0$0$0 == 0x0088
                           000088   908 _PORTB_0	=	0x0088
                           000089   909 G$PORTB_1$0$0 == 0x0089
                           000089   910 _PORTB_1	=	0x0089
                           00008A   911 G$PORTB_2$0$0 == 0x008a
                           00008A   912 _PORTB_2	=	0x008a
                           00008B   913 G$PORTB_3$0$0 == 0x008b
                           00008B   914 _PORTB_3	=	0x008b
                           00008C   915 G$PORTB_4$0$0 == 0x008c
                           00008C   916 _PORTB_4	=	0x008c
                           00008D   917 G$PORTB_5$0$0 == 0x008d
                           00008D   918 _PORTB_5	=	0x008d
                           00008E   919 G$PORTB_6$0$0 == 0x008e
                           00008E   920 _PORTB_6	=	0x008e
                           00008F   921 G$PORTB_7$0$0 == 0x008f
                           00008F   922 _PORTB_7	=	0x008f
                           000090   923 G$PORTC_0$0$0 == 0x0090
                           000090   924 _PORTC_0	=	0x0090
                           000091   925 G$PORTC_1$0$0 == 0x0091
                           000091   926 _PORTC_1	=	0x0091
                           000092   927 G$PORTC_2$0$0 == 0x0092
                           000092   928 _PORTC_2	=	0x0092
                           000093   929 G$PORTC_3$0$0 == 0x0093
                           000093   930 _PORTC_3	=	0x0093
                           000094   931 G$PORTC_4$0$0 == 0x0094
                           000094   932 _PORTC_4	=	0x0094
                           000095   933 G$PORTC_5$0$0 == 0x0095
                           000095   934 _PORTC_5	=	0x0095
                           000096   935 G$PORTC_6$0$0 == 0x0096
                           000096   936 _PORTC_6	=	0x0096
                           000097   937 G$PORTC_7$0$0 == 0x0097
                           000097   938 _PORTC_7	=	0x0097
                                    939 ;--------------------------------------------------------
                                    940 ; overlayable register banks
                                    941 ;--------------------------------------------------------
                                    942 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        943 	.ds 8
                                    944 ;--------------------------------------------------------
                                    945 ; internal ram data
                                    946 ;--------------------------------------------------------
                                    947 	.area DSEG    (DATA)
                           000000   948 G$per_test_counter$0$0==.
      000008                        949 _per_test_counter::
      000008                        950 	.ds 2
                           000002   951 G$per_test_counter_previous$0$0==.
      00000A                        952 _per_test_counter_previous::
      00000A                        953 	.ds 2
                           000004   954 G$display_timing$0$0==.
      00000C                        955 _display_timing::
      00000C                        956 	.ds 1
                           000005   957 Fdisplay$dbglink_semaphore$0$0==.
      00000D                        958 _dbglink_semaphore:
      00000D                        959 	.ds 1
                                    960 ;--------------------------------------------------------
                                    961 ; overlayable items in internal ram 
                                    962 ;--------------------------------------------------------
                                    963 ;--------------------------------------------------------
                                    964 ; indirectly addressable internal ram data
                                    965 ;--------------------------------------------------------
                                    966 	.area ISEG    (DATA)
                                    967 ;--------------------------------------------------------
                                    968 ; absolute internal ram data
                                    969 ;--------------------------------------------------------
                                    970 	.area IABS    (ABS,DATA)
                                    971 	.area IABS    (ABS,DATA)
                                    972 ;--------------------------------------------------------
                                    973 ; bit data
                                    974 ;--------------------------------------------------------
                                    975 	.area BSEG    (BIT)
                                    976 ;--------------------------------------------------------
                                    977 ; paged external ram data
                                    978 ;--------------------------------------------------------
                                    979 	.area PSEG    (PAG,XDATA)
                                    980 ;--------------------------------------------------------
                                    981 ; external ram data
                                    982 ;--------------------------------------------------------
                                    983 	.area XSEG    (XDATA)
                           007020   984 G$ADCCH0VAL0$0$0 == 0x7020
                           007020   985 _ADCCH0VAL0	=	0x7020
                           007021   986 G$ADCCH0VAL1$0$0 == 0x7021
                           007021   987 _ADCCH0VAL1	=	0x7021
                           007020   988 G$ADCCH0VAL$0$0 == 0x7020
                           007020   989 _ADCCH0VAL	=	0x7020
                           007022   990 G$ADCCH1VAL0$0$0 == 0x7022
                           007022   991 _ADCCH1VAL0	=	0x7022
                           007023   992 G$ADCCH1VAL1$0$0 == 0x7023
                           007023   993 _ADCCH1VAL1	=	0x7023
                           007022   994 G$ADCCH1VAL$0$0 == 0x7022
                           007022   995 _ADCCH1VAL	=	0x7022
                           007024   996 G$ADCCH2VAL0$0$0 == 0x7024
                           007024   997 _ADCCH2VAL0	=	0x7024
                           007025   998 G$ADCCH2VAL1$0$0 == 0x7025
                           007025   999 _ADCCH2VAL1	=	0x7025
                           007024  1000 G$ADCCH2VAL$0$0 == 0x7024
                           007024  1001 _ADCCH2VAL	=	0x7024
                           007026  1002 G$ADCCH3VAL0$0$0 == 0x7026
                           007026  1003 _ADCCH3VAL0	=	0x7026
                           007027  1004 G$ADCCH3VAL1$0$0 == 0x7027
                           007027  1005 _ADCCH3VAL1	=	0x7027
                           007026  1006 G$ADCCH3VAL$0$0 == 0x7026
                           007026  1007 _ADCCH3VAL	=	0x7026
                           007028  1008 G$ADCTUNE0$0$0 == 0x7028
                           007028  1009 _ADCTUNE0	=	0x7028
                           007029  1010 G$ADCTUNE1$0$0 == 0x7029
                           007029  1011 _ADCTUNE1	=	0x7029
                           00702A  1012 G$ADCTUNE2$0$0 == 0x702a
                           00702A  1013 _ADCTUNE2	=	0x702a
                           007010  1014 G$DMA0ADDR0$0$0 == 0x7010
                           007010  1015 _DMA0ADDR0	=	0x7010
                           007011  1016 G$DMA0ADDR1$0$0 == 0x7011
                           007011  1017 _DMA0ADDR1	=	0x7011
                           007010  1018 G$DMA0ADDR$0$0 == 0x7010
                           007010  1019 _DMA0ADDR	=	0x7010
                           007014  1020 G$DMA0CONFIG$0$0 == 0x7014
                           007014  1021 _DMA0CONFIG	=	0x7014
                           007012  1022 G$DMA1ADDR0$0$0 == 0x7012
                           007012  1023 _DMA1ADDR0	=	0x7012
                           007013  1024 G$DMA1ADDR1$0$0 == 0x7013
                           007013  1025 _DMA1ADDR1	=	0x7013
                           007012  1026 G$DMA1ADDR$0$0 == 0x7012
                           007012  1027 _DMA1ADDR	=	0x7012
                           007015  1028 G$DMA1CONFIG$0$0 == 0x7015
                           007015  1029 _DMA1CONFIG	=	0x7015
                           007070  1030 G$FRCOSCCONFIG$0$0 == 0x7070
                           007070  1031 _FRCOSCCONFIG	=	0x7070
                           007071  1032 G$FRCOSCCTRL$0$0 == 0x7071
                           007071  1033 _FRCOSCCTRL	=	0x7071
                           007076  1034 G$FRCOSCFREQ0$0$0 == 0x7076
                           007076  1035 _FRCOSCFREQ0	=	0x7076
                           007077  1036 G$FRCOSCFREQ1$0$0 == 0x7077
                           007077  1037 _FRCOSCFREQ1	=	0x7077
                           007076  1038 G$FRCOSCFREQ$0$0 == 0x7076
                           007076  1039 _FRCOSCFREQ	=	0x7076
                           007072  1040 G$FRCOSCKFILT0$0$0 == 0x7072
                           007072  1041 _FRCOSCKFILT0	=	0x7072
                           007073  1042 G$FRCOSCKFILT1$0$0 == 0x7073
                           007073  1043 _FRCOSCKFILT1	=	0x7073
                           007072  1044 G$FRCOSCKFILT$0$0 == 0x7072
                           007072  1045 _FRCOSCKFILT	=	0x7072
                           007078  1046 G$FRCOSCPER0$0$0 == 0x7078
                           007078  1047 _FRCOSCPER0	=	0x7078
                           007079  1048 G$FRCOSCPER1$0$0 == 0x7079
                           007079  1049 _FRCOSCPER1	=	0x7079
                           007078  1050 G$FRCOSCPER$0$0 == 0x7078
                           007078  1051 _FRCOSCPER	=	0x7078
                           007074  1052 G$FRCOSCREF0$0$0 == 0x7074
                           007074  1053 _FRCOSCREF0	=	0x7074
                           007075  1054 G$FRCOSCREF1$0$0 == 0x7075
                           007075  1055 _FRCOSCREF1	=	0x7075
                           007074  1056 G$FRCOSCREF$0$0 == 0x7074
                           007074  1057 _FRCOSCREF	=	0x7074
                           007007  1058 G$ANALOGA$0$0 == 0x7007
                           007007  1059 _ANALOGA	=	0x7007
                           00700C  1060 G$GPIOENABLE$0$0 == 0x700c
                           00700C  1061 _GPIOENABLE	=	0x700c
                           007003  1062 G$EXTIRQ$0$0 == 0x7003
                           007003  1063 _EXTIRQ	=	0x7003
                           007000  1064 G$INTCHGA$0$0 == 0x7000
                           007000  1065 _INTCHGA	=	0x7000
                           007001  1066 G$INTCHGB$0$0 == 0x7001
                           007001  1067 _INTCHGB	=	0x7001
                           007002  1068 G$INTCHGC$0$0 == 0x7002
                           007002  1069 _INTCHGC	=	0x7002
                           007008  1070 G$PALTA$0$0 == 0x7008
                           007008  1071 _PALTA	=	0x7008
                           007009  1072 G$PALTB$0$0 == 0x7009
                           007009  1073 _PALTB	=	0x7009
                           00700A  1074 G$PALTC$0$0 == 0x700a
                           00700A  1075 _PALTC	=	0x700a
                           007046  1076 G$PALTRADIO$0$0 == 0x7046
                           007046  1077 _PALTRADIO	=	0x7046
                           007004  1078 G$PINCHGA$0$0 == 0x7004
                           007004  1079 _PINCHGA	=	0x7004
                           007005  1080 G$PINCHGB$0$0 == 0x7005
                           007005  1081 _PINCHGB	=	0x7005
                           007006  1082 G$PINCHGC$0$0 == 0x7006
                           007006  1083 _PINCHGC	=	0x7006
                           00700B  1084 G$PINSEL$0$0 == 0x700b
                           00700B  1085 _PINSEL	=	0x700b
                           007060  1086 G$LPOSCCONFIG$0$0 == 0x7060
                           007060  1087 _LPOSCCONFIG	=	0x7060
                           007066  1088 G$LPOSCFREQ0$0$0 == 0x7066
                           007066  1089 _LPOSCFREQ0	=	0x7066
                           007067  1090 G$LPOSCFREQ1$0$0 == 0x7067
                           007067  1091 _LPOSCFREQ1	=	0x7067
                           007066  1092 G$LPOSCFREQ$0$0 == 0x7066
                           007066  1093 _LPOSCFREQ	=	0x7066
                           007062  1094 G$LPOSCKFILT0$0$0 == 0x7062
                           007062  1095 _LPOSCKFILT0	=	0x7062
                           007063  1096 G$LPOSCKFILT1$0$0 == 0x7063
                           007063  1097 _LPOSCKFILT1	=	0x7063
                           007062  1098 G$LPOSCKFILT$0$0 == 0x7062
                           007062  1099 _LPOSCKFILT	=	0x7062
                           007068  1100 G$LPOSCPER0$0$0 == 0x7068
                           007068  1101 _LPOSCPER0	=	0x7068
                           007069  1102 G$LPOSCPER1$0$0 == 0x7069
                           007069  1103 _LPOSCPER1	=	0x7069
                           007068  1104 G$LPOSCPER$0$0 == 0x7068
                           007068  1105 _LPOSCPER	=	0x7068
                           007064  1106 G$LPOSCREF0$0$0 == 0x7064
                           007064  1107 _LPOSCREF0	=	0x7064
                           007065  1108 G$LPOSCREF1$0$0 == 0x7065
                           007065  1109 _LPOSCREF1	=	0x7065
                           007064  1110 G$LPOSCREF$0$0 == 0x7064
                           007064  1111 _LPOSCREF	=	0x7064
                           007054  1112 G$LPXOSCGM$0$0 == 0x7054
                           007054  1113 _LPXOSCGM	=	0x7054
                           007F01  1114 G$MISCCTRL$0$0 == 0x7f01
                           007F01  1115 _MISCCTRL	=	0x7f01
                           007053  1116 G$OSCCALIB$0$0 == 0x7053
                           007053  1117 _OSCCALIB	=	0x7053
                           007050  1118 G$OSCFORCERUN$0$0 == 0x7050
                           007050  1119 _OSCFORCERUN	=	0x7050
                           007052  1120 G$OSCREADY$0$0 == 0x7052
                           007052  1121 _OSCREADY	=	0x7052
                           007051  1122 G$OSCRUN$0$0 == 0x7051
                           007051  1123 _OSCRUN	=	0x7051
                           007040  1124 G$RADIOFDATAADDR0$0$0 == 0x7040
                           007040  1125 _RADIOFDATAADDR0	=	0x7040
                           007041  1126 G$RADIOFDATAADDR1$0$0 == 0x7041
                           007041  1127 _RADIOFDATAADDR1	=	0x7041
                           007040  1128 G$RADIOFDATAADDR$0$0 == 0x7040
                           007040  1129 _RADIOFDATAADDR	=	0x7040
                           007042  1130 G$RADIOFSTATADDR0$0$0 == 0x7042
                           007042  1131 _RADIOFSTATADDR0	=	0x7042
                           007043  1132 G$RADIOFSTATADDR1$0$0 == 0x7043
                           007043  1133 _RADIOFSTATADDR1	=	0x7043
                           007042  1134 G$RADIOFSTATADDR$0$0 == 0x7042
                           007042  1135 _RADIOFSTATADDR	=	0x7042
                           007044  1136 G$RADIOMUX$0$0 == 0x7044
                           007044  1137 _RADIOMUX	=	0x7044
                           007084  1138 G$SCRATCH0$0$0 == 0x7084
                           007084  1139 _SCRATCH0	=	0x7084
                           007085  1140 G$SCRATCH1$0$0 == 0x7085
                           007085  1141 _SCRATCH1	=	0x7085
                           007086  1142 G$SCRATCH2$0$0 == 0x7086
                           007086  1143 _SCRATCH2	=	0x7086
                           007087  1144 G$SCRATCH3$0$0 == 0x7087
                           007087  1145 _SCRATCH3	=	0x7087
                           007F00  1146 G$SILICONREV$0$0 == 0x7f00
                           007F00  1147 _SILICONREV	=	0x7f00
                           007F19  1148 G$XTALAMPL$0$0 == 0x7f19
                           007F19  1149 _XTALAMPL	=	0x7f19
                           007F18  1150 G$XTALOSC$0$0 == 0x7f18
                           007F18  1151 _XTALOSC	=	0x7f18
                           007F1A  1152 G$XTALREADY$0$0 == 0x7f1a
                           007F1A  1153 _XTALREADY	=	0x7f1a
                           00FC06  1154 Fdisplay$flash_deviceid$0$0 == 0xfc06
                           00FC06  1155 _flash_deviceid	=	0xfc06
                           00FC00  1156 Fdisplay$flash_calsector$0$0 == 0xfc00
                           00FC00  1157 _flash_calsector	=	0xfc00
                                   1158 ;--------------------------------------------------------
                                   1159 ; absolute external ram data
                                   1160 ;--------------------------------------------------------
                                   1161 	.area XABS    (ABS,XDATA)
                                   1162 ;--------------------------------------------------------
                                   1163 ; external initialized ram data
                                   1164 ;--------------------------------------------------------
                                   1165 	.area XISEG   (XDATA)
                                   1166 	.area HOME    (CODE)
                                   1167 	.area GSINIT0 (CODE)
                                   1168 	.area GSINIT1 (CODE)
                                   1169 	.area GSINIT2 (CODE)
                                   1170 	.area GSINIT3 (CODE)
                                   1171 	.area GSINIT4 (CODE)
                                   1172 	.area GSINIT5 (CODE)
                                   1173 	.area GSINIT  (CODE)
                                   1174 	.area GSFINAL (CODE)
                                   1175 	.area CSEG    (CODE)
                                   1176 ;--------------------------------------------------------
                                   1177 ; global & static initialisations
                                   1178 ;--------------------------------------------------------
                                   1179 	.area HOME    (CODE)
                                   1180 	.area GSINIT  (CODE)
                                   1181 	.area GSFINAL (CODE)
                                   1182 	.area GSINIT  (CODE)
                           000000  1183 	C$display.c$78$1$348 ==.
                                   1184 ;	..\COMMON\display.c:78: uint16_t __data per_test_counter = 0, per_test_counter_previous = 0;
      000384 E4               [12] 1185 	clr	a
      000385 F5 08            [12] 1186 	mov	_per_test_counter,a
      000387 F5 09            [12] 1187 	mov	(_per_test_counter + 1),a
                           000005  1188 	C$display.c$78$1$348 ==.
                                   1189 ;	..\COMMON\display.c:78: extern uint16_t __xdata pkts_received, pkts_missing;
      000389 F5 0A            [12] 1190 	mov	_per_test_counter_previous,a
      00038B F5 0B            [12] 1191 	mov	(_per_test_counter_previous + 1),a
                           000009  1192 	C$display.c$80$1$348 ==.
                                   1193 ;	..\COMMON\display.c:80: uint8_t __data display_timing = 2;
      00038D 75 0C 02         [24] 1194 	mov	_display_timing,#0x02
                           00000C  1195 	C$display.c$158$1$348 ==.
                                   1196 ;	..\COMMON\display.c:158: static volatile uint8_t dbglink_semaphore = 0;
                                   1197 ;	1-genFromRTrack replaced	mov	_dbglink_semaphore,#0x00
      000390 F5 0D            [12] 1198 	mov	_dbglink_semaphore,a
                                   1199 ;--------------------------------------------------------
                                   1200 ; Home
                                   1201 ;--------------------------------------------------------
                                   1202 	.area HOME    (CODE)
                                   1203 	.area HOME    (CODE)
                                   1204 ;--------------------------------------------------------
                                   1205 ; code
                                   1206 ;--------------------------------------------------------
                                   1207 	.area CSEG    (CODE)
                                   1208 ;------------------------------------------------------------
                                   1209 ;Allocation info for local variables in function 'display_received_packet'
                                   1210 ;------------------------------------------------------------
                                   1211 ;st                        Allocated to registers r6 r7 
                                   1212 ;retran                    Allocated to registers r5 
                                   1213 ;------------------------------------------------------------
                           000000  1214 	G$display_received_packet$0$0 ==.
                           000000  1215 	C$display.c$82$0$0 ==.
                                   1216 ;	..\COMMON\display.c:82: uint8_t display_received_packet(struct axradio_status __xdata *st)
                                   1217 ;	-----------------------------------------
                                   1218 ;	 function display_received_packet
                                   1219 ;	-----------------------------------------
      0009F3                       1220 _display_received_packet:
                           000007  1221 	ar7 = 0x07
                           000006  1222 	ar6 = 0x06
                           000005  1223 	ar5 = 0x05
                           000004  1224 	ar4 = 0x04
                           000003  1225 	ar3 = 0x03
                           000002  1226 	ar2 = 0x02
                           000001  1227 	ar1 = 0x01
                           000000  1228 	ar0 = 0x00
      0009F3 AE 82            [24] 1229 	mov	r6,dpl
      0009F5 AF 83            [24] 1230 	mov	r7,dph
                           000004  1231 	C$display.c$84$1$0 ==.
                                   1232 ;	..\COMMON\display.c:84: uint8_t retran = 0;
      0009F7 7D 00            [12] 1233 	mov	r5,#0x00
                           000006  1234 	C$display.c$96$1$337 ==.
                                   1235 ;	..\COMMON\display.c:96: if (display_timing & 0x02)
      0009F9 E5 0C            [12] 1236 	mov	a,_display_timing
      0009FB 30 E1 3A         [24] 1237 	jnb	acc.1,00102$
                           00000B  1238 	C$display.c$99$2$338 ==.
                                   1239 ;	..\COMMON\display.c:99: display_timing &= 0x01;
      0009FE 53 0C 01         [24] 1240 	anl	_display_timing,#0x01
                           00000E  1241 	C$display.c$100$2$338 ==.
                                   1242 ;	..\COMMON\display.c:100: display_setpos(0);
      000A01 75 82 00         [24] 1243 	mov	dpl,#0x00
      000A04 C0 07            [24] 1244 	push	ar7
      000A06 C0 06            [24] 1245 	push	ar6
      000A08 C0 05            [24] 1246 	push	ar5
      000A0A 12 0E C1         [24] 1247 	lcall	_com0_setpos
      000A0D D0 05            [24] 1248 	pop	ar5
      000A0F D0 06            [24] 1249 	pop	ar6
      000A11 D0 07            [24] 1250 	pop	ar7
                           000020  1251 	C$display.c$101$2$338 ==.
                                   1252 ;	..\COMMON\display.c:101: display_writestr(display_timing ? "P:      T:      \nL:      t:      \n" : "P:      O:      \nL:      R:      \n");
      000A13 E5 0C            [12] 1253 	mov	a,_display_timing
      000A15 60 06            [24] 1254 	jz	00113$
      000A17 7B FA            [12] 1255 	mov	r3,#___str_0
      000A19 7C 63            [12] 1256 	mov	r4,#(___str_0 >> 8)
      000A1B 80 04            [24] 1257 	sjmp	00114$
      000A1D                       1258 00113$:
      000A1D 7B 1D            [12] 1259 	mov	r3,#___str_1
      000A1F 7C 64            [12] 1260 	mov	r4,#(___str_1 >> 8)
      000A21                       1261 00114$:
      000A21 7A 80            [12] 1262 	mov	r2,#0x80
      000A23 8B 82            [24] 1263 	mov	dpl,r3
      000A25 8C 83            [24] 1264 	mov	dph,r4
      000A27 8A F0            [24] 1265 	mov	b,r2
      000A29 C0 07            [24] 1266 	push	ar7
      000A2B C0 06            [24] 1267 	push	ar6
      000A2D C0 05            [24] 1268 	push	ar5
      000A2F 12 0E DD         [24] 1269 	lcall	_com0_writestr
      000A32 D0 05            [24] 1270 	pop	ar5
      000A34 D0 06            [24] 1271 	pop	ar6
      000A36 D0 07            [24] 1272 	pop	ar7
      000A38                       1273 00102$:
                           000045  1274 	C$display.c$103$1$337 ==.
                                   1275 ;	..\COMMON\display.c:103: display_setpos(0x03);
      000A38 75 82 03         [24] 1276 	mov	dpl,#0x03
      000A3B C0 07            [24] 1277 	push	ar7
      000A3D C0 06            [24] 1278 	push	ar6
      000A3F C0 05            [24] 1279 	push	ar5
      000A41 12 0E C1         [24] 1280 	lcall	_com0_setpos
                           000051  1281 	C$display.c$104$1$337 ==.
                                   1282 ;	..\COMMON\display.c:104: display_writehex16(pkts_received, 4, WRNUM_PADZERO);
      000A44 90 04 7C         [24] 1283 	mov	dptr,#_pkts_received
      000A47 E0               [24] 1284 	movx	a,@dptr
      000A48 FB               [12] 1285 	mov	r3,a
      000A49 A3               [24] 1286 	inc	dptr
      000A4A E0               [24] 1287 	movx	a,@dptr
      000A4B FC               [12] 1288 	mov	r4,a
      000A4C 74 08            [12] 1289 	mov	a,#0x08
      000A4E C0 E0            [24] 1290 	push	acc
      000A50 03               [12] 1291 	rr	a
      000A51 C0 E0            [24] 1292 	push	acc
      000A53 8B 82            [24] 1293 	mov	dpl,r3
      000A55 8C 83            [24] 1294 	mov	dph,r4
      000A57 12 48 18         [24] 1295 	lcall	_uart0_writehex16
      000A5A 15 81            [12] 1296 	dec	sp
      000A5C 15 81            [12] 1297 	dec	sp
                           00006B  1298 	C$display.c$115$2$339 ==.
                                   1299 ;	..\COMMON\display.c:115: display_setpos(0x0A);
      000A5E 75 82 0A         [24] 1300 	mov	dpl,#0x0a
      000A61 12 0E C1         [24] 1301 	lcall	_com0_setpos
      000A64 D0 05            [24] 1302 	pop	ar5
      000A66 D0 06            [24] 1303 	pop	ar6
      000A68 D0 07            [24] 1304 	pop	ar7
                           000077  1305 	C$display.c$116$2$339 ==.
                                   1306 ;	..\COMMON\display.c:116: display_writenum16(axradio_conv_freq_tohz(st->u.rx.phy.offset), 6, WRNUM_SIGNED);
      000A6A 74 06            [12] 1307 	mov	a,#0x06
      000A6C 2E               [12] 1308 	add	a,r6
      000A6D FE               [12] 1309 	mov	r6,a
      000A6E E4               [12] 1310 	clr	a
      000A6F 3F               [12] 1311 	addc	a,r7
      000A70 FF               [12] 1312 	mov	r7,a
      000A71 8E 82            [24] 1313 	mov	dpl,r6
      000A73 8F 83            [24] 1314 	mov	dph,r7
      000A75 A3               [24] 1315 	inc	dptr
      000A76 A3               [24] 1316 	inc	dptr
      000A77 E0               [24] 1317 	movx	a,@dptr
      000A78 F9               [12] 1318 	mov	r1,a
      000A79 A3               [24] 1319 	inc	dptr
      000A7A E0               [24] 1320 	movx	a,@dptr
      000A7B FA               [12] 1321 	mov	r2,a
      000A7C A3               [24] 1322 	inc	dptr
      000A7D E0               [24] 1323 	movx	a,@dptr
      000A7E FB               [12] 1324 	mov	r3,a
      000A7F A3               [24] 1325 	inc	dptr
      000A80 E0               [24] 1326 	movx	a,@dptr
      000A81 89 82            [24] 1327 	mov	dpl,r1
      000A83 8A 83            [24] 1328 	mov	dph,r2
      000A85 8B F0            [24] 1329 	mov	b,r3
      000A87 C0 07            [24] 1330 	push	ar7
      000A89 C0 06            [24] 1331 	push	ar6
      000A8B C0 05            [24] 1332 	push	ar5
      000A8D 12 07 A1         [24] 1333 	lcall	_axradio_conv_freq_tohz
      000A90 74 01            [12] 1334 	mov	a,#0x01
      000A92 C0 E0            [24] 1335 	push	acc
      000A94 74 06            [12] 1336 	mov	a,#0x06
      000A96 C0 E0            [24] 1337 	push	acc
      000A98 12 50 ED         [24] 1338 	lcall	_uart0_writenum16
      000A9B 15 81            [12] 1339 	dec	sp
      000A9D 15 81            [12] 1340 	dec	sp
                           0000AC  1341 	C$display.c$129$2$340 ==.
                                   1342 ;	..\COMMON\display.c:129: display_setpos(0x4C);
      000A9F 75 82 4C         [24] 1343 	mov	dpl,#0x4c
      000AA2 12 0E C1         [24] 1344 	lcall	_com0_setpos
      000AA5 D0 05            [24] 1345 	pop	ar5
      000AA7 D0 06            [24] 1346 	pop	ar6
      000AA9 D0 07            [24] 1347 	pop	ar7
                           0000B8  1348 	C$display.c$130$2$340 ==.
                                   1349 ;	..\COMMON\display.c:130: display_writenum16(st->u.rx.phy.rssi, 4, WRNUM_SIGNED);
      000AAB 8E 82            [24] 1350 	mov	dpl,r6
      000AAD 8F 83            [24] 1351 	mov	dph,r7
      000AAF E0               [24] 1352 	movx	a,@dptr
      000AB0 FB               [12] 1353 	mov	r3,a
      000AB1 A3               [24] 1354 	inc	dptr
      000AB2 E0               [24] 1355 	movx	a,@dptr
      000AB3 FC               [12] 1356 	mov	r4,a
      000AB4 C0 07            [24] 1357 	push	ar7
      000AB6 C0 06            [24] 1358 	push	ar6
      000AB8 C0 05            [24] 1359 	push	ar5
      000ABA 74 01            [12] 1360 	mov	a,#0x01
      000ABC C0 E0            [24] 1361 	push	acc
      000ABE 74 04            [12] 1362 	mov	a,#0x04
      000AC0 C0 E0            [24] 1363 	push	acc
      000AC2 8B 82            [24] 1364 	mov	dpl,r3
      000AC4 8C 83            [24] 1365 	mov	dph,r4
      000AC6 12 50 ED         [24] 1366 	lcall	_uart0_writenum16
      000AC9 15 81            [12] 1367 	dec	sp
      000ACB 15 81            [12] 1368 	dec	sp
      000ACD D0 05            [24] 1369 	pop	ar5
      000ACF D0 06            [24] 1370 	pop	ar6
      000AD1 D0 07            [24] 1371 	pop	ar7
                           0000E0  1372 	C$display.c$133$1$337 ==.
                                   1373 ;	..\COMMON\display.c:133: if (framing_insert_counter)
      000AD3 90 63 F5         [24] 1374 	mov	dptr,#_framing_insert_counter
      000AD6 E4               [12] 1375 	clr	a
      000AD7 93               [24] 1376 	movc	a,@a+dptr
      000AD8 70 03            [24] 1377 	jnz	00132$
      000ADA 02 0B 83         [24] 1378 	ljmp	00109$
      000ADD                       1379 00132$:
                           0000EA  1380 	C$display.c$135$2$341 ==.
                                   1381 ;	..\COMMON\display.c:135: per_test_counter_previous = per_test_counter;
      000ADD 85 08 0A         [24] 1382 	mov	_per_test_counter_previous,_per_test_counter
      000AE0 85 09 0B         [24] 1383 	mov	(_per_test_counter_previous + 1),(_per_test_counter + 1)
                           0000F0  1384 	C$display.c$136$2$341 ==.
                                   1385 ;	..\COMMON\display.c:136: per_test_counter = ((st->u.rx.pktdata[framing_counter_pos+1])<<8) | st->u.rx.pktdata[framing_counter_pos];
      000AE3 74 16            [12] 1386 	mov	a,#0x16
      000AE5 2E               [12] 1387 	add	a,r6
      000AE6 F5 82            [12] 1388 	mov	dpl,a
      000AE8 E4               [12] 1389 	clr	a
      000AE9 3F               [12] 1390 	addc	a,r7
      000AEA F5 83            [12] 1391 	mov	dph,a
      000AEC E0               [24] 1392 	movx	a,@dptr
      000AED FE               [12] 1393 	mov	r6,a
      000AEE A3               [24] 1394 	inc	dptr
      000AEF E0               [24] 1395 	movx	a,@dptr
      000AF0 FF               [12] 1396 	mov	r7,a
      000AF1 90 63 F6         [24] 1397 	mov	dptr,#_framing_counter_pos
      000AF4 E4               [12] 1398 	clr	a
      000AF5 93               [24] 1399 	movc	a,@a+dptr
      000AF6 FC               [12] 1400 	mov	r4,a
      000AF7 FA               [12] 1401 	mov	r2,a
      000AF8 7B 00            [12] 1402 	mov	r3,#0x00
      000AFA 0A               [12] 1403 	inc	r2
      000AFB BA 00 01         [24] 1404 	cjne	r2,#0x00,00133$
      000AFE 0B               [12] 1405 	inc	r3
      000AFF                       1406 00133$:
      000AFF EA               [12] 1407 	mov	a,r2
      000B00 2E               [12] 1408 	add	a,r6
      000B01 F5 82            [12] 1409 	mov	dpl,a
      000B03 EB               [12] 1410 	mov	a,r3
      000B04 3F               [12] 1411 	addc	a,r7
      000B05 F5 83            [12] 1412 	mov	dph,a
      000B07 E0               [24] 1413 	movx	a,@dptr
      000B08 FA               [12] 1414 	mov	r2,a
      000B09 7B 00            [12] 1415 	mov	r3,#0x00
      000B0B EC               [12] 1416 	mov	a,r4
      000B0C 2E               [12] 1417 	add	a,r6
      000B0D F5 82            [12] 1418 	mov	dpl,a
      000B0F E4               [12] 1419 	clr	a
      000B10 3F               [12] 1420 	addc	a,r7
      000B11 F5 83            [12] 1421 	mov	dph,a
      000B13 E0               [24] 1422 	movx	a,@dptr
      000B14 7E 00            [12] 1423 	mov	r6,#0x00
      000B16 4B               [12] 1424 	orl	a,r3
      000B17 F5 08            [12] 1425 	mov	_per_test_counter,a
      000B19 EE               [12] 1426 	mov	a,r6
      000B1A 4A               [12] 1427 	orl	a,r2
      000B1B F5 09            [12] 1428 	mov	(_per_test_counter + 1),a
                           00012A  1429 	C$display.c$137$2$341 ==.
                                   1430 ;	..\COMMON\display.c:137: if (pkts_received != 1)
      000B1D 90 04 7C         [24] 1431 	mov	dptr,#_pkts_received
      000B20 E0               [24] 1432 	movx	a,@dptr
      000B21 FE               [12] 1433 	mov	r6,a
      000B22 A3               [24] 1434 	inc	dptr
      000B23 E0               [24] 1435 	movx	a,@dptr
      000B24 FF               [12] 1436 	mov	r7,a
      000B25 BE 01 05         [24] 1437 	cjne	r6,#0x01,00134$
      000B28 BF 00 02         [24] 1438 	cjne	r7,#0x00,00134$
      000B2B 80 30            [24] 1439 	sjmp	00107$
      000B2D                       1440 00134$:
                           00013A  1441 	C$display.c$139$3$342 ==.
                                   1442 ;	..\COMMON\display.c:139: if (per_test_counter == per_test_counter_previous)
      000B2D E5 0A            [12] 1443 	mov	a,_per_test_counter_previous
      000B2F B5 08 09         [24] 1444 	cjne	a,_per_test_counter,00104$
      000B32 E5 0B            [12] 1445 	mov	a,(_per_test_counter_previous + 1)
      000B34 B5 09 04         [24] 1446 	cjne	a,(_per_test_counter + 1),00104$
                           000144  1447 	C$display.c$140$3$342 ==.
                                   1448 ;	..\COMMON\display.c:140: retran = 1;
      000B37 7D 01            [12] 1449 	mov	r5,#0x01
      000B39 80 22            [24] 1450 	sjmp	00107$
      000B3B                       1451 00104$:
                           000148  1452 	C$display.c$142$3$342 ==.
                                   1453 ;	..\COMMON\display.c:142: pkts_missing += per_test_counter - per_test_counter_previous - 1;
      000B3B E5 08            [12] 1454 	mov	a,_per_test_counter
      000B3D C3               [12] 1455 	clr	c
      000B3E 95 0A            [12] 1456 	subb	a,_per_test_counter_previous
      000B40 FE               [12] 1457 	mov	r6,a
      000B41 E5 09            [12] 1458 	mov	a,(_per_test_counter + 1)
      000B43 95 0B            [12] 1459 	subb	a,(_per_test_counter_previous + 1)
      000B45 FF               [12] 1460 	mov	r7,a
      000B46 1E               [12] 1461 	dec	r6
      000B47 BE FF 01         [24] 1462 	cjne	r6,#0xff,00137$
      000B4A 1F               [12] 1463 	dec	r7
      000B4B                       1464 00137$:
      000B4B 90 04 7E         [24] 1465 	mov	dptr,#_pkts_missing
      000B4E E0               [24] 1466 	movx	a,@dptr
      000B4F FB               [12] 1467 	mov	r3,a
      000B50 A3               [24] 1468 	inc	dptr
      000B51 E0               [24] 1469 	movx	a,@dptr
      000B52 FC               [12] 1470 	mov	r4,a
      000B53 90 04 7E         [24] 1471 	mov	dptr,#_pkts_missing
      000B56 EE               [12] 1472 	mov	a,r6
      000B57 2B               [12] 1473 	add	a,r3
      000B58 F0               [24] 1474 	movx	@dptr,a
      000B59 EF               [12] 1475 	mov	a,r7
      000B5A 3C               [12] 1476 	addc	a,r4
      000B5B A3               [24] 1477 	inc	dptr
      000B5C F0               [24] 1478 	movx	@dptr,a
      000B5D                       1479 00107$:
                           00016A  1480 	C$display.c$146$2$341 ==.
                                   1481 ;	..\COMMON\display.c:146: display_setpos(0x43);
      000B5D 75 82 43         [24] 1482 	mov	dpl,#0x43
      000B60 C0 05            [24] 1483 	push	ar5
      000B62 12 0E C1         [24] 1484 	lcall	_com0_setpos
                           000172  1485 	C$display.c$147$2$341 ==.
                                   1486 ;	..\COMMON\display.c:147: display_writehex16(pkts_missing, 4, WRNUM_PADZERO);
      000B65 90 04 7E         [24] 1487 	mov	dptr,#_pkts_missing
      000B68 E0               [24] 1488 	movx	a,@dptr
      000B69 FE               [12] 1489 	mov	r6,a
      000B6A A3               [24] 1490 	inc	dptr
      000B6B E0               [24] 1491 	movx	a,@dptr
      000B6C FF               [12] 1492 	mov	r7,a
      000B6D 74 08            [12] 1493 	mov	a,#0x08
      000B6F C0 E0            [24] 1494 	push	acc
      000B71 03               [12] 1495 	rr	a
      000B72 C0 E0            [24] 1496 	push	acc
      000B74 8E 82            [24] 1497 	mov	dpl,r6
      000B76 8F 83            [24] 1498 	mov	dph,r7
      000B78 12 48 18         [24] 1499 	lcall	_uart0_writehex16
      000B7B 15 81            [12] 1500 	dec	sp
      000B7D 15 81            [12] 1501 	dec	sp
      000B7F D0 05            [24] 1502 	pop	ar5
      000B81 80 13            [24] 1503 	sjmp	00110$
      000B83                       1504 00109$:
                           000190  1505 	C$display.c$151$2$343 ==.
                                   1506 ;	..\COMMON\display.c:151: display_setpos(0x43);
      000B83 75 82 43         [24] 1507 	mov	dpl,#0x43
      000B86 C0 05            [24] 1508 	push	ar5
      000B88 12 0E C1         [24] 1509 	lcall	_com0_setpos
                           000198  1510 	C$display.c$152$2$343 ==.
                                   1511 ;	..\COMMON\display.c:152: display_writestr("?");
      000B8B 90 64 40         [24] 1512 	mov	dptr,#___str_2
      000B8E 75 F0 80         [24] 1513 	mov	b,#0x80
      000B91 12 0E DD         [24] 1514 	lcall	_com0_writestr
      000B94 D0 05            [24] 1515 	pop	ar5
      000B96                       1516 00110$:
                           0001A3  1517 	C$display.c$154$1$337 ==.
                                   1518 ;	..\COMMON\display.c:154: return retran;
      000B96 8D 82            [24] 1519 	mov	dpl,r5
                           0001A5  1520 	C$display.c$155$1$337 ==.
                           0001A5  1521 	XG$display_received_packet$0$0 ==.
      000B98 22               [24] 1522 	ret
                                   1523 ;------------------------------------------------------------
                                   1524 ;Allocation info for local variables in function 'wait_dbglink_free'
                                   1525 ;------------------------------------------------------------
                           0001A6  1526 	G$wait_dbglink_free$0$0 ==.
                           0001A6  1527 	C$display.c$160$1$337 ==.
                                   1528 ;	..\COMMON\display.c:160: /*static*/ void wait_dbglink_free(void)
                                   1529 ;	-----------------------------------------
                                   1530 ;	 function wait_dbglink_free
                                   1531 ;	-----------------------------------------
      000B99                       1532 _wait_dbglink_free:
      000B99                       1533 00104$:
                           0001A6  1534 	C$display.c$163$2$346 ==.
                                   1535 ;	..\COMMON\display.c:163: if (dbglink_txfree() >= 56)
      000B99 12 69 39         [24] 1536 	lcall	_dbglink_txfree
      000B9C AF 82            [24] 1537 	mov	r7,dpl
      000B9E BF 38 00         [24] 1538 	cjne	r7,#0x38,00114$
      000BA1                       1539 00114$:
      000BA1 50 0B            [24] 1540 	jnc	00106$
                           0001B0  1541 	C$display.c$165$2$346 ==.
                                   1542 ;	..\COMMON\display.c:165: wtimer_runcallbacks();
      000BA3 12 4F 50         [24] 1543 	lcall	_wtimer_runcallbacks
                           0001B3  1544 	C$display.c$166$2$346 ==.
                                   1545 ;	..\COMMON\display.c:166: wtimer_idle(WTFLAG_CANSTANDBY);
      000BA6 75 82 02         [24] 1546 	mov	dpl,#0x02
      000BA9 12 4E CC         [24] 1547 	lcall	_wtimer_idle
      000BAC 80 EB            [24] 1548 	sjmp	00104$
      000BAE                       1549 00106$:
                           0001BB  1550 	C$display.c$168$1$345 ==.
                           0001BB  1551 	XG$wait_dbglink_free$0$0 ==.
      000BAE 22               [24] 1552 	ret
                                   1553 ;------------------------------------------------------------
                                   1554 ;Allocation info for local variables in function 'dbglink_received_packet'
                                   1555 ;------------------------------------------------------------
                                   1556 ;st                        Allocated to registers r6 r7 
                                   1557 ;pktlen                    Allocated to registers r4 r5 
                                   1558 ;i                         Allocated to registers r2 r3 
                                   1559 ;pktdata                   Allocated to registers r6 r7 
                                   1560 ;------------------------------------------------------------
                           0001BC  1561 	G$dbglink_received_packet$0$0 ==.
                           0001BC  1562 	C$display.c$171$1$345 ==.
                                   1563 ;	..\COMMON\display.c:171: void dbglink_received_packet(struct axradio_status __xdata *st)
                                   1564 ;	-----------------------------------------
                                   1565 ;	 function dbglink_received_packet
                                   1566 ;	-----------------------------------------
      000BAF                       1567 _dbglink_received_packet:
      000BAF AE 82            [24] 1568 	mov	r6,dpl
      000BB1 AF 83            [24] 1569 	mov	r7,dph
                           0001C0  1570 	C$display.c$175$1$348 ==.
                                   1571 ;	..\COMMON\display.c:175: if (!(DBGLNKSTAT & 0x10))
      000BB3 E5 E2            [12] 1572 	mov	a,_DBGLNKSTAT
      000BB5 20 E4 03         [24] 1573 	jb	acc.4,00102$
                           0001C5  1574 	C$display.c$176$1$348 ==.
                                   1575 ;	..\COMMON\display.c:176: return;
      000BB8 02 0D CA         [24] 1576 	ljmp	00111$
      000BBB                       1577 00102$:
                           0001C8  1578 	C$display.c$177$1$348 ==.
                                   1579 ;	..\COMMON\display.c:177: ++dbglink_semaphore;
      000BBB 05 0D            [12] 1580 	inc	_dbglink_semaphore
                           0001CA  1581 	C$display.c$178$1$348 ==.
                                   1582 ;	..\COMMON\display.c:178: if (dbglink_semaphore != 1) {
      000BBD 74 01            [12] 1583 	mov	a,#0x01
      000BBF B5 0D 02         [24] 1584 	cjne	a,_dbglink_semaphore,00130$
      000BC2 80 05            [24] 1585 	sjmp	00104$
      000BC4                       1586 00130$:
                           0001D1  1587 	C$display.c$179$2$349 ==.
                                   1588 ;	..\COMMON\display.c:179: --dbglink_semaphore;
      000BC4 15 0D            [12] 1589 	dec	_dbglink_semaphore
                           0001D3  1590 	C$display.c$180$2$349 ==.
                                   1591 ;	..\COMMON\display.c:180: return;
      000BC6 02 0D CA         [24] 1592 	ljmp	00111$
      000BC9                       1593 00104$:
                           0001D6  1594 	C$display.c$182$1$348 ==.
                                   1595 ;	..\COMMON\display.c:182: pktlen = st->u.rx.pktlen + axradio_framing_maclen;
      000BC9 74 06            [12] 1596 	mov	a,#0x06
      000BCB 2E               [12] 1597 	add	a,r6
      000BCC FE               [12] 1598 	mov	r6,a
      000BCD E4               [12] 1599 	clr	a
      000BCE 3F               [12] 1600 	addc	a,r7
      000BCF FF               [12] 1601 	mov	r7,a
      000BD0 74 18            [12] 1602 	mov	a,#0x18
      000BD2 2E               [12] 1603 	add	a,r6
      000BD3 F5 82            [12] 1604 	mov	dpl,a
      000BD5 E4               [12] 1605 	clr	a
      000BD6 3F               [12] 1606 	addc	a,r7
      000BD7 F5 83            [12] 1607 	mov	dph,a
      000BD9 E0               [24] 1608 	movx	a,@dptr
      000BDA FC               [12] 1609 	mov	r4,a
      000BDB A3               [24] 1610 	inc	dptr
      000BDC E0               [24] 1611 	movx	a,@dptr
      000BDD FD               [12] 1612 	mov	r5,a
      000BDE 90 63 90         [24] 1613 	mov	dptr,#_axradio_framing_maclen
      000BE1 E4               [12] 1614 	clr	a
      000BE2 93               [24] 1615 	movc	a,@a+dptr
      000BE3 7A 00            [12] 1616 	mov	r2,#0x00
      000BE5 2C               [12] 1617 	add	a,r4
      000BE6 FC               [12] 1618 	mov	r4,a
      000BE7 EA               [12] 1619 	mov	a,r2
      000BE8 3D               [12] 1620 	addc	a,r5
      000BE9 FD               [12] 1621 	mov	r5,a
                           0001F7  1622 	C$display.c$183$1$348 ==.
                                   1623 ;	..\COMMON\display.c:183: wait_dbglink_free();
      000BEA C0 07            [24] 1624 	push	ar7
      000BEC C0 06            [24] 1625 	push	ar6
      000BEE C0 05            [24] 1626 	push	ar5
      000BF0 C0 04            [24] 1627 	push	ar4
      000BF2 12 0B 99         [24] 1628 	lcall	_wait_dbglink_free
                           000202  1629 	C$display.c$184$1$348 ==.
                                   1630 ;	..\COMMON\display.c:184: dbglink_writestr("RX counter=");
      000BF5 90 64 42         [24] 1631 	mov	dptr,#___str_3
      000BF8 75 F0 80         [24] 1632 	mov	b,#0x80
      000BFB 12 59 1E         [24] 1633 	lcall	_dbglink_writestr
                           00020B  1634 	C$display.c$186$1$348 ==.
                                   1635 ;	..\COMMON\display.c:186: dbglink_writehex16(pkts_received, 4, WRNUM_PADZERO);
      000BFE 90 04 7C         [24] 1636 	mov	dptr,#_pkts_received
      000C01 E0               [24] 1637 	movx	a,@dptr
      000C02 FA               [12] 1638 	mov	r2,a
      000C03 A3               [24] 1639 	inc	dptr
      000C04 E0               [24] 1640 	movx	a,@dptr
      000C05 FB               [12] 1641 	mov	r3,a
      000C06 74 08            [12] 1642 	mov	a,#0x08
      000C08 C0 E0            [24] 1643 	push	acc
      000C0A 03               [12] 1644 	rr	a
      000C0B C0 E0            [24] 1645 	push	acc
      000C0D 8A 82            [24] 1646 	mov	dpl,r2
      000C0F 8B 83            [24] 1647 	mov	dph,r3
      000C11 12 5D 1B         [24] 1648 	lcall	_dbglink_writehex16
      000C14 15 81            [12] 1649 	dec	sp
      000C16 15 81            [12] 1650 	dec	sp
                           000225  1651 	C$display.c$187$1$348 ==.
                                   1652 ;	..\COMMON\display.c:187: dbglink_writestr(" length=");
      000C18 90 64 4E         [24] 1653 	mov	dptr,#___str_4
      000C1B 75 F0 80         [24] 1654 	mov	b,#0x80
      000C1E 12 59 1E         [24] 1655 	lcall	_dbglink_writestr
      000C21 D0 04            [24] 1656 	pop	ar4
      000C23 D0 05            [24] 1657 	pop	ar5
                           000232  1658 	C$display.c$188$1$348 ==.
                                   1659 ;	..\COMMON\display.c:188: dbglink_writenum16(pktlen, 3, 0);
      000C25 C0 05            [24] 1660 	push	ar5
      000C27 C0 04            [24] 1661 	push	ar4
      000C29 E4               [12] 1662 	clr	a
      000C2A C0 E0            [24] 1663 	push	acc
      000C2C 74 03            [12] 1664 	mov	a,#0x03
      000C2E C0 E0            [24] 1665 	push	acc
      000C30 8C 82            [24] 1666 	mov	dpl,r4
      000C32 8D 83            [24] 1667 	mov	dph,r5
      000C34 12 61 74         [24] 1668 	lcall	_dbglink_writenum16
      000C37 15 81            [12] 1669 	dec	sp
      000C39 15 81            [12] 1670 	dec	sp
                           000248  1671 	C$display.c$189$1$348 ==.
                                   1672 ;	..\COMMON\display.c:189: wait_dbglink_free();
      000C3B 12 0B 99         [24] 1673 	lcall	_wait_dbglink_free
                           00024B  1674 	C$display.c$190$1$348 ==.
                                   1675 ;	..\COMMON\display.c:190: dbglink_writestr(" RSSI=");
      000C3E 90 64 57         [24] 1676 	mov	dptr,#___str_5
      000C41 75 F0 80         [24] 1677 	mov	b,#0x80
      000C44 12 59 1E         [24] 1678 	lcall	_dbglink_writestr
      000C47 D0 04            [24] 1679 	pop	ar4
      000C49 D0 05            [24] 1680 	pop	ar5
      000C4B D0 06            [24] 1681 	pop	ar6
      000C4D D0 07            [24] 1682 	pop	ar7
                           00025C  1683 	C$display.c$191$1$348 ==.
                                   1684 ;	..\COMMON\display.c:191: dbglink_writenum16(st->u.rx.phy.rssi, 3, WRNUM_SIGNED);
      000C4F 8E 82            [24] 1685 	mov	dpl,r6
      000C51 8F 83            [24] 1686 	mov	dph,r7
      000C53 E0               [24] 1687 	movx	a,@dptr
      000C54 FA               [12] 1688 	mov	r2,a
      000C55 A3               [24] 1689 	inc	dptr
      000C56 E0               [24] 1690 	movx	a,@dptr
      000C57 FB               [12] 1691 	mov	r3,a
      000C58 C0 07            [24] 1692 	push	ar7
      000C5A C0 06            [24] 1693 	push	ar6
      000C5C C0 05            [24] 1694 	push	ar5
      000C5E C0 04            [24] 1695 	push	ar4
      000C60 74 01            [12] 1696 	mov	a,#0x01
      000C62 C0 E0            [24] 1697 	push	acc
      000C64 74 03            [12] 1698 	mov	a,#0x03
      000C66 C0 E0            [24] 1699 	push	acc
      000C68 8A 82            [24] 1700 	mov	dpl,r2
      000C6A 8B 83            [24] 1701 	mov	dph,r3
      000C6C 12 61 74         [24] 1702 	lcall	_dbglink_writenum16
      000C6F 15 81            [12] 1703 	dec	sp
      000C71 15 81            [12] 1704 	dec	sp
                           000280  1705 	C$display.c$192$1$348 ==.
                                   1706 ;	..\COMMON\display.c:192: dbglink_tx('\n');
      000C73 75 82 0A         [24] 1707 	mov	dpl,#0x0a
      000C76 12 49 80         [24] 1708 	lcall	_dbglink_tx
                           000286  1709 	C$display.c$193$1$348 ==.
                                   1710 ;	..\COMMON\display.c:193: wait_dbglink_free();
      000C79 12 0B 99         [24] 1711 	lcall	_wait_dbglink_free
                           000289  1712 	C$display.c$194$1$348 ==.
                                   1713 ;	..\COMMON\display.c:194: dbglink_writestr("  freqoffset=");
      000C7C 90 64 5E         [24] 1714 	mov	dptr,#___str_6
      000C7F 75 F0 80         [24] 1715 	mov	b,#0x80
      000C82 12 59 1E         [24] 1716 	lcall	_dbglink_writestr
      000C85 D0 04            [24] 1717 	pop	ar4
      000C87 D0 05            [24] 1718 	pop	ar5
      000C89 D0 06            [24] 1719 	pop	ar6
      000C8B D0 07            [24] 1720 	pop	ar7
                           00029A  1721 	C$display.c$195$1$348 ==.
                                   1722 ;	..\COMMON\display.c:195: dbglink_writenum32(axradio_conv_freq_tohz(st->u.rx.phy.offset), 7, WRNUM_SIGNED);
      000C8D 8E 82            [24] 1723 	mov	dpl,r6
      000C8F 8F 83            [24] 1724 	mov	dph,r7
      000C91 A3               [24] 1725 	inc	dptr
      000C92 A3               [24] 1726 	inc	dptr
      000C93 E0               [24] 1727 	movx	a,@dptr
      000C94 F8               [12] 1728 	mov	r0,a
      000C95 A3               [24] 1729 	inc	dptr
      000C96 E0               [24] 1730 	movx	a,@dptr
      000C97 F9               [12] 1731 	mov	r1,a
      000C98 A3               [24] 1732 	inc	dptr
      000C99 E0               [24] 1733 	movx	a,@dptr
      000C9A FA               [12] 1734 	mov	r2,a
      000C9B A3               [24] 1735 	inc	dptr
      000C9C E0               [24] 1736 	movx	a,@dptr
      000C9D 88 82            [24] 1737 	mov	dpl,r0
      000C9F 89 83            [24] 1738 	mov	dph,r1
      000CA1 8A F0            [24] 1739 	mov	b,r2
      000CA3 C0 07            [24] 1740 	push	ar7
      000CA5 C0 06            [24] 1741 	push	ar6
      000CA7 C0 05            [24] 1742 	push	ar5
      000CA9 C0 04            [24] 1743 	push	ar4
      000CAB 12 07 A1         [24] 1744 	lcall	_axradio_conv_freq_tohz
      000CAE A8 82            [24] 1745 	mov	r0,dpl
      000CB0 A9 83            [24] 1746 	mov	r1,dph
      000CB2 AA F0            [24] 1747 	mov	r2,b
      000CB4 FB               [12] 1748 	mov	r3,a
      000CB5 74 01            [12] 1749 	mov	a,#0x01
      000CB7 C0 E0            [24] 1750 	push	acc
      000CB9 74 07            [12] 1751 	mov	a,#0x07
      000CBB C0 E0            [24] 1752 	push	acc
      000CBD 88 82            [24] 1753 	mov	dpl,r0
      000CBF 89 83            [24] 1754 	mov	dph,r1
      000CC1 8A F0            [24] 1755 	mov	b,r2
      000CC3 EB               [12] 1756 	mov	a,r3
      000CC4 12 5F EA         [24] 1757 	lcall	_dbglink_writenum32
      000CC7 15 81            [12] 1758 	dec	sp
      000CC9 15 81            [12] 1759 	dec	sp
                           0002D8  1760 	C$display.c$196$1$348 ==.
                                   1761 ;	..\COMMON\display.c:196: dbglink_writestr("Hz/");
      000CCB 90 64 6C         [24] 1762 	mov	dptr,#___str_7
      000CCE 75 F0 80         [24] 1763 	mov	b,#0x80
      000CD1 12 59 1E         [24] 1764 	lcall	_dbglink_writestr
                           0002E1  1765 	C$display.c$197$1$348 ==.
                                   1766 ;	..\COMMON\display.c:197: dbglink_writenum32(axradio_conv_freq_tohz(axradio_get_freqoffset()), 7, WRNUM_SIGNED);
      000CD4 12 3A 4D         [24] 1767 	lcall	_axradio_get_freqoffset
      000CD7 12 07 A1         [24] 1768 	lcall	_axradio_conv_freq_tohz
      000CDA A8 82            [24] 1769 	mov	r0,dpl
      000CDC A9 83            [24] 1770 	mov	r1,dph
      000CDE AA F0            [24] 1771 	mov	r2,b
      000CE0 FB               [12] 1772 	mov	r3,a
      000CE1 74 01            [12] 1773 	mov	a,#0x01
      000CE3 C0 E0            [24] 1774 	push	acc
      000CE5 74 07            [12] 1775 	mov	a,#0x07
      000CE7 C0 E0            [24] 1776 	push	acc
      000CE9 88 82            [24] 1777 	mov	dpl,r0
      000CEB 89 83            [24] 1778 	mov	dph,r1
      000CED 8A F0            [24] 1779 	mov	b,r2
      000CEF EB               [12] 1780 	mov	a,r3
      000CF0 12 5F EA         [24] 1781 	lcall	_dbglink_writenum32
      000CF3 15 81            [12] 1782 	dec	sp
      000CF5 15 81            [12] 1783 	dec	sp
                           000304  1784 	C$display.c$198$1$348 ==.
                                   1785 ;	..\COMMON\display.c:198: dbglink_writestr("Hz");
      000CF7 90 64 70         [24] 1786 	mov	dptr,#___str_8
      000CFA 75 F0 80         [24] 1787 	mov	b,#0x80
      000CFD 12 59 1E         [24] 1788 	lcall	_dbglink_writestr
                           00030D  1789 	C$display.c$199$1$348 ==.
                                   1790 ;	..\COMMON\display.c:199: dbglink_tx('\n');
      000D00 75 82 0A         [24] 1791 	mov	dpl,#0x0a
      000D03 12 49 80         [24] 1792 	lcall	_dbglink_tx
                           000313  1793 	C$display.c$200$1$348 ==.
                                   1794 ;	..\COMMON\display.c:200: wait_dbglink_free();
      000D06 12 0B 99         [24] 1795 	lcall	_wait_dbglink_free
                           000316  1796 	C$display.c$208$1$348 ==.
                                   1797 ;	..\COMMON\display.c:208: wait_dbglink_free();
      000D09 12 0B 99         [24] 1798 	lcall	_wait_dbglink_free
      000D0C D0 04            [24] 1799 	pop	ar4
      000D0E D0 05            [24] 1800 	pop	ar5
      000D10 D0 06            [24] 1801 	pop	ar6
      000D12 D0 07            [24] 1802 	pop	ar7
                           000321  1803 	C$display.c$211$2$350 ==.
                                   1804 ;	..\COMMON\display.c:211: const uint8_t __xdata *pktdata = st->u.rx.mac.raw;
      000D14 74 14            [12] 1805 	mov	a,#0x14
      000D16 2E               [12] 1806 	add	a,r6
      000D17 F5 82            [12] 1807 	mov	dpl,a
      000D19 E4               [12] 1808 	clr	a
      000D1A 3F               [12] 1809 	addc	a,r7
      000D1B F5 83            [12] 1810 	mov	dph,a
      000D1D E0               [24] 1811 	movx	a,@dptr
      000D1E FE               [12] 1812 	mov	r6,a
      000D1F A3               [24] 1813 	inc	dptr
      000D20 E0               [24] 1814 	movx	a,@dptr
      000D21 FF               [12] 1815 	mov	r7,a
                           00032F  1816 	C$display.c$212$4$352 ==.
                                   1817 ;	..\COMMON\display.c:212: for (i=0; i < pktlen; ++i) {
      000D22 7A 00            [12] 1818 	mov	r2,#0x00
      000D24 7B 00            [12] 1819 	mov	r3,#0x00
      000D26                       1820 00109$:
      000D26 C3               [12] 1821 	clr	c
      000D27 EA               [12] 1822 	mov	a,r2
      000D28 9C               [12] 1823 	subb	a,r4
      000D29 EB               [12] 1824 	mov	a,r3
      000D2A 9D               [12] 1825 	subb	a,r5
      000D2B 40 03            [24] 1826 	jc	00131$
      000D2D 02 0D BF         [24] 1827 	ljmp	00107$
      000D30                       1828 00131$:
                           00033D  1829 	C$display.c$213$3$351 ==.
                                   1830 ;	..\COMMON\display.c:213: if (!(i & 0x000F)) {
      000D30 EA               [12] 1831 	mov	a,r2
      000D31 54 0F            [12] 1832 	anl	a,#0x0f
      000D33 70 45            [24] 1833 	jnz	00106$
                           000342  1834 	C$display.c$214$4$352 ==.
                                   1835 ;	..\COMMON\display.c:214: dbglink_tx('\n');
      000D35 75 82 0A         [24] 1836 	mov	dpl,#0x0a
      000D38 12 49 80         [24] 1837 	lcall	_dbglink_tx
                           000348  1838 	C$display.c$215$4$352 ==.
                                   1839 ;	..\COMMON\display.c:215: wait_dbglink_free();
      000D3B C0 07            [24] 1840 	push	ar7
      000D3D C0 06            [24] 1841 	push	ar6
      000D3F C0 05            [24] 1842 	push	ar5
      000D41 C0 04            [24] 1843 	push	ar4
      000D43 C0 03            [24] 1844 	push	ar3
      000D45 C0 02            [24] 1845 	push	ar2
      000D47 12 0B 99         [24] 1846 	lcall	_wait_dbglink_free
      000D4A D0 02            [24] 1847 	pop	ar2
      000D4C D0 03            [24] 1848 	pop	ar3
                           00035B  1849 	C$display.c$216$4$352 ==.
                                   1850 ;	..\COMMON\display.c:216: dbglink_writehex16(i, 3, WRNUM_PADZERO);
      000D4E C0 03            [24] 1851 	push	ar3
      000D50 C0 02            [24] 1852 	push	ar2
      000D52 74 08            [12] 1853 	mov	a,#0x08
      000D54 C0 E0            [24] 1854 	push	acc
      000D56 74 03            [12] 1855 	mov	a,#0x03
      000D58 C0 E0            [24] 1856 	push	acc
      000D5A 8A 82            [24] 1857 	mov	dpl,r2
      000D5C 8B 83            [24] 1858 	mov	dph,r3
      000D5E 12 5D 1B         [24] 1859 	lcall	_dbglink_writehex16
      000D61 15 81            [12] 1860 	dec	sp
      000D63 15 81            [12] 1861 	dec	sp
                           000372  1862 	C$display.c$217$4$352 ==.
                                   1863 ;	..\COMMON\display.c:217: dbglink_writestr(": ");
      000D65 90 64 73         [24] 1864 	mov	dptr,#___str_9
      000D68 75 F0 80         [24] 1865 	mov	b,#0x80
      000D6B 12 59 1E         [24] 1866 	lcall	_dbglink_writestr
      000D6E D0 02            [24] 1867 	pop	ar2
      000D70 D0 03            [24] 1868 	pop	ar3
      000D72 D0 04            [24] 1869 	pop	ar4
      000D74 D0 05            [24] 1870 	pop	ar5
      000D76 D0 06            [24] 1871 	pop	ar6
      000D78 D0 07            [24] 1872 	pop	ar7
      000D7A                       1873 00106$:
                           000387  1874 	C$display.c$219$3$351 ==.
                                   1875 ;	..\COMMON\display.c:219: dbglink_writehex16(pktdata[i], 2, WRNUM_PADZERO);
      000D7A EA               [12] 1876 	mov	a,r2
      000D7B 2E               [12] 1877 	add	a,r6
      000D7C F5 82            [12] 1878 	mov	dpl,a
      000D7E EB               [12] 1879 	mov	a,r3
      000D7F 3F               [12] 1880 	addc	a,r7
      000D80 F5 83            [12] 1881 	mov	dph,a
      000D82 E0               [24] 1882 	movx	a,@dptr
      000D83 F8               [12] 1883 	mov	r0,a
      000D84 79 00            [12] 1884 	mov	r1,#0x00
      000D86 C0 07            [24] 1885 	push	ar7
      000D88 C0 06            [24] 1886 	push	ar6
      000D8A C0 05            [24] 1887 	push	ar5
      000D8C C0 04            [24] 1888 	push	ar4
      000D8E C0 03            [24] 1889 	push	ar3
      000D90 C0 02            [24] 1890 	push	ar2
      000D92 74 08            [12] 1891 	mov	a,#0x08
      000D94 C0 E0            [24] 1892 	push	acc
      000D96 74 02            [12] 1893 	mov	a,#0x02
      000D98 C0 E0            [24] 1894 	push	acc
      000D9A 88 82            [24] 1895 	mov	dpl,r0
      000D9C 89 83            [24] 1896 	mov	dph,r1
      000D9E 12 5D 1B         [24] 1897 	lcall	_dbglink_writehex16
      000DA1 15 81            [12] 1898 	dec	sp
      000DA3 15 81            [12] 1899 	dec	sp
      000DA5 D0 02            [24] 1900 	pop	ar2
      000DA7 D0 03            [24] 1901 	pop	ar3
      000DA9 D0 04            [24] 1902 	pop	ar4
      000DAB D0 05            [24] 1903 	pop	ar5
      000DAD D0 06            [24] 1904 	pop	ar6
      000DAF D0 07            [24] 1905 	pop	ar7
                           0003BE  1906 	C$display.c$220$3$351 ==.
                                   1907 ;	..\COMMON\display.c:220: dbglink_tx(' ');
      000DB1 75 82 20         [24] 1908 	mov	dpl,#0x20
      000DB4 12 49 80         [24] 1909 	lcall	_dbglink_tx
                           0003C4  1910 	C$display.c$212$2$350 ==.
                                   1911 ;	..\COMMON\display.c:212: for (i=0; i < pktlen; ++i) {
      000DB7 0A               [12] 1912 	inc	r2
      000DB8 BA 00 01         [24] 1913 	cjne	r2,#0x00,00134$
      000DBB 0B               [12] 1914 	inc	r3
      000DBC                       1915 00134$:
      000DBC 02 0D 26         [24] 1916 	ljmp	00109$
      000DBF                       1917 00107$:
                           0003CC  1918 	C$display.c$223$1$348 ==.
                                   1919 ;	..\COMMON\display.c:223: dbglink_writestr("\n\n");
      000DBF 90 64 76         [24] 1920 	mov	dptr,#___str_10
      000DC2 75 F0 80         [24] 1921 	mov	b,#0x80
      000DC5 12 59 1E         [24] 1922 	lcall	_dbglink_writestr
                           0003D5  1923 	C$display.c$224$1$348 ==.
                                   1924 ;	..\COMMON\display.c:224: --dbglink_semaphore;
      000DC8 15 0D            [12] 1925 	dec	_dbglink_semaphore
      000DCA                       1926 00111$:
                           0003D7  1927 	C$display.c$226$1$348 ==.
                           0003D7  1928 	XG$dbglink_received_packet$0$0 ==.
      000DCA 22               [24] 1929 	ret
                                   1930 	.area CSEG    (CODE)
                                   1931 	.area CONST   (CODE)
                           000000  1932 Fdisplay$__str_0$0$0 == .
      0063FA                       1933 ___str_0:
      0063FA 50 3A 20 20 20 20 20  1934 	.ascii "P:      T:      "
             20 54 3A 20 20 20 20
             20 20
      00640A 0A                    1935 	.db 0x0a
      00640B 4C 3A 20 20 20 20 20  1936 	.ascii "L:      t:      "
             20 74 3A 20 20 20 20
             20 20
      00641B 0A                    1937 	.db 0x0a
      00641C 00                    1938 	.db 0x00
                           000023  1939 Fdisplay$__str_1$0$0 == .
      00641D                       1940 ___str_1:
      00641D 50 3A 20 20 20 20 20  1941 	.ascii "P:      O:      "
             20 4F 3A 20 20 20 20
             20 20
      00642D 0A                    1942 	.db 0x0a
      00642E 4C 3A 20 20 20 20 20  1943 	.ascii "L:      R:      "
             20 52 3A 20 20 20 20
             20 20
      00643E 0A                    1944 	.db 0x0a
      00643F 00                    1945 	.db 0x00
                           000046  1946 Fdisplay$__str_2$0$0 == .
      006440                       1947 ___str_2:
      006440 3F                    1948 	.ascii "?"
      006441 00                    1949 	.db 0x00
                           000048  1950 Fdisplay$__str_3$0$0 == .
      006442                       1951 ___str_3:
      006442 52 58 20 63 6F 75 6E  1952 	.ascii "RX counter="
             74 65 72 3D
      00644D 00                    1953 	.db 0x00
                           000054  1954 Fdisplay$__str_4$0$0 == .
      00644E                       1955 ___str_4:
      00644E 20 6C 65 6E 67 74 68  1956 	.ascii " length="
             3D
      006456 00                    1957 	.db 0x00
                           00005D  1958 Fdisplay$__str_5$0$0 == .
      006457                       1959 ___str_5:
      006457 20 52 53 53 49 3D     1960 	.ascii " RSSI="
      00645D 00                    1961 	.db 0x00
                           000064  1962 Fdisplay$__str_6$0$0 == .
      00645E                       1963 ___str_6:
      00645E 20 20 66 72 65 71 6F  1964 	.ascii "  freqoffset="
             66 66 73 65 74 3D
      00646B 00                    1965 	.db 0x00
                           000072  1966 Fdisplay$__str_7$0$0 == .
      00646C                       1967 ___str_7:
      00646C 48 7A 2F              1968 	.ascii "Hz/"
      00646F 00                    1969 	.db 0x00
                           000076  1970 Fdisplay$__str_8$0$0 == .
      006470                       1971 ___str_8:
      006470 48 7A                 1972 	.ascii "Hz"
      006472 00                    1973 	.db 0x00
                           000079  1974 Fdisplay$__str_9$0$0 == .
      006473                       1975 ___str_9:
      006473 3A 20                 1976 	.ascii ": "
      006475 00                    1977 	.db 0x00
                           00007C  1978 Fdisplay$__str_10$0$0 == .
      006476                       1979 ___str_10:
      006476 0A                    1980 	.db 0x0a
      006477 0A                    1981 	.db 0x0a
      006478 00                    1982 	.db 0x00
                                   1983 	.area XINIT   (CODE)
                                   1984 	.area CABS    (ABS,CODE)
