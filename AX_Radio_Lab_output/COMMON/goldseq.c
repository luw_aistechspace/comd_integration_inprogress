
#include <libmftypes.h>
#include <libmfflash.h>
#include <libmfdbglink.h>
#include <ax8052crypto.h>
#include <ax8052cryptoregaddr.h>
#include <libmfcrypto.h>
#include "misc.h"
#include "memory.h"
// #include <string.h>
#define USE_DBGLINK

/**
*
* \brief This function loads the lookup table in eeprom memory
*
*/
/*__xdata*/ void GOLDSEQUENCE_Init(void)
{
    uint8_t i = 0;


    flash_unlock();
    //flash_pageerase(MEM_GOLD_SEQUENCES_START+0x0002);

//    EraseMemoryPage(MEM_GOLD_SEQUENCES_START+1);
    i = flash_pageerase(MEM_GOLD_SEQUENCES_START/* + 1*/);
/*#ifdef USE_DBGLINK
        dbglink_writestr("flash erase = ");
        dbglink_writenum16(i, 2, WRNUM_PADZERO);
        dbglink_tx('\n');
#endif // USE_DBGLINK*/

    flash_write(MEM_GOLD_SEQUENCES_START+0x0000,0b1111100110100100); // 0xF9A4

  //  delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0002,0b001010111011000); // 0x15D8
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0004,0b1111100100110000); // 0xF930
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0006,0b101101010001110); // 0x5A8E
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0008,0b0000000010010100); // 0x0094
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x000A,0b100111101010110); // 0x4F56
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x000C,0b1000010100111100); // 0x853C
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x000E,0b011100010011111); // 0x389F
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0010,0b0100011111101000); // 0x47E8
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0012,0b000001101111011); // 0x037B
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0014,0b0010011010000010); // 0x2682
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0016,0b001111010001001); // 0x1E89
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0018,0b0001011000110111); //0x1637
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x001A,0b001000001110000); // 0x1070
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x001C,0b1000111001101101); // 0x8E6D
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x001E,0b101011100001100); // 0x570C
  //  delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0020,0b1100001001000000); // 0xC240
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0022,0b111010010110010); // 0x74B2
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0024,0b1110010001010110); // 0xE456
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0026,0b010010101101101); // 0x256D
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0028,0b0111011101011101); // 0x775D
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x002A,0b000110110000010); // 0x0D82
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x002C,0b1011111011011000); // 0xBED8
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x002E,0b101100111110101); // 0x59F5
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0030,0b0101101000011010);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0032,0b011001111001110);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0034,0b1010100001111011);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0036,0b000011011010011);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0038,0b0101000101001011); // 0x514B
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x003A,0b101110001011101); // 0xB8BA
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x003C,0b0010110111010011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x003E,0b111000100011010);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0040,0b1001001110011111);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0042,0b110011110111001);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0044,0b0100110010111001);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0046,0b110110011101000);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0048,0b1010001100101010); // 0xA32A
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x004A,0b110100101000000); // 0x6940
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x004C,0b1101010011100011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x004E,0b010101110010100);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0050,0b1110111100000111);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0052,0b100101011111110); // 0x4AFE
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0054,0b1111001011110101); // 0xF2F5
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0056,0b111101001001011); // 0x7A4B
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0058,0b0111110000001100); // 0x7C0C
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x005A,0b110001000010001); //0x6211
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x005C,0b0011101101110000); // 0x3B70
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x005E,0b010111000111100); // 0x2E3C
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0060,0b1001100011001110); // 0x98CE
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0062,0b000100000101010);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0064,0b1100100100010001); // 0xC911
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0066,0b001101100100001); // 0x1B21
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0068,0b0110000111111110); // 0x61FE
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x006A,0b101001010100100); // 0x52A4
//    delay_ms(5);
////////////////////////////////////////////////////////////////////////////////////
    flash_write(MEM_GOLD_SEQUENCES_START+0x006C,0b1011010110001001);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x006E,0b011011001100110);
 //   delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0070,0b1101111110110010);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0072,0b100010000000111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0074,0b0110101010101111);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0076,0b011110100110111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x0078,0b0011000000100001);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x007A,0b100000110101111);
//    delay_ms(5);

    flash_write(MEM_GOLD_SEQUENCES_START+0x007C,0b0001110101100110);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x007E,0b111111111100011);
//    delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0080,0b0000101111000101);
 //   delay_ms(5);
    flash_write(MEM_GOLD_SEQUENCES_START+0x0082,0b010000011000101);
//    delay_ms(5);
    flash_lock();


}

/**
* \brief This function generates a random number (between 1 and 33) and looks for the matching gold sequence stored in memory
*
* @return Random vector from the Gold sequence
*
*/

__xdata uint32_t GOLDSEQUENCE_Obtain(void)
{

__xdata uint16_t Rnd;
__xdata uint32_t RetVal;
Rnd = RNGBYTE;
flash_unlock();

delay_ms(50);

Rnd = ((Rnd % 33)*4);  // MOD operation is done so that the number is between 0 and 33, once a number between 0 and 32 has been obtained, its multiplied by 4 to match the memory address



RetVal = ((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd+2))<<16;

RetVal |= (((uint32_t)flash_read(MEM_GOLD_SEQUENCES_START+Rnd)) & 0x0000FFFF)/*<<1*/;
// question 2021-04-26: why <<1 ?

#ifdef USE_DBGLINK
    dbglink_writestr("gold rnd: ");
    dbglink_writehex16(Rnd, 4, WRNUM_PADZERO);
    dbglink_writestr(" ");
    dbglink_writehex32(RetVal, 8, WRNUM_PADZERO);
    dbglink_tx('\n');
#endif // USE_DBGLINK

flash_lock();
return(RetVal);
}
