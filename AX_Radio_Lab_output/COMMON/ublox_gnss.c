// Libraries
#include <libmftypes.h>
#include <libmfdbglink.h>
#include <libmfuart.h>
#include <libmfuart0.h>
#include <libmfuart1.h>
#include <stdlib.h>
#include <string.h>

#include "ublox_gnss.h"

// Macros


// External variables

// Global variables


/**
*
* \brief This function calculates fletcher 8 bit checksum as explained in Ublox documentation
*
* @param[in]  buffer: pointer to buffer data, must only include message payload
* @param[out] CK_A: Variable where the first byte of the checksum is saved
* @param[out] CK_B: Variable where the second byte of the checksum is saved
* @param[in]  length: Lenght of the payload data
*
*/

void UBLOX_GPS_FletcherChecksum8 ( uint8_t * buffer, uint8_t *CK_A, uint8_t *CK_B, uint16_t length)
{
    uint8_t i;
    *CK_A = 0;
    *CK_B = 0;
	for (i = 0; i < length; i++)
    {
		*CK_A += buffer[i];

		*CK_B += *CK_A;
	}
	return;
}

/**
* \brief This function gets the different components of a Ublox messages, calculates the checksum, creates the UBX packet and sends it via UART
*
* @param[in]   msg_class: message class according to Ublox documentation
* @param[in]   msg_id: message identifier (depends on message class)
* @param[in]   msg_length: payload length
* @param[out]  payload: buffer with payload data.
*
* @return 1: ACK received correctly, 0: NAK received
*/

uint8_t UBLOX_GPS_SendCommand_WaitACK(uint8_t msg_class, uint8_t msg_id, uint16_t msg_length,uint8_t *payload,uint8_t AnsOrAck,uint16_t *RxLength)
{   uint8_t i;
    uint8_t k;
    uint8_t RetVal = 0;
    uint8_t __xdata *buffer_aux;
    uint8_t __xdata *rx_buffer;
    uint8_t CK_A,CK_B;
    buffer_aux =(uint8_t *) malloc((msg_length)+10);
    rx_buffer =(uint8_t *) malloc(40);
     for(i = 0; i<40;i++)
    {
        *(rx_buffer+i)=0;
    }
    i=0;
    *buffer_aux = msg_class;
    buffer_aux++;
    *buffer_aux = msg_id;
    buffer_aux++;
    *buffer_aux = msg_length;
    buffer_aux +=2;
    memcpy(buffer_aux,payload,msg_length);
    buffer_aux -=4;
    UBLOX_GPS_FletcherChecksum8(buffer_aux,&CK_A,&CK_B,(msg_length)+4);

    uart1_tx(UBX_HEADER1_VAL);
    uart1_tx(UBX_HEADER2_VAL);
    uart1_tx(msg_class);
    uart1_tx(msg_id);
    uart1_tx((uint8_t)(msg_length & 0x00FF));//LSB
    uart1_tx((uint8_t)((msg_length & 0xFF00)>>8));//MSB


    for(i = 0;i<msg_length;i++)
    {
        uart1_tx(*(payload+i));

    }
    uart1_tx(CK_A);
    uart1_tx(CK_B);
#ifdef USE_DBGLINK
    dbglink_writestr("command sent\n");
#endif // USE_DBGLINK
   //message reception,
   do{
    delay(2500);
/*#ifdef USE_DBGLINK
    dbglink_writenum16(uart1_rxcount(), 4, WRNUM_PADZERO);
    dbglink_tx('\n');
#endif // USE_DBGLINK*/
   }while(!uart1_rxcount());
   delay(25000);
   k=0;

    do
    {
 //       wtimer_runcallbacks(); // si no pongo esto por algun motivo se pierden los eventos de timer , cuidado con los delays !!
        *(rx_buffer+k)=uart1_rx();
        k++;

        delay(2500);
    }while(uart1_rxcount());

//RECORDAR EL ENDIANNES
#ifdef USE_DBGLINK
    dbglink_writestr("msg rcv\n");
#endif // USE_DBGLINK

    //format and CRC verification
  if(rx_buffer[UBX_HEADER1_POS] == UBX_HEADER1_VAL && rx_buffer[UBX_HEADER2_POS] == UBX_HEADER2_VAL)
    {
        CK_A = 0;
        CK_B = 0;
        UBLOX_GPS_FletcherChecksum8(&rx_buffer[UBX_MESSAGE_CLASS],&CK_A,&CK_B,k-4);
         if(CK_A == rx_buffer[k-2] && CK_B == rx_buffer[k-1]) // verifico el checksum calculado
            {
                //CRC and format verified, decide if ANS or ACK
                if(!AnsOrAck)
                //ANS
                {
                    if(msg_class == rx_buffer[UBX_MESSAGE_CLASS] && msg_id == rx_buffer[UBX_MESSAGE_ID])
                    {
                        *RxLength = rx_buffer[UBX_MESSAGE_LENGTH_MSB]<<8 | rx_buffer[UBX_MESSAGE_LENGTH_LSB];
                        memcpy(payload,&rx_buffer[UBX_MESSAGE_PAYLOAD],*RxLength);
                        RetVal = OK;
                    }
                }
                else
                //ACK
                {
                    if(rx_buffer[UBX_MESSAGE_CLASS] == eACK && rx_buffer[UBX_MESSAGE_ID] == ACK) RetVal = ACK;
                    else RetVal = NAK;
                }
            }
    }
    free(buffer_aux);
    free(rx_buffer);
    return RetVal;
}

/**
*
* \brief This function inits the necesarry UART to communicate with the GSP hardware
*
*/
void UBLOX_GPS_PortInit(void)
{
    __xdata uint8_t  UBLOX_setNav1[]= {0xF0 ,0x04 ,0x00};/**< \brief Initial cofiguration for UBLOX GPS*/
    __xdata uint8_t  UBLOX_setNav2[]= {0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; /**< \brief Initial cofiguration for UBLOX GPS*/
    __xdata uint8_t  UBLOX_setNav3[] = {0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};/**< \brief Initial cofiguration for UBLOX GPS*/
    __xdata uint8_t  UBLOX_setNav5[] ={0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
    0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00};            /**< \brief Initial cofiguration for UBLOX GPS*/
    PORTA_1 = 1;
    uart_timer1_baud(CLKSRC_FRCOSC, 9600, 20000000UL);
    uart1_init(1, 8, 1);

    //GPS initialization  (eliminate NMAE messages, only UBX by pooling

    #ifdef USE_DBGLINK
    dbglink_writestr(" GPS init start\n ");
    #endif // USE_DBGLINK
    UBLOX_setNav1[1]=0x02;
    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));
    #ifdef USE_DBGLINK
    dbglink_writestr(" GPS init 1\n ");
    #endif // USE_DBGLINK

    UBLOX_setNav1[1]=0x01;
    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));

    #ifdef USE_DBGLINK
    dbglink_writestr(" GPS init 2\n ");
    #endif // USE_DBGLINK

    UBLOX_setNav1[1]=0x00;
    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));

     #ifdef USE_DBGLINK
    dbglink_writestr(" GPS init 3\n ");
    #endif // USE_DBGLINK

    UBLOX_setNav1[1]=0x03;
    do{
     delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));

    #ifdef USE_DBGLINK
    dbglink_writestr(" GPS init 4\n ");
    #endif // USE_DBGLINK

    UBLOX_setNav1[1]=0x05;
    do{
     delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x03,UBLOX_setNav1,ACK,NULL));

    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav2,ACK,NULL));

    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x01,0x08,UBLOX_setNav3,ACK,NULL));

    do{
    delay(5000);
    }while(!UBLOX_GPS_SendCommand_WaitACK(0x06,0x24,sizeof(UBLOX_setNav5),UBLOX_setNav5,ACK,NULL));
    #ifdef USE_DBGLINK
    dbglink_writestr("GPS init END\n");
    #endif // dbglink
}
